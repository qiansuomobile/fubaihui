//
//  YYYStepper.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/7.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYYStepper : UIView
@property (assign, nonatomic) CGFloat value;
@property (assign, nonatomic) CGFloat minimumValue;
@property (assign, nonatomic) CGFloat maximumValue;
@property (assign, nonatomic) CGFloat stepValue;
@property (assign, nonatomic) BOOL wraps;
@property (assign, nonatomic) BOOL continuous;
@property (assign, nonatomic) BOOL autorepeat;
@property (assign, nonatomic) CGFloat autorepeatInterval;
-(void)createUI;

@end


