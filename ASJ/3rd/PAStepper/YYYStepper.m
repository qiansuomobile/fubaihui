//
//  YYYStepper.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/7.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YYYStepper.h"
@interface YYYStepper()
@property (nonatomic,strong) UIButton *decrementButton;
@property (nonatomic,strong) UIButton *incrementButton;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) NSNumber *changingValue;
@property (readonly, nonatomic) NSNumberFormatter *numberFormater;
@end
@implementation YYYStepper

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _stepValue = 1.0;
    _countL = [[UILabel alloc] init];
    //默认1
    _countL.text = @"1";
    _countL.textAlignment = NSTextAlignmentCenter;
    _countL.font = [UIFont systemFontOfSize:18*KHeight];
    _countL.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-1*KHeight);
        make.width.mas_equalTo(60*Kwidth);
    }];
    
    _decrementButton = [[UIButton alloc] init];
    [_decrementButton setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [_decrementButton setTitleColor:RGBColor(150, 150, 150) forState:UIControlStateNormal];
    _decrementButton.titleLabel.font = [UIFont systemFontOfSize:18*KHeight];
    [_decrementButton setTitle:@"-" forState:UIControlStateNormal];
    [_decrementButton addTarget:self action:@selector(didPressButton:) forControlEvents:UIControlEventTouchUpInside];
    [_decrementButton addTarget:self action:@selector(didBeginLongTap:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragEnter];
    [_decrementButton addTarget:self action:@selector(didEndLongTap) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside | UIControlEventTouchCancel | UIControlEventTouchDragExit];
    [self addSubview:_decrementButton];
    [_decrementButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-1*KHeight);
        make.right.equalTo(_countL.mas_left).offset(-1*Kwidth);
    }];
    if (_value <= 1) {
        _decrementButton.userInteractionEnabled = NO;
    }
    _incrementButton = [[UIButton alloc] init];
    [_incrementButton setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [_incrementButton setTitle:@"+" forState:UIControlStateNormal];
    [_incrementButton setTitleColor:RGBColor(150, 150, 150) forState:UIControlStateNormal];
    _incrementButton.titleLabel.font = [UIFont systemFontOfSize:18*KHeight];
    [_incrementButton addTarget:self action:@selector(didPressButton:) forControlEvents:UIControlEventTouchUpInside];
    [_incrementButton addTarget:self action:@selector(didBeginLongTap:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragEnter];
    [_incrementButton addTarget:self action:@selector(didEndLongTap) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside | UIControlEventTouchCancel | UIControlEventTouchDragExit];
    [self addSubview:_incrementButton];
    [_incrementButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_countL.mas_right).offset(1*Kwidth);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-1*KHeight);
        make.right.equalTo(self.mas_right).offset(-1*Kwidth);
    }];
    
}

#pragma mark - Actions

- (void)didPressButton:(id)sender
{
//    if (_changingValue) {
//        return;
//    }
    UIButton *button = (UIButton *)sender;
    double changeValue;
    if (button == _decrementButton) {
        if (_value <= 1) {
            _decrementButton.userInteractionEnabled = NO;
        }else{
            _value += -1 * _stepValue;
            if (_value < 1) {
                _value += _stepValue;
                [MBProgressHUD showSuccess:@"不能再减啦亲" toView:self.superview];
            }
        }
        
    } else {
        _decrementButton.userInteractionEnabled = YES;
        _value += _stepValue;
    }
    _changingValue = [NSNumber numberWithDouble:changeValue];
    [self performSelector:@selector(changeValue:) withObject:_changingValue afterDelay:0.5];
    self.countL.text = [NSString stringWithFormat:@"%0.0f",_value];
}

- (void)didBeginLongTap:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    UIButton *button = (UIButton *)sender;
    double changeValue;
    
    if (button == _decrementButton) {
        changeValue = -1 * _stepValue;
    } else {
        changeValue = _stepValue;
    }
    _changingValue = [NSNumber numberWithDouble:changeValue];
    if (_continuous) {
        [self changeValue:_changingValue];
    }
    [self performSelector:@selector(longTapLoop) withObject:nil afterDelay:_autorepeatInterval];
    //self.countL.text = [NSString stringWithFormat:@"%@",_changingValue];
}

- (void)didEndLongTap
{
    
    if (!_continuous) {
        [self performSelectorOnMainThread:@selector(changeValue:) withObject:_changingValue waitUntilDone:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _changingValue = nil;
}

#pragma mark - Other methods
- (void)changeValue:(NSNumber *)change
{
    double toChange = [change doubleValue];
    double newValue = _value + toChange;
    if (toChange < 0) {
        if (newValue < _minimumValue) {
            if (!_wraps) {
                return;
            } else {
                newValue = _maximumValue;
            }
        }
    } else {
        if (newValue > _maximumValue) {
            if (!_wraps) {
                return;
            } else {
                newValue = _minimumValue;
            }
        }
    }
    [self setValue:newValue];
}

- (void)longTapLoop
{
    if (_autorepeat) {
        [self performSelector:@selector(longTapLoop) withObject:nil afterDelay:_autorepeatInterval];
        [self performSelectorOnMainThread:@selector(changeValue:) withObject:_changingValue waitUntilDone:YES];
    }
}

- (void)setValue:(double)val
{
    if (val < _minimumValue) {
        val = _minimumValue;
    } else if (val > _maximumValue) {
        val = _maximumValue;
    }
    _value = val;
    
    if (_value == val) {
        [self updateViews];
    }
}

- (void)updateViews
{
    NSLog(@"执行了");
    NSString *formatedValueString = [self formatedStringForValue:_value];
    _countL.text = formatedValueString;
    
}

- (NSString *)formatedStringForValue:(double)value{
    
    NSString *formatedValueString = [_numberFormater stringFromNumber:[NSNumber numberWithDouble:value]];
    return formatedValueString;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
