//
//  SaoYiSaoViewController.m
//  Diablo3
//
//  Created by 趙傳龍 on 15/6/2.
//  Copyright (c) 2015年 趙傳龍. All rights reserved.
//

#import "SaoYiSaoViewController.h"
#import "OtherCodeView.h"
#import <AVFoundation/AVFoundation.h>
#import "AdvertisementViewController.h"
#import "YYYShopQRCodeViewController.h"

@interface SaoYiSaoViewController ()<AVCaptureMetadataOutputObjectsDelegate>
@property (strong ,nonatomic)AVCaptureSession *session;
@property (strong ,nonatomic)AVCaptureVideoPreviewLayer *previewLayer;
@end

@implementation SaoYiSaoViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"二维码";
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self readQRcode];
    OtherCodeView *qrRectView = [[OtherCodeView alloc] initWithFrame:self.view.frame];
    qrRectView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:qrRectView];
    
    
    CGFloat screenHeight = self.view.frame.size.height;
    CGFloat screenWidth = self.view.frame.size.width;
    UIImageView *viewfinderView = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth-205)/2, (screenHeight-205)/2, 205, 205)];
    viewfinderView.image = [UIImage imageNamed:@"viewfinder"];
    [self.view addSubview:viewfinderView];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // something
        [_session startRunning];
    });
}

- (void)readQRcode{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (error) {
        NSLog(@"没有摄像头--%@",error);
        return;
    }
    
    
    //设置输出
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc]init];
    //输出代理
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    //拍摄回话
    AVCaptureSession *session = [[AVCaptureSession alloc]init];
    //添加session的输入和输出
    [session addInput:input];
    [session addOutput:output];
    //设置输出格式
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    
    //设置预览图层
    AVCaptureVideoPreviewLayer *preview = [AVCaptureVideoPreviewLayer layerWithSession:session];
    //设置preview的图层属性
    [preview setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //设置preview图层大小
    [preview setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //将图层添加到视图的图层
    [self.view.layer insertSublayer:preview above:0];
    
    
    
    //限制扫描区域
    CGFloat screenHeight = self.view.frame.size.height;
    CGFloat screenWidth = self.view.frame.size.width;
    CGRect cropRect = CGRectMake((screenWidth - 200) / 2,
                                 (screenHeight - 200) / 2,
                                 200,
                                 200);
    
    [output setRectOfInterest:CGRectMake(cropRect.origin.y / screenHeight,
                                          cropRect.origin.x / screenWidth,
                                          cropRect.size.height / screenHeight,
                                          cropRect.size.width / screenWidth)];
    
    self.previewLayer = preview;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // something
        [session startRunning];
    });
    self.session = session;
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    NSLog(@"扫描结果");
    
    [self.session stopRunning];

    if (metadataObjects.count > 0) {
        //yud
        NSString *url = [metadataObjects[0] stringValue];
        
        if ([url hasPrefix:@"http"]) {
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] initWithUrl:url];
            [self.navigationController pushViewController:adVC animated:YES];

        }else if([url hasPrefix:@"id"]){
        //id=153
            NSString *storeID = [url substringFromIndex:3];
            YYYShopQRCodeViewController *adVC = [[YYYShopQRCodeViewController alloc] init];
            adVC.theID = storeID;
            [self.navigationController pushViewController:adVC animated:YES];
        }
    }

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
