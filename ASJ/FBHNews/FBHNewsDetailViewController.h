//
//  FBHNewsDetailViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/8.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface FBHNewsDetailViewController : UIViewController

@property (nonatomic ,copy)NSString *newsID;

@end

NS_ASSUME_NONNULL_END
