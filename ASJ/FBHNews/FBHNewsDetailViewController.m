//
//  FBHNewsDetailViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/8.
//  Copyright © 2019 TS. All rights reserved.
//

#import "FBHNewsDetailViewController.h"

@interface FBHNewsDetailViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *newsWebView;

@end

@implementation FBHNewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"资讯详情";
    NSString *urlStr = FBHRequestUrl(kUrl_public(_newsID, @"1"));
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [_newsWebView loadRequest:requset];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
