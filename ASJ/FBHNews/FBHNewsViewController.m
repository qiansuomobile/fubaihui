//
//  FBHNewsViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/8.
//  Copyright © 2019 TS. All rights reserved.
//

#import "FBHNewsViewController.h"
#import "FBHNewsCell.h"
#import "FBHNewsDetailViewController.h"

@interface FBHNewsViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *newsTableView;

@property (nonatomic, strong)NSMutableArray *newsArr;

@end

@implementation FBHNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"资讯";
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self newsDataWithRequest];
}

#pragma mark - URL Data Request
- (void)newsDataWithRequest{
    
    [NetMethod Post:FBHRequestUrl(kUrl_information) parameters:nil success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"code"] intValue] == 200) {
            _newsArr = [NSMutableArray arrayWithArray:dic[@"list"]];
            [_newsTableView reloadData];
            
        }else{
            [MBProgressHUD showError:dic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma MARK - UITableViewDataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = _newsArr[indexPath.row];
    
    FBHNewsDetailViewController *fbhNewsDetailViewController = [[FBHNewsDetailViewController alloc]init];
    fbhNewsDetailViewController.newsID = dic[@"id"];
    [self.navigationController pushViewController:fbhNewsDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _newsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"NewsCell";
    FBHNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"FBHNewsCell" owner:self options:nil]lastObject];
    }
    NSDictionary *dic = _newsArr[indexPath.row];
    cell.titleLabel.text = dic[@"title"];
    cell.detailLabel.text = dic[@"present"];
    cell.timeLabel.text = [self getTimeFromTimestamp:dic[@"time"]];
    [cell.newsImageView sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(dic[@"picture"])] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
    return cell;
}

- (NSString *)getTimeFromTimestamp:(NSString *)time{
    
    //将对象类型的时间转换为NSDate类型
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    
    //设置时间格式
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //将时间转换为字符串
    
    NSString *timeStr=[formatter stringFromDate:myDate];
    
    return timeStr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
