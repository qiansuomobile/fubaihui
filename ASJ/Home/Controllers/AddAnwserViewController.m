//
//  AddAnwserViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AddAnwserViewController.h"
@interface AddAnwserViewController ()<UITextViewDelegate>
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) CustomTextView *textView;
@end

@implementation AddAnwserViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self createUI];
}

-(void)createUI{
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initNavigationBar];
    _titleL = [[UILabel alloc] init];
    _titleL.textColor = RGBColor(150, 150, 150);
    _titleL.font = [UIFont systemFontOfSize:20*KHeight];
    _titleL.text = self.model.title;
    [self.view addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10*Kwidth);
        make.right.equalTo(self.view.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.view.mas_top).offset(15*KHeight);
    }];
    
    _textView = [[CustomTextView alloc] initWithLimit:NO];
    _textView.delegate = self;
    _textView.placeholder = @" 添加回答";
    _textView.font = [UIFont systemFontOfSize:20*KHeight];
    _textView.placeholderL.font = [UIFont systemFontOfSize:20*KHeight];
    [self.view addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(_titleL.mas_bottom).offset(15*KHeight);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
    }];
}


-(void)initNavigationBar{
    self.title = @"回答";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
    
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(0, 0, 50, 50);
    [rightBtn setTitle:@"提交" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItemBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItemBtn;
}

//点击提交
-(void)clickSubmit{
    if (_textView.text.length <= 0) {
        [MBProgressHUD showSuccess:@"请输入您的回答" toView:self.view];
        return;
    }else{
        
        NSDictionary *dic = @{@"pid":self.model.question_id,@"uid":LoveDriverID,@"content":self.textView.text};
        
        [NetMethod Post:LoveDriverURL(@"APP/Problem/replyAdd") parameters:dic success:^(id responseObject) {
            
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } failure:^(NSError *error) {
            
            [MBProgressHUD showSuccess:NetProblem toView:self.view];
            
        }];
        
    }
    
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    _textView.placeholderL.hidden = YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
        if (textView.text.length == 0) {
            _textView.placeholderL.hidden = NO;
        }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
