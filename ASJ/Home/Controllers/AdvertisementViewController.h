//
//  AdvertisementViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertisementViewController : UIViewController
@property (nonatomic,copy) NSString *Ad_ID;
- (instancetype)initWithUrl:(NSString *)url;
@end
