//
//  AdvertisementViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AdvertisementViewController.h"
#import <WebKit/WebKit.h>

@interface AdvertisementViewController ()<UIWebViewDelegate,WKUIDelegate, WKNavigationDelegate>

@property(nonatomic,strong) WKWebView *webView;

@property (nonatomic ,strong) UIProgressView *loadWebProgressView;

@property(nonatomic,strong) NSString *yyyUrl;

@end

@implementation AdvertisementViewController

//yu
- (instancetype)initWithUrl:(NSString *)url
{
    self = [super init];
    if (self) {
        _yyyUrl = url;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initWebView];

}
-(WKWebView *)webView{
    
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0,  MainScreen.width, MainScreen.height -64)];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
    }
    return _webView;
}
-(void)initWebView{

    [self.view addSubview:self.webView];
    
    NSURLRequest *request;
    
    if (_yyyUrl) {
        //yu
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.yyyUrl]]];
         [_webView loadRequest:request];

    }else{
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL ,self.Ad_ID]]];
        
        NSString *path =[NSString stringWithFormat:@"%@%@",FBHBaseURL ,self.Ad_ID];
        [GCUtil YzSetWebCache:path webView:_webView];
    }
    

    
    
    
    self.loadWebProgressView = [[UIProgressView alloc]init];
    
    self.loadWebProgressView.frame = CGRectMake(0, 0, YzWidth, 4);
    
    self.loadWebProgressView.progressTintColor = RGBColor(255, 77, 33);
    
    self.loadWebProgressView.trackTintColor = [UIColor clearColor];

    [_webView addSubview:self.loadWebProgressView];
    
  
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//       
//        [_webView loadRequest:request];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//        
//            [self.view addSubview:_webView];
//        
//        });
//   
//    });




}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    [self.loadWebProgressView setProgress:0.3];
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [self.loadWebProgressView setProgress:0.5];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self.loadWebProgressView setProgress:1];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.loadWebProgressView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.loadWebProgressView removeFromSuperview];
    }];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [self.loadWebProgressView setProgress:0.8];
    //    self.mainWebView.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
