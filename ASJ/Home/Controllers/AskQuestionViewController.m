//
//  AskQuestionViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/31.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AskQuestionViewController.h"
#import "QuestionType.h"

#define rewardWidth (self.view.frame.size.width-100*Kwidth)/4
#define rewardHeight 45*KHeight
#define categoryWidth (self.view.frame.size.width-110*Kwidth)/5

@interface AskQuestionViewController ()<UITextViewDelegate>
@property (nonatomic,strong) CustomTextView *askTextView;//提问
@property (nonatomic,strong) CustomTextView *suppleTextView;//补充
@property (nonatomic,strong) UIView *bottomView;
@property (nonatomic,strong) UIView *rewardView;
@property (nonatomic,strong) UIView *categroyView;
@property (nonatomic,strong) NSMutableArray *btnArr;
//当前选中btn
@property (nonatomic,strong) UIButton *currentBtn;
//是否选好赏金
@property (nonatomic,assign) BOOL isSelected;
//是否选好分类
@property (nonatomic,assign) BOOL isCategroySelected;
//问题类型数组
@property (nonatomic,strong) NSMutableArray *questionTypeArr;
//悬赏金额
@property (nonatomic,copy) NSString *rewardPrice;
//分类ID
@property (nonatomic,copy) NSString *typeID;
@end

@implementation AskQuestionViewController

-(NSMutableArray *)questionTypeArr{
    if (!_questionTypeArr) {
        _questionTypeArr = [NSMutableArray array];
    }
    return _questionTypeArr;
}

-(NSMutableArray *)btnArr{
    if (!_btnArr) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}


-(void)viewWillAppear:(BOOL)animated{
    [self requestData];
    [self requestMoneyData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    
    if([self.FabuContent isEqualToString:@"自驾游"]){
        
          _askTextView.placeholder = @"你的标题(20字以内)";
        
        _suppleTextView.placeholder = @"正文详情";
        
        _askTextView.countL.text = @"0/20";

        
        self.title = @"内容";
        
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(YZDriverFabu)];
        
        self.navigationItem.rightBarButtonItem.title = @"发布";

    }
    
}

-(void)requestMoneyData{
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:LoveDriverURL(@"APP/Member/info") parameters:dic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            self.money = [responseObject[@"info"][@"silver"] integerValue];
        }
        NSLog(@"money:%ld",self.money);
        [self initRewardView];
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:@"获取用户银A币失败" toView:self.view];
    }];
}

-(void)YZDriverFabu{
    
//    发布自驾游
    
    NSDictionary *contentDic = @{@"uid":LoveDriverID,@"title":_askTextView.text,@"content":_suppleTextView.text,@"picture":@""};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travelAdd") parameters:contentDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [dic[@"code"] intValue];
        if (code ==200) {
            
            [MBProgressHUD showSuccess:dic[@"msg"] toView:self.view];
            
            [self performSelector:@selector(BackToViewController) withObject:nil afterDelay:1.5];
            
        }else{
            
            
            [MBProgressHUD showSuccess:dic[@"msg"] toView:self.view];
            
        }
        
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
}

-(void)requestData{
    [NetMethod Post:LoveDriverURL(@"APP/Problem/getType") parameters:nil success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    QuestionType *model = [[QuestionType alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.questionTypeArr addObject:model];
                }
            }
            [self initCategroyView];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)BackToViewController{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createUI{
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //假数据
    //self.money = 40;
    [self initNavigationBar];
    [self initTextView];
    [self initBottomView];
    //[self initRewardView];
    //[self initCategroyView];
    
}

//分类
-(void)initCategroyView{
    _categroyView = [[UIView alloc] init];
    _categroyView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:_categroyView];
    [_categroyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_bottom).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(340*KHeight);
    }];
    
    UILabel *categoryL = [[UILabel alloc] init];
    categoryL.text = @"选择分类:";
    categoryL.font = [UIFont systemFontOfSize:20*KHeight];
    categoryL.textColor = RGBColor(80, 80, 80);
    [_categroyView addSubview:categoryL];
    [categoryL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_categroyView.mas_left).offset(15*Kwidth);
        make.top.equalTo(_categroyView.mas_top).offset(30*KHeight);
    }];
    
    //NSArray *titleArr = @[@"健康",@"体育",@"文艺",@"自驾游",@"汽车养护",@"其他"];
    
    for (int i = 0; i < self.questionTypeArr.count; i ++) {
        int XX = i % 5;
        int YY = i / 5;
        float x = 15*Kwidth + (categoryWidth+20*Kwidth)*XX;
        float y = 70*KHeight+(30*KHeight+categoryWidth)*YY;
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, categoryWidth, categoryWidth)];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setBackgroundImage:[UIImage imageNamed:@"categroy"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"categroy1"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(clickCategroyBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 100+i;
        [_categroyView addSubview:btn];
        
        UILabel *titleL = [[UILabel alloc] initWithFrame:CGRectMake(x, y+categoryWidth, categoryWidth, 15*KHeight)];
        QuestionType *model = self.questionTypeArr[i];
        titleL.text = model.title;
        titleL.font = [UIFont systemFontOfSize:13*KHeight];
        titleL.textAlignment = NSTextAlignmentCenter;
        [_categroyView addSubview:titleL];
        
    }
    
}

-(void)initRewardView{
    _rewardView = [[UIView alloc] init];
    _rewardView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:_rewardView];
    [_rewardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(self.view.mas_bottom).offset(0);
        make.height.mas_equalTo(340*KHeight);
    }];
    
    UILabel *rewardL = [[UILabel alloc] init];
    rewardL.text = @"选择悬赏值:";
    rewardL.textColor = RGBColor(80, 80, 80);
    rewardL.font = [UIFont systemFontOfSize:20*KHeight];
    [_rewardView addSubview:rewardL];
    [rewardL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rewardView.mas_left).offset(20*Kwidth);
        make.top.equalTo(_rewardView.mas_top).offset(30*KHeight);
    }];
    NSArray *titleArr = @[@"0",@"5",@"10",@"20",@"30",@"40",@"50",@"60"];
    for (int i = 0; i < 8; i ++) {
        int XX = i % 4;
        int YY = i / 4;
        float x = 20*Kwidth + (rewardWidth+20*Kwidth)*XX;
        float y = 60*KHeight + 5*KHeight +(15*KHeight+rewardHeight)*YY;
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, rewardWidth, rewardHeight)];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 3.0*KHeight;
        btn.layer.borderWidth = 1.0*Kwidth;
        btn.layer.borderColor = [RGBColor(170, 170, 170) CGColor];
        //判断btn是否可点击
        if ([btn.titleLabel.text integerValue] > self.money ) {
            [btn setBackgroundColor:RGBColor(220, 220, 220)];
            btn.userInteractionEnabled = NO;
            [btn setTitleColor:RGBColor(170, 170, 170) forState:UIControlStateNormal];
        }else{
            [self.btnArr addObject:btn];
        }
        [btn addTarget:self action:@selector(clickRewardBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_rewardView addSubview:btn];
    }
    
    UILabel *thridL = [[UILabel alloc] init];
    thridL.text = @"提高悬赏更容易吸引高手为您解答";
    thridL.textColor = RGBColor(160, 160, 160);
    thridL.font = [UIFont systemFontOfSize:20*KHeight];
    [_rewardView addSubview:thridL];
    [thridL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_rewardView.mas_centerX);
        make.bottom.equalTo(_rewardView.mas_bottom).offset(-85*KHeight);
    }];
    
    UILabel *secondL = [[UILabel alloc] init];
    secondL.text = @"您可用的银A币：";
    secondL.textColor = RGBColor(80, 80, 80);
    secondL.font = [UIFont systemFontOfSize:20*KHeight];
    [_rewardView addSubview:secondL];
    [secondL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rewardView.mas_left).offset(95*Kwidth);
        make.bottom.equalTo(thridL.mas_top).offset(-5*KHeight);
    }];
    
    UILabel *priceL = [[UILabel alloc] init];
    priceL.text = [NSString stringWithFormat:@"%ld",self.money];
    priceL.textColor = RGBColor(64, 169, 92);
    priceL.font = [UIFont systemFontOfSize:20*KHeight];
    [_rewardView addSubview:priceL];
    [priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(secondL.mas_right).offset(0);
        make.bottom.equalTo(thridL.mas_top).offset(-5*KHeight);
    }];
    
}



-(void)initBottomView{
    _bottomView = [[UIView alloc] init];
    _bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(55*KHeight);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
    }];
    
    UIButton *rewardBtn = [[UIButton alloc] init];
    [rewardBtn setBackgroundImage:[UIImage imageNamed:@"reward"] forState:UIControlStateNormal];
    [rewardBtn addTarget:self action:@selector(clickReward) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:rewardBtn];
    [rewardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bottomView.mas_left).offset(5*KHeight);
        make.top.equalTo(_bottomView.mas_top).offset(5*KHeight);
        make.bottom.equalTo(_bottomView.mas_bottom).offset(-5*KHeight);
        make.width.mas_equalTo(KHeight*45);
    }];
    
    UIButton *cameraBtn = [[UIButton alloc] init];
    [cameraBtn setBackgroundImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [cameraBtn addTarget:self action:@selector(clickCategroy) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:cameraBtn];
    [cameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_bottomView.mas_right).offset(-5*KHeight);
        make.top.equalTo(_bottomView.mas_top).offset(5*KHeight);
        make.bottom.equalTo(_bottomView.mas_bottom).offset(-5*KHeight);
        make.width.mas_equalTo(KHeight*45);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomView.mas_left).offset(0);
        make.right.equalTo(_bottomView.mas_right).offset(0);
        make.top.equalTo(_bottomView.mas_top).offset(0);
        make.height.mas_equalTo(2*KHeight);
    }];
    
}



-(void)initTextView{
    _askTextView = [[CustomTextView alloc] initWithLimit:YES];
    _askTextView.delegate = self;
    _askTextView.placeholder = @"你的问题";
    _askTextView.tag = 50;
    _askTextView.returnKeyType = UIReturnKeyDone;
    _askTextView.font = [UIFont systemFontOfSize:20*KHeight];
    [self.view addSubview:_askTextView];
    [_askTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.height.mas_equalTo(180*KHeight);
    }];
    
    _suppleTextView = [[CustomTextView alloc] initWithLimit:NO];
    _suppleTextView.delegate = self;
    _suppleTextView.placeholder = @"补充说明(选填)";
    _suppleTextView.tag = 51;
    _suppleTextView.returnKeyType = UIReturnKeyDone;
    _suppleTextView.font = [UIFont systemFontOfSize:20*KHeight];
    [self.view addSubview:_suppleTextView];
    _suppleTextView.hidden = YES;
    
    [_suppleTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(_askTextView.mas_bottom).offset(1*KHeight);
        make.bottom.equalTo(self.view.mas_bottom).offset(-55*KHeight);
    }];
}


-(void)initNavigationBar{
    self.title = @"提问";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
    
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(0, 0, 50, 50);
    [rightBtn setTitle:@"提交" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItemBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItemBtn;
}

-(void)clickCategroyBtn:(UIButton *)sender{
    
    if (sender != _currentBtn) {
        _currentBtn.selected = NO;
        _currentBtn = sender;
        _currentBtn.selected = YES;
    }
    
    QuestionType *model = self.questionTypeArr[sender.tag-100];
    self.typeID = model.TypeID;
    NSLog(@"typeID:%@",self.typeID);
    
}

-(void)clickRewardBtn:(UIButton *)sender{
    for (UIButton *btn in self.btnArr) {
        [btn setBackgroundColor:[UIColor whiteColor]];
    }
    if (sender != _currentBtn) {
        _currentBtn.selected = NO;
        _currentBtn = sender;
    }
    _currentBtn.selected = YES;
    self.rewardPrice = _currentBtn.titleLabel.text;
    NSLog(@"rewardPrice:%@",self.rewardPrice);
    [_currentBtn setBackgroundColor:RGBColor(64, 169, 92)];
    
}

-(void)clickCategroy{
    if (!self.isCategroySelected) {
        [UIView animateWithDuration:0.2 animations:^{
            _categroyView.frame = CGRectMake(0, MainScreen.height-340*KHeight, MainScreen.width, 340*KHeight);
            _bottomView.frame = CGRectMake(0, MainScreen.height-393*KHeight, MainScreen.width, 55*KHeight);
        }];
        self.isCategroySelected = YES;
        self.isSelected = NO;
    }else{
        self.isCategroySelected = NO;
        [UIView animateWithDuration:0.2 animations:^{
            [_categroyView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left).offset(0);
                make.right.equalTo(self.view.mas_right).offset(0);
                make.top.equalTo(self.view.mas_bottom).offset(0);
                make.height.mas_equalTo(340*KHeight);
            }];
            _bottomView.frame = CGRectMake(0, MainScreen.height-55*KHeight, MainScreen.width, 55*KHeight);
        }];
    }
    
}

-(void)clickReward{
    if (!self.isSelected) {
        [UIView animateWithDuration:0.2 animations:^{
            _rewardView.frame = CGRectMake(0, MainScreen.height-340*KHeight, MainScreen.width, 340*KHeight);
            _bottomView.frame = CGRectMake(0, MainScreen.height-393*KHeight, MainScreen.width, 55*KHeight);
        }];
        self.isSelected = YES;
    }else{
        self.isSelected = NO;
        [UIView animateWithDuration:0.2 animations:^{
            [_rewardView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view.mas_left).offset(0);
                make.right.equalTo(self.view.mas_right).offset(0);
                make.top.equalTo(self.view.mas_bottom).offset(0);
                make.height.mas_equalTo(340*KHeight);
            }];
            _bottomView.frame = CGRectMake(0, MainScreen.height-55*KHeight, MainScreen.width, 55*KHeight);
        }];
    }
    
}

//点击提交
-(void)clickSubmit{
    if (_askTextView.text.length <= 0) {
        [MBProgressHUD showSuccess:@"请输入您的问题" toView:self.view];
        return;
    }else{
        if (self.rewardPrice.length == 0) {
            [MBProgressHUD showSuccess:@"请选择悬赏金额" toView:self.view];
        }else if (self.typeID.length == 0){
            [MBProgressHUD showSuccess:@"请选择问题类型" toView:self.view];
        }else{
            NSDictionary *dic = @{@"title":self.askTextView.text,@"type_id":self.typeID,@"uid":LoveDriverID,@"price":self.rewardPrice};
            [NetMethod Post:LoveDriverURL(@"APP/Problem/add") parameters:dic success:^(id responseObject) {
                
                [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                if ([responseObject[@"code"] isEqual:@200]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
                
            } failure:^(NSError *error) {
                [MBProgressHUD showSuccess:NetProblem toView:self.view];
            }];
        }
    }
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [_askTextView resignFirstResponder];
        [_suppleTextView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    if (textView.tag == 50) {
        _askTextView.placeholderL.hidden = YES;
    }else{
        _suppleTextView.placeholderL.hidden = YES;
    }
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
    
    
    if([self.FabuContent isEqualToString:@"自驾游"]){
       
        _askTextView.countL.text = [NSString stringWithFormat:@"%ld/20",_askTextView.text.length];
        if (_askTextView.text.length > 20) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字符个数不能大于20" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            textView.text = [textView.text substringToIndex:20];
            _askTextView.countL.text = @"20/20";
        }

    
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.tag == 50) {
        if (textView.text.length == 0) {
            _askTextView.placeholderL.hidden = NO;
        }
    }else{
        if (textView.text.length == 0) {
            _suppleTextView.placeholderL.hidden = NO;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
