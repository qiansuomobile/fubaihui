//
//  YzBindingAccountViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/9.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YzBindingAccountViewController : UIViewController
@property (nonatomic, copy)NSString *payAccount;
@end

NS_ASSUME_NONNULL_END
