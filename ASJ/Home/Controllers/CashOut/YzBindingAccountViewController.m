//
//  YzBindingAccountViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/9.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzBindingAccountViewController.h"

@interface YzBindingAccountViewController ()

@property (weak, nonatomic) IBOutlet UITextField *payAccountTextField;

@end

@implementation YzBindingAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"绑定支付宝账号";
    _payAccountTextField.text = _payAccount;
}

// 立即绑定
- (IBAction)nowCashOutClick:(UIButton *)sender {
    [self bindingPayID];
}


- (void)bindingPayID{
    if (_payAccountTextField.text.length == 0) {
        [MBProgressHUD showError:@"请输入支付宝账号" toView:self.view];
        return;
    }
    NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                @"openid":_payAccountTextField.text,
                                @"type":@"2"
                                };
    [NetMethod Post:FBHRequestUrl(kUrl_binding_pay) parameters:parameDic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:@"绑定账号成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
