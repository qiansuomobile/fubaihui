//
//  YzCashOutRMBViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/19.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzCashOutRMBViewController.h"
#import "YzBindingAccountViewController.h"
@interface YzCashOutRMBViewController ()

@property (nonatomic, copy)NSString *alipayAccount;
@property (weak, nonatomic) IBOutlet UILabel *aliAccountLabel;
@property (weak, nonatomic) IBOutlet UITextField *cashoutTextField;
@property (weak, nonatomic) IBOutlet UILabel *cashoutLabel;

@end

@implementation YzCashOutRMBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self isBindingPayAccount];
    [self getGoldRechargeDataRequest];
}
#pragma mark - action
- (IBAction)nowCashOut:(UIButton *)sender {
    if (_alipayAccount.length == 0) {
        YzBindingAccountViewController *baViewController = [[YzBindingAccountViewController alloc]init];
        baViewController.payAccount = _alipayAccount;
        [self.navigationController pushViewController:baViewController animated:YES];
        return;
    }
    
    [self nowCashOutRequset];
}

- (IBAction)changeAlipayAccount:(UIButton *)sender {
    YzBindingAccountViewController *baViewController = [[YzBindingAccountViewController alloc]init];
    baViewController.payAccount = _alipayAccount;
    [self.navigationController pushViewController:baViewController animated:YES];
}

#pragma mark - URL Requset

//提现
- (void)nowCashOutRequset{
    if (_cashoutTextField.text.length == 0) {
        [MBProgressHUD showError:@"请输入提现金额" toView:self.view];
        return;
    }
    
    NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                @"money":_cashoutTextField.text,
                                @"type":@"2"
                                };
    [NetMethod Post:FBHRequestUrl(kUrl_pick_otherpay) parameters:parameDic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"提现%@元成功",_cashoutTextField.text]];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}



//是否已经绑定支付宝账号
- (void)isBindingPayAccount{
    
    NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                @"type":@"2"
                                };
    [NetMethod Post:FBHRequestUrl(kUrl_isbangding) parameters:parameDic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            _alipayAccount = responseObject[@"data"];
            _aliAccountLabel.text = [NSString stringWithFormat:@"支付宝 (%@)",_alipayAccount];
        }else{
            NSLog(@"未绑定提现账号");
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

// 获取用户余额信息
- (void)getGoldRechargeDataRequest{
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *data = resDic[@"data"];
            //用户金积分
            _cashoutLabel.text  = [NSString stringWithFormat:@"可提现金额：%@",data[@"balance"]];
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
