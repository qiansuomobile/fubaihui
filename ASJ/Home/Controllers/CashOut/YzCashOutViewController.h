//
//  YzCashOutViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/8.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YzCashOutViewController : UIViewController

// 提现类型  1是提现用户金积分 2是提现商家金积分
@property (nonatomic, assign)NSInteger cashOutType;

@end

NS_ASSUME_NONNULL_END
