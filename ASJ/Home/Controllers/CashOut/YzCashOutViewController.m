//
//  YzCashOutViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/8.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzCashOutViewController.h"
#import "YzBindingAccountViewController.h"

@interface YzCashOutViewController ()
@property (weak, nonatomic) IBOutlet UITextField *balanceTextField;

//可转入余额
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@end

@implementation YzCashOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"提现";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getGoldRechargeDataRequest];
}

#pragma mark - Action

// 转入到余额
- (IBAction)transferToBalance:(UIButton *)sender {
    [self transferToBalanceRequest];
}


#pragma mark - URL Request
//转入到余额
- (void)transferToBalanceRequest{
    
    NSString *type;
    if (_cashOutType == 0) {
        type = @"1";
    }else{
        type = @"2";
    }
    NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                @"balance":_balanceTextField.text,
                                @"type":type
                                };
    [NetMethod Post:FBHRequestUrl(kUrl_pick_balace) parameters:parameDic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"提现金积分%@成功",_balanceTextField.text]];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

// 获取用户余额信息
- (void)getGoldRechargeDataRequest{
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *data = resDic[@"data"];
            //用户金积分
            if (_cashOutType == 0) {
              _balanceLabel.text  = [NSString stringWithFormat:@"用户可提现金额：%@",data[@"score"]];
            }else{
               _balanceLabel.text  = [NSString stringWithFormat:@"商户可提现金额：%@",data[@"store_score"]];
            }
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
