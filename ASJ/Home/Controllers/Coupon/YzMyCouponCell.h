//
//  YzMyCouponCell.h
//  ASJ
//
//  Created by Dororo on 2019/7/18.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YzMyCouponCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *couponLabel;
@property (weak, nonatomic) IBOutlet UILabel *sliverLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
