//
//  DriverAnswerViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DriverAnswerViewController.h"
#import "AnswerFirstTableViewCell.h"
#import "AskQuestionViewController.h"
#import "MyQuestionViewController.h"
#import "MyQuestionDetailViewController.h"
#import "QuestionModel.h"
//弹窗视图
#import "PellTableViewSelect.h"
#import "QuestionType.h"

#define BtnWidth (self.view.frame.size.width-160*Kwidth)/3

@interface DriverAnswerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *greenView;
// 1.推荐 2.最新 3.高悬赏
@property (nonatomic,assign) NSInteger dataType;
@property (nonatomic,strong) NSMutableArray *dataArr;
//筛选类型
@property (nonatomic,strong) NSMutableArray *typeArr;
@property (nonatomic,assign) NSInteger typeID;
@end

@implementation DriverAnswerViewController

-(NSMutableArray *)typeArr{
    if (!_typeArr) {
        _typeArr = [NSMutableArray array];
    }
    return _typeArr;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    //默认传1
    self.dataType = 1;
    [self requestData];
}

-(void)requestData{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    switch (self.dataType) {
        case 1:{
            [dic removeAllObjects];
            [dic setValue:@"1" forKey:@"is_sticky"];
        }
            break;
        case 2:{
            [dic removeAllObjects];
        }
            break;
        case 3:{
            [dic removeAllObjects];
            [dic setValue:@"1" forKey:@"gxs"];
        }
        default:
            break;
    }
    
    [dic setObject:[NSString stringWithFormat:@"%ld",self.typeID] forKey:@"type_id"];
    
    [NetMethod Post:LoveDriverURL(@"APP/Problem/index") parameters:dic success:^(id responseObject) {
        if ([responseObject[@"code"] isEqual:@200]) {
            [self.dataArr removeAllObjects];
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    QuestionModel *model = [[QuestionModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    model.create_time = [VerifyPictureURL ctime:model.create_time];
                    model.phone = [VerifyPictureURL FormatPhoneNumber:model.phone];
                    [self.dataArr addObject:model];
                }
                [self.tableView reloadData];
            }else{
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)requestTypeData{
    
    [self.typeArr removeAllObjects];
    [NetMethod Post:LoveDriverURL(@"APP/Problem/getType") parameters:nil success:^(id responseObject) {
        
        NSMutableArray *titleArr = [NSMutableArray array];
        
        if ([responseObject[@"code"] isEqual:@200]) {
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    QuestionType *model = [[QuestionType alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.typeArr addObject:model];
                    [titleArr addObject:model.title];
                }
                
                
                
                // 弹出QQ的自定义视图
                [PellTableViewSelect addPellTableViewSelectWithWindowFrame:CGRectMake(self.view.bounds.size.width +50, 165*KHeight, 130, 200) selectData:titleArr images:nil action:^(NSInteger index) {
                    
                    QuestionType *model = self.typeArr[index];
                    self.typeID = [model.TypeID integerValue] ;
                    [self requestData];
                    
                    
                    NSLog(@"选择%ld",index);
                } animated:YES];
                
            }
            
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];

}


-(void)createUI{
    [self initNavigationBar];
    [self initTableView];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(void)initNavigationBar{
    self.title = @"问答";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

//点击推荐/最新/高悬赏
-(void)clickCatecoryBtn:(UIButton *)sender{
    [UIView animateWithDuration:0.2 animations:^{
        _greenView.frame = CGRectMake(sender.frame.origin.x+3*Kwidth, _greenView.frame.origin.y, _greenView.frame.size.width, _greenView.frame.size.height);
    }];
    if (sender.tag == 200) {
        self.dataType = 1;
        [self requestData];
    }else if (sender.tag == 201){
        self.dataType = 2;
        [self requestData];
    }else{
        self.dataType = 3;
        [self requestData];
    }
}

//点击提问/我的回答/我的提问
-(void)clickHeadBtn:(UIButton *)sender{
    switch (sender.tag) {
        case 100:{
            AskQuestionViewController *askQuVC = [[AskQuestionViewController alloc] init];
            [self.navigationController pushViewController:askQuVC animated:YES];
        }
            break;
        case 101:{
            MyQuestionViewController *myQuestionVC = [[MyQuestionViewController alloc] init];
            myQuestionVC.isAnswer = YES;
            [self.navigationController pushViewController:myQuestionVC animated:YES];
        }
            break;
        case 102:{
            MyQuestionViewController *myQuestionVC = [[MyQuestionViewController alloc] init];
            [self.navigationController pushViewController:myQuestionVC animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)clickAnwser:(UIButton *)sender{
    QuestionModel *model = self.dataArr[sender.tag-1];
    MyQuestionDetailViewController *detailVC = [[MyQuestionDetailViewController alloc] init];
    detailVC.questionID = model.question_id;
    detailVC.isAnwser = YES;
    detailVC.nickName = model.nickname;
    [self.navigationController pushViewController:detailVC animated:YES];
}

//点击分类按钮
-(void)clickCategory{
    
    [self requestTypeData];
    
}


#pragma mark --- UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AnswerFirstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[AnswerFirstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.tag = 500+indexPath.row;
    cell.answerBtn.tag = indexPath.row+1;
  
    if (self.dataArr.count > 0) {
    
        QuestionModel *model = self.dataArr[indexPath.row];
        
        cell.questionModel = model;
    
    }
    
    [cell.answerBtn addTarget:self action:@selector(clickAnwser:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = RGBColor(230, 230, 230);
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = Red;
    [headerView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(0);
        make.top.equalTo(headerView.mas_top).offset(0);
        make.right.equalTo(headerView.mas_right).offset(0);
        make.height.mas_equalTo(150*KHeight);
    }];
    //3个按键
    NSArray *labelArr = @[@"提问",@"我的回答",@"我的提问"];
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = [[UIButton alloc] init];
        [btn setBackgroundImage:[UIImage imageNamed:labelArr[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickHeadBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 100+i;
        [bgView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView.mas_left).offset(40*Kwidth+i*(BtnWidth+40*Kwidth));
            make.top.equalTo(bgView.mas_top).offset(25*KHeight);
            make.height.mas_equalTo(BtnWidth);
            make.width.mas_equalTo(BtnWidth);
            
        }];
        
        UILabel *titleL = [[UILabel alloc] init];
        titleL.text = labelArr[i];
        titleL.textAlignment = NSTextAlignmentCenter;
        titleL.font = [UIFont systemFontOfSize:17*KHeight];
        titleL.textColor = [UIColor whiteColor];
        [bgView addSubview:titleL];
        [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView.mas_left).offset(40*Kwidth+i*(BtnWidth+40*Kwidth));
            make.top.equalTo(btn.mas_bottom).offset(5*KHeight);
            make.height.mas_equalTo(20*KHeight);
            make.width.mas_equalTo(BtnWidth);
        }];
    }
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = RGBColor(200, 200, 200);
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(0);
        make.right.equalTo(headerView.mas_right).offset(0);
        make.top.equalTo(bgView.mas_bottom).offset(5*KHeight);
        make.height.mas_equalTo(1*KHeight);
    }];
    
    NSArray *btnTitleArr = @[@"推荐",@"最新",@"高悬赏"];
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:btnTitleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickCatecoryBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 200+i;
        [headerView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(headerView.mas_left).offset(i*65*Kwidth);
            make.top.equalTo(lineView.mas_bottom).offset(0);
            make.bottom.equalTo(headerView.mas_bottom).offset(0);
            make.width.mas_equalTo(65*Kwidth);
        }];
    }
    
    UIButton *categoryBtn = [[UIButton alloc] init];
    [categoryBtn setTitle:@"分类" forState:UIControlStateNormal];
    [categoryBtn setTitleColor:Red forState:UIControlStateNormal];
    [categoryBtn addTarget:self action:@selector(clickCategory) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:categoryBtn];
    [categoryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerView.mas_right).offset(-10*Kwidth);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.bottom.equalTo(headerView.mas_bottom).offset(0);
        make.width.mas_equalTo(65*Kwidth);
    }];
    
    _greenView = [[UIView alloc] init];
    _greenView.backgroundColor = RGBColor(134, 228, 155);
    [headerView addSubview:_greenView];
    [_greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(5*Kwidth);
        make.bottom.equalTo(headerView.mas_bottom).offset(0);
        make.height.mas_equalTo(2*KHeight);
        make.width.mas_equalTo(70*Kwidth);
    }];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //需要动态计算高度
    QuestionModel *model = self.dataArr[indexPath.row];
    return [model CaculateHeight:model.title];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 190*KHeight;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (self.dataArr.count > 0) {
//        
//        QuestionModel *model = self.dataArr[indexPath.row];
//        
//        cell.questionModel = model;
//        
//    }
    
    
    QuestionModel *model = self.dataArr[indexPath.row];
    MyQuestionDetailViewController *detailVC = [[MyQuestionDetailViewController alloc] init];
    detailVC.questionID = model.question_id;
    detailVC.isAnwser = YES;
    detailVC.nickName = model.nickname;
    [self.navigationController pushViewController:detailVC animated:YES];

    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
