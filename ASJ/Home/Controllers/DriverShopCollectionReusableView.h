//
//  DriverShopCollectionReusableView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverShopCollectionReusableView : UICollectionReusableView
-(void)loadImageWithImageName:(NSString *)imageName;
@end
