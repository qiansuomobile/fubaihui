//
//  DriverShopCollectionReusableView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DriverShopCollectionReusableView.h"

@implementation DriverShopCollectionReusableView
-(void)loadImageWithImageName:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5*Kwidth);
        make.right.equalTo(self.mas_right).offset(-5*Kwidth);
        make.top.equalTo(self.mas_top).offset(0);
        make.height.mas_equalTo(100*KHeight);
        }];
    UILabel *label = [[UILabel alloc] init];
    label.text = @"为您推荐";
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentLeft;
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5*Kwidth);
        make.top.equalTo(imageView.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    
}
@end
