//
//  DriverShopViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverShopViewController : UIViewController
//用来区分商城类型 0.福百惠商城  1.银积分 //yu4店铺
@property (nonatomic,assign) NSInteger ShopType;
@property (nonatomic,retain) NSString *sid;//yu商户id
@end
