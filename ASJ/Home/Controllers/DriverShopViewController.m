//
//  DriverShopViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DriverShopViewController.h"
#import "DriverShopTableViewCell.h"
#import "DriverShopCollectionViewCell.h"
#import "DriverShopCollectionReusableView.h"
#import "GoodsDetailViewController.h"
#import "CategroyGoods.h"
#import "SortGoods.h"
#import "SearchResultViewController.h"
#import "CommentModel.h"

#import "YzMyCouponCell.h"

@interface DriverShopViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UISearchBarDelegate>
@property (nonatomic,strong) UICollectionView *collectionView;
//@property (nonatomic,strong) UISearchBar *searchBar;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *btnMutableArr;//存放btn
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableArray *titleArr;
@property (nonatomic,strong) NSMutableArray *cateIDArr;
//记录当前点击的分类ID
@property (nonatomic,strong) NSString *cate_id;
//是否点击过左侧按键
@property (nonatomic,assign) BOOL isClick;
//当前选中的按钮
@property (nonatomic,strong) UIButton *currentBtn;

@property (nonatomic,assign) NSInteger firstPage;

@property (nonatomic,assign) BOOL NoMoreData;

//是否加载默认页
@property (nonatomic,assign) BOOL isDefault;

//左侧按键的编号
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) NSMutableArray *commentArr;

// 判读是否是优惠券列表
@property (nonatomic)BOOL isCoupon;

@end

@implementation DriverShopViewController

static NSString *collectionID = @"goosCollectionCell";
static NSString *collectionHeaderID = @"collectionHeader";

-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

-(NSMutableArray *)cateIDArr{
    if (!_cateIDArr) {
        _cateIDArr = [NSMutableArray array];
    }
    return _cateIDArr;
}

-(NSMutableArray *)titleArr{
    if (!_titleArr) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

-(NSMutableArray *)btnMutableArr{
    if (!_btnMutableArr) {
        _btnMutableArr = [NSMutableArray array];
    }
    return _btnMutableArr;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isDefault = YES;
    [self createUI];
    [self requestData];
}

#pragma mark - URL Request
//请求右侧collectionView默认显示数据
-(void)requestData{
    
    NSMutableDictionary *categoryDic = [NSMutableDictionary dictionary];
    if (_ShopType == 4) {
        // 商家店铺
        [categoryDic setObject:_sid forKey:@"sid"];
        [categoryDic setObject:@"0" forKey:@"shopcate"];
        NSLog(@"商家店铺");
    }
    if (_ShopType == 1) {
        // 商家店铺
        [categoryDic setObject:_sid forKey:@"sid"];
        [categoryDic setObject:@"1" forKey:@"shopcate"];
        NSLog(@"银积分店铺店铺");
    }
    //左侧列表
    [NetMethod GET:FBHRequestUrl(kUrl_goods_category) parameters:categoryDic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            [self.titleArr removeAllObjects];
            NSArray *listArr = responseObject[@"data"];
            if (listArr) {
                
                for (NSDictionary *dic in listArr) {
                    CategroyGoods *categoryGood = [[CategroyGoods alloc] init];
                    [categoryGood setValuesForKeysWithDictionary:dic];
                    [self.titleArr addObject:categoryGood.title];
                    [self.cateIDArr addObject:categoryGood.goodID];
                }
                
                // 福百惠商城不显示优惠券
                if (self.ShopType != 0) {
                    [self.titleArr addObject:@"优惠券"];
                    [self.cateIDArr addObject:@"-1"];
                }
                
                [self.tableView reloadData];

                
                [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                [self tableView:_tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
            }
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:@"请检查网络连接" toView:self.view];
    }];
}


/**
 根据商品分类显示商品列表

 @param category 商品分类
 */
- (void)showGoodsListWithCategoryIndex:(NSInteger)index{
    
    if (self.cateIDArr.count == 0) {
        NSLog(@"无分类");
        return;
    }
    NSString *categoryID = self.cateIDArr[index];
    if (!categoryID) {
        [MBProgressHUD showError:@"没有该分类商品" toView:self.view];
        return;
    }
    
    if (index == (self.cateIDArr.count - 1) && self.ShopType != 0) {
        _isCoupon = YES;
        [self getYouhuiWithMallID];
        return;
    }
    
    _isCoupon = NO;
    
    NSMutableDictionary *goodsListDic = [NSMutableDictionary dictionary];
    [goodsListDic setObject:categoryID forKey:@"cid"];
    [goodsListDic setObject:[NSNumber numberWithInteger:self.firstPage] forKey:@"page"];
    [goodsListDic setObject:@"15" forKey:@"row_num"];
    if (_ShopType == 4) {
        //商家店铺
        [goodsListDic setObject:_sid forKey:@"store_id"];
    }
    
    if (_ShopType == 1) {
        //银积分商城
        [goodsListDic setObject:_sid forKey:@"store_id"];
        [goodsListDic setObject:@"1" forKey:@"type"];
    }
    
    [NetMethod GET:FBHRequestUrl(kUrl_goods_list) parameters:goodsListDic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            //NSLog(@"res===%@",responseObject);
            if (![responseObject[@"data"] isKindOfClass:[NSNull class]]) {
//                [self.dataArr removeAllObjects];
                for (NSDictionary *dic in responseObject[@"data"]) {
                    SortGoods *sortModel = [[SortGoods alloc] init];
                    [sortModel setValuesForKeysWithDictionary:dic];
                    
                    [self.dataArr addObject:sortModel];
                }
                [self.collectionView reloadData];
            }else{
                self.NoMoreData = YES;
//                [_collectionView.mj_footer endRefreshingWithNoMoreData];
                [self.collectionView reloadData];
                [MBProgressHUD showSuccess:@"都被你看光了亲~" toView:self.view];
            }
        }else{
            [MBProgressHUD showSuccess:@"请求出错"  toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:@"请检查网络连接" toView:self.view];
    }];
}

-(void)createUI{
    switch (_ShopType) {
        case 0:{
            self.title = @"福百惠商城";
        }
            break;
        default:
            self.title = @"商家店铺名称";//yu
            break;
    }
    //searchBar
//    [self setSearchBar];
    //tableView
    [self setTableView];
    //CollectionView
    [self setCollectionView];
}

-(void)setCollectionView{
    CGFloat magin = 4.5*Kwidth;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((MainScreen.width-Kwidth*80-15*Kwidth)/2, 200*KHeight);
    layout.sectionInset = UIEdgeInsetsMake(magin, magin, 0, magin);
    layout.minimumInteritemSpacing = magin;
    layout.minimumLineSpacing = magin;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    //下拉刷新//yu
    _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [_collectionView.mj_header endRefreshing];
    }];
    //上拉加载
    _collectionView.mj_footer = [MJRefreshBackFooter footerWithRefreshingBlock:^{
        if (!self.NoMoreData) {
            self.firstPage ++;
            if (self.isDefault) {
                //加载更多默认页数据
                [self requestData];
            }else{
                NSLog(@"有更多数据");
                [self showGoodsListWithCategoryIndex:_index];
            }
        }
        [_collectionView.mj_footer endRefreshing];
    }];
    _collectionView.backgroundColor = [UIColor colorWithRed:223/255.0f green:223/255.0f blue:223/255.0f alpha:1.0];
    //注册头部视图
    [_collectionView registerClass:[DriverShopCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeaderID];
    //注册cell
    [_collectionView registerClass:[DriverShopCollectionViewCell class] forCellWithReuseIdentifier:collectionID];
    [_collectionView registerNib:[UINib nibWithNibName:@"YzMyCouponCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"mallCouponCell"];
    
    [self.view addSubview:_collectionView];
    
    
    NSInteger bottomOffset = self.ShopType == 0 ? 0 : -64;
    
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tableView.mas_right).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(bottomOffset);
    }];
    
}

-(void)setTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource= self;
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.width.mas_equalTo(80*Kwidth);
    }];
}

//-(void)setSearchBar{
//    _searchBar = [[UISearchBar alloc] init];
//    _searchBar.delegate = self;
//    _searchBar.placeholder = @"寻找你喜欢的宝贝";
//    _searchBar.barTintColor = [UIColor whiteColor];
//    UITextField *searchField=[_searchBar valueForKey:@"_searchField"];
//    searchField.backgroundColor = [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.0f alpha:1.0];
//    [self.view addSubview:_searchBar];
//    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left).offset(0);
//        make.top.equalTo(self.view.mas_top).offset(0);
//        make.right.equalTo(self.view.mas_right).offset(0);
//        make.height.mas_equalTo(50*KHeight);
//    }];
//}

////点击左侧按钮
//-(void)btnClick:(UIButton *)sender{
//    sender.selected = YES;
//    //标记已经点击过左侧按钮
//    self.isClick = YES;
//    for (UIButton *btn in self.btnMutableArr) {
//        if (btn.tag != sender.tag) {
//            btn.selected = NO;
//        }
//    }
//
//    //清空数据数组
//    [self.dataArr removeAllObjects];
//    self.NoMoreData = NO;
//    self.firstPage = 1;
//    self.isDefault = NO;
//
//    _index = sender.tag-100;
////    [self requestSecondDataWithIndex:_index];
//    [self showGoodsListWithCategoryIndex:_index];
//}


#pragma mark -- UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isCoupon) {
        // 优惠券
        YzMyCouponCell *couponCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mallCouponCell" forIndexPath:indexPath];
        NSDictionary *dic = self.dataArr[indexPath.row];
        couponCell.couponLabel.text = [NSString stringWithFormat:@"满%@减%@",dic[@"money_condition"], dic[@"money"]];
        couponCell.sliverLabel.text = [NSString stringWithFormat:@"兑换此券%@银积分",dic[@"silver"]];
        couponCell.nameLabel.text = dic[@"name"];
        return couponCell;
    }
    
    //商品
    DriverShopCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionID forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    SortGoods *sortModel = self.dataArr[indexPath.row];
    cell.sortGood = sortModel;
    if (self.ShopType == 1) {
        cell.priceL.text = [NSString stringWithFormat:@"银积分:%.2f",[sortModel.f_silver floatValue]];
    }else{
        cell.priceL.text = [NSString stringWithFormat:@"¥%.2f",[sortModel.price floatValue]];
    }
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = CGSizeMake(MainScreen.width, KHeight*120);
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isCoupon) {
        return CGSizeMake((MainScreen.width-Kwidth*80-15*Kwidth)/2, 80);
    }
    CGSize sise = CGSizeMake((MainScreen.width-Kwidth*80-15*Kwidth)/2, 190*KHeight);

    return sise;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if (kind == UICollectionElementKindSectionHeader) {
        DriverShopCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeaderID forIndexPath:indexPath];
        //解决重用问题
        for (UIView *view in header.subviews) {
            [view removeFromSuperview];
        }
        [header loadImageWithImageName:@"tuijian"];// 临时数据
        
        return header;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isCoupon) {
        //获取优惠券
        NSLog(@"兑换优惠券");
        NSDictionary *dic = self.dataArr[indexPath.row];
        [self exchangeCouponWithCouponID:dic[@"id"]];
        return;
    }
    //判断是否获取到数据
    if (self.dataArr.count > 0) {
        
        SortGoods *model = self.dataArr[indexPath.row];
       
        [self requestDetailDataWith:model.goodID];
        
    }else{
        [MBProgressHUD showSuccess:@"未获取到数据,请检查网络连接设置" toView:self.view];
    }
    
    
}
//获取商品详情页面数据
-(void)requestDetailDataWith:(NSString *)goodID{
    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
    goodDetailVC.goodsID = goodID;
    goodDetailVC.shopType = self.ShopType;
    goodDetailVC.title = self.title;
    [self.navigationController pushViewController:goodDetailVC animated:YES];
    
    if (self.ShopType != 0) {
        NSNotification *notification =[NSNotification notificationWithName:@"kNotification_show_goods_detail"
                                                                    object:nil
                                                                  userInfo:@{@"goodsID":goodID}];
        //通过通知中心发送通知
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    }
    
    return;
}

#pragma mark --- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Select Row%ld",indexPath.row);
    DriverShopTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellSelect = YES;
    
    //清空数据数组
    [self.dataArr removeAllObjects];
    self.NoMoreData = NO;
    self.firstPage = 1;
    self.isDefault = NO;
    _index = indexPath.row;
    [self showGoodsListWithCategoryIndex:_index];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Deselect Row%ld",indexPath.row);
    DriverShopTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellSelect = NO;
}

#pragma mark - tableview data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DriverShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[DriverShopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.titleStr = self.titleArr[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55*KHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


#pragma mark - URL Request

/**
 获取商家优惠券列表
 */
- (void)getYouhuiWithMallID{
    
    NSDictionary *dic = [NSMutableDictionary dictionary];
    if (_ShopType == 1 || _ShopType == 4) {
        [dic setValue:_sid forKey:@"store_id"];
    }
    [NetMethod GET:FBHRequestUrl(kUrl_mall_coupon) parameters:dic success:^(id responseObject) {
        NSDictionary *resultDic = responseObject;
        [self.dataArr removeAllObjects];
        if ([resultDic[@"code"] intValue] == 200) {
            if (![resultDic[@"data"] isKindOfClass:[NSNull class]]) {
                NSArray *dataArr = resultDic[@"data"];
                for (NSDictionary *dic in dataArr) {
                    [self.dataArr addObject:dic];
                }
            }
            [self.collectionView reloadData];
        }else{
            [MBProgressHUD showError:resultDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


//兑换优惠券
- (void)exchangeCouponWithCouponID:(NSString *)couponid{
    
    NSDictionary *parameterDic = @{@"uid":LoveDriverID,
                                   @"token":FBH_USER_TOKEN,
                                   @"id":couponid
                                   };
    
    [NetMethod Post:FBHRequestUrl(kUrl_exchange_coupon) parameters:parameterDic success:^(id responseObject) {
        NSDictionary *resultDic = responseObject;
        if ([resultDic[@"code"] intValue] == 200) {
            [MBProgressHUD showSuccess:resultDic[@"msg"] toView:self.view];
        }else{
            [MBProgressHUD showError:resultDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


//#pragma mark --- UISearchBarDelegate
//
//-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//   
//    [self.searchBar resignFirstResponder];
//    
//    SearchResultViewController *searchResultVC = [[SearchResultViewController alloc] init];
//    
//    UITextField *searchField=[searchBar valueForKey:@"_searchField"];
//   
//    if (searchField.text.length > 0) {
//    
//        searchResultVC.keyWord = searchField.text;
//        
//        searchResultVC.shopType = self.ShopType;
//        
//        [self.navigationController pushViewController:searchResultVC animated:YES];
//    
//    }else{
//        [MBProgressHUD showSuccess:@"请输入您要搜索的宝贝" toView:self.view];
//    }
//    
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
