//
//  GoodsDetailViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortGoods.h"
@interface GoodsDetailViewController : UIViewController
@property (nonatomic,strong) SortGoods *sortGoods;
//判断是否是兑换商城商品
@property (nonatomic,assign) BOOL isConvert;
@property (nonatomic,strong) NSString *commentCount;
@property (nonatomic,strong) NSMutableArray *commentArr;
//用来区分商城类型 0.正品  1.银积分 4:商家店铺
@property (nonatomic,assign) NSInteger shopType;

@property (nonatomic,strong) NSString *godenType;

//商品 ID
@property (nonatomic, copy)NSString *goodsID;

@end
