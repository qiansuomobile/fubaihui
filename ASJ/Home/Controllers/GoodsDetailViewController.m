//
//  GoodsDetailViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "GoodsDetailViewController.h"
#import "GoodsDetailTableViewCell.h"
#import "GoodsDetailDescriptionCell.h"
#import "bottomView.h"
#import "YzCollectionViewController.h"
#import "ShoppingCarViewController.h"
#import "PAStepper.h"
#import "AllCommentViewController.h"
#import <WebKit/WebKit.h>
#import "GoodsDetailWebCell.h"

#import "YzCommitListViewController.h"

#import "FBHGoods.h"

@interface GoodsDetailViewController ()<UIWebViewDelegate, UITableViewDelegate,UITableViewDataSource,WKUIDelegate, WKNavigationDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *headerView;
@property (nonatomic,strong) UIPageControl *pageControl;
@property (nonatomic,strong) UIScrollView *scrollview;
@property (nonatomic,strong) bottomView *bottView;
@property (nonatomic,strong) GoodsDetailTableViewCell *countCell;
@property (nonatomic,strong) PAStepper *stepper;
@property (nonatomic,weak) UIView *NavBarView;
@property(nonatomic,assign) CGFloat webViewHeight;

//商品数据模型
@property(nonatomic, strong)FBHGoods *goods;
@end

@implementation GoodsDetailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed=YES;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
//    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
//    self.title = @"福百惠商城";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    

    _webViewHeight = 0;
    [self createUI];
    [self requestGoodsDetailsData];
}

-(void)createUI{
    
    [self initHeaderView];
    
    [self initTableView];
    
    
    [self addNavBarView];
    
    [self initBottomView];
}

-(void)addNavBarView{
    UIView* view = [[UIView alloc] init];
    self.NavBarView = view;
    view.frame = CGRectMake(0, 0, MainScreen.width, 64.0);
    [self.view addSubview:view];
}



-(void) initTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = _headerView;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-SafeAreaBottom - 55);
    }];
    
}

-(void)initBottomView{
    _bottView = [[bottomView alloc] initWithFrame:CGRectZero];
    _bottView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bottView];
    [_bottView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-SafeAreaBottom);
        make.height.mas_equalTo(55);
    }];
    for (UIButton *btn in _bottView.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            [btn addTarget:self action:@selector(clickBottomBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

-(void)clickBottomBtn:(UIButton *)btn{
    switch (btn.tag) {
        case 100:{
            NSLog(@"客服");
        }
            break;
        case 101:{
            [self requestCollection];
        }
            break;
        case 102:{
            //添加购物车
            [self addShoppingCar];
        }
            break;
        case 103:{
            NSLog(@"立即购买");
            ShoppingCarViewController *shopCarVC = [[ShoppingCarViewController alloc] init];
            shopCarVC.twoPage = YES;
            if (self.shopType == 1) {
                shopCarVC.type = ShoppingCartTypeSilver;
            }else if(self.shopType == 0){
                shopCarVC.type = ShoppingCartTypeFBH;
            }else{
                shopCarVC.type = ShoppingCartTypeStore;
            }
            [self.navigationController pushViewController:shopCarVC animated:YES];
        }
            break;
        default:
            break;
    }
}

//收藏请求
-(void)requestCollection{
    
    if (!DriverISLogIN) {
        //用户没登录
        LoginViewController *vc = [[LoginViewController alloc]init];
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        [self presentViewController:NaVC animated:YES completion:nil];
        return;
    }
    
    
    if (!_goods) {
        [MBProgressHUD showError:@"收藏失败" toView:self.view];
        return;
    }
    if (!FBH_USER_TOKEN) {
         [MBProgressHUD showError:@"获取用户token失败" toView:self.view];
        return;
    }
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"goods_id":self.goods.goodsID,
                          @"token":FBH_USER_TOKEN};
    
    [NetMethod Post:FBHRequestUrl(kUrl_goods_collect) parameters:dic success:^(id responseObject) {
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

//查看购物车
-(void)LookShopCar{
    ShoppingCarViewController *shopCarVC = [[ShoppingCarViewController alloc] init];
    shopCarVC.isConvert = self.isConvert;
    shopCarVC.twoPage = YES;
    [self.navigationController pushViewController:shopCarVC animated:YES];
}

-(void)clickMessageBtn{

    
}

//添加到购物车
-(void)addShoppingCar{
    
    if (!DriverISLogIN) {
        //用户没登录
        LoginViewController *vc = [[LoginViewController alloc]init];
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        [self presentViewController:NaVC animated:YES completion:nil];
        return;
    }
    
    if (!_goods) {
        [MBProgressHUD showError:@"加入购物车失败" toView:self.view];
    }
    
    NSString *num = [NSString stringWithFormat:@"%0.0f",self.countCell.stepper.value];
    
    NSDictionary *dic = @{@"goods_id":_goods.goodsID,
                          @"uid":LoveDriverID,
                          @"number":num,
                          @"token":FBH_USER_TOKEN};
    //发送请求
    [NetMethod Post:FBHRequestUrl(kUrl_add_cart) parameters:dic success:^(id responseObject) {
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


-(void)initHeaderView{
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainScreen.width, MainScreen.width * 5 / 9)];
//    _headerView.backgroundColor = [UIColor redColor];
    _scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, MainScreen.width, _headerView.frame.size.height)];
    
    _scrollview.delegate = self;
    _scrollview.bounces = NO;
    _scrollview.pagingEnabled = YES;
    _scrollview.showsHorizontalScrollIndicator = NO;
    [_headerView addSubview:_scrollview];

    //    点点
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.currentPage = 0;
    _pageControl.selected = YES;
    _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    [_headerView addSubview:_pageControl];
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_headerView.mas_right).offset(0);
        make.bottom.equalTo(_headerView.mas_bottom).offset(0);
        make.left.equalTo(_headerView.mas_left).offset(0);
        make.height.mas_equalTo(20*KHeight);
    }];
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

//查看全部评价
//-(void)lookAllComment{
//    AllCommentViewController *allCommentVC = [[AllCommentViewController alloc] init];
//    allCommentVC.dataArr = self.commentArr;
//    [self.navigationController pushViewController:allCommentVC animated:YES];
//}

#pragma mark --- UITableViewDatasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.webView loadHTMLString:_goods.content baseURL:nil];
//    });
    if (indexPath.section == 0) {
        GoodsDetailTableViewCell *cell = [[GoodsDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.shopType = self.shopType;
        if (_goods) {
            cell.goods = _goods;
            cell.stepper.maximum = [_goods.number integerValue];
        }
        self.countCell = cell;
        return cell;
    }else if (indexPath.section == 1){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commitCell"];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"commitCell"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.text = @"宝贝评价";
        return cell;
    }else{
        GoodsDetailWebCell *webCell = [tableView dequeueReusableCellWithIdentifier:@"webCell"];
        if (!webCell) {
            webCell = [[[NSBundle mainBundle]loadNibNamed:@"GoodsDetailWebCell" owner:self options:nil]lastObject];
            webCell.goosDetailWebView.scrollView.scrollEnabled = NO;
            webCell.goosDetailWebView.delegate = self;
            if (_webViewHeight == 0) {
                [webCell.webLoadingView startAnimating];
            }
        }
        if (_goods) {
            NSURL *url = [NSURL URLWithString:FBHRequestUrl(kUrl_public(_goodsID, @"2"))];
            [webCell.goosDetailWebView loadRequest:[NSURLRequest requestWithURL:url]];
        }
        return webCell;
    }
//    else{
//        GoodsDetailDescriptionCell *descCell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
//        descCell = [[[NSBundle mainBundle]loadNibNamed:@"GoodsDetailDescriptionCell" owner:self options:nil]lastObject];
//        if (_goods) {
//            descCell.contentLabel.text = @"继续拖动,查看图文详情";
//        }
//        return descCell;
//    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 180;
    }else if (indexPath.section == 1){
        //商品评价
        return 45.0;
    }
    
    
    return _webViewHeight + 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        NSLog(@"宝贝评价");
        YzCommitListViewController *commitListVC = [[YzCommitListViewController alloc]init];
        commitListVC.goodsID = _goodsID;
        [self.navigationController pushViewController:commitListVC animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - scrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat x = scrollView.contentOffset.x;
    int index = x/MainScreen.width;
    _pageControl.currentPage = index;
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //    if (scrollView == _tableView) {
    //        self.NavBarView.backgroundColor = [UIColor colorWithRed:255/255.0f green:80/255.0f blue:0/255.0f alpha:scrollView.contentOffset.y/(MainScreen.height-300*KHeight)];
    //        NSLog(@"scroll:%f",scrollView.contentOffset.y/(MainScreen.height-150*KHeight));
    //    }
    
}

#pragma mark - Reload Goods Data
- (void)reloadGoodsData{
    
    //商品轮播图
    _scrollview.contentSize = CGSizeMake(MainScreen.width * _goods.images.count, 0);
    _pageControl.numberOfPages = _goods.images.count;
    
    for (int i = 0 ; i < _goods.images.count; i ++) {
        //加载图片
        NSString *imgUrl = [NSString stringWithFormat:@"%@%@",FBHBaseURL,_goods.images[i]];
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
        imageView.frame = CGRectMake(i * MainScreen.width, 0, MainScreen.width, _scrollview.frame.size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_scrollview addSubview:imageView];
    }
    [_tableView reloadData];
}

#pragma mark - URL REQUEST
/**
 根据商品id请求商品详情数据
 */
- (void)requestGoodsDetailsData{
    if (!_goodsID) {
        [MBProgressHUD showError:@"无商品ID" toView:self.view];
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",kUrl_goods_detail, _goodsID];
    [NetMethod GET:FBHRequestUrl(url) parameters:nil success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"]intValue];
        if (code == 200) {
            NSDictionary *dataDic = dic[@"data"];
            _goods = [[FBHGoods alloc] init];
            [_goods setValuesForKeysWithDictionary:dataDic];
            [self reloadGoodsData];
        }else{
            [MBProgressHUD showError:dic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if (_webViewHeight != 0) {
        return;
    }
    
    _webViewHeight = webView.scrollView.contentSize.height;
    NSLog(@"_webViewHeight%f", _webViewHeight);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
    GoodsDetailWebCell *webCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    [webCell.webLoadingView stopAnimating];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
