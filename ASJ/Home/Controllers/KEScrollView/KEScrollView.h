//
//  KEScrollView.h
//  qmks
//
//  Created by zhongweiHan on 17/3/18.
//  Copyright © 2017年 Chun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KEScrollView;
@protocol KEScrollViewDelegate <NSObject>

@optional;
- (void)pagingScrollView:(KEScrollView *)scrollView willDisplayViewControllerAtIndex:(NSInteger)index;
- (void)pagingScrollView:(KEScrollView *)scrollView currentPageDidChange:(NSUInteger)page;
- (void)pagingScrollView:(KEScrollView *)scrollView currentPageDidChangeInFloat:(CGFloat)floatInPage;

@end

@interface KEScrollView : UIScrollView

@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic, weak) id<KEScrollViewDelegate> pagingDelegate;

- (void)setViewControllers:(NSArray *)viewControllers andCurrenPage:(NSInteger)page;

- (void)setCurrentPage:(NSInteger)currentPage animated:(BOOL)animated;

@end
