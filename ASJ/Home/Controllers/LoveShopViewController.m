//
//  LoveShopViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "LoveShopViewController.h"
#import "LoveShopTableViewCell.h"
#import "triangleView.h"
#import "GoodsDetailViewController.h"
#import "SortGoods.h"

#define btnWidth self.view.frame.size.width/3

@interface LoveShopViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *headView;
@property (nonatomic,strong) UIView *headerView;
@property (nonatomic,strong) UIButton *currentBtn;
@property (nonatomic,strong) triangleView *triangleV;
@property (nonatomic,strong) NSMutableArray *btnArr;
@property (nonatomic,copy) NSString *qg_time;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,assign) NSInteger currentTime;
@property (nonatomic,assign) NSInteger time10;
@property (nonatomic,assign) NSInteger time11;
@property (nonatomic,assign) NSInteger time12;
@property (nonatomic,assign) NSInteger time13;

@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) NSMutableArray *commentArr;
@end

@implementation LoveShopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed=YES;
    }
    return self;
}

-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}


-(NSMutableArray *)imageArr{
    if (!_imageArr) {
        _imageArr = [NSMutableArray array];
    }
    return _imageArr;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatrUI];
    //默认为1
    self.qg_time = @"1";
    [self requestData];
}

-(void)requestData{
    //清空数据源
    [self.dataArr removeAllObjects];
    [self.imageArr removeAllObjects];
    
    NSDictionary *dic = @{@"qg_time":self.qg_time};
    [NetMethod Post:LoveDriverURL(@"APP/Shopaqg/qianggou") parameters:dic success:^(id responseObject) {
        if (![responseObject[@"data"] isKindOfClass:[NSNull class]]) {
            //获取时间戳
            self.currentTime = [responseObject[@"time"] integerValue];
            self.time10 = [responseObject[@"t10"] integerValue];
            self.time11 = [responseObject[@"t11"] integerValue];
            self.time12 = [responseObject[@"t12"] integerValue];
            self.time13 = [responseObject[@"t13"] integerValue];
            //遍历
            for (NSDictionary *dic in responseObject[@"data"]) {
                SortGoods *sortModel = [[SortGoods alloc] init];
                [sortModel setValuesForKeysWithDictionary:dic];
                
                //获取图片url
                NSDictionary *coverDic = @{@"cover":sortModel.cover[0]};
                [NetMethod Post:LoveDriverURL(@"APP/Public/get_cover_url") parameters:coverDic success:^(id responseObject) {
                    if ([responseObject[@"code"] isEqual:@200]) {
                        NSString *picURLStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",responseObject[@"list"][0]];
                        [self.imageArr addObject:picURLStr];
                        [self.dataArr addObject:sortModel];
                        [self.tableView reloadData];
                                            }
                } failure:^(NSError *error) {
                    NSLog(@"获取图片失败");
                }];
            }
            
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];

}

-(void)creatrUI{
    self.view.backgroundColor = [UIColor whiteColor];
    [self initNavigationBar];
    [self initHeadView];
    [self initTableView];
//    [self initTriangleView];
    
}

-(void) initHeadView{
    _headView = [[UIView alloc] init];
    _headView.backgroundColor = RGBColor(42, 53, 64);
    [self.view addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(55*KHeight);
    }];
    NSArray *strArr = @[@"10:00\n已开抢",@"11:00\n抢购进行中",@"12:00\n即将开抢"];
    for (int i = 0; i < 3; i ++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i*btnWidth, 0, btnWidth, 55*KHeight)];
        [button setBackgroundImage:[UIImage imageNamed:@"blackBG"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"redBG"] forState:UIControlStateSelected];
        button.titleLabel.numberOfLines = 2;
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.titleLabel.font = [UIFont systemFontOfSize:16*KHeight];
        [button setTitle:strArr[i] forState:UIControlStateNormal];
        button.tag = 100 + i;
        [button addTarget:self action:@selector(TapHeadBtn:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            button.selected = YES;
            _currentBtn = button;
        }
        [_headView addSubview:button];
        
    }
}

-(void)initTriangleView{
    _triangleV = [[triangleView alloc] init];
    [self.view addSubview:_triangleV];
    [_triangleV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headView.mas_bottom).offset(0);
        make.left.equalTo(self.view.mas_left).offset(MainScreen.width/6-15*Kwidth);
        make.height.mas_equalTo(10*KHeight);
        make.width.mas_equalTo(30*Kwidth);
    }];
}

-(void)TapHeadBtn:(UIButton *)sender{
    
    //避免同一按钮被多次连续点击
    if (sender != _currentBtn) {
        _currentBtn.selected = NO;
        _currentBtn = sender;
//        [UIView animateWithDuration:0.2 animations:^{
//            _triangleV.frame = CGRectMake(_currentBtn.frame.origin.x+_currentBtn.frame.size.width/2-_triangleV.frame.size.width/2, _triangleV.frame.origin.y, _triangleV.frame.size.width, _triangleV.frame.size.height);
//        }];
        
        switch (sender.tag) {
            case 100:
            {
                self.qg_time = @"1";
                [self requestData];
            }
                break;
            case 101:{
                self.qg_time = @"2";
                [self requestData];
            }
                break;
            case 102:{
                self.qg_time = @"3";
                [self requestData];
            }
                break;
            default:
                break;
        }
        
    }
    
    
    _currentBtn.selected = YES;
}

-(void) initTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //_tableView.tableHeaderView = _headerView;
    _tableView.tableFooterView = [[UIView alloc] init];
    //下拉刷新
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
        [_tableView.mj_header endRefreshing];
    }];
    //上拉加载
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [_tableView.mj_footer endRefreshingWithNoMoreData];
    }];
    [self.view addSubview:self.tableView];
   
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(_headView.mas_bottom).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
    }];
}

-(void)initNavigationBar{
    self.title = @"爱抢购";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickBuy:(UIButton *)sender{
    if (self.dataArr) {
        SortGoods *sortModel = self.dataArr[sender.tag - 666];
        [self requestDetailDataWith:sortModel.goodID];
    }
}

//获取商品详情页面数据
-(void)requestDetailDataWith:(NSString *)goodID{
    NSDictionary *dic = @{@"id":goodID};
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
    [NetMethod Post:LoveDriverURL(@"APP/Shop/detail") parameters:dic success:^(id responseObject) {
        //NSLog(@"responsed === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            
            if (![responseObject[@"pllist"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"pllist"]) {
                    CommentModel *model = [[CommentModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.commentArr addObject:model];
                }
            }
            
            sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
                goodDetailVC.sortGoods = sortGood;
                goodDetailVC.commentCount = responseObject[@"plcount"];
                goodDetailVC.commentArr = self.commentArr;
                [self.navigationController pushViewController:goodDetailVC animated:YES];
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


#pragma mark ---UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    NSString *cellID = [NSString stringWithFormat:@"cell%ld%ld",indexPath.row,indexPath.section];
    
    LoveShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[LoveShopTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    cell.buyBtn.tag = indexPath.row + 666;
    [cell.buyBtn addTarget:self action:@selector(clickBuy:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.imageArr.count > 0 && self.dataArr.count > 0) {
        cell.sortModel = self.dataArr[indexPath.row];
        cell.imageStr = self.imageArr[indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.currentTime > self.time13) {
        [cell.buyBtn setBackgroundImage:[UIImage imageNamed:@"weikaishi"] forState:UIControlStateNormal];
        cell.buyBtn.layer.masksToBounds = YES;
        cell.buyBtn.layer.cornerRadius = 3.5*KHeight;
        cell.buyBtn.userInteractionEnabled = NO;
    }else if (self.currentTime > self.time12){

        if (![self.qg_time isEqualToString:@"3"]) {
            [cell.buyBtn setBackgroundImage:[UIImage imageNamed:@"weikaishi"] forState:UIControlStateNormal];
            cell.buyBtn.layer.masksToBounds = YES;
            cell.buyBtn.layer.cornerRadius = 3.5*KHeight;
            cell.buyBtn.userInteractionEnabled = NO;
        }
    }else if (self.currentTime > self.time11){
        if (![self.qg_time isEqualToString:@"2"]) {
            [cell.buyBtn setBackgroundImage:[UIImage imageNamed:@"weikaishi"] forState:UIControlStateNormal];
            cell.buyBtn.layer.masksToBounds = YES;
            cell.buyBtn.layer.cornerRadius = 3.5*KHeight;
            cell.buyBtn.userInteractionEnabled = NO;
        }
    }else if (self.currentTime > self.time10){
        if (![self.qg_time isEqualToString:@"1"]) {
            [cell.buyBtn setBackgroundImage:[UIImage imageNamed:@"weikaishi"] forState:UIControlStateNormal];
            cell.buyBtn.layer.masksToBounds = YES;
            cell.buyBtn.layer.cornerRadius = 3.5*KHeight;
            cell.buyBtn.userInteractionEnabled = NO;
        }
    }else{
        [cell.buyBtn setBackgroundImage:[UIImage imageNamed:@"weikaishi"] forState:UIControlStateNormal];
        cell.buyBtn.layer.masksToBounds = YES;
        cell.buyBtn.layer.cornerRadius = 3.5*KHeight;
        cell.userInteractionEnabled = NO;
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    _headerView = [[UIView alloc] init];
    _headerView.backgroundColor = [UIColor whiteColor];
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor redColor];
    [_headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headerView.mas_left).offset(0);
        make.top.equalTo(_headerView.mas_top).offset(2*KHeight);
        make.width.mas_equalTo(4*Kwidth);
        make.bottom.equalTo(_headerView.mas_bottom).offset(0);
    }];
    
    UILabel *leftL = [[UILabel alloc] init];
    leftL.text = @"限时限量 疯狂抢购";
    leftL.font = [UIFont systemFontOfSize:16*KHeight];
    leftL.textColor = [UIColor redColor];
    [_headerView addSubview:leftL];
    [leftL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView.mas_right).offset(5*Kwidth);
        make.centerY.equalTo(_headerView.mas_centerY);
    }];
    
    UILabel *rightL = [[UILabel alloc] init];
//    rightL.text = @"本次抢购时长1小时";
    rightL.font = [UIFont systemFontOfSize:16*KHeight];
    rightL.textColor = [UIColor grayColor];
    [_headerView addSubview:rightL];
    [rightL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_headerView.mas_right).offset(-10*Kwidth);
        make.centerY.equalTo(_headerView.mas_centerY);
    }];
    UIView *bottomLine = [[UIView alloc] init];
    bottomLine.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_headerView addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headerView.mas_left).offset(0);
        make.right.equalTo(_headerView.mas_right).offset(0);
        make.bottom.equalTo(_headerView.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    return _headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135*KHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50*KHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
