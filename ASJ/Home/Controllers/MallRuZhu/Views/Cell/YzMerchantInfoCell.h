//
//  YzMechantInfoCell.h
//  ASJ
//
//  Created by Dororo on 2019/6/28.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YzMerchantInfoDelegate <NSObject>
- (void)showTextFieldAlertViewWithIndexPath:(NSIndexPath *)indexPath;
@end

@interface YzMerchantInfoCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *mTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *mTextField;


@property (nonatomic, weak)id<YzMerchantInfoDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
