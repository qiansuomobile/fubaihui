//
//  YzMechantInfoTableView.m
//  ASJ
//
//  Created by Dororo on 2019/6/28.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzMerchantInfoTableView.h"
#import "YzMerchantInfoCell.h"

#import "CZHAddressPickerView.h"
#import "AddressPickerHeader.h"

@interface YzMerchantInfoTableView ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)NSArray *sectionData;

@property (nonatomic, copy)NSDictionary *provinceDic;
@property (nonatomic, copy)NSDictionary *cityDic;
@property (nonatomic, copy)NSDictionary *districtDic;
@end

@implementation YzMerchantInfoTableView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        [self initLoadData];
    }
    return self;
}

- (void)initLoadData{
    _sectionData = @[
  @[
  @{@"title":@"商户名称",
    @"placeholdr":@"商户名称"},
  @{@"title":@"联系人",
    @"placeholdr":@"您的称呼"},
  @{@"title":@"手机号",
    @"placeholdr":@"您的联系方式"}],
  @[
  @{@"title":@"营业执照号",
    @"placeholdr":@"商家的营业执照编号"},
  @{@"title":@"营业执照照片",
    @"placeholdr":@"(未选择任何文件)"},
  @{@"title":@"店铺Logo",
    @"placeholdr":@"(未选择任何文件)"},
  @{@"title":@"店铺分类",
    @"placeholdr":@"请选择店铺分类"},
  @{@"title":@"分类标签",
    @"placeholdr":@"请选择分类标签"},
  @{@"title":@"所在地点",
    @"placeholdr":@"选择地区"},
  @{@"title":@"详细地址",
    @"placeholdr":@"请填写店铺的详细地址"}],
  @{}];
}

#pragma mark - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if ((indexPath.section == 1 && indexPath.row == 1) ||
        (indexPath.section == 1 && indexPath.row == 2) ||
        (indexPath.section == 1 && indexPath.row == 3) ||
        (indexPath.section == 1 && indexPath.row == 4)) {
        [_kDelegate showMallTagViewControllerWithType:indexPath.row];
    }
    
    if (indexPath.section == 2 && indexPath.row == 0) {
        [_kDelegate showMallTagViewControllerWithType:-1];
    }
    
    if (indexPath.section == 1 && indexPath.row == 5) {
        
        YzMerchantInfoCell *meachantCell = [tableView cellForRowAtIndexPath:indexPath];
        [CZHAddressPickerView areaPickerViewWithAreaBlock:^(NSDictionary *province, NSDictionary *city, NSDictionary *area) {
            _provinceDic    = province;
            _cityDic        = city;
            _districtDic    = area;
            meachantCell.mTextField.text = [NSString stringWithFormat:@"%@ %@ %@",
                                            _provinceDic[@"region_name"],
                                            _cityDic[@"region_name"],
                                            _districtDic[@"region_name"]];
        }];
    }
}

#pragma MARK -UITABLEVIEWDATASOURCE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {return 3;}
    if (section == 1) {return 7;}
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _sectionData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        static NSString *cellIndentifier = @"MerchantProtocl";
        YzMerchantInfoCell *merchantProtoclCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        merchantProtoclCell = [[[NSBundle mainBundle]loadNibNamed:@"YzMerchantInfoCell" owner:self options:nil]objectAtIndex:0];
        
        return merchantProtoclCell;
    }
    
    static NSString *cellIndentifier = @"MerChantInfoCell";
    YzMerchantInfoCell *meachantCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!meachantCell) {
        meachantCell = [[[NSBundle mainBundle]loadNibNamed:@"YzMerchantInfoCell" owner:self options:nil]lastObject];
    }

    NSArray *sectionArr = _sectionData[indexPath.section];
    NSDictionary *sectionDic = sectionArr[indexPath.row];
    
    meachantCell.mTitleLabel.text = sectionDic[@"title"];
    meachantCell.mTextField.placeholder = sectionDic[@"placeholdr"];
    
    if(indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)){
        meachantCell.mTextField.enabled = NO;
        meachantCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        meachantCell.mTextField.enabled = YES;
        meachantCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return meachantCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

#pragma mark - SET
- (void)setMallTag:(NSDictionary *)mallTag{
    _mallTag = mallTag;
    if (_mallTag) {
        YzMerchantInfoCell *meachantCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
        NSString *title = [NSString stringWithFormat:@"%@", _mallTag[@"name"]];
        meachantCell.mTextField.text = title;
    }
}

- (void)setClassTag:(NSDictionary *)classTag{
    _classTag = classTag;
    if (_classTag) {
        YzMerchantInfoCell *meachantCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1]];
        NSString *title = [NSString stringWithFormat:@"%@", _classTag[@"name"]];
        meachantCell.mTextField.text = title;
    }
}

- (void)setImgName:(NSString *)imgName{
    _imgName = imgName;
    if (_imgName) {
        YzMerchantInfoCell *meachantCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
        NSRange range = [_imgName rangeOfString:@"/" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *name = [_imgName substringFromIndex:range.location + 1];
            NSLog(@"imgName %@",name);
            meachantCell.mTextField.text = name;
        }else{
            meachantCell.mTextField.text = _imgName;
        }
    }
}

- (void)setLogoName:(NSString *)logoName{
    _logoName = logoName;
    if (_logoName) {
        YzMerchantInfoCell *meachantCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        NSRange range = [_logoName rangeOfString:@"/" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *name = [_logoName substringFromIndex:range.location + 1];
            NSLog(@"imgName %@",name);
            meachantCell.mTextField.text = name;
        }else{
            meachantCell.mTextField.text = _logoName;
        }
    }
}

#pragma mark - GET

- (NSDictionary *)dataDic{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    YzMerchantInfoCell *name = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (name.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"商户名称不能为空"];
        return nil;
    }
    [dic setValue:name.mTextField.text forKey:@"name"];
    
    YzMerchantInfoCell *contacts = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (contacts.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"联系人不能为空"];
        return nil;
    }
    [dic setValue:contacts.mTextField.text forKey:@"contacts"];
    
    YzMerchantInfoCell *phone = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (phone.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"手机号不能为空"];
        return nil;
    }
    [dic setValue:phone.mTextField.text forKey:@"phone"];
    
    
    YzMerchantInfoCell *licence_number  = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if (licence_number.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"营业执照号不能为空"];
        return nil;
    }
    [dic setValue:licence_number.mTextField.text forKey:@"licence_number"];
    
    YzMerchantInfoCell *licence_cert = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    if (licence_cert.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"营业执照照片不能为空"];
        return nil;
    }
    [dic setValue:_imgName forKey:@"licence_cert"];
    
    YzMerchantInfoCell *logo = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
    if (logo.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"店铺logo不能为空"];
        return nil;
    }
    [dic setValue:_logoName forKey:@"logo"];
    
    YzMerchantInfoCell *cid = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
    if (cid.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"商户分类不能为空"];
        return nil;
    }
    [dic setValue:_mallTag[@"id"] forKey:@"cid"];
    
    YzMerchantInfoCell *screen  = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1]];
    if (screen.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"分类标签不能为空"];
        return nil;
    }
    [dic setValue:_classTag[@"id"] forKey:@"screen"];
    
    
    YzMerchantInfoCell *city = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:1]];
    if (city.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"地区不能为空"];
        return nil;
    }
    //省
    [dic setValue:_provinceDic[@"region_id"] forKey:@"province"];
    //市
    [dic setValue:_cityDic[@"region_id"] forKey:@"city"];
    //县
    [dic setValue:_districtDic[@"region_id"] forKey:@"district"];
    
    YzMerchantInfoCell *address = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:1]];
    if (address.mTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"详细地址不能为空"];
        return nil;
    }
     [dic setValue:address.mTextField.text forKey:@"address_detail"];
    
    return dic;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
