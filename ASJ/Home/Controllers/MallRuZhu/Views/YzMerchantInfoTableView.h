//
//  YzMechantInfoTableView.h
//  ASJ
//
//  Created by Dororo on 2019/6/28.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YzMerchantInfoTableViewDelegate <NSObject>

- (void)showMallTagViewControllerWithType:(NSInteger)type;

@end

@interface YzMerchantInfoTableView : UITableView

@property (nonatomic, copy)NSDictionary *mallTag;
@property (nonatomic, copy)NSDictionary *classTag;

@property (nonatomic, copy)NSString *imgName;

@property (nonatomic, copy)NSString *logoName;

@property (nonatomic, strong)NSDictionary *dataDic;

@property (nonatomic, weak)id<YzMerchantInfoTableViewDelegate>kDelegate;

@end

NS_ASSUME_NONNULL_END
