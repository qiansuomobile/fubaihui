//
//  YzMallRuzhuViewController.h
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

//当前选择照片的类型
typedef NS_ENUM(NSInteger, YzRuzhuChooseStatus) {
    YzRuzhuChooseStatusBL,          // 营业执照
    YzRuzhuChooseStatusLogo         // 店铺logo
};



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface YzMallRuzhuViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
