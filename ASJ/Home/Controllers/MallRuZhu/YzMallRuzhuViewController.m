//
//  YzMallRuzhuViewController.m
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzMallRuzhuViewController.h"
#import "YzMerchantInfoCell.h"
#import "YzMerchantInfoTableView.h"
#import "YzRuzhuTableViewController.h"
#import "AdvertisementViewController.h"

@interface YzMallRuzhuViewController ()<YzMerchantInfoTableViewDelegate, YzRuzhuTableviewControllerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic, strong)NSMutableArray *mallTagArr;
@property (nonatomic, strong)NSMutableArray *classTagArr;
@property (weak, nonatomic) IBOutlet YzMerchantInfoTableView *merchantInfoTableView;

@property (strong,nonatomic)UIImagePickerController * pickImage;

@property (nonatomic, assign)YzRuzhuChooseStatus chooseStatus;
@end

@implementation YzMallRuzhuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"商家入驻";
    [self requestMallRuzhuInfo];
    _merchantInfoTableView.kDelegate = self;
}

#pragma mark - url request

//获取商家入驻的信息
- (void)requestMallRuzhuInfo{
    [NetMethod Post:FBHRequestUrl(kUrl_mall_join_info) parameters:nil success:^(id responseObject) {
        NSDictionary *resultDic = responseObject;
        if ([resultDic[@"code"]intValue] == 200) {
            NSDictionary *dataDic = resultDic[@"data"];
            _mallTagArr = [NSMutableArray arrayWithArray:dataDic[@"dpfl"]];
            _classTagArr = [NSMutableArray arrayWithArray:dataDic[@"flbq"]];
            [self setMerchantTagView];
        }else{
           [MBProgressHUD showError:resultDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

//申请商家入驻
- (void)submitMallRuzhuWithDictionary:(NSDictionary *)dic{
    [NetMethod Post:FBHRequestUrl(kUrl_mall_join) parameters:dic success:^(id responseObject) {
        NSDictionary *resultDic = responseObject;
        if ([resultDic[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:resultDic[@"msg"]];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:resultDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark - merchantview kdelegate
- (void)showMallTagViewControllerWithType:(NSInteger)type{
    switch (type) {
        case 1:
        {
            _chooseStatus = YzRuzhuChooseStatusBL;
            [self createAlertSheet];
        }
            break;
        case 2:
        {
            _chooseStatus = YzRuzhuChooseStatusLogo;
            [self createAlertSheet];
        }
            break;
        case 3:
        {
            //商家标签
            YzRuzhuTableViewController *ruzhuTableViewController = [[YzRuzhuTableViewController alloc]init];
            ruzhuTableViewController.tagArray = _mallTagArr;
            ruzhuTableViewController.title = @"店铺分类";
            ruzhuTableViewController.kDelegate = self;
            [self.navigationController pushViewController:ruzhuTableViewController animated:YES];
        }
            break;
        case 4:
        {
            //分类标签
            YzRuzhuTableViewController *ruzhuTableViewController = [[YzRuzhuTableViewController alloc]init];
            ruzhuTableViewController.tagArray = _classTagArr;
            ruzhuTableViewController.title = @"分类标签";
            ruzhuTableViewController.kDelegate = self;
            [self.navigationController pushViewController:ruzhuTableViewController animated:YES];
        }
            break;
        case -1:
        {
            //商家入驻协议
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
            adVC.Ad_ID = kUrl_ruzhuxieyi;
            adVC.title = @"入驻协议";
            [self.navigationController pushViewController:adVC animated:YES];
        }
        default:
            break;
    }
}


#pragma mark - YzRuzhuTableviewControllerDelegate
- (void)selectTagWithDictionary:(NSDictionary *)dic type:(NSString *)type{
    if ([type isEqualToString:@"店铺分类"]) {
        _merchantInfoTableView.mallTag = dic;
    }else if([type isEqualToString:@"分类标签"]){
        _merchantInfoTableView.classTag = dic;
    }
}

#pragma mark - set
- (void)setMerchantTagView{
    _merchantInfoTableView.mallTag = _mallTagArr[0];
    _merchantInfoTableView.classTag = _classTagArr[0];
}

#pragma MARK - ACTION
- (IBAction)mechantRuzhuAction:(UIButton *)sender {
    // 商家入驻
    if (!_merchantInfoTableView.dataDic) {
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:_merchantInfoTableView.dataDic];
    [dic setValue:LoveDriverID forKey:@"uid"];
    [self submitMallRuzhuWithDictionary:dic];
}

#pragma mark - alertviewcontroller action
- (void)createAlertSheet{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *maleAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
    }];
    UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:@"去相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openAlbum];
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:maleAction];
    [alertController addAction:femaleAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - img
#pragma mark 打开相册选择图片
-(void)openAlbum{
    
    self.pickImage = [[UIImagePickerController alloc]init];
    
    self.pickImage.allowsEditing = YES;
    
    self.pickImage.delegate = self;
    
    [self presentViewController:self.pickImage animated:YES completion:nil];
    
}
#pragma mark  打开相机
- (void)openCamera {
    
    self.pickImage = [UIImagePickerController new];
    
    self.pickImage.delegate = self;
    
    self.pickImage.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    self.pickImage.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        self.pickImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.pickImage animated:YES completion:nil];
        
    }else{
        [MBProgressHUD showError:@"无法打开相机" toView:self.view];
    }
}


#pragma mark  显示相册选择的照片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage * image = info[UIImagePickerControllerEditedImage];
    
    NSData *data = UIImageJPEGRepresentation(image, 0.2);
    NSDictionary *dicPost = @{@"status":@"1"};
    [self dismissViewControllerAnimated:YES completion:nil];
    [NetMethod Post:FBHRequestUrl(kUrl_img_upload)  parameters:dicPost IMGparameters:@{@"img":data} success:^(id responseObject) {
        NSDictionary *resultDic = responseObject;
        if ([resultDic[@"code"] intValue] == 200) {
            if (resultDic[@"data"]) {
                NSString *imgPath = resultDic[@"data"];
                if (_chooseStatus == YzRuzhuChooseStatusBL) {
                    _merchantInfoTableView.imgName = imgPath;
                }else{
                    _merchantInfoTableView.logoName = imgPath;
                }
                
            }
            [MBProgressHUD showError:@"上传图片成功" toView:self.view];
        }else{
            [MBProgressHUD showError:resultDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
