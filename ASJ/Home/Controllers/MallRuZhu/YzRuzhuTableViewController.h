//
//  YzRuzhuTableViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/10.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YzRuzhuTableviewControllerDelegate <NSObject>
- (void)selectTagWithDictionary:(NSDictionary *)dic type:(NSString *)type;
@end

@interface YzRuzhuTableViewController : UITableViewController

@property (nonatomic, strong)NSArray *tagArray;

@property (nonatomic, weak)id<YzRuzhuTableviewControllerDelegate>kDelegate;
@end

NS_ASSUME_NONNULL_END
