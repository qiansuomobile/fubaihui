//
//  YzCouponCell.m
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzCouponCell.h"

@implementation YzCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.masksToBounds = YES;
    self.bgView.layer.borderColor = RGBColor(174, 139, 62).CGColor;
    self.bgView.layer.borderWidth = 0.5f;
    self.bgView.layer.cornerRadius = 5.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
