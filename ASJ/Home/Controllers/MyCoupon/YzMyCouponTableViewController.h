//
//  YzMyCouponTableViewController.h
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YzMyCouponDelegate <NSObject>
- (void)mallCouponInfo:(NSDictionary *)dic;
@end

@interface YzMyCouponTableViewController : UITableViewController

//是否是购物车选择的优惠券
@property (nonatomic, copy)NSString *sid;

@property (nonatomic, assign)int price;

@property (nonatomic, weak)id<YzMyCouponDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
