//
//  YzMyCouponTableViewController.m
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzMyCouponTableViewController.h"
#import "YzCouponCell.h"
#import "YYYJoinShopSecondViewController.h"
@interface YzMyCouponTableViewController ()
@property(nonatomic,retain)NSArray *list;
@end

@implementation YzMyCouponTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"优惠券";
    
    [SVProgressHUD showWithStatus:@"正在加载..."];
    NSDictionary *parameDic = [NSMutableDictionary dictionary];
    if (_sid) {
        [parameDic setValue:_sid forKey:@"store_id"];
    }
    [parameDic setValue:LoveDriverID forKey:@"uid"];
    [parameDic setValue:@"0" forKey:@"pageno"];
    [parameDic setValue:@"20" forKey:@"pagesize"];
    [NetMethod Post:FBHRequestUrl(kUrl_my_coupon) parameters:parameDic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            
            _list = responseObject[@"data"];
            
            [self.tableView reloadData];
            
        }else{
            NSString *msg = [responseObject objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
            
            
//            //假数据
//            _list = @[@{
//                          @"money":             @"1.00",
//                          @"money_condition":   @"100.00",
//                          @"gold":              @"2.00",
//                          @"gold_condition":    @"10.00",
//                          @"order_id":          @"0",
//                          @"store_id":          @"153",
//                          @"use_start_time":    @"1560278547",
//                          @"use_end_time":      @"1565462547",
//                          @"name":              @"米线"
//                          }];
//            [self.tableView reloadData];

        }
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [MBProgressHUD showSuccess:@"请检查网络连接" toView:self.view];
    }];
    
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YzCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YzCouponCell" owner:self options:nil]lastObject];
    }
    
    NSDictionary *dic = _list[indexPath.row];
    
    cell.moneyLable.text = dic[@"money"];
    cell.leftBottomLabel.text = [NSString stringWithFormat:@"满%@可用",dic[@"money_condition"]];
    
    
    NSString *start = [NSDate formattedTimeFromTimeInterval:[dic[@"use_start_time"] longLongValue]];
    NSString *end = [NSDate formattedTimeFromTimeInterval:[dic[@"use_end_time"] longLongValue]];

    cell.dateLabel.text = [NSString stringWithFormat:@"%@-%@",start,end ];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  120.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = _list[indexPath.row];
    
    if ([dic[@"status"] isEqualToString:@"1"]) {
        [MBProgressHUD showError:@"该优惠券已使用" toView:self.view];
        return;
    }
    
    if (_sid) {
        
        int money = [dic[@"money_condition"] intValue];
        if (self.price < money) {
            [MBProgressHUD showError:@"不满足满减金额" toView:self.view];
            return;
        }
        
        [_delegate mallCouponInfo:dic];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    YYYJoinShopSecondViewController *secondVc = [[YYYJoinShopSecondViewController alloc]init];
    secondVc.ShopType = 4;
    secondVc.sid = dic[@"store_id"];
    if (![dic[@"name"] isKindOfClass:[NSNull class]]) {
        secondVc.title = dic[@"name"];
    }
    
    [self.navigationController pushViewController:secondVc animated:YES];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
