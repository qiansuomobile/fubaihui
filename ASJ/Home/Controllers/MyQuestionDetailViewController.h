//
//  MyQuestionDetailViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"

@interface MyQuestionDetailViewController : UIViewController
@property (nonatomic,assign) BOOL isAnwser;
@property (nonatomic,copy) NSString *questionID;
@property (nonatomic,copy) NSString *nickName;

@end
