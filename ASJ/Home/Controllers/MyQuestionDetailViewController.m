//
//  MyQuestionDetailViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "MyQuestionDetailViewController.h"
#import "QuestionFirstTableViewCell.h"
#import "QusetionSecondTableViewCell.h"
#import "AddAnwserViewController.h"
#import "QuestionModel.h"
#import "QuestionReply.h"

@interface MyQuestionDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *footerView;
@property (nonatomic,strong) QuestionModel *questionModel;
@property (nonatomic,strong) NSMutableArray *dataArr;
@end

@implementation MyQuestionDetailViewController

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

-(void)createUI{
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initNavigationBar];
    [self initFooterView];
    [self initTableView];
    [self requestData];
}

-(void)requestData{
    [self.dataArr removeAllObjects];
    if (self.questionID) {
        NSDictionary *dic = @{@"id":self.questionID};
        [NetMethod Post:LoveDriverURL(@"APP/Problem/detail") parameters:dic success:^(id responseObject) {
            
            if ([responseObject[@"code"] isEqual:@200]) {
                NSLog(@"res:%@",responseObject);
                if (![responseObject[@"info"] isKindOfClass:[NSNull class]]) {
                    self.questionModel = [[QuestionModel alloc] init];
                    [_questionModel setValuesForKeysWithDictionary:responseObject[@"info"]];
                    for (NSDictionary *dic in _questionModel.reply) {
                        QuestionReply *model = [[QuestionReply alloc] init];
                        [model setValuesForKeysWithDictionary:dic];
                        [self.dataArr addObject:model];
                    }

                    [self.tableView reloadData];
                }
            }
            
        } failure:^(NSError *error) {
            [MBProgressHUD showSuccess:NetProblem toView:self.view];
        }];

    }
    }

-(void)initFooterView{
    _footerView = [[UIView alloc] initWithFrame:CGRectZero];
    _footerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:_footerView];
    [_footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(60*KHeight);
    }];
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = RGBColor(170, 170, 170);
    [_footerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_footerView.mas_left).offset(0);
        make.top.equalTo(_footerView.mas_top).offset(0);
        make.right.equalTo(_footerView.mas_right).offset(0);
        make.height.mas_equalTo(0.5*KHeight);
    }];
    if (self.isAnwser) {
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = RGBColor(59, 175, 91);
        bgView.layer.cornerRadius = 3.0;
        [_footerView addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_footerView.mas_centerY);
            make.centerX.equalTo(_footerView.mas_centerX);
            make.height.mas_equalTo(44*KHeight);
            make.width.mas_equalTo(175*Kwidth);
        }];
        UIImageView *answerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wolaihuida"]];
        [bgView addSubview:answerImage];
        [answerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView.mas_left).offset(15*Kwidth);
            make.centerY.equalTo(bgView.mas_centerY);
            make.width.mas_equalTo(20*Kwidth);
            make.height.mas_equalTo(20*Kwidth);
        }];
        
        UILabel *anwserL = [[UILabel alloc] init];
        anwserL.text = @"我来回答";
        anwserL.font = [UIFont systemFontOfSize:22*KHeight];
        anwserL.textColor = [UIColor whiteColor];
        [bgView addSubview:anwserL];
        [anwserL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(answerImage.mas_right).offset(10*Kwidth);
            make.centerY.equalTo(bgView.mas_centerY);
        }];
        
        UIButton *btn = [[UIButton alloc] init];
        [btn setBackgroundColor:[UIColor clearColor]];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 3.0;
        [btn addTarget:self action:@selector(clickAnwser) forControlEvents:UIControlEventTouchUpInside];
        [_footerView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_footerView.mas_centerY);
            make.centerX.equalTo(_footerView.mas_centerX);
            make.height.mas_equalTo(44*KHeight);
            make.width.mas_equalTo(175*Kwidth);
        }];
        
    }else{
        UILabel *footL = [[UILabel alloc] init];
        footL.text = @"正在为您寻找答案,请耐心等待";
        footL.font = [UIFont systemFontOfSize:20*KHeight];
        footL.textColor = RGBColor(150, 150, 150);
        [_footerView addSubview:footL];
        [footL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_footerView.mas_centerX);
            make.centerY.equalTo(_footerView.mas_centerY);
        }];
    }
    
}


-(void)initTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    //下拉刷新
//    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [_tableView.mj_header endRefreshing];
//    }];
//    //上拉加载
//    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [_tableView.mj_footer endRefreshing];
//    }];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 60*KHeight, 0));
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.bottom.equalTo(self.footerView.mas_top).offset(0);
    }];
}

-(void)initNavigationBar{
    if (self.nickName) {
        self.title = [NSString stringWithFormat:@"%@的提问",self.nickName];
    }else{
        self.title = @"我的提问";
    }
    
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
    
}


-(void)clickAnwser{
    AddAnwserViewController *addVC = [[AddAnwserViewController alloc] init];
    addVC.model = self.questionModel;
    [self.navigationController pushViewController:addVC animated:YES];
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}


//点击采纳

-(void)clickAdopt:(UIButton *)sender{
   
    QuestionReply *model = self.dataArr[sender.tag-1000];
    
    NSDictionary *dic = @{@"uid":LoveDriverID,@"wtid":self.questionModel.question_id,@"hdid":model.reply_id};
   
    [NetMethod Post:LoveDriverURL(@"APP/Problem/replyAdopt") parameters:dic success:^(id responseObject) {
        
        
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        
    
    } failure:^(NSError *error) {
    
        [MBProgressHUD showSuccess:@"采纳失败,请检查网络" toView:self.view];
    
    }];
}


#pragma mark --- UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.dataArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        QuestionFirstTableViewCell *cell = [[QuestionFirstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.model = self.questionModel;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        QusetionSecondTableViewCell *cell = [[QusetionSecondTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.isAnwser = self.isAnwser;
        cell.model = self.dataArr[indexPath.row];
        cell.adoptBtn.tag = 1000+indexPath.row;
        [cell.adoptBtn addTarget:self action:@selector(clickAdopt:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //模型类动态计算高度
    if (indexPath.section == 0) {
        return [self.questionModel CaculateCellHeight:self.questionModel.title];
    }else{
        QuestionReply *model = self.dataArr[indexPath.row];
        
        return [model CaculateCellHeight:model.content];
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10*KHeight;
    }
    return 0;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
