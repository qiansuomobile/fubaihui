//
//  MyQuestionViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "MyQuestionViewController.h"
#import "MyQuestionTableViewCell.h"
#import "MyQuestionDetailViewController.h"
#import "AnswerModel.h"
#import "QuestionModel.h"

@interface MyQuestionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@end

@implementation MyQuestionViewController

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


-(void)viewWillAppear:(BOOL)animated{
    if (self.isAnswer) {
        [self requestAnwserData];
    }else{
        [self requestQuestionData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

-(void)createUI{
    [self initNavigationBar];
    [self initTableView];
}

-(void)requestQuestionData{
    
    [self.dataArr removeAllObjects];
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:LoveDriverURL(@"APP/Problem/index") parameters:dic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    QuestionModel *model = [[QuestionModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.dataArr addObject:model];
                }
                [self.tableView reloadData];
            }
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)requestAnwserData{
    
    [self.dataArr removeAllObjects];
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:LoveDriverURL(@"APP/Problem/replySelf") parameters:dic success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    AnswerModel *model = [[AnswerModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.dataArr addObject:model];
                }
                [self.tableView reloadData];
            }
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(void)initNavigationBar{
    if (self.isAnswer) {
        self.title = @"我的回答";
    }else{
        self.title = @"我的提问";
    }
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
    
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[MyQuestionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.isAnswer = self.isAnswer;
    if (self.isAnswer) {
        cell.model = self.dataArr[indexPath.row];
    }else{
        cell.questionModel = self.dataArr[indexPath.row];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90*KHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.isAnswer) {
        
    }else{
        MyQuestionDetailViewController *detailVC = [[MyQuestionDetailViewController alloc] init];
        QuestionModel *model = self.dataArr[indexPath.row];
        detailVC.questionID = model.question_id;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
