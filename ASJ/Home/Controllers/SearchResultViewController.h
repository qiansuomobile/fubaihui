//
//  SearchResultViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/27.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultViewController : UIViewController
@property (nonatomic,copy) NSString *keyWord;
@property (nonatomic,assign) NSInteger shopType;
@end
