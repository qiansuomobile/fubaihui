//
//  SearchResultViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/27.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SearchResultTitleView.h"
#import "SearchResultCollectionViewCell.h"
#import "GoodsDetailViewController.h"
#import "SortGoods.h"


@interface SearchResultViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) UIView *headerView;
@property (nonatomic,strong) SearchResultTitleView *titleView;
@property (nonatomic,strong) UIButton *currentBtn;
@property (nonatomic,strong) NSMutableArray *dataArr;
//排序类型 1.综合 2.销量 3.价格
@property (nonatomic,strong) NSString *px;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,strong) NSMutableArray *commentArr;
@end

static NSString *collectionID1 = @"collectionCellID";

static NSInteger Pages;

@implementation SearchResultViewController

-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

-(NSMutableArray *)imageArr{
    if (!_imageArr) {
        _imageArr = [NSMutableArray array];
    }
    return _imageArr;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

-(void)viewWillAppear:(BOOL)animated{
   
    self.navigationController.navigationBar.hidden = NO;

}

- (void)viewDidLoad {
  
    [super viewDidLoad];
    
    [self createUI];
    
    Pages= 1;
    
    [self requestData];
    
    [self setRefreshing];
    
//    [self requestData];
}

-(void)LoadMoreData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages];
    
    
    NSLog(@" 第几页   %@",page);
    
    NSDictionary *dic = @{@"p":page,@"keywords":self.keyWord};
    
    NSString *urlStr;
    
    switch (self.shopType) {
            
        case 1:
        {
            urlStr = @"APP/Shop/search";
        }
            break;
        case 2:
        {
            urlStr = @"APP/Shopa/search";
        }
            break;
        case 3:
        {
            urlStr = @"APP/Shopyhls/search";
        }
            break;
        default:
            break;
    }
    
    [NetMethod Post:LoveDriverURL(urlStr) parameters:dic success:^(id responseObject) {
//        NSLog(@"res:%@",responseObject);
        
        if ([responseObject[@"count"] integerValue] == 0) {
            
            [MBProgressHUD showSuccess:@"没有更多了" toView:self.view];
            
            [self.collectionView reloadData];
            
            return ;
            
        }else{
           
            if ([responseObject[@"list"] isKindOfClass:[NSNull class]]){
             
                
                [MBProgressHUD showSuccess:@"没有更多了" toView:self.view.window];
                
                
                [self.collectionView reloadData];
                
                return;
                
            }else{
            
                NSMutableArray *modelArray = [NSMutableArray array];
                NSMutableArray *imagsAraay = [NSMutableArray array];
                
                for (NSDictionary *dic in responseObject[@"list"]) {
                
                    SortGoods *model = [[SortGoods alloc] init];
                    
                    [model setValuesForKeysWithDictionary:dic];
                    //调用获取图片接口
                    [modelArray addObject:model];
                    
//                    NSDictionary *coverDic = @{@"cover":model.cover};
//                 
//                    [NetMethod Post:LoveDriverURL(@"APP/Public/get_cover_url") parameters:coverDic success:^(id responseObject) {
//                       
//                        if ([responseObject[@"code"] isEqual:@200]) {
//                            NSString *imageStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",responseObject[@"list"][0]];
//                    
//                            [imagsAraay addObject:imageStr];
//                            
//                            [self.collectionView reloadData];
//                        
//                        }
//                    
//                    } failure:^(NSError *error) {
//                        [MBProgressHUD showSuccess:NetProblem toView:self.view];
//                    }];
                    
//                    [self.imageArr addObjectsFromArray:imagsAraay];
//                    
//                    [self.dataArr addObjectsFromArray:modelArray];
               
                }
             
                [self.imageArr addObjectsFromArray:imagsAraay];
                
                [self.dataArr addObjectsFromArray:modelArray];
                
                [self.collectionView reloadData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
       
        
//        [self requestData];
        
        [self.collectionView.mj_header endRefreshing];
        
    }];
    
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self LoadMoreData];
        
        [self.collectionView.mj_footer endRefreshing];
    }];
}





-(void)requestData{
   
    [self.dataArr removeAllObjects];
    
    [self.imageArr removeAllObjects];
   
//    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages];
    
    NSDictionary *dic = @{@"px":@"1",@"keywords":self.keyWord};
    
    NSString *urlStr;
    
    
    NSLog(@"搜索类型  %ld",(long)self.shopType);
  
    switch (self.shopType) {
    
        case 1:
        {
            urlStr = @"APP/Shop/search";
        }
            break;
        case 2:
        {
            urlStr = @"APP/Shopa/search";
        }
            break;
        case 3:
        {
            urlStr = @"APP/Shopyhls/search";
        }
            break;
        default:
            break;
    }
    
    [NetMethod Post:LoveDriverURL(urlStr) parameters:dic success:^(id responseObject) {
//        NSLog(@"res:%@",responseObject);
       
        if ([responseObject[@"count"] integerValue] == 0) {
            
            [MBProgressHUD showSuccess:@"无搜索结果" toView:self.view];
        
            [self.collectionView reloadData];
            
            return ;
        
        }else{
            
            if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
              
                for (NSDictionary *dic in responseObject[@"list"]) {
                
                    SortGoods *model = [[SortGoods alloc] init];
                    
                    [model setValuesForKeysWithDictionary:dic];
                    //调用获取图片接口
                    
//                    NSDictionary *coverDic = @{@"cover":model.cover};
//                  
//                    [NetMethod Post:LoveDriverURL(@"APP/Public/get_cover_url") parameters:coverDic success:^(id responseObject) {
//                      
//                        NSLog(@"%@",responseObject);
//                        
//                        
//                        if ([responseObject[@"code"] isEqual:@200]) {
//                            
//                            NSString *imageStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",responseObject[@"list"][0]];
//                            
//                            [self.imageArr addObject:imageStr];
//                           
//                            [self.collectionView reloadData];
//                        }
//                    } failure:^(NSError *error) {
//                    
//                        [MBProgressHUD showSuccess:NetProblem toView:self.view];
//                 
//                    }];

                    [self.dataArr addObject:model];
                }
                [self.collectionView reloadData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


-(void)createUI{
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initNavigationBar];
//    [self initHeaderView];
    [self initCollectionView];
}

-(void)initNavigationBar{
    _titleView = [[SearchResultTitleView alloc] initWithFrame:CGRectMake(0, 0, 200*Kwidth, 30*KHeight)];
//    _titleView.keyWordL.text = self.keyWord;
    self.navigationItem.titleView = _titleView;
}

-(void)initHeaderView{
    
    _headerView = [[UIView alloc] init];
    
    _headerView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_headerView];
    
    
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.right.equalTo(self.view.mas_bottom).offset(0);
        make.height.mas_equalTo(50*KHeight);
    }];
    NSArray *titleArr = @[@"综合排序",@"销量优先",@"价格"];
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*MainScreen.width/3, 0, MainScreen.width/3, 50*KHeight)];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:RGBColor(211, 84, 33) forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickHeaderBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i+100;
        if (i == 0) {
            btn.selected = YES;
        }
        [_headerView addSubview:btn];
    }
    
}

-(void)initCollectionView{
   
    CGFloat magin = 5*KHeight;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
   
    layout.itemSize = CGSizeMake((MainScreen.width-Kwidth*80-15*Kwidth)/2, 200*KHeight);
    
    layout.sectionInset = UIEdgeInsetsMake(magin, magin, 0, magin);
    
    layout.minimumInteritemSpacing = magin;
    
    layout.minimumLineSpacing = magin;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight) collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    //下拉刷新
    _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [_collectionView.mj_header endRefreshing];
    }];
    //上拉加载
    _collectionView.mj_footer = [MJRefreshBackFooter footerWithRefreshingBlock:^{
        [_collectionView.mj_footer endRefreshing];
    }];
  
    _collectionView.backgroundColor = [UIColor colorWithRed:223/255.0f green:223/255.0f blue:223/255.0f alpha:1.0];
    //注册cell
    [_collectionView registerClass:[SearchResultCollectionViewCell class] forCellWithReuseIdentifier:collectionID1];
   
    [self.view addSubview:_collectionView];
   
//    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left).offset(0);
//        make.top.equalTo(self.view.mas_bottom).offset(0);
//        make.right.equalTo(self.view.mas_right).offset(0);
//        make.bottom.equalTo(self.view.mas_bottom).offset(0);
//    }];
}

-(void)clickHeaderBtn:(UIButton *)sender{
    if (_currentBtn != sender) {
        _currentBtn.selected = NO;
        _currentBtn = sender;
        _currentBtn.selected = YES;
       
        
        switch (sender.tag) {
            case 100:{
                self.px = @"1";
                [self requestData];
            }
                break;
            case 101:{
                self.px = @"2";
                [self requestData];
            }
                break;
            case 102:{
                self.px = @"3";
                [self requestData];
            }
                break;

            default:
                break;
        
        }
    }
}

#pragma mark -- UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    SearchResultCollectionViewCell *cell;
    
    
    SearchResultCollectionViewCell *cell = [SearchResultCollectionViewCell collectionWithCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    
//    cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionID1 forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    
    SortGoods *sortGoods = self.dataArr[indexPath.row];
//    sortGoods = self.dataArr[indexPath.row];
    
    
//    if (self.shopType ==3 || self.shopType ==2) {
//        
////        cell.shopType = 2;
//        
//        [cell setCorevImg:sortGoods];
//    
//    }else{
    
        [cell setSortGood:sortGoods];
    if (self.shopType == 2) {
        cell.priceL.text = [NSString stringWithFormat:@"银积分:%ld",(long)[sortGoods.price integerValue]];
        cell.rmbL.hidden = YES;
        cell.goldBtn.hidden = YES;
        cell.silverBtn.hidden = YES;
    } else  if (self.shopType == 3) {
        cell.priceL.text = [NSString stringWithFormat:@"金积分:%ld",(long)[sortGoods.price integerValue]];
        cell.rmbL.hidden = YES;
        cell.goldBtn.hidden = YES;
        cell.silverBtn.hidden = YES;
    }else{
        cell.rmbL.hidden = NO;
        cell.goldBtn.hidden = NO;
        cell.silverBtn.hidden = NO;
    }

//    }
    


//    cell.imageStr = self.imageArr[indexPath.row];
    
    
//    if (self.imageArr.count == self.dataArr.count) {
//      
//        cell.shopType = self.shopType;
//        
//        cell.sortGood = self.dataArr[indexPath.row];
//        
//        cell.imageStr = self.imageArr[indexPath.row];
//    }
    
    return cell;

}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize sizes = CGSizeMake((YzWidth)/2-10, 190*KHeight);
    return sizes;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //判断是否获取到数据
   
    if (self.dataArr.count > 0) {
    
        SortGoods *model = self.dataArr[indexPath.row];
        
        [self requestDetailDataWith:model.goodID];
        
    }else{
        
        [MBProgressHUD showSuccess:@"未获取到数据,请检查网络连接设置" toView:self.view];
    
    }

}

//获取商品详情页面数据
-(void)requestDetailDataWith:(NSString *)goodID{
    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
    NSString *urlStr;
    
    //                        goodDetailVC.godenType = @"goden";
    switch (self.shopType) {
        case 1:
            urlStr = @"APP/Shop/detail";
            goodDetailVC.godenType = @"goden";
            
            break;
        case 2:
            urlStr = @"APP/Shopa/detail";
            goodDetailVC.godenType = @"sliver";
            
            break;
        case 3:
            urlStr = @"APP/Shopyhls/detail";
            goodDetailVC.godenType = @"gold";
            
            break;
        default:
            break;
    }
    [self.commentArr removeAllObjects];
    NSDictionary *dic = @{@"id":goodID};
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
    [NetMethod Post:LoveDriverURL(urlStr) parameters:dic success:^(id responseObject) {
        //        NSLog(@"responsed详情: === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            
            if (![responseObject[@"pllist"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"pllist"]) {
                    CommentModel *model = [[CommentModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.commentArr addObject:model];
                }
            }
            
            
            //除了正品商城,其他商城暂时不显示大图
            if (self.shopType != 2 && self.shopType != 3) {
                sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
                
                //                sortGood.GodenType = @"goden";
            }else{
                //                sortGood.GodenType = @"sliver";
                
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                if ([sortGood.number isEqualToString:@"0"]) {
                    [MBProgressHUD showSuccess:@"没存货了亲~" toView:self.view];
                }else{
                    
                    goodDetailVC.sortGoods = sortGood;
//                    goodDetailVC.commentCount = responseObject[@"plcount"];
//                    goodDetailVC.commentArr = self.commentArr;
                    goodDetailVC.ShopType = self.shopType;
                    if (self.shopType == 2||self.shopType == 3) {
                        //兑换商城
                        goodDetailVC.isConvert = YES;
                        
                    }else{
                        
                    }
                    [self.navigationController pushViewController:goodDetailVC animated:YES];
                }
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
