//
//  YzGoldRechargeViewController.m
//  ASJ
//
//  Created by Dororo on 2019/6/28.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzGoldRechargeViewController.h"
#import "AliPayTool.h"

@interface YzGoldRechargeViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *goldNumLabel;
@property (weak, nonatomic) IBOutlet UITextField *goldNumTextField;

@property (weak, nonatomic) IBOutlet UILabel *myGoldNumLabel;

@property (nonatomic ,copy)NSString *orderNum;

@end

@implementation YzGoldRechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"充值";
    
    [_goldNumTextField addTarget:self action:@selector(textFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
    [self getGoldRechargeDataRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AliPayResult:) name:@"AliPayResult" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(YZWXpayresult:) name:@"WXpayresult" object:nil];
}

#pragma mark - notification
- (void)YZWXpayresult:(NSNotification *)notif{
    NSString* result = notif.object;
    if([result isEqualToString:@"1"]){
        [MBProgressHUD showSuccess:@"支付成功" toView:self.view];
//        [NetMethod Post:FBHRequestUrl(kUrl_wechatapp) parameters:@{@"ordernum":_orderNum} success:^(id responseObject) {
//            if ([responseObject[@"code"] intValue] == 200) {
//                [MBProgressHUD showSuccess:@"支付成功" toView:self.view];
//                [self.navigationController popViewControllerAnimated:YES];
//            }else{
//                [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
//            }
//        } failure:^(NSError *error) {
//            [MBProgressHUD showSuccess:NetProblem toView:self.view];
//        }];
    }else{
        //在这里写支付失败之后的回调操作
        [MBProgressHUD showSuccess:@"支付失败" toView:self.view];
    }
}

- (void)AliPayResult:(NSNotification *)notif{
    NSInteger resultStatus = [notif.object[@"resultStatus"] integerValue];
    
    
    switch (resultStatus) {
        case 9000:{
            [MBProgressHUD showSuccess:@"支付成功" toView:self.view];
//            [NetMethod Post:FBHRequestUrl(kUrl_alipayapp) parameters:@{@"ordernum":_orderNum} success:^(id responseObject) {
//                if ([responseObject[@"code"] intValue] == 200) {
//                    [MBProgressHUD showSuccess:@"支付成功" toView:self.view];
//                    [self.navigationController popViewControllerAnimated:YES];
//                }else{
//                    [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
//                }
//            } failure:^(NSError *error) {
//                [MBProgressHUD showSuccess:NetProblem toView:self.view];
//            }];
        }
            break;
        case 6001:{
            [MBProgressHUD showSuccess:@"用户中途取消支付" toView:self.view];
        }
            break;
        case 6000:{
            [MBProgressHUD showSuccess:@"网络连接超时" toView:self.view];
        }
            break;
        case 4000:{
            [MBProgressHUD showSuccess:@"订单处理失败" toView:self.view];
        }
            break;
        default:
            break;
    }}

//支付宝支付
- (IBAction)aliPayTapGesture:(UITapGestureRecognizer *)sender {
    if (!_goldNumTextField.text || ([_goldNumTextField.text floatValue] == 0)) {
        [MBProgressHUD showError:@"充值金额不能为0" toView:self.view];
        return;
    }
    NSDictionary *wxPayDic = @{@"id":LoveDriverID,
                               @"money":_goldNumTextField.text,
                               @"type":@"1"
                               };
    [self goldRechargeWithPayInfo:wxPayDic];
}

//微信支付
- (IBAction)wxPayTapGesture:(UITapGestureRecognizer *)sender {
    
    if (!_goldNumTextField.text || ([_goldNumTextField.text floatValue] == 0)) {
        [MBProgressHUD showError:@"充值金额不能为0" toView:self.view];
        return;
    }
    NSDictionary *wxPayDic = @{@"id":LoveDriverID,
                               @"money":_goldNumTextField.text,
                               @"type":@"2"
                               };
    [self goldRechargeWithPayInfo:wxPayDic];
}
#pragma MARK - URL Request
//计算积分与人民币之间实时汇率
- (void)goldExchangeRateWithMoneyNum:(NSString *)num{
    
    NSDictionary *dic = @{@"money":num};
    [NetMethod Post:FBHRequestUrl(kUrl_exchange_rate) parameters:dic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            _goldNumLabel.text = [NSString stringWithFormat:@"%@分",responseObject[@"data"]];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
         [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

//微信支付宝充值 kUrl_gold_recharge
- (void)goldRechargeWithPayInfo:( NSDictionary * _Nonnull )payInfo{
    
    [NetMethod Post:FBHRequestUrl(kUrl_gold_recharge) parameters:payInfo success:^(id responseObject) {
        
        if ([payInfo[@"type"] isEqualToString:@"2"]) {
            
            //微信
            NSDictionary *wxDic = responseObject;
            NSString *errorCode = wxDic[@"errorCode"];
            _orderNum = wxDic[@"out_trade_no"];
            if ([errorCode intValue] == 0) {
                [AliPayTool showWxPayWithAppDic:wxDic[@"responseData"][@"app_response"]];
            }else{
                [MBProgressHUD showError:wxDic[@"errorMsg"] toView:self.view];
            }
        }else{
            //支付宝
            NSDictionary *jsonDict = responseObject;
            if ([jsonDict[@"code"] integerValue] == 200) {
                NSDictionary *dataDic = jsonDict[@"data"];
                _orderNum = dataDic[@"out_trade_no"];
                NSString *signedString = dataDic[@"response"];
                if (signedString != nil) {
                    //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
                    NSString *appScheme = @"FBHALIPAY";
                    // NOTE: 调用支付结果开始支付
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:nil];
                    });
                }
            }else{
                [MBProgressHUD showError:jsonDict[@"msg"] toView:self.view];
            }
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error.description);
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

// 获取用户金积分余额
- (void)getGoldRechargeDataRequest{
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *data = resDic[@"data"];
            //用户金积分
            _myGoldNumLabel.text = [NSString stringWithFormat:@"%@",data[@"score"]];
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldEditChanged:(UITextField*)textField{
    
    NSLog(@"textfield text %@",textField.text);
    [self goldExchangeRateWithMoneyNum:textField.text];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
