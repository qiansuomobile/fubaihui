//
//  YzNewHomeBuild.m
//  ASJ
//
//  Created by a1 on 2017/8/28.
//  Copyright © 2017年 TS. All rights reserved.
//
#define BtnWidth (self.view.frame.size.width-150*Kwidth)/4
#import "SDTimeLineTableViewController.h"
#import "AdOneTableViewCell.h"
#import "AdTwoTableViewCell.h"
#import "AdThreeTableViewCell.h"
#import "DriverShopViewController.h"
#import "GoodsDetailViewController.h"
#import "YZLoveDriverVC.h"
#import "LoveShopViewController.h"//爱抢购
#import "YzqiandaoVC.h"//签到
#import "DriverAnswerViewController.h"//司机问答
#import "SearchView.h"
#import "SearchResultViewController.h"
//点击广告后跳转
#import "AdvertisementViewController.h"
#import "NewModel.h"
#import "BottomGood.h"
#import "YzNewHomeBuild.h"
#import "ConversationListController.h"
#import "ApplyViewController.h"
#import "ChatViewController.h"
#import "YzCommentVC.h"
#import "YzSeachLove.h"
#import "YYYJoinShopViewController.h"

#import "YzHomeJingXuanView.h"
#import "SaoYiSaoViewController.h"
#import "AddFriendViewController.h"

@interface YzNewHomeBuild ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SDCycleScrollViewDelegate, YzHomeJingXuanDelegate>

@property (nonatomic,strong) UIScrollView *scrollview;

@property (nonatomic,strong) UIPageControl *pageControl;

@property (nonatomic,strong) UIImageView *hotBuyImage;

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) UIView *headerView;

@property (nonatomic,strong) UIView *footerView;

@property (nonatomic,strong) UIPageControl *pageC;

@property (nonatomic,strong) SearchView *searchView;
//轮播图图片数组
@property (nonatomic,strong) NSMutableArray *Ad_imageArr;
//轮播图id
@property (nonatomic,strong) NSMutableArray *Ad_IDArr;

@property (nonatomic,copy) NSString *loveShopPic;
//平台头条
@property (nonatomic,strong) NSMutableArray *newsArr;
//底部轮播图
@property (nonatomic,strong) NSMutableArray *bottomAd_imageArr;

@property (nonatomic,strong) NSMutableArray *bottomAd_idArr;

//平台精选
@property (nonatomic,strong) NSMutableArray *goodsArr;

@property (nonatomic,strong) YzHomeJingXuanView *jingXuanView;

@property (nonatomic,strong) NSMutableArray *commentArr;

@property (nonatomic,strong) NSMutableArray *DetailImageArr;

@property (nonatomic , strong) UIImageView *leftImg;

@property (nonatomic , strong) SDCycleScrollView *CycleScrollView1;

@property (nonatomic , strong) SDCycleScrollView *CycleScrollView2;

@property (nonatomic , strong) SDCycleScrollView *CycleScrollView3;

@end

@implementation YzNewHomeBuild
{
    UIButton *rightBtn;
    UILabel *countLabel;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //请求轮播图数据
    [self requestData1];
    
    [self createUI];
    [self.view addSubview:self.tableView];
    
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableFooterView = self.footerView;
    
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        
        UITableViewController* tvc=[[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
        [self addChildViewController:tvc];
        
        _tableView = tvc.tableView;
        _tableView.frame =CGRectMake(0, 0, YzWidth, YzHeight -44);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = RGBColor(237, 237, 237);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    }
    
    return _tableView;

}

-(void)createUI{
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"qrcode"] style:UIBarButtonItemStyleDone target:self action:@selector(showQRCodeView)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
//    _leftImg = leftImg;
//    _leftImg.hidden = NO;
    
    rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"xiaoxi"] forState:UIControlStateNormal];
    
    countLabel = [[UILabel alloc]initWithFrame:CGRectMake(rightBtn.frame.size.width/2 + 5, 0, 10, 10)];
    countLabel.backgroundColor = [UIColor redColor];
    countLabel.textColor = [UIColor whiteColor];
    countLabel.textAlignment = NSTextAlignmentCenter;
    countLabel.layer.cornerRadius = 5;
    countLabel.clipsToBounds = YES;
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
//    countLabel.text =[NSString stringWithFormat:@"%ld",unreadCount];
    if (unreadCount==0) {
        countLabel.hidden = YES;
    }else{
        countLabel.hidden = NO;
    }
    [rightBtn addSubview:countLabel];

    [rightBtn addTarget:self action:@selector(clickMessageBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _searchView = [[SearchView alloc] initWithFrame:CGRectMake(0, 0, MainScreen.width - 180, 30)];
    _searchView.textF.delegate = self;
    self.navigationItem.titleView = _searchView;
//    _searchView.layer.cornerRadius = 10;
    _searchView.textF.textColor = [UIColor blackColor];//yu

    
    [_searchView.searchBtn addTarget:self action:@selector(clickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    //下拉刷新
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //请求轮播图数据
        [self requestData1];
        [self requestData5];
        
        [_tableView.mj_header endRefreshing];
        
    }];

}
//扫一扫
- (void)showQRCodeView{
    SaoYiSaoViewController *lbx = [[SaoYiSaoViewController alloc]init];
    lbx.title = @"扫描二维码";
    [self.navigationController pushViewController:lbx animated:YES];
}

//首页banner
-(void)requestData1{
 
    [NetMethod GET:FBHRequestUrl(kUrl_home_banner) parameters:nil success:^(id responseObject) {
        NSLog(@" responseObject %@", responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            if (![responseObject[@"data"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"data"]) {
                    NSString *picURL = [NSString stringWithFormat:@"%@%@",FBHBaseURL,dic[@"img"]];
                    NSString *Ad_IDStr = dic[@"tzurl"];
                    [self.Ad_IDArr addObject:Ad_IDStr];
                    [self.Ad_imageArr addObject:picURL];
                }
                
                self.CycleScrollView1.imageURLStringsGroup = self.Ad_imageArr;
            }
            
        }
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"网络错误"];
    }];

}

-(void)requestData5{
    
//    NSDictionary *dic = @{@"pageno":@1};
    
    [NetMethod Post:FBHRequestUrl(kUrl_home_jingxuan) parameters:nil success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            
            
            if (![responseObject[@"data"] isKindOfClass:[NSNull class]]) {
                
                //平台精选
                NSArray *jxArray = responseObject[@"data"][@"jingxuan"];
                
                [self.goodsArr removeAllObjects];
                for (NSDictionary *dic in jxArray) {
                    
                    BottomGood *model = [[BottomGood alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    model.picture = [NSString stringWithFormat:@"%@%@", FBHBaseURL,model.path];
                    model.tzurl = [NSString stringWithFormat:@"%@%@",FBHRequestUrl(kUrl_goods_detail) ,model.goodID];
                    [self.goodsArr addObject:model];
                }
                _jingXuanView.jingXuanArr = self.goodsArr;
                
                // 平台公告
                NSMutableArray *titles = [NSMutableArray array];
                NSMutableArray *titles2 = [NSMutableArray array];
                NSArray *gonggaoArr =  responseObject[@"data"][@"gonggao"];
                for (NSDictionary *dic in gonggaoArr) {
                    NewModel *model = [[NewModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.newsArr addObject:model];
                    
                    [titles addObject:model.title];
                    [titles2 addObject:@""];
                    
                }
                self.CycleScrollView2.titlesGroup = titles;
                self.CycleScrollView2.titlesGroup2 = titles2;
                
                [self.tableView reloadData];
            }
            
        }
        
      
    } failure:^(NSError *error) {
        
        NSLog(@"请求精选失败");
        
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    _leftImg.hidden = NO;
    
    self.navigationController.navigationBar.hidden = NO;
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
//    countLabel.text =[NSString stringWithFormat:@"%ld",unreadCount];
    if (unreadCount==0) {
        countLabel.hidden = YES;
    }else{
        countLabel.hidden = NO;
    }
    [self requestData5];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    _leftImg.hidden = YES;
    
    [super viewWillDisappear:animated];
    
    
    [_searchView endEditing:YES];//yu
    
}
-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

-(NSMutableArray *)goodsArr{
    if (!_goodsArr) {
        _goodsArr = [NSMutableArray array];
    }
    return _goodsArr;
}

-(NSMutableArray *)bottomAd_idArr{
    if (!_bottomAd_idArr) {
        _bottomAd_idArr = [NSMutableArray array];
    }
    return _bottomAd_idArr;
}

-(NSMutableArray *)bottomAd_imageArr{
    if (!_bottomAd_imageArr) {
        _bottomAd_imageArr = [NSMutableArray array];
    }
    return _bottomAd_imageArr;
}


-(NSMutableArray *)newsArr{
    if (!_newsArr) {
        _newsArr = [NSMutableArray array];
    }
    return _newsArr;
}
-(NSMutableArray *)DetailImageArr{
    if (!_DetailImageArr) {
        _DetailImageArr = [NSMutableArray array];
    }
    return _DetailImageArr;
}

-(NSMutableArray *)Ad_IDArr{
    if (!_Ad_IDArr) {
        _Ad_IDArr = [NSMutableArray array];
    }
    return _Ad_IDArr;
}

-(NSMutableArray *)Ad_imageArr{
    if (!_Ad_imageArr) {
        _Ad_imageArr = [NSMutableArray array];
    }
    return _Ad_imageArr;
}
-(UIView *)headerView{
    
    if (!_headerView) {
        
        UIView *headerView = [[UIView alloc]init];
        
        headerView.frame = CGRectMake(0, 0, YzWidth, YzWidth-YzWidth/4+10);
        //    广告

        SDCycleScrollView *scrollview = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzWidth/2)];
        
        scrollview.tag = 1000;
        
        scrollview.delegate = self;
        
//        scrollview.imageURLStringsGroup = self.Ad_imageArr;
        
        scrollview.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        
        scrollview.autoScrollTimeInterval = 4.0;
        
        scrollview.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
        
        [headerView addSubview:scrollview];
        
        self.CycleScrollView1 = scrollview;
        
        
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor whiteColor];
        [headerView addSubview:bgView];
        
        bgView.frame =CGRectMake(0, YzWidth/2, YzWidth, YzWidth/4+10);
        NSArray *imageNameArr = @[@"home_shangcheng",@"home_jifen",@"home_huiyuan",@"home_jiameng"];
        NSArray *titleStrArr = @[@"福百惠商城",@"银积分商城",@"搜索会员",@"加盟商家"];

        for (int i = 0; i < imageNameArr.count; i ++) {
            float XX = i%4;
            float YY = i/4;
            float intervalX = 30*Kwidth;
            float intervalY = 15*KHeight;
            float x = intervalX+XX*(intervalX+BtnWidth);
            float y = 15*KHeight + YY*(intervalY+BtnWidth+4*KHeight);
            float ly = y +BtnWidth;
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, BtnWidth, BtnWidth)];
            [btn setBackgroundImage:[UIImage imageNamed:imageNameArr[i]] forState:UIControlStateNormal];
            btn.tag = 100+i;
            [btn addTarget:self action:@selector(TapBtn:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(x, ly + 2, BtnWidth, 15*KHeight)];
            
            lable.text = titleStrArr[i];
            lable.font = [UIFont systemFontOfSize:11];
            lable.textColor = [UIColor colorWithRed:130/255.0f green:130/255.0f blue:130/255.0f alpha:1.0];
            lable.textAlignment = NSTextAlignmentCenter;
//            lable.adjustsFontSizeToFitWidth = YES;
            [bgView addSubview:lable];
            [bgView addSubview:btn];
        }
        
        self.headerView = headerView;
        
    }
    
    return _headerView;
}


-(UIView *)footerView{
    
    if (!_footerView) {

        _footerView = [[UIView alloc]init];
        _footerView.frame = CGRectMake(0, 0, YzWidth, YzWidth / 2 + YzWidth/8 + 40);

        UIView *FooterShopView = [[UIView alloc]init];
        FooterShopView.frame = CGRectMake(0, 0, YzWidth, YzWidth/2 +40);
        FooterShopView.backgroundColor = [UIColor whiteColor];
        [_footerView addSubview:FooterShopView];

        
        UILabel *betaLabel = [[UILabel alloc]init];
        betaLabel.frame = CGRectMake(YzWidth/2 -40, 0, 80, 40);
        betaLabel.textAlignment = NSTextAlignmentCenter;
        betaLabel.text = @" 平台精选 ";
        betaLabel.backgroundColor = [UIColor whiteColor];
        [FooterShopView addSubview:betaLabel];
        
        // 精选商品图片
        self.jingXuanView.frame = CGRectMake(0, 40, YzWidth, FooterShopView.frame.size.height - 40);
        [FooterShopView addSubview:self.jingXuanView];
        
        UILabel *firstL = [[UILabel alloc] init];
        
        firstL.textAlignment = NSTextAlignmentCenter;
        
        firstL.text = @"平台\n公告";
        
        firstL.numberOfLines = 2;
        
        //        firstL.textColor = [UIColor redColor];
        
        firstL.font = [UIFont systemFontOfSize:15*KHeight];
        
        [_footerView addSubview:firstL];
        
        firstL.frame = CGRectMake(0, YzWidth/2 +40, YzWidth/6, YzWidth/8);
        
        SDCycleScrollView *scrollview = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(YzWidth/6, YzWidth/2 +40, YzWidth, YzWidth/8)];
        scrollview.tag = 2000;
        scrollview.delegate = self;
        scrollview.scrollDirection = UICollectionViewScrollDirectionVertical;
        scrollview.onlyDisplayText = YES;
        scrollview.titleLabelBackgroundColor = [UIColor clearColor];
        //        scrollview.titleLabelTextColor = [UIColor orangeColor];
        [_footerView addSubview:scrollview];
        self.CycleScrollView2 = scrollview;
        
    }

    return _footerView;
}

- (YzHomeJingXuanView *)jingXuanView{
    if (!_jingXuanView) {
        _jingXuanView  = [[YzHomeJingXuanView alloc]init];
        _jingXuanView.delegate = self;
    }
    
    return _jingXuanView;
}

// 平台精选跳转到商品详情
- (void)pushGoodsViewWithGoodsID:(BottomGood *)goodsId{
    if ([VerifyPhoneNumber isBlankString:goodsId.tzurl]) {
        
        [MBProgressHUD showSuccess:@"商品不存在" toView:self.view];
        
        return;
    }
    NSLog(@"\n\n\nn\n   model.tzurl    %@",goodsId.tzurl);
    [self requestDetailDataWith:goodsId.tzurl];
}


-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    
    switch (cycleScrollView.tag) {
        case 1000:
        {
            NSString *Ad_ID = self.Ad_IDArr[index];
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
            adVC.Ad_ID = Ad_ID;
            [self.navigationController pushViewController:adVC animated:YES];
  
        }
            break;
        case 2000:
        {
            NewModel *model = self.newsArr[index];
            
            NSString *Ad_ID = [NSString stringWithFormat:@"/Admin/Public/impublic/id/%@/type/4", model.New_id];
            
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
            
            adVC.Ad_ID = Ad_ID;
            adVC.title = model.title;
            [self.navigationController pushViewController:adVC animated:YES];

        }
            break;
        case 3000:
        {
            NSString *AD_ID = self.bottomAd_idArr[index];
            
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
            
            adVC.Ad_ID = AD_ID;
            
            [self.navigationController pushViewController:adVC animated:YES];
        }
            break;
        default:
            break;
    }
  
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

//点击中间按钮
-(void)TapBtn:(UIButton *)sender{
    switch (sender.tag) {
        case 100:{
            NSLog(@"福百惠商城");
            DriverShopViewController *DsVC = [[DriverShopViewController alloc] init];
            DsVC.ShopType = 0;
            [self.navigationController pushViewController:DsVC animated:YES];
        }
            break;
        case 101:{
            NSLog(@"银积分商城");
//            DriverShopViewController *DsVC = [[DriverShopViewController alloc] init];
//            DsVC.ShopType = 1;
//            [self.navigationController pushViewController:DsVC animated:YES];
            YYYJoinShopViewController *joinVC = [[YYYJoinShopViewController alloc] init];
            joinVC.shopType = 1;
            joinVC.title = @"银积分商家";
            joinVC.isFromSearch = NO;
            [self.navigationController pushViewController:joinVC animated:YES];
        }
            break;
        case 102:{
//            if (DriverISLogIN) {
//                YzSeachLove *vc = [[YzSeachLove alloc] init];
////                YzCommentVC *vc = [[YzCommentVC alloc]init];
//                [self.navigationController pushViewController:vc animated:YES];
//            }else{
//                LoginViewController *vc = [[LoginViewController alloc]init];
//                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
//                [self presentViewController:NaVC animated:YES completion:nil];
//            }
            
            if (DriverISLogIN) {
                AddFriendViewController *addController = [[AddFriendViewController alloc] init];
                [self.navigationController pushViewController:addController animated:YES];
                
            }else{
                
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                
            }

        }
            
            break;
        case 103:{
            
            //yu
            YYYJoinShopViewController *joinVC = [[YYYJoinShopViewController alloc] init];
            joinVC.isFromSearch = NO;
            joinVC.shopType = 4;
            joinVC.title = @"加盟商家";
            [self.navigationController pushViewController:joinVC animated:YES];
        }
            break;
        default:
            break;
    }
}
//热点购
-(void)GotoLoveShop{
    LoveShopViewController *loveShopVC = [[LoveShopViewController alloc] init];
    [self.navigationController pushViewController:loveShopVC animated:YES];
}

//获取商品详情页面数据
-(void)requestDetailDataWith:(NSString *)goodsID{
    
    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
    goodDetailVC.goodsID = goodsID;
    goodDetailVC.title = @"福百惠商城";
    [self.navigationController pushViewController:goodDetailVC animated:YES];
}
-(void)clickMessageBtn{
    ConversationListController *chatListVC = [[ConversationListController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:chatListVC animated:YES];
}

-(void)clickSearchBtn{
    
    if (_searchView.textF.text.length > 0) {
        
        //
        //yu 首页头部搜索事件 以前搜出来的是商品 现在改成 搜出来的是商铺
        //
        
        [self.searchView.textF resignFirstResponder];
//        SearchResultViewController *searchResultVC = [[SearchResultViewController alloc] init];
//        searchResultVC.keyWord = _searchView.textF.text;
//        searchResultVC.shopType = 1;
//        [self.navigationController pushViewController:searchResultVC animated:YES];
        
        YYYJoinShopViewController *joinVC = [[YYYJoinShopViewController alloc] init];
        joinVC.isFromSearch = YES;
        joinVC.shopType = 4;
        joinVC.searchText = _searchView.textF.text;
        joinVC.title = @"加盟商家";
        [self.navigationController pushViewController:joinVC animated:YES];
        
//        YMRootMapViewController *mapVC = [[YMRootMapViewController alloc]init];
//        [self.navigationController pushViewController:mapVC animated:YES];
        
        
        
    }else{
        
        [self.searchView.textF resignFirstResponder];
        [MBProgressHUD showSuccess:@"请先输入关键字" toView:self.view];
        
    }
    
}
//yu点击键盘的搜索事件
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self clickSearchBtn];
    
    return YES;
}


@end
