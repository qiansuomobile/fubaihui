//
//  YMPoi.h
//  BaiduMapDemo
//
//  Created by 杨蒙 on 16/6/19.
//  Copyright © 2016年 hrscy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMPoi : NSObject

/** 地区名称*/
@property (nonatomic, copy) NSString *address_detail;
/** 平均价格*/
@property (nonatomic, copy) NSString *avgPrice;
/** 评分*/
@property (nonatomic, copy) NSString *avgScore;
/** 标签*/
@property (nonatomic, copy) NSString *content;
/** */
@property (nonatomic, copy) NSString *phone;
/** */
@property (nonatomic, copy) NSString *distance;
/** */
@property (nonatomic, copy) NSString *pic;
/** 纬度*/
@property (nonatomic, assign) double latitude;
/** 经度*/
@property (nonatomic, assign) double longitude;
/** 店名*/
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *screen;



@end
