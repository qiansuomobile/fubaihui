//
//  YMMapTableViewCell.h
//  ASJ
//
//  Created by Ss H on 2018/3/26.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMMapTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;
@property (weak, nonatomic) IBOutlet UILabel *mapTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mapJlLabel;
@property (weak, nonatomic) IBOutlet UILabel *mapAressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;

@end
