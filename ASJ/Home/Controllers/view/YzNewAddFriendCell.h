//
//  YzNewAddFriendCell.h
//  ASJ
//
//  Created by a1 on 2019/5/28.
//  Copyright © 2019年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YzNewAddFriendCell : UITableViewCell

@property (strong, nonatomic)  UIImageView *mapImageView;

@property (strong, nonatomic)  UILabel *mapTitleLabel;

@property (strong, nonatomic)  UILabel *mapJlLabel;

@property (strong, nonatomic)  UILabel *mapAressLabel;

@property (strong, nonatomic)  UILabel *phoneNumber;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end


