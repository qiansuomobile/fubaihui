//
//  YzNewAddFriendCell.m
//  ASJ
//
//  Created by a1 on 2019/5/28.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "YzNewAddFriendCell.h"


@interface YzNewAddFriendCell ()

@property (nonatomic , strong) UIImageView *SilverLabel;

@property (nonatomic , strong) UILabel *SumLabel;

@property (nonatomic , strong) UILabel *ageLabel;

@property (nonatomic , strong) UILabel *aeraLabel;

@property (nonatomic , strong) UILabel *LookLabel;

@end

@implementation YzNewAddFriendCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = RGBColor(245, 245, 245);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"YzNewAddFriendCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

- (void)setUpSubview{
    
    
    UIView *baseView = [[UIView alloc] init];
    baseView.backgroundColor = [UIColor whiteColor];
    baseView.frame   = CGRectMake(10, 5, YzWidth-20, YzWidth/4);
    [self addSubview:baseView];
    baseView.layer.cornerRadius = 10;
    
    
    //yu图片
    UIImageView *imageV = [[UIImageView alloc]init];
    [imageV.layer setMasksToBounds:YES];
    [imageV.layer setCornerRadius:10.0];
    [baseView addSubview:imageV];
    imageV.frame = CGRectMake(6, 6, YzWidth/4- 6*2, YzWidth/4- 6*2);
    _mapImageView = imageV;
    
    
    //左上文字
    UILabel *date2 = [[UILabel alloc]init];
    date2.font = [UIFont systemFontOfSize:14];
    date2.textAlignment = NSTextAlignmentLeft;
    date2.numberOfLines = 2;
    date2.frame = CGRectMake(imageV.right + 5, 5, YzWidth - YzWidth/4 - 24 , 40);
    [baseView addSubview:date2];
    _mapTitleLabel = date2;
    
    //左下文字
    UILabel *one = [[UILabel alloc]init];
    one.font = [UIFont systemFontOfSize:12];
    one.textAlignment = NSTextAlignmentLeft;
    one.frame = CGRectMake(imageV.right +5, YzWidth/12+5, YzWidth/2, YzWidth/12);
    [baseView addSubview:one];
    _phoneNumber = one;
    _phoneNumber.textColor = [UIColor grayColor];
    
    
    //右下文字
    UILabel *two = [[UILabel alloc]init];
    two.font = [UIFont systemFontOfSize:12];
    two.textAlignment = NSTextAlignmentRight;
    two.frame = CGRectMake(YzWidth/2, YzWidth/6, YzWidth/2-20, YzWidth/12);
    two.right = baseView.width - 6;
    [baseView addSubview:two];
    _mapJlLabel = two;
    _mapJlLabel.textColor = [UIColor grayColor];
    
    

}


@end
