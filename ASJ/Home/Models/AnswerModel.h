//
//  AnswerModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/11.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnswerModel : NSObject

@property (nonatomic,copy)NSString *answer_id;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *uid;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *create_time;
@property (nonatomic,copy)NSString *update_time;
@property (nonatomic,copy)NSString *type_id;
@property (nonatomic,copy)NSString *price;
@property (nonatomic,copy)NSString *is_sticky;
@property (nonatomic,copy)NSString *is_cnyj;
@property (nonatomic,copy)NSString *num;

@end
