//
//  AnswerModel.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/11.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AnswerModel.h"

@implementation AnswerModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]){
        self.answer_id = value;
    }

}
@end
