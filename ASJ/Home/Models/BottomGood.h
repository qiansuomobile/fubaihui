//
//  BottomGood.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/17.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BottomGood : NSObject
@property (nonatomic,copy) NSString *goodID;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *catewz;
@property (nonatomic,copy) NSString *picture;
@property (nonatomic,copy) NSString *brief;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *tzurl;
@property (nonatomic,copy) NSString *status;

@property (nonatomic,copy) NSString *path;
@end
