//
//  CategroyGoods.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategroyGoods : NSObject
@property (nonatomic,strong) NSString *goodID;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *pid;
@property (nonatomic,strong) NSString *sort;
@property (nonatomic,strong) NSString *list_row;
@property (nonatomic,strong) NSString *meta_title;
@property (nonatomic,strong) NSString *keywords;
@property (nonatomic,strong) NSString *desc;
@property (nonatomic,strong) NSString *template_index;
@property (nonatomic,strong) NSString *template_lists;
@property (nonatomic,strong) NSString *template_detail;
@property (nonatomic,strong) NSString *template_edit;
@property (nonatomic,strong) NSString *model;
@property (nonatomic,strong) NSString *model_sub;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *link_id;
@property (nonatomic,strong) NSString *allow_publish;
@property (nonatomic,strong) NSString *display;
@property (nonatomic,strong) NSString *reply;
@property (nonatomic,strong) NSString *check;
@property (nonatomic,strong) NSString *reply_model;
@property (nonatomic,strong) NSString *extend;
@property (nonatomic,strong) NSString *create_time;
@property (nonatomic,strong) NSString *update_time;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *icon;
@property (nonatomic,strong) NSArray *_child;
@end
