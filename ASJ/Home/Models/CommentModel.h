//
//  CommentModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject
@property (nonatomic,copy) NSString *commentID;
@property (nonatomic,copy) NSString *user_id;
@property (nonatomic,copy) NSString *user_name;
@property (nonatomic,copy) NSString *g_id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *ctime;
@property (nonatomic,copy) NSString *starts;

-(CGFloat)CaculateCellHeight:(NSString *)contentStr;

@end


