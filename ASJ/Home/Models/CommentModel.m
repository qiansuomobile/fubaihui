//
//  CommentModel.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]){
        self.commentID = value;
    }

}

-(CGFloat)CaculateCellHeight:(NSString *)contentStr{
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont systemFontOfSize:12*KHeight],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize contentSize = [contentStr boundingRectWithSize:CGSizeMake(MainScreen.width-35*Kwidth, MAXFLOAT)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    return contentSize.height+200*KHeight;
}


@end
