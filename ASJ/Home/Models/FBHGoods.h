//
//  FBHGoods.h
//  ASJ
//
//  Created by Dororo on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBHGoods : NSObject

//商品id
@property (nonatomic,copy) NSString *goodsID;
//商品名称
@property (nonatomic,copy) NSString *title;
//商品描述
@property (nonatomic,copy) NSString *desc;
//商品详情
@property (nonatomic,copy) NSString *content;
//商品原价
@property (nonatomic,copy) NSString *price_sc;
//商品价格
@property (nonatomic,copy) NSString *price;
//库存
@property (nonatomic,copy) NSString *number;
//封面图
@property (nonatomic,copy) NSString *cover;
//金积分
@property (nonatomic,copy) NSString *sorts;
//银积分
@property (nonatomic,copy) NSString *silver;
//多少人购买
@property (nonatomic,copy) NSString *people;
//商品组图
@property (nonatomic,copy) NSArray *images;
//是否预约
@property (nonatomic,copy) NSString *reservation;
//预约时间点
@property (nonatomic,copy) NSArray *timeList;
//预约日期
@property (nonatomic,copy) NSString *date;
@end

NS_ASSUME_NONNULL_END
