//
//  FBHGoods.m
//  ASJ
//
//  Created by Dororo on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import "FBHGoods.h"

@implementation FBHGoods
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.desc = value;
    }
    if ([key isEqualToString:@"id"]){
        self.goodsID = value;
    }
}
@end
