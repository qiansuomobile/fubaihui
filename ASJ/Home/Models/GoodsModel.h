//
//  GoodsModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsModel : NSObject
@property (nonatomic,strong) NSString *goodID;
@property (nonatomic,strong) NSString *category;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *uid;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *number;
@property (nonatomic,strong) NSArray *cover;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *temp;
@property (nonatomic,strong) NSString *desc;
@property (nonatomic,strong) NSString *keywords;
@property (nonatomic,strong) NSString *tags;
@property (nonatomic,strong) NSString *supid;
@property (nonatomic,strong) NSString *maxchit;
@property (nonatomic,strong) NSString *position;
@property (nonatomic,strong) NSString *create_time;
@property (nonatomic,strong) NSString *update_time;
@property (nonatomic,strong) NSString *carrmb;
@property (nonatomic,strong) NSString *price_sc;
@property (nonatomic,strong) NSString *orid;
@property (nonatomic,strong) NSString *people;
@property (nonatomic,strong) NSString *picture;
@property (nonatomic,strong) NSString *f_sorts;
@property (nonatomic,strong) NSString *f_silver;
@property (nonatomic,strong) NSString *numberone;
@property (nonatomic,strong) NSString *qg_time;
@end
