//
//  GoodsModel.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "GoodsModel.h"

@implementation GoodsModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.desc = value;
    }
    if ([key isEqualToString:@"id"]){
        self.goodID = value;
    }
    if ([key isEqualToString:@"template"]) {
        self.temp = value;
    }
}
@end
