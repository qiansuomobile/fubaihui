//
//  NewModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewModel : NSObject
@property (nonatomic,copy) NSString *New_id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *catewz;
@property (nonatomic,copy) NSString *picture;
@property (nonatomic,copy) NSString *brief;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *tzurl;
@property (nonatomic,copy) NSString *status;
@end
