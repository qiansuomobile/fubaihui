//
//  QuestionModel.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "QuestionModel.h"

@implementation QuestionModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]){
        self.question_id = value;
    }
}


-(CGFloat)CaculateHeight:(NSString *)contentStr{
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont systemFontOfSize:18*KHeight],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize contentSize = [contentStr boundingRectWithSize:CGSizeMake(MainScreen.width-20*Kwidth, MAXFLOAT)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    return contentSize.height+100*KHeight;
}

-(CGFloat)CaculateCellHeight:(NSString *)contentStr{
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont systemFontOfSize:18*KHeight],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize contentSize = [contentStr boundingRectWithSize:CGSizeMake(MainScreen.width-20*Kwidth, MAXFLOAT)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    return contentSize.height+70*KHeight;
}

@end
