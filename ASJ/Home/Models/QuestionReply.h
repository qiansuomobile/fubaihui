//
//  QuestionReply.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionReply : NSObject
@property (nonatomic,copy) NSString *reply_id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *pid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *create_time;
@property (nonatomic,copy) NSString *top;
@property (nonatomic,copy) NSString *shit;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headpic;

-(CGFloat)CaculateCellHeight:(NSString *)contentStr;

@end
