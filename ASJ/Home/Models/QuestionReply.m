//
//  QuestionReply.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "QuestionReply.h"

@implementation QuestionReply
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]){
        self.reply_id = value;
    }

}

-(CGFloat)CaculateCellHeight:(NSString *)contentStr{
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20*KHeight],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize contentSize = [contentStr boundingRectWithSize:CGSizeMake(MainScreen.width-40*Kwidth-80*KHeight, MAXFLOAT)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    return contentSize.height+75*KHeight+20*Kwidth;
}



@end
