//
//  QuestionType.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionType : NSObject
@property (nonatomic,copy) NSString *TypeID;
@property (nonatomic,copy) NSString *title;
@end
