//
//  QuestionType.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "QuestionType.h"

@implementation QuestionType
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]){
        self.TypeID = value;
    }

}
@end
