//
//  SortGoods.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/4.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortGoods : NSObject
@property (nonatomic,copy) NSString *goodID;
@property (nonatomic,copy) NSString *category;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *number;
@property (nonatomic,strong) NSArray *cover;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *temp;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *keywords;
@property (nonatomic,copy) NSString *tags;
@property (nonatomic,copy) NSString *supid;
@property (nonatomic,copy) NSString *maxchit;
@property (nonatomic,copy) NSString *position;
@property (nonatomic,copy) NSString *create_time;
@property (nonatomic,copy) NSString *update_time;
@property (nonatomic,copy) NSString *carrmb;
@property (nonatomic,copy) NSString *price_sc;
@property (nonatomic,copy) NSString *orid;
@property (nonatomic,copy) NSString *people;
@property (nonatomic,copy) NSString *picture;
@property (nonatomic,copy) NSString *f_sorts;
@property (nonatomic,copy) NSString *f_silver;
@property (nonatomic,copy) NSString *numberone;
@property (nonatomic,copy) NSString *qg_time;
@property (nonatomic,copy) NSString *num;
@property (nonatomic,copy) NSString *with;
@property (nonatomic, copy) NSString *pic;

@property (nonatomic, copy) NSString *GodenType;

@end
