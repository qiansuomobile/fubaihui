//
//  SortGoods.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/4.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SortGoods.h"

@implementation SortGoods
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.desc = value;
    }
    if ([key isEqualToString:@"id"]){
        self.goodID = value;
    }
    if ([key isEqualToString:@"template"]) {
        self.temp = value;
    }
    if ([key isEqualToString:@"silver"]) {
        self.f_silver = value;
    }
}
@end
