//
//  AdOneTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderNewsView.h"
#import "NewModel.h"

@interface AdOneTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) NSMutableArray *newsArr;
@property (nonatomic,strong) NSMutableArray *btnArr;
@end
