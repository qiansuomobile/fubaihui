//
//  AdOneTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AdOneTableViewCell.h"

@implementation AdOneTableViewCell

-(void)setNewsArr:(NSMutableArray *)newsArr{
   
    self.btnArr = [NSMutableArray array];
    
    for (int i = 0;  i < newsArr.count; i ++) {
    
        HeaderNewsView *newsView = [[HeaderNewsView alloc] initWithFrame:CGRectMake(0, i*80*KHeight, MainScreen.width-56*Kwidth, 80*KHeight)];
        
        NewModel *model = newsArr[i];
        
        newsView.firstNewsL.text = model.title;
        
        newsView.secondNewsL.text = model.brief;
        
        [_scrollView addSubview:newsView];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, i*80*KHeight, MainScreen.width-56*Kwidth, 80*KHeight)];
        
        [btn setBackgroundColor:[UIColor clearColor]];
        
        [_scrollView addSubview:btn];
        
        [self.btnArr addObject:btn];
        
    }
}


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guanggao"]];
    
    [self addSubview:_imageV];
    
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(self.mas_left).offset(0);
        
        make.right.equalTo(self.mas_right).offset(0);
        
        make.top.equalTo(self.mas_top).offset(0);
        
        make.height.mas_equalTo(60*KHeight);
    
    }];
    
    UILabel *firstL = [[UILabel alloc] init];
    
    firstL.textAlignment = NSTextAlignmentRight;
    
    firstL.text = @"公告";
    
    firstL.numberOfLines = 2;
    
    firstL.textColor = [UIColor redColor];
    
    firstL.font = [UIFont systemFontOfSize:20*KHeight];
    
    [self addSubview:firstL];
    
    [firstL mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(self.mas_left).offset(20*Kwidth);
        
        make.top.equalTo(_imageV.mas_bottom).offset(0);
        
        make.bottom.equalTo(self.mas_bottom).offset(0);
        
        make.width.mas_equalTo(40*Kwidth);
    
    }];
    
    firstL.adjustsFontSizeToFitWidth = YES;
    
    UIView *lineView = [[UIView alloc] init];
    
    lineView.backgroundColor = [UIColor grayColor];
    
    [self addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(firstL.mas_right).offset(10*Kwidth);
        
        make.top.equalTo(_imageV.mas_bottom).offset(10);
        
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        
        make.width.mas_equalTo(1*Kwidth);
    
    }];
    
    
    _scrollView = [[UIScrollView alloc] init];
    
    _scrollView.backgroundColor = [UIColor clearColor];
    
    _scrollView.scrollEnabled = NO;
    
    [self addSubview:_scrollView];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(lineView.mas_right).offset(0);
        
        make.top.equalTo(_imageV.mas_bottom).offset(0);
        
        make.right.equalTo(self.mas_right).offset(0);
        
        make.bottom.equalTo(self.mas_bottom).offset(0);
    
    }];
    
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
