//
//  AdThreeTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AdThreeTableViewCell.h"

@implementation AdThreeTableViewCell

-(NSMutableArray *)btnArr{
    if (!_btnArr) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}

-(void)setModelArr:(NSMutableArray *)modelArr{

    for (int i = 0; i < modelArr.count; i ++) {
        
        UIButton *btn = self.btnArr[i];
        BottomGood *model = modelArr[i];
        NSLog(@"执行~~~~~~~~~~~~~~~~~~~~~~:%@",model.picture);
        UIImageView *image1 = [[UIImageView alloc] init];
        [image1 sd_setImageWithURL:[NSURL URLWithString:model.picture] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            NSLog(@"image:%@",image);
        }];
        
        [btn setBackgroundImage:image1.image forState:UIControlStateNormal];
        }
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    
    [self.btnArr removeAllObjects];
    
    self.backgroundColor = [UIColor whiteColor];
    UILabel *titleL = [[UILabel alloc] init];
    titleL.backgroundColor = [UIColor whiteColor];
    titleL.text = @"精选";
    titleL.textAlignment = NSTextAlignmentCenter;
    titleL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.mas_equalTo(40*KHeight);
    }];
    //图片1
    _btn1 = [[UIButton alloc] init];
    [_btn1 setBackgroundImage:[UIImage imageNamed:@"chanpin1"] forState:UIControlStateNormal];
    [self addSubview:_btn1];
    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.top.equalTo(titleL.mas_bottom).offset(1);
        make.width.mas_equalTo(MainScreen.width/2-1);
        make.height.mas_equalTo(KHeight*100);
    }];
    [self.btnArr addObject:_btn1];
    
    //图片2
    _btn2 = [[UIButton alloc] init];
    [_btn2 setBackgroundImage:[UIImage imageNamed:@"chanpin2"] forState:UIControlStateNormal];
    [self addSubview:_btn2];
    [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_btn1.mas_right).offset(1);
        make.top.equalTo(titleL.mas_bottom).offset(1);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.mas_equalTo(KHeight*100);
    }];
    [self.btnArr addObject:_btn2];
    
    //图片3
    _btn3 = [[UIButton alloc] init];
    [_btn3 setBackgroundImage:[UIImage imageNamed:@"chanpin3"] forState:UIControlStateNormal];
    [self addSubview:_btn3];
    [_btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.top.equalTo(_btn1.mas_bottom).offset(1);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo((MainScreen.width-3)/4);
    }];
    [self.btnArr addObject:_btn3];
    
    //图片4
    _btn4 = [[UIButton alloc] init];
    [_btn4 setBackgroundImage:[UIImage imageNamed:@"chanpin3"] forState:UIControlStateNormal];
    [self addSubview:_btn4];
    [_btn4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_btn3.mas_right).offset(1);
        make.top.equalTo(_btn1.mas_bottom).offset(1);
        make.right.equalTo(_btn1.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    [self.btnArr addObject:_btn4];
    
    //图片5
    _btn5 = [[UIButton alloc] init];
    [_btn5 setBackgroundImage:[UIImage imageNamed:@"chanpin3"] forState:UIControlStateNormal];
    [self addSubview:_btn5];
    [_btn5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_btn2.mas_left).offset(0);
        make.top.equalTo(_btn1.mas_bottom).offset(1);
        make.width.mas_equalTo((MainScreen.width-3)/4);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    [self.btnArr addObject:_btn5];
    
    //图片6
    _btn6 = [[UIButton alloc] init];
    [_btn6 setBackgroundImage:[UIImage imageNamed:@"chanpin3"] forState:UIControlStateNormal];
    [self addSubview:_btn6];
    [_btn6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_btn5.mas_right).offset(1);
        make.top.equalTo(_btn1.mas_bottom).offset(1);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    [self.btnArr addObject:_btn6];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
