//
//  AdTwoTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdTwoTableViewCell : UITableViewCell
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UIPageControl *pageControl;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,strong) NSMutableArray *btnArr;
@property (nonatomic,strong) NSTimer *advertScrollViewTime;


+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
