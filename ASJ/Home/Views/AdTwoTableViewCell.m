//
//  AdTwoTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AdTwoTableViewCell.h"


@interface AdTwoTableViewCell()<UIScrollViewDelegate>

@property (nonatomic, assign) NSTimeInterval timeInterval;

@end

static NSInteger x;

@implementation AdTwoTableViewCell




-(void)setImageArr:(NSMutableArray *)imageArr{
   
    self.btnArr = [NSMutableArray array];
   
    for (int i = 0 ; i < imageArr.count; i ++) {
    
        UIImageView *imageView = [[UIImageView alloc] init];
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageArr[i]]];
        
        imageView.frame = CGRectMake(i*MainScreen.width, 0, MainScreen.width, KHeight*90);
        
        [_scrollView addSubview:imageView];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*MainScreen.width, 0, MainScreen.width, KHeight*90)];
        
        btn.backgroundColor = [UIColor clearColor];
        
        [self.btnArr addObject:btn];
        
        [_scrollView addSubview:btn];
        
    }
    
    //    点点
    _pageControl = [[UIPageControl alloc] init];
    
    _pageControl.numberOfPages = imageArr.count;
    
    _pageControl.currentPage = 0;
    
    _pageControl.selected = YES;
    
    _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    
    _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    
    [self addSubview:_pageControl];
    
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.mas_left).offset(0);
        
        make.right.equalTo(self.mas_right).offset(0);
        
        make.bottom.equalTo(self.mas_bottom).offset(0);
        
        make.height.mas_equalTo(20*KHeight);
        
    }];

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
       
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self createUI];
        
        
        x = 0;
        //
        
        NSLog(@"  添加 轮播*****************");

        [self addTime];

    }
    return self;
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cell2";
    
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell){
        
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    
    return cell;
    
}



-(void)addTime{
   
    _advertScrollViewTime = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(autoScrollPage1) userInfo:nil repeats:YES];


   
//   _advertScrollViewTime =  [NSTimer scheduledTimerWithTimeInterval:3 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        
//        [self autoScrollPage1];
//        
//    }];

}




-(void)autoScrollPage1{
    
    if (_pageControl.currentPage ==2 ) {
     
        x =  1;
    }
    
    
    if (x ==  0){
        
        _pageControl.currentPage++;
        
        if (_pageControl.currentPage==2){
            
//            _pageControl.currentPage = ;
        
            x = 1;
            
        }
        
    }else{
        
        _pageControl.currentPage--;
        
        if (_pageControl.currentPage == 0){
            
            x = 0;
            
        }
    }
    
//       NSLog(@" 第  %ld 页",(long)_pageControl.currentPage);
    
    [_pageControl setCurrentPage:_pageControl.currentPage];
    
    [_scrollView setContentOffset:CGPointMake(YzWidth*_pageControl.currentPage, 0) animated:YES];
    
    
    
    
    

}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
  
    
    NSInteger x = scrollView.contentOffset.x/YzWidth;
    
    [_pageControl setCurrentPage:x];

    
}

-(void)createUI{
    
    _scrollView =  [[UIScrollView alloc] init];

    
//    CGFloat width = self.frame.size.width;
//    
//    CGFloat height = self.frame.size.height;

    
    _scrollView.scrollsToTop = NO;
    
    _scrollView.contentSize = CGSizeMake(3*YzWidth, 0);
    
//    _scrollView.contentOffset = CGPointMake(YzWidth, 0);
    
//    
//    _scrollView.contentSize = CGSizeMake(4*width, height);
//    
//    _scrollView.contentOffset = CGPointMake(width, 0);


    _scrollView.delegate = self;

    _scrollView.pagingEnabled = YES;
    
    [self addSubview:_scrollView];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    
    }];
    
}

- (void)toPlay {
    
    [self performSelector:@selector(autoPlayToNextPage) withObject:nil
               afterDelay:_timeInterval];
}

- (void)autoPlayToNextPage {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(autoPlayToNextPage) object:nil];
    
    [self.scrollView setContentOffset:CGPointMake(self.frame.size.width * 2, 0) animated:YES];
    
    [self performSelector:@selector(autoPlayToNextPage) withObject:nil afterDelay:_timeInterval];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [scrollView setContentOffset:CGPointMake(self.frame.size.width, 0) animated:YES];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
