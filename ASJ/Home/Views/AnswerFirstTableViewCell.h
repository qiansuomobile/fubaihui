//
//  AnswerFirstTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/31.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"


@interface AnswerFirstTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *contentL;
@property (nonatomic,strong) UILabel *rewardL;
@property (nonatomic,strong) UILabel *timeL;
@property (nonatomic,strong) UIButton *logoBtn;
@property (nonatomic,strong) UILabel *phoneL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) UIButton *answerBtn;
@property (nonatomic,strong) QuestionModel *questionModel;
@end
