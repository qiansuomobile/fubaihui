//
//  AnswerFirstTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/31.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AnswerFirstTableViewCell.h"

@implementation AnswerFirstTableViewCell

-(void)setQuestionModel:(QuestionModel *)questionModel{
    _contentL.text = questionModel.title;
    _rewardL.text = [NSString stringWithFormat:@"%@/银A币",questionModel.price];
    _timeL.text = questionModel.create_time;
    UIImageView *logoImage = [[UIImageView alloc] init];
    [logoImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.woaisiji.com/%@",questionModel.headpic]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_logoBtn setBackgroundImage:image forState:UIControlStateNormal];
    }];
    _phoneL.text = questionModel.phone;
    _countL.text = [NSString stringWithFormat:@"%@人回答",questionModel.num];
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _contentL = [[UILabel alloc] init];
    _contentL.text = @"司机司机司机司机,司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机司机!司机司机司机司机司机司机?";
    _contentL.lineBreakMode = NSLineBreakByWordWrapping;
    _contentL.numberOfLines = 0;
    _contentL.font = [UIFont systemFontOfSize:18*KHeight];
    _contentL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_contentL];
    [_contentL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
    }];
    
    _logoBtn = [[UIButton alloc] init];
    [_logoBtn setBackgroundImage:[UIImage imageNamed:@"userLogo"] forState:UIControlStateNormal];
    _logoBtn.layer.masksToBounds = YES;
    _logoBtn.layer.cornerRadius = 12*Kwidth;
    [self addSubview:_logoBtn];
    [_logoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.width.mas_equalTo(25*Kwidth);
        make.height.mas_equalTo(25*Kwidth);
    }];
    
    _phoneL = [[UILabel alloc] init];
    _phoneL.text = @"188*****362";
    _phoneL.textColor = RGBColor(195, 195, 195);
    _phoneL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_phoneL];
    [_phoneL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_logoBtn.mas_right).offset(5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-12*KHeight);
    }];
    
    UILabel *answerL = [[UILabel alloc] init];
    answerL.text = @"回答";
    answerL.textColor = RGBColor(72, 223, 123);
    answerL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:answerL];
    [answerL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-12*KHeight);
    }];
    
    _answerBtn = [[UIButton alloc] init];
    [_answerBtn setBackgroundImage:[UIImage imageNamed:@"huida"] forState:UIControlStateNormal];
    [self addSubview:_answerBtn];
    [_answerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(answerL.mas_left).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-14*KHeight);
        make.width.mas_equalTo(19*Kwidth);
        make.height.mas_equalTo(19*Kwidth);
    }];
    
    UIView *secondsView = [[UIView alloc] init];
    secondsView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:secondsView];
    [secondsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_answerBtn.mas_left).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-12*KHeight);
        make.width.mas_equalTo(1*Kwidth);
        make.height.mas_equalTo(25*KHeight);
    }];
    
    _countL = [[UILabel alloc] init];
    _countL.text = @"0人回答";
    _countL.textAlignment = NSTextAlignmentRight;
    _countL.textColor = RGBColor(140, 140, 140);
    _countL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(secondsView.mas_left).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-12*KHeight);
    }];

    UIImageView *rewardImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qian"]];
    [self addSubview:rewardImage];
    [rewardImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(_logoBtn.mas_top).offset(-10*KHeight);
        make.height.mas_equalTo(20*Kwidth);
        make.width.mas_equalTo(20*Kwidth);
    }];
    
    _rewardL = [[UILabel alloc] init];
    _rewardL.text = @"100/银A币";
    _rewardL.font = [UIFont systemFontOfSize:20*KHeight];
    _rewardL.textColor = RGBColor(248, 106, 0);
    [self addSubview:_rewardL];
    [_rewardL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(rewardImage.mas_right).offset(3*Kwidth);
        make.bottom.equalTo(_logoBtn.mas_top).offset(-10*KHeight);
    }];
    
    UIView *firstView = [[UIView alloc] init];
    firstView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:firstView];
    [firstView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rewardL.mas_right).offset(5*Kwidth);
        make.bottom.equalTo(_logoBtn.mas_top).offset(-10*KHeight);
        make.width.mas_equalTo(1*Kwidth);
        make.height.mas_equalTo(20*Kwidth);
    }];
    
    _timeL = [[UILabel alloc] init];
    _timeL.text = @"8分钟前";
    _timeL.font = [UIFont systemFontOfSize:20*KHeight];
    _timeL.textColor = RGBColor(171, 171, 171);
    [self addSubview:_timeL];
    [_timeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(firstView.mas_right).offset(3*Kwidth);
        make.bottom.equalTo(_logoBtn.mas_top).offset(-10*KHeight);
    }];
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
