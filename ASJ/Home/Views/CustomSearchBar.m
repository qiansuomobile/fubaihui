//
//  CustomSearchBar.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/7.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CustomSearchBar.h"

@implementation CustomSearchBar

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    [self setBackgroundColor:[UIColor clearColor]];
//    [self setBarTintColor:RGBColor(203, 51, 4)];
    self.barTintColor = [UIColor whiteColor];
    UITextField *searchTextField = [[[self.subviews firstObject] subviews] lastObject];
    searchTextField.backgroundColor = RGBColor(203, 51, 4);
    if (searchTextField)
    {
        UIView *searchIcon = searchTextField.leftView;
        if ([searchIcon isKindOfClass:[UIImageView class]]) {
            NSLog(@"aye");
        }
        searchTextField.rightView = searchIcon;
        searchTextField.leftViewMode = UITextFieldViewModeNever;
        searchTextField.rightViewMode = UITextFieldViewModeAlways;
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
