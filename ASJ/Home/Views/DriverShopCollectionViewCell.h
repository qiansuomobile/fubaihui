//
//  DriverShopCollectionViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortGoods.h"
@interface DriverShopCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *descripL;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) SortGoods *sortGood;

@end
