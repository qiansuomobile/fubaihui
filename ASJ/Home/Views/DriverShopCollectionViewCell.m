//
//  DriverShopCollectionViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DriverShopCollectionViewCell.h"

static NSString * const reuseIdentifier = @"YZCollectionCell";

@interface DriverShopCollectionViewCell ()

@end

@implementation DriverShopCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        _imageV = [[UIImageView alloc] init];
        _imageV.contentMode = UIViewContentModeScaleAspectFill;
        _imageV.clipsToBounds = YES;
        [self addSubview:_imageV];
        [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(0);
            make.top.equalTo(self.mas_top).offset(0);
            make.right.equalTo(self.mas_right).offset(0);
            make.height.mas_equalTo(KHeight*140);
        }];
        
        _descripL = [[UILabel alloc] init];
        _descripL.font = [UIFont systemFontOfSize:12*KHeight];
        _descripL.numberOfLines = 2;
        _descripL.textAlignment = NSTextAlignmentLeft;
        _descripL.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:_descripL];
        [_descripL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(5*Kwidth);
            make.top.equalTo(_imageV.mas_bottom).offset(0);
            make.right.equalTo(self.mas_right).offset(-5*Kwidth);
        }];
        
        _priceL = [[UILabel alloc] init];
        _priceL.font = [UIFont systemFontOfSize:12*KHeight];
        _priceL.textColor = [UIColor orangeColor];
        _priceL.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_priceL];
        [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(5*Kwidth);
            make.bottom.equalTo(self.mas_bottom).offset(-5*Kwidth);
        }];
        
        _countL = [[UILabel alloc] init];
        _countL.font = [UIFont systemFontOfSize:12*KHeight];
        _countL.textColor = [UIColor grayColor];
        _countL.textAlignment = NSTextAlignmentRight;
        [self addSubview:_countL];
        [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-5*Kwidth);
            make.bottom.equalTo(self.mas_bottom).offset(-5*Kwidth);
        }];
    }
    return self;
}

-(void)setSortGood:(SortGoods *)sortGood{
    if(sortGood.cover){
        [_imageV sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(sortGood.cover)] placeholderImage:[UIImage imageNamed:@"logo"]];
    }
    
    _descripL.text = sortGood.title;
    _priceL.text = [NSString stringWithFormat:@"¥%0.2f",[sortGood.price floatValue]];
    _countL.text = [NSString stringWithFormat:@"%@人付款",sortGood.people];
}


@end
