//
//  DriverShopTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverShopTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *lab;
@property (nonatomic,strong) NSString *titleStr;

@property (nonatomic, assign)BOOL cellSelect;
@end
