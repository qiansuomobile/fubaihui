//
//  DriverShopTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DriverShopTableViewCell.h"

@implementation DriverShopTableViewCell

-(void) setTitleStr:(NSString *)titleStr{
    _lab.text = titleStr;
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void) createUI{
    _lab = [[UILabel alloc]init];
    _lab.font = [UIFont systemFontOfSize:12.0f];
    _lab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_lab];
    [_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)setCellSelect:(BOOL)cellSelect{
    if (cellSelect) {
        _lab.textColor = [UIColor colorWithRed:207/255.0f green:156/255.0 blue:42/255.0f alpha:1.0];
        _lab.backgroundColor = [UIColor colorWithRed:223/255.0f green:223/255.0 blue:223/255.0f alpha:1.0];
    }else{
        _lab.textColor = [UIColor blackColor];
        _lab.backgroundColor = [UIColor whiteColor];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
