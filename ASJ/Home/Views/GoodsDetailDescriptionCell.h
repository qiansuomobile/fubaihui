//
//  GoodsDetailDescriptionCell.h
//  ASJ
//
//  Created by Dororo on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailDescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

NS_ASSUME_NONNULL_END
