//
//  GoodsDetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBHGoods.h"
#import "PKYStepper.h"
@interface GoodsDetailTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *titleL;
//@property (nonatomic,strong) UIButton *discountBtn;
@property (nonatomic,strong) UILabel *discountPriceL;

@property (nonatomic,strong) UILabel *numberLabel;
//@property (nonatomic,strong) UILabel *priceL;
//@property (nonatomic,strong) UILabel *transportL;
//@property (nonatomic,strong) UIButton *goldBtn;
//@property (nonatomic,strong) UIButton *silverBtn;
//模型类
@property (nonatomic,strong) FBHGoods *goods;
@property (nonatomic,strong) UILabel *RMBL;

@property (nonatomic , assign) NSInteger shopType;

@property (nonatomic,strong) PKYStepper *stepper;
@end
