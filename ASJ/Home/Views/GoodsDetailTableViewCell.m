//
//  GoodsDetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "GoodsDetailTableViewCell.h"

@implementation GoodsDetailTableViewCell

-(void)setGoods:(FBHGoods *)goods{
   
    _titleL.text = goods.title;
    if (_shopType == 1) {
        _discountPriceL.text = goods.silver;
    }else{
        _discountPriceL.text = goods.price;
    }
    
    _numberLabel.text = [NSString stringWithFormat:@"库存剩余数量:%@",goods.number];
}


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void) createUI{
    _titleL = [[UILabel alloc] init];
    NSString *titleStr = @"--";
    _titleL.text = titleStr;
    _titleL.font = [UIFont systemFontOfSize:20];
    _titleL.lineBreakMode = NSLineBreakByWordWrapping;
    _titleL.textAlignment = NSTextAlignmentLeft;
    _titleL.numberOfLines = 2;
//    _titleL.backgroundColor = [UIColor redColor];
//    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:titleStr];
//    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle1 setLineSpacing:5];
//    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [titleStr length])];
//    [_titleL setAttributedText:attributedString1];
    [_titleL sizeToFit];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top).offset(8);
    }];
    
    _RMBL = [[UILabel alloc] init];
    _RMBL.text = @"¥";
    _RMBL.textAlignment = NSTextAlignmentRight;
    _RMBL.textColor = RGBColor(252, 77, 0);
    _RMBL.font = [UIFont systemFontOfSize:12*KHeight];
    [self addSubview:_RMBL];
    [_RMBL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-25);
//        make.top.equalTo(_titleL.mas_bottom).offset(0);
    }];
    
    _discountPriceL = [[UILabel alloc] init];
    _discountPriceL.text = @"---";
    _discountPriceL.textAlignment = NSTextAlignmentLeft;
    _discountPriceL.textColor = RGBColor(252, 77, 0);
    _discountPriceL.font = [UIFont systemFontOfSize:28];
    [self addSubview:_discountPriceL];
    [_discountPriceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_RMBL.mas_right).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
//        make.top.equalTo(_titleL.mas_bottom).offset(0);
    }];
    
    _stepper = [[PKYStepper alloc] initWithFrame:CGRectMake(55*Kwidth, 10*KHeight, 120*Kwidth, 25*KHeight)];
    [_stepper setBorderColor:RGBColor(140, 140, 140)];
    [_stepper setLabelTextColor:[UIColor blackColor]];
    [_stepper setButtonTextColor:[UIColor blackColor] forState:UIControlStateNormal];
    _stepper.value = 1;
    _stepper.minimum = 1;
    self.stepper.countLabel.text = @"1";
    _stepper.valueChangedCallback = ^(PKYStepper *stepper, float count) {
        stepper.countLabel.text = [NSString stringWithFormat:@"%d", (int)count];
    };
    [self addSubview:_stepper];
    [_stepper mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-12);
        make.bottom.equalTo(self.mas_bottom).offset(-12);
        make.height.mas_equalTo(22);
        make.width.mas_equalTo(120);
    }];
    
    
    UILabel *countL = [[UILabel alloc] init];
    countL.text = @"数量";
    countL.font = [UIFont systemFontOfSize:10];
    countL.textColor = RGBColor(140, 140, 140);
    countL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:countL];
    [countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_stepper.mas_left).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-12);
    }];
    
    _numberLabel = [[UILabel alloc] init];
    _numberLabel.text = @"库存剩余量:--";
    _numberLabel.font = [UIFont systemFontOfSize:13];
    _numberLabel.textColor = RGBColor(140, 140, 140);
    _numberLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_numberLabel];
    [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
//        make.right.equalTo(countL.mas_left).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
