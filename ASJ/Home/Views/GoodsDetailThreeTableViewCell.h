//
//  GoodsDetailThreeTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"

@interface GoodsDetailThreeTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *nameL;
@property (nonatomic,strong) UILabel *judgeL;//评论
@property (nonatomic,strong) UILabel *dateL;
@property (nonatomic,strong) UIButton *noticeBtn;
@property (nonatomic,strong) CommentModel *model;
@property (nonatomic,strong) UIImageView *userLogo;
@property (nonatomic,strong) UILabel *noCommentL;
@end
