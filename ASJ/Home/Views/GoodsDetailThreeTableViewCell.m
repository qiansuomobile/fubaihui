//
//  GoodsDetailThreeTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "GoodsDetailThreeTableViewCell.h"

@implementation GoodsDetailThreeTableViewCell

-(void)setModel:(CommentModel *)model{
//    [_userLogo sd_setImageWithURL:[NSURL URLWithString:LoveDriverURL(model.starts)]];
    
    
    [_userLogo sd_setImageWithURL:[NSURL URLWithString:LoveDriverURL(model.starts)] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    
    _nameL.text = model.user_name;
    _judgeL.text = model.content;
    _dateL.text = [VerifyPictureURL dateStringFromTimer:model.ctime];
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"宝贝评价（14826）";
    _titleL.textAlignment = NSTextAlignmentLeft;
    _titleL.font = [UIFont systemFontOfSize:15*KHeight];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
        make.width.mas_equalTo(MainScreen.width-10*Kwidth);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    _userLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userLogo"]];
    _userLogo.layer.masksToBounds = YES;
    _userLogo.layer.cornerRadius = 15*Kwidth;
    [self addSubview:_userLogo];
    [_userLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(20*KHeight);
        make.width.mas_equalTo(30*Kwidth);
        make.height.mas_equalTo(30*Kwidth);
    }];
    
    _nameL = [[UILabel alloc] init];
    _nameL.text = @"1**odo";
    _nameL.font = [UIFont systemFontOfSize:15*KHeight];
    _nameL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_nameL];
    [_nameL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_userLogo.mas_right).offset(5*Kwidth);
        make.centerY.equalTo(_userLogo.mas_centerY);
//        make.top.equalTo(_titleL.mas_bottom).offset(23*KHeight);
//        make.width.mas_equalTo(MainScreen.width-55*Kwidth);
//        make.height.mas_equalTo(25*KHeight);
    }];
    
    _judgeL = [[UILabel alloc] init];
    _judgeL.text = @"第二次购买，好用，发动机声音小，动力大了，好评必须的！";
    _judgeL.lineBreakMode = NSLineBreakByWordWrapping;
    _judgeL.numberOfLines = 0;
    _judgeL.font = [UIFont systemFontOfSize:12*KHeight];
    _judgeL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_judgeL];
    [_judgeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(_userLogo.mas_bottom).offset(5*KHeight);
        //需要从model中返回计算高度
        //make.height.mas_equalTo(20*KHeight);
        make.width.mas_equalTo(MainScreen.width-25*Kwidth);
    }];
    
    _dateL = [[UILabel alloc] init];
    _dateL.text = @"2016-07-07";
    _dateL.font = [UIFont systemFontOfSize:10*KHeight];
    _dateL.textAlignment = NSTextAlignmentLeft;
    _dateL.textColor = RGBColor(161, 161, 161);
    
    [self addSubview:_dateL];
    [_dateL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(_judgeL.mas_bottom).offset(15*KHeight);
        //需要从model中返回计算高度
        //make.height.mas_equalTo(15*KHeight);
        //make.width.mas_equalTo(MainScreen.width-25*Kwidth);
    }];
   
    _noticeBtn = [[UIButton alloc] init];
    [_noticeBtn setTitle:@"查看全部评价" forState:UIControlStateNormal];
    _noticeBtn.titleLabel.font = [UIFont systemFontOfSize:12*KHeight];
    [_noticeBtn setTitleColor:RGBColor(251, 81, 49) forState:UIControlStateNormal];
    [_noticeBtn setBackgroundImage:[UIImage imageNamed:@"gengduoB"] forState:UIControlStateNormal];
    [self addSubview:_noticeBtn];
    [_noticeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(_dateL.mas_bottom).offset(30*KHeight);
        make.width.mas_equalTo(140*Kwidth);
    }];
    
    _noCommentL = [[UILabel alloc] init];
    _noCommentL.text = @"此商品暂无评论";
    _noCommentL.textAlignment = NSTextAlignmentCenter;
    _noCommentL.font = [UIFont systemFontOfSize:20*KHeight];
    _noCommentL.hidden = YES;
    [self addSubview:_noCommentL];
    [_noCommentL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
