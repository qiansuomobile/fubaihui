//
//  GoodsDetailTwoTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "GoodsDetailTwoTableViewCell.h"

@implementation GoodsDetailTwoTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    
    UILabel *countL = [[UILabel alloc] init];
    countL.text = @"数量";
    countL.font = [UIFont systemFontOfSize:15*KHeight];
    countL.textColor = RGBColor(140, 140, 140);
    countL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:countL];
    [countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _stepper = [[PKYStepper alloc] initWithFrame:CGRectMake(55*Kwidth, 10*KHeight, 120*Kwidth, 25*KHeight)];
    [_stepper setBorderColor:RGBColor(140, 140, 140)];
    [_stepper setLabelTextColor:[UIColor blackColor]];
    [_stepper setButtonTextColor:[UIColor blackColor] forState:UIControlStateNormal];
    _stepper.value = 1;
    _stepper.minimum = 1;
    self.stepper.countLabel.text = @"1";
    _stepper.valueChangedCallback = ^(PKYStepper *stepper, float count) {
        stepper.countLabel.text = [NSString stringWithFormat:@"%d", (int)count];
    };
    [self addSubview:_stepper];
    [_stepper mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(countL.mas_right).offset(15*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(25*KHeight);
        make.width.mas_equalTo(120*Kwidth);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
