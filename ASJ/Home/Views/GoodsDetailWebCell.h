//
//  GoodsDetailWebCell.h
//  ASJ
//
//  Created by Dororo on 2019/7/11.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailWebCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *goosDetailWebView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *webLoadingView;

@end

NS_ASSUME_NONNULL_END
