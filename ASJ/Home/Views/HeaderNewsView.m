//
//  HeaderNewsView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "HeaderNewsView.h"

@implementation HeaderNewsView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIImageView *firstDots = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dot_focus"]];
    [self addSubview:firstDots];
   
    [firstDots mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(21*KHeight);
        make.height.mas_equalTo(7*Kwidth);
        make.width.mas_equalTo(7*Kwidth);
    }];
    
    UIImageView *secondDots = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dot_focus"]];
    [self addSubview:secondDots];
  
    [secondDots mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-21*KHeight);
        make.height.mas_equalTo(7*Kwidth);
        make.width.mas_equalTo(7*Kwidth);
    }];
    
    _firstNewsL = [[UILabel alloc] init];
    _firstNewsL.textAlignment = NSTextAlignmentLeft;
    _firstNewsL.text = @"精品配件配件配件1111";
    _firstNewsL.font = [UIFont systemFontOfSize:14*KHeight];
    
    [self addSubview:_firstNewsL];
   
    [_firstNewsL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(firstDots.mas_centerY);
        make.left.equalTo(firstDots.mas_right).offset(7*Kwidth);
        make.right.equalTo(self.mas_right).offset(-7*Kwidth);
    }];
    
    _secondNewsL = [[UILabel alloc] init];
    _secondNewsL.textAlignment = NSTextAlignmentLeft;
    _secondNewsL.text = @"精品配件配件配件2222";
    _secondNewsL.font = [UIFont systemFontOfSize:14*KHeight];
    [self addSubview:_secondNewsL];
   
    [_secondNewsL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(secondDots.mas_centerY);
        make.left.equalTo(firstDots.mas_right).offset(7*Kwidth);
        make.right.equalTo(self.mas_right).offset(-7*Kwidth);
    }];
    
    
}

@end
