//
//  YzHomeJingXuanView.h
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomGood.h"
NS_ASSUME_NONNULL_BEGIN

@protocol YzHomeJingXuanDelegate <NSObject>

- (void)pushGoodsViewWithGoodsID:(BottomGood *)goodsId;

@end

@interface YzHomeJingXuanView : UIView
@property (nonatomic, weak)id<YzHomeJingXuanDelegate>delegate;
@property (nonatomic, strong)NSArray *jingXuanArr;

@end

NS_ASSUME_NONNULL_END
