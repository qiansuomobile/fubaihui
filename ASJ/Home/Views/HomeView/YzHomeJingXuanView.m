//
//  YzHomeJingXuanView.m
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzHomeJingXuanView.h"
#import <UIButton+WebCache.h>

#define BTN_TAG 6666

@implementation YzHomeJingXuanView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    for (int i = 0; i < 4; i++) {
        UIButton *jxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        jxBtn.layer.borderWidth = 2.0f;
        jxBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        jxBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        jxBtn.tag = BTN_TAG + i;
        CGRect frame;
        if (i == 0) {
            frame  = CGRectMake(0, 0, YzWidth/2, YzWidth/2);
        }else if(i == 1){
            frame  = CGRectMake(YzWidth/2, 0, YzWidth/2, YzWidth/4);
        }else{
            frame = CGRectMake(YzWidth/2 + (i -2) * (YzWidth / 4), YzWidth/4, YzWidth/4, YzWidth/4);
        }
        jxBtn.frame = frame;
        [jxBtn addTarget:self action:@selector(showGoodsView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:jxBtn];
    }
}

- (void)showGoodsView:(UIButton *)btn{
    [_delegate pushGoodsViewWithGoodsID:_jingXuanArr[btn.tag - BTN_TAG]];
}

- (void)setJingXuanArr:(NSArray *)jingXuanArr{
    _jingXuanArr = jingXuanArr;
    if (_jingXuanArr) {
        for (int i = 0; i < _jingXuanArr.count; i++) {
            BottomGood *model = _jingXuanArr[i];
            UIButton *jxBtn = [self viewWithTag:BTN_TAG + i];
            [jxBtn sd_setImageWithURL:[NSURL URLWithString:model.picture] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        }
    }
}


@end
