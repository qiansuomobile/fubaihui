//
//  LoveShopTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortGoods.h"
@interface LoveShopTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *descripL;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) UIProgressView *progressV;//进度条
@property (nonatomic,strong) UIButton *buyBtn;
@property (nonatomic,strong) UILabel *countL;//已抢
@property (nonatomic,strong) SortGoods *sortModel;
@property (nonatomic,copy) NSString *imageStr;
@property (nonatomic,strong) UILabel *progressL;
@end
