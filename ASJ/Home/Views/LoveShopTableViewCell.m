//
//  LoveShopTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "LoveShopTableViewCell.h"

@implementation LoveShopTableViewCell

-(void)setImageStr:(NSString *)imageStr{
    [_imageV sd_setImageWithURL:[NSURL URLWithString:imageStr]];
}

-(void)setSortModel:(SortGoods *)sortModel{
    _titleL.text = sortModel.title;
    _countL.text = [NSString stringWithFormat:@"已抢%@件",sortModel.people];
    _priceL.text = sortModel.price;
    [_progressV setProgress:[sortModel.number floatValue] animated:YES];
    _progressL.text = [NSString stringWithFormat:@"%0.0f%%",_progressV.progress*100];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"产品"]];
    [self addSubview:_imageV];
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(120*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    _titleL.numberOfLines = 2;
    _titleL.lineBreakMode = NSLineBreakByCharWrapping;
    _titleL.text = @"威露士洗衣液加量9瓶底价";
    _titleL.textAlignment = NSTextAlignmentLeft;
    _titleL.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
        //make.height.mas_equalTo(15*KHeight);
    }];
    
    _descripL = [[UILabel alloc] init];
    _descripL.text = @"下单立减30元";
    _descripL.textAlignment = NSTextAlignmentLeft;
    _descripL.font = [UIFont systemFontOfSize:16*KHeight];
    _descripL.textColor = RGBColor(222, 52, 74);
    //暂时隐藏
    _descripL.hidden = YES;
    [self addSubview:_descripL];
    [_descripL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(1*KHeight);
       
    }];
    
    _countL = [[UILabel alloc] init];
    _countL.text = @"已抢4543件";
    _countL.textAlignment = NSTextAlignmentLeft;
    _countL.font = [UIFont systemFontOfSize:12*KHeight];
    _countL.textColor = RGBColor(222, 52, 74);
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
    }];
    
    _buyBtn = [[UIButton alloc] init];
    [_buyBtn setBackgroundImage:[UIImage imageNamed:@"抢按钮"] forState:UIControlStateNormal];
    [self addSubview:_buyBtn];
    [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(_countL.mas_top).offset(-5*KHeight);
        make.height.mas_equalTo(30*KHeight);
        make.width.mas_equalTo(65*Kwidth);
    }];
    
    //进度条
    UIView *bgView = [[UIView alloc] init];
    bgView.layer.borderWidth = 1*KHeight;
    bgView.layer.borderColor = [[UIColor redColor] CGColor];
    bgView.layer.cornerRadius = 4.0;
    bgView.clipsToBounds = YES;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.height.mas_equalTo(15*KHeight);
        make.width.mas_equalTo(90*Kwidth);
    }];
    
    
    _progressV = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressV.layer.cornerRadius = 8.0;
    //    _progressV.layer.borderWidth = 0.5*KHeight;
    //    _progressV.layer.borderColor = [[UIColor redColor] CGColor];
    //设置进度条颜色
    _progressV.trackTintColor=RGBColor(253, 183, 182);
    
    //设置进度条上进度的颜色
    _progressV.progressTintColor=[UIColor redColor];
    //设置进度条的背景图片
    //_progressV.trackImage=[UIImage imageNamed:@"进度条"];
    //设置进度条上进度的背景图片
    //_progressV.progressImage=[UIImage imageNamed:@"1.png"];
    
    [_progressV setProgress:0.3 animated:YES];
    
    [bgView addSubview:_progressV];
    [_progressV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bgView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    _progressL = [[UILabel alloc] init];
    _progressL.text = [NSString stringWithFormat:@"%0.0f%%",_progressV.progress*100];
    _progressL.textColor = [UIColor whiteColor];
    _progressL.textAlignment = NSTextAlignmentCenter;
    _progressL.font = [UIFont systemFontOfSize:12*KHeight];
    [bgView addSubview:_progressL];
    [_progressL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bgView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];

    
    
    UILabel *rmbL = [[UILabel alloc] init];
    rmbL.text = @"¥";
    rmbL.textAlignment = NSTextAlignmentLeft;
    rmbL.font = [UIFont systemFontOfSize:13*KHeight];
    rmbL.textColor = RGBColor(222, 52, 74);
    [self addSubview:rmbL];
    [rmbL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.bottom.equalTo(bgView.mas_top).offset(-8*KHeight);
    }];
    
    _priceL = [[UILabel alloc] init];
    _priceL.text = @"65";
    _priceL.textAlignment = NSTextAlignmentLeft;
    _priceL.font = [UIFont systemFontOfSize:20*KHeight];
    _priceL.textColor = RGBColor(222, 52, 74);
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(rmbL.mas_right).offset(0);
        make.bottom.equalTo(bgView.mas_top).offset(-5*KHeight);
    }];
    
    }

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
