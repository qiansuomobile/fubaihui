//
//  MyQuestionTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnswerModel.h"
#import "QuestionModel.h"

@interface MyQuestionTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *solutionL;
@property (nonatomic,strong) UILabel *answerCountL;
@property (nonatomic,strong) UILabel *timeL;
@property (nonatomic,assign) BOOL isAnswer;
@property (nonatomic,strong) AnswerModel *model;
@property (nonatomic,strong) QuestionModel *questionModel;
@end
