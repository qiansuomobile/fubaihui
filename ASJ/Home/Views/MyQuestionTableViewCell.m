//
//  MyQuestionTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "MyQuestionTableViewCell.h"

@implementation MyQuestionTableViewCell

-(void)setQuestionModel:(QuestionModel *)questionModel{
    self.titleL.text = questionModel.title;
    if ([questionModel.is_cnyj isEqualToString:@"1"]) {
        _solutionL.text = @"已解决";
    }else{
        _solutionL.text = @"未解决";
    }
    
    self.answerCountL.text = [NSString stringWithFormat:@"回答:%@",questionModel.num];
    self.timeL.text = [VerifyPictureURL ctime:questionModel.create_time];
}

-(void)setModel:(AnswerModel *)model{
    self.titleL.text = model.title;
    if ([model.is_cnyj isEqualToString:@"1"]) {
        _solutionL.text = @"已经采纳";
    }else{
        _solutionL.text = @"未采纳";
    }
    
    self.answerCountL.text = [NSString stringWithFormat:@"回答:%@",model.num];
    self.timeL.text = [VerifyPictureURL ctime:model.create_time];
}


-(void)setIsAnswer:(BOOL)isAnswer{
    if (isAnswer) {
        _solutionL.text = @"待评价";
    }
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"谁有睡在我上铺的兄弟啊（白白云。）";
    _titleL.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
        make.right.equalTo(self.mas_right).offset(-90*Kwidth);
    }];
    
    _solutionL = [[UILabel alloc] init];
    _solutionL.text = @"未解决";
    _solutionL.textColor = RGBColor(237, 128, 68);
    _solutionL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_solutionL];
    [_solutionL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
    
    _timeL = [[UILabel alloc] init];
    _timeL.text = @"1小时前";
    _timeL.textColor = RGBColor(165, 165, 165);
    _timeL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_timeL];
    [_timeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = RGBColor(165, 165, 165);
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_timeL.mas_left).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
        make.height.mas_equalTo(20*KHeight);
        make.width.mas_equalTo(0.5*Kwidth);
    }];
    
    _answerCountL = [[UILabel alloc] init];
    _answerCountL.text = @"回答：3";
    _answerCountL.textColor = RGBColor(165, 165, 165);
    _answerCountL.textAlignment = NSTextAlignmentRight;
    _answerCountL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_answerCountL];
    [_answerCountL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lineView.mas_left).offset(-5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
