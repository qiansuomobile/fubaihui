//
//  QuestionFirstTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"

@interface QuestionFirstTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *questionL;
@property (nonatomic,strong) UILabel *timeL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) UILabel *rewardL;
@property (nonatomic,strong) QuestionModel *model;
@end
