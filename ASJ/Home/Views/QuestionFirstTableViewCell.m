
//
//  QuestionFirstTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "QuestionFirstTableViewCell.h"
#import "VerifyPictureURL.h"

@implementation QuestionFirstTableViewCell

-(void)setModel:(QuestionModel *)model{
    self.questionL.text = [NSString stringWithFormat:@"              %@",model.title];
    self.timeL.text = [VerifyPictureURL ctime:model.create_time];
    self.rewardL.text = model.price;
    self.countL.text = [NSString stringWithFormat:@"回答:%ld",model.reply.count];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *questionL = [[UILabel alloc] init];
    questionL.text = @"问:";
    questionL.font = [UIFont systemFontOfSize:20*KHeight];
    questionL.textColor = RGBColor(41, 158, 86);
    [self addSubview:questionL];
    [questionL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*Kwidth);
    }];
    
    UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinbi222"]];
    [self addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(questionL.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(12*Kwidth);
        make.height.mas_equalTo(20*KHeight);
        make.width.mas_equalTo(20*KHeight);
    }];
    
    _rewardL = [[UILabel alloc] init];
    _rewardL.text = @"10";
    _rewardL.textColor = RGBColor(240, 173, 47);
    _rewardL.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:_rewardL];
    [_rewardL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageV.mas_right).offset(2*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*Kwidth);
    }];
    
    _questionL = [[UILabel alloc] init];
    _questionL.text = @"              谁有睡在我上铺的兄弟啊（白白云。）";
    _questionL.font = [UIFont systemFontOfSize:20*KHeight];
    _questionL.lineBreakMode = NSLineBreakByCharWrapping;
    _questionL.numberOfLines = 0;
    [self addSubview:_questionL];
    [_questionL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
    }];
    
    _timeL = [[UILabel alloc] init];
    _timeL.textColor = RGBColor(152, 152, 152);
    _timeL.text = @"1小时前";
    _timeL.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:_timeL];
    [_timeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
    
    _countL = [[UILabel alloc] init];
    _countL.textColor = RGBColor(152, 152, 152);
    _countL.text = @"回答:3";
    _countL.font = [UIFont systemFontOfSize:16*KHeight];
    _countL.textAlignment = NSTextAlignmentRight;
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
