//
//  QusetionSecondTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionReply.h"


@interface QusetionSecondTableViewCell : UITableViewCell

@property (nonatomic,strong) QuestionReply *model;

@property (nonatomic,strong) UIButton *logoBtn;
@property (nonatomic,strong) UILabel *userNameL;
@property (nonatomic,strong) UILabel *contentL;
@property (nonatomic,strong) UILabel *timeL;
//采纳
@property (nonatomic,strong) UIButton *adoptBtn;
@property (nonatomic,assign) BOOL isAnwser;
//补充
@property (nonatomic,strong) UILabel *suppleL;
@end
