//
//  QusetionSecondTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "QusetionSecondTableViewCell.h"
#import "VerifyPictureURL.h"

@implementation QusetionSecondTableViewCell

-(void)setModel:(QuestionReply *)model{
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.woaisiji.com/%@",model.headpic]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_logoBtn setBackgroundImage:image forState:UIControlStateNormal];
    }];
    _userNameL.text = model.nickname;
    _contentL.text = model.content;
    _timeL.text = [VerifyPictureURL ctime:model.create_time];
}


-(void)setIsAnwser:(BOOL)isAnwser{
    if (isAnwser) {
        _adoptBtn.hidden = YES;
        _suppleL = [[UILabel alloc] init];
        _suppleL.text = @"2条问答补充";
        _suppleL.font = [UIFont systemFontOfSize:17*KHeight];
        _suppleL.textColor = RGBColor(150, 150, 150);
        _suppleL.textAlignment = NSTextAlignmentRight;
        [self addSubview:_suppleL];
        [_suppleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-15*Kwidth);
            make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        }];
    }
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _logoBtn = [[UIButton alloc] init];
    [_logoBtn setBackgroundImage:[UIImage imageNamed:@"userLogo"] forState:UIControlStateNormal];
    _logoBtn.layer.masksToBounds = YES;
    _logoBtn.layer.cornerRadius = 20*Kwidth;
    [self addSubview:_logoBtn];
    [_logoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.top.equalTo(self.mas_top).offset(15*Kwidth);
        make.height.mas_equalTo(40*KHeight);
        make.width.mas_equalTo(40*KHeight);
    }];
    
    _userNameL = [[UILabel alloc] init];
    _userNameL.text = @"祎雪凉夜love";
    _userNameL.textColor = RGBColor(150, 150, 150);
    _userNameL.font = [UIFont systemFontOfSize:17*KHeight];
    [self addSubview:_userNameL];
    [_userNameL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_logoBtn.mas_right).offset(5*Kwidth);
        make.centerY.equalTo(_logoBtn.mas_centerY);
    }];
    
    _contentL = [[UILabel alloc] init];
    _contentL = [[UILabel alloc] init];
    _contentL.text = @"我倒是有睡在我下铺的鬼呵呵...（床下有人）";
    _contentL.lineBreakMode = NSLineBreakByCharWrapping;
    _contentL.numberOfLines = 0;
    _contentL.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:_contentL];
    [_contentL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_logoBtn.mas_right).offset(5*Kwidth);
        make.top.equalTo(_logoBtn.mas_bottom).offset(5*KHeight);
        make.right.equalTo(self.mas_right).offset(-20*Kwidth-40*KHeight);
    }];
    
    _timeL = [[UILabel alloc] init];
    _timeL.text = @"1小时前";
    _timeL.textColor = RGBColor(150, 150, 150);
    _timeL.font = [UIFont systemFontOfSize:17*KHeight];
    [self addSubview:_timeL];
    [_timeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_logoBtn.mas_right).offset(5*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
    }];
    
    _adoptBtn = [[UIButton alloc] init];
    [_adoptBtn setBackgroundImage:[UIImage imageNamed:@"caina"] forState:UIControlStateNormal];
    [_adoptBtn setTitle:@"采纳" forState:UIControlStateNormal];
    [_adoptBtn setTitleColor:RGBColor(150, 150, 150) forState:UIControlStateNormal];
    _adoptBtn.titleLabel.font = [UIFont systemFontOfSize:15*KHeight];
    [self addSubview:_adoptBtn];
    [_adoptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*KHeight);
        make.bottom.equalTo(self.mas_bottom).offset(-6*KHeight);
        make.width.mas_equalTo(60*Kwidth);
        make.height.mas_equalTo(30*KHeight);
    }];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
