//
//  SearchResultCollectionViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortGoods.h"

@interface SearchResultCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UIButton *goldBtn;
@property (nonatomic,strong) UIButton *silverBtn;
@property (nonatomic,strong) UILabel *rmbL;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) SortGoods *sortGood;
@property (nonatomic,strong) NSString *imageStr;
@property (nonatomic,assign) NSInteger shopType;



+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;


-(void)setCorevImg:(SortGoods *)sortGood;

@end
