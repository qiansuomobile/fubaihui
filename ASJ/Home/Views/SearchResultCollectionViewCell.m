//
//  SearchResultCollectionViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SearchResultCollectionViewCell.h"

@implementation SearchResultCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self setUpSubview];
        
    }
    
    return self;
}


+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:@"collectionCellID"];
    
    id cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCellID" forIndexPath:indexPath];
    
    return cell;
    
}

-(void)setUpSubview{
   
    _imageV = [[UIImageView alloc] init];
    
    [self addSubview:_imageV];
    

    
    
    
    _titleL = [[UILabel alloc] init];
    _titleL.font = [UIFont systemFontOfSize:15*KHeight];
    [self addSubview:_titleL];

    
    
    _goldBtn = [[UIButton alloc] init];
    [_goldBtn setBackgroundImage:[UIImage imageNamed:@"jinbiB(1)"] forState:UIControlStateNormal];
    _goldBtn.titleLabel.font = [UIFont systemFontOfSize:11*KHeight];
    [self addSubview:_goldBtn];


    
    
    
    _silverBtn = [[UIButton alloc] init];

    _silverBtn.titleLabel.font = [UIFont systemFontOfSize:11*KHeight];
    [self addSubview:_silverBtn];
    
    
    
    _rmbL = [[UILabel alloc] init];
    _rmbL.font = [UIFont systemFontOfSize:13*KHeight];
    _rmbL.textColor = RGBColor(252, 77, 0);
    [self addSubview:_rmbL];


    
    
    
    _priceL = [[UILabel alloc] init];
    
    _priceL.font = [UIFont systemFontOfSize:20*KHeight];
    _priceL.textColor = RGBColor(252, 77, 0);
    [self addSubview:_priceL];
    
    
    _countL = [[UILabel alloc] init];
    _countL.font = [UIFont systemFontOfSize:13*KHeight];
    _countL.textColor = RGBColor(150, 150, 150);
    [self addSubview:_countL];


}

-(void)setImageStr:(NSString *)imageStr{
    
    [_imageV sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"logo"]];
}



-(void)setCorevImg:(SortGoods *)sortGood{
 
    
    
    
    NSString *str = sortGood.cover;
    
//    NSRange range = [str rangeOfString:@","];
//    
//    if (sortGood.cover ) {
//        
//    }
    
    
    NSString *subStr;
   
    if([str rangeOfString:@","].location !=NSNotFound)//_roaldSearchText
    {
        
        
        NSRange range = [str rangeOfString:@","];
        
        
        subStr = [str substringToIndex:range.location];

        
        NSLog(@" 截取 对   id  %@",subStr);

    }
    else
    {
        NSLog(@"no");
        
        
        subStr = sortGood.cover;
    }

    
    
    
    
//    NSString *cover = [NSString stringWithFormat:@"http://www.woaisiji.com%@",sortGood.cover[0]];

//    NSDictionary *coverDic = @{@"cover":sortGood.cover};
//    //
//        [NetMethod Post:LoveDriverURL(@"APP/Public/get_cover_url") parameters:coverDic success:^(id responseObject) {
//    
//    //        NSLog(@"%@",responseObject);
//    
//            if ([responseObject[@"code"] isEqual:@200]) {
//    
//                NSString *imageStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",responseObject[@"list"][0]];
//    
//                [_imageV sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"logo"]];
//            }
//    
//    //            [self.imageArr addObject:imageStr];
//    
//    //            [self.collectionView reloadData];
//    
//        } failure:^(NSError *error) {
//    
//    //        [MBProgressHUD showSuccess:NetProblem toView:super.view];
//            
//        }];

//    NSLog(@" 图片ID  数组   %@",sortGood.cover[0]);
    
    
    
    [NetMethod Post:LoveDriverURL(@"APP/Public/get_img_path") parameters:@{@"img_id":subStr} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        
        if ([dic[@"code"] isEqual:@200]) {
            
            
            
            NSString *cover = [NSString stringWithFormat:@"http://www.woaisiji.com%@",dic[@"path"]];
            
            NSLog(@"图片路径   %@",cover);
            
            [_imageV sd_setImageWithURL:[NSURL URLWithString:cover] placeholderImage:[UIImage imageNamed:@"logo"]];
            

        }
        
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
//    [_imageV sd_setImageWithURL:[NSURL URLWithString:cover] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    
//    NSLog(@" 图片地址  ***********  %@",cover);
    
    _sortGood = sortGood;
    
    
    _titleL.text = sortGood.title;
    
    
    [_goldBtn setBackgroundImage:[UIImage imageNamed:@"jinbiB(1)"] forState:UIControlStateNormal];
   
    [_goldBtn setTitle:[NSString stringWithFormat:@"送金A币%@",sortGood.f_sorts] forState:UIControlStateNormal];
    
    [_silverBtn setBackgroundImage:[UIImage imageNamed:@"yinbiB"] forState:UIControlStateNormal];
    
    [_silverBtn setTitle:[NSString stringWithFormat:@"送银A币%@",sortGood.f_silver] forState:UIControlStateNormal];
    
    _rmbL.text = @"¥";
    
    
    _priceL.text = sortGood.price;
    
    
    _countL.text = [NSString stringWithFormat:@"%@人付款",sortGood.people];
    
    //兑换商城
    if (self.shopType == 2) {
        _rmbL.font = [UIFont systemFontOfSize:20*KHeight];
        _rmbL.text = [NSString stringWithFormat:@"%@银A币",sortGood.price];
        _priceL.hidden = YES;
        [_countL mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.rmbL.mas_right).offset(10*Kwidth);
            make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
        }];
    }
    
    
    
    [self setUPFrame];
    

    
    
}



-(void)setSortGood:(SortGoods *)sortGood{
    
    
//    NSDictionary *coverDic = @{@"cover":sortGood.cover};
//    
//    [NetMethod Post:LoveDriverURL(@"APP/Public/get_cover_url") parameters:coverDic success:^(id responseObject) {
//        
////        NSLog(@"%@",responseObject);
//        
//        if ([responseObject[@"code"] isEqual:@200]) {
//            
//            NSString *imageStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",responseObject[@"list"][0]];
//                
//            [_imageV sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"logo"]];
//        }
//
////            [self.imageArr addObject:imageStr];
//            
////            [self.collectionView reloadData];
//        
//    } failure:^(NSError *error) {
//        
////        [MBProgressHUD showSuccess:NetProblem toView:super.view];
//        
//    }];
    
    
    
    
    
    

    
    
    
    
    NSString *imageStr = [NSString stringWithFormat:@"http://www.woaisiji.com%@",sortGood.pic];
    
    [_imageV sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    
    _sortGood = sortGood;
    
    
    _titleL.text = sortGood.title;
    
    
    
    
    [_goldBtn setBackgroundImage:[UIImage imageNamed:@"jinbiB(1)"] forState:UIControlStateNormal];
    [_goldBtn setTitle:[NSString stringWithFormat:@"送金A积分%@",sortGood.f_sorts] forState:UIControlStateNormal];
    
    [_silverBtn setBackgroundImage:[UIImage imageNamed:@"yinbiB"] forState:UIControlStateNormal];
  
    [_silverBtn setTitle:[NSString stringWithFormat:@"送银A积分%@",sortGood.f_silver] forState:UIControlStateNormal];
  
    _rmbL.text = @"¥";
    
    
    _priceL.text = sortGood.price;
  
    
    _countL.text = [NSString stringWithFormat:@"%@人付款",sortGood.people];

    //兑换商城
    if (self.shopType == 2) {
        _rmbL.font = [UIFont systemFontOfSize:20*KHeight];
        _rmbL.text = [NSString stringWithFormat:@"%@银A积分",sortGood.price];
        _priceL.hidden = YES;
        [_countL mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.rmbL.mas_right).offset(10*Kwidth);
            make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
        }];
    }
    
    
    
    [self setUPFrame];
    
}

-(void)setUPFrame{
    
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 90*KHeight, 0));
    }];

    
    
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(_imageV.mas_bottom).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
    }];

    
    [_goldBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(10*Kwidth);
    }];

    
    [_silverBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goldBtn.mas_right).offset(10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(10*Kwidth);
    }];

    
    [_rmbL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];

    
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.rmbL.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-8*Kwidth);
    }];
    
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceL.mas_right).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];

    
    
}
-(void)setImage:(UIImage *)image{
    
    
}

@end
