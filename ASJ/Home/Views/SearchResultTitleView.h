//
//  SearchResultTitleView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultTitleView : UIView
@property (nonatomic,strong) UILabel *keyWordL;
@end
