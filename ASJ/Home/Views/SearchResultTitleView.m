//
//  SearchResultTitleView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SearchResultTitleView.h"

@implementation SearchResultTitleView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setNeedsDisplay];
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.backgroundColor = [UIColor clearColor];
    _keyWordL = [[UILabel alloc] init];
//    _keyWordL.text = @"note3电池";
    _keyWordL.textAlignment = NSTextAlignmentCenter;
    _keyWordL.textColor = [UIColor whiteColor];
    _keyWordL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_keyWordL];
    [_keyWordL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(void)drawRect:(CGRect)rect{
    //设置背景颜色
    [[UIColor clearColor]set];
    
    UIRectFill([self bounds]);
    
    //拿到当前视图准备好的画板
    
    CGContextRef
    context = UIGraphicsGetCurrentContext();
    
    //利用path进行绘制三角形
    CGContextSetLineWidth(context, 1*Kwidth);//线宽
    
    CGContextBeginPath(context);//标记
    
    CGContextMoveToPoint(context,1*Kwidth, self.frame.size.height - 5*KHeight);//设置起点
    
    CGContextAddLineToPoint(context,1*Kwidth, self.frame.size.height - 1*KHeight);
    
    CGContextAddLineToPoint(context,self.frame.size.width - 1*Kwidth, self.frame.size.height - 1*KHeight);
    
    CGContextAddLineToPoint(context,self.frame.size.width - 1*Kwidth, self.frame.size.height - 5*KHeight);
    
    //CGContextClosePath(context);//路径结束标志，不写默认封闭
    
    //[RGBColor(249, 56, 65) setFill]; //设置填充色
    
    [[UIColor whiteColor] setStroke]; //设置边框颜色
    
    CGContextDrawPath(context,kCGPathFillStroke);//绘制路径path
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
