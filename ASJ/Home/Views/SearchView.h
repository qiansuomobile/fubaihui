//
//  SearchView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/27.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIView
@property (nonatomic,strong) UITextField *textF;
@property (nonatomic,strong) UIButton *searchBtn;
@end
