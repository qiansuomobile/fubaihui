//
//  SearchView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/27.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI:frame];
    }
    return self;
}

-(void)createUI:(CGRect)frame{
   
    self.backgroundColor = RGBColor(240, 240, 240);
    
    _textF = [[UITextField alloc] init];
    _textF.frame = CGRectMake(35, 0, frame.size.width - 60, frame.size.height);
    _textF.textColor = [UIColor whiteColor];
    _textF.font = [UIFont systemFontOfSize:15*KHeight];
    _textF.backgroundColor = RGBColor(240, 240, 240);
    _textF.placeholder = @"搜索商品或商家";
    _textF.borderStyle = UITextBorderStyleNone;
    _textF.returnKeyType = UIReturnKeySearch;
   
    [self addSubview:_textF];
    
    _searchBtn = [[UIButton alloc] init];
    
    _searchBtn.frame = CGRectMake(10, 5, 19,19);
    
    [_searchBtn setBackgroundImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
    
    [self addSubview:_searchBtn];
    
    self.layer.cornerRadius = 8;
}


@end
