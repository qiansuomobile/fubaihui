//
//  YJItem.h
//  BKoo
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 王永军. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger , YJItemType) {

    YJItemTypeQRCode,
    YJItemTypeOther,
};


@interface YJItem : UIButton

@property (nonatomic , assign) YJItemType * type;


-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;



@end
