//
//  YJItem.m
//  BKoo
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 王永军. All rights reserved.
//

#import "YJItem.h"
#import <objc/runtime.h>

@implementation YJItem

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title
{

    self =  [YJItem buttonWithType:UIButtonTypeSystem];
    if (self) {
        
        [self setTitle:title forState:UIControlStateNormal];
        self.frame = frame;
    }
    return self;


}

@end
