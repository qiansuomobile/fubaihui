//
//  YJMenu.h
//  BKoo
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 王永军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YJItem.h"


typedef void (^YJMenuDidSelectedBlock)(YJItem * item);

@interface YJMenu : UIView

@property (nonatomic , copy) YJMenuDidSelectedBlock didSelectedBlock;

-(instancetype)initWithFrame:(CGRect)frame;


@end
