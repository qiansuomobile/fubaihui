//
//  YJView.h
//  BKoo
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 王永军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YJMenu.h"


@protocol QRViewDelegate <NSObject>

-(void)scanTypeConfig:(YJItem *)item;

@end

@interface YJView : UIView


@property (nonatomic, weak) id<QRViewDelegate> delegate;
/**
 *  透明的区域
 */
@property (nonatomic, assign) CGSize transparentArea;


@end
