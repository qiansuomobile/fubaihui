//
//  bottomView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/25.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "bottomView.h"

@implementation bottomView
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
//    _serviceBtn = [[UIButton alloc] init];
//    [_serviceBtn setBackgroundImage:[UIImage imageNamed:@"kefu2"] forState:UIControlStateNormal];
//   // [_serviceBtn setTitle:@"客服" forState:UIControlStateNormal];
//    [self addSubview:_serviceBtn];
//    [_serviceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).offset(0);
//        make.top.equalTo(self.mas_top).offset(0);
//        make.bottom.equalTo(self.mas_bottom).offset(0);
//        make.width.mas_equalTo(MainScreen.width/3/2-0.5*Kwidth);
//    }];
    
    _collectBtn = [[UIButton alloc] init];
    _collectBtn.tag = 101;
    [_collectBtn setImage:[UIImage imageNamed:@"shoucang2"] forState:UIControlStateNormal];
    [self addSubview:_collectBtn];
    [_collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(1*Kwidth);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(80);
    }];
    
    
    _buyBtn = [[UIButton alloc] init];
    _buyBtn.tag = 103;
    _buyBtn.layer.masksToBounds = YES;
    _buyBtn.layer.cornerRadius = 35.0/2;
    [_buyBtn setTitle:@"查看购物车" forState:UIControlStateNormal];
    [_buyBtn setBackgroundColor:RGBColor(255, 81, 2)];
    _buyBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self addSubview:_buyBtn];
    [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.mas_equalTo(MainScreen.width/4);
    }];
    
    _addCarBtn = [[UIButton alloc] init];
    _addCarBtn.tag = 102;
    _addCarBtn.layer.masksToBounds = YES;
    _addCarBtn.layer.cornerRadius = 35.0/2;
    [_addCarBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    _addCarBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [_addCarBtn setBackgroundColor:RGBColor(254, 148, 2)];
    [self addSubview:_addCarBtn];
    [_addCarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_buyBtn.mas_left).offset(-10);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.mas_equalTo(MainScreen.width/4);
    }];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
