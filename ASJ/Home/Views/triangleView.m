//
//  triangleView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/30.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "triangleView.h"

@implementation triangleView

-(id) init{
    self = [super init];
    if (self) {
        [self setNeedsDisplay];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


-(void)drawRect:(CGRect)rect

{
    //设置背景颜色
    [[UIColor clearColor]set];
    
    UIRectFill([self bounds]);
    
    //拿到当前视图准备好的画板
    
    CGContextRef
    context = UIGraphicsGetCurrentContext();
    
    //利用path进行绘制三角形
    
    CGContextBeginPath(context);//标记
    
    CGContextMoveToPoint(context,0, 0);//设置起点
    
    CGContextAddLineToPoint(context,self.frame.size.width, 0);
    
    CGContextAddLineToPoint(context,self.frame.size.width/2, self.frame.size.height);
    
    CGContextClosePath(context);//路径结束标志，不写默认封闭
    
    [RGBColor(249, 56, 65) setFill]; //设置填充色
    
    //[[UIColor whiteColor] setStroke]; //设置边框颜色
    
    CGContextDrawPath(context,kCGPathFillStroke);//绘制路径path
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
