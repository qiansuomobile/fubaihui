//
//  LoginViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "LoginViewController.h"
#import "CustomTextField.h"
#import "YzRegisterViewController.h"
#import "YYYChangPasswordViewController.h"
#import "YzforgetPsVC.h"
#import "WXUtil.h"
#import "AdvertisementViewController.h"

@interface LoginViewController ()<UITextFieldDelegate,UINavigationBarDelegate>

@property (nonatomic,strong) CustomTextField *userNameTF;

@property (nonatomic,strong) CustomTextField *passwordTF;

//蒙版
@property (nonatomic,strong)MBProgressHUD *hud;


@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated{
    //隐藏navigationBar123123
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    

//    [self.navigationController setNavigationBarHidden:YES animated:NO];

}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.view endEditing:YES];
    [super viewWillDisappear:animated];//必须调用
    
    
    
    //    [self.navigationController.navigationBar setTranslucent:NO];
    //
    //
    //    self.navigationController.navigationBar.clipsToBounds = NO;

}

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self createUI];
}
#pragma mark - 蒙版懒加载
- (MBProgressHUD *)hud
{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:_hud];
    }
    return _hud;
}


-(void)YZsurePersenData{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) createUI{
    
    //背景图
    UIImageView *bgView = [[UIImageView alloc] init];
    bgView.userInteractionEnabled = NO;
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIImageView * logoImage =[[UIImageView alloc] init];
    logoImage.frame         = CGRectMake(YzWidth/8*3, YzWidth/4+20, YzWidth/4, YzWidth/4);
    logoImage.image         = [UIImage imageNamed:@"福百惠.png"];
    [self.view addSubview:logoImage];
    
    
    
    
    //返回键
    UIButton *goBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    goBackButton.frame = CGRectMake(10, 44, 44, 44);
//    goBackButton.backgroundColor = [UIColor redColor];
    [goBackButton setImage:[UIImage imageNamed:@"new_home_icon_back_black"] forState:UIControlStateNormal];
    [goBackButton addTarget:self action:@selector(YZsurePersenData) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:goBackButton];

    
    //用户名
    self.userNameTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, 240*KHeight, MainScreen.width-100, 35)];
    self.userNameTF.textField.delegate = self;
    self.userNameTF.textField.keyboardType = UIKeyboardTypePhonePad;
    self.userNameTF.textField.placeholder = @"请输入手机号";
    self.userNameTF.iconImage.image = [UIImage imageNamed:@"user_accout"];
    [self.view addSubview:self.userNameTF];
    
    self.userNameTF.layer.borderWidth = 0.5;
    self.userNameTF.layer.borderColor = [UIColor grayColor].CGColor;
    self.userNameTF.layer.cornerRadius = 17.5;

    
    //密码
    self.passwordTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, self.userNameTF.frame.origin.y+self.userNameTF.frame.size.height+20, MainScreen.width-100, 35)];
    self.passwordTF.textField.secureTextEntry = YES;
    self.passwordTF.textField.delegate = self;
    self.passwordTF.textField.placeholder = @"请输入密码";
    self.passwordTF.iconImage.image = [UIImage imageNamed:@"user_password"];
    [self.view addSubview:self.passwordTF];
    
    self.passwordTF.layer.borderWidth = 0.5;
    self.passwordTF.layer.borderColor = [UIColor grayColor].CGColor;
    self.passwordTF.layer.cornerRadius = 17.5;

    
    
    //登录
    UIButton *loginBtn = [[UIButton alloc] init];
    [loginBtn setTitle:@"确认登录" forState:UIControlStateNormal];
//    [loginBtn setBackgroundImage:[UIImage imageNamed:@"btn_login_bg"] forState:UIControlStateNormal];
    [loginBtn setBackgroundColor:Green];
    [loginBtn addTarget:self action:@selector(clickLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(50);
        make.top.equalTo(self.passwordTF.mas_bottom).offset(30);
        make.right.equalTo(bgView.mas_right).offset(-50);
        make.height.mas_equalTo(40);
    }];
    
    loginBtn.layer.cornerRadius = 20;
    
    
    UIButton *forgetBtn = [[UIButton alloc] init];
    [forgetBtn setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [forgetBtn setTitleColor:b_Gray forState:UIControlStateNormal];
    [forgetBtn addTarget:self action:@selector(forgetPassword) forControlEvents:UIControlEventTouchUpInside];
    forgetBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    forgetBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14];
    [self.view addSubview:forgetBtn];
    [forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(bgView.mas_right).offset(-50);
        make.top.equalTo(loginBtn.mas_bottom).offset(15);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
    
    //立即注册
    UIButton *registerBtn = [[UIButton alloc] init];
    [registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
    [registerBtn setTitleColor:b_Gray forState:UIControlStateNormal];
    [registerBtn addTarget:self action:@selector(clickRegister) forControlEvents:UIControlEventTouchUpInside];
    registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    registerBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14];
    [self.view addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(50);
        make.top.equalTo(loginBtn.mas_bottom).offset(15);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
    

    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:@"登录即表示同意福百惠用户协议" forState:UIControlStateNormal];
    [sureButton setTitleColor:b_Gray forState:UIControlStateNormal];
//    sureButton.titleLabel.textColor = [UIColor redColor];
//    sureButton.titleLabel.text = @"阅读并同意福百惠用户协议及用户保护声明";
    sureButton.titleLabel.font = [UIFont systemFontOfSize:14];
    sureButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [sureButton addTarget:self action:@selector(backItem) forControlEvents:UIControlEventTouchUpInside];
//    [sureButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.view addSubview:sureButton];
    
    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(50);
        make.top.equalTo(loginBtn.mas_bottom).offset(80);
        make.right.equalTo(bgView.mas_right).offset(-50);
//        make.width.mas_equalTo(YzWidth-100);
        make.height.mas_equalTo(30);
    }];

    
//    UILabel * ationLabel = [[UILabel alloc] init];
//    ationLabel.frame     = CGRectMake(0, registerBtn.frame.origin.y + 20, YzWidth, 20);
//    ationLabel.text      = @"阅读并同意福百惠用户协议及用户保护声明";
//    ationLabel.font      = [UIFont systemFontOfSize:10];
//    ationLabel.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview: ationLabel];
    
    
}
//登录
-(void)clickLogin{
    NSDictionary *loginDic = @{@"username":self.userNameTF.textField.text,@"password":self.passwordTF.textField.text};
    
    [NetMethod Post:FBHRequestUrl(kUrl_user_login) parameters:loginDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [[dic objectForKey:@"code"] intValue];
        
        if (code == 200){
            
            NSDictionary *dataDic = dic[@"data"];
            int userid = [[dataDic objectForKey:@"uid"] intValue];
            
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DriverISLogIN"];
//            [[NSUserDefaults standardUserDefaults] setObject:self.passwordTF.textField.text forKey:@"DriverLogPassWord"];

            NSString *uid = [NSString stringWithFormat:@"%d",userid];
            
            [[NSUserDefaults standardUserDefaults] setObject:uid forKey:@"LoveDriverID"];
            [[NSUserDefaults standardUserDefaults] setObject:self.userNameTF.textField.text forKey:@"LoveUserName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [JPUSHService setAlias:uid completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                NSLog(@"iResCode %ld, iAlias %@, seq %ld", (long)iResCode, iAlias, (long)seq);
            } seq:200];
            [self loginWithUsername:uid uid:uid];
            
            //获取用户token
            //获取用户token
            [NetMethod getUserToken:^(BOOL result) {
                if (result) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DriverISLogIN"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [NetMethod sendDeviceToken:FBH_DEVICE_TOKEN];
                    [self delayMethod];
                }else{
                    [MBProgressHUD showError:@"获取用户token失败" toView:self.view];
                }
            }];
        }else{
            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
        }
        
    } failure:^(NSError *error) {
        
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}
//点击登陆后的操作
- (void)loginWithUsername:(NSString *)username uid:(NSString *)uid{
    
    NSString *sign = [NSString stringWithFormat:@"fbhim%@",uid];
    NSString *signmd5 = [WXUtil md5:sign];
    NSLog(@"👌%@", signmd5);
    [[EMClient sharedClient] registerWithUsername:username password:signmd5 completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            NSLog(@"error1注册成功");
        }else{
            NSLog(@"error1注册失败");
            NSLog(@"error1CODE %u DES %@", aError.code, aError.errorDescription);
        }
        
        [[EMClient sharedClient] loginWithUsername:username password:signmd5 completion:^(NSString *aUsername, EMError *aError) {
            if (!aError) {
                NSLog(@"error2登录成功");
                //设置是否自动登录
                [[EMClient sharedClient].options setIsAutoLogin:YES];
                
                //保存最近一次登录用户名
                //        [weakself saveLastLoginUsername];
                //发送自动登陆状态通知
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
            }else{
                NSLog(@"error2 -登录失败");
                NSLog(@"error2CODE %u DES %@", aError.code, aError.errorDescription);
            }
        }];
    }];
}

-(void)tagsAliasCallback:(int)iResCode
                    tags:(NSSet*)tags
                   alias:(NSString*)alias
{
    NSLog(@"rescode: %d, \ntags: %@, \nalias: %@\n", iResCode, tags , alias);
}




//忘记密码
-(void)forgetPassword{
    
//    YzforgetPsVC *vc = [[YzforgetPsVC alloc]init];
//
//    [self.navigationController pushViewController:vc animated:YES];
    YYYChangPasswordViewController *vc = [[YYYChangPasswordViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
//注册
-(void)clickRegister{
    YzRegisterViewController *registerVC = [[YzRegisterViewController alloc]init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)delayMethod{
    
//    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}
//66

#pragma mark - action
- (void)backItem{
    
    AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
    adVC.Ad_ID = kUrl_Screct;
    adVC.title = @"隐私协议";
    [self.navigationController pushViewController:adVC animated:YES];
    
}
@end
