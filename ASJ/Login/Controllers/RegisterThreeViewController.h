//
//  RegisterThreeViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterThreeViewController : UIViewController


@property (nonatomic , assign) int CodeString;

@property (nonatomic , copy)NSString *Password;

@property (nonatomic , copy)NSString *UserName;

@end
