//
//  RegisterThreeViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterThreeViewController.h"
#import "RegisterThreeTableViewCell.h"


@interface RegisterThreeViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UITextField *passwordTF;
@property (nonatomic,strong) UITextField *confirmTF;
@end

@implementation RegisterThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

-(void)createUI{
    
    [self setupBackBtn];
    
    self.tableView = [[UITableView alloc] init];
    
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    self.tableView.delegate = self;
    
    self.tableView.dataSource = self;
    
    self.tableView.scrollEnabled = NO;
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    
    }];
}

-(void)setupBackBtn{
    
    self.title = @"注册3/3";
    
    UIButton *backBtn = [[UIButton alloc] init];
    
    backBtn.frame = CGRectMake(0, 0, 15, 20);
    
    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
    
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem = leftBtn;
}

-(void)clickBack{
    
    
//    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}
//注册
-(void) TapRegister{
   
    
    if (![_passwordTF.text isEqualToString:_confirmTF.text]) {
    
        [MBProgressHUD showSuccess:@"两次密码输入不一致" toView:self.view];
    
    }else{
    
        if (_passwordTF.text.length <= 6) {
        
            [MBProgressHUD showSuccess:@"密码过于简单可能被盗" toView:self.view];
            
            
        
        }else{
        
            self.Password = _confirmTF.text;
            NSLog(@"%@      %@", self.UserName,self.Password);

            [self RegisterCoding];
        }
    
    }

}


-(void)RegisterCoding{
    
    NSDictionary *dic = @{@"username":self.UserName,@"password":self.Password};
    
    
//    NSLog(@"%@      %@", self.UserName,self.Password);
//    
    [NetMethod Post:FBHRequestUrl(kUrl_user_register) parameters:dic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"%@",dic);
        
        int code = [[dic objectForKey:@"code"] intValue];
        
        if (code == 200) {
            
            NSString *userID = [dic objectForKey:@"uid"];

            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DriverISLogIN"];

            [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"LoveDriverID"];
            
            [MBProgressHUD showSuccess:@"注册成功" toView:self.view];
            
            [[NSUserDefaults standardUserDefaults] setObject:self.UserName forKey:@"LoveUserName"];
            [self performSelector:@selector(BackToHome) withObject:nil afterDelay:1.5];
        
        }else{
            
            NSString *msg = [dic objectForKey:@"msg"];
            
            [MBProgressHUD showSuccess:msg toView:self.view];
            
        }
        
    } failure:^(NSError *error) {
        
//        [MBProgressHUD showMessag:error toView:self.view];

    }];
}

#pragma mark --- UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RegisterThreeTableViewCell *cell = [[RegisterThreeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textField.delegate = self;
    
    if (indexPath.section == 0) {
        
        _passwordTF = cell.textField;
 
    
    }else{
    
        cell.passwordL.text = @"确认密码";
    
        _confirmTF = cell.textField;
    
    }
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (section == 1) {
        UIButton *registerBtn = [[UIButton alloc] init];
        [registerBtn setBackgroundColor:[UIColor colorWithRed:252/255.0f green:163/255.0f blue:46/255.0f alpha:1.0]];
        [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
        [registerBtn addTarget:self action:@selector(TapRegister) forControlEvents:UIControlEventTouchUpInside];
        registerBtn.layer.cornerRadius = 3.0*Kwidth;
        [view addSubview:registerBtn];
        [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view.mas_left).offset(5);
            make.right.equalTo(view.mas_right).offset(-5);
            make.top.equalTo(view.mas_top).offset(20);
            make.height.mas_equalTo(45);
        }];
    }
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 200;
    }else{
        return 0;
    }
}

#pragma mark --- UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)BackToHome{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//00
@end
