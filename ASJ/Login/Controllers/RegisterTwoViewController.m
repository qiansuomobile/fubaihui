//
//  RegisterTwoViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterTwoViewController.h"

#import "RegisterTwoTableViewCell.h"

#import "RegisterThreeViewController.h"

@interface RegisterTwoViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) RegisterTwoTableViewCell *cell;

@property (nonatomic , assign) int LDContent;

@end

@implementation RegisterTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self addTapInView];
    
    
}

-(void)createUI{
    self.title = @"注册2/3";
    
    //    UIButton *backBtn = [[UIButton alloc] init];
    //    backBtn.frame = CGRectMake(0, 0, 15, 20);
    //    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    //    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
    //    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    //    self.navigationItem.leftBarButtonItem = leftBtn;
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)TapNext{
    
    NSString *code = [NSString stringWithFormat:@"%d",self.LDContent];
    
    
    NSLog(@"验证码  %@",_cell.textField.text);
    
    if ([code isEqualToString:_cell.textField.text] ) {
        
        RegisterThreeViewController *vc = [[RegisterThreeViewController alloc]init];
        
        //            vc.CodeString = self.LDContent;
        vc.UserName = self.phoneNumber;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        //        [self.navigationController pushViewController:[RegisterThreeViewController new] animated:YES];
        
    }else{
        
        //        [];
        [MBProgressHUD showSuccess:@"验证码错误" toView:self.view];
        
    }
}

-(void)getCode{
    
//    NSLog(@"获取验证码");
//    
//        [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:self.phoneNumber
//                                       zone:@"86"
//                           customIdentifier:nil
//                                     result:^(NSError *error){
//                                         if (!error) {
//    
//                                             NSLog(@"获取验证码成功 **********");
//    
//    
//    
//    
//                                         } else {
//                                             NSLog(@"错误信息：%@",error);
//                                         }
//                                         }];
//    //
//    //    http://www.woaisiji2.com/APP/Public/sendsms
//    
//    NSDictionary *dic = @{@"phone":self.phoneNumber};
//    
//    [NetMethod Post:LoveDriverURL(@"APP/Public/sendsms") parameters:dic success:^(id responseObject) {
//        
//        NSLog(@"获取验证码   %@",responseObject);
//        
//        NSDictionary *dic = responseObject;
//        
//        int code = [[dic objectForKey:@"code"] intValue];
//        
//        
//        if (code ==200) {
//            [self startTime];
//            int content = [[dic objectForKey:@"content"] intValue];
//            
//            self.LDContent = content;
//            
//            //            然后再判断
//        }else{
//            [MBProgressHUD showSuccess:[responseObject objectForKey:@"returncode"] toView:self.view];
//        }
//        
//    } failure:^(NSError *error) {
//        
//    }];
    
}

//点击屏幕时，取消键盘响应
-(void)addTapInView{
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    
    tapGestureRecognizer.cancelsTouchesInView = NO;
    
    //将触摸事件添加到当前view
    
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    
    [_cell.textField resignFirstResponder];
}


#pragma mark -- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RegisterTwoTableViewCell *cell = [[RegisterTwoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    [cell.codeBtn addTarget:self action:@selector(getCode) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _cell = cell;
    
    return cell;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    //下一步
    
    UIButton *nextBtn = [[UIButton alloc] init];
    
    [nextBtn setBackgroundColor:[UIColor colorWithRed:252/255.0 green:153/255.0 blue:46/255.0 alpha:1.0]];
    
    [nextBtn setTitle:@"下一步,设计密码" forState:UIControlStateNormal];
    
    nextBtn.layer.cornerRadius = 3.0*Kwidth;
    
    [nextBtn addTarget:self action:@selector(TapNext) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:nextBtn];
    
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(view.mas_left).offset(10);
        
        make.top.equalTo(view.mas_top).offset(20);
        
        make.right.equalTo(view.mas_right).offset(-10);
        
        make.height.mas_equalTo(50);
        
    }];
    
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    UILabel *sendMessageL = [[UILabel alloc] init];
    
    sendMessageL.text = [NSString stringWithFormat:@"验证码短信发送至%@",self.phoneNumber];
    
    sendMessageL.textAlignment = NSTextAlignmentCenter;
    
    sendMessageL.font = [UIFont systemFontOfSize:14];
    
    [view addSubview:sendMessageL];
    
    [sendMessageL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(view.mas_left).offset(5);
        
        make.top.equalTo(view.mas_top).offset(20);
        
    }];
    
    [sendMessageL sizeToFit];
    
    return view;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 200;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(void)startTime
{
    //开启倒计时
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer,^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置界面的按钮显示 根据自己需求设置
                [_cell.codeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
                _cell.codeBtn.userInteractionEnabled = YES;
            });
        }
        else
        {
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_cell.codeBtn setTitle:[NSString stringWithFormat:@"%@秒后重复",strTime] forState:UIControlStateNormal];
                _cell.codeBtn.userInteractionEnabled = NO;
            }
                           );
            timeout--;
        }
    }
                                      );
    dispatch_resume(_timer);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
