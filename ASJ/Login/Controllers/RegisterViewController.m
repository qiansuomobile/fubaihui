//
//  RegisterViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterTwoViewController.h"
#import "RegisterTableViewCell.h"
#import "VerifyPhoneNumber.h"
#import "CustomTextField.h"

@interface RegisterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) RegisterTableViewCell *cell;

@property (nonatomic ,strong) UIButton *NextBtn;

@property (nonatomic ,strong) CustomTextField *userNameTF;//用户名
@property (nonatomic ,strong) CustomTextField *codeTF;//验证码
@property (nonatomic ,strong) CustomTextField *passwordTF;//密码
@property (nonatomic ,strong) CustomTextField *yaoqingCodeTF;//邀请码



@end

@implementation RegisterViewController

-(void)viewWillAppear:(BOOL)animated{
    
    //取消隐藏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    self.view.backgroundColor = [UIColor whiteColor];
//    [self createUI];
    
    //新注册流程
    [self createUINew];
}


-(void)createUINew{

    
    //用户名
    self.userNameTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, 40, MainScreen.width-100, 35)];
    self.userNameTF.textField.delegate = self;
    self.userNameTF.textField.placeholder = @"请输入手机号";
    self.userNameTF.iconImage.image = [UIImage imageNamed:@"user_accout"];
    [self.view addSubview:self.userNameTF];
    
    
    self.codeTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, self.userNameTF.frame.origin.y+self.userNameTF.frame.size.height+20, MainScreen.width-100, 35)];
    self.codeTF.textField.secureTextEntry = YES;
    self.codeTF.textField.delegate = self;
    self.codeTF.textField.placeholder = @"请输入验证码";
    self.codeTF.iconImage.image = [UIImage imageNamed:@"user_password"];
    [self.view addSubview:self.codeTF];
    
    
    self.passwordTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, self.codeTF.frame.origin.y+self.codeTF.frame.size.height+20, MainScreen.width-100, 35)];
    self.passwordTF.textField.secureTextEntry = YES;
    self.passwordTF.textField.delegate = self;
    self.passwordTF.textField.placeholder = @"请输入密码";
    self.passwordTF.iconImage.image = [UIImage imageNamed:@"user_password"];
    [self.view addSubview:self.passwordTF];
    
//    self.yaoqingCodeTF = [[CustomTextField alloc] initWithFrame:CGRectMake(50, self.passwordTF.frame.origin.y+self.passwordTF.frame.size.height+20, MainScreen.width-100, 35)];
//    self.yaoqingCodeTF.textField.secureTextEntry = YES;
//    self.yaoqingCodeTF.textField.delegate = self;
//    self.yaoqingCodeTF.textField.placeholder = @"请输入邀请码(非必填)";
//    self.yaoqingCodeTF.iconImage.image = [UIImage imageNamed:@"user_password"];
//    [self.view addSubview:self.yaoqingCodeTF];

    
    
    

    UIButton *sureLoginGuideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureLoginGuideButton.frame     = CGRectMake(0, YzHeight -150-88, YzWidth, 20);
    sureLoginGuideButton.titleLabel.font = [UIFont systemFontOfSize:11];
    [sureLoginGuideButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [sureLoginGuideButton setTitle:@"确认注册表示您已阅读并同意<<福百惠注册协议>>" forState:UIControlStateNormal];
    [self.view addSubview:sureLoginGuideButton];
    [sureLoginGuideButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame     = CGRectMake(50, YzHeight - 110-88, YzWidth-100, 44);
    loginBtn.backgroundColor = [UIColor greenColor];
    [loginBtn setTitle:@"注册" forState:UIControlStateNormal];
    [self.view addSubview:loginBtn];
    
    
    UIButton *backLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backLoginBtn.frame     = CGRectMake(YzWidth/2-80, YzHeight - 60-88, 160, 30);
    [backLoginBtn setTitle:@"已有账号，去登录" forState:UIControlStateNormal];
    [backLoginBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    backLoginBtn.titleLabel.font = [UIFont systemFontOfSize:13];

    [self.view addSubview:backLoginBtn];
    

}


-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)TapNext{
    
    NSLog(@"咋回事 ====    %@",_cell.textField.text);
    
    //手机号验证
    
    BOOL isPhoneNumber = [VerifyPhoneNumber checkTelNumber:_cell.textField.text];
    
    if (isPhoneNumber) {
        
        RegisterTwoViewController *vc = [[RegisterTwoViewController alloc] init];
        
        vc.phoneNumber = _cell.textField.text;
        
        [self.navigationController pushViewController:vc animated:YES];
   
    }else{
    
        [MBProgressHUD showSuccess:@"请输入正确的手机号码" toView:self.view];
        
        return;
    
    }
}


#pragma mark --- UITableViewDelegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    RegisterTableViewCell *cell = [[RegisterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    cell.textField.delegate = self;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _cell = cell;
    
    return cell;

}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
   
    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    //下一步
    UIButton *nextBtn = [[UIButton alloc] init];
    
    [nextBtn setBackgroundColor:Gray];
    
    [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    
    nextBtn.layer.cornerRadius = 3.0*Kwidth;
    
    [nextBtn addTarget:self action:@selector(TapNext) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:nextBtn];
    
    self.NextBtn = nextBtn;
    
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(view.mas_top).offset(20);
        make.right.equalTo(view.mas_right).offset(-10);
        make.height.mas_equalTo(50);
    }];
    
    UIButton *readBtn = [[UIButton alloc] init];
    [readBtn setBackgroundImage:[UIImage imageNamed:@"icon_accept"] forState:UIControlStateNormal];
    [view addSubview:readBtn];
    [readBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(nextBtn.mas_bottom).offset(15);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    UILabel *readL = [[UILabel alloc] init];
    readL.text = @"我已阅读并同意";
    readL.textAlignment = NSTextAlignmentCenter;
    readL.font = [UIFont systemFontOfSize:14];
    [view addSubview:readL];
    [readL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(readBtn.mas_right).offset(3);
        make.top.equalTo(nextBtn.mas_bottom).offset(17);
    }];
    [readL sizeToFit];
    
    UILabel *delegateL = [[UILabel alloc ]init];
    delegateL.text = @"使用条款和隐私政策";
    delegateL.textColor = [UIColor colorWithRed:69/255.0 green:166/255.0 blue:212/255.0 alpha:1.0];
    delegateL.textAlignment = NSTextAlignmentCenter;
    delegateL.font = [UIFont systemFontOfSize:14];
    [view addSubview:delegateL];
    [delegateL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(readL.mas_right).offset(0);
        make.top.equalTo(nextBtn.mas_bottom).offset(17);
    }];
    [delegateL sizeToFit];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    return view;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 200;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

#pragma mark --- UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    [textField resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"执行");
    [_cell.textField resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    if (textField == _cell.textField) {
    
        if (textField.text.length >=10){
            
            self.NextBtn.backgroundColor = [UIColor redColor];
            
//            return NO;
            return YES;

        
        }else{
            
            self.NextBtn.backgroundColor = Gray;
           
            return YES;
        
        }
//    BOOL isPhoneNumber = [VerifyPhoneNumber isMobileNumber:_cell.textField.text];
//
//            if (!isPhoneNumber) {
//                
//                [MBProgressHUD showSuccess:@"请输入正确的手机号码" toView:self.view];
//
////                RegisterTwoViewController *vc = [[RegisterTwoViewController alloc] init];
////                
////                vc.phoneNumber = _cell.textField.text;
////                
////                [self.navigationController pushViewController:vc animated:YES];
//            }else{
//    
//            }
//            
//        }
        
   
//    }
    
//    return YES;
}
    

@end
