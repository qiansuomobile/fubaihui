//
//  YzCommitListViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzCommitListViewController.h"
#import "YzCommitListCell.h"
@interface YzCommitListViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *commitTableView;

@property (nonatomic, strong)NSMutableArray *dataSource;

@end

@implementation YzCommitListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"宝贝评价";
    _dataSource = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestCommitData];
}

#pragma mark - tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifer = @"commitCell";
    YzCommitListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YzCommitListCell" owner:self options:nil]lastObject];
    }
    
    NSDictionary *dic = _dataSource[indexPath.row];
    
    cell.nameLabel.text = dic[@"user_name"];
    cell.timeLabel.text = dic[@"add_time"];
    
    NSString *content = dic[@"content"];
    cell.infoLabel.text = content.length > 0 ? content : @"无评价内容";
    
    NSArray *imgArr = dic[@"img"];
    for (int i = 0; i < imgArr.count; i++) {
        if (i == 0) {
            [cell.imageView1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL,imgArr[i]]] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        }
        if (i == 1) {
            [cell.imageView2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL,imgArr[i]]] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        }
        if (i == 2) {
            [cell.imageView3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL,imgArr[i]]] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 180;
}

#pragma mark - URL Request
- (void)requestCommitData{
    
    NSString *str = [NSString stringWithFormat:@"/APP/Public/evaluate/id/%@/page/1",_goodsID];
    [NetMethod GET:FBHRequestUrl(str) parameters:nil success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            
            if ([responseObject[@"data"] isKindOfClass:[NSNull class]]) {
                [MBProgressHUD showError:@"暂无评价" toView:self.view];
                return;
            }
            NSArray *arr = responseObject[@"data"];
            for (NSDictionary *dic in arr) {
                [_dataSource addObject:dic];
            }
            [self.commitTableView reloadData];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
