//
//  YzRegisterViewController.m
//  ASJ
//
//  Created by Dororo on 2019/6/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzRegisterViewController.h"
#import "JPUSHService.h"
#import "WXUtil.h"
#import "AdvertisementViewController.h"

@interface YzRegisterViewController ()

//手机号
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTextField;

//验证码
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;

//密码
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

//邀请码
@property (weak, nonatomic) IBOutlet UITextField *recommendCodeTextField;
//获取验证码按钮
@property (weak, nonatomic) IBOutlet UIButton *pinButton;

@end

@implementation YzRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"注册";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - ACTION
//发送验证码
- (IBAction)sendVerificationCode:(UIButton *)sender {
    [self sendSMSCodeWithPhoneNumber:self.phoneNumTextField.text result:^(bool r) {
        if (r) {
            //获取验证码成功，倒计时
            [self safeVerifyGetMessageCode:sender];
        }else{
            //获取失败，可重新获取
            sender.enabled = YES;
            [sender setTitle:@"重新获取" forState:UIControlStateNormal];
        }
    }];
}

//注册协议
- (IBAction)showRegistrationAgreement:(UIButton *)sender {
    AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
    adVC.title = @"注册协议";
    adVC.Ad_ID = @"/Admin/Public/hidden";
    [self.navigationController pushViewController:adVC animated:YES];
}

//注册
- (IBAction)userRegisterClick:(UIButton *)sender {
    [self userRegister];
}


/**
 获取验证码后倒计时

 @param sender sms button
 */
- (void)safeVerifyGetMessageCode:(UIButton *)sender {
    
    __block UIBackgroundTaskIdentifier bgTask;
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid)
            {
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    
    __block int timeout = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        if(timeout <= 0){
            dispatch_source_cancel(timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:@"重新获取" forState:UIControlStateNormal];
                sender.enabled = YES;
            });
        } else {
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%ds", seconds==0?60:seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:[NSString stringWithFormat:@"%@",strTime] forState:UIControlStateNormal];
                sender.enabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(timer);
}


#pragma mark - URL REQUEST

/**
 获取短信验证码

 @param num 手机号
 @param result 获取结果是否成功
 */
- (void)sendSMSCodeWithPhoneNumber:(NSString *)num result:(void (^)(bool r))result{
    
    NSDictionary *dic = @{@"phone":num};
    
    [NetMethod Post:FBHRequestUrl(kUrl_send_sms) parameters:dic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        int code = [[dic objectForKey:@"code"] intValue];
        if (code ==200) {
            //发送成功
            result(YES);
        }else{
            result(NO);
            [MBProgressHUD showError:[responseObject objectForKey:@"returncode"] toView:self.view];
        }
    } failure:^(NSError *error) {
        result(NO);
        [MBProgressHUD showError:error.description toView:self.view];
    }];   
}


/**
 用户注册
 */
- (void)userRegister{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:self.phoneNumTextField.text forKey:@"username"];
    [dic setObject:self.passwordTextField.text forKey:@"password"];
    [dic setObject:[NSNumber numberWithInt:[self.pinTextField.text intValue]] forKey:@"yzm"];
    [dic setObject:@1 forKey:@"agree"];
    if (self.recommendCodeTextField.text.length > 0) {
        [dic setObject:[NSNumber numberWithInt:[self.recommendCodeTextField.text intValue]]
                forKey:@"recommend_code"];
    }else{
        [MBProgressHUD showError:@"请填写邀请码" toView:self.view];
        return;
    }
    
    [NetMethod Post:FBHRequestUrl(kUrl_user_register) parameters:dic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        int code = [[dic objectForKey:@"code"] intValue];
        if (code == 200) {
            [self clickLogin];
        }else{
            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}

-(void)clickLogin{
    NSDictionary *loginDic = @{@"username":self.phoneNumTextField.text,@"password":self.passwordTextField.text};
    
    [NetMethod Post:FBHRequestUrl(kUrl_user_login) parameters:loginDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [[dic objectForKey:@"code"] intValue];
        
        if (code == 200){
            
            NSDictionary *dataDic = dic[@"data"];
            int userid = [[dataDic objectForKey:@"uid"] intValue];
            
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DriverISLogIN"];
            NSString *uid = [NSString stringWithFormat:@"%d",userid];
            
            [[NSUserDefaults standardUserDefaults] setObject:uid forKey:@"LoveDriverID"];
            [[NSUserDefaults standardUserDefaults] setObject:self.phoneNumTextField.text forKey:@"LoveUserName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [JPUSHService setAlias:uid completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                NSLog(@"iResCode %ld, iAlias %@, seq %ld", (long)iResCode, iAlias, (long)seq);
            } seq:200];
            
            [self loginWithUsername:uid];
            //获取用户token
            [NetMethod getUserToken:^(BOOL result) {
                if (result) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DriverISLogIN"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [NetMethod sendDeviceToken:FBH_DEVICE_TOKEN];
                    
                    [self dismissLogInController];
                }else{
                    [MBProgressHUD showError:@"获取用户token失败" toView:self.view];
                }
            }];
        }else{
            
            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
        }
        
    } failure:^(NSError *error) {
        
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}

//点击登陆后的操作
- (void)loginWithUsername:(NSString *)uid{
    
    NSString *sign = [NSString stringWithFormat:@"fbhim%@",uid];
    NSString *signmd5 = [WXUtil md5:sign];
    NSLog(@"🐸%@", signmd5);
    
    [[EMClient sharedClient] registerWithUsername:uid password:signmd5 completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            NSLog(@"error1注册成功");
        }else{
            NSLog(@"error1注册失败");
        }
        
        [[EMClient sharedClient] loginWithUsername:uid password:signmd5 completion:^(NSString *aUsername, EMError *aError) {
            if (!aError) {
                NSLog(@"error2登录成功");
                //设置是否自动登录
                [[EMClient sharedClient].options setIsAutoLogin:YES];
                
                //保存最近一次登录用户名
                //        [weakself saveLastLoginUsername];
                //发送自动登陆状态通知
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
            }else{
                NSLog(@"error2 -登录失败");
            }
        }];
    }];
}

- (void)dismissLogInController{
    UIViewController * presentingViewController = self.presentingViewController;
    while (presentingViewController.presentingViewController) {
        presentingViewController = presentingViewController.presentingViewController;
    }
    [presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
