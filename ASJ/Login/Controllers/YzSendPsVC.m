//
//  YzSendPsVC.m
//  ASJ
//
//  Created by Jack on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzSendPsVC.h"
@interface YzPassWordDone : UITextField

@end

@implementation YzPassWordDone

//控制左视图的大小
-(CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(5, 10, 11, 17.5);
    
    return inset;
    
    //return CGRectInset(bounds,50,0);
}


//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds
{
    //return CGRectInset(bounds, 50, 0);
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);//更好理解些
    
    return inset;
    
}
//
////控制编辑文本的位置
//
-(CGRect)editingRectForBounds:(CGRect)bounds
{
    //return CGRectInset( bounds, 10 , 0 );
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    
    return inset;
}

@end

@interface YzSendPsVC ()<UITextFieldDelegate>

@property (nonatomic , strong) YzPassWordDone *oldPW;

@property (nonatomic , strong) YzPassWordDone *onceNewPW;
@end

@implementation YzSendPsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.title = @"修改密码";
    
    [self creatViews];

}

-(void)creatViews{
    
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    self.title = @"修改密码";
    
    self.oldPW = [[YzPassWordDone alloc]initWithFrame:CGRectMake(0, 42, YzWidth, 40)];
    //self.oldPW.delegate = self;
    
    self.oldPW.secureTextEntry = YES;
    
    self.oldPW.placeholder =@"请输入您的新密码";
    
    self.oldPW.delegate = self;
    
    self.oldPW.font = [UIFont systemFontOfSize:15];
    
    self.oldPW.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.oldPW];
    
    
    
    
    
    self.onceNewPW = [[YzPassWordDone alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.oldPW.frame) + 40, YzWidth, 40)];
    
    self.onceNewPW.placeholder =@"请确认您的新密码";
    
    self.onceNewPW.delegate = self;
    
    self.onceNewPW.secureTextEntry = YES;
    
    self.onceNewPW.font = [UIFont systemFontOfSize:15];
    
    self.onceNewPW.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.onceNewPW];
    
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    button.frame = CGRectMake(10, CGRectGetMaxY(self.onceNewPW.frame) + 20, YzWidth-20, 38);
    
    [button setTitle:@"提交" forState:UIControlStateNormal];
    
    [button setTintColor:[UIColor whiteColor]];
    
    button.layer.masksToBounds = YES;
    
    [button.layer setCornerRadius:3];
    
    button.backgroundColor = RGBColor(255, 64, 31);
    
    [button addTarget:self action:@selector(YZtapModifyNENEWWPW) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];



}




//确认新密码

-(void)YZtapModifyNENEWWPW{
    
    if (self.onceNewPW.text.length<6 || self.oldPW.text.length<6) {
        [MBProgressHUD showSuccess:@"密码过于简单" toView:self.view];
    }else if(![self.onceNewPW.text isEqualToString:self.oldPW.text]){
        [MBProgressHUD showSuccess:@"两次输入不一致" toView:self.view];
    }else{
        if ([self.oldPW.text isEqualToString:self.onceNewPW.text]) {
            
            NSDictionary *dic = @{@"phone":self.UserName,@"userpassword":self.onceNewPW.text};
            
                NSLog(@"%@   -------   %@", self.UserName,self.onceNewPW.text);
            //
            [NetMethod Post:LoveDriverURL(@"APP/User/forget") parameters:dic success:^(id responseObject) {
                
                NSDictionary *dic = responseObject;
                
                NSLog(@"%@",dic);
                
                int code = [[dic objectForKey:@"code"] intValue];
                
                if (code == 200) {
                    NSString *msg = [dic objectForKey:@"msg"];

                    [MBProgressHUD showSuccess:msg toView:self.view];
                    
                    [self performSelector:@selector(BackToHome) withObject:nil afterDelay:1.5];
                    
                }else{
                    
                    NSString *msg = [dic objectForKey:@"msg"];
                    
                    [MBProgressHUD showSuccess:msg toView:self.view];
                    
                }
                
            } failure:^(NSError *error) {
                
            }];
    }
   }
}


-(void)BackToHome{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
