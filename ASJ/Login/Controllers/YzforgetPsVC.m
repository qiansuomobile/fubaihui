//
//  YzforgetPsVC.m
//  ASJ
//
//  Created by Jack on 16/9/1.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzforgetPsVC.h"

#import "YzSendPsVC.h"

@interface YZPassword : UITextField

@end

@implementation YZPassword

//控制左视图的大小
-(CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(5, 10, 11, 17.5);
    
    return inset;
    
    //return CGRectInset(bounds,50,0);
}


//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds
{
    //return CGRectInset(bounds, 50, 0);
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);//更好理解些
    
    return inset;
    
}
//
////控制编辑文本的位置
//
-(CGRect)editingRectForBounds:(CGRect)bounds
{
    //return CGRectInset( bounds, 10 , 0 );
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    
    return inset;
}


@end


@interface YzforgetPsVC ()<UITextFieldDelegate>

@property (nonatomic , strong )YZPassword *PhoneTf;

@property (nonatomic , strong)YZPassword *CodeNumTf;

@property (nonatomic , strong) UIButton *getCodeBtn;

@property (nonatomic , strong) NSString * LDContent;

@end

@implementation YzforgetPsVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"找回密码";
    self.view.backgroundColor = RGBColor(237, 237, 237);
    [self ForgetCodeUI];
}

-(void)ForgetCodeUI{
    
    self.PhoneTf = [[YZPassword alloc]init];
    
    self.PhoneTf.frame = CGRectMake(0, 20, YzWidth, 40);
    
    self.PhoneTf.placeholder = @"请输入手机号";
    
    [self.view addSubview:self.PhoneTf];
    
    self.PhoneTf.backgroundColor = [UIColor whiteColor];
    
    self.PhoneTf.leftView.frame = CGRectMake(0, 0, 20, 40);
    
    
    //    self.PhoneTf seteditingRectForBounds = CGRectMake(0, 0, 0, 0);
    //    [self.PhoneTf setLeftViewMode:UITextFieldViewModeAlways];
    
    
    self.CodeNumTf = [[YZPassword alloc]init];
    
    self.CodeNumTf.frame = CGRectMake(0, 90, YzWidth - 100, 40 );
    
    [self.view addSubview:self.CodeNumTf];
    
    self.CodeNumTf.placeholder = @"验证码";
    
    self.CodeNumTf.backgroundColor = [UIColor whiteColor];
    
    
    
    self.getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.getCodeBtn.frame =CGRectMake(YzWidth - 100, 90, 100, 40);
    
    [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
    self.getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [self.getCodeBtn addTarget:self action:@selector(setGetCodeBtn) forControlEvents:UIControlEventTouchUpInside];
    
    [self.getCodeBtn setBackgroundColor:[UIColor redColor]];
    
    
    
    [self.view addSubview:self.getCodeBtn];
    
    
    UIButton *nextType = [UIButton buttonWithType:UIButtonTypeCustom];
    
    nextType.frame = CGRectMake(20, 200, YzWidth-40, 40);
    
    [nextType setBackgroundColor:[UIColor redColor]];
    
    [nextType setTitle:@"下一步" forState:UIControlStateNormal];
    
    nextType.layer.cornerRadius = 3;
    
    [nextType addTarget:self action:@selector(NextType) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextType];
}

-(void)setGetCodeBtn{
    
    
    BOOL isPhoneNumber = [VerifyPhoneNumber checkTelNumber:self.PhoneTf.text];
    
    if (isPhoneNumber) {
        NSDictionary *dic = @{@"phone":self.PhoneTf.text};
        
        [NetMethod Post:LoveDriverURL(@"APP/Public/revise") parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
            NSDictionary *dic = responseObject;
            int code = [[dic objectForKey:@"code"] intValue];
            if (code ==200) {
                [self startTime];
//                int content = [[dic objectForKey:@"content"] intValue];
                
                self.LDContent = [dic objectForKey:@"content"] ;
            }
        } failure:^(NSError *error) {
            
        }];
    }else{
        [MBProgressHUD showSuccess:@"请输入正确的手机号码" toView:self.view];
        return;
    }
}
-(void)NextType
{
    BOOL isPhoneNumber = [VerifyPhoneNumber checkTelNumber:self.PhoneTf.text];
    
    if (isPhoneNumber) {
        
        
        NSString *code = [NSString stringWithFormat:@"%@",self.LDContent];
        
        if ([code isEqualToString:self.CodeNumTf.text] ) {
            
            YzSendPsVC *vc = [[YzSendPsVC alloc]init];
            
            vc.UserName = self.PhoneTf.text;
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
         
            [MBProgressHUD showSuccess:@"验证码错误" toView:self.view];
        }
        
    }else{
        
        [MBProgressHUD showSuccess:@"请输入正确的手机号码" toView:self.view];
        
        return;
    }
    
}

-(void)startTime
{
    //开启倒计时
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer,^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置界面的按钮显示 根据自己需求设置
                [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                self.getCodeBtn.userInteractionEnabled = YES;
            });
        }
        else
        {
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%@秒后可重复",strTime] forState:UIControlStateNormal];
                self.getCodeBtn.userInteractionEnabled = NO;
            }
                           );
            timeout--;
        }
    }
                                      );
    dispatch_resume(_timer);
}
-(void)viewWillAppear:(BOOL)animated{
    //取消隐藏
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];

}
@end
