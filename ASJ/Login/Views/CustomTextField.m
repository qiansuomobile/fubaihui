//
//  CustomTextField.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        _iconImage = [[UIImageView alloc] init];
        _iconImage.frame = CGRectMake(10, 7.5, 20, 20);
        [self addSubview:_iconImage];
        
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(_iconImage.frame.origin.y+_iconImage.frame.size.width+6, 7, self.frame.size.width-_iconImage.frame.size.width-_iconImage.frame.origin.y, self.frame.size.height-8)];
        [self addSubview:_textField];
        
        
        
        
    }
    
    return self;
    
}


@end
