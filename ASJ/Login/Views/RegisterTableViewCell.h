//
//  RegisterTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewCell : UITableViewCell
@property (nonatomic,strong) UITextField *textField;
@end
