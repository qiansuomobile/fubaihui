//
//  RegisterTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterTableViewCell.h"

@implementation RegisterTableViewCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *countryL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 60)];
    countryL.text = @"中国+86";
    countryL.font = [UIFont systemFontOfSize:20];
    countryL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:countryL];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(countryL.frame.size.width+countryL.frame.origin.x+5, 6, 1, 60-12)];
    lineView.backgroundColor = Gray;
    [self addSubview:lineView];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(lineView.frame.origin.x+6, 0, MainScreen.width-lineView.frame.origin.x-6, 60)];
    _textField.font = [UIFont systemFontOfSize:20];
    _textField.placeholder = @"请输入你的手机号";
    _textField.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:_textField];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
