//
//  RegisterThreeTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterThreeTableViewCell.h"

@implementation RegisterThreeTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    
    UIView *lineViewOne = [[UIView alloc] init];
    lineViewOne.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f  blue:237/255.0f  alpha:1.0];
    [self addSubview:lineViewOne];
    [lineViewOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(2);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
    }];
    
    UIView *lineViewTwo = [[UIView alloc] init];
    lineViewTwo.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f  blue:237/255.0f  alpha:1.0];
    [self addSubview:lineViewTwo];
    [lineViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(2);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    
    _passwordL = [[UILabel alloc] init];
    _passwordL.text = @"密码";
    _passwordL.font = [UIFont systemFontOfSize:16];
    _passwordL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_passwordL];
    [_passwordL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(90);
    }];
    
    _textField = [[UITextField alloc] init];
    _textField.secureTextEntry = YES;
    [self addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_passwordL.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
