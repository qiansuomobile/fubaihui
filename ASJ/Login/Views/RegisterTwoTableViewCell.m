//
//  RegisterTwoTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "RegisterTwoTableViewCell.h"

@implementation RegisterTwoTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *codeL = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 70, 60*KHeight)];
    codeL.text = @"验证码";
    codeL.textAlignment = NSTextAlignmentLeft;
    codeL.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:codeL];
    
    _textField = [[UITextField alloc] init];
    _textField.font = [UIFont systemFontOfSize:16*KHeight];
    _textField.placeholder = @"4位数字";
    _textField.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:_textField];
    
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(codeL.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(140*Kwidth);
    }];
    
    _codeBtn = [[UIButton alloc] init];
    [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_codeBtn setBackgroundColor:Gray];
    _codeBtn.layer.cornerRadius = 2.0*Kwidth;
    [self addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.right.equalTo(self.mas_right).offset(-8*Kwidth);
    }];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
