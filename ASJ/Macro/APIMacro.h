//
//  APIMacro.h
//  ASJ
//
//  Created by Dororo on 2019/6/27.
//  Copyright © 2019 TS. All rights reserved.
//

#ifndef APIMacro_h
#define APIMacro_h

#define IMG_NAMED(imgName) [UIImage imageNamed:imgName]

// 判断是否是ipad
#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

// 判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
#define SafeAreaBottom ((IS_IPHONE_X == YES || IS_IPHONE_Xr == YES || IS_IPHONE_Xs == YES || IS_IPHONE_Xs_Max == YES) ? 34.0 : 0)

#define FBHBaseURL  @"http://wasj.zhangtongdongli.com"
//#define FBHBaseURL  @"http://newwasj.zhangtongdongli.com"

#define TOKEN_KEY   @"okhgkuejg97DhukoodkjkdjYIidnjkdjiipsteom"

#define FBHRequestUrl(detailUrl) [NSString stringWithFormat:@"%@%@",FBHBaseURL,detailUrl]

#define kUrl_public(id, type) [NSString stringWithFormat:@"/Admin/Public/impublic/id/%@/type/%@",id, type]

//平台热线
#define kUrl_hot_phone @"18210521289"

//用户注册
#define kUrl_user_register  @"/APP/user/register"
//用户登录
#define kUrl_user_login     @"/APP/user/login"
//获取用户信息
#define kUrl_user_info      @"/APP/Member/user_info"
//获取token
#define kUrl_get_token      @"/APP/Public/get_token"


//短信验证码
#define kUrl_send_sms       @"/APP/Public/sendsms"

//上传图片
#define kUrl_img_upload     @"/APP/Xinv/Xupload"


// 首页轮播图
#define kUrl_home_banner    @"/APP/Public/bander/name/n_sybn"
//首页精选和公告
#define kUrl_home_jingxuan  @"/APP/Xone/goodslist"


//商品详情
#define kUrl_goods_detail   @"/APP/Shop/goods_detail/id/"
//商品分类
#define kUrl_goods_category @"/APP/public/category_list"
//商品列表
#define kUrl_goods_list     @"/APP/Shop/index"

//收藏商品
#define kUrl_goods_collect  @"/APP/Member/goods_collect"
//我的收藏列表
#define kUrl_collect_list   @"/APP/Member/goods_collect_list"
//取消收藏
#define kUrl_collect_cancel @"/APP/Member/cancel_goods_collect"


//获取购物车列表
#define kUrl_cart_list      @"/APP/Order/cart_list"
//添加物品到购物车
#define kUrl_add_cart       @"/APP/Order/add_cart"
//删除购物车物品
#define kUrl_del_goods      @"/APP/Order/del_good_car"
//购物车数量改变
#define kUrl_setinc_goods   @"/APP/Order/cart_goods_setInc"

//获取商家入驻信息
#define kUrl_mall_join_info @"/APP/Xinv/sjtojoininfo"
//提交商家入驻申请
#define kUrl_mall_join      @"/APP/Xinv/sjtojoinApi"
//加盟商家筛选
#define kUrl_mall_screening @"/APP/Xtojoin/garage_list"

//普通订单提交显示页面
#define kUrl_show_order     @"/APP/Order/add_order"
//提交普通订单
#define kUrl_add_order      @"/APP/Order/add_order_do"

//获取用户地址列表
#define kUrl_place_list     @"/APP/Member/placeList"
//增加收货地址
#define kUrl_place_add      @"/APP/Member/placeAdd"
//编辑地址
#define kUrl_place_Edit     @"/APP/Member/placeEdit"

//获取积分余额
#define kUrl_get_wallet     @"/APP/Xtojoin/wallet"
//金积分充值换算
#define kUrl_conversios     @"/APP/Xuser/conversios"
//积分支付
#define kUrl_order_pay      @"/APP/Order/pay"
//金积分充值
#define kUrl_gold_recharge  @"/APP/Xuser/index"
//金积分充值换算
#define kUrl_exchange_rate  @"/APP/Xuser/conversios"
//金积分提现到余额
#define kUrl_pick_balace    @"/APP/Xuser/refund"
//金积分、银积分转让
#define kUrl_give_integral  @"/APP/Member/give_integral"

//获取服务端签名
#define kUrl_get_sign       @"/APP/public/get_sign"

//余额提现到支付宝、微信
#define kUrl_pick_otherpay  @"/APP/Xuser/withdrawal"
//提现绑定支付宝、微信id
#define kUrl_binding_pay    @"/APP/Xuser/wxopenid"
//检测是否绑定支付宝、微信
#define kUrl_isbangding     @"/APP/Xuser/isbangding"

//微信支付
#define kUrl_pay_wxpay      @"/APP/Xalpay/wxpayandroid"
//支付宝支付
#define kUrl_pay_alipay     @"/APP/Xalpay/tcalpay"

//微信支付回调
#define kUrl_wechatapp      @"/APP/Xalpay/Wechatapp"
//支付宝支付回调
#define kUrl_alipayapp     @"/APP/Xalpay/alipayapp"

//我的订单
#define kUrl_my_order       @"/APP/Member/my_order"
//订单详情
#define kUrl_order_detail   @"/APP/Member/order_detail"
//取消订单
#define kUrl_order_cancel   @"/APP/Member/cancel"
//查看物流
#define kUrl_courier_api    @"/APP/Xtojoin/Courier_api"
//确认收货
#define kUrl_receiving      @"/APP/Member/receiving"
//评价订单
#define kUrl_do_evaluate    @"/APP/Member/do_evaluate"
//获取退款理由
#define kUrl_refund_reason  @"/APP/Public/refund_reason"
//申请退款
#define kUrl_is_refund      @"/APP/Member/is_refund"

//获取商城点评列表
#define kUrl_circle_index   @"/APP/Circle/index"
//发表商城点评
#define kUrl_circle_add     @"/APP/Circle/add"
//商城点赞
#define kUrl_circle_laud    @"/APP/Circle/laud"
//商城评论
#define kUrl_circle_comment @"/APP/Circle/commentAdd"
//发布商城点评
#define kUrl_circle_add     @"/APP/Circle/add"

//我的推荐//yu
#define kUrl_mybill FBHRequestUrl(@"/APP/Xtojoin/mybill")

//我的账单
#define kUrl_my_bill        @"/APP/Xtojoin/myzhangdan"

//资讯
#define kUrl_information    @"/APP/Love/information"

//搜索会员
#define kUrl_friend_search  @"/APP/Friend/search"
//添加好友
#define kUrl_friend_add     @"/APP/Friend/add"
//删除好友
#define kUrl_friend_del     @"/APP/Friend/del"
//好友列表
#define kUrl_friend_list    @"/APP/Friend/index"

//商家入驻协议
#define kUrl_ruzhuxieyi     @"/Admin/Public/impublic/id/1620/type/3"

//隐私协议
#define kUrl_Screct         @"/Admin/Public/hidden"

//判断是否是商家
#define kUrl_pdjmsj         @"/APP/Xuser/pdjmsj"

//商家登录
#define kUrl_mall_login     @"/Admin/Public/login.html"

//商家支付码支付
#define kUrl_pay_store      @"/APP/Member/do_pay_store"

//获取商家优惠券列表
#define kUrl_mall_coupon    @"/APP/Public/coupon_list"
//兑换优惠券
#define kUrl_exchange_coupon @"/APP/Public/exchange_coupon"

#define kUrl_my_coupon      @"/APP/Xtojoin/preferential"

//获取用户头像
#define kUrl_user_touxiang  @"/APP/Xuser/touxiang"

//关于我们
#define kUrl_about_me       @"/Admin/Public/aboutt"

//我的推荐会员
#define kUrl_member_list    @"/APP/member/one_member_list"

//获取商家分类
#define kUrl_get_business   @"/APP/Public/get_business"


//上传设备token
#define kUrl_reveive_token  @"/APP/Push/receive_token"


#endif /* APIMacro_h */
