//
//  AppDelegate.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzqiandaoVC.h"
#import "WXUtil.h"
#import "WXApi.h"
#import "YzNavigationVC.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "CZNewFeatureController.h"

#import "MainViewController.h"
#import "AppDelegate+EaseMob.h"
#import "AppDelegate+Parse.h"

#import <Bugly/Bugly.h>
#import <UserNotifications/UserNotifications.h>
#import "JPUSHService.h"

#import <PushKit/PushKit.h>
#import "AudioTalkManager.h"

#define CZVersionKey @"version"

@interface AppDelegate ()<WXApiDelegate, UIApplicationDelegate, UNUserNotificationCenterDelegate, JPUSHRegisterDelegate, PKPushRegistryDelegate>

@end

@implementation AppDelegate
{
    NSString *supportVersion;

}


-(void)Yzapplication:(UIApplication *)application YzdidFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
    
        //初始化应用，appKey和appSecret从后台申请得
    [self huanxin:application withLaunchOption:launchOptions];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [Bugly startWithAppId:@"b4e87816a5"];
    if (DriverISLogIN) {
        [NetMethod getUserToken:^(BOOL result) {
            if (result) {
                NSLog(@"获取用户token成功");
            }else{
                NSLog(@"获取用户token失败");
            }
        }];
    }
    
}

#ifdef NSFoundationVersionNumber_iOS_9_x_Max

#pragma mark- JPUSHRegisterDelegate

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    
    NSDictionary * userInfo = notification.request.content.userInfo;
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
//    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
//    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    
    NSLog(@"ios10  以下推送    %@  ****\n%@\n%@\n%@",title,subtitle,body,content);

    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }else{
        
        NSLog(@"本地通知成功");
    }
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
   
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

// iOS 12 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center openSettingsForNotification:(UNNotification *)notification{
    if (notification && [notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //从通知界面直接进入应用
    }else{
        //从通知设置界面进入应用
    }
}

#endif

//


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self Yzapplication:application YzdidFinishLaunchingWithOptions:launchOptions];
    
    MainViewController *tabbar = [[MainViewController alloc]init];
    self.window.rootViewController = tabbar;
    
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];

    [JPUSHService setupWithOption:launchOptions
                           appKey:appKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    
    // 设置VoIP
//    PKPushRegistry *pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
//    pushRegistry.delegate = self;
//    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
//    
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
//        if (!error) {
//            NSLog(@"request authorization succeeded!");
//        }else{
//            NSLog(@"注册通知错误 %@", error.description);
//        }
//    }];
//    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
//        NSLog(@"settings %@",settings);
//    }];
    
    
    
    [WXApi registerApp:@"wxc1184669ab904cdd" withDescription:nil];
    
    [self.window makeKeyAndVisible];
    return YES;
    
}


- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        NSLog(@"联网成功");
    }
    else{
        NSLog(@"onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        NSLog(@"授权成功");
    }
    else {
        NSLog(@"onGetPermissionState %d",iError);
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
    
    NSLog(@"  deviceToken   ========     %@",deviceToken);
}
//实现注册APNs失败接口（可选）

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
    
//    NSLog(@"error -- %@",error);

}
// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application {
//    [JPUSHService setBadge:0];
    
    if (DriverISLogIN) {
        [NetMethod getUserToken:^(BOOL result) {
            if (result) {
                NSLog(@"获取用户token成功");
            }else{
                NSLog(@"获取用户token失败");
            }
        }];
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
 
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//
//    if ([url.absoluteString hasPrefix:@"wxc1184669ab904cdd"]) {
//
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"WXpayresult" object:@"1"];
//
//        return  [WXApi handleOpenURL:url delegate:self];
//
//    }
//
//    if ([url.host isEqualToString:@"safepay"]) {
//        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"AliPayResult" object:resultDic];
//        }];
//
//    }
//    return YES;
//}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    if ([url.absoluteString hasPrefix:@"wxc1184669ab904cdd"]) {
        
        return  [WXApi handleOpenURL:url delegate:self];
        
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AliPayResult" object:resultDic];
        }];
    }
    return YES;
}


#pragma mark - 微信支付回调
-(void)onResp:(BaseResp*)resp
{
    NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
    
    NSString *strTitle;
    
    if([resp isKindOfClass:[SendMessageToWXResp class]])
        
    {
        strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
    }
    
    if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        strTitle = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
                
            case WXSuccess:
            {
                strMsg = @"支付结果：成功！";
                
                NSLog(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"WXpayresult" object:@"1"];
            }
                break;
                
            default:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"WXpayresult" object:@"0"];
                
                strMsg = [NSString stringWithFormat:@"支付结果：失败！retcode = %d, retstr = %@", resp.errCode,resp.errStr];
                
                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
            }
                break;
                
        }
        
    }
    
}
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
//    if (_mainController) {
//        [_mainController didReceiveLocalNotification:notification];
//    }
//}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert);
}
//iOS 7 Remote Notification
- (void)application:(UIApplication *)application didReceiveRemoteNotification:  (NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}





+ (BOOL)setBadge:(int)value{
    return YES;
}


#pragma MARK - 环信
-(void)huanxin:(UIApplication *)application withLaunchOption:(NSDictionary *)launchOptions
{
    if (NSClassFromString(@"UNUserNotificationCenter")) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    }
    
    _connectionState = EMConnectionConnected;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
    //        [[UINavigationBar appearance] setBarTintColor:RGBACOLOR(30, 167, 252, 1)];
    //        [[UINavigationBar appearance] setTitleTextAttributes:
    //         [NSDictionary dictionaryWithObjectsAndKeys:RGBACOLOR(245, 245, 245, 1), NSForegroundColorAttributeName, [UIFont fontWithName:@ "HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];
    //    }
    NSString *apnsCertName = nil;
    #if DEBUG
        apnsCertName = @"环信开发环境PUSH";
    #else
        apnsCertName = @"环信生产环境PUSH";
    #endif
//    apnsCertName = @"woaisiji66";
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSString *appkey = [ud stringForKey:@"identifier_appkey"];
    // 设置Appkey
    EMOptions *options = [EMOptions optionsWithAppkey:@"66666616#fubaihui"];
    // 设置推送证书名称
    options.apnsCertName = apnsCertName;
    // 初始化SDK
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    if (!appkey)
        
    {
        //        appkey = @"66666616#lovedriver1";
        appkey = @"66666616#fubaihui";
        [ud setObject:appkey forKey:@"identifier_appkey"];
        [ud synchronize];
    }
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:appkey
                apnsCertName:apnsCertName
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    
}


#pragma mark - Push registry delegate

//获取VoIP的deviceToken
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)pushCredentials forType:(PKPushType)type{
    //   swift
    //    let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
    
    
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[pushCredentials.token length]];
    
    [pushCredentials.token enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    
    [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"FBH_DEVICE_TOKEN"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if (DriverISLogIN) {
        [NetMethod sendDeviceToken:string];
    }
    
    NSLog(@"VoIP deviceToken %@",string);
}

//清除问题token
- (void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(PKPushType)type{
    
}

//处理VoIP推送
- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type{
    NSLog(@"收到 VoIP消息 ⬇️%@", payload.dictionaryPayload);
    
//    NSDictionary *notifDic = payload.dictionaryPayload[@"aps"];
//
//    NSDictionary *alertDic = notifDic[@"alert"];
//
//    NSDictionary *dataDic = notifDic[@"data"];
//
//    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
//    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
//    content.title = [alertDic[@"title"] isKindOfClass:[NSNull class]] ? @"" : alertDic[@"title"];
//    content.body = [alertDic[@"body"] isKindOfClass:[NSNull class]] ? @"" : alertDic[@"body"];
//    content.badge = [NSNumber numberWithInt:[notifDic[@"badge"] intValue]];
//
//    UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
//                                                  triggerWithTimeInterval:1 repeats:NO];
//    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notifDic[@"unique_code"]
//                                                                          content:content trigger:trigger];
//    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//        NSLog(@"结束");
//    }];
//
//    if ([dataDic[@"open_sound"] intValue] == 1) {
//        [[AudioTalkManager sharedClinet] playAudioWithData:alertDic];
//    }
    
}


@end
