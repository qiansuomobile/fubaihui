//
//  CZNewFeatureCell.m
//  传智
//
//  Created by apple on 15-3-7.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "CZNewFeatureCell.h"
#import "MainViewController.h"
@interface CZNewFeatureCell ()

@property (nonatomic, weak) UIImageView *imageView;

//@property (nonatomic, weak) UIButton *shareButton;

@property (nonatomic, weak) UIButton *startButton;

@property (nonatomic, weak) UIImageView *Backimage;

@end

@implementation CZNewFeatureCell

//- (UIButton *)shareButton
//{
//    if (_shareButton == nil) {
//        
//       
//        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//       
//        [btn setTitle:@"进入APP" forState:UIControlStateNormal];
//        
//        [btn sizeToFit];
////        [btn setImage:[UIImage imageNamed:@"new_feature_share_false"] forState:UIControlStateNormal];
//        [btn setBackgroundImage:[UIImage imageNamed:@"guide page_getin"] forState:UIControlStateNormal];
////        [btn setImage:[UIImage imageNamed:@"new_feature_share_true"] forState:UIControlStateSelected];
////        btn.backgroundColor = GredColor;
//        
//        
//        [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        
//        
////        [self.contentView addSubview:btn];
//        
//        _shareButton = btn;
//        
//    }
//    
//    return _shareButton;
//}

- (UIButton *)startButton
{
    if (_startButton == nil) {
        
        UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     
        [startBtn setTitle:@"立即体验" forState:UIControlStateNormal];
        
        [startBtn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        
//        [startBtn setBackgroundImage:[UIImage imageNamed:@"ydy-anniu"] forState:UIControlStateNormal];
        
        [startBtn sizeToFit];
        
        [startBtn addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:startBtn];
        
        
        _startButton = startBtn;

    }
    return _startButton;
}

- (UIImageView *)imageView
{
    if (_imageView == nil) {
        
        UIImageView *imageV = [[UIImageView alloc] init];
        
        _imageView = imageV;
        
        // 注意:一定要加载contentView
        [self.contentView addSubview:imageV];
        
    }
    return _imageView;
}

//
//- (UIImageView *)Backimage
//{
//    if (_Backimage == nil) {
//        
//        UIImageView *imageV = [[UIImageView alloc] init];
//        
//        _Backimage = imageV;
//        
//        imageV.image = [UIImage imageNamed:@"引导页-1.png"];
//        
//        imageV.backgroundColor = [UIColor clearColor];
//        
//        // 注意:一定要加载contentView
//        [self.contentView addSubview:imageV];
//        
//    }
//    
//    return _Backimage;
//}


// 布局子控件的frame
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
    
    // 分享按钮
//    self.shareButton.center = CGPointMake(self.width * 0.5, self.height * 0.8);
    
//    self.Backimage.frame = self.bounds;
    
    // 开始按钮
//     self.startButton.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.8);
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    
    self.imageView.image = image;
}

// 判断当前cell是否是最后一页
- (void)setIndexPath:(NSIndexPath *)indexPath count:(int)count
{
    if (indexPath.row == count - 1) { // 最后一页,显示分享和开始按钮
//        self.shareButton.hidden = NO;
        self.startButton.hidden = YES;
        
        
    }else{ // 非最后一页，隐藏分享和开始按钮
//        self.shareButton.hidden = YES;
        self.startButton.hidden = YES;
    }
}

// 点击开始微博的时候调用
- (void)start

{
//    CZNewFeatureCell *mzgpc = [[CZNewFeatureCell alloc] init];
//
//    __weak typeof(CZNewFeatureCell) *weakMZ = mzgpc;
//    
//        [UIView animateWithDuration:2.0f
//                         animations:^{
//                             
//                             weakMZ.alpha = 0.0;
//                         }
//         
//                         completion:^(BOOL finished) {
//                             
//
//
//                         }];
    
    MainViewController *tabbarvc = [[MainViewController alloc]init];
    
    [UIApplication sharedApplication].keyWindow.rootViewController = tabbarvc;
    
    }

@end
