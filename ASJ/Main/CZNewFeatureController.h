//
//  CZNewFeatureController.h
//  传智微博
//
//  Created by apple on 15-3-7.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMPageControl.h"

// 定义枚举值 确定pageControl的位置
typedef NS_ENUM(NSInteger,UIPageControlOfStyle) {
    UIPageControlOfStyleNone,// 默认值
    UIPageControlOfStyleLeft,
    UIPageControlOfStyleCenter,
    UIPageControlOfStyleRight,
    
};


@interface CZNewFeatureController : UICollectionViewController


@property(nonatomic,assign,readwrite)UIPageControlOfStyle UIPageControlOfStyle;

@property (strong,nonatomic,readonly) SMPageControl * pageControl;


@end
