        //
//  CZNewFeatureController.m
//  传智微博
//
//  Created by apple on 15-3-7.
//  Copyright (c) 2015年 apple. All rights reserved.
//
#import "CZNewFeatureController.h"
#import "CZNewFeatureCell.h"
#import "MainViewController.h"
//#import "YzPageControl.h"


@interface CZNewFeatureController ()

@property (nonatomic, weak) UIPageControl *control;

//@property (nonatomic , strong)YzPageControl *control;

@end

@implementation CZNewFeatureController

static NSString *ID = @"cell";

- (instancetype)init
{

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    // 设置cell的尺寸
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    
    // 清空行距
    layout.minimumLineSpacing = 0;
    
    // 设置滚动的方向
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    return [super initWithCollectionViewLayout:layout];
}
// self.collectionView != self.view
// 注意： self.collectionView 是 self.view的子控件

// 使用UICollectionViewController
// 1.初始化的时候设置布局参数
// 2.必须collectionView要注册cell
// 3.自定义cell


- (void)viewDidLoad {
    
    [super viewDidLoad];

    // 注册cell,默认就会创建这个类型的cell
    
    [self.collectionView registerClass:[CZNewFeatureCell class] forCellWithReuseIdentifier:ID];
    
//    [self.collectionView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"引导页-1.png"]]];

    // 分页
    self.collectionView.pagingEnabled = YES;
  
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.bounces = YES;
    
    self.collectionView.showsHorizontalScrollIndicator = NO;
    

    // 添加pageController
//    [self setUpPageControl];
}
// 添加pageController
- (void)setUpPageControl
{
    // 添加pageController,只需要设置位置，不需要管理尺寸
    UIPageControl *control = [[UIPageControl alloc]init];
    
//    _pageControl = [[SMPageControl alloc]init];

//    self.pageControl.frame = CGRectMake(kWidth/2-50, kHeight*.75, 100, 10);
    

//    self.pageControl.center = CGPointMake(kWidth/2-50, kHeight*.75);
    
//    self.UIPageControlOfStyle = UIPageControlOfStyleCenter;

//
////WithFrame:CGRectMake(kWidth/2-50, kHeight*.8, 100, 20)];
    control.numberOfPages = 3;
//
    control.pageIndicatorTintColor = RGBColorAlpha(255, 255, 255, .6);
//
    control.currentPageIndicatorTintColor =[UIColor whiteColor];
//
////    [control setImagePageStateNormal:[UIImage imageNamed:@""]];
//    
    control.currentPage = 0;
//
//    // 设置center
    control.center = CGPointMake(self.view.frame.size.width * 0.5, self.view.frame.size.height *.88 );
    
    
//    YzPageControl *pageControl = [[YzPageControl alloc] initWithFrame:CGRectMake(kWidth/2-40,kHeight*.7, 80, 20)];
////    pageControl.backgroundColor = [UIColor clearColor];
//    pageControl.numberOfPages = 2;
//    pageControl.currentPage = 0;
//    [pageControl setImagePageStateNormal:[UIImage imageNamed:@"diandian_07.png"]];
//    [pageControl setImagePageStateHighlighted:[UIImage imageNamed:@"diandian_04.png"]];
//    [self.view addSubview:pageControl];
    
//    [self.pageControl setPageIndicatorImage:[UIImage imageNamed:@"diandian_07"]];
//    [self.pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"diandian_04"]];


    _control = control;
    
    [self.view addSubview:control];
}

#pragma mark - UIScrollView代理
// 只要一滚动就会调用
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 获取当前的偏移量，计算当前第几页
    int page = scrollView.contentOffset.x / scrollView.bounds.size.width + 0.5;
    // 设置页数
    _control.currentPage = page;
    
    //yu第四张引导图 左滑进入主页面
    if (scrollView.contentOffset.x > 2 * YzWidth) {
        MainViewController *tabbarvc = [[MainViewController alloc]init];
        [UIApplication sharedApplication].keyWindow.rootViewController = tabbarvc;
    }
}
#pragma mark - UICollectionView代理和数据源
// 返回有多少组
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
// 返回第section组有多少个cell
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row ==2) {
        
        MainViewController *tabbarvc = [[MainViewController alloc]init];
        
        [UIApplication sharedApplication].keyWindow.rootViewController = tabbarvc;

    }
}

// 返回cell长什么样子
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // dequeueReusableCellWithReuseIdentifier
    // 1.首先从缓存池里取cell
    // 2.看下当前是否有注册Cell,如果注册了cell，就会帮你创建cell
    // 3.没有注册，报错
    CZNewFeatureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];

    // 拼接图片名称 3.5 320 480
//    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    NSString *imageName = [NSString stringWithFormat:@"leader_%ld",indexPath.row+1];
//    if (screenH > 480) { // 5 , 6 , 6 plus
//        imageName = [NSString stringWithFormat:@"new_feature_%ld-568h",indexPath.row + 1];
//    }
    cell.image = [UIImage imageNamed:imageName];
    
    
    [cell setIndexPath:indexPath count:4];

//    leader_
    return cell;
    
}



@end
