//
//  YzNavigationVC.m
//  Yunyige
//
//  Created by Jack on 16/2/19.
//  Copyright © 2016年 ZLyunduan. All rights reserved.
//

#import "YzNavigationVC.h"


@interface YzNavigationVC ()<UIGestureRecognizerDelegate>

@end


#define TITLEFONT 19


#define KColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]


@implementation YzNavigationVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    [self CreatUI];
    
}

-(void)CreatUI{
    
    //    [self.navigationBar setBarTintColor:KColor(2, 40, 21)];
    
    [self.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationBar setTranslucent:NO];
    
    __weak typeof (self) weakSelf = self;
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.interactivePopGestureRecognizer.delegate = weakSelf;
        
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    
    // 3.设置导航栏文字的主题
    UIFont * font = [UIFont fontWithName:@"Arial-ItalicMT" size:TITLEFONT];
    
    NSDictionary * textAttributes = @{NSFontAttributeName:font,
                                      NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    [[UINavigationBar appearance]setTitleTextAttributes:textAttributes];
   
    self.navigationBar.barTintColor = RGBColor(26,149,68);
    
    
//    self.navigationBar.translucent = NO;
    
    // 4.修改所有UIBarButtonItem的外观
    UIBarButtonItem *barItem = [UIBarButtonItem appearance];
    NSShadow *shadow = [[NSShadow alloc]init];
    shadow.shadowColor = RGBColor(26,149,68);
    [barItem setTintColor:RGBColor(26,149,68)];
    [barItem setTintColor:[UIColor whiteColor]];
    
    
        //KColor(159, 211, 79)]
    
    
    
}
- (void)setTabBarHidden:(BOOL)hidden

{
    
    UIView *tab = self.tabBarController.view;
    
    CGRect  tabRect=self.tabBarController.tabBar.frame;
    
    if ([tab.subviews count] < 2) {
        
        return;
        
    }
    
    UIView *view;
    
    if ([[tab.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]]) {
        
        view = [tab.subviews objectAtIndex:1];
        
    } else {
        
        view = [tab.subviews objectAtIndex:0];
        
    }
    
    
    
    if (hidden) {
        
        view.frame = tab.bounds;
        
        tabRect.origin.y=[[UIScreen mainScreen] bounds].size.height+self.tabBarController.tabBar.frame.size.height;
        
    } else {
        
        view.frame = CGRectMake(tab.bounds.origin.x, tab.bounds.origin.y, tab.bounds.size.width, tab.bounds.size.height);
        
        
        tabRect.origin.y=[[UIScreen mainScreen] bounds].size.height-self.tabBarController.tabBar.frame.size.height;
        
    }
    
    
    
    [UIView animateWithDuration:0.5f animations:^{
        
        
        self.tabBarController.tabBar.frame=tabRect;
        
        
    }completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    if (self.viewControllers.count>0) {
        
        
        viewController.hidesBottomBarWhenPushed = YES;
        
        
        viewController.navigationItem.leftBarButtonItem = [self creatBackButton];
        
    }
    
    
    [super pushViewController:viewController animated:animated];
    
    
}

//NV统一左侧按钮

-(UIBarButtonItem *)creatBackButton
{
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"icon-left"]style:UIBarButtonItemStylePlain target:self action:@selector(popSelf)];
    
    
    
//    yu
//    UIView *backGroView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
//    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-left"]];
//    leftImageView.width = 27/3.0;
//    leftImageView.height = 48/3.0;
//    [backGroView addSubview:leftImageView];
//    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backGroView];;
//    leftImageView.centerY = backGroView.centerY;
    
    return barButtonItem;
    
    
}


//


-(void)popSelf

{
    
    [self popViewControllerAnimated:YES];
    
}



@end
