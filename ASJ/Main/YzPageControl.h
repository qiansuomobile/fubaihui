//
//  YzPageControl.h
//  Yunyige
//
//  Created by Jack on 16/3/31.
//  Copyright © 2016年 ZLyunduan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YzPageControl : UIPageControl


{
    
//    UIImage* activeImage;
//    
//    UIImage* inactiveImage;
    
    
    UIImage *imagePageStateNormal;

    UIImage *imagePageStateHighlighted;

    
}


- (id)initWithFrame:(CGRect)frame;

@property (nonatomic, retain) UIImage *imagePageStateNormal;

@property (nonatomic, retain) UIImage *imagePageStateHighlighted;

@end
