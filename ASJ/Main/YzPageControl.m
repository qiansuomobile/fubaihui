//
//  YzPageControl.m
//  Yunyige
//
//  Created by Jack on 16/3/31.
//  Copyright © 2016年 ZLyunduan. All rights reserved.
//

#import "YzPageControl.h"


@interface YzPageControl()
// 声明一个私有方法, 该方法不允许对象直接使用

- (void)updateDots;

@end


@implementation YzPageControl




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@synthesize imagePageStateNormal;

@synthesize imagePageStateHighlighted;


//-(id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    
//    activeImage = [UIImage imageNamed:@"diandian_04.png"];
//    
//    inactiveImage = [UIImage imageNamed:@"diandian_07.png"];
//    
//    
//    
//    return self;
//}

- (id)initWithFrame:(CGRect)frame { // 初始化
    self = [super initWithFrame:frame];
    return self;
}



- (void)setImagePageStateNormal:(UIImage *)image {  // 设置正常状态点按钮的图片
//    [imagePageStateHighlighted release];
    
    imagePageStateHighlighted = image;
    
    [self updateDots];
}

- (void)setImagePageStateHighlighted:(UIImage *)image { // 设置高亮状态点按钮图片

    imagePageStateNormal = image ;

    [self updateDots];
}
//
//-(void)updateDots
//
//{
//    for (int i=0; i<[self.subviews count]; i++) {
//        
//        UIImageView* dot = [self.subviews objectAtIndex:i];
//        
//        CGSize size;
//        
//        size.height = 7;     //自定义圆点的大小
//        
//        size.width = 7;      //自定义圆点的大小
//        [dot setFrame:CGRectMake(dot.frame.origin.x, dot.frame.origin.y, size.width, size.width)];
//        if (i==self.currentPage)dot.image=activeImage;
//        
//        else dot.image=inactiveImage;
//    }
//    
//}

- (void)updateDots { // 更新显示所有的点按钮
    
    if (imagePageStateNormal || imagePageStateHighlighted)
    {
        NSArray *subview = self.subviews;  // 获取所有子视图
        for (NSInteger i = 0; i < [subview count]; i++)
        {
            UIImageView *dot = [subview objectAtIndex:i];  // 以下不解释, 看了基本明白
            dot.image = self.currentPage == i ? imagePageStateNormal : imagePageStateHighlighted;
        }
    }
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event { // 点击事件
    [super endTrackingWithTouch:touch withEvent:event];
    [self updateDots];
}

//-(void)setCurrentPage:(NSInteger)page
//
//{
//    
//    [super setCurrentPage:page];
//    
//    [self updateDots];
//    
//}

@end
