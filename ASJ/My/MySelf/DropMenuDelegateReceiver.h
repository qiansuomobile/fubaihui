//
//  DropMenuDelegateReceiver.h
//  ASJ
//
//  Created by zhang ming on 2018/3/1.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DropMenuDelegateReceiver : NSObject<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, copy) void (^block1)(void);
@property (nonatomic, copy) void (^block2)(void);
@property (nonatomic, copy) void (^block3)(void);
@end
