//
//  MyViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactListViewController.h"

@interface MyViewController : UIViewController
@property (nonatomic, strong) ContactListViewController *contactsVC;

@end
