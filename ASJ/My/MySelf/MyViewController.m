//
//  MyViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
//YZ
#import "YzNavigationVC.h"
#import "MyViewController.h"
#import "YzCollectionViewController.h"
#import "YzwalletViewController.h"
#import "YzMyOrderVC.h"
#import "YzSetingViewController.h"
#import "YZpersonaldataVC.h"
#import "YzDriverAllOrderVC.h"
#import "LoginViewController.h"
#import "YzHistroyVC.h"
#import "YzMyTuiJianVC.h"
#import "YzMyTuiJinaCodeVC.h"
#import "YzMyLocationVC.h"
#import "YzMyCouponTableViewController.h"
#import "YzMallRuzhuViewController.h"
#import "AdvertisementViewController.h"

//#define IsLogin [[NSUserDefaults standardUserDefaults] boolForKey:@"islogin"]

@interface MyViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UINavigationBar * _naviBar;

}
@property (nonatomic, strong)NSArray *cellTitleArray;
@property (nonatomic, strong)NSArray *CellImageArray;
@property (nonatomic , strong)UITableView *TableView;
@property (nonatomic ,strong)UIView *headerView;//
@property (nonatomic , strong)UIImage *headerIMG;
@property (nonatomic ,strong)UILabel *nikeNameLabel;
@property (nonatomic ,strong)UILabel *NameLabel;
@property (nonatomic , copy)NSString *HeardimgPath;
@property (nonatomic ,strong)UIImageView *heardImagView;
@property (nonatomic ,strong)NSString *godelBit;
@property (nonatomic ,strong)NSString *YInBit;
@end

@implementation MyViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"个人中心";
    
    
    [self loadUI];
    self.navigationController.navigationBar.tintColor = Green;
}


-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64 - 49) style:UITableViewStyleGrouped];
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    self.TableView.delegate = self;
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    self.cellTitleArray = @[@"优惠券",@"设置",@"我的推荐",@"推荐码",@"收货地址", @"商家入驻", @"商家登录", @"平台热线"];
    self.CellImageArray = @[@"My_coupon",@"My_setting",@"My_recommend",@"My_recommend_code",@"My_address", @"My_ruzhu", @"My_ruzhu", @"My_hot_p"];

    self.headerView = [UIView new];
    self.headerView.frame = CGRectMake(0, 0, YzWidth, YzWidth/3);
    self.headerView.backgroundColor = RGBColor(237, 237, 237);
//    self.TableView.tableHeaderView = self.headerView;
    

    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, YzWidth, YzWidth/3-20)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.headerView addSubview:backView];
    
    
//    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"logo.png"];
//    self.headerIMG = [UIImage imageNamed:@"logo"];
    
    
    
    UIImageView *heardimage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 12.5, YzWidth/3-45, YzWidth/3-45)];
    [heardimage.layer setMasksToBounds:YES];
    [heardimage.layer setCornerRadius:YzWidth/6-23];
    
//    if (fullPath) {
//        heardimage.image = self.headerIMG;
//
//    }else{
    NSLog(@" - - %@", [NSURL URLWithString:LoveUserheardImg]);
    [heardimage sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
//yu
//    }
    
    
    [backView addSubview:heardimage];
    _heardImagView = heardimage;
    
    
    UILabel *nikeNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(YzWidth/3 - 10, YzWidth/6 -10 -25, 150, 15)];
    [backView addSubview:nikeNameLabel];
    _nikeNameLabel = nikeNameLabel;
    
    
    UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(YzWidth/3 - 10, YzWidth/6 -5 , 200, 15)];
    NameLabel.textColor = [UIColor redColor];
    [backView addSubview:NameLabel];
    _NameLabel = NameLabel;
    
    
    
    
    
    if (DriverISLogIN) {
        _nikeNameLabel.text = LDnickname;
        _NameLabel.text = LoveUserName;
    }else{
        
        _NameLabel.text = @"请登录";
        heardimage.image = [UIImage imageNamed:@"kefu"];
    }
    
    
    UIButton *PushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PushBtn.frame = CGRectMake(60, 0, YzWidth - 60, YzWidth/3 - 30);
    [PushBtn addTarget:self action:@selector(PushPresonData) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:PushBtn];

    UIImageView *rightDisplyImg = [[UIImageView alloc]init];
    
    rightDisplyImg.frame = CGRectMake(YzWidth-40, YzWidth/6-20, 25, 20);
    
    rightDisplyImg.image = [UIImage imageNamed:@"rightDisplay"];
    
    [backView addSubview:rightDisplyImg];
}


-(void)JumpToAction:(UIButton *)sender{
    
    if (sender.tag == 0) {
        //我的好友
        if (DriverISLogIN) {
            _contactsVC = [[ContactListViewController alloc] initWithNibName:nil bundle:nil];
            [self.navigationController pushViewController:_contactsVC animated:YES];
            
        }else{
            
            LoginViewController *vc = [[LoginViewController alloc]init];
            YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
            [self presentViewController:NaVC animated:YES completion:nil];
            
        }

    }else if (sender.tag == 1){
        if (DriverISLogIN) {
            
            YzCollectionViewController  * serviceVC =[[YzCollectionViewController alloc]init];
            serviceVC.title = @"收藏";
            [self.navigationController pushViewController:serviceVC animated:YES];
            
        }else{
            
            LoginViewController *vc = [[LoginViewController alloc]init];
            YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
            [self presentViewController:NaVC animated:YES completion:nil];
            //                [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else if (sender.tag == 2){
        if (DriverISLogIN) {
            
            YzwalletViewController  * serviceVC =[[YzwalletViewController alloc]init];
            serviceVC.godelBit =     _godelBit;
            serviceVC.YinBit = _YInBit;
            [self.navigationController pushViewController:serviceVC animated:YES];
            
        }else{
            LoginViewController *vc = [[LoginViewController alloc]init];
            YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
            [self presentViewController:NaVC animated:YES completion:nil];
        }

    }else if (sender.tag == 3){
        //我的订单
        if (DriverISLogIN) {
            YzMyOrderVC *vc = [[YzMyOrderVC alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            LoginViewController *vc = [[LoginViewController alloc]init];
            YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
            [self presentViewController:NaVC animated:YES completion:nil];
        }
    }
    
}

-(void)PushPresonData{
    
    if (DriverISLogIN) {
        
        YZpersonaldataVC *vc = [[YZpersonaldataVC alloc]init];
        vc.HeardImgPath = _HeardimgPath;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        
        LoginViewController *vc = [[LoginViewController alloc]init];
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        [self presentViewController:NaVC animated:YES completion:nil];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return YzWidth/4 + 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return YzWidth/3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footView  = [[UIView alloc] init];
    footView.frame    = CGRectMake(0, 0, YzWidth, YzWidth/4);
    
    NSArray *titleArray = @[@"我的好友",@"我的收藏",@"我的钱包",@"我的订单"];
    NSArray *imageArray = @[@"My_friend",@"My_love",@"My_money",@"My_order"];
    
    //底部按钮
    for (int i =0 ; i <4 ; i ++) {
        
        UIView *baseView  = [[UIView alloc] init];
        baseView.frame    = CGRectMake(YzWidth/4 *i, 10 , YzWidth/4, YzWidth/4);
        [footView addSubview:baseView];
        baseView.backgroundColor = [UIColor whiteColor];
        
        UIButton *logobtn = [UIButton buttonWithType:UIButtonTypeCustom];
        logobtn.tag = i;
        logobtn.frame    = CGRectMake(30, 15, YzWidth/4 - 60,YzWidth/4 - 60);
        [logobtn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [logobtn addTarget:self action:@selector(JumpToAction:) forControlEvents:UIControlEventTouchUpInside];
        //        logobtn.backgroundColor = [UIColor redColor];
        [baseView addSubview:logobtn];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.frame    = CGRectMake( 0, YzWidth / 6 - 5, YzWidth/4, 20);
        titleLabel.tag = i ;
        titleLabel.text     = titleArray[i];
        titleLabel.font     = [UIFont systemFontOfSize:13];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [baseView addSubview:titleLabel];
    }
    
    return footView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cellTitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];

    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.imageView.image = [UIImage imageNamed:self.CellImageArray[indexPath.row]];
    
    cell.textLabel.text = self.cellTitleArray[indexPath.row];
    cell.textLabel.alpha = 0.7;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
        {
            //优惠券
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            YzMyCouponTableViewController *couponVC = [[YzMyCouponTableViewController  alloc]init];
            [self.navigationController pushViewController:couponVC animated:YES];
        }
            
            break;
        case 1:
        {
            //设置
            YzSetingViewController * serviceVC =[[YzSetingViewController alloc]init];
            [self.navigationController pushViewController:serviceVC animated:YES];
        }
            break;
            
//        case 2:
//        {
//           
//            //客服
//            [SVProgressHUD showInfoWithStatus:@"客服"];
//           
//        }
//            break;
            
        case 2:
        {
            //我的推荐
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            YzMyTuiJianVC *vc = [[YzMyTuiJianVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
            
        {
            //推荐码
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            YzMyTuiJinaCodeVC *vc = [[YzMyTuiJinaCodeVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            
            break;
        
        case  4:
        {
            //收货地址
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            YzMyLocationVC *myLocation = [[YzMyLocationVC alloc]init];
            [self.navigationController pushViewController:myLocation animated:YES];
        }
            break;
        case 5:
        {
            //商家入驻
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            YzMallRuzhuViewController *ruzhuVC = [[YzMallRuzhuViewController alloc]init];
            [self.navigationController pushViewController:ruzhuVC animated:YES];
        }
            break;
        case 6:
        {
            //商家登录
            if (!DriverISLogIN) {
                LoginViewController *vc = [[LoginViewController alloc]init];
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                [self presentViewController:NaVC animated:YES completion:nil];
                return;
            }
            AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
            adVC.Ad_ID = kUrl_mall_login;
            adVC.title = @"商家登录";
            [self.navigationController pushViewController:adVC animated:YES];
        }
            break;
        case 7:
        {
            //平台热线
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",kUrl_hot_phone]]options:@{} completionHandler:^(BOOL success) {
                
            }];
        }
            break;
        default:
            break;
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if (DriverISLogIN) {
        [NetMethod getUserToken:^(BOOL result) {
            if (result) {
                NSLog(@"获取用户token成功");
            }else{
                NSLog(@"获取用户token失败");
            }
        }];
    }
    
    //    self.navigationController.navigationBar.hidden = NO;
    [self GetUsereInfo];
    
    [self.TableView reloadData];
}

-(void)GetUsereInfo{
    
    //    APP/Member/info
    
    if (DriverISLogIN) {
        
        NSDictionary *parameDic = @{@"uid":LoveDriverID, @"token":FBH_USER_TOKEN};
        
        [NetMethod Post:FBHRequestUrl(kUrl_user_info) parameters:parameDic success:^(id responseObject) {
            
            
            NSDictionary *dic = responseObject;
            
            int code = [[dic objectForKey:@"code"] intValue];
            
            if (code == 200){
                NSDictionary *info = dic[@"data"];
                
                NSString *nickname = info[@"nickname"];
                NSString *username = info [@"username"];
                _NameLabel.text = username;
                
                NSString *heardimgpath = info[@"pic"];
                NSString *url = FBHRequestUrl(heardimgpath);
                
                //yu
                [_heardImagView sd_setImageWithURL:[NSURL URLWithString:url]  placeholderImage:[UIImage imageNamed:@"logo"]];
                
                [[NSUserDefaults standardUserDefaults] setObject:info forKey:@"LdUserInfo"];
                [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
                [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"ImgPath"];
                if (nickname) {
                    [[NSUserDefaults standardUserDefaults] setObject:nickname forKey:@"nickname"];
                    _nikeNameLabel.text = nickname;
                }
//                [self saveImage:_heardImagView.image withName:@"logo"];//yu
                [self.TableView reloadData];
            }else{
                [MBProgressHUD showError:@"获取用户信息失败" toView:self.view];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD showError:error.description toView:self.view];
        }];
    }else{
        _nikeNameLabel.text = @"";
        _NameLabel.text = @"请登录";
        _heardImagView.image = [UIImage imageNamed:@""];
    }
}



//- (void)saveImage:(UIImage *)currentImage withName:(NSString *)imageName
//{
//
//    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.0);
//
//    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
//
//    [imageData writeToFile:fullPath atomically:NO];
//
//}




///Users/a1/Desktop/App_v1.0/LoveDriver IOS/ASJ/LoveDriverPay/openssl

@end
