//
//  YzUserInfoModel.h
//  ASJ
//
//  Created by Jack on 16/9/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzUserInfoModel : NSObject

@property (nonatomic , copy) NSString *uid;

@property (nonatomic , copy) NSString *UserID;

@property (nonatomic , copy) NSString *nickname;

@property (nonatomic , copy) NSString *username;

@property (nonatomic, copy) NSString *is_friend;

//
//@property (nonatomic , copy) NSString *sex;
//
//
//@property (nonatomic , copy) NSString *birthday;
//
//@property (nonatomic , copy) NSString *qq;
//
//@property (nonatomic , copy) NSString *silver;
//
//@property (nonatomic , copy) NSString *score;
//
//@property (nonatomic , copy) NSString *login;
//
//@property (nonatomic , copy) NSString *reg_ip;
//
//@property (nonatomic , copy) NSString *reg_time;
//@property (nonatomic , copy) NSString *last_login_ip;
//@property (nonatomic , copy) NSString *last_login_time;
//@property (nonatomic , copy) NSString *status;
//@property (nonatomic , copy) NSString *sign_time;
//
//
//@property (nonatomic , copy) NSString *sign_last;
//@property (nonatomic , copy) NSString *continues;
//@property (nonatomic , copy) NSString *succession;
@property (nonatomic , copy) NSString *headpic;

@end
