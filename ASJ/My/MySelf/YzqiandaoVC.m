//
//  YzqiandaoVC.m
//  ASJ
//
//  Created by Jack on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzqiandaoVC.h"

@interface YzqiandaoVC ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic ,strong)UIButton *qiandaoBTN;

@property (nonatomic ,strong)UILabel *YiQiandaoL;

@property (nonatomic ,strong)UILabel *LianXuQiandL;

@property (nonatomic ,strong)NSMutableArray *QiandaoArray;

@property (nonatomic ,strong)UIView *TableHeaderView;

@property (nonatomic ,strong)UIScrollView *MainScroolView;

@property (nonatomic , strong) UIWebView *tianqiWeb;

@end

@implementation YzqiandaoVC


-(NSMutableArray *)QiandaoArray{
    
    if (!_QiandaoArray) {
        
        _QiandaoArray = [NSMutableArray array];
    }
    
    return _QiandaoArray;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"我的签到";
    
    self.CellImageArray = @[@"chanpin2",@"banner3.jpg",@"chanpin2"];
    
    [self RELOadUI];
    
    [self TecheHistory];
    
    
    
//    [self FristLoadUI];
    
    [self data];
    
}


-(void)FristLoadUI{
    
    
    for (int i = 0; i<31; i++) {
        
        
        UIImageView *pointImg = [UIImageView new];
        
        pointImg.frame = CGRectMake(14 +i *(YzWidth - 35)/6, 26, 12, 12);
        
        pointImg.image = [UIImage imageNamed:@"yiqiandao-tian-A"];
        
        pointImg.layer.cornerRadius = 6;
        
        [_MainScroolView addSubview:pointImg];
        
    
        
        UILabel *dateLabel = [[UILabel alloc]init];
        
        dateLabel.frame = CGRectMake(14 +i *(YzWidth - 35)/6, 36, 14, 12);
        
        dateLabel.text = [NSString stringWithFormat:@"%d ",i+1];
        
        dateLabel.textColor = [UIColor whiteColor];
        
        dateLabel.textAlignment = NSTextAlignmentCenter;
        
        dateLabel.font = [UIFont systemFontOfSize:10];
        
        [_MainScroolView addSubview:dateLabel];
        
    }

}



-(void)RELOadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight -64)];
    
    self.TableView.scrollEnabled = YES;
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    //    self.TableView.bounces = NO;
    
//    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [self.view addSubview:self.TableView];
    
    
    
    UIScrollView *websc = [[UIScrollView alloc]init];
    
    websc.frame = CGRectMake(0, 0, YzWidth, YzHeight - YzWidth/2 );
    
    self.TableView.tableFooterView = websc;

    
    UIWebView *webView = [[UIWebView alloc]init];
    
    webView.frame = CGRectMake(0, -100, YzWidth, YzHeight - YzWidth/2+40 +80);
    
    //    webView.delegate = self;
    
    [webView endEditing:NO];
    
    
    webView.scalesPageToFit = YES;
    
    
    NSString *path = @"http://m.baidu.com/from=2001a/s?word=%E5%A4%A9%E6%B0%94&ts=8296479&t_kt=0&ie=utf-8&rsv_iqid=9264249347020784832&rsv_t=5c71BpVYihM8ErjU9aJIOhHooEkDlkodtSeK1fiESxpRiyrBtVu%252BYTJyPps&sa=ib&rsv_pq=9264249347020784832&rsv_sug4=11478&ss=111&inputT=9388";
    
    
    //    NSString *path = @"http://www.baidu.com";
    
//    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path]]];
//    
//    [webView reload];
    
    
    [websc addSubview:webView];
    
    [GCUtil YzSetWebCache:path webView:webView];
    
    
  
    
    [websc endEditing: NO];
    
    UIView *label = [[UIView alloc]init];
    
    // - YzWidth/2+40
    
    label.frame = CGRectMake(0, 0, YzWidth, YzHeight);
    
    label.backgroundColor = [UIColor clearColor];
    
    [websc addSubview:label];
    

    
    

    
    UIView *heardView = [UIView new];
    
    heardView.frame = CGRectMake(0, 0, YzWidth, YzWidth/2+80);
    
    heardView.backgroundColor = [UIColor blueColor];
    
    self.TableView.tableHeaderView = heardView;
    
    
    _TableHeaderView = heardView;
    
    UIImageView *img = [UIImageView new];
    
    img.frame = heardView.frame;
    
    img.alpha = 0.5;
    
    img.image = [UIImage imageNamed:@"qiandaobeijing"];
    
    img.backgroundColor =[UIColor clearColor];
    
    [_TableHeaderView addSubview:img];
    
    
    
    
    
    UIButton *qiandaoBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [qiandaoBTN setBackgroundImage:[UIImage imageNamed:@"qiandaoBT"] forState:UIControlStateNormal];
    
    qiandaoBTN.frame = CGRectMake(YzWidth/2-YzWidth/8, 30, YzWidth/4, YzWidth/4);
    
    [heardView addSubview:qiandaoBTN];
    
    qiandaoBTN.layer.cornerRadius = YzWidth/8;
    
    [qiandaoBTN addTarget:self action:@selector(LoveDriverLogin) forControlEvents:UIControlEventTouchUpInside];
    
    //    qiandaoBTN.backgroundColor = [UIColor whiteColor];
    
    [_TableHeaderView addSubview:qiandaoBTN];
    
    _qiandaoBTN = qiandaoBTN;
    
    
    [_qiandaoBTN setTitle:@"签到" forState:UIControlStateNormal];
    
    [_qiandaoBTN setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    
    
    
    UIScrollView *MainScroolView = [[UIScrollView alloc]init];
    
    MainScroolView.frame = CGRectMake(14, YzWidth/2 - 30, YzWidth - 28, 60);
    
    //    MainScroolView.contentOffset = CGPointMake((YzWidth - 35)/6 *31, 50);
    
    MainScroolView.contentSize = CGSizeMake((YzWidth - 35)/6 *31, 0);
    
    //    MainScroolView.backgroundColor = [UIColor blueColor];
    
    MainScroolView.showsHorizontalScrollIndicator = NO;
    
    [_TableHeaderView addSubview:MainScroolView];
    
    _MainScroolView= MainScroolView;
    
    
    
    UIImageView *lineView = [UIImageView new];
    
    lineView.frame = CGRectMake(15, 30, (YzWidth - 35)/6 *30, 2);
    
    lineView.image = [UIImage imageNamed:@"qiandao-xian(1).jpg"];
    
    [_MainScroolView addSubview:lineView];
    
}







-(void)CreatUI{
    
    
    for (int i = 0; i<31; i++) {
        
        
        UIImageView *pointImg = [UIImageView new];
        
        pointImg.frame = CGRectMake(14 +i *(YzWidth - 35)/6, 26, 12, 12);
        
        pointImg.image = [UIImage imageNamed:@"yiqiandao-tian-A"];
        
        pointImg.layer.cornerRadius = 6;
        
        [_MainScroolView addSubview:pointImg];
        
        
        
        UIImageView *TeckImg = [UIImageView new];
        
        TeckImg.frame = CGRectMake(13 +i *(YzWidth - 35)/6, 10 , 14, 18);
        
        if ([self.QiandaoArray[i] isEqualToString:@"0"]) {
            
            //            TeckImg.image = [UIImage imageNamed:@"YzqiandaoNo.jpg"];
            
        }else{
            
            TeckImg.image = [UIImage imageNamed:@"qiandao-biaoq"];
        }
        
        
        [_MainScroolView addSubview:TeckImg];
        
        
        UILabel *dateLabel = [[UILabel alloc]init];
        
        dateLabel.frame = CGRectMake(14 +i *(YzWidth - 35)/6, 38, 14, 12);
        
        dateLabel.text = [NSString stringWithFormat:@"%d ",i+1];
        
        dateLabel.textColor = [UIColor whiteColor];
        
        dateLabel.textAlignment = NSTextAlignmentCenter;
        
        dateLabel.font = [UIFont systemFontOfSize:10];
        
        [_MainScroolView addSubview:dateLabel];
        
    }
    
    
    [self performSelector:@selector(ScrotoToday) withObject:nil afterDelay:1];
    
}



//动画到当前时间

-(void)ScrotoToday{
    
    
    
    NSTimeInterval timewww = 1;
    
    NSString *today = [self TodayString];
    
    CGFloat todayfloat = [[NSString stringWithFormat:@"%@",today] floatValue];
    
    //    todayfloat = 29;
    
    //    NSLog(@"%f",todayfloat);
    
    if (todayfloat > 28) {
        
        todayfloat = 26;
        
        timewww = 2.5;
        
    }else if (todayfloat <5){
        
        todayfloat = 1;
        
        timewww = 0.5;
        
    }else if(todayfloat >=5 && todayfloat <=28) {
        
        todayfloat = todayfloat - 2.65;
        timewww = 1.5;
        
    }else{
        
    }
    
    
    [UIView animateWithDuration:timewww animations:^{
        
        _MainScroolView.contentOffset = CGPointMake((todayfloat - 1)*(YzWidth - 35)/6  , 0) ;
        
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(NSString *)TodayString{
   
    NSDate * senddate=[NSDate date];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    
    [dateformatter setDateFormat:@"dd"];
    
    NSString * locationString=[dateformatter stringFromDate:senddate];
    
    return locationString;
    //    NSLog(@" ******* %@",locationString);
}



-(void)data{
    
    NSDate * senddate=[NSDate date];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString * locationString=[dateformatter stringFromDate:senddate];
    
    
    NSLog(@" ******* %@",locationString);
}

-(NSString *)dateStringFromNumberTimer:(NSString *)timerStr{
    
    //转化为Double
    //    double t = [timerStr doubleValue]/1000;
    //计算出距离1970的NSDate
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timerStr intValue]];
    //转化为 时间格式化字符串
    NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
    df.dateFormat = @"yyyy-MM-dd HH:MM";
    
    //    HH:mm:ss
    //转化为 时间字符串
    return [df stringFromDate:date];
}

-(void)TecheHistory{
    
    
    NSDictionary *userInfo = @{@"user_id":LoveDriverID};
    
    [NetMethod Post:LoveDriverURL(@"APP/Sign/index") parameters:userInfo success:^(id responseObject) {
        
        NSDictionary *LogInDic = responseObject;
        
        NSDictionary *dataArray = LogInDic[@"dataarr"];
        
        for (int i = 1; i<32; i++) {
            
            NSString *sss ;
            
            if (i>9) {
                
                sss = [NSString stringWithFormat:@"%d",i];
                
            }else{
                
                sss = [NSString stringWithFormat:@"0%d",i];
                
            }
            
            NSString *date = [dataArray objectForKey:sss];
            
            [self.QiandaoArray addObject:date];
        }
        
        [self CreatUI];
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
}



-(void)LoveDriverLogin{
    
    
    
    NSDictionary *userInfo = @{@"user_id":LoveDriverID};
    
    [NetMethod Post:LoveDriverURL(@"APP/Sign/sign") parameters:userInfo success:^(id responseObject) {
        
        NSDictionary *DIC = responseObject;
        
        NSString *msg = DIC[@"msg"];
        
        if ([msg isEqualToString:@"今天已经签到过了"]) {
            
        }else{
            
            [self TecheHistory];
            
        }
        
        [MBProgressHUD showSuccess:msg toView:self.view];
        
    } failure:^(NSError *error) {
        
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    return self.CellImageArray.count;
    
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];

    UIImageView *img = [UIImageView new];
    
    
    img.frame = CGRectMake(0, 10, YzWidth, 135);
    
    img.image = [UIImage imageNamed:self.CellImageArray[indexPath.row]];
    
    [cell addSubview:img];
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 145;
    
}
-(void)Movie{
    
    
}
@end
