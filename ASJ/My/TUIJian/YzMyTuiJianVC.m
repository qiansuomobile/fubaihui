//
//  YzMyTuiJianVC.m
//  ASJ
//
//  Created by a1 on 2019/5/29.
//  Copyright © 2019年 TS. All rights reserved.
//

//#import "YzMyTuijianCell.h"
//我的推荐

//yu
@interface JFView:UIView
@end
@implementation JFView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title
{
    
    frame.size.height = 40;
    frame.size.width = YzWidth/2.0;

    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yyyJiFenImg"]];
        imageV.width = 25;
        imageV.height = 20;
        [self addSubview:imageV];
        imageV.centerY = 40/2.0;
        
        UILabel *date2 = [[UILabel alloc]init];
        date2.font = [UIFont systemFontOfSize:13];
        date2.textColor = [UIColor grayColor];
        date2.textAlignment = NSTextAlignmentLeft;
        [self addSubview:date2];
        date2.text = title;
        [date2 sizeToFit];
        date2.centerY = imageV.centerY;
        date2.left = imageV.right;

    }
    return self;
}

@end

@interface YzMyTuijianCell : UITableViewCell

@property (nonatomic , strong) UILabel *titleLabel1;//昵称
@property (nonatomic , strong) UILabel *dateLabel;//日期
@property (nonatomic, strong)UIImageView *headPic;//头像
//@property (nonatomic , strong) UILabel *godenlLabel;//金积分
//@property (nonatomic , strong) UILabel *yinLabel;//银积分
//@property (nonatomic , strong) UILabel *lastMoneyLabel;//余额
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end

@implementation YzMyTuijianCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        //        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"YzMyTuijianCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}


-(void)setUpSubview{
    
    UILabel *imageV = [[UILabel alloc]init];
//    [imageV.layer setMasksToBounds:YES];
//    [imageV.layer setCornerRadius:4.0];
    [self addSubview:imageV];
    self.titleLabel1 = imageV;
    
    
    
    UILabel *date2 = [[UILabel alloc]init];
    date2.font = [UIFont systemFontOfSize:13];
    date2.textColor = [UIColor grayColor];
    date2.textAlignment = NSTextAlignmentLeft;
    [self addSubview:date2];
    _dateLabel = date2;
    
    UIImageView *headImageView = [[UIImageView alloc]init];
    headImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:headImageView];
    
    _headPic = headImageView;
    
    
//    UILabel *one = [[UILabel alloc]init];
//    one.font = [UIFont systemFontOfSize:13];
//    one.textAlignment = NSTextAlignmentLeft;
//    [self addSubview:one];
//    _godenlLabel = one;
    
    
//    UILabel *two = [[UILabel alloc]init];
//    two.font = [UIFont systemFontOfSize:13];
//    two.textAlignment = NSTextAlignmentCenter;
//    [self addSubview:two];
//
//    _yinLabel = two;
    
//    UILabel *therr = [[UILabel alloc]init];
//    therr.font = [UIFont systemFontOfSize:13];
//    therr.numberOfLines = 2;
//    therr.textAlignment = NSTextAlignmentCenter;
////    [therr.layer setMasksToBounds:YES];
////    [therr.layer setCornerRadius:10.0];
//    [self addSubview:therr];
//    _lastMoneyLabel = therr;
    
    self.headPic.frame      = CGRectMake(10, 15, 60, 60);
    self.titleLabel1.frame = CGRectMake(80, 10, YzWidth - 20, 40);
    self.dateLabel.frame   = CGRectMake(80, 50, YzWidth - 100, 30);
//
//    self.godenlLabel.frame = CGRectMake(10, 80, YzWidth/3, 30);
//    self.yinLabel.frame    = CGRectMake(YzWidth/3, 80, YzWidth/3, 30);
//    self.lastMoneyLabel.frame = CGRectMake(YzWidth/3*2, 80, YzWidth/3.0, 30);
    
    
//    self.lastMoneyLabel.frame = CGRectMake(0, 40, YzWidth, 30);
}

@end

#import "YzMyTuiJianVC.h"

@interface YzMyTuiJianVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong ) UITableView *mainTableView;
@property (nonatomic , strong ) NSMutableArray *datasouceArray;

@end

@implementation YzMyTuiJianVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.title = @"我的推荐";
    self.mainTableView = [[UITableView alloc] init];
    self.mainTableView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 64);
    self.mainTableView.backgroundColor = [UIColor whiteColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    
    
    
 
    [self requestWithUrl:FBHRequestUrl(kUrl_member_list) dic:@{@"uid":LoveDriverID,
                                                               @"page":@"1",
                                                               @"row_num":@"50",
                                                               @"token":FBH_USER_TOKEN}];
}
//yu
-(void)setTopViewWithDic:(NSDictionary*)dic{
    
    
    //
    //头部
    //
    UIView *headerView  = [[UIView alloc] init];
    headerView.frame    = CGRectMake(0, 0, YzWidth, 250);
    self.mainTableView.tableHeaderView = headerView;
    headerView.backgroundColor = RGBColor(245, 246, 247);//浅灰色
    
    
    UIImageView *headerImage = [[UIImageView alloc] init];
    headerImage.frame        = CGRectMake(YzWidth/3+30, 30, YzWidth/3-60, YzWidth/3 - 60);
    [headerView addSubview:headerImage];
    headerImage.backgroundColor = [UIColor orangeColor];
    
//    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"logo.png"];
    [headerImage.layer setMasksToBounds:YES];
    [headerImage.layer setCornerRadius:headerImage.width/2.0];
//    if (fullPath) {
//        [headerImage sd_setImageWithURL:[NSURL URLWithString:fullPath]];
//
//    }else{
//        headerImage.image = [UIImage imageNamed:@"logo"];
//
//    }
    [headerImage sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];

    //
    //名字
    //
    UILabel *titlelabel      = [[UILabel alloc] init];
    //    titlelabel.frame         = CGRectMake(YzWidth/2 - 80, YzWidth/3-20, 160, 20);
    titlelabel.text          = LDnickname;
    //    titlelabel.textColor     = [UIColor blueColor];
    [headerView addSubview:titlelabel];
    [titlelabel sizeToFit];
    titlelabel.top = headerImage.bottom + 5;
    titlelabel.numberOfLines = 2;
    titlelabel.centerX = YzWidth/2.0;
    
    
    //    NSArray * titleArray = @[@"金积分",@"银积分",@"余额"];
    //    for (int i =0 ; i<3 ; i++) {
    //        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //        button.frame     = CGRectMake(YzWidth/3*i, YzWidth/3, YzWidth/3, YzWidth/6);
    ////        button.backgroundColor = [UIColor greenColor];
    //        button.titleLabel.font = [UIFont systemFontOfSize:13];
    //        [button setTitle:titleArray[i] forState:UIControlStateNormal];
    //        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    //        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //        [headerView addSubview:button];
    //    }
    
    //
    //个人积分
    //
    
    //金
    JFView *jinJF = [[JFView alloc] initWithFrame:CGRectZero title:[NSString stringWithFormat:@"金积分：%@",dic[@"_score"]]];
    [headerView addSubview:jinJF];
    jinJF.top = titlelabel.bottom + 10;
    jinJF.left = 30;
    
    //商家金
    JFView *sjjinJF = [[JFView alloc] initWithFrame:CGRectZero title:[NSString stringWithFormat:@"商家金积分：%@",dic[@"_store_score"]]];
    [headerView addSubview:sjjinJF];
    sjjinJF.top = jinJF.bottom ;
    sjjinJF.left = jinJF.left;
    
    
        //余额
        JFView *yue = [[JFView alloc] initWithFrame:CGRectZero title:[NSString stringWithFormat:@"余额：%@",dic[@"_balance"]]];
        [headerView addSubview:yue];
        yue.top = titlelabel.bottom + 10;
        yue.left = jinJF.right + 15;
        
    
    
        //银积分
        JFView *yinJF = [[JFView alloc] initWithFrame:CGRectZero title:[NSString stringWithFormat:@"银积分：%@",dic[@"_silver"]]];
        [headerView addSubview:yinJF];
        yinJF.top = yue.bottom ;
        yinJF.left = jinJF.right + 15;
        
    
    
}
//yu
-(void)requestWithUrl:(NSString *)requestUrl dic:(NSDictionary *)requsetDic{
    
    [SVProgressHUD showWithStatus:@"正在加载..."];
    
    
    [NetMethod Post:requestUrl
         parameters:requsetDic
            success:^(id responseObject) {
                
                
                NSDictionary *dic = responseObject;
                int code = [[dic objectForKey:@"code"] intValue];
                if (code == 200){
                    
                    if (![dic[@"data"] isKindOfClass:[NSNull class]]) {
                        NSArray *data =  dic[@"data"];
                        _datasouceArray = [NSMutableArray arrayWithArray:data];
                        [_mainTableView reloadData];
                    }else{
                        [MBProgressHUD showError:@"暂无推荐会员" toView:self.view];
                    }
                    
                    
                }else{
                    NSString *msg = [dic objectForKey:@"msg"];
                    [MBProgressHUD showError:msg toView:self.view];
                }
                
                
                [SVProgressHUD dismiss];
                
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
                NSLog(@"error = %@",error.description);
                
            }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasouceArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = _datasouceArray[indexPath.row];
    //yu
    YzMyTuijianCell *cell = [YzMyTuijianCell cellWithTableView:tableView];
    
    [cell.headPic sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(dic[@"headpic"])]
                     placeholderImage:[UIImage imageNamed:@"logo"]];
    cell.titleLabel1.text = dic[@"nickname"];//成功推荐用户：
    cell.dateLabel.text   = [NSString stringWithFormat:@"注册时间：%@",dic[@"reg_time"]];
//    cell.godenlLabel.text = [NSString stringWithFormat:@"获得金积分：%@",dic[@"score"]];
//    cell.yinLabel.text    = [NSString stringWithFormat:@"获得银积分：%@",dic[@"silver"]];
//    cell.lastMoneyLabel.text = [NSString stringWithFormat:@"获得余额：%@",dic[@"balance"]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

- (NSString *)getTimeFromTimestamp:(NSString *)time{
    
    //将对象类型的时间转换为NSDate类型
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    
    //设置时间格式
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //将时间转换为字符串
    
    NSString *timeStr=[formatter stringFromDate:myDate];
    
    return timeStr;
}
@end
