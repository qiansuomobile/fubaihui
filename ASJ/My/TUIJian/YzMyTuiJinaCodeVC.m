//
//  YzMyTuiJinaCodeVC.m
//  ASJ
//
//  Created by a1 on 2019/5/29.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "YzMyTuiJinaCodeVC.h"
#import "DDShardView.h"

@interface YzMyTuiJinaCodeVC ()
@property (nonatomic, strong)UIImage *img;
@end

@implementation YzMyTuiJinaCodeVC

- (void)viewDidLoad {

    [super viewDidLoad];
    
    //yu
    self.title =@"推荐码";
    self.view.backgroundColor = [UIColor whiteColor];
    
//    UIImageView *LogoImageView = [[UIImageView alloc] init];
//    LogoImageView.frame        = CGRectMake(YzWidth/2-50, 20, 100, 80);
//    LogoImageView.image        = [UIImage imageNamed:@""];
//    LogoImageView.backgroundColor = [UIColor greenColor];
//    [self.view addSubview:LogoImageView];
    
    UIImageView *headerImage = [[UIImageView alloc] init];
    headerImage.frame        = CGRectMake(0, 30, YzWidth/8, YzWidth/8);
    [self.view addSubview:headerImage];
    headerImage.backgroundColor = [UIColor orangeColor];
    headerImage.centerX = YzWidth/2.0;
//    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"logo.png"];
    [headerImage.layer setMasksToBounds:YES];
    [headerImage.layer setCornerRadius:headerImage.width/2.0];
//    if (fullPath) {
//        headerImage.image = [UIImage imageNamed:@"logo"];
//    }else{
//        [headerImage sd_setImageWithURL:[NSURL URLWithString:fullPath]];
//
//    }
    [headerImage sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];


    //昵称
    UILabel *titleLabel        = [[UILabel alloc] init];
    titleLabel.frame           = CGRectMake(YzWidth/2-80, 0, 160, 20);
    titleLabel.text            = LDnickname;
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.textAlignment   = NSTextAlignmentCenter;
    [self.view addSubview:titleLabel];
    titleLabel.top = headerImage.bottom + 5;
    
    //@"推荐人"
    UILabel *tuiianPeople      = [[UILabel alloc] init];
    tuiianPeople.frame         = CGRectMake(YzWidth/2-80, 0, 160, 20);
    tuiianPeople.text          = @"推荐人";
    tuiianPeople.textColor = [UIColor grayColor];
    tuiianPeople.font = [UIFont systemFontOfSize:12];
    tuiianPeople.textAlignment   = NSTextAlignmentCenter;
    [self.view addSubview:tuiianPeople];
    tuiianPeople.top = titleLabel.bottom + 5;
    
    
    NSString *catesURLStr;
    catesURLStr = @"/APP/Xtojoin/recommended";
    
    [SVProgressHUD showWithStatus:@"正在加载..."];

    [NetMethod Post:FBHRequestUrl(catesURLStr)
         parameters:@{@"uid":LoveDriverID
                      }
           success:^(id responseObject) {
//           uid:用户id,nickname:用户名称,pid：推荐人id，recommend_code:邀请码，name:推荐人名称

               if ([responseObject[@"code"] isEqual:@200]) {
                   titleLabel.text = responseObject[@"data"][@"nickname"];
                   tuiianPeople.text = [NSString stringWithFormat:@"推荐人:%@", responseObject[@"data"][@"name"] ];
                   
                   NSString *code = [NSString stringWithFormat:@"%@ \n 我的邀请码",responseObject[@"data"][@"recommend_code"] ];
                   
                   NSArray *titleArray = @[code,@"商家邀请码",@"会员推荐码",@"商家支付码"];
                   
                   for ( int i = 0; i <4 ; i ++) {
                       
                       UIButton *actionbtn = [UIButton buttonWithType:UIButtonTypeCustom];
                       actionbtn.backgroundColor = RGBColor(0, 212, 196);
                       actionbtn.titleLabel.font = [UIFont systemFontOfSize:15];
                       actionbtn.titleLabel.numberOfLines = 0;
                       actionbtn.titleLabel.textAlignment = NSTextAlignmentCenter;
                       [actionbtn setTitle:titleArray[i] forState:UIControlStateNormal];
                       actionbtn.frame     = CGRectMake(20+(YzWidth/2-20+1)*(i%2), 150+ YzWidth/4*(i/2) +1, YzWidth/2-20, YzWidth/4-1);
                       [self.view addSubview:actionbtn];
                       actionbtn.tag = i;
                       [actionbtn addTarget:self action:@selector(jumpToAction:) forControlEvents:UIControlEventTouchUpInside];
                       
                   }
                   
               }else{
                   NSString *msg = [responseObject objectForKey:@"msg"];
                   [MBProgressHUD showError:msg toView:self.view];
               }
               
               
               [SVProgressHUD dismiss];
           }
     
           failure:^(NSError *error) {
               [SVProgressHUD dismiss];

               [MBProgressHUD showSuccess:@"请检查网络连接" toView:self.view];
           }];
}


-(void)jumpToAction:(UIButton *)sender{
  
    NSLog(@"");//@[code,@"商家邀请码",@"会员推荐码",@"商家支付码"]
    
    NSString *str;
    NSString *url;
    if (sender.tag == 0) {
        return;
    }else if(sender.tag == 1){
        url = @"/APP/Xinv/tojoin";
        str= @"商家邀请码";
    }else if (sender.tag == 2){
        url = @"/APP/Xinv/invcode";
        str= @"会员推荐码";
    }else{
        url = @"/APP/Xtojoin/shopcode";
        str = @"商家支付码";
    }
    
    [SVProgressHUD showWithStatus:@"正在加载..."];
    [NetMethod Post:FBHRequestUrl(url) parameters:@{@"id":LoveDriverID} success:^(id responseObject) {
      
        if ([responseObject[@"code"] isEqual:@200]) {
            
        }else{
            NSString *msg = [responseObject objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
        }
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        
        UIImage *yImage =   [[UIImage alloc ] initWithData:data];

        if (yImage) {
            
            UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, self.view.height - SafeAreaHeight)];
            clearView.backgroundColor = [UIColor clearColor];
            [self.view addSubview:clearView];
            [clearView setTapActionWithBlock:^{
                [clearView removeFromSuperview];
            }];
            
            self.img = yImage;
            
            UIImageView *imgV = [[UIImageView alloc] initWithImage:yImage];
            [clearView addSubview:imgV];
            imgV.width = 200;
            imgV.height = 200;
            imgV.center = clearView.center;
           
            UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [shareBtn setFrame:CGRectMake(40, clearView.frame.size.height - 64, YzWidth - 80, 44)];
            [shareBtn setBackgroundColor:Green];
            shareBtn.layer.cornerRadius = 5.0f;
            [shareBtn setTitle:[NSString stringWithFormat:@"分享%@", str] forState:UIControlStateNormal];
            [shareBtn addTarget:self action:@selector(showShareView) forControlEvents:UIControlEventTouchUpInside];
            [clearView addSubview:shareBtn];
           
        }else{
            [MBProgressHUD showSuccess:@"请检查网络连接" toView:self.view];

        }
        
    }];
}

- (void)showShareView{
    DDShardView *shardView = [[DDShardView alloc]init];
    shardView.type = DDShareTypeIMG;
    [shardView shareEventScoreImage:self.img];
}

@end
