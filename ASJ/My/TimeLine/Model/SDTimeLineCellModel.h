//
//  SDTimeLineCellModel.h
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 362419100(2群) 459274049（1群已满）
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */

#import <Foundation/Foundation.h>

@class SDTimeLineCellLikeItemModel, SDTimeLineCellCommentItemModel,SDTimeLineCommentModel;

@interface SDTimeLineCellModel : NSObject

@property (nonatomic, copy) NSString *iconName;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *msgContent;

@property (nonatomic, strong) NSArray *picNamesArray;

//说说内容
@property (nonatomic, copy) NSString *content;
//图片数组
@property (nonatomic, strong) NSArray *picture;
//用户ID
@property (nonatomic, copy) NSString *uid;
//状态
@property (nonatomic, copy) NSString *status;
//时间戳
@property (nonatomic, copy) NSString *create_time;
//类型
@property (nonatomic, copy) NSString *type_id;

@property (nonatomic, copy) NSString *is_sticky;
//点赞人数
@property (nonatomic, copy) NSString *like_num;

@property (nonatomic, copy) NSString *view_num;
//评论列表
@property (nonatomic, strong) NSArray *commentList;

@property (nonatomic, copy) NSString *competence;

@property (nonatomic ,copy) NSString *headpic;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *UserID;

//  装模型数组
@property (nonatomic ,strong) NSMutableArray *ContentsArray;

//
//@property (nonatomic, copy) NSString *msgContent;



@property (nonatomic, assign, getter = isLiked) BOOL liked;

@property (nonatomic, strong) NSArray<SDTimeLineCellLikeItemModel *> *likeItemsArray;

@property (nonatomic, strong) NSArray<SDTimeLineCellCommentItemModel *> *commentItemsArray;

@property (nonatomic, strong) NSMutableArray<SDTimeLineCommentModel *> *commnetsCellArray;

@property (nonatomic, assign) BOOL isOpening;

@property (nonatomic, assign, readonly) BOOL shouldShowMoreButton;

@end


@interface SDTimeLineCellLikeItemModel : NSObject

@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *userId;

@end


@interface SDTimeLineCellCommentItemModel : NSObject

@property (nonatomic, copy) NSString *commentString;

@property (nonatomic, copy) NSString *firstUserName;

@property (nonatomic, copy) NSString *firstUserId;

@property (nonatomic, copy) NSString *secondUserName;

@property (nonatomic, copy) NSString *secondUserId;

@end





//评论模型
@interface SDTimeLineCommentModel : NSObject

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *create_time;

@property (nonatomic, copy) NSString *uid;

@property (nonatomic, copy) NSString *Addid;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *Sencednickname;

@property (nonatomic, copy) NSString *cid;

@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *headpic;

@end


//SDTimeLineCommentModel



