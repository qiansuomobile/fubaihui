//
//  SDTimeLineTableViewController.m
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 362419100(2群) 459274049（1群已满）
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */
#import "MessagePhotoView.h"//图片视图 01.22

#import "ZBMessageShareMenuView.h"

#import "SDTimeLineTableViewController.h"

#import "SDRefresh.h"

#import "SDTimeLineCellOperationMenu.h"

#import "SDTimeLineTableHeaderView.h"

#import "SDTimeLineRefreshHeader.h"

#import "SDTimeLineRefreshFooter.h"

#import "SDTimeLineCell.h"

#import "SDTimeLineCellModel.h"

#import "UITableView+SDAutoTableViewCellHeight.h"

#import "UIView+SDAutoLayout.h"

#define kTimeLineTableViewCellId @"SDTimeLineCell"

#import "YzLookPersonVC.h"

#import "YzSendCicle.h"

#import "YzChangeLookPersonVC.h"
//#import "SDTimeLineCommentModel.h"


const CGFloat kNaviBarHeight = 44;

const CGFloat kStatusHeight = 20;


static CGFloat textFieldH = 40;

@interface SDTimeLineTableViewController () <SDTimeLineCellDelegate, UITextFieldDelegate,MessagePhotoViewDelegate,ZBMessageShareMenuViewDelegate>


//@property (nonatomic,strong) MessagePhotoView *photoView;


@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, assign) BOOL isReplayingComment;

@property (nonatomic, weak) NSIndexPath *currentEditingIndexthPath;

@property (nonatomic, copy) NSString *commentToUser;

@property (nonatomic, weak) UIView *backView;

@property (nonatomic, weak) UIColor *backColor;

@property (nonatomic, strong) NSMutableArray *CicleArray;

@property (nonatomic, strong)  SDTimeLineTableHeaderView *headerView ;

@property (nonatomic , copy) NSIndexPath *CurrenindexPath;

@end


static NSInteger Pages;


@implementation SDTimeLineTableViewController

{
    SDTimeLineRefreshFooter *_refreshFooter;
    SDTimeLineRefreshHeader *_refreshHeader;
    CGFloat _lastScrollViewOffsetY;
    CGFloat _totalKeybordHeight;
}

-(NSMutableArray *)CicleArray{
    
    if (!_CicleArray) {
        
        _CicleArray = [NSMutableArray array];
        
    }
    
    return _CicleArray;
}

-(void)YZsendCicle{
    
    
    if (DriverISLogIN) {
        
//        [self CricleRequst];
        
        YzSendCicle *vc =[[YzSendCicle alloc]init];
        
        YzNavigationVC *navc = [[YzNavigationVC alloc]initWithRootViewController:vc];
        
        [self presentViewController:navc animated:YES completion:nil];

    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.title = @"分享";
    
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 43*Kwidth, 43*Kwidth)];
    
    [leftBtn setTitle:@"发布" forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(YZsendCicle) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.rightBarButtonItem = leftItem;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.edgesForExtendedLayout = UIRectEdgeTop;
    
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    _headerView = [[SDTimeLineTableHeaderView alloc]init];
    
    _headerView.frame = CGRectMake(0, 0, YzWidth, 240);
   
    self.tableView.tableHeaderView = _headerView;
    [ self.headerView.headBtn addTarget:self action:@selector(Jumppp) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableView registerClass:[SDTimeLineCell class] forCellReuseIdentifier:kTimeLineTableViewCellId];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];

}

-(void)Jumppp{
    
    YzChangeLookPersonVC *vc =[[YzChangeLookPersonVC alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)JUmpChangePersonData{
    
    YzChangeLookPersonVC *vc =[[YzChangeLookPersonVC alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)CricleRequst{
    
    Pages= 1;
    
    [self setRefreshing];
    
    [self RequestConstaction];
    
}
//刷新司机圈

- (void)setRefreshing{
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequestConstaction];
        
        [self.tableView.mj_header endRefreshing];
        
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self loadMoreData];
        [self.tableView.mj_footer endRefreshing];
        
    }];
    
}

-(void)RequestConstaction{
    
    NSDictionary *cicle = @{@"uid":LoveDriverID,@"p":@"1"};
    
    [NetMethod Post:FBHRequestUrl(kUrl_circle_index) parameters:cicle success:^(id responseObject) {
        
        NSDictionary *InfoDic = responseObject;
        
        if ([InfoDic[@"code"] isEqual:@200]) {
            
            NSArray *listArray = InfoDic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            [self.CicleArray removeAllObjects];
            for (NSDictionary *CircleDic in listArray) {
                
                SDTimeLineCellModel *model =[SDTimeLineCellModel objectWithKeyValues:CircleDic];
                
                model.UserID = CircleDic[@"id"];
                
                if ([model.commentList isKindOfClass:[NSNull class]]) {
                    
                    model.commentList =nil;
                    
                }else{
                    //                    遍历评论数组
                    
                    NSMutableArray *pinglunArray = [NSMutableArray array];
                    for (NSDictionary *contentsDIC in model.commentList) {
                        
                        SDTimeLineCommentModel *Model = [SDTimeLineCommentModel objectWithKeyValues:contentsDIC];
                        [pinglunArray addObject:Model];
                        //
                    }
                    //                    评论数组复制
                    model.commnetsCellArray =pinglunArray;
                    
                }
                
                if ([model.picture isKindOfClass:[NSNull class]]) {
                    
                    model.picture =nil;
                }
                
                [self.CicleArray addObject:model];
                
            }
            
            [self.tableView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)loadMoreData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages];
    
    NSDictionary *cicle = @{@"uid":LoveDriverID,@"p":page};
    
    __weak __block SDTimeLineTableViewController * copy_self = self;
    
    [NetMethod Post:FBHRequestUrl(kUrl_circle_index) parameters:cicle success:^(id responseObject) {
        
        NSDictionary *InfoDic = responseObject;
        
        if ([InfoDic[@"code"] isEqual:@200]) {
            
            NSArray *listArray = InfoDic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            for (NSDictionary *CircleDic in listArray) {
                
                SDTimeLineCellModel *model =[SDTimeLineCellModel mj_objectWithKeyValues:CircleDic];
                
                model.UserID = CircleDic[@"id"];
                
                if ([model.commentList isKindOfClass:[NSNull class]]) {
                    
                    model.commentList =nil;
                    
                }else{
                    //                    遍历评论数组
                    
                    NSMutableArray *pinglunArray = [NSMutableArray array];
                    for (NSDictionary *contentsDIC in model.commentList) {
                        
                        SDTimeLineCommentModel *Model = [SDTimeLineCommentModel mj_objectWithKeyValues:contentsDIC];
                        [pinglunArray addObject:Model];
                        //
                    }
                    //                    评论数组复制
                    model.commnetsCellArray =pinglunArray;
                    
                }
                
                if ([model.picture isKindOfClass:[NSNull class]]) {
                    
                    model.picture =nil;
                }
                
                [copy_self.CicleArray addObject:model];
                
            }
            
            [copy_self.tableView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}
//获取朋友圈列表

- (void)setupTextField
{
    _textField = [UITextField new];
    
    _textField.returnKeyType = UIReturnKeySend;
    
    _textField.delegate = self;
    
    _textField.layer.borderColor = [[UIColor lightGrayColor]
                                    colorWithAlphaComponent:0.8].CGColor;
    
    _textField.layer.borderWidth = 1;
    
    _textField.backgroundColor = [UIColor whiteColor];
    
    _textField.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self
                                  .view.width, textFieldH);
    
    [[UIApplication sharedApplication].keyWindow addSubview:_textField];
    
    [_textField becomeFirstResponder];
    
    [_textField resignFirstResponder];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.CicleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SDTimeLineCell *cell = [tableView dequeueReusableCellWithIdentifier:kTimeLineTableViewCellId];
    if (!cell) {
        cell = [[SDTimeLineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTimeLineTableViewCellId];
    }
    
    cell.indexPath = indexPath;
    
    __weak typeof(self) weakSelf = self;
    
    if (!cell.moreButtonClickedBlock) {
        
        [cell setMoreButtonClickedBlock:^(NSIndexPath *indexPath) {
            
            SDTimeLineCellModel *model = weakSelf.CicleArray[indexPath.row];
            
            model.isOpening = !model.isOpening;
            
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }];
        //
        cell.delegate = self;
    }
    
    //  点赞刷新

//    if(!cell.DianzanButtonClickdBlock){
//       
//        [cell setDianzanButtonClickdBlock:^(NSIndexPath *indexPath) {
//            
//            SDTimeLineCellModel *model = weakSelf.CicleArray[indexPath.row];
//            
//            model.isOpening = !model.isOpening;
//            
//            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        }];
//        //
//        cell.delegate = self;
//
//        
//    }
    
    
    ////// 此步设置用于实现cell的frame缓存，可以让tableview滑动更加流畅 //////
    
    [cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
    
    ///////////////////////////////////////////////////////////////////////
    SDTimeLineCellModel *model = self.CicleArray[indexPath.row];
    cell.model =model;
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    SDTimeLineCellModel *model = self.CicleArray[indexPath.row];
//    //    点击头像
//
//    YzLookPersonVC *vc = [[YzLookPersonVC alloc]init];
//
//    vc.PerSonID = model.uid;
//
//    if ([model.uid isEqualToString:LoveDriverID]) {
//
//    }else{
//
//    [self.navigationController pushViewController:vc animated:YES];
//
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // >>>>>>>>>>>>>>>>>>>>> * cell自适应 * >>>>>>>>>>>>>>>>>>>>>>>>
   
    id model = self.CicleArray[indexPath.row];
    
    
    return [self.tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[SDTimeLineCell class] contentViewWidth:[self cellContentViewWith]];
}



- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_textField resignFirstResponder];
}

- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
       
        width = [UIScreen mainScreen].bounds.size.height;
    
    }
    
    return width;
}


#pragma mark - SDTimeLineCellDelegate


//评论

- (void)didClickcCommentButtonInCell:(UITableViewCell *)cell
{
    
    [self setupTextField];

    [_textField becomeFirstResponder];
    
    
     self.currentEditingIndexthPath = [self.tableView indexPathForCell:cell];
    
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    NSInteger row = index.row;

    self.CurrenindexPath =[NSIndexPath indexPathForRow:row inSection:0];
    
//      NSLog(@"  第几个   cell     %@  ",self.CurrenindexPath);
    
   
    [self adjustTableViewToFitKeyboard];
    
}


//点赞   搞定********************

- (void)didClickLikeButtonInCell:(UITableViewCell *)cell
{
    //    点赞
    
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    NSInteger row = index.row;
    
    SDTimeLineCellModel *model = self.CicleArray[index.row];
    
    NSDictionary *usetInfo = @{@"uid":LoveDriverID,@"id":model.UserID};
    
    [NetMethod Post:FBHRequestUrl(kUrl_circle_laud) parameters:usetInfo success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        if ([dic[@"code"] isEqual:@200]) {
            
            [MBProgressHUD showSuccess:@"点赞成功" toView:self.view];
            int likeNum = [model.like_num intValue];
            likeNum = likeNum +1;
            model.like_num = [NSString stringWithFormat:@"%d",likeNum];
            
            [self.CicleArray replaceObjectAtIndex:index.row withObject:model];
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        }else{
            
            [MBProgressHUD showSuccess:dic[@"msg"] toView:self.view];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

//分享  搞定*********************

- (void)didClickcSharButtonInCell:(UITableViewCell *)cell{
    
    NSLog(@"XXXXXXXX   分享");
}



- (void)adjustTableViewToFitKeyboard
{
    //    [_textField becomeFirstResponder];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:_currentEditingIndexthPath];
    
    CGRect rect = [cell.superview convertRect:cell.frame toView:window];
    
    [self adjustTableViewToFitKeyboardWithRect:rect];
}



- (void)adjustTableViewToFitKeyboardWithRect:(CGRect)rect
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    CGFloat delta = CGRectGetMaxY(rect) - (window.bounds.size.height - _totalKeybordHeight);
    
    CGPoint offset = self.tableView.contentOffset;
    
    offset.y += delta;
    
    if (offset.y < 0) {
        
        offset.y = 0;
        
    }
    
    [self.tableView setContentOffset:offset animated:YES];
    
}

//添加评论    *********************************

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     SDTimeLineCellModel *model = self.CicleArray[self.CurrenindexPath.row];
    
    if (_textField.text.length) {
        
        NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"content":_textField.text,@"cid":model.UserID};
        
        [NetMethod Post:FBHRequestUrl(kUrl_circle_comment) parameters:InfoDic success:^(id responseObject) {
            
            if (![responseObject[@"code"] isEqual:@200]) {
                
                [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];

            }
            
        } failure:^(NSError *error) {
            
        }];

                NSMutableArray *temp = [NSMutableArray new];
        //        //        评论数组
                [temp addObjectsFromArray:model.commnetsCellArray];
        
        //        //评论模型
        //
        SDTimeLineCommentModel *commenModel = [[SDTimeLineCommentModel alloc]init];
        
        //        if (self.isReplayingComment) {
        
        commenModel.nickname = LDnickname;
        
        commenModel.content = textField.text;
        
        [temp addObject:commenModel];
        
        model.commnetsCellArray = [temp copy];
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:self.CurrenindexPath,nil] withRowAnimation:UITableViewRowAnimationNone];

        self.textField.text = @"";
        
        [_textField resignFirstResponder];
        
        [self.textField removeFromSuperview];


        return YES;
    }
    
    return NO;
}



- (void)keyboardNotification:(NSNotification *)notification
{
    NSDictionary *dict = notification.userInfo;
    
    CGRect rect = [dict[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    
    CGRect textFieldRect = CGRectMake(0, rect.origin.y - textFieldH, rect.size.width, textFieldH);
    
    if (rect.origin.y == [UIScreen mainScreen].bounds.size.height) {
        
        textFieldRect = rect;
        
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        _textField.frame = textFieldRect;
        
    }];
    
    CGFloat h = rect.size.height + textFieldH;
    
    if (_totalKeybordHeight != h) {
        
        _totalKeybordHeight = h;
        
        [self adjustTableViewToFitKeyboard];
        
    }
}

-(void)TalkBtn:(UIButton *)sender{
    
    
    switch (sender.tag) {
        case 200:
        {
            
            NSLog(@"xxxxxx");
            
        }
            break;
        case 201:
        {
            
            
        }
            break;
            
        case 202:
        {
            
            
        }
            
            break;
            
        case 203:
        {
            
            
        }
            break;
            
        case 204:
        {
            
            
            
        }
            
            break;
        default:
            break;
    }
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    //    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.textField resignFirstResponder];
    
    [self.textField removeFromSuperview];
}



- (void)dealloc
{
    [_refreshHeader removeFromSuperview];
    
    [_refreshFooter removeFromSuperview];
    
    [self.textField removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (DriverISLogIN) {
        [self CricleRequst];
    }else{
        LoginViewController *vc = [[LoginViewController alloc]init];
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        [self presentViewController:NaVC animated:YES completion:nil];
    }
    
    [_headerView.HeaderImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
}

@end
