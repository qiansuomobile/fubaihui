//
//  YzLookPersonVC.m
//  ASJ
//
//  Created by Jack on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzChangeLookPersonVC.h"

@interface YzChangeLookPersonVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
    NSArray *_PersonLike;
    
    NSArray *_shouru;
    
    NSArray *_Lovetype;
    
    NSArray *_MreeType;
    
}


@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic ,strong)  NSArray *PersonLikeDetail;

@property (nonatomic ,strong)  NSArray *shouruDetail;

@property (nonatomic ,strong)  NSArray *LovetypeDetail;

@property (nonatomic ,strong)  NSArray *MreeTypeDetail;

@property (nonatomic , copy) NSString *signature;

@property (nonatomic , copy) NSString *present;

@property (nonatomic , copy) NSString *Address;

@property (nonatomic , copy) NSString *area;

@property (nonatomic , strong) UIImageView *heardImg;

@property (nonatomic , strong) UILabel *ageLabel;

@property (nonatomic , copy) NSString *nickname;

@property (nonatomic , copy) NSString *mobile;

@property (nonatomic , copy) NSString *nation;

@property (nonatomic , copy) NSString *jg_city;

@property (nonatomic , copy) NSString *is_remote;

@property (nonatomic , copy) NSString *smoke;

@property (nonatomic , copy) NSString *liqueur;



@property (nonatomic , copy) NSString *change001;

@property (nonatomic , copy) NSString *change002;

@property (nonatomic , copy) NSString *change003;

@property (nonatomic , copy) NSString *change004;

@property (nonatomic , copy) NSString *change005;

@property (nonatomic , copy) NSString *change006;

@property (nonatomic , copy) NSString *change007;

@property (nonatomic , strong) UITextField *currentTF;

//present

@end

@implementation YzChangeLookPersonVC

-(UITextField *)currentTF{
    
    if (!_currentTF) {
        
        _currentTF = [[UITextField alloc]init];
    
    }
    
    return _currentTF;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"编辑资料";
    
    [self CreatLookUI];
    
    [self LoadPersonInfoRequest];
    
    [self GetTelPhone];
    
}

-(void)SaveData{
    
//    [_currentTF resignFirstResponder];
    
    
//    [self.TableView reloadData];
    
    [self.view endEditing:YES];
    
    
    
    
//    NSIndexPath  *index = 0;
//  
//    [self.TableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    
  
    
    
    
    if ([VerifyPhoneNumber isBlankString:_change001]) {
        
        _change001 = @"请编辑";
    }
    if ([VerifyPhoneNumber isBlankString:_change002]) {
        
        _change002 = @"请编辑";
    }
//    if ([VerifyPhoneNumber isBlankString:_change001]) {
//        
//        _change001 = @"请编辑";
//    }
    if ([VerifyPhoneNumber isBlankString:_change004]) {
        
        _change004 = @"请编辑";
    }
    if ([VerifyPhoneNumber isBlankString:_change005]) {
        
        _change005 = @"请编辑";
    }
    if ([VerifyPhoneNumber isBlankString:_change006]) {
        
        _change006 = @"请编辑";
    }
    if ([VerifyPhoneNumber isBlankString:_change007]) {
        
        _change007 = @"请编辑";
    }

    
    
      NSLog(@"%@\n%@\n%@\n%@\n%@\n%@\n%@",_change001,_change002,_change005,_change003,_change004,_change006,_change007);
    
    [NetMethod Post:LoveDriverURL(@"APP/User/xuser") parameters:@{@"uid":LoveDriverID,@"present":_change001,@"smoke":_change002,@"liqueur":_change004,@"is_purchase":_change005,@"jg_city":_change006,@"nation":_change007} success:^(id responseObject) {
        
        if ([responseObject[@"code"] isEqual:@200]) {
            
            
            [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
            
            
            [self performSelector:@selector(JUmpBack) withObject:nil afterDelay:1.5];

            
        }else{
            
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }
        
        
        
        
    } failure:^(NSError *error) {
        
    
    
    }];
    
    //保存 数据
}


-(void)JUmpBack{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)CreatLookUI{
    
    UIBarButtonItem *rightitem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(SaveData)];
    
    self.navigationItem.rightBarButtonItem = rightitem;
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    //
    [self.view addSubview:self.TableView];
    
    
    self.headerView = [UIView new];
    
    self.headerView.frame = CGRectMake(0, 0, YzWidth, YzWidth/2);
    
    self.headerView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.tableHeaderView = self.headerView;
    
    
    
    
    
    
    UIImageView *bgIMG = [UIImageView new];
    
    bgIMG.frame = self.headerView.frame;
    
    bgIMG.image = [UIImage imageNamed:@"backImage.jpg"];
    
    [self.headerView addSubview:bgIMG];
    
    
    
    
    UIImageView *heardImg = [[UIImageView alloc]init];
    
    heardImg.frame = CGRectMake(YzWidth/2 -35, 40, 70, 70);
    
    [heardImg.layer setMasksToBounds:YES];
    //
    [heardImg.layer setCornerRadius:35.0];
    
    
    //    heardImg.image = [UIImage imageNamed:@"logo"];
    
    [self.headerView addSubview:heardImg];
    
    _heardImg = heardImg;
    
    [_heardImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
    //
    _PersonLike = @[@"是否吸烟",@"是否饮酒"];
    
    _shouru = @[@"月薪"];
    
    _Lovetype = @[@"籍贯",@"民族"];
    
    //    _MreeType = @[@"和父母同住",@"父母情况",@"兄弟姐妹"];
    
}
-(void)GetTelPhone{
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/info") parameters:@{@"uid":LoveDriverID} success:^(id responseObject) {
        
        NSDictionary *UserInfoDic = responseObject;
        
        //        NSLog(@"个人详细信息  %@",  UserInfoDic);
        
        if ([UserInfoDic[@"code"] isEqual:@200]) {
            
            NSDictionary *info = UserInfoDic [@"info"];
            
            //            吸烟
            NSString *mobile =@"";
            
            if (![info[@"mobile"] isKindOfClass:[NSNull class]]) {
                
                mobile = info[@"mobile"];
                
            }else{
                mobile = @"xxxx";
            }
            
            _mobile = mobile;
            
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)LoadPersonInfoRequest{
    
    
    [NetMethod Post:LoveDriverURL(@"APP/Circle/info") parameters:@{@"uid":LoveDriverID} success:^(id responseObject) {
        
        NSDictionary *UserInfoDic = responseObject;
        
        NSLog(@"个人详细信息  %@",  UserInfoDic);
        
        if ([UserInfoDic[@"code"] isEqual:@200]) {
            
            NSDictionary *info = UserInfoDic [@"info"];
            
            self.title = info [@"nickname"];
            
            self.nickname= info[@"nickname"];
            
            
            _ageLabel.text = info[@"age"];
            
            NSString *path = [NSString stringWithFormat:@"http://www.woaisiji.com%@",info[@"headpic"]];
            
            [_heardImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"logo"]];
            
            if (![info[@"present"] isKindOfClass:[NSNull class]]) {
            
                _present = info[@"present"];
            
            }else{
            
                _present = @"请编辑";
            
            }
            
            
            
            //            present
            
            NSDictionary *detail = info[@"detail"];
            
            if ([info[@"detail"] isKindOfClass:[NSNull class]]) {
                
                
                _PersonLikeDetail= @[@"",@""];
                
                _shouruDetail = @[@""];
                
                return ;
            }
            
            
            //            职务
            NSString *smoke =@"";
            
            if (![detail[@"smoke"] isKindOfClass:[NSNull class]]) {
                
                smoke = detail[@"smoke"];
                
            }else{
                
                smoke = @"请编辑";
                
            }
            
            _smoke = smoke;
            
            //            品牌
            NSString *liqueur =@"";
            
            if (![detail[@"liqueur"] isKindOfClass:[NSNull class]]) {
                
                liqueur = detail[@"liqueur"];
                
            }else{
                
                liqueur = @"请编辑";
                
            }
            
            _liqueur = liqueur;
            
            
            
            
            
            _PersonLikeDetail = @[smoke,liqueur];
            
            
            //            月薪
            NSString *monthly_salary = @"";
            
            
            if (![detail[@"monthly_salary"] isKindOfClass:[NSNull class]]) {
                
                monthly_salary = detail[@"monthly_salary"];
                
            }else{
                monthly_salary = @"请编辑";
            }
            
            _shouruDetail =@[monthly_salary];
            
            
            // 城市
            NSString *jg_city = @"";
            
            if (![detail[@"jg_city"] isKindOfClass:[NSNull class]]) {
                
                jg_city = detail[@"jg_city"];
                
            }else{
                jg_city = @"请编辑";
            }
            
            
            _jg_city = jg_city;
            
            //民族
            NSString *nation = @"";
            
            if (![detail[@"nation"] isKindOfClass:[NSNull class]]) {
                
                nation = detail[@"nation"];
                
            }else{
                nation = @"请编辑";
            }
            
            _nation = nation;
            
            
            
            
            NSString *jg_prov = @"";
            
            
            if (![detail[@"jg_prov"] isKindOfClass:[NSNull class]]) {
                
                jg_prov = detail[@"jg_prov"];
                
            }else{
                jg_prov = @"";
            }
            
            
            //  地址
            NSString *is_remote = @"";
            
            if (![detail[@"is_remote"] isKindOfClass:[NSNull class]]) {
                
                is_remote = detail[@"is_remote"];
                
            }else{
                is_remote = @"";
            }
            
            _is_remote = is_remote;
            
            //            [self GetCity:jg_city JgID:jg_prov nation:nation is_remote:is_remote];
            
            
        }else{
            
            [ MBProgressHUD showSuccess:@"出了什么情况" toView:self.view];
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 7;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row ==0) {
        
        return 60;
        
    }
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.textLabel.alpha = 0.7;
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    
    
    
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = @"个人独白";
            
            //            cell.detailTextLabel.text = _present;
            //
            //            cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
            //
            //            cell.detailTextLabel.numberOfLines = 0;
            
            
            UITextField *dubai = [[UITextField alloc]init];
            
            dubai.delegate  = self;

            dubai.text = _present;
            
            dubai.frame  = CGRectMake(0, 0, YzWidth - 100, 40);
            
            //            dubai.backgroundColor = [UIColor grayColor];
            _currentTF = dubai;

            dubai.textAlignment = NSTextAlignmentRight;
            
            dubai.font = [UIFont systemFontOfSize:14];
            
            dubai.tag = 100;
            
            cell.accessoryView = dubai;
            
        }
            break;
            
        case 1:
        {
            cell.textLabel.text = @"职务";
            
            UITextField *dubai = [[UITextField alloc]init];
            
            dubai.delegate  = self;

            dubai.frame = CGRectMake(0, 0,  240, 40);
            
            dubai.textAlignment = NSTextAlignmentRight;
            
            _currentTF = dubai;

            
            dubai.tag = 200;
            
            cell.accessoryView = dubai;
            
            //            UITextField
            cell.detailTextLabel.text = _smoke;
        }
            break;
        case 2:
        {
            cell.textLabel.text = @"电话";
            
            UITextField *dubai = [[UITextField alloc]init];
           
            
            dubai.delegate  = self;
            
            dubai.frame = CGRectMake(0, 0,  240, 40);
            
            dubai.textAlignment = NSTextAlignmentRight;
            
            dubai.tag = 300;
            
            cell.accessoryView = dubai;
            
            dubai.userInteractionEnabled = NO;
            
            dubai.text = _mobile;
            
        }
            break;
        case 3:
        {
            cell.textLabel.text = @"品牌";
            
            UITextField *jiguan = [[UITextField alloc]init];
            
            _currentTF = jiguan;

             jiguan.delegate  = self;
            
            jiguan.frame = CGRectMake(0, 0,  240, 40);
            
            jiguan.textAlignment = NSTextAlignmentRight;
            
            jiguan.tag = 400;
            
            cell.accessoryView = jiguan;
            //品牌
            jiguan.text = _liqueur;
        }
            break;
        case 4:
        {
            cell.textLabel.text = @"地址";
            
            UITextField *dubai = [[UITextField alloc]init];
             dubai.delegate  = self;
            
            _currentTF = dubai;

            dubai.frame = CGRectMake(0, 0,  240, 40);
            
            dubai.textAlignment = NSTextAlignmentRight;
            
            dubai.tag = 500;
            
            cell.accessoryView = dubai;
            
            dubai.text= _is_remote;
            
        }
            
            break;
        case 5:
        {
            cell.textLabel.text = @"城市";
            
            UITextField *dubai = [[UITextField alloc]init];
            
            _currentTF = dubai;

             dubai.delegate  = self;
            
            dubai.frame = CGRectMake(0, 0,  240, 40);
            
            dubai.textAlignment = NSTextAlignmentRight;
            
            dubai.tag = 600;
            
            cell.accessoryView = dubai;
            
            dubai.text = _jg_city;
            
        }
            break;
        case 6:
        {
            cell.textLabel.text = @"民族";
            
            UITextField *dubai = [[UITextField alloc]init];
            
            _currentTF = dubai;
            
            dubai.delegate  = self;
            
            dubai.frame = CGRectMake(0, 0,  240, 40);
            
            dubai.textAlignment = NSTextAlignmentRight;
            
            dubai.tag = 700;
            
            cell.accessoryView = dubai;
            
            dubai.text = _nation;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}


//-(void)textFieldDidEndEditing:(UITextField *)textField{

    
    
//    switch (textField.tag) {
//        case 100:
//        {
//            if ([textField.text isEqualToString:_present]) {
//                
//                _change001 = _present;
//                
//            }else{
//                
//                _change001 = textField.text;
//            }
//        }
//            break;
//        case 200:
//        {
//            if ([textField.text isEqualToString:_present]) {
//                
//                _change002 = _present;
//                
//            }else{
//                
//                _change002 = textField.text;
//            }
//        }
//            break;
//        case 300:
//        {
////            if ([textField.text isEqualToString:_present]) {
////                
////                _change003 = _present;
////                
////            }else{
////                
////                _change003 = textField.text;
////            }
// 
//        }
//            break;
//        case 400:
//        {
//            if ([textField.text isEqualToString:_present]) {
//                
//                _change004 = _present;
//                
//            }else{
//                
//                _change004 = textField.text;
//            }
//
//        }
//            break;
//        case 500:
//        {
//            if ([textField.text isEqualToString:_present]) {
//                
//                _change005 = _present;
//                
//            }else{
//                
//                _change005 = textField.text;
//            }
// 
//        }
//            break;
//        case 600:
//        {
//            if ([textField.text isEqualToString:_jg_city]) {
//                
//                _change006 = _jg_city;
//                
//            }else{
//                
//                _change006 = textField.text;
//            }
// 
//        }
//            break;
//        case 700:
//        {
//            if ([textField.text isEqualToString:_nation]) {
//                
//                _change007 = _nation;
//                
//            }else{
//                
//                _change007 = textField.text;
//            }
//
//        }
//            break;
//        default:
//            break;
//    }
//}

-( void)GetCity:(NSString *)CityId JgID:(NSString *)JgID  nation:(NSString *)nation
      is_remote:(NSString *)is_remote{
    
    [NetMethod Post:LoveDriverURL(@"APP/Public/get_city_address") parameters:@{@"prov":JgID,@"city":CityId} success:^(id responseObject) {
        
        NSDictionary *area = responseObject;
        //        NSLog(@" 地址在哪里  %@",area[@"address"]);
        
        _area = area[@"address"];
        
        
        _LovetypeDetail = @[ _area, nation];
        
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
        
        
    }];
    
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    switch (textField.tag) {
        case 100:
        {
            if ([textField.text isEqualToString:_present]) {
                
                _change001 = _present;
                
            }else{
                
                _change001 = textField.text;
            }
        }
            break;
        case 200:
        {
            if ([textField.text isEqualToString:_present]) {
                
                _change002 = _present;
                
            }else{
                
                _change002 = textField.text;
            }
        }
            break;
        case 300:
        {
            //            if ([textField.text isEqualToString:_present]) {
            //
            //                _change003 = _present;
            //
            //            }else{
            //
            //                _change003 = textField.text;
            //            }
            
        }
            break;
        case 400:
        {
            if ([textField.text isEqualToString:_present]) {
                
                _change004 = _present;
                
            }else{
                
                _change004 = textField.text;
            }
            
        }
            break;
        case 500:
        {
            if ([textField.text isEqualToString:_present]) {
                
                _change005 = _present;
                
            }else{
                
                _change005 = textField.text;
            }
            
        }
            break;
        case 600:
        {
            if ([textField.text isEqualToString:_jg_city]) {
                
                _change006 = _jg_city;
                
            }else{
                
                _change006 = textField.text;
            }
            
        }
            break;
        case 700:
        {
            if ([textField.text isEqualToString:_nation]) {
                
                
                
                _change007 = _nation;
                
            }else{
                
                _change007 = textField.text;
            }
            
        }
            break;
        default:
            break;
    }
    
    
    _currentTF = textField;

    return YES;
}





@end
