//
//  YzLookPersonVC.m
//  ASJ
//
//  Created by Jack on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzLookPersonVC.h"

@interface YzLookPersonVC ()<UITableViewDelegate,UITableViewDataSource>{

    NSArray *_PersonLike;
    
    NSArray *_shouru;
    
    NSArray *_Lovetype;
    
    NSArray *_MreeType;
    
}


@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic ,strong)  NSArray *PersonLikeDetail;

@property (nonatomic ,strong)  NSArray *shouruDetail;

@property (nonatomic ,strong)  NSArray *LovetypeDetail;

@property (nonatomic ,strong)  NSArray *MreeTypeDetail;

@property (nonatomic , copy) NSString *signature;

@property (nonatomic , copy) NSString *present;

@property (nonatomic , copy) NSString *Address;

@property (nonatomic , copy) NSString *area;

@property (nonatomic , copy) NSString *nation;

@property (nonatomic , strong) UIImageView *heardImg;

@property (nonatomic , strong) UILabel *ageLabel;

@property (nonatomic , copy) NSString *nickname;

@property (nonatomic , copy) NSString *mobile;

@property (nonatomic , copy) NSString *is_remote;

@property (nonatomic , copy) NSString *smoke;

@property (nonatomic , copy) NSString *liqueur;

@property (nonatomic , copy) NSString *jg_city;


//present

@end

@implementation YzLookPersonVC




- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self CreatLookUI];
    
    
    [self LoadPersonInfoRequest];
    
    [self GetTelPhone];

}

-(void)CreatLookUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight-114)];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    
//    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
//    
    [self.view addSubview:self.TableView];
    
    
    self.headerView = [UIView new];
    
    self.headerView.frame = CGRectMake(0, 0, YzWidth, YzWidth/2);
   
    self.headerView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.tableHeaderView = self.headerView;
    
    UIImageView *bgIMG = [UIImageView new];
    bgIMG.frame = self.headerView.frame;
    bgIMG.image = [UIImage imageNamed:@"backImage.jpg"];
    [self.headerView addSubview:bgIMG];
    
    
    
    UIImageView *heardImg = [[UIImageView alloc]init];
    
    heardImg.frame = CGRectMake(YzWidth/2 -35, YzWidth/4 - 10, 70, 70);
    
    [heardImg.layer setMasksToBounds:YES];
    //
    [heardImg.layer setCornerRadius:35.0];

    
//    heardImg.image = [UIImage imageNamed:@"logo"];
    
    [self.headerView addSubview:heardImg];
    
    _heardImg = heardImg;
    
    
    
    UILabel *ageLabel = [[UILabel alloc]init];
    
    ageLabel.frame = CGRectMake(0, 0, 0, 0);
    
    [self.headerView addSubview:ageLabel];
    
    _ageLabel = ageLabel;
    
    
    
    
    
    
    UIView *footer = [[UIView alloc]init];
    
    footer.frame = CGRectMake(0, YzHeight-114, YzWidth, 50);
    
    footer.backgroundColor =RGBColor(71, 71, 71);
    
    [self.view addSubview:footer];
    
    NSArray *btnIMgArray = @[@"YzMessege",@"haoyou"];
    
    NSArray *btnName = @[@"打招呼",@"加好友"];
    
    for (int i = 0; i<2; i++) {
        
        UIButton *BTN = [UIButton buttonWithType:UIButtonTypeCustom];
        
        BTN.frame = CGRectMake(YzWidth/4* (2*i +1) -60, 5, 110, 40);
        
        [BTN setImage:[UIImage imageNamed:btnIMgArray[i]] forState:UIControlStateNormal];

        BTN.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

//        [BTN sizeToFit];
        
        BTN.tag = 300+i;
        
        [BTN addTarget:self action:@selector(SelectorSayhi:) forControlEvents:UIControlEventTouchUpInside];
        
        [BTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        [BTN setTitle:btnName[i] forState:UIControlStateNormal];
        
        BTN.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [footer addSubview:BTN];
        
        
    }

    
    _PersonLike = @[@"电话",@"品牌"];
    
    
//    _PersonLike = @[@"是否吸烟",@"是否饮酒",@"锻炼习惯",@"饮食习惯",@"逛街购物",@"宗教信仰",@"作息时间",@"交际圈子",@"最大消费"];
    
    _shouru = @[@"月薪"];
    
    _Lovetype = @[@"城市",@"民族"];
    
//    _MreeType = @[@"和父母同住",@"父母情况",@"兄弟姐妹"];

}

-(void)GetTelPhone{
    
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/info") parameters:@{@"uid":self.PerSonID} success:^(id responseObject) {
        
        NSDictionary *UserInfoDic = responseObject;
        
//        NSLog(@"个人详细信息  %@",  UserInfoDic);
        
        if ([UserInfoDic[@"code"] isEqual:@200]) {
        
        
        
            NSDictionary *info = UserInfoDic [@"info"];

        
            
//            吸烟
            NSString *mobile =@"";
            if (![info[@"mobile"] isKindOfClass:[NSNull class]]) {
                
                mobile = info[@"mobile"];
                
            }else{
                mobile = @"";
            }
            
        
        
            _mobile = mobile;
            
            
            
            
            
            
            
            
        }
        
        
        
        
    } failure:^(NSError *error) {
        
    }];
    
}



-(void)LoadPersonInfoRequest{
    
    
    
    
    
    
    
    [NetMethod Post:LoveDriverURL(@"APP/Circle/info") parameters:@{@"uid":self.PerSonID} success:^(id responseObject) {
        
        NSDictionary *UserInfoDic = responseObject;
        
        NSLog(@"个人详细信息  %@",  UserInfoDic);
        
        if ([UserInfoDic[@"code"] isEqual:@200]) {
            
        
            NSDictionary *info = UserInfoDic [@"info"];
            
            
            self.title = info [@"nickname"];
            
            self.nickname= info[@"nickname"];
            
            
            _ageLabel.text = info[@"age"];
        
            NSString *path = [NSString stringWithFormat:@"http://www.woaisiji.com%@",info[@"headpic"]];
            
            [_heardImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"logo"]];
            
            if (![info[@"signature"] isKindOfClass:[NSNull class]]) {
                _signature    = info[@"signature"];
            }else{
                _signature = @"";
            }

            
            if (![info[@"present"] isKindOfClass:[NSNull class]]) {
                _present = info[@"present"];
            }else{
                _present = @"";
            }
            
//            present
            
            NSDictionary *detail = info[@"detail"];
            if (!detail) {
                return;
            }
            if ([info[@"detail"] isKindOfClass:[NSNull class]]) {
                
                
                _PersonLikeDetail= @[@"",@""];
                
                _shouruDetail = @[@""];
                
                return ;
            }
            
            
//            职务
            NSString *smoke =@"";
            if (![detail[@"smoke"] isKindOfClass:[NSNull class]]) {
            
                smoke = detail[@"smoke"];
            
            }else{
                smoke = @"暂无";
            }
            
            
            _smoke = smoke;
            
            
            
//    品牌
            NSString *liqueur =@"";
            
            if (![detail[@"liqueur"] isKindOfClass:[NSNull class]]) {
                
                liqueur = detail[@"liqueur"];
                
            }else{
                liqueur = @"暂无";
            }

            _liqueur =liqueur;
//            _PersonLikeDetail = @[smoke,liqueur];
            
            
            
            
            
//            月薪
            NSString *monthly_salary = @"";
            
            
            if (![detail[@"monthly_salary"] isKindOfClass:[NSNull class]]) {
                
                monthly_salary = detail[@"monthly_salary"];
                
            }else{
                monthly_salary = @"";
            }

//            购车
            NSString *is_car =@"";
            
            if (![detail[@"is_car"] isKindOfClass:[NSNull class]]) {
                
                is_car = detail[@"is_car"];
                
            }else{
                is_car = @"";
            }

//            购房
            NSString *is_purchase = @"";
            
            if (![detail[@"is_purchase"] isKindOfClass:[NSNull class]]) {
                
                is_purchase = detail[@"is_purchase"];
                
            }else{
                is_purchase = @"";
            }

            
            
            _shouruDetail =@[monthly_salary];
            
            
            
            NSString *jg_city = @"";
            
            if (![detail[@"jg_city"] isKindOfClass:[NSNull class]]) {
                
                jg_city = detail[@"jg_city"];
                
            }else{
                jg_city = @"暂无";
            }

            _jg_city = jg_city;
            
            
            NSString *jg_prov= @"";
           
            
            if (![detail[@"jg_prov"] isKindOfClass:[NSNull class]]) {
                
                jg_prov = detail[@"jg_prov"];
                
            }else{
                jg_prov = @"";
            }
            
            
            NSString *is_remote = @"";
            
            if (![detail[@"is_remote"] isKindOfClass:[NSNull class]]) {
                
                is_remote = detail[@"is_remote"];
                
            }else{
                is_remote = @"暂无";
            }
            
            _is_remote = is_remote;
            
            
            NSString *nation = @"";
            
            if (![detail[@"nation"] isKindOfClass:[NSNull class]]) {
                
                nation = detail[@"nation"];
                
            }else{
                nation = @"暂无";
            }

            
            _nation = nation;
            
            [self GetCity:jg_city JgID:jg_prov nation:nation is_remote:is_remote];
        
            
//            NSString *address =
            
      
            
            
//            [self.TableView reloadData];

        }else{
            
            [ MBProgressHUD showSuccess:@"出了什么情况" toView:self.view];
        }
        
        [self.TableView reloadData];

    } failure:^(NSError *error) {
        
    }];
    
    
}
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    
//    return 5;
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{


    return 7;
//    return self.cellTitleArray.count;

}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (indexPath.section ==2  ||  indexPath.section == 1) {
//        
//        return 60;
//
//    }
//    return 44;
    
    if (indexPath.row ==0) {
        
        return 60;
        
    }
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.textLabel.alpha = 0.7;
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    
    
    
    switch (indexPath.row) {
//        case 0:
//        {
//            cell.textLabel.text = @"个性签名";
//            
//            cell.detailTextLabel.text = _signature;
//         
//            cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
//            
//            cell.detailTextLabel.numberOfLines = 0;
//
//        }
//            break;
        case 0:
        {
            cell.textLabel.text = @"个人独白";
            
            cell.detailTextLabel.text = _present;
            
            cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
            
            cell.detailTextLabel.numberOfLines = 0;

       
        }
            break;
//        case 2:
//        {
//            cell.textLabel.text = @"司机圈";
//            
//            cell.detailTextLabel.text = @"";
//            
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }
//            break;
        case 1:
        {
            cell.textLabel.text = @"职务";
            
            cell.detailTextLabel.text = _smoke;

        }
            break;
        case 2:
        {
            cell.textLabel.text = @"电话";
        
            cell.detailTextLabel.text = _mobile;

        }
            break;
        case 3:
        {
            cell.textLabel.text = @"品牌";
            
            cell.detailTextLabel.text = _liqueur;

        }
            break;
        case 4:
        {
            cell.textLabel.text =@"地址";
            
            cell.detailTextLabel.text = _is_remote;

        }
            break;
        case 5:
        {
            cell.textLabel.text = @"城市";
            
            cell.detailTextLabel.text = _jg_city;
            
        }
            break;
        case 6:
        {
            cell.textLabel.text = @"民族";
            
            cell.detailTextLabel.text = _nation;
        }
            break;
        default:
            break;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section ==6) {
        return 0;
    }
    return 20;
}

-(void)SelectorSayhi:(UIButton *)sender{
    
    
    switch (sender.tag) {
        case 300:
        {
            
            
//           打招呼
            
        }
            break;
            
        default:
        {
            
//           加好友
            NSDictionary *Info = @{@"uid":LoveDriverID,@"add_who":self.PerSonID,@"who_add":LoveDriverID};
            
           
            [NetMethod Post:LoveDriverURL(@"APP/Friend/add") parameters:Info success:^(id responseObject) {
                
                if ([responseObject[@"code"] isEqual:@200]) {
                    
                    [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                
                }else{
                    
                    [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                }
                
            } failure:^(NSError *error) {
                
            }];
            
            
        }
            break;
    }
    
    
}



-( void)GetCity:(NSString *)CityId JgID:(NSString *)JgID  nation:(NSString *)nation
      is_remote:(NSString *)is_remote{
    
    [NetMethod Post:LoveDriverURL(@"APP/Public/get_city_address") parameters:@{@"prov":JgID,@"city":CityId} success:^(id responseObject) {
        
        NSDictionary *area = responseObject;
//        NSLog(@" 地址在哪里  %@",area[@"address"]);
    
        _area = area[@"address"];
     
        
        _LovetypeDetail = @[ _area, nation];
        
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    
    
    }];
    
}


@end
