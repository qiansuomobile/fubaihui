//
//  YzSendCicle.m
//  ASJ
//
//  Created by Jack on 16/10/10.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzSendCicle.h"

@interface YzSendCicle ()<MessagePhotoViewDelegate,ZBMessageShareMenuViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>



@property (nonatomic,strong) MessagePhotoView *photoView;

@property (nonatomic , strong) NSMutableArray *imageArray;

@property (nonatomic , strong) NSMutableArray *imageIDS;

@property (nonatomic , strong) UITextView *textView;

@property (nonatomic , strong) MBProgressHUD *hud ;


@end

@implementation YzSendCicle

-(NSMutableArray *)imageArray{
    
    if (!_imageArray) {
        
        _imageArray = [NSMutableArray array];
        
    }
    
    return _imageArray;
}

-(NSMutableArray *)imageIDS{
    
    if (!_imageIDS) {
        
        _imageIDS = [NSMutableArray array];
        
    }
    
    return _imageIDS;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"发布";
    
    self.view.backgroundColor = [UIColor whiteColor];
   
    [self addOptionRighButton];
    
    //图片视图
    [self sharePhotoview];
    
    UITextView *textView = [[UITextView alloc]init];
    
    textView.delegate = self;
    textView.font = [UIFont systemFontOfSize:15.0f];
    
    textView.text = @"说点什么吧";
    
    textView.frame = CGRectMake(0, 0, YzWidth, 100);
    
    [self.view addSubview:textView];
    
    _textView = textView;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(YzDown)];
    
    tap.delegate =self;
    
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];

    
    
    
}
-(void)YzDown{
    

    [self.view endEditing:YES];
    //[_textView resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    _textView= textView;
   
    if([textView.text isEqualToString:@"说点什么吧"]){
    
        textView.text = @"";
    
    }
    
    [textView becomeFirstResponder];
    
//    return YES;
}

- (void)sharePhotoview
{
    if (!self.photoView)
    {
        self.photoView = [[MessagePhotoView alloc]initWithFrame:CGRectMake(0,100,YzWidth, 200)];
        
        [self.view addSubview:self.photoView];
        
        self.photoView.delegate = self;
        
    }
}


//加载右边button
- (void) addOptionRighButton {
   
    UIButton *optionButton = [UIButton buttonWithType: UIButtonTypeCustom];
    
    [optionButton setFrame: CGRectMake(0, 25, 60, 40)];
    
    [optionButton setTitle:@"提交" forState:UIControlStateNormal];
    
    [optionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [optionButton setAlpha:0.85];
   
    optionButton.titleLabel.font = [UIFont systemFontOfSize:16];
   
    [optionButton addTarget: self action: @selector(optionRightButtonAction) forControlEvents: UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: optionButton];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(BackToVC)];
    
    self.navigationItem.leftBarButtonItem.title = @"取消";

}

- (void)optionRightButtonAction {
    
    [_textView resignFirstResponder];
    
    if (self.photoView.photoMenuItems.count==0) {
        
        _hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];

        [self performSelector:@selector(SendCicle) withObject:nil afterDelay:1];
        
    }else{
    
    
    for (NSData *data in self.photoView.ImageDatas) {
        [NetMethod Post:FBHRequestUrl(kUrl_img_upload)  parameters:nil IMGparameters:@{@"img":data} success:^(id responseObject){

            NSDictionary *dic = responseObject;
            [self.imageIDS  addObject:dic[@"data"]];
            } failure:^(NSError *error) {
                
            }];
    }
        _hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
        
        [self performSelector:@selector(SendCicle) withObject:nil afterDelay:2];

    }

}



-(void)SendCicle{
    
    NSString *textString = _textView.text;
    
    if ([VerifyPictureURL isBlankString: _textView.text ]) {
        textString = @" ";
    }
    
    NSDictionary *DIC = nil;
    
    if(self.imageIDS.count>0){
        
        NSString *string = [NSString stringWithFormat:@"%@",self.imageIDS];
        
        string = [string substringFromIndex:1];
        
        string =  [[string componentsSeparatedByString:@")"] firstObject];
        
        string=[string stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        string=[string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSLog(@"   图片id数组    string   =====  %@",string);
        DIC = @{@"uid":LoveDriverID,@"content":textString,@"picture":string};
    
    }else{

        DIC = @{@"uid":LoveDriverID,@"content":textString,@"picture":@""};
    }
    
    [NetMethod Post:FBHRequestUrl(kUrl_circle_add) parameters:DIC success:^(id responseObject) {
        
        [_hud hide:YES];
        
//        NSLog(@"发布=======    %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            [MBProgressHUD showSuccess:@"发布成功" toView:self.view];
            
            [self performSelector:@selector(BackToVC) withObject:nil afterDelay:1];
 
        }else{
           
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }
        
        
    } failure:^(NSError *error) {
        
    }];

}

-(void)BackToVC{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//实现代理方法
-(void)addPicker:(ZYQAssetPickerController *)picker{
    
    [self presentViewController:picker animated:YES completion:nil];
}

//08.23 相机调用跳转
-(void)addUIImagePicker:(UIImagePickerController *)picker
{
    [self presentViewController:picker animated:YES completion:nil];
}


#pragma mark - ZBMessageShareMenuView Delegate
- (void)didSelecteShareMenuItem:(ZBMessageShareMenuItem *)shareMenuItem atIndex:(NSInteger)index{
    
//    NSLog(@"xxxx");
}

@end
