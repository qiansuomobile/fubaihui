//
//  SDTimeLineCell.h
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 362419100(2群) 459274049（1群已满）
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */
#import "SDTimeLineCellModel.h"

#import "UIView+SDAutoLayout.h"

#import "SDTimeLineCellCommentView.h"

#import "SDWeiXinPhotoContainerView.h"

#import "SDTimeLineCellOperationMenu.h"

#import <UIKit/UIKit.h>

@protocol SDTimeLineCellDelegate <NSObject>

- (void)didClickLikeButtonInCell:(UITableViewCell *)cell;

- (void)didClickcCommentButtonInCell:(UITableViewCell *)cell;

- (void)didClickcSharButtonInCell:(UITableViewCell *)cell;


@end

@class SDTimeLineCellModel;

@interface SDTimeLineCell : UITableViewCell{
    
    
        UIImageView *_iconView;
        
        UILabel *_nameLable;
        
        UILabel *_contentLabel;
        
        SDWeiXinPhotoContainerView *_picContainerView;
        
        UILabel *_timeLabel;
        
        UIButton *_moreButton;
        
        UIButton *_operationButton;
        
        SDTimeLineCellCommentView *_commentView;
        
        BOOL _shouldOpenContentLabel;
        
        SDTimeLineCellOperationMenu *_operationMenu;
        
        UILabel *_YzTimeLabel;
    
}

@property (nonatomic, weak) id<SDTimeLineCellDelegate> delegate;

@property (nonatomic, strong) SDTimeLineCellModel *model;

@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, copy) void (^moreButtonClickedBlock)(NSIndexPath *indexPath);

@property (nonatomic , copy) void (^DianzanButtonClickdBlock)(NSIndexPath *indexPath);


@property (nonatomic, copy) void (^didClickCommentLabelBlock)(NSString *commentId, CGRect rectInWindow, NSIndexPath *indexPath);

@end
