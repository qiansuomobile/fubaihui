//
//  SDTimeLineTableHeaderView.m
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 362419100(2群) 459274049（1群已满）
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */
#import "YzChangeLookPersonVC.h"

#import "SDTimeLineTableHeaderView.h"

#import "UIView+SDAutoLayout.h"

@implementation SDTimeLineTableHeaderView
//{
//    UIImageView *_backgroundImageView;
//  
//    UIImageView *_iconView;
//    
//    UILabel *_nameLabel;
//
//}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
//        [self setup];
    
        
        [self CreatUI];
    }
    
    return self;
}

//
//-(void)Jump:(id)jump{
//
//
//    NSLog(@"**********");
//    
//}

- (void)setup
{
    
//    __weak typeof(self) weakSelf = self;

    
//    self.backgroundColor = RGBColor(250, 251, 251);
//    
//    _backgroundImageView = [UIImageView new];
//    
//    _backgroundImageView.image = [UIImage imageNamed:@"pbg.jpg"];
//    
//    [self addSubview:_backgroundImageView];
    
//  头像  _HeaderImg
    
    _HeaderImg = [UIImageView new];
    
    [_HeaderImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];

    //    _HeaderImg.layer.borderColor = [UIColor whiteColor].CGColor;

    //    _HeaderImg.layer.borderWidth = 3;
    
    _HeaderImg.layer.masksToBounds = YES;
    
    _HeaderImg.layer.cornerRadius = YzWidth/6;
    
    [self addSubview:_HeaderImg];

//    _backgroundImageView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(-60, 0, 0, 0));

//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Jump:)];
//    
//    [self addGestureRecognizer:tap];

    
    
    
//    [weakSelf setJump:^{
//       
//        if ([weakSelf.delegate respondsToSelector:@selector(Jump:)]) {
//            [weakSelf.delegate Jump:weakSelf];
//        }
//
//    }];
//    
    
    
//    if ([weakSelf.delegate respondsToSelector:@selector(Jump:)]) {
//        
//        [weakSelf.delegate Jump:weakSelf];
//        
//    }

    
    
    
    _HeaderImg.frame = CGRectMake(YzWidth/3, 25, YzWidth/3, YzWidth/3);
    
    
//     self.headBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    
////    self.headBtn  = [[UIButton alloc]init];
//    
//     self.headBtn.frame = CGRectMake(0, 0, 0, 240);
//    
//    [ self.headBtn addTarget:self action:@selector(Jump) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self addSubview: self.headBtn];

    
    

    
    
    
}


-(void)CreatUI{
    
    UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = CGRectMake(0, 0, YzWidth, 240);
    
    btn.backgroundColor = [UIColor blackColor];
    
    //    [ btn addTarget:self action:@selector(Jumppp) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview: btn];
    
    _headBtn = btn;

    
    self.backgroundColor = RGBColor(250, 251, 251);
    
    UIImageView* _backgroundImageView = [UIImageView new];
    
    _backgroundImageView.image = [UIImage imageNamed:@"pbg.jpg"];
    
    [self addSubview:_backgroundImageView];

    _backgroundImageView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(-60, 0, 0, 0));
    
    

    
    _HeaderImg = [UIImageView new];
    
    [_HeaderImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    _HeaderImg.layer.masksToBounds = YES;
    
    _HeaderImg.layer.cornerRadius = YzWidth/6;
    
    [self addSubview:_HeaderImg];
    
    _HeaderImg.frame = CGRectMake(YzWidth/3, 25, YzWidth/3, YzWidth/3);
    
    
    
    
    
    
    
}

//-(void)Jump{
//    
//    
//    if (self.Jump) {
//        self.Jump();
//    }
//    
//}
//

@end
