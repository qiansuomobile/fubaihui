//
//  YzCollectionViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
#import "YzCollectionDetail.h"
#import "YzshoucangCell.h"
#import "YzShouCangModel.h"
#import "GoodsDetailViewController.h"
#import "YzMyoderModel.h"

@interface YzCollectionDetail ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end



static NSInteger Pages;

@implementation YzCollectionDetail


- (NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    return _sources;
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages =1;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadUI];
    
    [self RequsturlData];
    
    [self setRefreshing];
    
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self loadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}


-(void)loadMoreData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"2",@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/gzList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的收藏   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzShouCangModel *model = [[YzShouCangModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                NSArray *dataArray = infoDic[@"data"];
                
                if (dataArray.count == 0) {
                    
                    
                }else{
                    
                    NSDictionary *DataDic = dataArray[0];
                    model =  [YzShouCangModel  objectWithKeyValues:DataDic];
                    model.cover = DataDic [@"cover"];
                    
                    [freshA  addObject:model];
                    
                    NSLog(@"%@",model.cover);
                    
                }
                
            }
            
            [self.sources  addObjectsFromArray:freshA];
            
            
            NSLog(@"数组个数   %lu",(unsigned long)self.sources.count);
            
            //            NSLog(@"数据源   %@",self.sources );
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
        //        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)RequsturlData{
    
    //    我的收藏列表
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"2",@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/gzList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的收藏  %@",dic );
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            if (![dic[@"list"] isKindOfClass:[NSNull class]]) {
                
                NSArray *info = dic[@"list"];
                
                YzShouCangModel *model = [[YzShouCangModel alloc]init];
                
                [self.sources removeAllObjects];
                
                
                if ([info isKindOfClass:[NSNull class]]) {
                    
                    [self.TableView.mj_footer endRefreshing];
                    
                    [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                    
                    return ;
                }
                
                
                
                
                for (NSDictionary *infoDic in info) {
                    
                    NSArray *dataArray = infoDic[@"data"];
                    
                    
                    if ([dataArray isKindOfClass:[NSNull class]]) {
                        
                        return ;
                    }

                    NSDictionary *DataDic = dataArray[0];
                    
                    
                    model =  [YzShouCangModel  objectWithKeyValues:DataDic];

                    model.cover = DataDic [@"cover"];
                    
                    [self.sources  addObject:model];
                    
                }
                
                
            }
            else{
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return;
                
            }
            
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}


//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//
//    return self.sources.count;
//}
////


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources .count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzshoucangCell *cell = [YzshoucangCell cellWithTableView:tableView];
    
    YzShouCangModel *model = [[YzShouCangModel alloc]init];
    
    model =self.sources[indexPath.row];
    
    [cell setOrderModel:model];
    
    
    [cell.BuyCar  addTarget:self action:@selector(BuyCatBtn) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}



-(void)BuyCatBtn{
    
    
//    兑换商城
    
    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc]init];
    
    YzShouCangModel *model = [[YzShouCangModel alloc]init];
    
    model =self.sources[indexPath.row];
    
//    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
    
}



-(void)requestDetailDataWith:(NSString *)goodID{
   
    NSString *urlStr;
  
//    switch (self.ShopType) {
//        case 1:
//            urlStr = @"APP/Shop/detail";
//            break;
//        case 2:
            urlStr = @"APP/Shopa/detail";
//            break;
//        case 3:
//            urlStr = @"APP/Shopyhls/detail";
//            break;
//        default:
//            break;
//    }
    NSDictionary *dic = @{@"id":goodID};
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
   
    [NetMethod Post:LoveDriverURL(urlStr) parameters:dic success:^(id responseObject) {
        //NSLog(@"responsed === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            //除了正品商城,其他商城暂时不显示大图
//            
//            if (self.ShopType == 1) {
//                sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
//            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                if ([sortGood.number isEqualToString:@"0"]) {
                    [MBProgressHUD showSuccess:@"没存货了亲~" toView:self.view];
                }else{
                    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
                    goodDetailVC.sortGoods = sortGood;
//                    if (self.ShopType == 2) {
                        //兑换商城
                        goodDetailVC.isConvert = YES;
//                    }
                    [self.navigationController pushViewController:goodDetailVC animated:YES];
                }
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}



@end
