//
//  YzCollectionDetailCell.h
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzMyoderModel.h"
#import <UIKit/UIKit.h>

#import "YzOrderArrayModel.h"

@interface YzCollectionDetailCell : UITableViewCell


@property (nonatomic , strong) UIButton *BuyCar;

//@property (nonatomic , strong) UILabel *GoldLabel;
//
//@property (nonatomic , strong) UILabel *SilverLabel;
@property (nonatomic , strong) NSString *ifOrNoGold;



-(void)setOrderModel:(YzMyoderModel *)model OrderModel:(YzOrderArrayModel *)OrderModel;

-(void)setOrderModel:(YzMyoderModel *)model ;


+ (instancetype)cellWithTableView:(UITableView *)tableView;


-(void)setUPframe;


@end
