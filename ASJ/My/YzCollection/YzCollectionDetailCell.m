//
//  YzCollectionDetailCell.m
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzCollectionDetailCell.h"




@interface YzCollectionDetailCell ()

@property (nonatomic, weak) UILabel *date;
@property (nonatomic, strong) UILabel *order_sn;
@property (nonatomic, strong) UILabel *state;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, weak) UIImageView *imageV;


@property (nonatomic , strong) UILabel *MoneyLabel;

@property (nonatomic , strong) UILabel *BusNameLabel;

@property (nonatomic , strong) UILabel *QQMoneyLabel;


@property (nonatomic , strong) UILabel *SumLabel;

@end


@implementation YzCollectionDetailCell








- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}



- (void)setUpSubview{
    
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    
//    imageV.backgroundColor = [UIColor whiteColor];
    
//    imageV.image = [UIImage imageNamed:@"kefu"];
    
    [self addSubview:imageV];
    
    _imageV = imageV;
    
    
    
    
    
    UILabel *date = [[UILabel alloc]init];
    
    date.font = [UIFont systemFontOfSize:13];
    
    date.numberOfLines = 0;
    
    //    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    //    date.textColor = [UIColor grayColor];
    
    [self addSubview:date];
    
    _BusNameLabel = date;
    
    
    
    UILabel *date2 = [[UILabel alloc]init];
    
    date2.font = [UIFont systemFontOfSize:11];
    
    date2.textColor = Red;
    
//    date2.text = @"送100Q币";
    
    date2.backgroundColor = [UIColor redColor];
    
    [date2.layer setMasksToBounds:YES];
    
    [date2.layer setCornerRadius:2.0];
    
    date2.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:date2];
    
    _QQMoneyLabel = date2;
    
    
    
    
    
    
    UILabel *date3 = [[UILabel alloc]init];
    
    date3.font = [UIFont systemFontOfSize:14];
    date3.textColor = Red;
    
    //    date3.text = @"888";
    
    date3.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:date3];
    
    _MoneyLabel = date3;
    
    
    
    
    
    UILabel *date4 = [[UILabel alloc]init];
    
    date4.font = [UIFont systemFontOfSize:11];
    
    date4.textColor = [UIColor redColor];
    
    //    date.textColor = [UIColor grayColor];
    
    //    date3.text = @"888";
    
    date4.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:date4];
    
    
    
    
    
    
    UILabel *date5 = [[UILabel alloc]init];
    
    date5.font = [UIFont systemFontOfSize:11];
    
    date5.textColor = [UIColor redColor];
    
    
    //    date.textColor = [UIColor grayColor];
    
    //    date3.text = @"888";
    
    date5.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:date5];
    
    
    
    
    
    
    
    
    
    UILabel *date6 = [[UILabel alloc]init];
    
    date6.font = [UIFont systemFontOfSize:11];
    
    //    date6.textColor = [UIColor redColor];
    
    
    //    date.textColor = [UIColor grayColor];
    
    //    date3.text = @"888";
    
    date6.textAlignment = NSTextAlignmentRight;
    
    [self addSubview:date6];
    
    _SumLabel = date6;
    
    
    
    
    
    
    UIButton *BuyCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //    [BuyCarBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    
//    BuyCarBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    //    [BuyCarBtn setTintColor:[UIColor redColor]];
    
//    [BuyCarBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//
//    [BuyCarBtn.layer setMasksToBounds:YES];
//    [BuyCarBtn.layer setCornerRadius:2.0]; //设置矩形四个圆角半径
//    //边框宽度
//    [BuyCarBtn.layer setBorderWidth:1.0];
//    //设置边框颜色有两种方法：第一种如下:
    //    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //    CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0, 0, 0, 1 });
    //    [BuyCarBtn.layer setBorderColor:colorref];//边框颜色
    //第二种方法如下:
//    BuyCarBtn.layer.borderColor=[UIColor redColor].CGColor;
    
    [BuyCarBtn setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:BuyCarBtn];
    
    
    _BuyCar = BuyCarBtn;
    
    //    _BuyCar.hidden = YES;
    
    
    
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell){
    
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    
    }
    
    return cell;
    
}

-(void)setUPframe{
    
    
//    self.BuyCar.frame = CGRectMake(YzWidth - 100, 115, 70, 20);
    
    self.imageV.frame = CGRectMake(12, 10, 100, 100);
    
    self.BuyCar.frame = self.imageV.frame;
    
    self.BusNameLabel.frame = CGRectMake(122, 0, YzWidth - 130, 55);
    
    //    self.MoneyLabel.frame = CGRectMake(130, 90, 60, 20);
    
    //    self.QQMoneyLabel.frame = CGRectMake(140, 60, 60, 20);
    
    self.MoneyLabel.frame = CGRectMake(122, 60, 70, 20);
    
    self.SumLabel.frame = CGRectMake(100, 90, YzWidth -110, 20);
    
}



-(void)setOrderModel:(YzMyoderModel *)model{
    //    goods_price
    
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(model.goods_img)] placeholderImage:[UIImage imageNamed:@"logo"]];
    self.MoneyLabel.text =[NSString stringWithFormat:@"¥%@", model.goods_price];
    self.SumLabel.text = [NSString stringWithFormat:@"共%@件商品 合计:¥%@ ",model.goods_num,model.goods_price];
    self.BusNameLabel.text = model.goods_name;
    [self setUPframe];
//    if ([self.ifOrNoGold isEqualToString:@"2"]) {
//        self.MoneyLabel.text =[NSString stringWithFormat:@"银币%@", model.goods_price];
//    }else if ([self.ifOrNoGold isEqualToString:@"3"]){
//        self.MoneyLabel.text =[NSString stringWithFormat:@"金币%@", model.goods_price];
//    }else{
//
//    }
    
    
    
    
    
//    self.GoldLabel.text = [NSString stringWithFormat:@"金币:%@",model.goods_f_sorts];
    
    
//    self.SilverLabel.text = [NSString stringWithFormat:@"银币:%@",model.goods_f_silver];
//    if ([self.ifOrNoGold isEqualToString:@"2"]) {
//        self.SumLabel.text = [NSString stringWithFormat:@"共%@件商品 合计银币:%@ ",model.goods_num,model.goods_price];
//    }else if ([self.ifOrNoGold isEqualToString:@"3"]){
//        self.SumLabel.text = [NSString stringWithFormat:@"共%@件商品 合计金币:%@ ",model.goods_num,model.goods_price];
//    }else{
//
//    }
    
    
    
}


//SortGoods *model = [[SortGoods alloc]init];


-(void)setOrderModel:(YzMyoderModel *)model OrderModel:(YzOrderArrayModel *)OrderModel{
    
    //    goods_price
    
//    [self.imageV sd_setImageWithURL:[NSURL URLWithString:LoveDriverURL(model.cover)] placeholderImage:[UIImage imageNamed:@"kefu"]];
    
//    [self.imageV sd_setImageWithURL:[NSURL URLWithString:LoveDriverURL(model.cover)] placeholderImage:[UIImage imageNamed:@"logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        NSLog(@"image:%@",image);
//    }];
    if ([self.ifOrNoGold isEqualToString:@"2"]) {
        self.MoneyLabel.text =[NSString stringWithFormat:@"银积分%@", model.goods_price];
    }else if ([self.ifOrNoGold isEqualToString:@"3"]){
        self.MoneyLabel.text =[NSString stringWithFormat:@"金积分%@", model.goods_price];
    }else{
        self.MoneyLabel.text =[NSString stringWithFormat:@"¥%@.00", model.goods_price];
    }    
    self.BusNameLabel.text = model.goods_name;
    
//    self.GoldLabel.text = [NSString stringWithFormat:@"金币:%@",model.goods_f_sorts];
//
//    self.SilverLabel.text = [NSString stringWithFormat:@"银币:%@",model.goods_f_silver];
    
    
    [self setUPframe];
    
}


//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    
//    // Configure the view for the selected state
//}

@end
