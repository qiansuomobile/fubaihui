//
//  YzCollectionViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
#import "YzCollectionViewController.h"
#import "YzshoucangCell.h"
#import "YzShouCangModel.h"
#import "GoodsDetailViewController.h"
#import "YzMyoderModel.h"

@interface YzCollectionViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end



static NSInteger Pages;

@implementation YzCollectionViewController


- (NSMutableArray *)sources{

    if (!_sources) {

        _sources = [NSMutableArray array];

    }

    return _sources;

}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages =1;

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadUI];
    
    [self RequsturlData];
    
    [self setRefreshing];
    
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self loadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}

#pragma mark - URL Request

-(void)loadMoreData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,
                              @"token":FBH_USER_TOKEN,
                              @"page":page,
                              @"row_num":@"20"
                              };
    
    [NetMethod Post:FBHRequestUrl(kUrl_collect_list) parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"data"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
            }
            
            for (NSDictionary *infoDic in info) {
                [self.sources  addObject:infoDic];
            }
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
        //        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)RequsturlData{
    
    //    我的收藏列表
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,
                              @"token":FBH_USER_TOKEN,
                              @"page":@"1",
                              @"row_num":@"20"
                              };
    
    [NetMethod Post:FBHRequestUrl(kUrl_collect_list) parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            if (![dic[@"data"] isKindOfClass:[NSNull class]]) {
                
                NSArray *info = dic[@"data"];
                
                if ([info isKindOfClass:[NSNull class]]) {
                    
                    [self.TableView.mj_footer endRefreshing];
                    [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                    return ;
                }
                
                [self.sources removeAllObjects];

                for (NSDictionary *infoDic in info) {
                    YzShouCangModel *model = [[YzShouCangModel alloc]init];
                    [model setValuesForKeysWithDictionary:infoDic];
                    [self.sources  addObject:model];
                }
                 [self.TableView reloadData];
            }
            else{
                
                [self.TableView.mj_footer endRefreshing];
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                return;
                
            }
        }
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)delCollectGoodsWithGoodsID:(NSString *)goodsid{
    
    NSDictionary *paramDic = @{@"uid":LoveDriverID,
                               @"token":FBH_USER_TOKEN,
                               @"goods_id":goodsid
                               };
    [NetMethod Post:FBHRequestUrl(kUrl_collect_cancel) parameters:paramDic success:^(id responseObject) {
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


#pragma mark - UI

-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}

- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
    //删除
    WS(weakSelf);
    UIContextualAction *deleteRowAction = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleDestructive title:@"delete" handler:^(UIContextualAction * _Nonnull action, __kindof UIView * _Nonnull sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
        
        YzShouCangModel *model = self.sources[indexPath.row];
        [weakSelf delCollectGoodsWithGoodsID:model.goodsID];
        [weakSelf.sources removeObject:model];
        completionHandler (YES);
        [weakSelf.TableView reloadData];
    }];
    deleteRowAction.title = @"删除收藏";
    UISwipeActionsConfiguration *config = [UISwipeActionsConfiguration configurationWithActions:@[deleteRowAction]];
    return config;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources .count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzshoucangCell *cell = [YzshoucangCell cellWithTableView:tableView];
    
    YzShouCangModel *model = self.sources[indexPath.row];
    [cell setOrderModel:model];
    
//    [cell.BuyCar addTarget:self action:@selector(BuyCarBtn) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    YzShouCangModel *model =self.sources[indexPath.row];
    
    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
    goodDetailVC.goodsID = model.goodsID;
    if ([model.silver floatValue] > 0) {
        goodDetailVC.shopType = 1;
    }
    [self.navigationController pushViewController:goodDetailVC animated:YES];
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
}


@end
