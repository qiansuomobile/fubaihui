//
//  YzShouCangModel.m
//  ASJ
//
//  Created by Jack on 16/9/6.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzShouCangModel.h"

@implementation YzShouCangModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.desc = value;
    }
    if ([key isEqualToString:@"id"]){
        self.goodsID = value;
    }
}
@end
