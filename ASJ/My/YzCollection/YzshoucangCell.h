//
//  YzshoucangCell.h
//  ASJ
//
//  Created by Jack on 16/9/6.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YzShouCangModel.h"




@interface YzshoucangCell : UITableViewCell


@property (nonatomic , strong) UIButton *BuyCar;

-(void)setOrderModel:(YzShouCangModel *)model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;


-(void)setUPframe;



@end
