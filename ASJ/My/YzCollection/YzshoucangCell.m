//
//  YzCollectionDetailCell.m
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzshoucangCell.h"




@interface YzshoucangCell ()

@property (nonatomic, weak) UILabel *date;
@property (nonatomic, strong) UILabel *order_sn;
@property (nonatomic, strong) UILabel *state;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, weak) UIImageView *imageV;

//@property (nonatomic , strong) UIButton *BuyCar;

@property (nonatomic , strong) UILabel *MoneyLabel;

@property (nonatomic , strong) UILabel *BusNameLabel;

@end


@implementation YzshoucangCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}



- (void)setUpSubview{
    
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    
//    imageV.backgroundColor = [UIColor whiteColor];
    
    imageV.image = [UIImage imageNamed:@"logo"];
    
    [self addSubview:imageV];
    
    _imageV = imageV;
    
    
    
    
    
    UILabel *date = [[UILabel alloc]init];
    
    date.font = [UIFont systemFontOfSize:13];
    
    date.numberOfLines = 0;
    
//    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    //    date.textColor = [UIColor grayColor];
    
    [self addSubview:date];
    
    _BusNameLabel = date;

    
    UILabel *date3 = [[UILabel alloc]init];
    
    date3.font = [UIFont systemFontOfSize:15];
    
    date3.textColor = [UIColor orangeColor];
    
    //    date3.text = @"888";
    
    [self addSubview:date3];
    
    _MoneyLabel = date3;
    
    
    
    
    
    
    UIButton *BuyCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [BuyCarBtn setTitle:@"查看详情" forState:UIControlStateNormal];
    
    BuyCarBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    //    [BuyCarBtn setTintColor:[UIColor redColor]];
    
    [BuyCarBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    
    
    
    [BuyCarBtn.layer setMasksToBounds:YES];
    [BuyCarBtn.layer setCornerRadius:2.0]; //设置矩形四个圆角半径
    //边框宽度
    [BuyCarBtn.layer setBorderWidth:1.0];
    //设置边框颜色有两种方法：第一种如下:
    //    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //    CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0, 0, 0, 1 });
    //    [BuyCarBtn.layer setBorderColor:colorref];//边框颜色
    //第二种方法如下:
    BuyCarBtn.layer.borderColor=[UIColor redColor].CGColor;
    
    [self addSubview:BuyCarBtn];
    
    _BuyCar = BuyCarBtn;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
    
}

-(void)setUPframe{
    
    self.BuyCar.frame = CGRectMake(YzWidth - 100, 90, 70, 20);
    
    self.imageV.frame = CGRectMake(15, 10, 100, 100);
    
    self.BusNameLabel.frame = CGRectMake(130, 0, YzWidth - 140, 70);
    
    self.MoneyLabel.frame = CGRectMake(130, 90, 100, 20);
}

-(void)setOrderModel:(YzShouCangModel *)model{
    //    goods_price
    
    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",FBHBaseURL,model.img];
    
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    if ([model.silver floatValue] > 0) {
        self.MoneyLabel.text = [NSString stringWithFormat:@"银积分%@",model.silver];
    }else{
        self.MoneyLabel.text = [NSString stringWithFormat:@"¥ %@",model.price];
    }
    
    
    self.BusNameLabel.text = model.title;
    
    [self setUPframe];

}


@end
