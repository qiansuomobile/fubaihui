//
//  YzLoveVipCell.h
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YzUserInfoModel.h"

@interface YzLoveVipCell : UITableViewCell



-(void)setLoveVipModel:(YzUserInfoModel *)model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;


@end
