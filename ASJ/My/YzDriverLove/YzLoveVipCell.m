//
//  YzLoveVipCell.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzLoveVipCell.h"


@interface YzLoveVipCell ()

@property (nonatomic , strong) UIImageView *SilverLabel;

@property (nonatomic , strong) UILabel *SumLabel;

@property (nonatomic , strong) UILabel *LookLabel;


@end

@implementation YzLoveVipCell



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
//        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}



- (void)setUpSubview{
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    
    [imageV.layer setMasksToBounds:YES];

    [imageV.layer setCornerRadius:4.0];
    
    [self addSubview:imageV];
    
    _SilverLabel = imageV;
    
    
    
    UILabel *date2 = [[UILabel alloc]init];
    
    date2.font = [UIFont systemFontOfSize:12];
    
    date2.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:date2];
    
    _SumLabel = date2;
    
    
    
    UILabel *therr = [[UILabel alloc]init];
    
    therr.font = [UIFont systemFontOfSize:12];
    
    therr.textAlignment = NSTextAlignmentCenter;
    
    
    
    [therr.layer setMasksToBounds:YES];
    
    [therr.layer setCornerRadius:10.0];

    
    [self addSubview:therr];
    
    _LookLabel = therr;

    
    
    
    
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"VipCell";
    
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell){
        
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    
    return cell;
    
}

-(void)SetChatFrame{
    
    
    _SilverLabel.frame = CGRectMake(15, 5, 45, 45);
    
    _SumLabel.frame = CGRectMake(75, 5, 200, 20);
    
    _LookLabel.frame = CGRectMake(YzWidth - 100, 15, 90, 20);
}


-(void)setLoveVipModel:(YzUserInfoModel *)model;
{
    
    _SumLabel.text = model.nickname;
    
    NSString *ImgPath = [NSString stringWithFormat:@"%@%@",FBHBaseURL,model.headpic];
//    
//    _ageLabel.text = [NSString stringWithFormat:@"%@岁",model.age];
//    
//    _aeraLabel.text = @"";
//    
//    
//    if ([model.sex isEqualToString:@"1"] && [model.is_attention isEqualToString:@"1"]) {
//        
//        _LookLabel.textColor = [UIColor whiteColor];
//
//        _LookLabel.text = @"点击搜索";
//
//        _LookLabel.backgroundColor = [UIColor blueColor];
//
//        
//    }else if ([model.sex isEqualToString:@"2"] && [model.is_attention isEqualToString:@"1"]){
//        
//        _LookLabel.textColor = RGBColor(240, 153, 173);
//        
//        _LookLabel.text = @"点击搜索";
//
//        
//        _LookLabel.backgroundColor = RGBColor(255, 223, 230);
//   
//    }else {
//        _LookLabel.textColor = RGBColor(240, 153, 173);
//        
//        _LookLabel.text = @"点击搜索";
//        
//        _LookLabel.backgroundColor = RGBColor(255, 223, 230);
//
////        _LookLabel.hidden = YES;
//    }
    _LookLabel.textColor = RGBColor(240, 153, 173);
    _LookLabel.backgroundColor = RGBColor(255, 223, 230);

    _LookLabel.text = model.username;
    [_SilverLabel sd_setImageWithURL:[NSURL URLWithString:ImgPath] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
    
    
    [self SetChatFrame];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
