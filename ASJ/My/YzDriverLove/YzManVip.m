//
//  YzManVip.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzLoveVipCell.h"
#import "YzWomenVip.h"
#import "YzLoveVipModel.h"

#import "YzManVip.h"

@interface YzManVip ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end




static NSInteger Pages;


@implementation YzManVip



-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
    }
    
    return _sources;

}


- (void)viewDidLoad {
    
    [super viewDidLoad];
     Pages= 1;
    
    
//    self.view.backgroundColor = [UIColor greenColor];

    [self loadUI];
    
    
    [self LoadManVipRequst];
    
    
    [self setRefreshing];
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self LoadManVipRequst];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self NewloadMoreData];
        
        [self.TableView.mj_footer endRefreshing];
    }];
}


-(void)NewloadMoreData{


    NSString *pp =[NSString stringWithFormat:@"%ld",(long)Pages];
    
    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"sex":@"1",@"p":pp};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        NSDictionary *Info = responseObject;
        
        
//        NSLog(@"男会员  %@",Info);
        
        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"list"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            NSMutableArray *manArray = [NSMutableArray array];
            
            for (NSDictionary *UserInfoDic in Info[@"list"]) {
                
                model = [YzLoveVipModel objectWithKeyValues:UserInfoDic];
                
                [manArray addObject:model];
            }
            
            
            [self.sources addObjectsFromArray:manArray];
            
        }
        
        [self.TableView reloadData];
        
        
        
    } failure:^(NSError *error) {
        
    }];

    
}


-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64-35)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}




-(void)LoadManVipRequst{
    
    
    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"sex":@"1" ,@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        
        
        NSDictionary *Info = responseObject;
        
        
//        NSLog(@"男会员  %@",Info);

        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"list"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            [self.sources removeAllObjects];
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            for (NSDictionary *UserInfoDic in Info[@"list"]) {
                
                model = [YzLoveVipModel objectWithKeyValues:UserInfoDic];
                
                [self.sources addObject:model];
            }
            
            
        }
        
        [self.TableView reloadData];

        
        
    } failure:^(NSError *error) {
        
    }];
    
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sources.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YzLoveVipCell *cell = [YzLoveVipCell cellWithTableView:tableView];
    
    
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    model = self.sources[indexPath.row];
    
    [cell setLoveVipModel:model];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    model = self.sources[indexPath.row];
    
    
    YzLookPersonVC *vc = [[YzLookPersonVC alloc]init];
    
    vc.PerSonID = model.uid;
    
    [self.navigationController pushViewController:vc animated:YES];    
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 55;
}

@end
