//
//  YzSeachLove.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzseachLoveListVC.h"


#import "YzSeachLove.h"

#import "YzWomenVip.h"

@interface YzSeachLove ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIActionSheetDelegate>

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic , strong) NSArray *TitleArray;

@property (nonatomic , copy) NSString *SexType;

@property (nonatomic , copy) NSString *sexID;

@property (nonatomic , strong) NSArray *ageArray;

@property (nonatomic , copy) NSString *age;

@property (nonatomic , copy) NSString *ageType;

@property (nonatomic , copy) NSString *heardType;

@property (nonatomic , copy) NSString *heard;


@property (nonatomic , strong) UITextField *serchTextfield;

@end

@implementation YzSeachLove

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //    self.view.backgroundColor = [UIColor orangeColor];
    self.title = @"搜索会员";
    [self CreatSerchUI];
    
    
    self.ageArray = @[@"18~22",@"23~25",@"26~28",@"29~35",@"35以上"];
    
    //    1（18~22）；2（23~25）；3（26~28）；4（29~35）；5（35以上）
}


-(void)CreatSerchUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight-44)];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    self.TableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    //    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    //
    [self.view addSubview:self.TableView];
    
    
    _TitleArray = @[];
    
    
    UIView *heard= [[UIView alloc]init];
    
    heard.frame = CGRectMake(0, 0, YzWidth, 80);
    
    self.TableView.tableHeaderView = heard;
    
    
    UITextField *serchTextField = [[UITextField alloc]init];
    
    serchTextField.delegate = self;
    
    serchTextField.backgroundColor = [UIColor whiteColor];
    
    serchTextField.frame = CGRectMake(0, 20, YzWidth,40);
    
    serchTextField.placeholder = @"  昵称／手机号";
    
    //    serchTextField
    
    UIImageView *laftview = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuobiao"]];
    
    //    laftview.image = [UIImage imageNamed:@"logo"];
    
    laftview.frame = CGRectMake(25, 0, 15, 15);
    
    serchTextField.leftView= laftview;
    
    serchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    //    serchTextField.leftViewMode =laftview;
    
    [heard addSubview:serchTextField];
    
    self.serchTextfield = serchTextField;
    
    
    
    UIView *footer = [[UIView alloc]init];
    footer.frame = CGRectMake(0, 0, YzWidth, 100);
    self.TableView.tableFooterView = footer;
    
    UIButton *serchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    serchBtn.layer.cornerRadius = 4;
    
    //    serchBtn.tintColor = RGBColor(255, 64, 32);
    
    [serchBtn setBackgroundColor: RGBColor(255, 64, 32)];
    
    [serchBtn setTitle:@"查找" forState:UIControlStateNormal];
    
    //    serchBtn.titleLabel.text = @"查找";
    
    serchBtn.frame = CGRectMake(20, 30, YzWidth-40, 40);
    
    [serchBtn addTarget:self  action:@selector(serchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    [footer addSubview:serchBtn];
    
    
}

-(void)serchBtn{
    
    YzseachLoveListVC *vc = [[YzseachLoveListVC alloc]init];
    
    if (!self.sexID) {
        self.sexID = @"";
    }
    
    
    if (!self.age) {
        self.age = @"";
    }
    
    
    if (!self.heard) {
        self.heard = @"";
    }
    
    vc.title = @"查找会员";
    
    vc.SexType = self.sexID;
    
    vc.Age = self.age;
    
    vc.Heard = self.heard;
    
    vc.serchName = self.serchTextfield.text;
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _TitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"SerchCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    
    cell.textLabel.text = _TitleArray[indexPath.row];
    
    switch (indexPath.row) {
        case 0:
        {
            if (self.SexType) {
                
                cell.detailTextLabel.text =self.SexType;
                
            }
            
        }
            break;
        case 1:
        {
            if (self.ageType) {
                
                cell.detailTextLabel.text =self.ageType;
                
            }
        }
            break;
        case 2:
        {
            
        }
            break;
        case 3:
        {
            
            if (self.heardType) {
                cell.detailTextLabel.text =self.heardType;
                
            }
            
        }
            break;
        default:
            break;
    }
    
    
    
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    switch (indexPath.row) {
        case 0:
        {
            [self choosePersonType];
        }
            break;
        case 1:
        {
            [self chooseAgeType];
        }
            break;
            //        case 2:
            //        {
            //
            //        }
            //            break;
            //
            //        case 3:
            //        {
            //            [self chooseHeardType];
            //        }
            break;
        default:
            break;
    }
    
}


-(void)choosePersonType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男",@"女", nil];
    
    [sheet.layer setCornerRadius:5];
    
    sheet.tag = 100;
    
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}


-(void)chooseAgeType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"18-22",@"23-25",@"26-28",@"29-35",@"35以上", nil];
    
    [sheet.layer setCornerRadius:5];
    
    sheet.tag = 200;
    
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}


-(void)chooseHeardType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"有",@"无", nil];
    
    [sheet.layer setCornerRadius:5];
    
    sheet.tag = 300;
    
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}


// 性别、婚姻
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (actionSheet.tag) {
            
        case 100:
            
        {
            switch (buttonIndex) {
                    
                case 0:
                    
                    //            男
                {
                    self.SexType = @"男";
                    
                    self.sexID = @"1";
                }
                    break;
                    
                case 1:
                    
                    //            女
                {
                    self.SexType = @"女";
                    
                    self.sexID = @"2";
                }
                    
                    break;
                    
                default:
                    
                    break;
            }
            
            
        }
            break;
            
        case 200:
        {
            switch (buttonIndex) {
                    
                case 0:
                {
                    
                    self.ageType = @"18-22";
                    
                    self.age =@"1";
                }
                    
                    break;
                    
                case 1:
                    
                {
                    self.ageType = @"23-25";
                    
                    self.age =@"2";
                }
                    
                    break;
                    
                case 2:
                {
                    self.ageType = @"26-28";
                    
                    self.age =@"3";
                }
                    break;
                case 3:
                {
                    self.ageType = @"28-35";
                    
                    self.age =@"4";
                }
                    break;
                case 4:
                {
                    self.ageType = @"35以上";
                    
                    self.age =@"5";
                }
                    break;
                default:
                    
                    break;
            }
            
        }
            break;
            
            
        case 300:
            
            switch (buttonIndex) {
                case 0:
                {
                    self.heardType = @"有";
                    
                    self.heard = @"1";
                }
                    break;
                case 1:
                {
                    self.heardType = @"无";
                    
                    self.heard = @"2";
                }
                    break;
                default:
                    break;
            }
            
            
            break;
            
            
        default:
            break;
            
            
            
            
            
    }
    
    
    [self.TableView reloadData];
}

@end
