//
//  YzWomenVip.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzLoveVipCell.h"
#import "YzWomenVip.h"
#import "YzLoveVipModel.h"
@interface YzWomenVip ()<UITableViewDelegate,UITableViewDataSource>




@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;


@end



static NSInteger Pages;


@implementation YzWomenVip


-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    
    return _sources;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor redColor];
 Pages= 1;
    
    [self loadUI];
    
    
    if (![self.title isEqualToString:@"查找会员"]) {
        
        [self LoadWomenVipRequst];

    }else{
        
        [self GetSerchPerson:self.SexType Age:self.Age Heard:self.Heard];
        
    }
    
    [self setRefreshing];
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self LoadWomenVipRequst];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self NewloadMoreData];
        
        [self.TableView.mj_footer endRefreshing];
    }];
}


-(void)NewloadMoreData{

    NSString *pp = [NSString stringWithFormat:@"%ld",(long)Pages];

    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"sex":@"2",@"p":pp};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        NSDictionary *Info = responseObject;
        
        NSLog(@"女会员  %@",Info);
        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"list"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            NSArray *ListArray = Info[@"list"];
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            NSMutableArray *womenArray = [NSMutableArray array];
           
            for (NSDictionary *UserInfoDic in ListArray) {
                
                model = [YzLoveVipModel objectWithKeyValues:UserInfoDic];
                
                [womenArray addObject:model];
                
            }
            
            [self.sources addObjectsFromArray:womenArray];
            
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    

    
    
}


-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight -64 -35)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}

-(void)GetSerchPerson:(NSString *)SexType Age:(NSString *)Age Heard:(NSString *)Heard{
    
    NSDictionary *InfoDic;
    
//    搜索昵称条件  是否为空
    
    if ([VerifyPictureURL isBlankString:self.serchName]) {
        
        InfoDic = @{@"uid":LoveDriverID,@"sex":SexType ,@"age":Age ,@"headpic":Heard};
        
    }else if (![VerifyPictureURL isBlankString:self.serchName]){
        
        InfoDic = @{@"uid":LoveDriverID,@"nickname":self.serchName};

    }else{
        
        InfoDic = @{@"uid":LoveDriverID,@"sex":SexType ,@"age":Age ,@"headpic":Heard,@"nickname":self.serchName};
        
    }
    
    
    
    

    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        NSDictionary *Info = responseObject;
        
//        NSLog(@"女会员  %@",Info);
        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"list"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            NSArray *ListArray = Info[@"list"];
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            for (NSDictionary *UserInfoDic in ListArray) {
                
                model = [YzLoveVipModel objectWithKeyValues:UserInfoDic];
                
                [self.sources addObject:model];
                
            }
            
            
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];

    
    
    
    
}

-(void)LoadWomenVipRequst{
    
    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"sex":@"2",@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        NSDictionary *Info = responseObject;
        
        NSLog(@"女会员  %@",Info);
        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"list"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            [self.sources removeAllObjects];
            
            NSArray *ListArray = Info[@"list"];
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            for (NSDictionary *UserInfoDic in ListArray) {
                
                model = [YzLoveVipModel objectWithKeyValues:UserInfoDic];
                
                [self.sources addObject:model];

            }
            

        }
        
        [self.TableView reloadData];

    } failure:^(NSError *error) {
        
    }];

}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.sources.count;

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YzLoveVipCell *cell = [YzLoveVipCell cellWithTableView:tableView];
    
    
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    model = self.sources[indexPath.row];
    
    [cell setLoveVipModel:model];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    model = self.sources[indexPath.row];
    
    
    YzLookPersonVC *vc = [[YzLookPersonVC alloc]init];
    
    vc.PerSonID = model.uid;
    
    [self.navigationController pushViewController:vc animated:YES];    
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 55;
}

@end
