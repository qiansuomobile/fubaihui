//
//  YzYuanfenCell.h
//  ASJ
//
//  Created by Jack on 16/9/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YzLoveVipModel.h"


@interface YzYuanfenCell : UICollectionViewCell

@property (nonatomic , strong) UIButton *nextBtn;

@property (nonatomic , strong) UIButton *LikePersonBtn;

@property (nonatomic , strong) YzLoveVipModel *model;


+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
