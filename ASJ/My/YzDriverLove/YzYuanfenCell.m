//
//  YzYuanfenCell.m
//  ASJ
//
//  Created by Jack on 16/9/23.
//  Copyright © 2016年 TS. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "YzYuanfenCell.h"

@interface YzYuanfenCell()

@property (nonatomic, weak) UIImageView *imageView;

//@property (nonatomic, weak) UIButton *shareButton;

@property (nonatomic, weak) UIButton *startButton;

@property (nonatomic, weak) UIImageView *Backimage;

@property (nonatomic, strong) UILabel *NicknameLabel;

@property (nonatomic , strong) UILabel *InfoLabel;


@end

static NSString * const reuseIdentifier = @"YZCollectionCell";

@implementation YzYuanfenCell



- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self setUpSubview];
        
    }
    return self;
}

- (void)setUpSubview{
    
    
    self.backgroundColor = RGBColor(237, 237, 237);
    
    UIImageView *imageV = [[UIImageView alloc] init];
    
    imageV.layer.masksToBounds = YES;
    
    imageV.layer.cornerRadius = 10;
    
    // 注意:一定要加载contentView
    [self.contentView addSubview:imageV];
    
    _imageView = imageV;
    
    
    
    UIView *HavfView = [UIView new];
    
    [self.contentView addSubview:HavfView];
    
    
    
    UILabel *NickNameLabel = [[UILabel alloc]init];
    
    [self.contentView addSubview:NickNameLabel];
    
    NickNameLabel.font = [UIFont systemFontOfSize:15];
    
    _NicknameLabel = NickNameLabel;
    
    
    
    UILabel *infolabel = [[UILabel alloc]init];
    
    [self.contentView addSubview:infolabel];
    
    infolabel.font = [UIFont systemFontOfSize:15];
    
    _InfoLabel = infolabel;
    
    
    
    
    UIButton *LikeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [VerifyPictureURL setUpBtn:LikeBtn font:[UIFont systemFontOfSize:15] title:@"点击搜索" color:nil tag:200];
    
    LikeBtn.backgroundColor= [UIColor redColor];
    
    [LikeBtn.layer setMasksToBounds:YES];
    
    [LikeBtn.layer setCornerRadius:15.0];
    
    LikeBtn.alpha = 0.7;
    
        [self.contentView addSubview:LikeBtn];
    
    //    _LikePersonBtn = LikeBtn;
    
    
    
    
    
    UIButton *NextBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [VerifyPictureURL setUpBtn:NextBtn font:[UIFont systemFontOfSize:16] title:@"下一位" color:nil tag:200];
    
    NextBtn.backgroundColor= [UIColor orangeColor];
    
    [NextBtn.layer setMasksToBounds:YES];
    
    [NextBtn.layer setCornerRadius:15.0];
    
    NextBtn.alpha = 0.7;
    
    [self.contentView addSubview:NextBtn];
    
    _nextBtn = NextBtn;
    
    
    
}

+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:reuseIdentifier];
    
    id cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    return cell;
}



-(void)setModel:(YzLoveVipModel *)model{
    
    NSString *path = [NSString stringWithFormat:@"http:www.woaisiji.com%@",model.headpic];
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"logo"]];
    
//    _NicknameLabel.text = [NSString stringWithFormat:@"%@  %@岁",model.nickname,model.age];
//    
//    _InfoLabel.text = [NSString stringWithFormat:@"身高: %@cm  %@",model.height,model.constellation];
    
    [self SetupFrame];
    
}

-(void)SetupFrame{
    
    _imageView.frame = CGRectMake(15, 15, YzWidth -30, YzWidth -30);
    
    _NicknameLabel.frame = CGRectMake(40, YzHeight - 150 -60 -35 , 160, 20);
    
    _InfoLabel.frame = CGRectMake(40, YzHeight - 110 -60 -35, 200, 25);
    
    //    _LikePersonBtn.frame = CGRectMake(YzWidth/2 -120,YzHeight - 110 -50,240,30);
    
    _nextBtn.frame = CGRectMake(YzWidth - 100 , YzHeight - 110 - 60 - 40, 70, 30);
    
}

@end
