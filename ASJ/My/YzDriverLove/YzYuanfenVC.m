//
//  YzYuanfenVC.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzLookPersonVC.h"

#import "YzLoveVipModel.h"

#import "YzYuanfenCell.h"

#import "YzYuanfenVC.h"

@interface YzYuanfenVC (){
    
    NSIndexPath *_indexPath;
    
    NSIndexPath *_currentIndexPath;
    
}

@property (nonatomic , strong) NSMutableArray *sources;

@end



@implementation YzYuanfenVC

static NSInteger Pages;

static NSString *ID = @"YZCollectionCell";

- (instancetype)init
{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    
    
    // 设置cell的尺寸
    //    layout.itemSize = [UIScreen mainScreen].bounds.size;
    
    layout.itemSize = CGSizeMake(YzWidth, YzHeight - 99);
    
    
    
    // 清空行距
    layout.minimumLineSpacing = 0;
    
    // 设置滚动的方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    
    return [super initWithCollectionViewLayout:layout];
    
}

-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    return _sources;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages= 1;
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    
    
    [self setRefreshing];
    
    
    
    [self NewRequsturlData];
    
    // 注册cell,默认就会创建这个类型的cell
    
    [self.collectionView registerClass:[YzYuanfenCell class] forCellWithReuseIdentifier:ID];
    
    //    [self.collectionView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"引导页-1.png"]]];
    
    // 分页
    self.collectionView.pagingEnabled = YES;
    
    //    self.collectionView.bounces = NO;
    
    //    self.collectionView.showsVerticalScrollIndicator = NO;
    
    
    //    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:50] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
    
}


//刷新历史订单
- (void)setRefreshing{
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self NewRequsturlData];
        
        [self.collectionView.mj_header endRefreshing];
        
    }];
    
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self NewloadMoreData];
        
        [self.collectionView.mj_footer endRefreshing];
        
    }];
    
}

-(void)NewRequsturlData{
    
    //   缘分
    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"is_sticky":@"1",@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        
        NSDictionary *dic = responseObject;
        
        //        NSLog(@"缘分*****😄😄😄😄😄😄   %@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                //                [self.collectionView.mj_header endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            [self.sources removeAllObjects];
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            
            for (NSDictionary *userInfoDic in info) {
                
                model = [YzLoveVipModel objectWithKeyValues:userInfoDic];
                
                [self.sources addObject:model];
                
            }
            
            [self.collectionView reloadData];
            
            
            //            [self.collectionView.mj_header endRefreshing];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)NewloadMoreData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"is_sticky":@"1",@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/index") parameters:InfoDic success:^(id responseObject) {
        
        
        NSDictionary *dic = responseObject;
        
        //        NSLog(@"缘分*****😄😄😄😄😄😄   %@",dic);
        
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                //                [self.collectionView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
            }
            
            YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
            
            NSMutableArray *UserInfoArray = [NSMutableArray array];
            
            for (NSDictionary *userInfoDic in info) {
                
                
                model = [YzLoveVipModel objectWithKeyValues:userInfoDic];
                
                
                [UserInfoArray addObject:model];
                
            }
            
            [self.sources addObjectsFromArray:UserInfoArray];
        }
        
        [self.collectionView reloadData];
        
        //        [self.collectionView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    return _sources.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    _currentIndexPath = indexPath;
    
    
    // dequeueReusableCellWithReuseIdentifier
    // 1.首先从缓存池里取cell
    // 2.看下当前是否有注册Cell,如果注册了cell，就会帮你创建cell
    // 3.没有注册，报错
    
    YzYuanfenCell *cell = [YzYuanfenCell collectionWithCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    
    model = self.sources[indexPath.row];
    // 拼接图片名称 3.5 320 480
    //    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    //    NSString *imageName = [NSString stringWithFormat:@"leader_%ld",indexPath.row+1];
    //    if (screenH > 480) { // 5 , 6 , 6 plus
    //        imageName = [NSString stringWithFormat:@"new_feature_%ld-568h",indexPath.row + 1];
    //    }
    //    cell.image = [UIImage imageNamed:imageName];
    
    
    //    [cell setIndexPath:indexPath count:4];
    
    [cell.nextBtn addTarget:self action:@selector(nextPage) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.model = model;
    //    leader_
    return cell;
    
}


-(void)nextPage
{
    //         NSLog(@"%@",[self.collectionView indexPathsForVisibleItems]);
    
    NSIndexPath *currentIndexPath=[[self.collectionView indexPathsForVisibleItems]lastObject];
    
    
    NSLog(@"当前位置  %@",currentIndexPath);
    
    //2）计算出下一个需要展示的位置
    
    NSInteger nextItem = currentIndexPath.item+1;
    
    NSInteger nextSection=currentIndexPath.section;
    
    
    NSLog(@" 第几个===   %ld",(long)nextItem);
    
    if (nextItem ==_sources.count) {
        
        nextItem ++;
        
        Pages ++;
        
        [self NewloadMoreData];
        
        //                 nextSection++;
        
    }
    
    NSIndexPath *nextIndexPath=[NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    if (nextIndexPath.item >_sources.count) {
        
        
        //        [self NewloadMoreData];
        
    }else{
        
        if (!nextIndexPath) {
            
            
            //        [self NewloadMoreData];
            
        }else{
            
            //3）通过动画滚动到下一个位置
            
            [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
        }
        //    NSIndexPath *currentIndexPath=[[self.collectinView indexPathsForVisibleItems]lastObject];
    }
}
-(void)NextDriverLove{
    
    
    //    NSInteger path = [self.collectionView  indexPathForCell:YzYuanfenCell];
    
    
    //    _currentIndexPath.row++;
    //1获取当前正在展示的位置
    
    //    NSIndexPath *currentIndexPath=[[self.collectionView indexPathsForVisibleItems] firstObject];
    
    // 2计算出下一个需要展示的位置
    
    
    NSInteger nextItem = _currentIndexPath.item;
    
    NSInteger nextSection = _currentIndexPath.section;
    
    //    if ( nextItem == self.sources.count ) {
    
    nextItem ++;
    
    //        nextSection = 0;
    
    //    }
    
    NSIndexPath *nextIndexPath=[NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    
    //    NSLog(@"");
    NSLog(@"滚动位置    %@",[self.collectionView indexPathsForVisibleItems]);
    
    
    [self.collectionView scrollToItemAtIndexPath:_currentIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    // 将collectionView在控制器view的中心点转化成collectionView上的坐标
//    CGPoint pInView = [self.view convertPoint:self.collectionView.center toView:self.collectionView];
//    // 获取这一点的indexPath
//    NSIndexPath *indexPathNow = [self.collectionView indexPathForItemAtPoint:pInView];
//    // 赋值给记录当前坐标的变量
//    _currentIndexPath = indexPathNow;
//    // 更新底部的数据
//    // ...
//}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
    
    model = self.sources[indexPath.row];
    
    YzLookPersonVC *vc = [[YzLookPersonVC alloc]init];
    
    vc.PerSonID = model.uid;
    
    vc.headpic = model.headpic;
    
    [self.navigationController pushViewController:vc animated:YES];
    //    点击下一位
    
}


@end
