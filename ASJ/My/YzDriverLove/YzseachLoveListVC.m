//
//  YzseachLoveListVC.m
//  ASJ
//
//  Created by Jack on 16/9/28.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzLoveVipCell.h"

#import "YzLoveVipModel.h"

#import "YzseachLoveListVC.h"
#import "YzUserInfoModel.h"
#import "ChatViewController.h"

@interface YzseachLoveListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end

@implementation YzseachLoveListVC


-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    
    return _sources;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self loadUI];
    
    // Do any additional setup after loading the view.
    
    
    [self GetSerchPerson:self.SexType Age:self.Age Heard:self.Heard];
    


//    [self setRefreshing];

}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
//        Pages= 1;
        
//        [self LoadWomenVipRequst];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
//    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        
//        Pages ++;
//        
//        [self NewloadMoreData];
//        
//        [self.TableView.mj_footer endRefreshing];
//    }];
}


-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight -64)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}

-(void)GetSerchPerson:(NSString *)SexType Age:(NSString *)Age Heard:(NSString *)Heard{
    
    NSDictionary *InfoDic = [NSDictionary dictionary];
    
    NSString *headpic = @"";

    if (self.serchName.length) {
        
        
        
    }else{
  
        headpic = @"1";
    
    }
    
    InfoDic = @{@"uid":LoveDriverID,
                @"token":FBH_USER_TOKEN,
                @"page":@"1",
                @"row_num":@"15",
                @"keyword":self.serchName};
//    InfoDic = @{@"uid":LoveDriverID,@"headpic":headpic,@"nickname":self.serchName};
    [NetMethod Post:FBHRequestUrl(kUrl_friend_search) parameters:InfoDic success:^(id responseObject) {
        
        NSDictionary *Info = responseObject;
        
        if ([Info[@"code"] isEqual:@200]){
            
            if ([Info[@"data"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                return ;
            }
            
            NSArray *ListArray = Info[@"data"];
            if (ListArray.count == 0) {
                [MBProgressHUD showSuccess:@"该号未注册" toView:self.view];
                return;
            }
            for (NSDictionary *UserInfoDic in ListArray) {
                YzUserInfoModel *model = [[YzUserInfoModel alloc]init];
                [model setValuesForKeysWithDictionary:UserInfoDic];
                [self.sources addObject:model];
            }
        }else{
            [MBProgressHUD showError:Info[@"msg"] toView:self.view];
        }
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.sources.count;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzLoveVipCell *cell = [YzLoveVipCell cellWithTableView:tableView];
    
    YzUserInfoModel *model = self.sources[indexPath.row];
    
    [cell setLoveVipModel:model];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    
//    YzLoveVipModel *model = [[YzLoveVipModel alloc]init];
//
//    model = self.sources[indexPath.row];
//
//    YzLookPersonVC *vc = [[YzLookPersonVC alloc]init];
//
//    vc.PerSonID = model.uid;
//
//    [self.navigationController pushViewController:vc animated:YES];
    
    
    
    YzUserInfoModel *model = [[YzUserInfoModel alloc]init];
    model = [self.sources objectAtIndex:indexPath.row];
//    [self.searchController.searchBar endEditing:YES];
    ChatViewController *chatController = [[ChatViewController alloc] initWithConversationChatter:model.uid conversationType:EMConversationTypeChat withInforModel:model];
    chatController.title = model.nickname.length > 0 ? model.nickname : model.uid;
    [self.navigationController pushViewController:chatController animated:YES];

    
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 55;
}



@end
