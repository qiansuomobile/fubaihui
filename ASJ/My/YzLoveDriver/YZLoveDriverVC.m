//
//  YZLoveDriverVC.m
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YZLoveDriverVC.h"
#import "YzConstactionVC.h"
#import "YzDriverBaterVC.h"
#import "YzTraverlingVC.h"
#import "YzCommentVC.h"

@interface YZLoveDriverVC ()

@end

@implementation YZLoveDriverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"搜索会员";
    
    
    [self CreatUI];
}



-(void)CreatUI{
//    YzNewFriend001
    
//    IMG_0210
    NSArray *imgName = @[@"IMG_0210.JPG",@"sj_icon02",@"YzNewsDriver001",@"sj_icon05"];
    
    for (int i=0; i<3; i ++) {
        
        
        
        UIImageView *btnImg = [[UIImageView alloc]init];
        
        btnImg.frame=CGRectMake(12+(i%2)*(YzWidth/2 -5) ,10+(i/2) *(YzWidth/2+90),YzWidth/2-20, YzWidth/2-20);
        
        btnImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imgName[i]]];
        
        btnImg.layer.cornerRadius = 4;
        
        [self.view addSubview:btnImg];
        
        
        
        UIButton *DriverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        DriverBtn.frame = CGRectMake(10+(i%2)*YzWidth/2, 10+(i/2) *(YzWidth/2 +90), YzWidth/2-20, YzWidth/2-20);
        
        DriverBtn.tag = 100+i;
        
        DriverBtn.backgroundColor = [UIColor greenColor];
        
        DriverBtn.layer.cornerRadius = 4;
        
        [self.view addSubview:DriverBtn];
        
        DriverBtn.backgroundColor = [UIColor clearColor];
        
        [DriverBtn addTarget:self action:@selector(LoveDriver:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    UIButton *driver = [UIButton buttonWithType:UIButtonTypeCustom];
    
    driver.frame = CGRectMake(10, YzWidth/2-3 , YzWidth - 20, 90+6);
    
    driver.tag = 104;
    
    driver.backgroundColor = [UIColor greenColor];
    
    driver.layer.cornerRadius = 4;
    
    [self.view addSubview:driver];
    
    [driver addTarget:self action:@selector(LoveDriver:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIImageView *btnImg = [[UIImageView alloc]init];
    
    btnImg.frame =  driver.frame;
    
    btnImg.image = [UIImage imageNamed:@"sj_icon03"];
    
    btnImg.layer.cornerRadius = 4;
    
    [self.view addSubview:btnImg];
    
    
}

-(void)LoveDriver:(UIButton *)sender{
    
    
    
    switch (sender.tag) {
        case 100:
        {
            //            司机爱情
            
            if (DriverISLogIN) {
                
                YzCommentVC *vc = [[YzCommentVC alloc]init];
                
                [self.navigationController pushViewController:vc animated:YES];
                
            }else{
                
                LoginViewController *vc = [[LoginViewController alloc]init];
                
                YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
                
                
                
                [self presentViewController:NaVC animated:YES completion:nil];
                
            }
            
        }
            break;
        case 101:
        {
            YzDriverBaterVC *vc= [[YzDriverBaterVC alloc]init];
            
            vc.title = @"健康";
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        case 102:
        {
            //            司机咨询
            
            YzConstactionVC *vc= [[YzConstactionVC alloc]init];
            
            vc.TITLE =@"咨询";
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        case 103:
        {
            //        app商城
            
            
//            [MBProgressHUD showSuccess:@"暂未开通,敬请期待" toView:self.view];
            
        }
            break;
            
        case 104:
        {
            
            //            自驾游
            
            YzTraverlingVC *vc = [[YzTraverlingVC alloc]init];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        default:
            break;
    }
    
    
}



@end
