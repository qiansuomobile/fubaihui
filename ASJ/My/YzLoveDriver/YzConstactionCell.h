//
//  YzConstactionCell.h
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YzDriverNewsModel.h"

@interface YzConstactionCell : UITableViewCell


+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic , strong)YzDriverNewsModel *Model;

-(void)setUPframe;

-(void)SetDriverModel:(YzDriverNewsModel *)model;

-(void)SetJiankangModel:(YzDriverNewsModel *)model;


@end
