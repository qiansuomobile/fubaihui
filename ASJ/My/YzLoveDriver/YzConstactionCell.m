//
//  YzConstactionCell.m
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzConstactionCell.h"



@interface YzConstactionCell ()



@property (nonatomic, strong) UILabel *NewsTimeLabel;

@property (nonatomic, strong) NSMutableArray *imageArr;

@property (nonatomic, weak) UIImageView *NewsImg;

//@property (nonatomic , strong) UIButton *BuyCar;

@property (nonatomic , strong) UILabel *LikePersonLabel;

@property (nonatomic , strong) UILabel *NewsTitleLabel;

@property (nonatomic , strong) UILabel *LookPersonLabel;

@property (nonatomic , strong) UIImageView *headpicImg;

@property (nonatomic , strong) UILabel *nicknameLabel;

@property (nonatomic , strong) UIImageView *likePersonImg;

@property (nonatomic , strong) UIImageView *lookPersonImg;




@end



@implementation YzConstactionCell



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}



- (void)setUpSubview{
    
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    
//    imageV.backgroundColor = [UIColor whiteColor];
    
//    imageV.image = [UIImage imageNamed:@"sj_icon01"];
    
    [self addSubview:imageV];
    
    _NewsImg = imageV;
    
    
   
    
    UIImageView *imageV1 = [[UIImageView alloc]init];
    
//    imageV1.backgroundColor = [UIColor whiteColor];
    
    imageV1.layer.masksToBounds = YES;
    
    imageV1.layer.cornerRadius = 10;
    
//    imageV1.image = [UIImage imageNamed:@"sj_icon01"];
    
    [self addSubview:imageV1];
    
    _headpicImg = imageV1;

    
    
    
    
    UIImageView *imageV2 = [[UIImageView alloc]init];
    
//    imageV2.backgroundColor = [UIColor whiteColor];
    
    imageV2.image = [UIImage imageNamed:@"likeperson-1"];
    
    [self addSubview:imageV2];
    
    _likePersonImg = imageV2;

    
    
    
    
    UIImageView *imageV3 = [[UIImageView alloc]init];
    
//    imageV3.backgroundColor = [UIColor whiteColor];
    
    imageV3.image = [UIImage imageNamed:@"lookperson-1"];
    
    [self addSubview:imageV3];
    
    _lookPersonImg = imageV3;

    
    
    
    
    UILabel *date = [[UILabel alloc]init];
    
    date.font = [UIFont systemFontOfSize:15];
    
    date.numberOfLines = 0;
    
//    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    date.textColor = [UIColor blackColor];
    
    [self addSubview:date];
    
    _NewsTitleLabel = date;
    
    
    
    
    
    UILabel *date2 = [[UILabel alloc]init];
    
    date2.font = [UIFont systemFontOfSize:12];
    
    date2.textColor = [UIColor grayColor];
    
//    date2.text = @"2016-07-24 16:24";
    
//    date2.backgroundColor = [UIColor redColor];
    
//    [date2.layer setMasksToBounds:YES];
//    
//    [date2.layer setCornerRadius:2.0];
    
    date2.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:date2];
    
    _NewsTimeLabel = date2;
    
    
    
    
    
    
    UILabel *NickName = [[UILabel alloc]init];
    
    NickName.font = [UIFont systemFontOfSize:12];
    
    NickName.textColor = [UIColor grayColor];
    
    //    date2.text = @"2016-07-24 16:24";
    
    //    date2.backgroundColor = [UIColor redColor];
    
    //    [date2.layer setMasksToBounds:YES];
    //
    //    [date2.layer setCornerRadius:2.0];
    
    NickName.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:NickName];
    
    _nicknameLabel = NickName;
    
    
    
    
    
    
    UILabel *date3 = [[UILabel alloc]init];
    
    date3.font = [UIFont systemFontOfSize:12];
    
    date3.textColor = [UIColor grayColor];
    
    date3.text = @"888";
    
    [self addSubview:date3];
    
    _LookPersonLabel = date3;
    
    
    
    
    UILabel *data4 = [[UILabel alloc]init];
    
    data4.font = [UIFont systemFontOfSize:12];
    
    data4.textColor = [UIColor grayColor];
    
//    data4.text = @"88";
    
    [self addSubview:data4];
    
    _LikePersonLabel = data4;

    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
    
}


// 健康

-(void)SetJiankangModel:(YzDriverNewsModel*)model{
    
    _Model = model;
    
    self.NewsTitleLabel.text = model.title;
    
    self.NewsTimeLabel.text = [VerifyPictureURL dateStringFromTimer:model.update_time];
    
    _LookPersonLabel.text = model.view_num;
    
    _lookPersonImg.frame = CGRectMake(YzWidth - 180, 73, 15, 15);
    
    NSString *path = [NSString stringWithFormat:@"http://www.woaisiji.com%@",model.picture[0]];
    
    [self.NewsImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@""]];


//    _likePersonImg.frame = CGRectMake(YzWidth - 130, 73, 15, 15);

    [self setUPframe];

}

//健康    咨询
-(void)setModel:(YzDriverNewsModel *)Model{
    
    
    _Model = Model;
    
    self.NewsTitleLabel.text = Model.title;
    
    self.NewsTimeLabel.text = [VerifyPictureURL dateStringFromTimer:Model.time];
    
    _LookPersonLabel.text = Model.view_num;
    
    
    
    NSString *path = [NSString stringWithFormat:@"http://www.woaisiji.com%@",Model.picture[0]];
    
    [self.NewsImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@""]];
    
    [self setUPframe];
    
}



//自驾游
-(void)SetDriverModel:(YzDriverNewsModel *)model{
    
    _Model = model;
    
    self.NewsTitleLabel.text = model.title;
    
    
//    [self.NewsImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.picture[0]] placeholderImage:[UIImage imageNamed:@"kefu"]]];
    NSString *imgPath = [NSString stringWithFormat:@"http://www.woaisiji.com%@",model.picture[0]];
    
    [self.NewsImg sd_setImageWithURL:[NSURL URLWithString:imgPath] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    
    NSString *headpicpath = [NSString stringWithFormat:@"http://www.woaisiji.com%@",model.headpic];

    
    [_headpicImg sd_setImageWithURL:[NSURL URLWithString:headpicpath] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    self.NewsTimeLabel.text = [VerifyPictureURL dateStringFromTimer:model.create_time];

    _LookPersonLabel.text = model.view_num;
    
    _LikePersonLabel.text = model.top;
    
    
    
    _nicknameLabel.text = model.nickname;

    [self setTralUpFrame];
}



-(void)setTralUpFrame{
    
    self.NewsImg.frame = CGRectMake(YzWidth - 85, 13, 75, 75);
    
    
    //    新闻title
    self.NewsTitleLabel.frame = CGRectMake(10, 5, YzWidth - 120, 50);
    
    
    //    查看人数
    self.LookPersonLabel.frame = CGRectMake(YzWidth - 160, 70, 40, 20);
    
    
    //    时间
//    self.NewsTimeLabel.frame = CGRectMake(10, 70, YzWidth/2 -20, 20);
    
    //    点赞
    self.LikePersonLabel.frame = CGRectMake(YzWidth - 110, 70, 20, 20);
    
    
    
    //    用户头像
    _headpicImg.frame = CGRectMake(10, 70, 20, 20);
    
    
    //    用户昵称
    _nicknameLabel.frame = CGRectMake(35, 70, YzWidth/2 -40, 20);
    
    _lookPersonImg.frame = CGRectMake(YzWidth - 180, 73, 15, 15);
    
    _likePersonImg.frame = CGRectMake(YzWidth - 130, 73, 15, 15);
    
}

-(void)setUPframe{

    self.NewsImg.frame = CGRectMake(YzWidth - 90, 13, 75, 75);

    //    新闻title
    self.NewsTitleLabel.frame = CGRectMake(10, 5, YzWidth - 120, 50);
    
    //    查看人数
    self.LookPersonLabel.frame = CGRectMake(YzWidth - 160, 70, 40, 20);
    
    //    时间
    self.NewsTimeLabel.frame = CGRectMake(10, 70, YzWidth/2 -20, 20);
    //    点赞
    self.LikePersonLabel.frame = CGRectMake(YzWidth - 110, 70, 20, 20);
    
//    _lookPersonImg.frame = CGRectMake(YzWidth - 180, 73, 15, 15);
//    用户头像
//    _headpicImg.frame = CGRectMake(10, 70, 20, 20);
    
//    用户昵称
//    _nicknameLabel.frame = CGRectMake(30, 70, YzWidth/2 -40, 20);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
