//
//  YzConstactionVC.m
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzDriverNewsVC.h"

#import "YzConstactionVC.h"

#import "YzConstactionCell.h"

#import "YzDriverNewsModel.h"

@interface YzConstactionVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end


static NSInteger Pages;

@implementation YzConstactionVC



-(NSMutableArray *)sources{
   
    if (!_sources) {
    
        _sources= [NSMutableArray array];
    
    }
    
    return _sources;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"咨询";
    
    [self loadUI];
    
    [self RequestConstaction];
    
    [self setRefreshing];
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequestConstaction];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self loadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}
-(void)loadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/information") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            
            for (NSDictionary *NewsDic in listArray) {
                
                if (listArray.count == 0) {
                    
                }else{
                
                
//                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                
                model.NewsId = NewsDic[@"id"];
                
                [freshA  addObject:model];
                
                }
//                [self.sources addObject:model];
            }

            
            [self.sources  addObjectsFromArray:freshA];
            
            
            NSLog(@"数组个数   %lu",(unsigned long)self.sources.count);
            
            //            NSLog(@"数据源   %@",self.sources );
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
        //        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
        
    }];

    
    
}
-(void)RequestConstaction{
    [self isnotwork];

    
    [NetMethod Post:LoveDriverURL(@"APP/Love/information") parameters:@{@"p":[NSString stringWithFormat:@"%ld",(long)Pages]} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            
            [self.sources removeAllObjects];
            
            for (NSDictionary *NewsDic in listArray) {
                
                
                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                
                model.NewsId = NewsDic[@"id"];
                
                NSLog(@"title   %@",model.title);
                
                [self.sources addObject:model];
            }
            
            
            
            [GCUtil saveEntitysData:self.sources  fileName:ART_NEWS];
            
            
        }
        
        
        [self.TableView reloadData];
        
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources.count;
    //    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     *  2.1.0  新界面
     */
    
    YzConstactionCell *cell = [YzConstactionCell cellWithTableView:tableView];
    
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
//    cell.Model =model;
    
    [cell setModel:model];
    
//    [cell setUPframe];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YzDriverNewsModel *model = self.sources[indexPath.row];

    
    
    
    YzDriverNewsVC *vc = [[YzDriverNewsVC alloc]init];
    
    vc.NewsID = model.NewsId;;
    
    vc.title = model.title;
    
    vc.NewsType = @"5";
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)isnotwork{
    
    
    
    //创建网络监听管理者对象
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                //                NSLog(@"未识别的网络");
                
                
                //                [MBProgressHUD showSuccess:@"未识别的网络" toView:super.];
            {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"未识别的网络" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                
            }
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
            {
                
                
                [MBProgressHUD showSuccess:@"网络异常请检查网络" toView:self.view];
                
                
                //加载数据
                [self initializeColumnData];
                //                [GCUtil entitysData:@""];
                
                return ;
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"2G,3G,4G...的网络");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"wifi的网络");
                break;
            default:
                break;
        }
    }];
    
    //开始监听
    [manager startMonitoring];
    
}


-(void)initializeColumnData{
    
    if (![GCUtil connectedToNetwork]&&(!self.sources)) {
        
        NSLog(@"没网络");
        return;
    }
    
    //新闻列表数组
    self.sources = [GCUtil entitysData:ART_NEWS];
    
    
    [self.TableView reloadData];
    
    
    NSLog(@"没网络   **********   %@",self.sources);
    
    
    //    return;
    
    

}

@end
