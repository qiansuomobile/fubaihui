//
//  YzDriverNewsModel.h
//  ASJ
//
//  Created by Jack on 16/9/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzDriverNewsModel : NSObject<NSCoding>



@property (nonatomic , copy) NSString *title;

@property (nonatomic , copy) NSString *headpic;

@property (nonatomic , copy) NSString *nickname;

@property (nonatomic , copy) NSString *content;

@property (nonatomic , strong)NSArray *picture;

@property (nonatomic , copy) NSString *uid;

@property (nonatomic , copy) NSString *time;

@property (nonatomic , copy) NSString *update_time;

@property (nonatomic , copy) NSString *is_sticky;

@property (nonatomic , copy) NSString *sort;

@property (nonatomic , copy) NSString *NewsId;

@property (nonatomic , copy) NSString *view_num;

@property (nonatomic ,copy) NSString *create_time;

@property (nonatomic ,copy) NSString *top;

@property (nonatomic ,copy) NSString *pid;


@end
