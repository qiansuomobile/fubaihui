//
//  YzDriverNewsModel.m
//  ASJ
//
//  Created by Jack on 16/9/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzDriverNewsModel.h"

@implementation YzDriverNewsModel


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.title = [decoder decodeObjectForKey:@"title"];
    self.headpic = [decoder decodeObjectForKey:@"headpic"];
    self.view_num = [decoder decodeObjectForKey:@"view_num"];
    self.content = [decoder decodeObjectForKey:@"content"];
    self.picture = [decoder decodeObjectForKey:@"picture"];
    //    self.pid = [decoder decodeObjectForKey:@"pid"];
    self.NewsId = [decoder decodeObjectForKey:@"NewsId"];
    self.time = [decoder decodeObjectForKey:@"time"];
    self.create_time = [decoder decodeObjectForKey:@"create_time"];
    self.nickname = [decoder decodeObjectForKey:@"nickname"];
    self.update_time = [decoder decodeObjectForKey:@"update_time"];
    //    self.picture = [decoder decodeObjectForKey:@"picture"];
    //    self.picture = [decoder decodeObjectForKey:@"picture"];
    //    self.picture = [decoder decodeObjectForKey:@"picture"];
    //    self.picture = [decoder decodeObjectForKey:@"picture"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.content forKey:@"content"];
    [encoder encodeObject:self.picture forKey:@"picture"];
    [encoder encodeObject:self.NewsId forKey:@"NewsId"];
    [encoder encodeObject:self.time forKey:@"time"];
    [encoder encodeObject:self.headpic forKey:@"headpic"];
    [encoder encodeObject:self.nickname forKey:@"nickname"];
    [encoder encodeObject:self.view_num forKey:@"view_num"];
    [encoder encodeObject:self.create_time forKey:@"create_time"];
    [encoder encodeObject:self.update_time forKey:@"update_time"];
    
    
}

//@property (nonatomic , copy) NSString *headpic;
//
//@property (nonatomic , copy) NSString *nickname;
//
//@property (nonatomic , copy) NSString *content;
//
//@property (nonatomic , strong)NSArray *picture;
//
//@property (nonatomic , copy) NSString *uid;
//
//@property (nonatomic , copy) NSString *time;
//
//@property (nonatomic , copy) NSString *update_time;
//
//@property (nonatomic , copy) NSString *is_sticky;
//
//@property (nonatomic , copy) NSString *sort;
//
//@property (nonatomic , copy) NSString *NewsId;
//
//@property (nonatomic , copy) NSString *view_num;
//
//@property (nonatomic ,copy) NSString *create_time;
//
//@property (nonatomic ,copy) NSString *top;
//
//@property (nonatomic ,copy) NSString *pid;

@end
