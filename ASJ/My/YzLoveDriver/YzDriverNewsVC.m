//
//  YzDriverNewsVC.m
//  ASJ
//
//  Created by Jack on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzConstactionCell.h"

#import "YzDriverNewsModel.h"

#import "YzDriverNewsVC.h"

#import "YzPinglunCell.h"

#import "TURLSessionProtocol.h"


@interface YzDriverNewsVC ()<UITextFieldDelegate,UIWebViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>{
    
    NSIndexPath *_indexPath;
    
    NSIndexPath *_currentIndexPath;
    
}


@property (nonatomic , strong) UITextField *CommenTextfild;

@property (nonatomic , strong) UIView *CommentView;

@property (nonatomic , assign) BOOL TTTT ;

@property (nonatomic , strong) UITableView *TableView;

@property (nonatomic , strong) UIWebView *NewsWebView;

//@property (nonatomic , strong) UIView *HeadView;

@property (nonatomic ,strong) UIProgressView *loadWebProgressView;

@property (nonatomic ,strong) NSMutableArray *sources;

@property (nonatomic ,strong) UIView *HeardView;

@property (nonatomic , copy) NSString *URL;



@end

@implementation YzDriverNewsVC


-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources= [NSMutableArray array];
        
    }
    return _sources;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//    [YMWebCacheProtocol start];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self LoadWebView];
    
}

-(void)LoadTableView{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight- 60 - 60)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    //    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.backgroundColor = [UIColor whiteColor];
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    
    
    self.TableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    self.HeardView = [[UIView alloc]init];
    
    self.HeardView.frame = CGRectMake(0, 0, YzWidth, YzHeight- 60 - 60);
    
    self.TableView.tableHeaderView =self.HeardView;
    
    [self.view insertSubview:self.loadWebProgressView belowSubview:self.HeardView];

    
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}



-(void)LoadWebView{
    
    
    if(![GCUtil connectedToNetwork]){
        
        [MBProgressHUD showSuccess:@"网络连接异常" toView:self.view.window];
        
    }
    
    
    UIWebView *webView = [[UIWebView alloc]init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(YzDown)];
    
    tap.delegate =self;
    
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    //    webView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 60 - 50);
    
    webView.delegate = self;
    
    webView.scalesPageToFit = YES;
    
    //网页离线缓存
   
    NSString *path1 = [NSString stringWithFormat:@"http://www.woaisiji.com/APP/Public/get_goods_detail/id/%@/type/%@",self.NewsID,self.NewsType];
    
    _URL = path1;
    
    
    NSLog(@"%@",path1);
    
    
//    NSString *path = @"http://www.woaisiji.net/Index/choujiang.html";
    
    //设置网页缓存
    
    [GCUtil YzSetWebCache:path1 webView:webView];

    self.NewsWebView = webView;
    
    self.loadWebProgressView = [[UIProgressView alloc]init];
    
    self.loadWebProgressView.frame = CGRectMake(0, 1, YzWidth, 4);
    
    self.loadWebProgressView.progressTintColor = [UIColor greenColor];

    self.loadWebProgressView.trackTintColor = [UIColor clearColor];
    
    
    
    if ([self.NewsType isEqualToString:@"5"]) {
        
        webView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 60 );
        
//        self.loadWebProgressView.frame = CGRectMake(0, 1, YzWidth, 4);

        [self.view addSubview:webView];
        
        [webView addSubview:self.loadWebProgressView];
        
//        [self.view insertSubview:self.loadWebProgressView belowSubview:webView];

//        [self.HeardView addSubview:webView];
  
    }else if([self.NewsType isEqualToString:@"3"]){
        
        webView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 60 - 60);
        //        司机健康
        [self LoadRequstDriverBest];
        
        [self LoadContetView];
        
        NSArray *btnIMgArray = @[@"Yzfenxiang",@"Yzpinglun"];
        
        for (int i = 0 ; i<2; i++) {
            
            UIButton *Type = [UIButton buttonWithType:UIButtonTypeCustom];
            
            Type.tag = 500 +i;
            
            [Type setImage:[UIImage imageNamed:btnIMgArray[i]] forState:UIControlStateNormal];
            
            Type.frame = CGRectMake(YzWidth -50- i*40 , 10, 30, 30);
            
            [Type addTarget:self action:@selector(SelectBtn:) forControlEvents:UIControlEventTouchUpInside];
            //        Type.backgroundColor = [UIColor redColor];
            
            [self.CommentView  addSubview:Type];
            
        }
        
        [self LoadTableView];
        
        [self.HeardView addSubview:webView];
        
    }else{
        
        //        自驾游
        [self LoadRequstDetail];
        
        webView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 60 - 60);
        
        [self LoadContetView];
        
        NSArray *btnIMgArray = @[@"Yzfenxiang",@"Yzdianzan",@"Yzpinglun"];
        
        for (int i = 0 ; i<3; i++) {
            
            UIButton *Type = [UIButton buttonWithType:UIButtonTypeCustom];
            
            Type.tag = 400 +i;
            
            [Type setImage:[UIImage imageNamed:btnIMgArray[i]] forState:UIControlStateNormal];
            
            Type.frame = CGRectMake(YzWidth -50- i*40 , 10, 30, 30);
            
            [Type addTarget:self action:@selector(SelectBtn:) forControlEvents:UIControlEventTouchUpInside];
            //        Type.backgroundColor = [UIColor redColor];
            
            [self.CommentView  addSubview:Type];
        }
        
        [self LoadTableView];
        
        [self.HeardView addSubview:webView];
        
    }
    
}
-(void)YzDown{
    
    [self.view endEditing:NO];
    
}
-(void)LoadContetView{
    
    UIView *commentView = [UIView new];
    
    commentView.frame = CGRectMake(0, YzHeight - 120, YzWidth, 60);
    
    [self.view addSubview:commentView];
    
    commentView.backgroundColor = [UIColor whiteColor];
    
    self.CommentView = commentView;
    
//    self.TableView.tableFooterView = commentView;
    
    
    _CommenTextfild = [[UITextField alloc]init];
    
    _CommenTextfild.returnKeyType = UIReturnKeySend;
    
    _CommenTextfild.delegate = self;
    
    _CommenTextfild.font = [UIFont systemFontOfSize:14];
    
    _CommenTextfild.frame = CGRectMake(20, 10, YzWidth -150, 30);
    
    _CommenTextfild.placeholder = @"  写评论,调戏下小编";
    
    //    _CommenTextfild.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    _CommenTextfild.leftView = [UIView new];
    
    [commentView addSubview:_CommenTextfild];
    
    _CommenTextfild.layer.cornerRadius = 15;
    
    _CommenTextfild.backgroundColor = RGBColor(237, 237, 237);
    
    
}
//获取健康评论列表

-(void)LoadRequstDriverBest{
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/jkReply") parameters:@{@"id":self.NewsID} success:^(id responseObject) {
        
        NSDictionary *DIC = responseObject;
        
        int code = [DIC[@"code"] intValue];
        
        NSLog(@"%@",DIC);
        
        if ( code == 200) {
            
            NSArray *ListArray = DIC[@"list"];
            
            if ([ListArray isKindOfClass:[NSNull class]]) {
                
                //                [MBProgressHUD showSuccess:@"" toView:self.view ];
                return ;
            }
            
            [self.sources removeAllObjects];
            
            for (NSDictionary *listDic in ListArray) {
                
                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:listDic];
                
                [self.sources addObject:model];

            }
            
            [self.TableView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)LoadRequstDetail{
    
    //    自驾游评价
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travelDetail") parameters:@{@"id":self.NewsID} success:^(id responseObject) {
        
        NSDictionary *DIC = responseObject;
        
        int code = [DIC[@"code"] intValue];
        
//        NSLog(@"%@",DIC);
        
        if ( code == 200) {
            
            NSArray *ListArray = DIC[@"list"];
            
            if ([ListArray isKindOfClass:[NSNull class]]) {
                
                //                [MBProgressHUD showSuccess:@"" toView:self.view ];
                return ;
            }
            
            [self.sources removeAllObjects];
            
            for (NSDictionary *listDic in ListArray) {
                
                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:listDic];
                
                [self.sources addObject:model];
                
            }
            
            [self.TableView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}


-(void)SelectBtn:(UIButton *)sender{
    
    
    if (!DriverISLogIN) {
        
        LoginViewController *vc = [[LoginViewController alloc]init];
        
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        
        [self presentViewController:NaVC animated:YES completion:nil];
        
    }
    
    switch (sender.tag) {
        case 400:
        {
            //            分享
        }
            break;
        case 401:
        {
            //            点赞
            [self YZsetZan];
        }
            break;
        case 402:
        {
            //            评论
            
            //            NSLog(@"GFHGHJGKJHJK");
        }
            break;
            
        case 500:
        {
        }
            break;
            
        case 501:
        {
            
        }
            break;
        default:
            break;
    }
    
    
}

-(void)YZsetZan{

    //    点赞
    NSDictionary *infodic = @{@"uid":LoveDriverID,@"id":self.NewsID};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/setyes") parameters:infodic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        //        NSLog(@"  %@", dic[@"msg"] );
        
        [MBProgressHUD showSuccess:dic[@"msg"] toView:self.view];
        
    } failure:^(NSError *error) {
        
    }];
    
}


-(void)IsDriverISLogIN{
    
    LoginViewController *vc = [[LoginViewController alloc]init];
    
    YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
    
    [self presentViewController:NaVC animated:YES completion:nil];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if (!DriverISLogIN) {
        
        [self IsDriverISLogIN];
        
    }else{
        
        
        //    _CommenTextfild.text = textField.text;
        
        //    NSLog(@"%@",_CommenTextfild.text);
        
        NSString *goodStr =  [NetMethod setEMOjiStr:textField.text];
        
        NSDictionary *InfoDic = @{@"uid":LoveDriverID,@"content":goodStr,@"pid":self.NewsID};
        
        if ([self.NewsType isEqualToString:@"3"]) {
            //       司机健康
            [NetMethod Post:LoveDriverURL(@"APP/Love/jkReplyAdd") parameters:InfoDic success:^(id responseObject) {
                
                NSDictionary *DIC = responseObject;
                
                //            NSLog(@" 发送表情      %@",  DIC);
                
                int code = [DIC[@"code"] intValue];
                
                if (code ==200) {
                    
                    //            NSLog(@"%@",DIC[@"msg"]);
                    
                    [MBProgressHUD showSuccess:DIC[@"msg"] toView:self.view];
                    
                    [self LoadRequstDriverBest];
                    
                    self.CommenTextfild.text = @"";
                }
                
            } failure:^(NSError *error) {
                
            }];
            
            [self.TableView reloadData];
            
        }else{
            //        自驾游
            
            [NetMethod Post:LoveDriverURL(@"APP/Love/lyReplyAdd") parameters:InfoDic success:^(id responseObject) {
                
                NSDictionary *DIC = responseObject;
                
                int code = [DIC[@"code"] intValue];
                
                if (code ==200) {
                    
                    //            NSLog(@"%@",DIC[@"msg"]);
                    
                    [MBProgressHUD showSuccess:DIC[@"msg"] toView:self.view];
                    
                    [self LoadRequstDetail];
                    
                    self.CommenTextfild.text = @"";
                }
                
            } failure:^(NSError *error) {
                
            }];
            
            [self.TableView reloadData];
            
          
//            [self.TableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//            
        }
        
    }
    
    [self.view endEditing:NO];
    return YES;
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    [self.loadWebProgressView setProgress:0.3];
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [self.loadWebProgressView setProgress:0.5];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self.loadWebProgressView setProgress:1];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.loadWebProgressView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.loadWebProgressView removeFromSuperview];
    }];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [self.loadWebProgressView setProgress:0.8];
    //    self.mainWebView.hidden=YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzPinglunCell *cell = [YzPinglunCell cellWithTableView:tableView];
    
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
    [cell setPingLunModel:model];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *heard = [[UIView alloc]init];
    
    heard.frame = CGRectMake(0, 0, YzWidth, 50);
    
    heard.backgroundColor = RGBColor(243, 243, 243);
    
    
    UIView *heard2 = [[UIView alloc]init];
    
    heard2.frame = CGRectMake(0, 10, YzWidth, 40);
    
    heard2.backgroundColor = [UIColor whiteColor];
    
    
    [heard addSubview:heard2];
    
    UILabel *pingl = [[UILabel alloc]init];
    
    pingl.frame = CGRectMake(15, 15, 200, 20);
    
    pingl.textColor = [UIColor grayColor];
    
    pingl.text = @"热门评论";
    
    [heard2 addSubview:pingl];
    
    
    if (self.sources.count == 0) {
        
        pingl.text = @"暂无评论";
        
    }
    
    return heard;
}



//
////获取web里的所有的img url
//- (NSString *)createImgArrayJavaScript{
//    NSString *js = @"var imgArray = document.getElementsByTagName('img'); var imgstr = ''; function f(){ for(var i = 0; i < imgArray.length; i++){ imgstr += imgArray[i].src;imgstr += ';';} return imgstr; } f();";
//    return js;
//}
//
////返回web img图片的数量
//- (NSString *)createGetImgNumJavaScript{
//    NSString *js = @"var imgArray = document.getElementsByTagName('img');function f(){ var num=imgArray.length;return num;} f();";
//    return js;
//}

@end
