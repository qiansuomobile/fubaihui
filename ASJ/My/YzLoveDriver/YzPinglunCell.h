//
//  YzPinglunCell.h
//  ASJ
//
//  Created by Jack on 16/9/10.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzDriverNewsModel.h"
#import <UIKit/UIKit.h>

@interface YzPinglunCell : UITableViewCell




+ (instancetype)cellWithTableView:(UITableView *)tableView;


-(void)setPingLunModel:(YzDriverNewsModel *)model;

@end
