//
//  YzPinglunCell.m
//  ASJ
//
//  Created by Jack on 16/9/10.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzPinglunCell.h"



@interface YzPinglunCell()


@property (nonatomic , strong) UILabel *nickNameLabel;

@property (nonatomic , strong) UILabel *CreatTimeLabel;

@property (nonatomic , strong) UILabel *ContentLabel;

@property (nonatomic , strong) UIImageView *HeardImgView;

@end

@implementation YzPinglunCell




- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
       
//        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        self.backgroundColor = [UIColor whiteColor];
        
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpSubview];
    }
    return self;
}




+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
    
}


-(void)setUpSubview{
    
    
    
    
    UIImageView *imageV3 = [[UIImageView alloc]init];
    
    //    imageV3.backgroundColor = [UIColor whiteColor];
    
//    imageV3.image = [UIImage imageNamed:@"lookperson-1"];
    imageV3.layer.masksToBounds = YES;

    imageV3.layer.cornerRadius = 20;
    
    [self addSubview:imageV3];
    
    _HeardImgView = imageV3;
    
    
    
    
    
    UILabel *date = [[UILabel alloc]init];
    
    date.font = [UIFont systemFontOfSize:13];
    
    date.numberOfLines = 0;
    
    //    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    date.textColor = [UIColor blackColor];
    
    [self addSubview:date];
    
    _ContentLabel = date;
    
    
    
    
    
    UILabel *date2 = [[UILabel alloc]init];
    
    date2.font = [UIFont systemFontOfSize:10];
    
    date2.numberOfLines = 0;
    
    //    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    date2.textColor = [UIColor grayColor];
    
    [self addSubview:date2];
    
    _CreatTimeLabel = date2;

    
    
    
    
    
    
    UILabel *date3 = [[UILabel alloc]init];
    
    date3.font = [UIFont systemFontOfSize:12];
    
    date3.numberOfLines = 0;
    
    //    date.text = @"多语种即时在线翻译_百度翻译   请输入需要翻译的文字内容或者网页地址";
    
    date3.textColor = [UIColor blackColor];
    
    [self addSubview:date3];
    
    _nickNameLabel = date3;


    
    
}


//健康    咨询
-(void)setPingLunModel:(YzDriverNewsModel *)Model{
    
    
//    _Model = Model;
    NSString *path = [NSString stringWithFormat:@"http://www.woaisiji.com%@",Model.headpic];
    
    [self.HeardImgView sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    
    self.nickNameLabel.text = Model.nickname;
    
    self.CreatTimeLabel.text = [VerifyPictureURL dateStringFromTimer:Model.create_time];
    
//    const char *jsonString = [Model.content UTF8String];   // goodStr 服务器返回的 json
//        NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
//        NSString *goodMsg1 = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];
  
    
    self.ContentLabel.text = [NetMethod GetEmojiStr:Model.content];
    

    [self setUPframe];
    
}


-(void)setUPframe{
    
    //    头像
    self.HeardImgView.frame = CGRectMake(10, 5, 40, 40);
    
    
    //    内容
    self.ContentLabel.frame = CGRectMake(60, 45, YzWidth-75, 50);
    
    //    昵称
    self.nickNameLabel.frame = CGRectMake(60, 5, 100, 20);
    
//评论时间
    self.CreatTimeLabel.frame = CGRectMake(60, 30, 200, 10);

    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
