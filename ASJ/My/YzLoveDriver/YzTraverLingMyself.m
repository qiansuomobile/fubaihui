//
//  YzTraverLingMyself.m
//  ASJ
//
//  Created by Jack on 16/9/12.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzTraverLingMyself.h"

#import "YzConstactionCell.h"

#import "YzTraverlingVC.h"

#import "YzDriverNewsVC.h"


@interface YzTraverLingMyself ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end


static NSInteger Pages;

@implementation YzTraverLingMyself


-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources= [NSMutableArray array];
    }
    
    return _sources;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //
    self.title = @"我的发布";
    
    Pages =1;
    
    [self loadUI];
    
    [self RequestConstaction];
    
    [self setRefreshing];
    // Do any additional setup after loading the view.
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequestConstaction];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self loadMoreData];
        
        [self.TableView.mj_footer endRefreshing];
        
    }];
}



-(void)loadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travel") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            
            for (NSDictionary *NewsDic in listArray) {
                
                if (listArray.count == 0) {
                    
                }else{
                    
                    model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                    
                    model.NewsId = NewsDic[@"id"];
                    
                    [freshA  addObject:model];
                    
                }
            }
            [self.sources  addObjectsFromArray:freshA];
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        
    }];
    
}
-(void)RequestConstaction{
    
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travel") parameters:@{@"uid":LoveDriverID,@"p":@"1"} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            [self.sources removeAllObjects];
            
            for (NSDictionary *NewsDic in listArray) {
                
                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                
                model.NewsId = NewsDic[@"id"];
                
                
//                NSLog(@"健康  %@",model.title);
                
                
                [self.sources addObject:model];
            }
            
        }
        
        [self.TableView reloadData];
        
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     *  2.1.0  新界面
     */
    
    YzConstactionCell *cell = [YzConstactionCell cellWithTableView:tableView];
    
    //    cell.logtimes = self.logtimes[indexPath.row];
    //
    //    cell.picS_url = self.sources[indexPath.row];
    
    
    //    cell.textLabel.text = @"xxxxx";
    
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
    //    cell.Model= model;
    
    [cell SetDriverModel:model];
    
    
    //    [cell setUPframe];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzDriverNewsVC *vc = [[YzDriverNewsVC alloc]init];
    
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
    vc.NewsID = model.NewsId;
    
    vc.title = model.title;
    
    vc.NewsType = @"4";
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end

