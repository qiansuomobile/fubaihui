//
//  YzTraverlingVC.m
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzTraverLingMyself.h"

#import "YzConstactionCell.h"

#import "YzTraverlingVC.h"

#import "YzDriverNewsVC.h"

#import "YzSendTraverlingVC.h"

@interface YzTraverlingVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end


static NSInteger Pages;

@implementation YzTraverlingVC


-(NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources= [NSMutableArray array];
    }
    
    return _sources;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//
    self.title = @"自驾游";
    
    Pages =1;
    
    [self loadUI];
    
    [self RequestConstaction];
    
    [self setRefreshing];
    // Do any additional setup after loading the view.
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequestConstaction];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self loadMoreData];
        
        [self.TableView.mj_footer endRefreshing];
        
    }];
}



-(void)loadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travel") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            
            for (NSDictionary *NewsDic in listArray) {
                
                if (listArray.count == 0) {
                    
                }else{
                    
                    model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                    
                    model.NewsId = NewsDic[@"id"];
                    
                    [freshA  addObject:model];
                    
                }
            }
            [self.sources  addObjectsFromArray:freshA];
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        
    }];
    
}
-(void)RequestConstaction{
    
    
    [self isnotwork];

    
    [NetMethod Post:LoveDriverURL(@"APP/Love/travel") parameters:@{@"p":@"1"} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//                NSLog(@"%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            
            [self.sources removeAllObjects];
            
            for (NSDictionary *NewsDic in listArray) {
                
                YzDriverNewsModel *model = [[YzDriverNewsModel alloc]init];
                
                model = [YzDriverNewsModel objectWithKeyValues:NewsDic];
                
                model.NewsId = NewsDic[@"id"];
                
                
//                NSLog(@"健康  %@",model.title);
                
                
                [self.sources addObject:model];
            }
            
            
            [GCUtil saveEntitysData:self.sources fileName:ART_TRA];

            
        }
        
        [self.TableView reloadData];
        
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    YzConstactionCell *cell = [YzConstactionCell cellWithTableView:tableView];
    YzConstactionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[YzConstactionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
    [cell SetDriverModel:model];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzDriverNewsVC *vc = [[YzDriverNewsVC alloc]init];
    
    YzDriverNewsModel *model = self.sources[indexPath.row];
    
    vc.NewsID = model.NewsId;
    
    vc.title = model.title;
    
    vc.NewsType = @"4";

    [self.navigationController pushViewController:vc animated:YES];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *heard = [[UIView alloc]init];
    
    heard.backgroundColor = [UIColor whiteColor];
    
    heard.frame = CGRectMake(0, 0, YzWidth, 50);
    
    
    UIButton *fabu = [UIButton buttonWithType:UIButtonTypeCustom];
    
    fabu.tag = 100;
    
    [fabu setImage:[UIImage imageNamed:@"Yzfabu"] forState:UIControlStateNormal];
    
    fabu.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    fabu.frame = CGRectMake(10, 10, 80, 30);
    
    fabu.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [fabu setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    [heard addSubview:fabu];
    
    [fabu addTarget:self action:@selector(FabuBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [fabu setTitle:@"发布" forState:UIControlStateNormal];
    
    fabu.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    
    
    
    UIButton *fabu2 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [fabu2 setImage:[UIImage imageNamed:@"Yzwoyifabu"] forState:UIControlStateNormal];
    
    fabu2.tag = 200;
    
    fabu2.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    fabu2.frame = CGRectMake(YzWidth-110, 10, 100, 30);
    
    fabu2.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [fabu2 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    [heard addSubview:fabu2];
    
    [fabu2 setTitle:@"已发布" forState:UIControlStateNormal];
    
    [fabu2 addTarget:self action:@selector(FabuBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return heard;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
    
}
-(void)FabuBtn:(UIButton*)sender{
    
    switch (sender.tag) {
        case 100:
        {
            YzSendTraverlingVC *vc = [[YzSendTraverlingVC alloc]init];
//            vc.FabuContent = @"自驾游";

            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 200:
        {
            
            YzTraverLingMyself *vc = [[YzTraverLingMyself alloc]init];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        default:
            break;
    }
    
    
    
}

-(void)initializeColumnData{
    
    if (![GCUtil connectedToNetwork]&&(!self.sources)) {
        
        NSLog(@"没网络");
        return;
    }
    //新闻列表数组
    self.sources = [GCUtil entitysData:ART_TRA];
    
    [self.TableView reloadData];
    
}



-(void)isnotwork{
    //创建网络监听管理者对象
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                //                NSLog(@"未识别的网络");
                
                
                //                [MBProgressHUD showSuccess:@"未识别的网络" toView:super.];
            {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"未识别的网络" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                
            }
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
            {
                
                [MBProgressHUD showSuccess:@"网络异常请检查网络" toView:self.view];
                
                //加载数据
                [self initializeColumnData];
                //                [GCUtil entitysData:@""];
                
                return ;
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"2G,3G,4G...的网络");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"wifi的网络");
                break;
            default:
                break;
        }
    }];
    
    //开始监听
    [manager startMonitoring];
    
}



@end
