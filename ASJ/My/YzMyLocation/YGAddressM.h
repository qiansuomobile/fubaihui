//
//  YGAddressM.h
//  Yunyige
//
//  Created by zlyunduan on 15/12/31.
//  Copyright © 2015年 ZLyunduan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YGAddressM : NSObject


/*
 "consignee" = Nice ,
 "best_time" = ,
 "address_name" = ,
 "mobile" = ,
 "province" = 安徽,
 "user_id" = 24,
 "zipcode" = ,
 "address_id" = 42,
 "sign_building" = ,
 "address" = Shanghai ,
 "city" = 安庆,
 "district" = 0,
 "tel" = 1233,
 "email" = ,
 "country" = 0,*/

//
///**
//*  地址id
//*/
//@property (nonatomic, copy) NSString *address_id;
//
///**
// *  姓名
// */
//@property (nonatomic, copy) NSString *consignee;
//
///**
// *  电话
// */
//@property (nonatomic, copy) NSString *tel;
//
///**
// *   详细地址
// */
//@property (nonatomic, copy) NSString *content;
//
///**
// *  省份
// */
//@property (nonatomic, copy) NSString *province;
///**
// *  城市
// */
//@property (nonatomic, copy) NSString *city;
//
///**
// *  district
// */
//@property (nonatomic, copy) NSString *district;
//
///**
// *  后缀地址
// */
//@property (nonatomic, copy) NSString *address;



@property (nonatomic , copy) NSString *New_place;


@property (nonatomic , copy) NSString *New_nickname;


@property (nonatomic , copy) NSString *New_mobile;


@property (nonatomic , copy) NSString *status;


@property (nonatomic , copy) NSString *New_user_id;


@property (nonatomic , copy) NSString *ctime;


@property (nonatomic , copy) NSString *LocationID;


@property (nonatomic , copy) NSString *New_code;


@property (nonatomic, copy) NSString *is_default;



@end
