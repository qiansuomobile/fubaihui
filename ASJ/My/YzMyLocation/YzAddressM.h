//
//  YzAddressM.h
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzAddressM : NSObject

/*
 "consignee" = Nice ,
 "best_time" = ,
 "address_name" = ,
 "mobile" = ,
 "province" = 安徽,
 "user_id" = 24,
 "zipcode" = ,
 "address_id" = 42,
 "sign_building" = ,
 "address" = Shanghai ,
 "city" = 安庆,
 "district" = 0,
 "tel" = 1233,
 "email" = ,
 "country" = 0,*/


/**
 *  地址id
 */
@property (nonatomic, copy) NSString *address_id;

/**
 *  姓名
 */
@property (nonatomic, copy) NSString *consignee;

/**
 *  电话
 */
@property (nonatomic, copy) NSString *tel;

/**
 *   详细地址
 */
@property (nonatomic, copy) NSString *content;

/**
 *  省份
 */
@property (nonatomic, copy) NSString *province;
/**
 *  城市
 */
@property (nonatomic, copy) NSString *city;

/**
 *  district
 */
@property (nonatomic, copy) NSString *district;

/**
 *  后缀地址
 */
@property (nonatomic, copy) NSString *address;



//@property (nonatomic, copy) NSString *new_code;

@end
