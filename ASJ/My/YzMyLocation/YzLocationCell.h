//
//  YzLocationCell.h
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YGAddressM.h"
#import <UIKit/UIKit.h>


@class YzLocationCell;

//@class YGAddressM;

//@class YzAddressM;

typedef NS_ENUM(NSInteger, CellBtnTouchIndex) {
    
    CellBtnTouchIndexSure,
    
    CellBtnTouchIndexEdit,
    
    CellBtnTouchIndexDel
    
};


//代理
@protocol YzAddressCellDelegate <NSObject>


- (void)addressCellDidClickedBtnWith:(YzLocationCell *)addressCell addressModel:(YGAddressM *)addressModel index:(CellBtnTouchIndex)index;


@end



@interface YzLocationCell : UITableViewCell


@property (nonatomic, strong) YGAddressM *addressM;



@property (nonatomic, weak)id<YzAddressCellDelegate>delegate;

//@property (nonatomic, strong) YzAddressM *YaddressM;

+ (instancetype)cellWithTableView:(UITableView *)tableView;




/**
 *  确认
 */

@property (nonatomic, strong) UIButton *sure;



/**
 *  背景
 */


@property (nonatomic, strong) UIImageView *imageV;



@end


