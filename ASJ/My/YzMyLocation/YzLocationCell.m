//
//  YzLocationCell.m
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzLocationCell.h"

@interface YzLocationCell()


@property (nonatomic , strong) UIView *BackView;

/**
 *  姓名
 */
@property (nonatomic, strong) UILabel *nameL;

/**
 *  电话
 */
@property (nonatomic, strong) UILabel *phoneL;

/**
 *  详细地址
 */
@property (nonatomic, strong) UILabel *content;

/**
 *  分隔符
 */
@property (nonatomic, strong) UIView *divide;

/**
 *  编辑
 */
@property (nonatomic, strong) UIButton *edit;

/**
 *  删除
 */
@property (nonatomic, strong) UIButton *delete;


@end


@implementation YzLocationCell



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        [self setUpSubview];
        
    }
    
    return self;
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    
//    
//    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//    
//    if (!cell){
    
      id cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    
//    }
    return cell;
    
}


-(void)setUpSubview{
    
    _BackView = [UIView new];
    
    _BackView.layer.cornerRadius = 2;
    
    _BackView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:_BackView];
    
    
    
    /**
     *  姓名
     */
    self.nameL = [[UILabel alloc]init];
    
    [self setUpLable:self.nameL title:@"" font:14 color:[UIColor blackColor]];
    
//    self.nameL.text = @"天王老子";
    /**
     *  电话
     */
  
    
    
    self.phoneL = [[UILabel alloc]init];
    
    [self setUpLable:self.phoneL title:@"" font:14 color:[UIColor blackColor]];
    
    self.phoneL.textAlignment = NSTextAlignmentRight;
    
//    self.phoneL.text= @"1888888";
    
    /**
     *  详细地址
     */
    
    self.content = [[UILabel alloc]init];
    
    self.content.numberOfLines = 0;
    
    
    [self setUpLable:self.content title:@"" font:14 color:[UIColor blackColor]];
    
    
    
    
    
    // 分隔符
    self.divide = [[UIView alloc]init];
    
    self.divide.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [_BackView addSubview:self.divide];
    
    
    
    
    
    // 默认
    self.sure = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sure.hidden = YES;
//    if (self.addressM.status) {
//        [self setUpButton:self.sure image:@"yixuanze" title:@"默认地址" index:CellBtnTouchIndexSure];
//    }else{
//        [self setUpButton:self.sure image:@"weixuanze" title:@"默认地址" index:CellBtnTouchIndexSure];
//    }
    
    
    
    
    
    // 编辑
    self.edit = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.edit  setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
  
    [self setUpButton:self.edit image:@"bianji" title:@"编辑" index:CellBtnTouchIndexEdit];
    
//    self.edit.hidden = YES;
    
    
    
    
    // 删除
    
    self.delete = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self setUpButton:self.delete image:@"shanchu" title:@"删除" index:CellBtnTouchIndexDel];
    
}


- (void)setUpButton:(UIButton *)btn image:(NSString *)image title:(NSString *)title index:(CellBtnTouchIndex)index{
    
    
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn setTitle:title forState:UIControlStateNormal];
    
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    btn.tag = index;
    
    [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
    
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [btn sizeToFit];
    
    [_BackView addSubview:btn];
    
}


- (void)clickBtn:(UIButton *)btn{
    
    if ([self.delegate respondsToSelector:@selector(addressCellDidClickedBtnWith:addressModel:index:)]) {
        
        [self.delegate addressCellDidClickedBtnWith:self addressModel:self.addressM index:btn.tag];
    }
    
}

- (void)setUpLable:(UILabel *)label title:(NSString *)title font:(NSInteger)font color:(UIColor *)color{
    
    label.text = title;
    
    label.font = [UIFont systemFontOfSize:font];
    
    [label sizeToFit];
    
    label.textColor = color;
    
    [_BackView addSubview:label];
    
}


- (void)setAddressM:(YGAddressM *)addressM{
    
    _addressM = addressM;
    
    [self setUpData];
    
    [self setUpFrame];
    
}

- (void)setUpData{
    
    
    self.nameL.text = self.addressM.New_nickname;
    
    [self.nameL sizeToFit];
    
//    手机号
//    NSString *number = [ self.addressM.New_mobile substringWithRange:NSMakeRange(3,4)] ;
    
    //
//    NSString *ppp = [ self.addressM.New_mobile stringByReplacingOccurrencesOfString:number withString:@"****"];
    
    
    self.phoneL.text = self.addressM.New_mobile;
    
    [self.phoneL sizeToFit];
    
    self.content.text = self.addressM.New_place;
    
    self.content.textColor = [UIColor grayColor];
    
    [self.content sizeToFit];
    
}

- (void)setUpFrame{
    
    
    
    
    _BackView.frame =self.bounds;
    
    /**
     *  姓名
     */
    self.nameL.frame = CGRectMake(10, 14,60, 15);
    
    /**
     *  电话
     */
    self.phoneL.frame = CGRectMake(CGRectGetMaxX(self.nameL.frame) + 20, 18, self.phoneL.frame.size.width, self.phoneL.frame.size.height);
   
    self.phoneL.frame = CGRectMake(YzWidth -200, 14, 180, 15);
    
    
    /**
     *  详细地址
     */
    
    // bounding Size
    CGFloat textW = YzWidth - 30;
    
    CGSize size = CGSizeMake(textW, MAXFLOAT);
    
    // bounding Attribute
    //翻页模式
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14], NSParagraphStyleAttributeName:paragraphStyle};
    
    // 获取大小
    
    
    CGSize textSize = [self.addressM.New_place  boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil].size;
    
    self.content.frame = CGRectMake(10, 10 + CGRectGetMaxY(self.nameL.frame), textSize.width, textSize.height);
    
    // 分割线
    self.divide.frame = CGRectMake(0, CGRectGetMaxY(self.content.frame) + 10, YzWidth, 1);
    
    
    // btn
    self.sure.frame = CGRectMake(5, 9 + CGRectGetMaxY(self.divide.frame), self.sure.bounds.size.width + 20, self.sure.bounds.size.height);
    
    
    self.delete.frame = CGRectMake(YzWidth - 20 - self.delete.bounds.size.width-5 ,  9 + CGRectGetMaxY(self.divide.frame) ,  self.delete.bounds.size.width + 10, self.delete.bounds.size.height);
    
    //    编辑
    self.edit.frame = CGRectMake(YzWidth -20 - self.delete.bounds.size.width - 20 - 40, 9 + CGRectGetMaxY(self.divide.frame), self.edit.bounds.size.width + 10, self.edit.bounds.size.height);
    
    
    _BackView.frame = CGRectMake(5, 10, YzWidth - 10, textSize.height + 90);
    
    
}



@end
