//
//  YzMyLocationVC.h
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YGAddressM.h"

@protocol yzLocationDelegate <NSObject>

-(void)GetMylocation:(YGAddressM *)LocationModel index:(NSIndexPath*)index;

@end

@interface YzMyLocationVC : UIViewController

@property (nonatomic,assign) BOOL isAccount;
@property (nonatomic ,assign) id <yzLocationDelegate>delegate;

//@property (nonatomic ,weak) YzMyLocationVC *delegate;

@end
