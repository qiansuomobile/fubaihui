//
//  YzMyLocationVC.m
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzLocationCell.h"

#import "YzMyLocationVC.h"

#import "YGAddressM.h"

#import "YzSetAddressVC.h"

@interface YzMyLocationVC ()<YzAddressCellDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (strong, nonatomic) NSMutableArray * addresses;

@property (nonatomic, strong) NSMutableArray *cellArray;

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic , strong)YGAddressM *model;
@end

static NSInteger proSelectCellIndex = 0;


@implementation YzMyLocationVC

- (NSMutableArray *)cellArray{
    
    if (!_cellArray) {
        
        _cellArray = [NSMutableArray array];
        
    }
    
    return _cellArray;
}


- (NSMutableArray *)addresses{
    
    if (!_addresses) {
        
        _addresses = [NSMutableArray array];
        
    }
    
    return _addresses;
}

//addresses

- (void)viewDidLoad {

    [super viewDidLoad];
    
     self.title = @"收货地址";
    
    [self LoadUI];
    
}

-(void)RequestWithLocation{
    
    NSDictionary *parametDic = @{@"uid":LoveDriverID};
    
    [NetMethod Post:FBHRequestUrl(kUrl_place_list) parameters:parametDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [dic[@"code"] intValue];
        
//        NSLog(@"%@",Dic);
        
        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            
            if( [listArray isKindOfClass:[NSNull class]] ){
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                return ;
            }
            
            
            [self.addresses removeAllObjects];
           
            for (NSDictionary *InfoDic in listArray) {
               
                YGAddressM *model = [[YGAddressM alloc]init];

                model.New_place = InfoDic[@"new_place"];

                model.New_nickname =InfoDic[@"new_nickname"];
                
                model.New_mobile = InfoDic[@"new_mobile"];

                model.LocationID = InfoDic[@"id"];
                
                model.ctime = InfoDic[@"ctime"];
                
                model.New_code = InfoDic[@"new_code"];
                
                model.status = InfoDic[@"status"];
                
                model.is_default = InfoDic[@"is_default"];
                
                
                [self.addresses addObject:model];
                
            }
            
            [self.TableView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
//    proSelectCellIndex = 0;

}

-(void)LoadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight-44 - 64)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];

    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    
//    self.TableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    self.TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:self.TableView];
    
    
    
    UIButton *addAddress = [UIButton buttonWithType:UIButtonTypeCustom];
    
    addAddress.frame = CGRectMake(0, YzHeight - 44 - 64 - SafeAreaBottom, YzWidth, 44);
    
    [addAddress addTarget:self action:@selector(tapAddArress) forControlEvents:UIControlEventTouchUpInside];
    
    [addAddress setTitle:@"添加新地址" forState:UIControlStateNormal];
    
    addAddress.backgroundColor = Green;
    
    [addAddress setTintColor:[UIColor whiteColor]];
    
    [self.view addSubview:addAddress];

    
}

- (void)setOriginal:(YGAddressM *)model success:(void(^)(BOOL isOk))success{


    [self RequestWithLocation];

}



- (void)getNewAddress{

    proSelectCellIndex = 0;
            
    [self.TableView reloadData];
    
    proSelectCellIndex = 0;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.addresses.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzLocationCell *cell = [YzLocationCell cellWithTableView:tableView];
    
    cell.delegate = self;
    
    YGAddressM *address = self.addresses[indexPath.row];
    
    cell.addressM = address;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YGAddressM *address = self.addresses[indexPath.row];
    CGFloat textW = YzWidth - 60;
    
    CGSize size = CGSizeMake(textW, MAXFLOAT);
    //翻页模式
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14], NSParagraphStyleAttributeName:paragraphStyle};
    
    // 获取大小
    CGSize textSize = [address.New_place boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil].size;
    
    return textSize.height + 110;
    
}

- (CGFloat)getHeight:(YGAddressM *)addressM{
    
    // bounding Size
    CGFloat textW = YzWidth - 60;
    
    CGSize size = CGSizeMake(textW, MAXFLOAT);
    
    // bounding Attribute
    //翻页模式
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14], NSParagraphStyleAttributeName:paragraphStyle};
    
    // 获取大小
    
//    addressM.New_place = ddd;
    
    CGSize textSize = [addressM.New_place boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil].size;

//    NSLog(@"cell  .frame   %.f", textSize.height);
    
    return textSize.height + 110;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YGAddressM * addressModel = self.addresses[indexPath.row];
    if (self.isAccount) {
        if ([self.delegate respondsToSelector:@selector(GetMylocation:index:)]) {
            [self.delegate GetMylocation:addressModel index:indexPath];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)addressCellDidClickedBtnWith:(YzLocationCell *)addressCell addressModel:(YGAddressM *)addressModel index:(CellBtnTouchIndex)index{

    switch (index) {
            
        case CellBtnTouchIndexSure:{
            
            __block YzMyLocationVC *copy_self = self;
            
            [self setOriginal:addressModel success:^(BOOL isOk) {
                
                if (isOk) {
                    
                    [copy_self.navigationController popViewControllerAnimated:YES];
                }
                
            
            }];
            
        }
//                [self setOriginal:addressModel];
            break;
            
        case CellBtnTouchIndexEdit:{
            // 跳转到编辑地址界面
            
            YzSetAddressVC *addAddress = [[YzSetAddressVC alloc]init];
            
            addAddress.address = addressModel;
            
            [self.navigationController pushViewController:addAddress animated:YES];
            
        }
            
            break;
            
            
        case CellBtnTouchIndexDel:
        {

            self.model = addressModel;
            
                        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确定删除默认地址么" delegate:self cancelButtonTitle:@"取消"   otherButtonTitles:@"确定", nil];
            
                        alertview.tag = 222;
            
                        [alertview show];
            //
        }
            break;
        default:
            break;
            
    }

}


//删除地址

- (void)deleteAddress:(YGAddressM *)model{
    
    [NetMethod Post:FBHRequestUrl(@"/APP/Member/placeDel") parameters:@{@"uid":LoveDriverID,@"id":model.LocationID} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [dic[@"code"] intValue];
        
        if( code == 200 ){
            
            [self.addresses removeObject:model];
            
            [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
            
        
            [self RequestWithLocation];
      
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];

}

//添加地址
-(void)tapAddArress{
    
    YzSetAddressVC * vc =[YzSetAddressVC new];
    vc.type =@"1";
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        
    }else{
        
         [self deleteAddress:self.model];
    }
}

-(void)viewWillAppear:(BOOL)animated{

    [self RequestWithLocation];
}


@end
