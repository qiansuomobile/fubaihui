//
//  YzSetAddressVC.h
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YGAddressM.h"
#import <Foundation/Foundation.h>



@interface YzSetAddressVC : UIViewController

@property (nonatomic , strong) YGAddressM *address;

@property (nonatomic, strong) UITextField *nameT;

@property (nonatomic, strong) UITextField *phoneT;

@property (nonatomic, strong) UITextField *areaT;

@property (nonatomic, strong) UITextField *detailAreaT;

@property (nonatomic, strong) NSString *type;

@end
