//
//  YzSetAddressVC.m
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ZYLPickerView.h"

#import "YzSetAddressVC.h"

#define Nickname [[NSUserDefaults standardUserDefaults] objectForKey:@"nickname"]


@interface YzSetAddressVC ()<UITextFieldDelegate>


@property (nonatomic , strong)UIView *StarView;

@property (strong, nonatomic) UIButton * confirmButton;

@property (strong, nonatomic) NSArray *imageName;

@property (nonatomic , copy ) NSString *CityName;


// 确认按钮
@property (nonatomic, weak) UIButton *sureBtn;

// 地址View
@property (nonatomic, strong) ZYLPickerView *pickerAddess;

// 地址数据表
@property (nonatomic, strong) NSArray *plist;

//@property (nonatomic, strong) UIToolbar *toolBar;
//
@property (nonatomic , strong) UIView *EditView;

@property (nonatomic , assign) BOOL isBool;


@end




@implementation YzSetAddressVC





- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.isBool = YES;
    
    //    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"编辑地址";
    
    [self AddAderssView];
    
    
}

-(void)AddAderssView{
    
    _EditView = [UIView new];
    
    _EditView.frame = CGRectMake(0, 10, YzWidth, 45 *4 +40);
    
    _EditView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_EditView];
    
    
    NSArray *NameArray = @[@"收件人:",@"手机号:",@"地区:",@"详细地址:"];
    
    for (int i = 0; i<4; i ++) {
        
        
        //        if (i != 3) {
        
        UILabel *Name = [UILabel new];
        
        Name.frame = CGRectMake(10, 0 +i *45, 70, 45);
        
        [_EditView addSubview:Name];
        
        Name.text = NameArray[i];
        
        Name.font = [UIFont systemFontOfSize:14];
        //        }
        
        
        UILabel *line = [UILabel new];
        
        line.frame = CGRectMake(0, 45 + i*45, YzWidth, .5);
        
        [_EditView addSubview:line];
        
        line.backgroundColor = [UIColor grayColor];
        
        line.alpha = .4;
        
    }
    
    
    [self YZsetUpField];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(YZsure)];
    self.navigationItem.rightBarButtonItem.title = @"保存";
}

-(void)YZsure{
    
    //    保存数据
    
    BOOL isPhoneNumber = [VerifyPhoneNumber checkTelNumber:_phoneT.text];
    
    
    if (isPhoneNumber) {
        
    }else{
        
        [MBProgressHUD showSuccess:@"请输入正确手机号" toView:self.view];
        
        return;
    }
    
    
    if ([VerifyPhoneNumber isBlankString:_nameT.text]) {
        
        
        [MBProgressHUD showSuccess:@"请输入用户名" toView:self.view];
        
        return;
    }
    
    
    if ([VerifyPhoneNumber isBlankString:_areaT.text]) {
        
        
        [MBProgressHUD showSuccess:@"请输入地区" toView:self.view];
        
        return;
    }
    
    
    if ([VerifyPhoneNumber isBlankString:_detailAreaT.text]) {
        
        
        [MBProgressHUD showSuccess:@"请输入详细地址" toView:self.view];
        
        return;
    }
    
    NSString *location = [NSString stringWithFormat:@"%@ %@",_areaT.text,_detailAreaT.text];
    
    NSDictionary *LocationDic;
    NSString *url;
    if ([self.type isEqualToString:@"1"]) {
        url = kUrl_place_add;
        LocationDic = @{@"uid":LoveDriverID,@"new_nickname":_nameT.text,@"new_place":location,@"new_mobile":_phoneT.text,@"new_code":@""};
    }else{
        url = kUrl_place_Edit;
        LocationDic = @{@"uid":LoveDriverID,@"new_nickname":_nameT.text,@"new_place":location,@"new_mobile":_phoneT.text,@"id":self.address.LocationID};
        
    }
    
    [NetMethod Post:FBHRequestUrl(url) parameters:LocationDic success:^(id responseObject) {
        
        NSDictionary *DIC = responseObject;
        
        
        //NSLog(@"%@",DIC);
        
        int code = [DIC[@"code"] intValue];
        
        
        if ( code == 200) {
            
            [self performSelector:@selector(BackToViewcontrol) withObject:nil afterDelay:1.5];
            
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            
        }else{
            
            [MBProgressHUD showSuccess:DIC[@"msg"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}
- (void)YZsetUpField{
    
    // 收件人
    UITextField *nameT = [[UITextField alloc]init];
    
    nameT.frame = CGRectMake(60, 0 + 5, YzWidth - 70, 45);
    
    nameT.delegate = self;

    nameT.text = self.address.New_nickname;
    nameT.placeholder = @"请输入收件人";
    
    nameT.textAlignment = NSTextAlignmentRight;
    
    nameT.font = [UIFont systemFontOfSize:16];
    
    [_EditView addSubview:nameT];
    
    _nameT = nameT;
    
    
    
    
    // 手机号
    
    UITextField *phoneT = [[UITextField alloc]init];
    
    phoneT.frame = CGRectMake(80, 45 +5, YzWidth - 90, 45);
    
    phoneT.delegate = self;

    phoneT.text = self.address.New_mobile;
    phoneT.placeholder = @"请输入手机号";
    phoneT.textAlignment = NSTextAlignmentRight;
    
    phoneT.font = [UIFont systemFontOfSize:16];
    
    [_EditView addSubview:phoneT];
    
    _phoneT = phoneT;
    
    
    
    UITextField *areaT = [[UITextField alloc]init];
    
    areaT.frame = CGRectMake(80, 45  + 45 +5, YzWidth - 90, 45);
    
    areaT.font = [UIFont systemFontOfSize:14];
    
    areaT.delegate = self;
    
    NSArray *b = [self.address.New_place componentsSeparatedByString:@" "];
    
    NSString *a3 = [b objectAtIndex:b.count-1];
    NSString *a1;
    if (b.count==2) {
        a1= [NSString stringWithFormat:@"%@",[b objectAtIndex:0]];
    }else{
        a1 = [NSString stringWithFormat:@"%@%@",[b objectAtIndex:0],[b objectAtIndex:1]];
    }
    if (self.address.New_place.length!=0) {
        areaT.text = a1;
    }
    areaT.placeholder = @"请选择地址";
    
    areaT.textAlignment = NSTextAlignmentRight;
    
    [_EditView addSubview:areaT];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, YzWidth - 90, 45)];
    [areaT addSubview:btn];
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    
    _areaT = areaT;
    
    
    
    // 详细地址
    
    UITextField *detailareaT = [[UITextField alloc]init];
    
    detailareaT.frame = CGRectMake(80, 45 + 45 + 45 +5, YzWidth - 90, 45);
    
    detailareaT.font = [UIFont systemFontOfSize:14];
    detailareaT.delegate = self;

    detailareaT.delegate = self;
    if (self.address.New_place.length!=0) {
        detailareaT.text = a3;
    }
    detailareaT.placeholder = @"请输入详细地址";
    detailareaT.textAlignment = NSTextAlignmentRight;
    
    [_EditView addSubview:detailareaT];
    
    _detailAreaT = detailareaT;
    
}

-(void)click{
    
    [_nameT resignFirstResponder];
    [_phoneT resignFirstResponder];
    [_detailAreaT resignFirstResponder];
    
    if (self.isBool) {
        
        self.StarView = [[UIView alloc]init];
        
        self.StarView.frame = CGRectMake(0,YzHeight , YzWidth, 160);
        
        [self.view addSubview:self.StarView];
        
        self.pickerAddess = [[ZYLPickerView alloc]initWithFrame:CGRectMake(0,0 , YzWidth, 160)];
        
        [self.StarView addSubview:self.pickerAddess];
        
        
        [UIView animateWithDuration:.5 animations:^{
            
            self.StarView.frame = CGRectMake(0, YzHeight - 240, YzWidth, 160);
            
            
        } completion:^(BOOL finished) {
            
            
        }];
        
        __weak __typeof(self) weakself = self;
        
        self.pickerAddess.SelectBlock = ^(NSString *proCityName){
            
            if (proCityName != nil) {
                
                NSRange range = [proCityName rangeOfString:@" "];
                
                NSString *str = [proCityName substringToIndex:range.location];
                
                NSString *strt = [proCityName substringFromIndex:range.location + range.length];
                
                if ([str isEqualToString:strt]) {
                    
                    proCityName = str;
                    
                }
                
                weakself.areaT.text = proCityName;
                
            }
            
            //            weakself.isBool = YES;
            
        };
        
    }else{
        
        [self disMiss];
    }
    
    
}



#pragma mark - 删除视图
-(void)disMiss{
    
    
    [UIView animateWithDuration:.5 animations:^{
        
        self.StarView.frame = CGRectMake(0, YzHeight , YzWidth, 160);
        
        
    } completion:^(BOOL finished) {
        
        [self.StarView removeFromSuperview];
        
        self.isBool = YES;
        
    }];
    
}


- (void)keyboardWillAppear:(NSNotification *)notification{
    
    
    CGRect frame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    
    if (frame.origin.y == YzHeight){
        
        // 未弹出键盘
        [UIView animateWithDuration:duration animations:^{
            self.view.transform = CGAffineTransformIdentity;
        }];
        
        
    }else{
        
        //         动画弹出bottomToolBar
        //                [UIView animateWithDuration:duration animations:^{
        //
        //                    self.view.transform = CGAffineTransformMakeTranslation(0, - 80);
        //
        //                }];
        
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == self.areaT) {
        
        //   [self notifKeyboard];
        
        
//        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];

       
        
//        [self click];
        
        self.isBool = NO;

    }else{
        
        [self disMiss];
        
    }
    
    //    if (textField == self.detailAreaT) {
    //
    //        [self notifKeyboard];
    //
    //
    //        [self disMiss];
    //
    //
    //
    //    }else{
    //
    //        //
    //    }
    
}


- (void)notifKeyboard{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
    [self disMiss];
    
}


-(void)BackToViewcontrol{
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
