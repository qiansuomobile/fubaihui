//
//  AddCommentViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCommentViewController : UIViewController
@property (nonatomic,strong) NSString *picStr;
@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *gid;
@end
