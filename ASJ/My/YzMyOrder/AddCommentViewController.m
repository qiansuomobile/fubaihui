//
//  AddCommentViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AddCommentViewController.h"
#import "CommentTitleTableViewCell.h"
#import "CustomTextView.h"
#import "CommentTableViewCell.h"

@interface AddCommentViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) CustomTextView *textView;
@end

@implementation AddCommentViewController

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

-(void)createUI{
    self.title = @"添加评价";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initBottomView];
    [self initTableView];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-55*KHeight);
    }];
}

-(void)initBottomView{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectZero];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.height.mas_equalTo(55*KHeight);
    }];
    
    UIButton *commentBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    [commentBtn setTitle:@"发表评价" forState:UIControlStateNormal];
    [commentBtn setBackgroundColor:[UIColor orangeColor]];
    [commentBtn addTarget:self action:@selector(addComment) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:commentBtn];
    [commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView.mas_right).offset(0);
        make.top.equalTo(bottomView.mas_top).offset(0);
        make.bottom.equalTo(bottomView.mas_bottom).offset(0);
        make.width.mas_equalTo(100*Kwidth);
    }];
    
}

-(void)addComment{
    if (self.textView.text.length == 0) {
        [MBProgressHUD showSuccess:@"您还没有添加评论" toView:self.view];
    }else{
        NSDictionary *dic = @{@"user_id":LoveDriverID,@"content":self.textView.text,@"gid":self.gid};
        [NetMethod Post:LoveDriverURL(@"APP/Order/add_pl") parameters:dic success:^(id responseObject) {
            
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
            if ([responseObject[@"code"] isEqual:@200]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            
        } failure:^(NSError *error) {
            [MBProgressHUD showSuccess:NetProblem toView:self.view];
        }];
    }
}


#pragma mark --- UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        CommentTitleTableViewCell *cell = [[CommentTitleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        [cell.imageV sd_setImageWithURL:[NSURL URLWithString:self.picStr]];
        cell.titleL.text = self.titleStr;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        CommentTableViewCell *cell = [[CommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textView.returnKeyType = UIReturnKeyDone;
        cell.textView.delegate = self;
        self.textView = cell.textView;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 80*KHeight;
    }
    return 240*KHeight;
}

#pragma mark --- UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    _textView.placeholderL.hidden = YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        _textView.placeholderL.hidden = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
