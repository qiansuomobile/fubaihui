//
//  CommentTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextView.h"

@interface CommentTableViewCell : UITableViewCell
@property (nonatomic,strong) CustomTextView *textView;
@end
