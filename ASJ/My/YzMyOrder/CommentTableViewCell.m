//
//  CommentTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *titleL = [[UILabel alloc] init];
    titleL.text = @"添加评价";
    titleL.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*Kwidth);
    }];
    
    _textView = [[CustomTextView alloc] initWithLimit:NO];
    _textView.placeholder = @"在此处添加评论哦~";
    _textView.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(titleL.mas_bottom).offset(5*KHeight);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*Kwidth);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
