//
//  CommentTitleTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTitleTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleL;

@end
