//
//  CommentTitleTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/19.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CommentTitleTableViewCell.h"

@implementation CommentTitleTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _imageV = [[UIImageView alloc] init];
    //_imageV.image = [UIImage imageNamed:@"chaipin"];
    [self addSubview:_imageV];
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(50*KHeight);
        make.width.mas_equalTo(50*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    //_titleL.text = @"我爱司机产品名称我爱司机产品名称我爱司机产品名称";
    _titleL.font = [UIFont systemFontOfSize:15*KHeight];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-60*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
