//
//  FirstOrderDetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstOrderDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *orderNumL;
@property (nonatomic,strong) UILabel *createTimeL;

@end
