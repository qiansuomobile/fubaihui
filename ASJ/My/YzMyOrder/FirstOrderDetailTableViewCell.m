//
//  FirstOrderDetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "FirstOrderDetailTableViewCell.h"

@implementation FirstOrderDetailTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _orderNumL = [[UILabel alloc] init];
    _orderNumL.text = @"订单编号:1521546845612548";
    _orderNumL.font = [UIFont systemFontOfSize:14*KHeight];
    _orderNumL.textColor = RGBColor(140, 140, 140);
    [self addSubview:_orderNumL];
    [_orderNumL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
    }];
    
    _createTimeL = [[UILabel alloc] init];
    _createTimeL.text = @"创建时间:2016-8-16 16:39";
    _createTimeL.font = [UIFont systemFontOfSize:14*KHeight];
    _createTimeL.textColor = RGBColor(140, 140, 140);
    [self addSubview:_createTimeL];
    [_createTimeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
