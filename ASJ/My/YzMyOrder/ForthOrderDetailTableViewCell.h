//
//  ForthOrderDetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForthOrderDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *sortL;
@property (nonatomic,strong) UILabel *payL;
@property (nonatomic,strong) UILabel *priceL;


@end
