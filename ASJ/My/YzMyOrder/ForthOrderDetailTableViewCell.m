//
//  ForthOrderDetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ForthOrderDetailTableViewCell.h"

@implementation ForthOrderDetailTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *titleL = [[UILabel alloc] init];
    titleL.text = @"订单总价支付明细";
    titleL.font = [UIFont systemFontOfSize:15*KHeight];
    titleL.textAlignment = NSTextAlignmentRight;
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(10*KHeight);
    }];
    
    UILabel *sortL = [[UILabel alloc] init];
    sortL.text = @"金A币使用";
    sortL.font = [UIFont systemFontOfSize:15*KHeight];
    sortL.textAlignment = NSTextAlignmentRight;
    [self addSubview:sortL];
    [sortL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(titleL.mas_bottom).offset(5*KHeight);
    }];
    
    _sortL = [[UILabel alloc] init];
    _sortL.text = @"¥5.00";
    _sortL.font = [UIFont systemFontOfSize:15*KHeight];
    _sortL.textAlignment = NSTextAlignmentRight;
    [self addSubview:_sortL];
    [_sortL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(titleL.mas_bottom).offset(5*KHeight);
    }];
    
    
    UILabel *payL = [[UILabel alloc] init];
    payL.text = @"实付款";
    payL.font = [UIFont systemFontOfSize:15*KHeight];
    payL.textAlignment = NSTextAlignmentRight;
    [self addSubview:payL];
    [payL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(sortL.mas_bottom).offset(3*KHeight);
    }];
    
    _payL = [[UILabel alloc] init];
    _payL.text = @"¥489.00";
    _payL.font = [UIFont systemFontOfSize:15*KHeight];
    _payL.textAlignment = NSTextAlignmentRight;
    [self addSubview:_payL];
    [_payL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(sortL.mas_bottom).offset(3*KHeight);
    }];
    
    UILabel *orderL = [[UILabel alloc] init];
    orderL.text = @"订单总价(含运费)";
    orderL.font = [UIFont systemFontOfSize:17*KHeight];
    orderL.textAlignment = NSTextAlignmentRight;
    [self addSubview:orderL];
    [orderL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(payL.mas_bottom).offset(3*KHeight);
    }];
    
    _priceL = [[UILabel alloc] init];
    _priceL.text = @"¥494.00";
    _priceL.font = [UIFont systemFontOfSize:17*KHeight];
    _priceL.textAlignment = NSTextAlignmentRight;
    _priceL.textColor = [UIColor orangeColor];
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(payL.mas_bottom).offset(3*KHeight);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
