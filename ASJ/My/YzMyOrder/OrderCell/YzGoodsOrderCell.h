//
//  YzGoodsOrderCell.h
//  ASJ
//
//  Created by Dororo on 2019/7/5.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YzMyoderModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YzOrderClickStatus){
    YzOrderClickStatusPay,              //付款
    YzOrderClickStatusCancel,           //取消订单
    YzOrderClickStatusRefund,           //申请退款
    YzOrderClickStatusCancelRefund,     //取消申请退款
    YzOrderClickStatusLogistics,        //查看物流
    YzOrderClickStatusConfirm,          //确认收货
    YzOrderClickStatusAfterSale,        //申请售后
    YzOrderClickStatusAssess            //评价
};

@protocol YzGoodsOrderCellDelegate <NSObject>
- (void)showOrderStatusDetailWithStatus:(YzOrderClickStatus)status orderModel:(YzMyoderModel *)model;
@end

@interface YzGoodsOrderCell : UITableViewCell

//商家店铺名称
@property (weak, nonatomic) IBOutlet UILabel *mallLabel;

//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgView;

//商品名称
@property (weak, nonatomic) IBOutlet UILabel *goodsName;

//商品价格
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

//商品数量
@property (weak, nonatomic) IBOutlet UILabel *goodsNumLabel;

//商品总数和总价
@property (weak, nonatomic) IBOutlet UILabel *goodsTotalLabel;

//商品状态
@property (weak, nonatomic) IBOutlet UILabel *goodsStatusLabel;


@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;

@property (weak, nonatomic) IBOutlet UIImageView *payTypeImageView;


@property (weak, nonatomic) IBOutlet UIButton *leftButton;

@property (weak, nonatomic) IBOutlet UIButton *rightButton;


@property (nonatomic, weak)id<YzGoodsOrderCellDelegate>delegate;

// 订单按钮点击状态
@property (nonatomic, assign)YzOrderClickStatus clickStatus;



/**
 订单数据模型
 */
@property (nonatomic, strong)YzMyoderModel *orederModel;



@end

NS_ASSUME_NONNULL_END
