//
//  YzGoodsOrderCell.m
//  ASJ
//
//  Created by Dororo on 2019/7/5.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzGoodsOrderCell.h"

@interface YzGoodsOrderCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBtnLayoutConstraint;

@end

@implementation YzGoodsOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)drawRect:(CGRect)rect{
    
    _leftButton.layer.masksToBounds = YES;
    _leftButton.layer.cornerRadius = 9.0f;
    _leftButton.layer.borderColor = RGBColor(218, 219, 220).CGColor;
    _leftButton.layer.borderWidth = 0.5f;
    
    _rightButton.layer.masksToBounds = YES;
    _rightButton.layer.cornerRadius = 9.0f;
    _rightButton.layer.borderColor = RGBColor(237, 121, 161).CGColor;
    _rightButton.layer.borderWidth = 0.5f;
    
    _goodsImgView.layer.cornerRadius = 5.0f;
}

- (void)setOrederModel:(YzMyoderModel *)orederModel{
    _orederModel = orederModel;
    
    _mallLabel.text     =   _orederModel.store_name;
    _goodsStatusLabel.text= _orederModel.status_m;
    
    _goodsName.text     =   _orederModel.goods_name;
    _priceLabel.text    =   [NSString stringWithFormat:@"%@",_orederModel.goods_price];
    _goodsNumLabel.text =   [NSString stringWithFormat:@"x%@",_orederModel.goods_num];
    _goodsTotalLabel.text=  [NSString stringWithFormat:@"共%@件商品，合计：¥%@",_orederModel.goods_num,_orederModel.pay_price];
    
    _orderNumLabel.text = [NSString stringWithFormat:@"订单号：%@",_orederModel.ordernum];
    
    [_payTypeImageView sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(_orederModel.symbol)]];
    [_goodsImgView sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(_orederModel.goods_img)]];
    
    [self orderButtonStatus:_orederModel.status_o];
}

- (void)orderButtonStatus:(NSInteger)status{
    
    NSString *leftBtnTitle;
    NSString *rightBtnTitle;
    if (status == 0) {
        //未支付
        leftBtnTitle    = @"取消订单";
        rightBtnTitle   = @"付款";
        _rightButton.hidden = NO;
        _leftButton.hidden = NO;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == 1){
        //待发货
        leftBtnTitle    = @"申请退款";
        _rightButton.hidden = YES;
        _leftButton.hidden = NO;
        _leftBtnLayoutConstraint.constant = 15.0f;
    }else if(status == 2){
        //已发货
        leftBtnTitle    = @"查看物流";
        rightBtnTitle   = @"确认收货";
        _rightButton.hidden = NO;
        _leftButton.hidden = NO;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == 3){
        //交易成功，未评价
        rightBtnTitle   = @"评价";
        _rightButton.hidden = NO;
        _leftButton.hidden = YES;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == 4){
        //交易成功，已评价
        rightBtnTitle   = @"订单完成";
        _rightButton.hidden = NO;
        _leftButton.hidden = YES;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == 5){
        //已申请退款
        rightBtnTitle   = @"已申请退款";
        _rightButton.hidden = NO;
        _leftButton.hidden = YES;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == 6){
        //申请退款成功
        rightBtnTitle   = @"退款成功";
        _rightButton.hidden = NO;
        _leftButton.hidden = YES;
        _leftBtnLayoutConstraint.constant = 90.0f;
    }else if(status == -1){
        leftBtnTitle    = @"已取消";
        _rightButton.hidden = YES;
        _leftButton.hidden = NO;
        _leftBtnLayoutConstraint.constant = 15.0f;
    }else if(status == -2){
        leftBtnTitle    = @"交易关闭";
        _rightButton.hidden = YES;
        _leftButton.hidden = NO;
        _leftBtnLayoutConstraint.constant = 15.0f;
    }
    
    [_leftButton setTitle:leftBtnTitle forState:UIControlStateNormal];
    [_rightButton setTitle:rightBtnTitle forState:UIControlStateNormal];
}
- (IBAction)leftButtonClick:(UIButton *)sender {
    NSLog(@"left btn clcik");
    if (_orederModel.status_o == 0) {
        //未支付
        _clickStatus = YzOrderClickStatusCancel;    //取消订单
        [self orderBtnClick];
    }else if(_orederModel.status_o == 1){
        //待发货
        _clickStatus = YzOrderClickStatusRefund;    //申请退款
        [self orderBtnClick];
    }else if(_orederModel.status_o == 2){
        //已发货
        _clickStatus = YzOrderClickStatusLogistics; //查看物流
        [self orderBtnClick];
    }
}

- (IBAction)rightButtonClick:(UIButton *)sender {
    NSLog(@"right btn clcik");
    if (_orederModel.status_o == 0) {
        //未支付
        _clickStatus = YzOrderClickStatusPay;       //付款
        [self orderBtnClick];
    }else if(_orederModel.status_o == 2){
        //已发货
        _clickStatus = YzOrderClickStatusConfirm;   //确认收货
        [self orderBtnClick];
    }else if(_orederModel.status_o == 3) {
        //交易成功 未评价
        _clickStatus = YzOrderClickStatusAssess;    //评价
        [self orderBtnClick];
    }
}

- (void)orderBtnClick{
    
    NSLog(@"order id %@",_orederModel.order_id);
    
    if ([_delegate respondsToSelector:@selector(showOrderStatusDetailWithStatus:orderModel:)]) {
        [_delegate showOrderStatusDetailWithStatus:_clickStatus orderModel:_orederModel];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
