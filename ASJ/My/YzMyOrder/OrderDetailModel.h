//
//  OrderDetailModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/10.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailModel : NSObject

@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *address;

//订单号
@property (nonatomic,copy) NSString *order_sn;
//下单时间
@property (nonatomic,copy) NSString *add_time;

@property (nonatomic,copy) NSString *goods_id;
@property (nonatomic,copy) NSString *goods_img;
@property (nonatomic,copy) NSString *goods_name;
@property (nonatomic,copy) NSString *goods_num;
@property (nonatomic,copy) NSString *goods_price;

//订单状态
@property (nonatomic,copy) NSString *status_o;
//订单状态描述
@property (nonatomic,copy) NSString *status_m;

//商品总价
@property (nonatomic,copy) NSString *total_price;
//优惠金额
@property (nonatomic,copy) NSString *discount;
//支付金额
@property (nonatomic,copy) NSString *pay_price;

//符号
@property (nonatomic,copy) NSString *symbol;

//备注
@property (nonatomic, copy)NSString *remark;


@end
