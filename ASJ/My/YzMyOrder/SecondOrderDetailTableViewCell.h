//
//  SecondOrderDetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondOrderDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *addressImage;
@property (nonatomic,strong) UILabel *nameL;
@property (nonatomic,strong) UILabel *phoneL;
@property (nonatomic,strong) UILabel *addressL;

@end
