//
//  SecondOrderDetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "SecondOrderDetailTableViewCell.h"

@implementation SecondOrderDetailTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _addressImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"didian"]];
    [self addSubview:_addressImage];
    [_addressImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(30*Kwidth);
        make.height.mas_equalTo(30*Kwidth);
    }];
    
    _nameL = [[UILabel alloc] init];
    _nameL.text = @"收货人  李四";
    _nameL.textColor = RGBColor(120, 120, 120);
    _nameL.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:_nameL];
    [_nameL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_addressImage.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(15*KHeight);
    }];
    
    _phoneL = [[UILabel alloc] init];
    _phoneL.text = @"13120018540";
    _phoneL.textAlignment = NSTextAlignmentRight;
    _phoneL.textColor = RGBColor(120, 120, 120);
    _phoneL.font = [UIFont systemFontOfSize:16*KHeight];
    [self addSubview:_phoneL];
    [_phoneL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(15*KHeight);
    }];
    
    _addressL = [[UILabel alloc] init];
    _addressL.text = @"北京 北京市 朝阳区 管庄 朝阳区朝阳路8号朗庭大厦B座317室";
    _addressL.textColor = RGBColor(120, 120, 120);
    _addressL.font = [UIFont systemFontOfSize:16*KHeight];
    _addressL.lineBreakMode = NSLineBreakByCharWrapping;
    _addressL.numberOfLines = 2;
    [self addSubview:_addressL];
    [_addressL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_addressImage.mas_right).offset(10*Kwidth);
        make.top.equalTo(_nameL.mas_bottom).offset(5*KHeight);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
