//
//  ThirdOrderDetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailModel.h"

@interface ThirdOrderDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *contentL;
@property (nonatomic,strong) UILabel *priceL;
//@property (nonatomic,strong) UILabel *codeL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) UIButton *cancelBtn;
@property (nonatomic,strong) UIButton *judgeBtn;

@property (nonatomic,copy) NSString *imageStr;
@property (nonatomic,strong) OrderDetailModel *model;


@end
