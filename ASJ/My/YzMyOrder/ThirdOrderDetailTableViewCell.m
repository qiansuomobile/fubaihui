//
//  ThirdOrderDetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ThirdOrderDetailTableViewCell.h"

@implementation ThirdOrderDetailTableViewCell

-(void)setModel:(OrderDetailModel *)model{
    self.titleL.text = model.goods_name;
    
    self.priceL.text = [NSString stringWithFormat:@"¥%@",model.goods_price];
    
    self.countL.text = [NSString stringWithFormat:@"x%@",model.goods_num];
    
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(model.goods_img)]];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    [self addSubview:_imageV];
    [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.width.mas_equalTo(80*KHeight);
        make.height.mas_equalTo(80*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"--------------";
    _titleL.font = [UIFont systemFontOfSize:13*KHeight];
    _titleL.numberOfLines = 2;
    _titleL.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.right.equalTo(self.mas_right).offset(-100*Kwidth);
    }];
    
    _contentL = [[UILabel alloc] init];
    _contentL.text = @"-------------";
    _contentL.font = [UIFont systemFontOfSize:13*KHeight];
    _contentL.textColor = RGBColor(160, 160, 160);
    _contentL.numberOfLines = 2;
    _contentL.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:_contentL];
    [_contentL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageV.mas_right).offset(10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-100*Kwidth);
    }];
    _contentL.hidden = YES;
    
    
    _priceL = [[UILabel alloc] init];
    _priceL.text = @"¥-----";
    _priceL.textAlignment = NSTextAlignmentRight;
    _priceL.font = [UIFont systemFontOfSize:13*KHeight];
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
    }];
    
    _countL = [[UILabel alloc] init];
    _countL.text = @"x1";
    _countL.textColor = RGBColor(160, 160, 160);
    _countL.textAlignment = NSTextAlignmentRight;
    _countL.font = [UIFont systemFontOfSize:12*KHeight];
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(_priceL.mas_bottom).offset(15*KHeight);
    }];
    
    _cancelBtn = [[UIButton alloc] init];
    [_cancelBtn setTitle:@"申请退货" forState:UIControlStateNormal];
    [_cancelBtn setBackgroundColor:[UIColor whiteColor]];
    [_cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _cancelBtn.layer.masksToBounds = YES;
    _cancelBtn.layer.cornerRadius = 2.0*KHeight;
    _cancelBtn.layer.borderWidth = 0.5;
    _cancelBtn.layer.borderColor = [RGBColor(200, 200, 200) CGColor];
    _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:12*KHeight];
    [self addSubview:_cancelBtn];
    
//    [_cancelBtn addTarget:self action:@selector(CancelOrderID) forControlEvents:UIControlEventTouchUpInside];
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-8*KHeight);
        make.height.mas_equalTo(30*KHeight);
        make.width.mas_equalTo(55*Kwidth);
    }];
    
    _judgeBtn = [[UIButton alloc] init];
    [_judgeBtn setTitle:@"评价" forState:UIControlStateNormal];
    [_judgeBtn setBackgroundColor:[UIColor whiteColor]];
    [_judgeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _judgeBtn.layer.masksToBounds = YES;
    _judgeBtn.layer.cornerRadius = 2.0*KHeight;
    _judgeBtn.layer.borderWidth = 0.5;
    _judgeBtn.layer.borderColor = [RGBColor(200, 200, 200) CGColor];
    _judgeBtn.titleLabel.font = [UIFont systemFontOfSize:12*KHeight];
    [self addSubview:_judgeBtn];
    [_judgeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.cancelBtn.mas_left).offset(-10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-8*KHeight);
        make.height.mas_equalTo(30*KHeight);
        make.width.mas_equalTo(55*Kwidth);
    }];
}


-(void)CancelOrderID{
    
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
