//
//  YzGoldOrderViewController.m
//  ASJ
//
//  Created by Ss H on 2018/4/4.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "YzGoldOrderViewController.h"
#import "YzOrderWaitGoVc.h"

#import "YzOrderWaitPayVc.h"

#import "YzOrderWaitGetVc.h"

#import "YzOrderWaitTalkVC.h"

#import "YzOrderViewController.h"

#import "YzDriverOrderVC.h"

#import "YzgoldOneVC.h"

#import "YzgoldThreeVC.h"

#import "YzgoldForVC.h"

@interface YzGoldOrderViewController ()

@end

@implementation YzGoldOrderViewController

-(instancetype)init{
    
    if (self = [super init]) {
        
        self.titles =@[@"全部",@"待发货",@"待收货"];
        
        self.viewControllerClasses = @[[YzgoldOneVC class],[YzgoldThreeVC class],[YzgoldForVC class]];
        
        //        ,@"待评论"
        //        ,[YzOrderWaitTalkVC class]
        
        self.menuItemWidth = YzWidth / 3;
        
        //        self.menuView.WMMenuItem
        
        self.menuViewStyle = WMMenuViewStyleLine;
        
        self.progressColor = [UIColor redColor];
        
        //      self.menuBGColor = [UIColor redColor];
    }
    
    return self;
    
}


- (NSArray *)getController{
    
    //    NSArray *titles = @[@"上装", @"下装",@"外套",@"裙子",@"套装"];
    //
    //    self.titles = titles;
    
    Class vc1;
    
    vc1 = [YzgoldOneVC class];
    
    Class vc3;
    
    vc3 = [YzgoldThreeVC class];
    
    Class vc4;
    
    vc4 = [YzgoldForVC class];
    
    //    Class vc5;
    //
    //    vc5 = [YzOrderWaitTalkVC class];
    
    self.menuItemWidth = YzWidth / 3;
    
    self.menuViewStyle = WMMenuViewStyleDefault;

    return @[vc1,  vc3, vc4];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    //    兑换商城
    
    self.viewControllerClasses = [self getController];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    [VerifyPictureURL YZcheckNETwork];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
