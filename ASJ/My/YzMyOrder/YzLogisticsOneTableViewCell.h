//
//  YzLogisticsOneTableViewCell.h
//  ASJ
//
//  Created by Ss H on 2018/4/2.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YzLogisticsOneTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *dianView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
