//
//  YzLogisticsOneTableViewCell.m
//  ASJ
//
//  Created by Ss H on 2018/4/2.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "YzLogisticsOneTableViewCell.h"

@implementation YzLogisticsOneTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.dianView.layer.cornerRadius = 5;
    self.dianView.layer.masksToBounds = YES;
    self.timeLabel.numberOfLines = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
