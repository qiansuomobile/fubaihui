//
//  YzLogisticsTableViewCell.h
//  ASJ
//
//  Created by Ss H on 2018/4/2.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YzLogisticsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *dianView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
