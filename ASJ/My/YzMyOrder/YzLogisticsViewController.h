//
//  YzLogisticsViewController.h
//  ASJ
//
//  Created by Ss H on 2018/4/2.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YzLogisticsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic , strong)UITableView *logisticsTable;
@property(nonatomic , strong)NSString *orderID;
@property(nonatomic , strong)NSString *titleStr;
@property(nonatomic , strong)NSString *timeStr;
@property(nonatomic , strong)NSString *logisticsStr;
@property(nonatomic , strong)NSMutableArray *logisticsArray;

@end
