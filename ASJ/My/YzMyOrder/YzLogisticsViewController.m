//
//  YzLogisticsViewController.m
//  ASJ
//
//  Created by Ss H on 2018/4/2.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "YzLogisticsViewController.h"
#import "YzLogisticsTableViewCell.h"

@interface YzLogisticsViewController ()

@end

@implementation YzLogisticsViewController
{
    UIView *heardView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"物流详情";
    [self initTableView];
    
}
-(void)initTableView{
    _logisticsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight-64) style:UITableViewStylePlain];
    _logisticsTable.delegate = self;
    _logisticsTable.dataSource = self;
    [self.view addSubview:_logisticsTable];
    [self initWithHeardView];
    _logisticsTable.tableHeaderView = heardView;
}
-(void)initWithHeardView
{
    heardView = [[UIView alloc]initWithFrame:CGRectZero];
    heardView.frame = CGRectMake(0, 0, YzWidth, 100);
    heardView.backgroundColor =[UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1];
    UIView *backGroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, 90)];
    backGroundView.backgroundColor = [UIColor whiteColor];
    UILabel *orderTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, YzWidth-20, 30)];
    orderTitle.text = [NSString stringWithFormat:@"订单编号: %@",self.orderID];
    [backGroundView addSubview:orderTitle];
    
    UILabel *logisticsTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, YzWidth-20, 30)];
    logisticsTitle.text = [NSString stringWithFormat:@"物流公司: %@",self.logisticsStr];
    [backGroundView addSubview:logisticsTitle];
    [heardView addSubview:backGroundView];
    [self.view addSubview:heardView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.logisticsArray.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier =@"YzLogisticsTableViewCell";
    YzLogisticsTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell =[[NSBundle mainBundle] loadNibNamed:@"YzLogisticsTableViewCell" owner:self options:nil].lastObject;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 0) {
        cell.dianView.backgroundColor = [UIColor greenColor];
        cell.lineView.hidden = YES;
    }else{
        cell.dianView.backgroundColor = [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1];
        cell.lineView.hidden = NO;
    }
    cell.titleLabel.text = [self.logisticsArray[indexPath.row]objectForKey:@"AcceptStation"];
    cell.timeLabel.text = [self.logisticsArray[indexPath.row]objectForKey:@"AcceptTime"];
    return cell;
}
-(NSString *)timeStr:(NSString *)time
{
    float num = [time floatValue]/1000;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY/MM/dd"];
    
    NSDate*confromTimesp = [NSDate dateWithTimeIntervalSince1970:num];
    NSString*confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
