//
//  YzMyOrderVC.m
//  ASJ
//
//  Created by Jack on 16/9/6.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzOrderWaitGoVc.h"

#import "YzOrderWaitPayVc.h"

#import "YzOrderWaitGetVc.h"

#import "YzOrderWaitTalkVC.h"

#import "YzOrderViewController.h"

#import "YzMyOrderVC.h"


@interface YzMyOrderVC ()
@property (nonatomic, strong) NSArray *menuArray;

@property (nonatomic, strong) NSMutableArray *controllers;
@end

@implementation YzMyOrderVC

-(instancetype)init{
    
    if (self = [super init]) {
        
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.menuItemWidth = [UIScreen mainScreen].bounds.size.width / self.menuArray.count;
        self.titleColorSelected = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
        self.titleColorNormal = [UIColor colorWithRed:0.4 green:0.8 blue:0.1 alpha:1.0];
        self.postNotification = YES;
    }
    
    return self;
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title =@"我的订单";
}

#pragma mark - datasource & delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return self.menuArray.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index{
    return self.menuArray[index];
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView{
    return CGRectMake(0, 0, self.view.frame.size.width, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView{
    CGFloat menuViewHeight = [self pageController:pageController preferredFrameForMenuView:self.menuView].size.height;
    return CGRectMake(0, menuViewHeight, self.view.frame.size.width, self.view.frame.size.height - menuViewHeight);
}

- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}



#pragma mark - get
- (NSArray *)menuArray{
    if (!_menuArray) {
        _menuArray = @[@"全部", @"待付款",@"待发货",@"待收货",@"已完成"];
    }
    return _menuArray;
}

- (NSMutableArray *)controllers{
    if (!_controllers) {
        _controllers = [NSMutableArray array];
        
        YzOrderViewController *orderVC1 = [[YzOrderViewController alloc]init];
        orderVC1.type = YzOrderTypeAll;
        
        YzOrderViewController *orderVC2 = [[YzOrderViewController alloc]init];
        orderVC2.type = YzOrderTypePay;
        
        YzOrderViewController *orderVC3 = [[YzOrderViewController alloc]init];
        orderVC3.type = YzOrderTypeShip;
        
        YzOrderViewController *orderVC4 = [[YzOrderViewController alloc]init];
        orderVC4.type = YzOrderTypeReceipt;
        
        YzOrderViewController *orderVC5 = [[YzOrderViewController alloc]init];
        orderVC5.type = YzOrderTypeEnd;
        
        [_controllers addObject:orderVC1];
        [_controllers addObject:orderVC2];
        [_controllers addObject:orderVC3];
        [_controllers addObject:orderVC4];
        [_controllers addObject:orderVC5];
    }
    return _controllers;
}
@end
