//
//  YzMyoderModel.h
//  ASJ
//
//  Created by Jack on 16/9/6.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzMyoderModel : NSObject

//订单编号
@property (nonatomic, copy) NSString *order_id;

//图片路径
@property (nonatomic, copy) NSString *goods_img;

@property (nonatomic, copy) NSString *goods_id;

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *goods_num;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, copy) NSString *ordernum;


@property (nonatomic , assign) NSInteger status_o;

@property (nonatomic, copy) NSString *status_m;

//物流
@property (nonatomic, copy) NSString *wuliu;    //物流id
@property (nonatomic, copy) NSString *wuliunum; //物流单号



@property (nonatomic, copy) NSString *store_id;

@property (nonatomic, copy) NSString *store_name;

@property (nonatomic, copy) NSString *pay_type;



//货币符号
@property (nonatomic, copy) NSString *symbol;

//支付价格
@property (nonatomic, copy) NSString *pay_price;

//优惠
@property (nonatomic, copy) NSString *discount;



@end
