//
//  YzOrderArrayModel.h
//  ASJ
//
//  Created by Jack on 16/9/13.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzOrderArrayModel : NSObject

@property (nonatomic ,strong ) NSMutableArray *OrderDataArray;

@property (nonatomic ,strong ) NSArray *data;

@property (nonatomic , copy) NSString *OrderID;

@property (nonatomic , copy) NSString *alipay_order;

@property (nonatomic , copy) NSString *alipay;

@property (nonatomic , copy) NSString *beizhu;

@property (nonatomic , copy) NSString *mobile;

@property (nonatomic , copy) NSString *number;

@property (nonatomic , copy) NSString *ordernum;

@property (nonatomic , copy) NSString *pay_status;

@property (nonatomic , copy) NSString *time;

@property (nonatomic , copy) NSString *status;

@property (nonatomic , copy) NSString *scores;

@property (nonatomic , copy) NSString *place;

@property (nonatomic , copy) NSString *rmb;
@end
