//
//  YzOrderArrayModel.m
//  ASJ
//
//  Created by Jack on 16/9/13.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzOrderArrayModel.h"

@implementation YzOrderArrayModel



-(NSMutableArray *)OrderDataArray{
    
    if (!_OrderDataArray) {
        _OrderDataArray = [NSMutableArray array];
    }
    
    return _OrderDataArray;
    
}
//
-(NSArray *)data{
    
    if (!_data) {
        
        _data = [NSArray array];
    }
    
    return _data;


}

@end
