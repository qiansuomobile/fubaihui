//
//  YzOrderDetailViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/10/9.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzOrderDetailViewController.h"
#import "FirstOrderDetailTableViewCell.h"
#import "SecondOrderDetailTableViewCell.h"
#import "ThirdOrderDetailTableViewCell.h"
#import "ForthOrderDetailTableViewCell.h"
#import "OrderDetailModel.h"
#import "AddCommentViewController.h"
#import "YzLogisticsOneTableViewCell.h"
#import "YzLogisticsViewController.h"

@interface YzOrderDetailViewController ()<UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UIView *bottomView;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,copy) NSString *addressStr;
@property (nonatomic,copy) NSString *nameStr;
@property (nonatomic,copy) NSString *phoneStr;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic ,copy) NSString *cancelOrderID;
@property (nonatomic ,strong) NSMutableArray *cancelOrderArray;
@property (nonatomic ,strong) NSMutableArray *logisticsArray;
@property(nonatomic , strong)NSString *logisticsStr;
@property(nonatomic , strong)NSString *titleStr;
@property(nonatomic , strong)NSString *timeStr;

@property (nonatomic, strong)OrderDetailModel *orderDetailModel;
@end

@implementation YzOrderDetailViewController

-(NSMutableArray *)imageArr{
    if (!_imageArr) {
        _imageArr = [NSMutableArray array];
    }
    return _imageArr;
}
-(NSMutableArray *)cancelOrderArray{
    if (!_cancelOrderArray) {
        _cancelOrderArray = [NSMutableArray array];
    }
    return _cancelOrderArray;
}
-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
-(NSMutableArray *)logisticsArray{
    if (!_logisticsArray){
        _logisticsArray = [NSMutableArray array];
    }
    return _logisticsArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];
    [self requestData];
}

-(void)requestData{
  
//    _orderDic = [[NSDictionary alloc]init];

    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"token":FBH_USER_TOKEN,
                          @"id":_order_id
                          };
    [NetMethod Post:FBHRequestUrl(kUrl_order_detail) parameters:dic success:^(id responseObject) {
    
        if ([responseObject[@"code"] isEqual:@200]) {
            _orderDetailModel  = [[OrderDetailModel alloc]init];
            [_orderDetailModel setValuesForKeysWithDictionary:responseObject[@"data"]];
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)createUI{
    self.title = @"订单详情";
    
    self.view.backgroundColor = [UIColor whiteColor];
    //[self initBottomView];
    
    [self initTableView];
}

-(void)initBottomView{
    _bottomView = [[UIView alloc] initWithFrame:CGRectZero];
    _bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(50*KHeight);
    }];
    
    UIButton *confirmBtn = [[UIButton alloc] init];
    [confirmBtn setTitle:@"确认收货" forState:UIControlStateNormal];
    [confirmBtn setBackgroundColor:[UIColor orangeColor]];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:20*KHeight];
    confirmBtn.layer.masksToBounds = YES;
    confirmBtn.layer.cornerRadius = 3.0*KHeight;
    [_bottomView addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_bottomView.mas_centerY);
        make.right.equalTo(_bottomView.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(35*KHeight);
        make.width.mas_equalTo(85*Kwidth);
    }];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
    }];
}

-(void)addComment:(UIButton *)sender{
    
    AddCommentViewController *commentVC = [[AddCommentViewController alloc] init];
    OrderDetailModel *model = self.dataArr[sender.tag - 100];
    commentVC.titleStr = model.goods_name;
    commentVC.gid = model.goods_id;
    commentVC.picStr = self.imageArr[sender.tag-100];
    [self.navigationController pushViewController:commentVC animated:YES];
}

#pragma mark --- UITableViewDelegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return 2;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        FirstOrderDetailTableViewCell *cell = [[FirstOrderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.orderNumL.text = [NSString stringWithFormat:@"订单编号: %@",_order_id];
        cell.createTimeL.text = [NSString stringWithFormat:@"下单时间: %@",_orderDetailModel.add_time];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.section == 1){
        SecondOrderDetailTableViewCell *cell = [[SecondOrderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.addressL.text = [NSString stringWithFormat:@"收货地址:%@",_orderDetailModel.address];
        cell.nameL.text = [NSString stringWithFormat:@"收货人:%@",_orderDetailModel.username];
        cell.phoneL.text = [NSString stringWithFormat:@"手机号:%@",_orderDetailModel.phone];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        if (indexPath.row == 0) {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            cell.imageView.image = [UIImage imageNamed:@"home"];
            cell.textLabel.text = @"订单详情";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            
            ThirdOrderDetailTableViewCell *cell = [[ThirdOrderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            
            cell.model = _orderDetailModel;
            
            cell.cancelBtn.tag = indexPath.row -1;
            
            cell.judgeBtn.tag = indexPath.row + 99;
            
            [cell.cancelBtn addTarget:self action:@selector(CancelOrderID:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.judgeBtn addTarget:self action:@selector(addComment:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        if (self.titleStr.length!=0) {
            YzLogisticsViewController *logistics = [[YzLogisticsViewController alloc]init];
            logistics.orderID = self.order_id;
            logistics.logisticsStr = self.logisticsStr;
            logistics.logisticsArray = self.logisticsArray;
            [self.navigationController pushViewController:logistics animated:YES];
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
            
        default:{
            
            NSString *ordirid = self.cancelOrderArray[alertView.tag];
//            NSLog(@"退货ID   %@",ordirid);

            NSDictionary *cancelOrderID = @{@"id":ordirid};
            
            [NetMethod Post:LoveDriverURL(@"APP/Order/ttt") parameters:cancelOrderID success:^(id responseObject) {
//                NSLog(@"退款退货   %@",responseObject);
                if ([responseObject[@"code"] isEqual:@200]) {
                    
                    [self requestData];
                    
                    [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                    
                }else{
                    [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                }
                
            } failure:^(NSError *error) {
                
            }];
            
        }
            break;
    }
    
    
}
-(void)CancelOrderID:(UIButton *)sender{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"申请退货吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
    
    alert.tag =sender.tag;
    
    [alert show];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 60*KHeight;
    }else if (indexPath.section == 1){
        return 90*KHeight;
    }else{
        if (indexPath.row == 1) {
            return 130*KHeight;
        }
        return 40.0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10*KHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
