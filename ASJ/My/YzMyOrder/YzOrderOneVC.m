//
//  YzOrderViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
//#import "YzCollectionViewController.h"
#import "GoodsDetailViewController.h"
#import "YzMyoderModel.h"
#import "YzOrderViewController.h"
#import "YzOrderArrayModel.h"
#import "YzOrderOneVC.h"
#import "YzOrderDetailViewController.h"


@interface YzOrderOneVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@property (nonatomic,strong) NSMutableArray *commentArr;

@end



static NSInteger Pages;

@implementation YzOrderOneVC

-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

- (NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    return _sources;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages = 1;
    
    self.title = @"全部";
    
    [self loadUI];
    
    [self setRefreshing];
    
    [self NewRequsturlData];
    
}
//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self NewRequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        [self NewloadMoreData];
        
        [self.TableView.mj_footer endRefreshing];
    }];
}

-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64) style:UITableViewStyleGrouped];
    
    //    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.sources.count;
}
//


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    YzOrderArrayModel *model = [[YzOrderArrayModel alloc]init];
    
    model = self.sources[section];
    
    return model.data.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzCollectionDetailCell *cell = [YzCollectionDetailCell cellWithTableView:tableView];
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[indexPath.section];
    
    YzMyoderModel *model = [[YzMyoderModel alloc]init];
    
    model = OrderModel.OrderDataArray[indexPath.row];
    
//    cell.BuyCar.hidden = YES;
    
//    cell.GoldLabel.hidden = YES;
//    
//    cell.SilverLabel.hidden = YES;
    
    
    cell.BuyCar.tag = [model.goods_id floatValue];
    cell.ifOrNoGold = @"2";
    [cell.BuyCar addTarget:self action:@selector(ShopDetail:) forControlEvents:UIControlEventTouchUpInside];


    
    [cell setOrderModel:model OrderModel:OrderModel];
    
    return cell;
    
}

-(void)ShopDetail:(UIButton *)sender{
    
    //    查看商品详情
    [self requestDetailDataWith:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[indexPath.section];
    
    //    SortGoods *model = [[SortGoods alloc]init];
    
    YzMyoderModel *model = [[YzMyoderModel alloc]init];
    
    model = OrderModel.OrderDataArray[indexPath.row];
    
    SortGoods *SortModel = [[SortGoods alloc]init];
    
    SortModel.goodID = model.goods_id;
    //    跳转到订单详情*************************************
    YzOrderDetailViewController *OrderDetailVC = [[YzOrderDetailViewController alloc] init];
    OrderDetailVC.order_id = model.order_id;
//    OrderDetailVC.orderNum = OrderModel.ordernum;
//    if ([OrderModel.alipay isEqualToString:@"0"]||[OrderModel.status isEqualToString:@"3"]) {
//        OrderDetailVC.type =@"0";
//    }else{
//        OrderDetailVC.type =@"1";
//    }
//    OrderDetailVC.goldOrsl = @"2";
//    OrderDetailVC.createTime = [VerifyPictureURL dateStringFromTimer:OrderModel.time];
    [self.navigationController pushViewController:OrderDetailVC animated:YES];
//    [self requestDetailDataWith:model.goods_id];
    
    
}


-(void)requestDetailDataWith:(NSString *)goodID{
    NSDictionary *dic = @{@"id":goodID};
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
    [NetMethod Post:LoveDriverURL(@"APP/Shopa/detail") parameters:dic success:^(id responseObject) {
        //NSLog(@"responsed === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
            
            if (![responseObject[@"pllist"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"pllist"]) {
                    CommentModel *model = [[CommentModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.commentArr addObject:model];
                }
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
                goodDetailVC.sortGoods = sortGood;
//                goodDetailVC.commentCount = responseObject[@"plcount"];
//                goodDetailVC.commentArr = self.commentArr;
                goodDetailVC.ShopType = [@"2" integerValue];
                goodDetailVC.isConvert = YES;
                goodDetailVC.godenType = @"sliver";
                [self.navigationController pushViewController:goodDetailVC animated:YES];
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
    
}

-(void)NewloadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"2",@"p":page};
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzOrderArrayModel *Arraymodel = [[YzOrderArrayModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                Arraymodel = [YzOrderArrayModel objectWithKeyValues:infoDic];
                
                NSArray *dataArray = [NSArray array];
                
                dataArray = Arraymodel.data;
                
                
                if (dataArray.count == 0) {
                    
                    
                }else{
                    
                    YzMyoderModel *model = [[YzMyoderModel alloc]init];
                    
                    Arraymodel.OrderDataArray = [NSMutableArray array];
                    
                    for (NSDictionary *dic  in dataArray) {
                        
                        model = [YzMyoderModel objectWithKeyValues:dic];
                        
                        [Arraymodel.OrderDataArray addObject:model];
                        
                    }
                    
                }
                
                [freshA  addObject:Arraymodel];
                
            }
            [self.sources  addObjectsFromArray:freshA];
            
        }
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)NewRequsturlData{
    
    //    我的订单
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"2",@"p":@"1"};
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        NSLog(@"\n\n\n\n\n订单详情   %@  \n\n\n",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            [self.sources removeAllObjects];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                YzOrderArrayModel *model = [[YzOrderArrayModel alloc]init];
                
                model =  [YzOrderArrayModel  objectWithKeyValues:infoDic];
                
                YzMyoderModel *OrderModel = [[YzMyoderModel alloc]init];
                
                for (NSDictionary *dataDic in model.data) {
                    
                    OrderModel = [YzMyoderModel objectWithKeyValues:dataDic];
                    
                    [model.OrderDataArray addObject:OrderModel];
                    
                }
                
                [freshA  addObject:model];
                
                self.sources = freshA;
                
            }
        }
        
        [self.TableView reloadData];
        
        
        [self.TableView.mj_header endRefreshing];
        
        
    } failure:^(NSError *error) {
        
    }];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[section];
    
    UIView *footerView = [[UIView alloc]init];
    
    footerView.frame = CGRectMake(0, 0, YzWidth, 60);
    
    footerView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *DataLabel = [[UILabel alloc]init];
    
    DataLabel.frame = CGRectMake(0, 5, YzWidth - 15, 20);
    
    DataLabel.textAlignment = NSTextAlignmentRight;
    
    DataLabel.font = [UIFont systemFontOfSize:14];
    
    
    DataLabel.text = [NSString stringWithFormat:@"共计 %lu件商品  合计：%@银积分",(unsigned long)OrderModel.data.count,OrderModel.scores];

    [footerView addSubview:DataLabel];
    
    
    UIView *whiteView = [[UIView alloc]init];
    
    whiteView.backgroundColor = RGBColor(237, 237, 237);
    
    whiteView.frame = CGRectMake(0, 45, YzWidth, 15);
    
    [footerView addSubview:whiteView];

    return footerView;
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[section];
    
    
    UIView *HeadView = [[UIView alloc]init];
    
    HeadView.frame = CGRectMake(0, 0, YzWidth, 40);
    
    HeadView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *OrderNumLabel = [[UILabel alloc]init];
    
    OrderNumLabel.frame = CGRectMake(10, 10, YzWidth/2, 20);
    
    OrderNumLabel.font = [UIFont systemFontOfSize:15];
    
    OrderNumLabel.text = [NSString stringWithFormat:@"订单号%@",OrderModel.ordernum];
    
    [HeadView addSubview:OrderNumLabel];
    
    
    UILabel *stateLabel = [[UILabel alloc]init];
    
    stateLabel.frame = CGRectMake(YzWidth/2, 10, YzWidth/2-10, 20);
    
    stateLabel.textAlignment = NSTextAlignmentRight;
    
    stateLabel.text = @"待收货";
    
    stateLabel.textColor = [UIColor redColor];
    
    stateLabel.font = [UIFont systemFontOfSize:14];
    
    [HeadView addSubview:stateLabel];
    
    
    
    return HeadView;
    
}

@end
