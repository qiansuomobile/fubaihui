//
//  YzOrderRufendViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YzMyoderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface YzOrderRufendViewController : UIViewController

@property (nonatomic, strong)YzMyoderModel *model;

@end

NS_ASSUME_NONNULL_END
