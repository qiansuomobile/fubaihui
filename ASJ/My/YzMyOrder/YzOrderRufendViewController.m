//
//  YzOrderRufendViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzOrderRufendViewController.h"
#import "YzRefundReasonViewController.h"
@interface YzOrderRufendViewController ()<YzRefundReasonViewControllerDelegate>

//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;

//商品名
@property (weak, nonatomic) IBOutlet UILabel *goodsTitleLabel;
//商品价格
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLabel;

//商品数量
@property (weak, nonatomic) IBOutlet UILabel *goodsNumLabel;


//退款金额
@property (weak, nonatomic) IBOutlet UILabel *rufendPriceLabel;

//退款说明
@property (weak, nonatomic) IBOutlet UITextField *rufendInfoLabel;

//退款原因
@property (weak, nonatomic) IBOutlet UILabel *rufendReasonLabel;

@end

@implementation YzOrderRufendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self rufendData];
    
    
}

- (IBAction)submitRufendClick:(UIButton *)sender {
    [self requsetRufendData];
}
- (IBAction)showRufendReasonView:(UITapGestureRecognizer *)sender {
//    kUrl_refund_reason
    YzRefundReasonViewController *refundReasonVC = [[YzRefundReasonViewController alloc]init];
    refundReasonVC.delegate = self;
    [self.navigationController pushViewController:refundReasonVC animated:YES];
}

- (void)rufendData{
    [_goodsImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL,_model.goods_img]]];
    _goodsNumLabel.text = [NSString stringWithFormat:@"✖️%@",_model.goods_num];
    _goodsTitleLabel.text = _model.goods_name;
    _goodsPriceLabel.text = [NSString stringWithFormat:@"¥%@",_model.goods_price];
    
    _rufendPriceLabel.text = [NSString stringWithFormat:@"¥%@",_model.pay_price];
}

#pragma mark - SET
- (void)setModel:(YzMyoderModel *)model{
    _model = model;
}

#pragma mark - YzRefundReasonViewControllerDelegate
- (void)selectRefundReasonTitle:(NSString *)str{
    
    _rufendReasonLabel.text = str;
}

#pragma mark - Url Request
- (void)requsetRufendData{
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"token":FBH_USER_TOKEN,
                          @"id":_model.order_id,
                          @"refund_explain":_rufendReasonLabel.text,
                          @"refund_reason":_rufendInfoLabel.text
                          };
    
    [NetMethod Post:FBHRequestUrl(kUrl_is_refund) parameters:dic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:@"申请退款成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
