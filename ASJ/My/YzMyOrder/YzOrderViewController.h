//
//  YzOrderViewController.h
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YzOrderType){
    YzOrderTypeAll = 0, //全部订单
    YzOrderTypePay,     //待付款
    YzOrderTypeShip,    //待发货
    YzOrderTypeReceipt, //待收货
    YzOrderTypeEnd      //已完成
};

@interface YzOrderViewController : UIViewController

@property (nonatomic, assign)YzOrderType type;

@end
