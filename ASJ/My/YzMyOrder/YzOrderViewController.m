//
//  YzOrderViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
//#import "YzCollectionViewController.h"
#import "GoodsDetailViewController.h"
#import "YzMyoderModel.h"
#import "YzOrderViewController.h"
#import "PayViewController.h"
#import "YzOrderDetailViewController.h"

#import "NewAddLookorderVC.h"

#import "YzGoodsOrderCell.h"

#import "YYYEvaluationController.h"
#import "YzOrderRufendViewController.h"


@interface YzOrderViewController ()<UITableViewDelegate,UITableViewDataSource, YzGoodsOrderCellDelegate>

@property (nonatomic, strong) NSMutableArray *sources;
@property (nonatomic , strong) UITableView *TableView;
@property (nonatomic , copy) NSString *OrderID;
@property (nonatomic , copy) NSString *OrderNum;
@property (nonatomic,strong) NSMutableArray *commentArr;

@end



static NSInteger Pages;

@implementation YzOrderViewController

-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

- (NSMutableArray *)sources{
    if (!_sources) {
        _sources = [NSMutableArray array];
    }
    return _sources;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self NewRequsturlData];
    self.navigationController.navigationBar.hidden = NO;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages = 1;
    
    self.title = @"全部";
    
    [self loadUI];
    
    [self setRefreshing];

    
}
//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self NewRequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
//    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//
//        Pages ++;
//
////        [self NewloadMoreData];
//
//        [self.TableView.mj_footer endRefreshing];
//    }];
}


-(void)loadUI{
    
    //self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64)];
    _TableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64 - 40)
                                              style:UITableViewStylePlain];
    _TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    YzOrderArrayModel *model = [[YzOrderArrayModel alloc]init];
//    
//    model = self.sources[section];
    
    return self.sources.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"goodsOrderCell";
    YzGoodsOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YzGoodsOrderCell" owner:self options:nil]lastObject];
    }
    cell.delegate = self;
    
    YzMyoderModel *model = self.sources[indexPath.row];
    cell.orederModel = model;
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 158;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc]init];
//    YzMyoderModel *model = self.sources[indexPath.row];
////    SortGoods *SortModel = [[SortGoods alloc]init];
////    SortModel.goodID = model.goods_id;
//    
//    //    跳转到订单详情*************************************
//    YzOrderDetailViewController *OrderDetailVC = [[YzOrderDetailViewController alloc] init];
//    OrderDetailVC.order_id = model.order_id;
////    OrderDetailVC.orderNum = OrderModel.ordernum;
////    if ([OrderModel.alipay isEqualToString:@"0"]||[OrderModel.status isEqualToString:@"3"]) {
////        OrderDetailVC.type =@"0";
////    }else{
////        OrderDetailVC.type =@"1";
////    }
////    OrderDetailVC.createTime = [VerifyPictureURL dateStringFromTimer:OrderModel.time];
//    [self.navigationController pushViewController:OrderDetailVC animated:YES];
}


#pragma MARK - ORDER CELL YzGoodsOrderCellDelegate

- (void)showOrderStatusDetailWithStatus:(YzOrderClickStatus)status orderModel:(nonnull YzMyoderModel *)model{
    NSLog(@"order status %ld", (long)status);
    
    switch (status) {
        case YzOrderClickStatusCancel:
        {
            NSLog(@"🔔---->取消订单 ");
            [self orderCancelWithOrderID:model.order_id];
        }
            break;
        case YzOrderClickStatusPay:
        {
            NSLog(@"🔔---->去支付 ");
            [self showOrderPayViewControllerWithModel:model];
        }
            break;
        case YzOrderClickStatusLogistics:
        {
            NSLog(@"🔔---->查看物流");
            [self showLogisticsViewControllerWithModel:model];
        }
            break;
        case YzOrderClickStatusRefund:
        {
            NSLog(@"🔔---->申请退款");
            [self showRufendViewControllerWithModel:model];
        }
            break;
        case YzOrderClickStatusConfirm:
        {
            NSLog(@"🔔---->确认支付");
            [self orderReceivingWithOrderID:model.order_id];
        }
            break;
        case YzOrderClickStatusAssess:
        {
            NSLog(@"🔔---->去评价");
            [self showOrderAssessViewControllerWithModel:model];
        }
        default:
            break;
    }
}

#pragma MARK - Action
//付款
- (void)showOrderPayViewControllerWithModel:(YzMyoderModel *)model{
    
    NSInteger payType;
    
    if ([model.pay_type isEqualToString:@"4"]) {
        payType = 1;
    }else{
        payType = 0;
    }
    
    PayViewController *payVC = [[PayViewController alloc] init];
    payVC.orderNum = model.ordernum;
    payVC.orderID = model.order_id;
    payVC.payPrice = [model.pay_price floatValue];
    payVC.payType = payType;
    payVC.merge = @"0";
    [self.navigationController pushViewController:payVC animated:YES];
}

//物流
- (void)showLogisticsViewControllerWithModel:(YzMyoderModel *)model{
    NewAddLookorderVC *logisticsVC = [[NewAddLookorderVC alloc]init];
    logisticsVC.wuliuID = model.wuliu;
    logisticsVC.wuliuNum = model.wuliunum;
    [self.navigationController pushViewController:logisticsVC animated:YES];
}

//评价
- (void)showOrderAssessViewControllerWithModel:(YzMyoderModel *)model{
    YYYEvaluationController *evaluationVC = [[YYYEvaluationController alloc]init];
    evaluationVC.model = model;
    [self.navigationController pushViewController:evaluationVC animated:YES];
}

//申请退款
- (void)showRufendViewControllerWithModel:(YzMyoderModel *)model{
    YzOrderRufendViewController *orderRufendViewController = [[YzOrderRufendViewController alloc]init];
    orderRufendViewController.model = model;
    [self.navigationController pushViewController:orderRufendViewController animated:YES];
}


#pragma MARK - URL Requset
// 获取我的订单
-(void)NewRequsturlData{
    
    //    我的订单
    //    LoveDriverID
    NSDictionary *infoDic = @{@"uid":LoveDriverID,
                              @"select_t":[NSNumber numberWithInteger:self.type],
                              @"token":FBH_USER_TOKEN,
                              @"page":@"1"
                              };
    
    [NetMethod Post:FBHRequestUrl(kUrl_my_order) parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"data"];
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                return ;
            }
            
            [self.sources removeAllObjects];
            NSMutableArray *freshA = [NSMutableArray array];
            for (NSDictionary *dataDic in info) {
                YzMyoderModel *OrderModel = [[YzMyoderModel alloc]init];
                [OrderModel setValuesForKeysWithDictionary:dataDic];
                [freshA addObject:OrderModel];
            }
            self.sources = freshA;
            [self.TableView reloadData];
        }
        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
    }];
}


//取消订单
- (void)orderCancelWithOrderID:(NSString *)orderID{
    
    NSDictionary *parameterDic = @{@"uid":LoveDriverID,
                                   @"token":FBH_USER_TOKEN,
                                   @"id":orderID};
    
    [NetMethod Post:FBHRequestUrl(kUrl_order_cancel) parameters:parameterDic success:^(id responseObject) {
        
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"] intValue];
        if (code ==200) {
            //刷新订单列表
            [self NewRequsturlData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


//确认收货
- (void)orderReceivingWithOrderID:(NSString *)orderID{
    
    NSDictionary *parameterDic = @{@"uid":LoveDriverID,
                                   @"token":FBH_USER_TOKEN,
                                   @"id":orderID};
    [NetMethod Post:FBHRequestUrl(kUrl_receiving) parameters:parameterDic success:^(id responseObject) {
        
        [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        
        NSDictionary *dic = responseObject;
        int code = [dic[@"code"] intValue];
        if (code ==200) {
            //刷新订单列表
            [self NewRequsturlData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


@end
