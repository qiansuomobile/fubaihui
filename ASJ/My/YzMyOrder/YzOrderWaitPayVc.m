//
//  YzOrderViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"

#import "GoodsDetailViewController.h"

#import "YzMyoderModel.h"

#import "YzOrderWaitPayVc.h"

#import "PayViewController.h"
#import "YzOrderDetailViewController.h"

@interface YzOrderWaitPayVc ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;


@property (nonatomic , copy) NSString *OrderID;

@property (nonatomic,strong) NSMutableArray *commentArr;
@end


static NSInteger Pages;

@implementation YzOrderWaitPayVc


-(NSMutableArray *)commentArr{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

- (NSMutableArray *)sources{
    
    if (!_sources) {
        _sources = [NSMutableArray array];
    }
    return _sources;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    Pages = 1;
    
//    self.title = @"全部";
    
    [self loadUI];
    
    [self setRefreshing];
    
    [self NewRequsturlData];
}


//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self NewRequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self NewloadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}


-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64) style:UITableViewStyleGrouped];
    
    //    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.sources.count;
}
//


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    YzOrderArrayModel *model = [[YzOrderArrayModel alloc]init];
    
    model = self.sources[section];
    
    return model.data.count;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzCollectionDetailCell *cell = [YzCollectionDetailCell cellWithTableView:tableView];
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[indexPath.section];
    
    YzMyoderModel *model = [[YzMyoderModel alloc]init];
    
    model = OrderModel.OrderDataArray[indexPath.row];
    
    cell.BuyCar.tag = [model.goods_id floatValue];
    
    
    [cell.BuyCar addTarget:self action:@selector(ShopDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [cell setOrderModel:model OrderModel:OrderModel];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc]init];
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[indexPath.section];
    
    //    SortGoods *model = [[SortGoods alloc]init];
    
    YzMyoderModel *model = [[YzMyoderModel alloc]init];
    
    model = OrderModel.OrderDataArray[indexPath.row];
    
    
    SortGoods *SortModel = [[SortGoods alloc]init];
    
    SortModel.goodID = model.goods_id;
    
    //    跳转到订单详情*************************************
    YzOrderDetailViewController *OrderDetailVC = [[YzOrderDetailViewController alloc] init];
    OrderDetailVC.order_id = model.order_id;
//    OrderDetailVC.orderNum = OrderModel.ordernum;
//    OrderDetailVC.type =@"0";
//    OrderDetailVC.createTime = [VerifyPictureURL dateStringFromTimer:OrderModel.time];
    [self.navigationController pushViewController:OrderDetailVC animated:YES];
    //    [self requestDetailDataWith:SortModel.goodID];
    
    
}

-(void)ShopDetail:(UIButton *)sender{
    
    //    查看商品详情
    [self requestDetailDataWith:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
}




-(void)requestDetailDataWith:(NSString *)goodID{
    NSDictionary *dic = @{@"id":goodID};
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
    [NetMethod Post:LoveDriverURL(@"APP/Shop/detail") parameters:dic success:^(id responseObject) {
        //NSLog(@"responsed === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
            
            if (![responseObject[@"pllist"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dic in responseObject[@"pllist"]) {
                    CommentModel *model = [[CommentModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [self.commentArr addObject:model];
                }
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
                goodDetailVC.sortGoods = sortGood;
//                goodDetailVC.commentCount = responseObject[@"plcount"];
//                goodDetailVC.commentArr = self.commentArr;
                [self.navigationController pushViewController:goodDetailVC animated:YES];
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


-(void)NewloadMoreData{
    
    //    Pages ++;
    
    //    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    //
    //    NSDictionary *infoDic = @{@"uid":@"3",@"type":@"1",@"p":page};
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":page,@"alipay":@"0",@"status":@"0"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzOrderArrayModel *Arraymodel = [[YzOrderArrayModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                Arraymodel = [YzOrderArrayModel objectWithKeyValues:infoDic];
                
                Arraymodel.OrderID = infoDic[@"id"];
                
                NSArray *dataArray = [NSArray array];
                
                dataArray = Arraymodel.data;
                
                
                if (dataArray.count == 0) {
                    
                    
                }else{
                    
                    YzMyoderModel *model = [[YzMyoderModel alloc]init];
                    
                    Arraymodel.OrderDataArray = [NSMutableArray array];
                    
                    for (NSDictionary *dic  in dataArray) {
                        
                        model = [YzMyoderModel objectWithKeyValues:dic];
                        
                        [Arraymodel.OrderDataArray addObject:model];
                        
                    }
                    
                }
                
                [freshA  addObject:Arraymodel];
                
            }
            [self.sources  addObjectsFromArray:freshA];
            
        }
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)NewRequsturlData{
    
    //    我的订单
    
    //    NSDictionary *infoDic = @{@"uid":@"3",@"type":@"1",@"p":@"1"};
    
    
    //    状态（alipay：0，status：0待付款；alipay：1，status：0待发货；alipay：1，status：1已发货）
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":@"1",@"alipay":@"0",@"status":@"0"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        NSLog(@"%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            [self.sources removeAllObjects];
            
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                YzOrderArrayModel *model = [[YzOrderArrayModel alloc]init];
                
                model =  [YzOrderArrayModel  objectWithKeyValues:infoDic];
                
                model.OrderID = infoDic[@"id"];
                
                YzMyoderModel *OrderModel = [[YzMyoderModel alloc]init];
                
                for (NSDictionary *dataDic in model.data) {
                    
                    OrderModel = [YzMyoderModel objectWithKeyValues:dataDic];
                    
                    [model.OrderDataArray addObject:OrderModel];
                    
                }
                
                [freshA  addObject:model];
                
                self.sources = freshA;
                
            }
        }
        
        [self.TableView reloadData];
        
        
        [self.TableView.mj_header endRefreshing];
        
        
    } failure:^(NSError *error) {
        
    }];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 80;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 40;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[section];
    
    UIView *footerView = [[UIView alloc]init];
    
    footerView.frame = CGRectMake(0, 0, YzWidth, 65);
    
    footerView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *DataLabel = [[UILabel alloc]init];
    
    DataLabel.frame = CGRectMake(0, 5, YzWidth - 15, 20);
    
    DataLabel.textAlignment = NSTextAlignmentRight;
    
    DataLabel.font = [UIFont systemFontOfSize:14];
    
    
    DataLabel.text = [NSString stringWithFormat:@"共计 %lu件商品  合计：%@元",(unsigned long)OrderModel.data.count,OrderModel.rmb];

    [footerView addSubview:DataLabel];
    
    
    
    UIView *whiteView = [[UIView alloc]init];
    
    whiteView.backgroundColor = RGBColor(237, 237, 237);
    
    whiteView.frame = CGRectMake(0, 65, YzWidth, 15);
    
    [footerView addSubview:whiteView];
    
    
    
    UIButton *goToPayBtn = [[UIButton alloc]init];
    
    [VerifyPictureURL setUpBtn:goToPayBtn title:@"确认付款" tag:section];
    
    goToPayBtn.frame = CGRectMake(YzWidth - 100, 32, 85, 28);
    
    [goToPayBtn addTarget:self action:@selector(GotoPay:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:goToPayBtn];
    
    
    
    UIButton *CanCellBtn = [[UIButton alloc]init];
    
    [VerifyPictureURL setUpBtn:CanCellBtn title:@"取消订单" tag:section];
    
    CanCellBtn.frame = CGRectMake(YzWidth - 200, 32, 85, 28);
    
    [CanCellBtn addTarget:self action:@selector(CancelOrderPay:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:CanCellBtn];
    
    return footerView;
    
}

-(void)CancelOrderPay:(UIButton *)sender{
    
    
    UIAlertController *alertController= [UIAlertController alertControllerWithTitle:@"提示" message:@"取消订单" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        
        
        NSLog(@"取消订单");
        
        YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
        
        OrderModel = self.sources[sender.tag];
        
        
        NSDictionary *OrderDelDic = @{@"uid":LoveDriverID,@"type":@"1",@"id":OrderModel.OrderID};
        
        [NetMethod Post:LoveDriverURL(@"APP/Member/orderDel") parameters:OrderDelDic success:^(id responseObject) {
            
            
            if ([responseObject[@"code"] isEqual:@200]) {
                
                [MBProgressHUD showSuccess:@"订单取消成功" toView:self.view];
                
                
                [self NewRequsturlData];
                
                [self.TableView reloadData];
                
                
            }else{
                
                
                [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
                
            }
            
            
        } failure:^(NSError *error) {
            
            
        }];
        
        
    }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:okAction];
    
    
    
    [self presentViewController:alertController animated:YES
                     completion:nil];
    
}


-(void)GotoPay:(UIButton *)sender{
    
    //            [alertController  addAction:[uial]];
    
    UIAlertController *alertController= [UIAlertController alertControllerWithTitle:@"提示" message:@"确认付款？" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        
        NSLog(@"确认付款");
        
        
        
        YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
        
        OrderModel = self.sources[sender.tag];
        
        
        PayViewController *vc = [[PayViewController alloc]init];
        
        
        vc.orderID = OrderModel.ordernum;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
        
    }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES
                     completion:nil];
    
    
    
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    YzOrderArrayModel *OrderModel = [[YzOrderArrayModel alloc]init];
    
    OrderModel = self.sources[section];
    
    
    UIView *HeadView = [[UIView alloc]init];
    
    HeadView.frame = CGRectMake(0, 0, YzWidth, 40);
    
    HeadView.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *OrderNumLabel = [[UILabel alloc]init];
    
    OrderNumLabel.frame = CGRectMake(10, 10, YzWidth/2, 20);
    
    OrderNumLabel.font = [UIFont systemFontOfSize:14];
    
    OrderNumLabel.text = [NSString stringWithFormat:@"订单号%@",OrderModel.ordernum];
    
    [HeadView addSubview:OrderNumLabel];
    
    
    
    
    UILabel *stateLabel = [[UILabel alloc]init];
    
    stateLabel.frame = CGRectMake(YzWidth/2, 10, YzWidth/2-10, 20);
    
    stateLabel.textAlignment = NSTextAlignmentRight;
    
    
    //    if ([OrderModel.status isEqualToString:@"1"]) {
    //
    //        stateLabel.text = @"已发货";
    //
    //    }else if ([OrderModel.status isEqualToString:@"2"]){
    //
    //        stateLabel.text = @"已退货";
    //
    //    }else if ([OrderModel.status isEqualToString:@"3"]){
    //
    //        stateLabel.text = @"已取消";
    //
    //    }else if ([OrderModel.status isEqualToString:@"0"]){
    //
    //        stateLabel.text = @"待发货";
    //
    //    }else{
    //    }
    
    
    stateLabel.text = @"待付款";
    
    
    stateLabel.textColor = [UIColor redColor];
    
    stateLabel.font = [UIFont systemFontOfSize:14];
    
    [HeadView addSubview:stateLabel];
    
    
    
    
    
    
    
    
    return HeadView;
    
}


@end
