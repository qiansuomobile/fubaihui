//
//  YzOrderViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
//#import "YzCollectionViewController.h"

#import "GoodsDetailViewController.h"

#import "YzMyoderModel.h"

#import "YzOrderWaitTalkVC.h"



@interface YzOrderWaitTalkVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end


static NSInteger Pages;

@implementation YzOrderWaitTalkVC


- (NSMutableArray *)sources{
    if (!_sources) {
        _sources = [NSMutableArray array];
    }
    return _sources;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    Pages = 1;
    
    self.title = @"全部";
    
    //    self.view.backgroundColor = [UIColor whiteColor];
    
    //    self.sources = [NSMutableArray array];
    
    
    //    if ([self.TitleString isEqualToString:@"我的订单"]) {
    [self loadUI];
    
    [self setRefreshing];
    
    
    [self RequsturlData];
    
    //    }
    
    
    // Do any additional setup after loading the view.
}


//刷新历史订单
- (void)setRefreshing{
    
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self loadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}
-(void)loadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        
        //        NSLog(@"%@",dic);
        
        //        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            //            YzMyoderModel *model = [YzMyoderModel object];
            NSArray *info = dic[@"list"];
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzMyoderModel *model = [[YzMyoderModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                NSArray *dataArray = infoDic[@"data"];
                
                if (dataArray.count == 0) {
                    
                    
                }else{
                    
                    NSDictionary *DataDic = dataArray[0];
                    
                    model =  [YzMyoderModel  objectWithKeyValues:DataDic];
                    
                    [freshA  addObject:model];
                    
                }
                
            }
            
            [self.sources  addObjectsFromArray:freshA];
            
            
            NSLog(@"数组个数   %lu",(unsigned long)self.sources.count);
            
            //            NSLog(@"数据源   %@",self.sources );
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
        //        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
        
    }];
    
    
}
-(void)RequsturlData{
    
    
    //    我的订单
    
    
    
    //    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/orderList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
                
            }

            
            YzMyoderModel *model = [[YzMyoderModel alloc]init];
            
            [self.sources removeAllObjects];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            
            for (NSDictionary *infoDic in info) {
                
                NSArray *dataArray = infoDic[@"data"];
                
                NSDictionary *DataDic = dataArray[0];
                
                model =  [YzMyoderModel  objectWithKeyValues:DataDic];
                
                //                [self.sources removeAllObjects];
                
                [freshA  addObject:model];
                
                //                NSLog(@"%@",model.cover);
                
                self.sources = freshA;
                
            }
            
            //            NSLog(@"数据源   %@",self.sources );
            
        }
        
        [self.TableView reloadData];
        
        //        [self.TableView.mj_header endRefreshing];
        
    } failure:^(NSError *error) {
        
        
    }];
    
}


-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 64)];
    
    //    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.sources .count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     *  2.1.0  新界面
     */
    
    YzCollectionDetailCell *cell = [YzCollectionDetailCell cellWithTableView:tableView];
    
    YzMyoderModel *model = [[YzMyoderModel alloc]init];
    
    model =self.sources[indexPath.row];
    
//    [cell setOrderModel:model];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
    
}
@end
