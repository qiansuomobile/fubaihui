//
//  YzRefundReasonViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YzRefundReasonViewControllerDelegate <NSObject>
- (void)selectRefundReasonTitle:(NSString *_Nonnull)str;
@end

NS_ASSUME_NONNULL_BEGIN

@interface YzRefundReasonViewController : UIViewController


@property (nonatomic, weak)id<YzRefundReasonViewControllerDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
