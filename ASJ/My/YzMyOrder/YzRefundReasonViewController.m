//
//  YzRefundReasonViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/29.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzRefundReasonViewController.h"

@interface YzRefundReasonViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *refundReasonTableView;

@property (nonatomic, strong)NSMutableArray *dataSoruce;

@end

@implementation YzRefundReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _dataSoruce = [NSMutableArray array];
    [self getRefundReasonData];
}

#pragma  MARK - Tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSoruce.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIndentifier];
    }
    cell.textLabel.text = _dataSoruce[indexPath.row];
    return cell;
}

#pragma -MARK - Tableview Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate selectRefundReasonTitle:_dataSoruce[indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma MARK - URL Requset
- (void)getRefundReasonData{
    [NetMethod Post:FBHRequestUrl(kUrl_refund_reason) parameters:nil success:^(id responseObject) {
        _dataSoruce = [NSMutableArray arrayWithArray:responseObject[@"data"]];
        [_refundReasonTableView reloadData];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
