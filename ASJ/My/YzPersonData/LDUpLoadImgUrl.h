//
//  LDUpLoadImgUrl.h
//  ASJ
//
//  Created by Jack on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^HttpSuccessBlock)(id JSON);
typedef void (^HttpFailureBlock)(NSError *error);

@interface LDUpLoadImgUrl : NSObject


+(void)parseUrl:(NSString *)urlStr dictionary:(NSDictionary *)parameters success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure;

@end
