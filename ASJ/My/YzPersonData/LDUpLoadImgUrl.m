//
//  LDUpLoadImgUrl.m
//  ASJ
//
//  Created by Jack on 16/9/2.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "LDUpLoadImgUrl.h"
#import "AFHTTPRequestOperationManager.h"


@implementation LDUpLoadImgUrl


#pragma mark----------网址解析
+(void)parseUrl:(NSString *)urlStr dictionary:(NSDictionary *)parameters success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure
{
    
    // NSString * installld = [[UIDevice currentDevice].identifierForVendor UUIDString];
    
    //#warning------------------------- 获取设备号
    
//    NSString * token =  [BKMD5 md5:installd];
    
    
    //  KCLog(@"%@",installd);
    
#pragma mark------------- AFN网络请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
#pragma mark------------- 请求超时
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    
    manager.requestSerializer.timeoutInterval = 10.f;
    
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
//    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
//    
//    [manager.requestSerializer setValue:installd forHTTPHeaderField:@"installId"];
    
    
    
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //
    //    NSString *MD = [BKMD5 md5:installd];
    //
    //    manager.requestSerializer.timeoutInterval = 10.f;
    //
    //    //申明返回的结果是json类型
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    //申明请求的数据是json类型
    //    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //
    //    [manager.securityPolicy setAllowInvalidCertificates:YES];
    //
    //    [manager.requestSerializer setValue:MD forHTTPHeaderField:@"token"];
    //
    //    [manager.requestSerializer setValue:installd forHTTPHeaderField:@"installId"];
    
    
    //发送请求
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        if (success == nil) return ;
        
        
        success(responseObject);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure == nil)return ;
        
        failure(error);
        
    }];
    
}


@end
