//
//  YZpersonaldataVC.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "MBProgressHUD.h"
#import "YZpersonaldataVC.h"
#import "UIView+Toast.h"
#import "YzChangeNameViewController.h"
#import "YzChangePersonDataVC.h"
#import "YzMyLocationVC.h"
#import "AFNetworking.h"
#import "LDUpLoadImgUrl.h"



@interface YZpersonaldataVC ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>


@property (strong,nonatomic)UIImagePickerController * pickImage;

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic , strong) UIImageView *heardImg;//头像

@property (nonatomic , strong) UIImage *HearDIMG;

@property (nonatomic , copy) NSString *ImgID;

@property (nonatomic , copy) NSString *ImgPath;

@end

@implementation YZpersonaldataVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"个人资料";
    
    
    [self LoadUI];
    
    
//    [self saveImg];
    
}

-(void)LoadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [self.view addSubview:self.TableView];
    
    self.cellTitleArray = @[@"头像",@"帐号",@"昵称"];
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cellTitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            _heardImg = [[UIImageView alloc]initWithFrame:CGRectMake(YzWidth-80, 20, 40, 40)];
            
            _heardImg.layer.cornerRadius = 20;
            
            _heardImg.layer.masksToBounds = YES;
            [cell addSubview:_heardImg];
        }
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.textLabel.text = self.cellTitleArray[indexPath.row];
    
    cell.textLabel.alpha = 0.7;
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    if (indexPath.row == 0) {
        
        //yu
        NSLog(@" - - %@", [NSURL URLWithString:LoveUserheardImg]);
        [_heardImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    
    
    switch (indexPath.row) {
        case 1:
        {
            //            NSString *nick = [NSString stringWithFormat:@"%@",LDusername];
            cell.detailTextLabel.text = LDusername;
        }
            break;
            
        case 2:
        {
            cell.detailTextLabel.text = LDnickname;
        }
            
        default:
            break;
    }
    
    
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 80;
    }else{
        return 44;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
        {
            [self chooseImageUsingType];
        }
            break;
        case 1:{
            NSLog(@"  --------   ");
            [self.view makeToast:@"帐号为登录名不可更改" duration:2 position:@"bottom"];
        }
            break;
        case 2:
        {
            //            修改昵称
            YzChangeNameViewController *vc = [[YzChangeNameViewController alloc]init];
            
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 3:
        {
            
            YzChangePersonDataVC *vc = [[YzChangePersonDataVC alloc]init];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        default:
        {
            
            YzMyLocationVC *vc = [[YzMyLocationVC alloc]init];
            
            [self.navigationController pushViewController:vc animated:YES];
            
            
        }
            break;
    }
    
}


#pragma mark Actionsheet 的初始化
-(void)chooseImageUsingType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍摄",@"相册", nil];
    
    [sheet.layer setCornerRadius:10];
    
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
            
        case 0:
            
            [self openCamera];
            
            break;
            
        case 1:
            
            [self openAlbum];
            
            break;
            
        default:
            
            break;
    }
}



#pragma mark 打开相册选择图片
-(void)openAlbum{
    
    self.pickImage = [[UIImagePickerController alloc]init];
    
    self.pickImage.allowsEditing = YES;
    
    self.pickImage.delegate = self;
    
    [self.pickImage.navigationBar setBarTintColor:Green];
    
    [self presentViewController:self.pickImage animated:YES completion:nil];
    
}
#pragma mark  打开相机
- (void)openCamera {
    
    self.pickImage = [UIImagePickerController new];
    
    self.pickImage.delegate = self;
    
    self.pickImage.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    self.pickImage.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        
    {
        self.pickImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:self.pickImage animated:YES completion:nil];
   
    }else{
        
        //        [YGAlertTool showAlert:@"无法打开相机"];
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"" otherButtonTitles:@"", nil];
//        
//        [alert show];
        
    }
}

#pragma mark  显示相册选择的照片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage * image = info[UIImagePickerControllerEditedImage];
    
    //    UIImage * image = info[UIImagePickerControllerOriginalImage];
    
    [self.heardImg setImage:image];
    
    self.HearDIMG = image;
    
    NSData *data = UIImageJPEGRepresentation(image, 0.0);
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSDictionary *dicPost = @{@"uid":LoveDriverID,
                              @"token":FBH_USER_TOKEN
                              };
    [NetMethod Post:FBHRequestUrl(@"/APP/Member/upload_headpic") parameters:dicPost IMGparameters:@{@"file":data} success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            
            NSString *dataY = responseObject[@"data"];
            NSString *path = FBHRequestUrl(dataY);
            [[NSUserDefaults standardUserDefaults] setObject:path forKey:@"ImgPath"];
//            NSLog(@"这里啦啦啦啦%@",path);
            [self.TableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];

    [self saveImage:image withName:@"logo"];
    

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.TableView reloadData];
    
}

//-(void)UPImgUserID
//{
//    //    上传图片ID
//    NSDictionary *Userinfo = [NSDictionary dictionary];
//    
//    Userinfo = @{@"uid":LoveDriverID,@"headpic":self.ImgID};
//    
//    [NetMethod Post:LoveDriverURL(@"APP/Member/edit2") parameters:Userinfo success:^(id responseObject) {
//        
////        [self.TableView reloadData];
//        
//    } failure:^(NSError *error) {
//        
//    }];
//    
//    
//}



- (void)saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.0);
    
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    
    [imageData writeToFile:fullPath atomically:NO];
    
}


//-(void)saveImg{
//
//
////    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"IMGHead.png"];
//
//    self.HearDIMG = [UIImage imageNamed:@"logo"];
//
//}

@end
