//
//  YzChangeNameViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzChangeNameViewController.h"

@interface YzChangeNameViewController ()<UITextFieldDelegate>

@property (nonatomic , strong)UITextField *changeName;

@end

@implementation YzChangeNameViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    self.title = @"昵称";
    
    self.view.backgroundColor = RGBColor(239, 239, 239);
    
    
    self.changeName = [[UITextField alloc]initWithFrame:CGRectMake(20, 30, YzWidth - 20, 30)];
    
    self.changeName.text = [NSString stringWithFormat:@"%@",LDnickname];
    
    self.changeName.clearButtonMode = UITextFieldViewModeAlways;
    
    [self.view addSubview:self.changeName];
    
    
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 100, YzWidth-20, 50)];
    
    detailLabel.numberOfLines = 0;
    
    detailLabel.font = [UIFont systemFontOfSize:12];
    
    [self.view addSubview:detailLabel];
    
    detailLabel.textColor = RGBColor(163, 163, 163);
    
    detailLabel.text = @"1.昵称非会员登录名，\n\n2.与业务或品牌冲突的昵称，福百惠有权收回";
    
    
    
    
    UIButton * seveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    seveBtn.frame = CGRectMake(20, 180, YzWidth-40, 40);
    
    //    seveBtn.titleLabel.text = @"保  存";
    
    [seveBtn setTitle:@"保  存" forState:UIControlStateNormal];
    
    seveBtn.backgroundColor =RGBColor(255, 64, 32);
    
    seveBtn.layer.cornerRadius = 4;
    
    
    [self.view addSubview:seveBtn];
    
    [seveBtn addTarget:self action:@selector(SeveName) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}




-(void)SeveName{
    
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"token":FBH_USER_TOKEN,
                          @"nickname":self.changeName.text};
    //yu
    [NetMethod Post:FBHRequestUrl(@"/APP/Member/upd_nickname") parameters:dic success:^(id responseObject) {
        
        
        NSDictionary *dic = responseObject;
        
        NSLog(@"%@",dic[@"msg"]);
        
        if ([dic[@"code"] intValue] == 200) {
            
            [MBProgressHUD showError:@"修改成功" toView:self.view];
            
            [[NSUserDefaults standardUserDefaults] setObject:self.changeName.text forKey:@"nickname"];
            
            
            [self performSelector:@selector(backToHome) withObject:nil afterDelay:1.5];
            
        }else{
            //yu
            [MBProgressHUD showError:dic[@"msg"] toView:self.view];
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    [self.changeName becomeFirstResponder];
    
    
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
