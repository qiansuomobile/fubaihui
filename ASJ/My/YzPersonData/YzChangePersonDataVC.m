//
//  YzChangePersonDataVC.m
//  ASJ
//
//  Created by Jack on 16/8/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzChangePersonDataVC.h"
#import "HMDatePickView.h"
#import "ZYLPickerView.h"


@interface YzChangePersonDataVC ()<UIPickerViewDataSource,UIPickerViewDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic, strong)NSArray *DataArray1;//资料1

@property (nonatomic, strong)NSArray *DataArray2;//资料2

@property (nonnull , copy) NSString *YZdate ;

@property (copy, nonatomic) NSString *dateStr;

@property (nonatomic , strong)NSArray *StarArray;

@property (nonatomic , strong)NSArray *StudyTypeArray;

@property (nonatomic , strong)UIPickerView *YzPickerView;

@property (nonatomic , strong)UIView *StarView;


@property (strong , nonatomic) UIButton *btn;

@property (assign , nonatomic) BOOL isBool;

@property (nonatomic , assign) BOOL LocationBool;

@property (nonatomic , assign) BOOL DateBool;

@property (nonatomic , assign) BOOL BretLocationBool;


@property (nonatomic , strong) UIView *datePickVC;

@property (nonatomic , strong) ZYLPickerView *zylpvc;

@property (nonatomic , strong) NSMutableDictionary *userInfoDic;

@property (nonatomic , strong) NSArray *IncomeArray;

//@property (nonatomic ,copy)NSString *starTypeStr;



@end

@implementation YzChangePersonDataVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"基本资料";
    
    [self LoadUI];
    
    self.isBool = YES;
    
    self.DateBool = YES;
    
    //    self.LocationBool = YES;
    
    self.BretLocationBool = YES;
    
}


-(void)LoadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    
    [self.view addSubview:self.TableView];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(YZsurePersenData)];
    
    self.navigationItem.rightBarButtonItem.title = @"保存";
    
    
    NSDictionary *dic  = LDUserInfo;
    _userInfoDic = dic.mutableCopy;
    //yu后台字段处理
    if (dic[@"borthday"]){
        [_userInfoDic setObject:dic[@"borthday"] forKeyedSubscript:@"birthday"];
    }
    
    //    NSLog(@"dic     %@",_userInfoDic);
    
    
    
    self.cellTitleArray = @[@"性别",@"年龄",@"星座"];
    
    self.StudyTypeArray = @[@"初高中",@"大专",@"本科",@"硕士",@"博士"];
    
    self.IncomeArray = @[@"3千-5千",@"5千-8千",@"8千－1万",@"1万－2万",@"2万以上"];
    
    self.StarArray = @[@"白羊",@"金牛",@"双子",@"巨蟹",@"狮子",@"处女",@"天平",@"天蝎",@"射手",@"魔羯",@"水瓶",@"双鱼"];
    
    self.CellImageArray = @[@"电话",@"生日",@"婚姻",@"月收入",@"教育程度"];
    
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    
    [dateFormat setDateFormat:@"yyyy-MM-dd "];//设定时间格式,这里可以设置成自己需要的格式
    
    NSDate *date =[dateFormat dateFromString:_userInfoDic[@"birthday"]];
    
    
    //            NSDate *today = [NSDate datewit];
    
    //            计算年龄
    NSInteger age =      [self ageWithDateOfBirth:date];
    
    self.age = [NSString stringWithFormat:@"%ld",age];
    
    self.StarType = [self getStartType:date];
    
    NSLog(@" 年龄  ＝＝＝＝  %@    星座%@",self.age,self.StarType);
    
    if ([_userInfoDic[@"sex"] isEqualToString:@"1"]) {
        
        self.SexType = @"男";
        
    }else if([_userInfoDic[@"sex"] isEqualToString:@"2"]){
        
        self.SexType = @"女";
        
    }else{
        
        self.SexType = @"保密";
    }
    
    self.DataArray1 = @[self.SexType,self.age,self.StarType];
    
    NSString *Phone =LDusername;
    
    if ([_userInfoDic[@"marriage"] isEqualToString:@"1"]) {
        
        self.MerryType = @"已婚";
        
    }else if([_userInfoDic[@"marriage"] isEqualToString:@"2"]){
        
        self.MerryType = @"保密";
        
    }else{
        
        self.MerryType = @"未婚";
        
    }
    
    NSString *birthday = _userInfoDic[@"birthday"]?_userInfoDic[@"birthday"]:_userInfoDic[@"borthday"];
    
    self.DataArray2 = @[Phone,birthday,self.MerryType,_userInfoDic[@"offer"],_userInfoDic[@"school"]];
    
    
    [self.TableView reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        
        return self.cellTitleArray.count;
        
    }else{
        
        return self.CellImageArray.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    //    cell.imageView.image = [UIImage imageNamed:self.CellImageArray[indexPath.row]];
    
    cell.textLabel.alpha = 0.7;
    
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (indexPath.section ==0) {
        
        cell.textLabel.text = self.cellTitleArray[indexPath.row];
        
        cell.detailTextLabel.text = self.DataArray1[indexPath.row];
        
        switch (indexPath.row) {
            case 0:
            {
                if (self.SexType) {
                    
                    cell.detailTextLabel.text =self.SexType;
                    
                }
                
            }
                break;
            case 1:{
                if (self.age) {
                    
                    cell.detailTextLabel.text =self.age;
                    
                }
                
            }
                break;
            case 2:
            {
                if (self.StarType) {
                    
                    cell.detailTextLabel.text =self.StarType;
                    
                }
                
                
            }
                break;
            default:
                break;
        }
        
        
        
        cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
        
    }else{
        
        cell.textLabel.text = self.CellImageArray[indexPath.row];
        
        cell.detailTextLabel.text = self.DataArray2[indexPath.row];
        
        switch (indexPath.row) {
            case 0:
            {
                //                if (self.PhoneNume) {
                
                cell.detailTextLabel.text = LDusername;
                //                }
            }
                break;
            case 1:
            {
                if (self.TodayDate) {
                    
                    cell.detailTextLabel.text = self.TodayDate;
                }
            }
                break;
            case 2:
            {
                if (self.MerryType) {
                    
                    cell.detailTextLabel.text = self.MerryType;
                }
                
            }
                break;
            case 3:
            {
                if (self.MoneyNum) {
                    
                    cell.detailTextLabel.text = self.MoneyNum;
                }
                
            }
                break;
            case 4:
            {
                if (self.JiaoyeType) {
                    
                    cell.detailTextLabel.text = self.JiaoyeType;
                }
                
            }
                break;
                //            case 5:
                //            {
                //                if (self.Location) {
                //
                //                    cell.detailTextLabel.text = self.Location;
                //                }
                //
                //            }
                //                break;
                //            case 6:
                //            {
                //                if (self.BrtLocation) {
                //
                //                    cell.detailTextLabel.text = self.BrtLocation;
                //                }
                //
                //            }
                break;
            default:
                break;
        }
        
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        
        switch (indexPath.row) {
            case 0:
            {
                [self choosePersonType];
            }
                break;
            case 1:
            {
                
            }
                break;
            case 2:
            {
                
                //                [self startPicker];
                //
                //                self.YzPickerView.tag = 100;
                //
                //                self.isBool = NO;
                
                
            }
                break;
            default:
                break;
        }
        
    }else{
        
        switch (indexPath.row) {
            case 0:
            {
                
            }
                break;
            case 1:
            {
                //                生日
                [self showDataPicker];
                
                self.DateBool = NO;
                
            }
                break;
            case 2:
            {
                [self chooseMerreyType];
            }
                
                break;
                
            case 3:
            {
                
                //                收入
                
                
                
                [self startPicker];
                self.YzPickerView.tag = 400;
                
                self.isBool = NO;
            }
                
                break;
                
            case 4:
            {
                // 教育程度
                
                
                
                [self startPicker];
                self.YzPickerView.tag = 200;
                self.isBool = NO;
                
            }
                break;
                
                
                //            case 5:
                //            {
                //
                //                _zylpvc = [[ZYLPickerView alloc] initWithFrame:CGRectMake(0,YzHeight , YzWidth, 160)];
                //
                //                [self.view addSubview:_zylpvc];
                //
                //                _zylpvc.tag = 100;
                //
                //                [self CreatPickerUI];
                //
                //                self.LocationBool = NO;
                //
                //            }
                //                break;
                //
                //            case 6:
                //                //            故乡
                //            {
                //
                //                _zylpvc = [[ZYLPickerView alloc] initWithFrame:CGRectMake(0,YzHeight , YzWidth, 160)];
                //
                //                [self.view addSubview:_zylpvc];
                //
                //                _zylpvc.tag = 200;
                //
                //                [self CreatPickerUI];
                //
                //                self.BretLocationBool = NO;
                //
                //            }
                //
                //                break;
                
            default:
                break;
        }
        
    }
    
    //    [tableView reloadData];
    
    
}




-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    switch (pickerView.tag) {
        case 100:
        {
            
            return self.StarArray.count;
        }
            break;
        case 200:
        {
            return self.StudyTypeArray.count;
        }
            break;
            
        case 400:
        {
            return self.IncomeArray.count;
        }
            break;
            
        default:
        {
            return 0;
        }
            
            break;
    }
    
    
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    //    return self.StarArray[row];
    
    switch (pickerView.tag) {
        case 100:
        {
            
            return self.StarArray[row];
        }
            break;
        case 200:
        {
            return self.StudyTypeArray[row];
        }
            
            break;
            
        case 400:
        {
            
            //            self.MoneyNum = self.IncomeArray[row];
            
            return self.IncomeArray[row];
            
            
        }
            break;
        default:
        {
            return nil;
        }
            
            break;
    }
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    
    //    switch (pickerView.tag) {
    //
    //        case 100:
    //
    //
    //            self.StarType = self.StarArray[row];
    //
    //            NSLog(@"   星座   %@",self.StarType);
    //
    //
    //            break;
    //        case 200:
    //
    //
    //            self.JiaoyeType = self.StudyTypeArray[row];
    //
    //
    //            NSLog(@"   学历   %@",self.JiaoyeType);
    //
    //
    //        case 400:
    //            self.MoneyNum = self.IncomeArray[row];
    //
    //
    //            NSLog(@"   收入   %@",self.MoneyNum);
    //
    //
    //            break;
    //        default:
    //            break;
    //    }
    if (self.YzPickerView.tag == 200) {
        
        self.JiaoyeType = self.StudyTypeArray[row];
        
        
    }else if(self.YzPickerView.tag == 400){
        
        self.MoneyNum = self.IncomeArray[row];
        
    }
    
    NSLog(@"   学历   %@",self.JiaoyeType);
    
    NSLog(@"   收入   %@",self.MoneyNum);
}

#pragma mark Actionsheet 的初始化
-(void)choosePersonType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男",@"女", nil];
    
    [sheet.layer setCornerRadius:5];
    
    sheet.tag = 100;
    
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}
-(void)chooseMerreyType{
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"已婚",@"未婚", nil];
    
    [sheet.layer setCornerRadius:5];
    
    sheet.tag = 200;
    sheet.delegate = self;
    
    [sheet showInView:self.view];
    
}

// 性别、婚姻
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (actionSheet.tag) {
            
        case 100:
            
        {
            switch (buttonIndex) {
                    
                case 0:
                    
                    //            男
                {
                    self.SexType = @"男";
                }
                    break;
                    
                case 1:
                    
                    //            女
                {
                    self.SexType = @"女";
                }
                    
                    break;
                    
                default:
                    
                    break;
            }
            
            
        }
            break;
            
        case 200:
        {
            switch (buttonIndex) {
                    
                case 0:
                    //            已婚
                {
                    self.MerryType = @"已婚";
                }
                    
                    break;
                    
                case 1:
                    
                    //            未婚
                {
                    self.MerryType = @"未婚";
                }
                    
                    break;
                    
                default:
                    
                    break;
            }
            
        }
            break;
        default:
            break;
    }
    
    
    [self.TableView reloadData];
}


//星座  学历  搞定 收入
-(void)YZpressentPickerView:(UIButton*)button{
    //确定
    if (button.tag == 1) {
        //确定
        //        self.completeBlock(_dateStr);
        
    }
    
    [UIView animateWithDuration:.5 animations:^{
        
        self.StarView.frame = CGRectMake(0, YzHeight, YzWidth, 230);
        
        
    } completion:^(BOOL finished) {
        
        
        [self.StarView removeFromSuperview];
        
        [self.YzPickerView removeFromSuperview];
        
        self.isBool = YES;
    }];
    
    
    [self.TableView reloadData];
}


//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//
//
//
//    [UIView animateWithDuration:.5 animations:^{
//
//        self.StarView.frame = CGRectMake(0, YzHeight, YzWidth, 230);
//
//
//    } completion:^(BOOL finished) {
//
//        [self.StarView removeFromSuperview];
//
//    }];
//
//}


//搞定

-(void)CreatPickerUI
{
    
    if (self.LocationBool || self.BretLocationBool) {
        
        [UIView animateWithDuration:.4 animations:^{
            
            _zylpvc.frame =CGRectMake(0,YzHeight - 240, YzWidth, 240);
            
        } completion:^(BOOL finished) {
            
        }];
        
        __weak __typeof(self) weakself = self;
        
        _zylpvc.SelectBlock = ^(NSString *proCityName){
            
            if (proCityName != nil) {
                
                NSRange range = [proCityName rangeOfString:@" "];
                
                NSString *str = [proCityName substringToIndex:range.location];
                
                NSString *strt = [proCityName substringFromIndex:range.location + range.length];
                
                if ([str isEqualToString:strt]) {
                    
                    proCityName = str;
                    
                }
                
                switch (weakself.zylpvc.tag) {
                    case 100:
                    {
                        weakself.Location = proCityName;
                        
                    }
                        break;
                        
                    default:{
                        
                        weakself.BrtLocation = proCityName;
                        
                    }
                        break;
                }
                
                [weakself.TableView reloadData];
                
            }
            
        };
        
    }else{
        
        [UIView animateWithDuration:.4 animations:^{
            
            _zylpvc.frame = CGRectMake(0, YzHeight, YzWidth, 240);
            
        } completion:^(BOOL finished) {
            
            [_zylpvc removeFromSuperview];
            
            self.LocationBool = YES;
            
            self.BretLocationBool = YES;
            
        }];
        
        
    }
    
    
    
}

// 生日设定  搞定
-(void)showDataPicker{
    
    //    self.DateBool = YES;
    
    if (self.DateBool) {
        
        /** 自定义日期选择器 */
        
        HMDatePickView *datePickVC = [[HMDatePickView alloc] initWithFrame:CGRectMake(0,YzHeight, YzWidth, 240)];
        
        [self.view addSubview:datePickVC];
        
        _datePickVC = datePickVC;
        
        //
        [UIView animateWithDuration:.5 animations:^{
            
            datePickVC.frame =CGRectMake(0,YzHeight - 240, YzWidth, 240);
            
        } completion:^(BOOL finished) {
            
        }];
        
        //配置属性
        [datePickVC configuration];
        
        //距离当前日期的年份差（设置最大可选日期）
        datePickVC.maxYear = -1;
        //设置最小可选日期(年分差)
        
        //    _datePickVC.minYear = 10;
        datePickVC.date = [NSDate date];
        
        //设置字体颜色
        datePickVC.fontColor = [UIColor redColor];
        
        __weak __typeof(self) weakself = self;
        
        //日期回调
        datePickVC.completeBlock = ^(NSString *selectDate) {
            
            weakself.TodayDate = selectDate;
            
            NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
            
            [dateFormat setDateFormat:@"yyyy-MM-dd "];//设定时间格式,这里可以设置成自己需要的格式
            
            NSDate *date =[dateFormat dateFromString:self.TodayDate];
            
            
            //            NSDate *today = [NSDate datewit];
            
            
            //            计算年龄
            //            NSInteger age =      [self ageWithDateOfBirth:date];
            
            
            
            self.StarType = [self getStartType:date];
            
            NSInteger age =      [self ageWithDateOfBirth:date];
            
            self.age = [NSString stringWithFormat:@"%ld",age];
            
            
            
            
            
            NSLog(@" 年龄  ＝＝＝＝  %ld     星座%@",(long)age,self.StarType);
            
            
            
            NSLog(@" XXXXX    %@",selectDate);
            
            [self.TableView reloadData];
            
        };
        
        
    }else{
        
        [UIView animateWithDuration:.5 animations:^{
            
            _datePickVC.frame = CGRectMake(0, YzHeight, YzWidth, 230);
            
        } completion:^(BOOL finished) {
            
            [_datePickVC removeFromSuperview];
            
            self.DateBool = YES;
            
        }];
    }
    
}

//选择星座  学历搞定

-(void)startPicker{
    
    if (self.isBool) {
        
        self.StarView = [[UIView alloc]initWithFrame:CGRectMake(0, YzHeight, YzWidth, 240)];
        
        self.StarView.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:self.StarView];
        
        [UIView animateWithDuration:.5 animations:^{
            
            self.StarView.frame = CGRectMake(0, YzHeight - 240, YzWidth, 240);
            
        } completion:^(BOOL finished) {
            
            //        [self.StarView removeFromSuperview];
            
        }];
        
        
        
        
        //确定按钮
        UIButton *commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        commitBtn.frame = CGRectMake(YzWidth - 50, 0, 40, 35);
        commitBtn.tag = 1;
        commitBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [commitBtn setTitle:@"确定" forState:UIControlStateNormal];
        [commitBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        [commitBtn addTarget:self action:@selector(YZpressentPickerView:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.StarView addSubview:commitBtn];
        
        //取消按钮
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame = CGRectMake(10, 0, 40, 35);
        cancelBtn.tag = 0;
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        [cancelBtn addTarget:self action:@selector(YZpressentPickerView:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.StarView addSubview:cancelBtn];
        
        //cancelBtn.selected = cancelBtn.isSelected;
        
        self.YzPickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 35, YzWidth, 160)];
        
        [self.view addSubview:self.YzPickerView];
        
        self.YzPickerView.delegate = self;
        
        self.YzPickerView.dataSource = self;
        
        [self.StarView addSubview:self.YzPickerView];
        
        [self pickerView:self.YzPickerView didSelectRow:0 inComponent:0];
        
        
        
    }else{
        
        
        [UIView animateWithDuration:.5 animations:^{
            
            self.StarView.frame = CGRectMake(0, YzHeight, YzWidth, 160);
            
        } completion:^(BOOL finished) {
            
            [self.StarView removeFromSuperview];
            
            self.isBool = YES;
            
        }];
        
    }
    
}


//保存提交信息
-(void)YZsurePersenData{
    
    
    //    NSLog(@" 生日    %@",self.TodayDate);
    
    _userInfoDic = LDUserInfo;
    
    if(!self.TodayDate){
        
        self.TodayDate = _userInfoDic[@"birthday"];
    }
    
    NSString *sex;
    
    if ([self.SexType isEqualToString:@"男"]) {
        
        sex = @"1";
        
    }else if ([self.SexType isEqualToString:@"女"]){
        
        sex = @"2";
    }else{
        
        sex = @"0";
    }
    
    //    NSLog(@"上传性别 %@",sex);
    
    
    NSString *marriage ;
    
    if ([self.MerryType isEqualToString:@"未婚"]) {
        
        marriage = @"0";
        
    }else if ([self.MerryType isEqualToString:@"已婚"]){
        
        marriage = @"1";
        
    }else{
        
        marriage = @"2";
    }
    
    
    
    
    
    
//    NSDictionary *dicHalf = @{@"uid":LoveDriverID,@"sex":sex,@"birthday":self.TodayDate};
    
//    [NetMethod Post:LoveDriverURL(@"APP/Member/edit") parameters:dicHalf success:^(id responseObject) {
//        //
//        //
//        //        NSDictionary *dic = responseObject;
//        //
//        //        NSLog(@"  修改资料  %@",dic);
//        //
//        //
//        //
//        //        NSLog(@"   %@",dic[@"msg"]);
//        //
//        //        int code = [dic[@"code"] intValue];
//        //
//        //        if (code ==200) {
//        //
//        //            [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
//        //
//        //            [self performSelector:@selector(BacktoRootViewController) withObject:nil afterDelay:1.5];
//        //
//        //        }
//
//
//
//    } failure:^(NSError *error) {
//
//
//
//    }];
    
    
    if (!self.JiaoyeType) {
        
        
        self.JiaoyeType = _userInfoDic[@"school"];
    }
    
    if (!self.MoneyNum) {
        
        
        self.MoneyNum = _userInfoDic[@"offer"];
    }
    
    
    
    
    NSLog(@"性别   %@,      生日%@",sex ,self.TodayDate);

    NSLog(@"  学历＊＊＊＊＊%@,婚姻＊＊＊＊  %@,  收入 ＊＊＊＊＊%@",self.JiaoyeType,marriage,self.MoneyNum);
    
    NSDictionary *DicAll = @{@"uid":LoveDriverID,
                             @"token":FBH_USER_TOKEN,
                             @"sex":sex,
                             @"age":_age,
                             @"borthday":self.TodayDate,
                             
                             @"school":self.JiaoyeType,
                             @"marriage":marriage,
                             @"offer":self.MoneyNum};
    
    //yu
    [NetMethod Post:FBHRequestUrl(@"/APP/Member/upd_info") parameters:DicAll success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        NSLog(@"  修改资料  %@",dic);
        
        
        
        NSLog(@"   %@",dic[@"msg"]);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
            
            [self performSelector:@selector(BacktoRootViewController) withObject:nil afterDelay:1.5];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
    
}

-(void)BacktoRootViewController{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//计算年龄
- (NSInteger)ageWithDateOfBirth:(NSDate *)date;
{
    // 出生日期转换 年月日
    NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger brithDateYear  = [components1 year];
    NSInteger brithDateDay   = [components1 day];
    NSInteger brithDateMonth = [components1 month];
    
    // 获取系统当前 年月日
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger currentDateYear  = [components2 year];
    NSInteger currentDateDay   = [components2 day];
    NSInteger currentDateMonth = [components2 month];
    
    // 计算年龄
    NSInteger iAge = currentDateYear - brithDateYear - 1;
    if ((currentDateMonth > brithDateMonth) || (currentDateMonth == brithDateMonth && currentDateDay >= brithDateDay)) {
        iAge++;
    }
    
    return iAge;
}

//计算星座
-(NSString *)getStartType:(NSDate *)in_date
{
    //计算星座
    
    NSString *retStr=@"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM"];
    int i_month=0;
    NSString *theMonth = [dateFormat stringFromDate:in_date];
    if([[theMonth substringToIndex:0] isEqualToString:@"0"]){
        i_month = [[theMonth substringFromIndex:1] intValue];
    }else{
        i_month = [theMonth intValue];
    }
    
    [dateFormat setDateFormat:@"dd"];
    int i_day=0;
    NSString *theDay = [dateFormat stringFromDate:in_date];
    if([[theDay substringToIndex:0] isEqualToString:@"0"]){
        i_day = [[theDay substringFromIndex:1] intValue];
    }else{
        i_day = [theDay intValue];
    }
    /*
     摩羯座 12月22日------1月19日
     水瓶座 1月20日-------2月18日
     双鱼座 2月19日-------3月20日
     白羊座 3月21日-------4月19日
     金牛座 4月20日-------5月20日
     双子座 5月21日-------6月21日
     巨蟹座 6月22日-------7月22日
     狮子座 7月23日-------8月22日
     处女座 8月23日-------9月22日
     天秤座 9月23日------10月23日
     天蝎座 10月24日-----11月21日
     射手座 11月22日-----12月21日
     */
    switch (i_month) {
        case 1:
            if(i_day>=20 && i_day<=31){
                retStr=@"水瓶座";
            }
            if(i_day>=1 && i_day<=19){
                retStr=@"摩羯座";
            }
            break;
        case 2:
            if(i_day>=1 && i_day<=18){
                retStr=@"水瓶座";
            }
            if(i_day>=19 && i_day<=31){
                retStr=@"双鱼座";
            }
            break;
        case 3:
            if(i_day>=1 && i_day<=20){
                retStr=@"双鱼座";
            }
            if(i_day>=21 && i_day<=31){
                retStr=@"白羊座";
            }
            break;
        case 4:
            if(i_day>=1 && i_day<=19){
                retStr=@"白羊座";
            }
            if(i_day>=20 && i_day<=31){
                retStr=@"金牛座";
            }
            break;
        case 5:
            if(i_day>=1 && i_day<=20){
                retStr=@"金牛座";
            }
            if(i_day>=21 && i_day<=31){
                retStr=@"双子座";
            }
            break;
        case 6:
            if(i_day>=1 && i_day<=21){
                retStr=@"双子座";
            }
            if(i_day>=22 && i_day<=31){
                retStr=@"巨蟹座";
            }
            break;
        case 7:
            if(i_day>=1 && i_day<=22){
                retStr=@"巨蟹座";
            }
            if(i_day>=23 && i_day<=31){
                retStr=@"狮子座";
            }
            break;
        case 8:
            if(i_day>=1 && i_day<=22){
                retStr=@"狮子座";
            }
            if(i_day>=23 && i_day<=31){
                retStr=@"处女座";
            }
            break;
        case 9:
            if(i_day>=1 && i_day<=22){
                retStr=@"处女座";
            }
            if(i_day>=23 && i_day<=31){
                retStr=@"天秤座";
            }
            break;
        case 10:
            if(i_day>=1 && i_day<=23){
                retStr=@"天秤座";
            }
            if(i_day>=24 && i_day<=31){
                retStr=@"天蝎座";
            }
            break;
        case 11:
            if(i_day>=1 && i_day<=21){
                retStr=@"天蝎座";
            }
            if(i_day>=22 && i_day<=31){
                retStr=@"射手座";
            }
            break;
        case 12:
            if(i_day>=1 && i_day<=21){
                retStr=@"射手座";
            }
            if(i_day>=21 && i_day<=31){
                retStr=@"摩羯座";
            }
            break;
    }
    return retStr;
}
@end
