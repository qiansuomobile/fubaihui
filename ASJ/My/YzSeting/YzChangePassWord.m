//
//  YzChangePassWord.m
//  ASJ
//
//  Created by Jack on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzChangePassWord.h"

@interface YZPasswordTF : UITextField

@end

@implementation YZPasswordTF

//控制左视图的大小
-(CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(5, 10, 11, 17.5);
    return inset;
}


//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds
{
    //return CGRectInset(bounds, 50, 0);
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);//更好理解些
    return inset;
}
//
////控制编辑文本的位置
//
-(CGRect)editingRectForBounds:(CGRect)bounds
{
    //return CGRectInset( bounds, 10 , 0 );
    CGRect inset = CGRectMake(25, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    return inset;
}

//控制placeHolder的颜色、字体
//-(void)drawPlaceholderInRect:(CGRect)rect
//{
//
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    CGContextSetFillColorWithColor(context, [UIColor yellowColor].CGColor);
//
//    [[UIColor orangeColor] setFill];
//
//    [[self placeholder]drawInRect:CGRectMake(3, 10, rect.size.width-8, rect.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],
//                                                                                                          NSForegroundColorAttributeName:KColor(163, 140, 130)}];


//}

@end

@interface YzChangePassWord ()<UITextFieldDelegate>

@property (strong,nonatomic)YZPasswordTF * oldPW;
@property (strong,nonatomic)YZPasswordTF * onceNewPW;
@property (strong,nonatomic)YZPasswordTF * twiceNewPW;
@property (nonatomic , strong) UITextField *NewPhoneT;
@property (nonatomic , strong) YZPasswordTF *YIjianfankui;
@end



@implementation YzChangePassWord

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    if([self.SetTitle isEqualToString:@"修改手机号"])
    {
        self.title = @"修改手机号";
        
        [self changePhoneNum];
        
    }else if([self.SetTitle  isEqualToString:@"修改密码"]){
        
        self.title = @"修改密码";
        
        [self creatViews];
    
    }else if([self.SetTitle isEqualToString:@"意见反馈"]){

        self.title = @"意见反馈";

        [self YIjianFankui];
    
    }
    
    
    
}

-(void)YIjianFankui{
    
    
//    YIjianfankui
    
    
    
    self.YIjianfankui = [[YZPasswordTF alloc]initWithFrame:CGRectMake(0, 42, YzWidth, 100)];
    //self.oldPW.delegate = self;
    
//    self.YIjianfankui.secureTextEntry = YES;
    
    self.YIjianfankui.placeholder =@"请填下您的宝贵意见";
    
    self.YIjianfankui.delegate = self;
    
    self.YIjianfankui.font = [UIFont systemFontOfSize:15];
    
    self.YIjianfankui.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.YIjianfankui];

    
    
    
    
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    button.frame = CGRectMake(10, CGRectGetMaxY(self.YIjianfankui.frame) + 40, YzWidth-20, 38);
    
    [button setTitle:@"确定" forState:UIControlStateNormal];
    
    [button setTintColor:[UIColor whiteColor]];
    
    button.layer.masksToBounds = YES;
    
    [button.layer setCornerRadius:3];
    
    button.backgroundColor = RGBColor(255, 64, 31);
    
    [button addTarget:self action:@selector(YZyijian) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];


}


-(void)YZyijian{
    
    BOOL isPhoneNumber = [VerifyPhoneNumber isBlankString:_YIjianfankui.text];
    
    if (!isPhoneNumber) {
    
    [MBProgressHUD showSuccess:@"感谢您的宝贵意见" toView:self.view];
        
    [self performSelector:@selector(BackToRootVC) withObject:nil afterDelay:2.0];
        
    
    }else{
        
        [MBProgressHUD showSuccess:@"请填写内容" toView:self.view];
 
        
    }
}

-(void)BackToRootVC{
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)changePhoneNum{
    
    UIView *backView = [UIView new];
    
    backView.frame = CGRectMake(0, 50, YzWidth, 80);
    
    backView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backView];
    
    UILabel *graLine = [UILabel new];
    
    graLine.backgroundColor = RGBColor(237, 237, 237);
    
    graLine.frame = CGRectMake(0, 40, YzWidth, 1);
    
    [backView addSubview:graLine];
    
    
    
    
    NSArray *titleArray = @[@"请输入手机号",@"国家／地区",@"手机号"];
    for (int i = 0; i<3; i++) {
        
        UILabel *titleLabel = [UILabel new];
        
        titleLabel.frame = CGRectMake(10, 10+i*40, 200, 40);
        
        titleLabel.text = titleArray[i];
        
        titleLabel.font = [UIFont systemFontOfSize:14];
        
        [self.view addSubview:titleLabel];
        
    }
    
    UILabel *group = [UILabel new];
    
    group.text = @"中国大陆(+86)";
    
    group.frame = CGRectMake(YzWidth - 120, 50, 110, 40);
    
    group.font = [UIFont systemFontOfSize:14];
    
    [self.view addSubview:group];
    
    
    
    
    UITextField *changePhone = [UITextField new];
    
    changePhone.placeholder = @"新手机号";
    
    changePhone.textAlignment = NSTextAlignmentRight;
    
    changePhone.frame = CGRectMake(YzWidth - 200, 10+80, 180, 40);
    
    changePhone.font = [UIFont systemFontOfSize:14];
    
    [self.view addSubview:changePhone];
    
    
    _NewPhoneT = changePhone;
    
    
    
    UIButton *setNewP = [UIButton buttonWithType:UIButtonTypeCustom];
    
    setNewP .frame = CGRectMake(20 , 260, YzWidth - 40, 40);
    
    setNewP.layer.cornerRadius = 3;
    
    [setNewP addTarget:self action:@selector(SetNewPhoneNum) forControlEvents:UIControlEventTouchUpInside];
    
    [setNewP setTitle:@"确定" forState:UIControlStateNormal];
    
    [setNewP setBackgroundColor:[UIColor redColor]];
    
    [self.view addSubview:setNewP];
    
    
    
    
}



-(void)creatViews{
    
    
    
    //    self.title = @"修改密码";
    
    self.oldPW = [[YZPasswordTF alloc]initWithFrame:CGRectMake(0, 42, YzWidth, 40)];
    //self.oldPW.delegate = self;
    
    self.oldPW.secureTextEntry = YES;
    
    self.oldPW.placeholder =@"请输入您的旧密码";
    
    self.oldPW.delegate = self;
    
    self.oldPW.font = [UIFont systemFontOfSize:15];
    
    self.oldPW.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.oldPW];
    
    
    
    
    
    self.onceNewPW = [[YZPasswordTF alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.oldPW.frame) + 40, YzWidth, 40)];
    
    self.onceNewPW.placeholder =@"请输入您的新密码";
    
    self.onceNewPW.delegate = self;
    
    self.onceNewPW.secureTextEntry = YES;
    
    self.onceNewPW.font = [UIFont systemFontOfSize:15];
    
    self.onceNewPW.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.onceNewPW];
    
    
    
    NSArray *psArray = @[@"旧密码",@"新密码"];
    
    for (int i = 0; i<2; i++) {
        
        UILabel *psLabel = [[UILabel alloc]init];
        
        psLabel.frame = CGRectMake(20, 20 +i *80 , 80, 20);
        
        psLabel.text = psArray[i];
        
        psLabel.textColor = [UIColor grayColor];
        
        psLabel.font = [UIFont systemFontOfSize:13];
        
        [self.view addSubview:psLabel];
        
    }
    
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    button.frame = CGRectMake(10, CGRectGetMaxY(self.onceNewPW.frame) + 20, YzWidth-20, 38);
    
    [button setTitle:@"确定" forState:UIControlStateNormal];
    
    [button setTintColor:[UIColor whiteColor]];
    
    button.layer.masksToBounds = YES;
    
    [button.layer setCornerRadius:3];
    
    button.backgroundColor = RGBColor(255, 64, 31);
    
    [button addTarget:self action:@selector(YZtapModifyPW) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
}



-(void)YZtapModifyPW{

    //    修改密码
    NSDictionary *dic = @{@"uid":LoveDriverID,@"password":self.oldPW.text,@"newpassword":self.onceNewPW.text};
    
    [NetMethod Post:LoveDriverURL(@"APP/User/profile") parameters:dic success:^(id responseObject) {
        
        NSDictionary *UserDic = responseObject;
        
        int code = [[UserDic objectForKey:@"code"] intValue];
        
        
        if (code ==200) {
            
            
            [MBProgressHUD showMessag:@"修改成功" toView:self.view];
            
            
            [self performSelector:@selector(BackToHome) withObject:nil afterDelay:1.5];
            
            
        }else
        {
            
            NSString *msg = [UserDic objectForKey:@"msg"];
            
            [MBProgressHUD showMessag:msg toView:self.view];
            
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}



-(void)BackToHome{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}



-(void)SetNewPhoneNum{
    
    //    修改手机号
    
    BOOL isPhoneNumber = [VerifyPhoneNumber checkTelNumber:_NewPhoneT.text];
    
    if (isPhoneNumber) {
        
        NSDictionary *dic = @{@"uid":LoveDriverID,@"phone":_NewPhoneT.text};
        
        [NetMethod Post:LoveDriverURL(@"APP/User/editphone") parameters:dic success:^(id responseObject) {
            
            NSDictionary *dic = responseObject;
            
            int code = [dic[@"code"] intValue];
            
            
            if (code == 200) {
                
                
                [MBProgressHUD showMessag:@"修改成功" toView:self.view];
                
                
                [self performSelector:@selector(BackToHome) withObject:nil afterDelay:1.5];
                
            }else
            {
                
                NSString *msg = [dic objectForKey:@"msg"];
                
                [MBProgressHUD showMessag:msg toView:self.view];
                
            }
            
            
        } failure:^(NSError *error) {
            
        }];
        
    }else{
        
        [MBProgressHUD showSuccess:@"请输入正确的手机号码" toView:self.view];
        return;
    }
    
}
@end
