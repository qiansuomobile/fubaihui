//
//  YzObautLDVC.m
//  ASJ
//
//  Created by Jack on 16/9/20.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzObautLDVC.h"

@interface YzObautLDVC ()

@end

@implementation YzObautLDVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *LogoImg = [[UIImageView alloc]init];
    
    LogoImg.frame = CGRectMake(YzWidth/4, 25, YzWidth/2, YzWidth/2);
    
    LogoImg.image = [UIImage imageNamed:@"logo"];
    
    [self.view addSubview:LogoImg];
    
    
    
    
    UILabel *AboutLabel = [[UILabel alloc]init];
    
    AboutLabel.frame = CGRectMake(10, YzWidth/2, YzWidth-20, 200);
    
    AboutLabel.numberOfLines = 0;
    
    AboutLabel.text = @"";
    
    AboutLabel.font = [UIFont systemFontOfSize:15];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:AboutLabel.text];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:4];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [AboutLabel.text length])];
  
    AboutLabel.attributedText = attributedString;
    
    
    [self.view addSubview:AboutLabel];
    
    
    
    
    // Do any additional setup after loading the view.
}

@end
