//
//  YzSaveCenter.m
//  ASJ
//
//  Created by Jack on 16/8/25.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzChangePassWord.h"

#import "YzSaveCenter.h"

#import "LoginViewController.h"

@interface YzSaveCenter ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;


@end

@implementation YzSaveCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"帐号安全";
    
    [self LoadUI];
    
}
-(void)LoadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight - 44)];
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.TableView.backgroundColor = RGBColor(237 , 237, 237);
    self.TableView.delegate = self;
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
    self.cellTitleArray = @[@"会员名",@"修改手机号",@"修改登录密码"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.cellTitleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.textLabel.text = self.cellTitleArray[indexPath.row];
    
    NSDictionary *dic = LDUserInfo;
    
    switch (indexPath.row) {
        case 0:
        {
            
            cell.detailTextLabel.text = dic[@"nickname"];
        }
            break;
        case 1:
        {
            NSString *mobile = dic[@"mobile"];
            
            if (![VerifyPhoneNumber isBlankString:mobile]) {
                
                NSString *number = [mobile substringWithRange:NSMakeRange(3,4)] ;
                //
                NSString *ppp = [mobile stringByReplacingOccurrencesOfString:number withString:@"****"];
                
                cell.detailTextLabel.text = ppp;
                
            }else{
                
                cell.detailTextLabel.text = @"";
            }
            
        }
            break;
        case 2:
        {
            
        }
        default:
            break;
    }
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.row) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
                YzChangePassWord *vc =[[YzChangePassWord alloc]init];
                vc.SetTitle = @"修改手机号";
                [self.navigationController pushViewController:vc animated:YES];

        }
            break;
        case 2:
        {
            
            YzChangePassWord *vc =[[YzChangePassWord alloc]init];
            vc.SetTitle = @"修改密码";
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        default:
            break;
    }
    
    
    
}
@end
