//
//  YzSetingViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzqiandaoVC.h"

#import "YzSetingViewController.h"
#import "YzChangeNameViewController.h"
#import "YzSaveCenter.h"
#import "YYYChangeMobileNoViewController.h"
#import "YZLoveDriverVC.h"
#import "YYYChangPasswordViewController.h"
#import "SDTimeLineTableViewController.h"

#import "LoginViewController.h"

#import "YzObautLDVC.h"

#import "YzHistroyVC.h"

#import "YzChangePassWord.h"
#import "ApplyViewController.h"

#import "AdvertisementViewController.h"

@interface YzSetingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic, strong)NSArray *cellClickedActionArr;//yu

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;

@property (nonatomic ,copy) NSString *fileData;

@property (nonatomic ,copy) NSString *OrderID;

@property (nonatomic , strong)MBProgressHUD *hud;

@end

@implementation YzSetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"账户管理";
    
    [self loadUI];
    
    [self GetSdData];
        
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.TableView reloadData];
}

-(void)loadUI{
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(247 , 247, 247);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
//    UIView *view = [UIView new];
//
//    view.frame = CGRectMake(0, 0, YzWidth, 40);
//
//    self.TableView.tableHeaderView = view;
    
//    self.cellTitleArray = @[@"意见反馈",@"账号与安全",@"新消息通知",@"清除系统缓存"];
    //yu
    self.cellTitleArray = @[@[@"账户头像",@"账户昵称",@"手机号",@"密码管理",@"清除缓存"],@[@"意见反馈",@"关于我们"]];
    _cellClickedActionArr = @[@[@"",@"changeNickName",@"changeMobileNo",@"changePassWord",@"clearCache"],
                              @[@"gotoYiJianFanKui",@"gotoAboutUs"]
                            ];
    
    
    
//    self.CellImageArray = @[@"Yzyijian",@"YzsavePs",@"YzMessege",@"Yzqchc"];
//    self.CellImageArray = @[@"Yzyijian",@"YzsavePs",@"Yzqchc"];
    
    
    
    if (!DriverISLogIN) {
        
    }else{
    
        //退出登录
        UIButton *exitBut = [UIButton buttonWithType:UIButtonTypeCustom];
        exitBut.frame = CGRectMake(40 , YzHeight - 44 - 64 - SafeAreaBottom - 49, YzWidth - 80, 44);
        exitBut.backgroundColor = RGBColor(74, 140, 63);
        exitBut.layer.masksToBounds = YES;
        exitBut.layer.cornerRadius = 5.0f;
        exitBut.titleLabel.textColor = [UIColor whiteColor];
        [exitBut addTarget:self action:@selector(EXIBUT) forControlEvents:UIControlEventTouchUpInside];
        [exitBut setTitle:@"退出登录" forState:UIControlStateNormal];
        [self.view addSubview:exitBut];
 
    }

}

-(void)EXIBUT{
    
//        __weak YzSetingViewController *weakSelf = self;
        [self showHudInView:self.view hint:NSLocalizedString(@"setting.logoutOngoing", @"loging out...")];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            EMError *error = [[EMClient sharedClient] logout:YES];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [weakSelf hideHud];
//                if (error != nil) {
//                    [weakSelf showHint:error.errorDescription];
//                }
//                else{
//                    [[ApplyViewController shareController] clear];
//                }
//            });
//        });
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DriverISLogIN"];
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:LoveDriverID];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr = self.cellTitleArray[section];
    
    return arr.count;

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row != 0) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    
    cell.backgroundColor = tableView.backgroundColor;;
    
    
    NSArray *arr = self.cellTitleArray[indexPath.section];

    cell.textLabel.text = arr[indexPath.row];
    cell.textLabel.alpha = 0.7;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];

    
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        //头像
//        UIImageView *img = [[UIImageView alloc]init];
//
//
//        img.frame = CGRectMake(10, 4, 36, 36);
//
//        cell.imageView.image = [UIImage imageNamed:self.CellImageArray[indexPath.row]];
//
//        //[cell addSubview:img];
        
        UIImageView *_heardImg = [[UIImageView alloc]initWithFrame:CGRectMake(YzWidth-80, 10, 30, 30)];
        _heardImg.layer.cornerRadius = 30/2.0;
        _heardImg.layer.masksToBounds = YES;
        [_heardImg sd_setImageWithURL:[NSURL URLWithString:LoveUserheardImg] placeholderImage:[UIImage imageNamed:@"logo"]];
        [cell addSubview:_heardImg];
    }else if ((indexPath.section == 0 && indexPath.row == 1)){
        cell.detailTextLabel.text = LDnickname;
    }else if ((indexPath.section == 0 && indexPath.row == 2)){
        //手机号
        NSMutableString *n = ((NSString *)LDusername).mutableCopy;
//        [n replaceCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        cell.detailTextLabel.text = n;
    }else if (((indexPath.section == 0 && indexPath.row == 4))){
        cell.detailTextLabel.text = self.fileData ;
        cell.detailTextLabel.text = [self getCaCheData] ;
        [cell.detailTextLabel setTextColor:[UIColor grayColor]];
    }
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header =  [[UIView alloc] init];
    header.backgroundColor = RGBColor(234, 234, 234);
    return header;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 50;
    }
    else{
        return 44;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 0;
        
    }else{
        return 15;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *actionArr = _cellClickedActionArr[indexPath.section];
    
    NSString *selectorY = actionArr[indexPath.row];
    
    if (selectorY.length < 1) {
        return;
    }
    
    [self performSelector:NSSelectorFromString(selectorY)];
    
//    switch (indexPath.row) {
//        case 0:
//        {
//            if (indexPath.section == 0) {
//                [self gotoYiJianFanKui];
//            }else{
//                [self gotoAboutUs];
//            }
//        }
//            break;
//        case 1:{
//            [self gotoAccoutSafe];
//        }
//            break;
//        case 2:
//        {
//            [self clearCache];
//        }
//            break;
//        default:
//            break;
//    }
}

- (void)changePassWord{
    //yu
    YYYChangPasswordViewController *vc = [[YYYChangPasswordViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)changeMobileNo{
    //修改手机号
    YYYChangeMobileNoViewController *vc = [[YYYChangeMobileNoViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)changeNickName{
    YzChangeNameViewController *vc = [[YzChangeNameViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)FinashiremoveCache{
    
    _hud. hidden= YES;
    [MBProgressHUD showSuccess:@"缓存清理完成" toView:self.view];
}
- (void)gotoYiJianFanKui{
    if (DriverISLogIN) {
        
        YzChangePassWord *Center = [[YzChangePassWord alloc]init];
        
        Center.SetTitle = @"意见反馈";
        
        [self.navigationController pushViewController:Center animated:YES];
        
    }else{
        
        LoginViewController *vc = [[LoginViewController alloc]init];
        
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        
        
        
        [self presentViewController:NaVC animated:YES completion:nil];
        
    }
}
- (void)gotoAboutUs{
    
//    YzObautLDVC *vc = [[YzObautLDVC alloc]init];
//
//    [self.navigationController pushViewController:vc animated:YES];
    
    AdvertisementViewController *adVC = [[AdvertisementViewController alloc] init];
    adVC.title = @"关于我们";
    adVC.Ad_ID = kUrl_about_me;
    
    [self.navigationController pushViewController:adVC animated:YES];
    
}
- (void)gotoAccoutSafe{
    if (DriverISLogIN) {
        
        YzSaveCenter *Center = [[YzSaveCenter alloc]init];
        
        [self.navigationController pushViewController:Center animated:YES];
        
    }else{
        
        
        LoginViewController *vc = [[LoginViewController alloc]init];
        
        YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];
        
        
        
        [self presentViewController:NaVC animated:YES completion:nil];
        
    }
}
- (void)clearCache{
    [self RevomeSdData];
    
    [GCUtil clearFile];
    
    [self.TableView reloadData];
}
- (CGFloat)sizeWithFile:(NSString *)filePath
{
    CGFloat totalSize = 0;
    
    NSFileManager *mgr = [NSFileManager defaultManager];
    
    BOOL isDirectory;
    
    BOOL isExists = [mgr fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    if (isExists) {
        
        if (isDirectory) {
            
            NSArray *subPaths =  [mgr subpathsAtPath:filePath];
            
            for (NSString *subPath in subPaths) {
                
                NSString *fullPath = [filePath stringByAppendingPathComponent:subPath];
                
                BOOL isDirectory;
                
                [mgr fileExistsAtPath:fullPath isDirectory:&isDirectory];
                
                if (!isDirectory) { // 计算文件尺寸
                    
                    NSDictionary *dict =  [mgr attributesOfItemAtPath:fullPath error:nil];
                    
                    totalSize += [dict[NSFileSize] floatValue];;
                }
            }
            
            
            
        }else{
            
            NSDictionary *dict =  [mgr attributesOfItemAtPath:filePath error:nil];
            
            totalSize =  [dict[NSFileSize] floatValue];
            
        }
        
    }
    
    return totalSize;
}






-(NSString *)getCaCheData{
    
    //    CGFloat cache = [GCUtil folderSizeAtPath:@""];
    NSString * cachPath = [ NSSearchPathForDirectoriesInDomains ( NSCachesDirectory , NSUserDomainMask , YES ) firstObject ];
    
    NSString * filedata = [GCUtil YzfolderSizeAtPath:cachPath];
    
    NSLog(@"   缓存森   *******%@",filedata);
    
    return filedata;
    
    
    
}


//获取缓存
-(void)GetSdData{
    
    CGFloat fileSize = [SDWebImageManager sharedManager].imageCache.getSize / 1024.0;
    
    NSString *fileData ;
    
    if (fileSize > 1024) {
        
        fileSize =  fileSize / 1024.0;
        
        fileData = [NSString stringWithFormat:@"%.1fM",fileSize];
        
    }else{
        
        fileData =[NSString stringWithFormat:@"%.fKB",fileSize];
    }
    
    self.fileData = fileData;
    
    
    [self.TableView reloadData];
    
}


//删除缓存
-(void)RevomeSdData{
    
    _hud = [MBProgressHUD showMessag:@"正在清理" toView:self.view];
    
    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    
    NSString *filePath = [docPath stringByAppendingPathComponent:@"com.hackemist.SDWebImageCache.default"];
    
    CGFloat size =  [self sizeWithFile:filePath];
    
    [[SDWebImageManager sharedManager].imageCache clearDisk];
    
    NSString *filedata ;
    
    if (size>1024) {
        
        //        NSLog(@"%f/1024",size);
        
        
        filedata = [NSString stringWithFormat:@"%.1fM",size];
    }else{
        
        //        NSLog(@"%.f",size);
        
        filedata =  [NSString stringWithFormat:@"%.fKB",size];
    }
    
    self.fileData = filedata;
    
    
    
    [self performSelector:@selector(FinashiremoveCache) withObject:nil afterDelay:1.5];
    
}

@end
