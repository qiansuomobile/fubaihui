//
//  YzGoldWallert.m
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzGoldWallert.h"

#import "YzWalletDetailVC.h"
#import "LoveShopViewController.h"

#import "YzqiandaoVC.h"

@interface YzGoldWallert ()


@property (nonatomic , strong) UIImageView *GoldImg;

@property (nonatomic , strong) UILabel *GoldLabel;

@property (nonatomic , strong) UILabel *GoldNum;

@property (nonatomic , strong) UIButton *workBtn;

@property (nonatomic , strong) UIButton *DetailBtn;

@end

@implementation YzGoldWallert

- (void)viewDidLoad {
   
    
    
    [super viewDidLoad];
    
    
//    self.title = @"金币";
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self LoadUI];
    
    // Do any additional setup after loading the view.
}


-(void)LoadUI{
    
    
    self.GoldImg = [[UIImageView alloc]initWithFrame:CGRectMake(YzWidth/3, 20, YzWidth/3, YzWidth/3)];
    
    
    
    self.GoldImg.image = [UIImage imageNamed:self.ImgName];
    
    [self.view addSubview:self.GoldImg];
    
    
    self.GoldLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, YzWidth/3+40, YzWidth, 30)];
    
    self.GoldLabel.textAlignment = NSTextAlignmentCenter;
    
//    self.GoldLabel.text = @"我的金币";
    
    [self.view addSubview:self.GoldLabel];
    
    
    
    self.GoldNum = [[UILabel alloc]initWithFrame:CGRectMake(0, YzWidth/3 +80, YzWidth, 30)];
   
    self.GoldNum.textAlignment = NSTextAlignmentCenter;
    
//    self.GoldNum.text = @"我的金币";
    
    
    self.GoldNum.font = [UIFont systemFontOfSize:16];

    
    [self.view addSubview:self.GoldNum];
    
    
    
    
    self.workBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.workBtn.frame = CGRectMake(10, YzWidth/3 +140, YzWidth-20, 40);
    
   
    
    [self.workBtn addTarget:self action:@selector(workDay:) forControlEvents:UIControlEventTouchUpInside];
    
    self.workBtn.backgroundColor = RGBColor(0, 173, 59);
    
    [self.view addSubview:self.workBtn];
    
//    [self.workBtn.layer setMasksToBounds:YES];
    [self.workBtn.layer setCornerRadius:3.0];
    
    
    
    self.DetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.DetailBtn.frame = CGRectMake(10, YzWidth/3 +190, YzWidth-20, 40);
    
    [self.DetailBtn setTitle:@"查看明细" forState:UIControlStateNormal];
    
    [self.DetailBtn addTarget:self action:@selector(LookDetail) forControlEvents:UIControlEventTouchUpInside];
    
    self.DetailBtn.backgroundColor = [UIColor grayColor];

    
    [self.view addSubview:self.DetailBtn];
    
    [self.DetailBtn.layer setCornerRadius:3.0];

    
    if ([self.title isEqualToString:@"金积分"]) {
        self.GoldLabel.text = @"我的金积分";
        self.GoldNum.text = self.jinbiNum;
         [self.workBtn setTitle:@"抢购送金积分" forState:UIControlStateNormal];
        
        self.workBtn.tag = 2000;
        
    }else{
        self.GoldLabel.text = @"我的银积分";
        self.GoldNum.text = self.yinbiNum;
         [self.workBtn setTitle:@"签到送银积分" forState:UIControlStateNormal];
        
        self.workBtn.tag = 3000;
    }

    
    
    
    UILabel *poblom = [[UILabel alloc]init];
    
    poblom.frame = CGRectMake(0, YzHeight - 120, YzWidth, 40);
    
//    [self.view addSubview:poblom];
    
    poblom.numberOfLines = 0;
    
    poblom.textAlignment = NSTextAlignmentCenter;
    
    poblom.font = [UIFont systemFontOfSize:12];
    
    poblom.text = @"常见问题\n本服务最终解释权由福百惠所有";
    
    poblom.textColor = [UIColor grayColor];
    
    [self.view addSubview:poblom];
    
    
}


-(void)workDay:(UIButton *)sender{
    
    
    switch (sender.tag) {
        case 2000:
        {
            
            
            LoveShopViewController *vc = [[LoveShopViewController alloc]init];
            
            
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
        {YzqiandaoVC *vc = [[YzqiandaoVC alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
   
        }
            break;
    }
    
}



-(void)LookDetail{
    
    
    YzWalletDetailVC *vc = [[YzWalletDetailVC alloc]init];
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
@end
