//
//  YzHistroyVC.m
//  ASJ
//
//  Created by Jack on 16/9/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzCollectionViewController.h"

#import "YzCollectionDetail.h"
#import "YzHistroyTwoVC.h"

#import "YzLookMenmreyVC.h"
#import "YzLookHistroryVC.h"


@interface YzHistroyTwoVC ()<UIScrollViewDelegate>

@property (nonatomic ,strong)UISegmentedControl *control;

@property (nonatomic ,strong)UIScrollView *MainScroll;

@property (nonatomic ,strong)YzLookMenmreyVC *YzOneVC;

@property (nonatomic ,strong)YzLookHistroryVC *YzTwoVC;

@end

@implementation YzHistroyTwoVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // 适应scrollView
    self.automaticallyAdjustsScrollViewInsets = NO;
    

    _control = [[UISegmentedControl alloc]initWithItems:@[@"福百惠商城",@"银币商城"]];

    
    _control.frame = CGRectMake(0, 0, YzWidth/2, 30);
    
    _control.layer.cornerRadius = 4;
    
    _control.selectedSegmentIndex = 0;
    
    _control.tintColor = [UIColor whiteColor];
    
    
    [_control addTarget:self action:@selector(myAction:)forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.titleView = _control;
    
    
    
    
    _MainScroll = [[UIScrollView alloc]init];
    
    _MainScroll.frame = CGRectMake(0, 0, YzWidth, YzHeight);
    
    //    _MainScroll.scrollEnabled = NO;
    
    _MainScroll.pagingEnabled = YES;
    
    _MainScroll.bounces = NO;
    
    _MainScroll.delegate = self;
    
    _MainScroll.contentSize = CGSizeMake(2*YzWidth, 0);
    
    [self.view addSubview:_MainScroll];
    
    
    //
    //    YzMyOrderVC *vc = [[YzMyOrderVC alloc]init];
    //
    //
    //    [MainScroll addSubview:vc];
    
    
    // 创建控制器
    _YzOneVC = [YzLookMenmreyVC new];
    
    _YzTwoVC = [YzLookHistroryVC new];
    
    // 添加为self的子控制器
    
    [self addChildViewController:_YzOneVC];
    [self addChildViewController:_YzTwoVC];
    _YzOneVC.view.frame = CGRectMake(0, 0, _MainScroll.frame.size.width, CGRectGetHeight(_MainScroll.frame));
    
    _YzTwoVC.view.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, 0, _MainScroll.frame.size.width, CGRectGetHeight(_MainScroll.frame));
    
    
    [_MainScroll addSubview:_YzOneVC.view];
    [_MainScroll addSubview:_YzTwoVC.view];
    
    
    
}

-(void)myAction:(UISegmentedControl *)sender{
    
    
    [_MainScroll setContentOffset:CGPointMake(sender.selectedSegmentIndex * _MainScroll.frame.size.width, 0) animated:NO];
    
    //
    //    switch (sender.selectedSegmentIndex) {
    //        case 0:
    //        {
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //        }
    //            break;
    //
    //
    //        case 1:
    //        {
    //
    //
    //
    //
    //
    //
    //
    //        }
    //            break;
    //
    //
    //        default:
    //            break;
    //    }
    //
    //
    
    
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger n = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    _control.selectedSegmentIndex = n;
    
}


@end
