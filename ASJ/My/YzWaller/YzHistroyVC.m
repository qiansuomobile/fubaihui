//
//  YzHistroyVC.m
//  ASJ
//
//  Created by Jack on 16/9/23.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzCollectionViewController.h"

#import "YzCollectionDetail.h"
#import "YzHistroyVC.h"

@interface YzHistroyVC ()<UIScrollViewDelegate>

@property (nonatomic ,strong)UIScrollView *MainScroll;

@property (nonatomic ,strong)YzCollectionViewController *YzOneVC;

@property (nonatomic ,strong)YzCollectionDetail *YzTwoVC;

@end

@implementation YzHistroyVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // 适应scrollView
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    _MainScroll = [[UIScrollView alloc]init];
    
    _MainScroll.frame = CGRectMake(0, 0, YzWidth, YzHeight);
    
    //    _MainScroll.scrollEnabled = NO;
    
    _MainScroll.pagingEnabled = YES;
    
    _MainScroll.bounces = NO;
    
    _MainScroll.delegate = self;
    
    _MainScroll.contentSize = CGSizeMake(2*YzWidth, 0);
    
    [self.view addSubview:_MainScroll];
    
    
    //
    //    YzMyOrderVC *vc = [[YzMyOrderVC alloc]init];
    //
    //
    //    [MainScroll addSubview:vc];
    
    
    // 创建控制器
    _YzOneVC = [YzCollectionViewController new];
    
    _YzTwoVC = [YzCollectionDetail new];
   
    // 添加为self的子控制器
    
    [self addChildViewController:_YzOneVC];
    [self addChildViewController:_YzTwoVC];
    _YzOneVC.view.frame = CGRectMake(0, 0, _MainScroll.frame.size.width, CGRectGetHeight(_MainScroll.frame));
    
    _YzTwoVC.view.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, 0, _MainScroll.frame.size.width, CGRectGetHeight(_MainScroll.frame));
    
    
    [_MainScroll addSubview:_YzOneVC.view];
    [_MainScroll addSubview:_YzTwoVC.view];
    
    
    
}

-(void)myAction:(UISegmentedControl *)sender{
    
    
    [_MainScroll setContentOffset:CGPointMake(sender.selectedSegmentIndex * _MainScroll.frame.size.width, 0) animated:NO];
    
    //
    //    switch (sender.selectedSegmentIndex) {
    //        case 0:
    //        {
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //        }
    //            break;
    //
    //
    //        case 1:
    //        {
    //
    //
    //
    //
    //
    //
    //
    //        }
    //            break;
    //
    //
    //        default:
    //            break;
    //    }
    //
    //
    
    
    
}


@end
