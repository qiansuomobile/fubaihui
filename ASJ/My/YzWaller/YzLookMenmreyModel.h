//
//  YzLookMenmreyModel.h
//  ASJ
//
//  Created by Jack on 16/9/7.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YzLookMenmreyModel : NSObject

@property (nonatomic ,copy)NSString *title;

@property (nonatomic ,copy)NSString *price;

@property (nonatomic ,copy)NSString *number;

@property (nonatomic ,strong)NSArray *cover;

@property (nonatomic ,copy)NSString *content;

@property (nonatomic ,copy)NSString *status;

@property (nonatomic ,copy)NSString *f_silver;

@property (nonatomic ,copy)NSString *f_sorts;


@end
