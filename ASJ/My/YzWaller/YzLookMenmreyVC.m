//
//  YzCollectionViewController.m
//  ASJ
//
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzCollectionDetailCell.h"
#import "YzLookMenmreyVC.h"
#import "YzshoucangCell.h"
#import "YzShouCangModel.h"
#import "GoodsDetailViewController.h"
#import "YzMyoderModel.h"
#import "YzLookMenmreyModel.h"


@interface YzLookMenmreyVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sources;

@property (nonatomic , strong) UITableView *TableView;

@end



static NSInteger Pages;

@implementation YzLookMenmreyVC


- (NSMutableArray *)sources{
    
    if (!_sources) {
        
        _sources = [NSMutableArray array];
        
    }
    
    return _sources;
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"浏览历史";
    
    Pages = 1;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self loadUI];
    
    [self RequsturlData];
    
    [self setRefreshing];
    
}

//刷新历史订单
- (void)setRefreshing{
    
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        Pages= 1;
        
        [self RequsturlData];
        
        [self.TableView.mj_header endRefreshing];
        
    }];
    
    self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        Pages ++;
        
        //        NSLog(@"%d",Pages);
        
        [self loadMoreData];
        
        NSLog(@"%ld",(long)Pages);
        
        [self.TableView.mj_footer endRefreshing];
        
        
    }];
    
    
}

-(void)loadMoreData{
    
    //    Pages ++;
    
    NSString *page = [NSString stringWithFormat:@"%ld",(long)Pages] ;
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":page};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/hisList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"我的订单   xx%@",dic);
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
           
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                
                return ;
                
            }
            
            YzShouCangModel *model = [[YzShouCangModel alloc]init];
            
            NSMutableArray *freshA = [NSMutableArray array];
            
            for (NSDictionary *infoDic in info) {
                
                NSArray *dataArray = infoDic[@"data"];
                
                
                if ([dataArray isKindOfClass:[NSNull class]]) {
                    
                    [self.TableView.mj_footer endRefreshing];
                    
                    [MBProgressHUD showSuccess:@"被你看光了" toView:self.view];
                    
                    return ;
                    
                }

                
                if (dataArray.count == 0) {
                    
                    
                }else{
                    NSDictionary *DataDic = dataArray[0];
                    model =  [YzShouCangModel  objectWithKeyValues:DataDic];
                    
                    [freshA  addObject:model];
                    
//                    NSLog(@"%@",model.cover);
                    
                }
                
            }
            
            [self.sources  addObjectsFromArray:freshA];
            
            
            NSLog(@"数组个数   %lu",(unsigned long)self.sources.count);
            
            //            NSLog(@"数据源   %@",self.sources );
            
        }
        
        [self.TableView reloadData];
        
        [self.TableView.mj_footer endRefreshing];
        
        //        [self.TableView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)RequsturlData{
    
    //    我的收藏列表
    
    NSDictionary *infoDic = @{@"uid":LoveDriverID,@"type":@"1",@"p":@"1"};
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/hisList") parameters:infoDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [dic[@"code"] intValue];
        
        if (code ==200) {
            
            NSArray *info = dic[@"list"];
            
            
            if ([info isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }

            YzShouCangModel *model = [[YzShouCangModel alloc]init];
            
            [self.sources removeAllObjects];
            
            
            for (NSDictionary *infoDic in info) {
                
                NSArray *dataArray = infoDic[@"data"];
                NSDictionary *DataDic = dataArray[0];
                
                model =  [YzShouCangModel  objectWithKeyValues:DataDic];
                [self.sources  addObject:model];
                
            }
//            NSLog(@"数据源   %@",self.sources );
        }
        
        [self.TableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)loadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return self.sources.count;
    return self.sources .count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     *  2.1.0  新界面
     */
    
    YzshoucangCell *cell = [YzshoucangCell cellWithTableView:tableView];
    
    //    cell.logtimes = self.logtimes[indexPath.row];
    //
    //    cell.picS_url = self.sources[indexPath.row];
    
    YzShouCangModel *model = [[YzShouCangModel alloc]init];
    
    model =self.sources[indexPath.row];
    //    cell.textLabel.text = @"xxxxx";
    
    //    [cell setUPframe];
    
    [cell setOrderModel:model];
    
    return cell;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GoodsDetailViewController *vc = [[GoodsDetailViewController alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
    
}


-(void)requestDetailDataWith:(NSString *)goodID{
    
    NSString *urlStr;
    
    //    switch (self.ShopType) {
    //        case 1:
                urlStr = @"APP/Shop/detail";
    //            break;
    //        case 2:
//    urlStr = @"APP/Shopa/detail";
    //            break;
    //        case 3:
    //            urlStr = @"APP/Shopyhls/detail";
    //            break;
    //        default:
    //            break;
    //    }
    NSDictionary *dic = @{@"id":goodID};
    
    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
    
    [NetMethod Post:LoveDriverURL(urlStr) parameters:dic success:^(id responseObject) {
        //NSLog(@"responsed === %@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            SortGoods *sortGood = [[SortGoods alloc] init];
            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
            //除了正品商城,其他商城暂时不显示大图
            //
            //            if (self.ShopType == 1) {
                            sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
            //            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hide:YES];
                if ([sortGood.number isEqualToString:@"0"]) {
                    [MBProgressHUD showSuccess:@"没存货了亲~" toView:self.view];
                }else{
                    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
                    goodDetailVC.sortGoods = sortGood;
                    //                    if (self.ShopType == 2) {
                    //兑换商城
//                    goodDetailVC.isConvert = YES;
                    //                    }
                    [self.navigationController pushViewController:goodDetailVC animated:YES];
                }
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}



@end
