//
//  YzSilverViewController.h
//  ASJ
//
//  Created by Ss H on 2018/3/19.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"

@interface YzSilverViewController : UIViewController
@property (nonatomic,strong) OrderModel *orderModel;

@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UITextField *goldTextField;
@property (weak, nonatomic) IBOutlet UILabel *goldMoneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *toBuyButton;
@property (nonatomic,assign) BOOL isConvert;

@end
