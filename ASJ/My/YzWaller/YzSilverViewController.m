//
//  YzSilverViewController.m
//  ASJ
//
//  Created by Ss H on 2018/3/19.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "YzSilverViewController.h"
#import "FinishPayViewController.h"
#import "PayViewController.h"

#define NUM @"0123456789"

@interface YzSilverViewController ()<UITextFieldDelegate>
{
    NSString * resultStr;
    NSString *moneyStr;
    NSString *gold;

}
@end

@implementation YzSilverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"购买金积分";
    self.goldTextField.delegate = self;
    self.goldTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.goldTextField.clearButtonMode = UITextFieldViewModeAlways;
    [self resultMessage];

}
-(void)resultMessage
{
//    http://123.56.236.200/APP/Gold/index
    [NetMethod Post:LoveDriverURL(@"APP/Gold/index") parameters:@{@"uid":LoveDriverID} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        NSLog(@"%@",dic);
        gold = [dic objectForKey:@"gold"];
        self.goldLabel.text = [NSString stringWithFormat:@"%@%@%@",@"一元人民币可购买",gold,@"个金积分"];
        
    } failure:^(NSError *error) {
        NSLog(@"error:%@",error.description);

    }];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUM] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        resultStr = toBeString;
    moneyStr = [NSString stringWithFormat:@"%.1f",[resultStr floatValue]/[gold intValue]];
    self.goldMoneyLabel.text = [NSString stringWithFormat:@"%@%@%@",@"共计",moneyStr,@"元人民币"];
        return [string isEqualToString:filtered];

}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.goldTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)toBuyButtonAction:(id)sender {
    //post数据
    if (self.goldTextField.text.length!=0) {
        [NetMethod Post:LoveDriverURL(@"APP/Gold/gold") parameters:@{@"uid":LoveDriverID,@"money":moneyStr,@"gold":gold} success:^(id responseObject) {
            
            NSDictionary *dic = responseObject;
            
            NSLog(@"%@",dic);
                //跳转
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    PayViewController *payVC = [[PayViewController alloc] init];
                    payVC.payPrice = [responseObject[@"money"] floatValue];
                    payVC.ifOrNoToBuyGold = YES;
                    payVC.payType = [@"1" integerValue];
                    payVC.orderID = responseObject[@"orderid"];
                    [self.navigationController pushViewController:payVC animated:YES];
                });
            
        } failure:^(NSError *error) {
            NSLog(@"error:%@",error.description);
            
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
