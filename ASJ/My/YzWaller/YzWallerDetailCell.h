//
//  YzCollectionDetailCell.h
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzWallerModel.h"
#import <UIKit/UIKit.h>

@interface YzWallerDetailCell : UITableViewCell

-(void)setOrderModel:(YzWallerModel *)model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;


-(void)setUPframe;
@end
