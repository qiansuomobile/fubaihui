//
//  YzCollectionDetailCell.m
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzWallerDetailCell.h"

@interface YzWallerDetailCell ()

@property (nonatomic, weak) UILabel *date;
@property (nonatomic, strong) UILabel *order_sn;
@property (nonatomic, strong) UILabel *state;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, weak) UIImageView *imageV;

@property (nonatomic , strong) UILabel *BuyCar;

@property (nonatomic , strong) UILabel *MoneyLabel;

@property (nonatomic , strong) UILabel *BusNameLabel;

@property (nonatomic , strong) UILabel *QQMoneyLabel;

@end


@implementation YzWallerDetailCell








- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self setUpSubview];
    }
    
    return self;
    
}



- (void)setUpSubview{
    
    UILabel *DateLabel = [[UILabel alloc]init];
    
    
    //        DateLabel.text = [self ctime:model.ctime];
    
    DateLabel.font = [UIFont systemFontOfSize:16];
    
    DateLabel.textAlignment = NSTextAlignmentCenter;
    
//    DateLabel.numberOfLines = 0;
    
    [self.contentView addSubview:DateLabel];
    
    self.BuyCar = DateLabel;
    
    
    UILabel *MoneyNumLabel2 = [[UILabel alloc]init];
    
    MoneyNumLabel2.font = [UIFont systemFontOfSize:12];
    
//    MoneyNumLabel2.numberOfLines = 0;
    
    MoneyNumLabel2.textAlignment = NSTextAlignmentCenter;

    [self.contentView addSubview:MoneyNumLabel2];
    
    self.MoneyLabel = MoneyNumLabel2;
    
    
    
    
    
    
    UIImageView *LogoImg = [[UIImageView alloc]init];
    
    
    LogoImg.image = [UIImage imageNamed:@"logo"];
    
    [self.contentView addSubview:LogoImg];
    
    self.imageV = LogoImg;
    
    
    
    
    
    
    
    
    
    UILabel *MoneyNumLabel = [[UILabel alloc]init];
    
    MoneyNumLabel.font = [UIFont systemFontOfSize:12];
    
    //        NSString *time = [self dateStringFromNumberTimer:model.ctime];
    
    //        NSLog(@"%@",time);
    
    //        MoneyNumLabel.text = [NSString stringWithFormat:@"   %@  %@ \n %@",model.silver,model.score,time];
    //        @"+ 100\n钱包签到 2016-8-24 签到";
    MoneyNumLabel.numberOfLines = 0;
    
    [self.contentView addSubview:MoneyNumLabel];
    
    
    self.BusNameLabel = MoneyNumLabel;
    
    
    
    
    
    
    
    
    
    
    
    
    UILabel *MoneyNumLabel3 = [[UILabel alloc]init];
    
    MoneyNumLabel3.font = [UIFont systemFontOfSize:14];
    
//    MoneyNumLabel3.numberOfLines = 0;
    
    [self.contentView addSubview:MoneyNumLabel3];
    
    self.QQMoneyLabel = MoneyNumLabel3;
    
    
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"cellID";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
    
}

-(void)setUPframe{
    
    //    时间
    self.BuyCar.frame = CGRectMake(15, 8, 70, 20);
    
    self.MoneyLabel.frame = CGRectMake(15, 33, 70, 20);

    //Logo
    self.imageV.frame = CGRectMake(100, 10, 40, 40);
    
    self.BusNameLabel.frame = CGRectMake(160, 35, YzWidth - 160, 20);
    self.QQMoneyLabel.frame = CGRectMake(160, 5, 120, 30);
    
}
-(void)setOrderModel:(YzWallerModel *)model{
    
    NSString *today = [VerifyPictureURL getWeekDayFordate:model.ctime];
    
   
    NSString *month =[VerifyPictureURL ctime:model.ctime];
    
    
    self.BuyCar.text = today;
    
    
    self.MoneyLabel.text = month;

    
    NSString *time1 = [VerifyPictureURL dateStringFromNumberTimer:model.ctime];
    NSString *time = [time1 substringToIndex:10];

    NSString *add;
    
    if ([model.status isEqualToString:@"0"]) {
        add = @"-";
    }else if ([model.status isEqualToString:@"1"]){
    
        add = @"+";
    }
    
    
    if ([model.score isEqualToString:@"0"] && ![model.silver isEqualToString:@"0"]) {
     
        self.QQMoneyLabel.text = [NSString stringWithFormat:@"%@%@ 银积分",add,model.silver];
        
    }else if ([model.score isEqualToString:@"0"] && [model.silver isEqualToString:@"0"]){
        
        self.QQMoneyLabel.text = @"暂无收益";
    
    }else if (![model.score isEqualToString:@"0"] && [model.silver isEqualToString:@"0"]){
        
        
        self.QQMoneyLabel.text = [NSString stringWithFormat:@"%@%@ 金积分",add,model.score];

    }else{

        self.QQMoneyLabel.text = [NSString stringWithFormat:@"%@%@ 金积分 +%@ 银积分",add,model.score ,model.silver];

    }
    if ([model.back1 isEqualToString:@"商城"]) {
     
        self.imageV.image = [UIImage imageNamed:@"bill_product"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"商城"];
        
    }else if([model.back1 isEqualToString:@"管理员发放"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_driver_present"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"管理员发放"];

    }else if([model.back1 isEqualToString:@"金积分转让"]) {
        self.imageV.image = [UIImage imageNamed:@"jinbi-48"];
    
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@%@%@",time,@"金积分转让",@"转给",model.back2];

    }else if([model.back1 isEqualToString:@"银积分转让"]) {
        
        self.imageV.image = [UIImage imageNamed:@"yinbi-48"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@%@%@",time,@"银积分转让",@"转给",model.back2];

    }else if([model.back1 isEqualToString:@"金积分商城"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_convert"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"金积分商城"];
    }else if([model.back1 isEqualToString:@"银积分商城"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_convert"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"银积分商城"];
    }
    else if([model.back1 isEqualToString:@"签到奖励"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_daily_sign"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"签到奖励"];
    }else if([model.back1 isEqualToString:@"退货返还"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_return_goods"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"退货返还"];
    }else if([model.back1 isEqualToString:@"扣除赠送"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_driver_deduct"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"扣除赠送"];
    }else if([model.back1 isEqualToString:@"提问悬赏"]) {
            
        self.imageV.image = [UIImage imageNamed:@"bill_question"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@%@",time,@"提问悬赏"];
    }else if([model.back1 isEqualToString:@"答题采纳"]) {
        
        self.imageV.image = [UIImage imageNamed:@"bill_question_answer"];
        
        self.BusNameLabel.text = [NSString stringWithFormat:@"%@",time,@"答题采纳"];
    }else{
      
        self.imageV.image = [UIImage imageNamed:@"bill_convert"];

        self.BusNameLabel.text = [NSString stringWithFormat:@"%@",time];
    }

    [self setUPframe];
}

@end
