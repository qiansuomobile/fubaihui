//
//  YzWalletDetailVC.m
//  ASJ
//
//  Created by Jack on 16/8/24.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "YzWallerDetailCell.h"

#import "YzWalletDetailVC.h"

#import "YzWallerModel.h"

@interface YzWalletDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)NSArray *cellTitleArray;

@property (nonatomic, strong)NSArray *CellImageArray;

@property (nonatomic , strong)UITableView *TableView;

@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic , strong) UIImageView *heardImg;//头像


@property (nonatomic , strong) NSMutableArray *ListArray;


@end

@implementation YzWalletDetailVC


-(NSMutableArray *)ListArray{
    
    if (!_ListArray) {
     
        _ListArray = [NSMutableArray array];
    }
    
    
    return _ListArray;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"账单";
    
    [self setRefresh];
    
    [self LoadUI];

}
-(void)setRefresh{
//    LoveDriverID
    
    [NetMethod Post:LoveDriverURL(@"APP/Member/billList") parameters:@{@"uid":LoveDriverID} success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
//        NSLog(@"%@",dic);
        
        int code = [dic[@"code"] intValue];

        if (code ==200) {
            
            NSArray *listArray = dic[@"list"];
            
            if ([listArray isKindOfClass:[NSNull class]]) {
                
                [self.TableView.mj_footer endRefreshing];
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
                
            }
            
            YzWallerModel *model = [[YzWallerModel alloc]init];
            
            for (NSDictionary *listDic in listArray) {
                
                model = [YzWallerModel objectWithKeyValues:listDic];
                
//                NSLog(@"%@",model.ctime);
                
                [self.ListArray addObject:model];

            }
            
        }
        
        [self.TableView reloadData];
    
    } failure:^(NSError *error) {
        
    }];
    
}
-(void)LoadUI{
    
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
    
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    
    self.TableView.delegate = self;
    
    self.TableView.dataSource = self;
    
    [self.view addSubview:self.TableView];
    

}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.ListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YzWallerModel *model = [[YzWallerModel alloc]init];
    
    model = self.ListArray[indexPath.row];
    
    YzWallerDetailCell *cell = [YzWallerDetailCell cellWithTableView:tableView];
    
    [cell setOrderModel:model];
    
    return cell;
}

@end
