//
//  YzwalletViewController.m
//  ASJ


          
 
 //
//  Created by Jack on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "YzHistroyTwoVC.h"

#import "YzHistroyVC.h"

#import "YzwalletViewController.h"

#import "YzGoldWallert.h"

#import "NewOrderDetailVC.h"

#import "YzCollectionDetail.h"

#import "YzMyLocationVC.h"

#import "YzCollectionViewController.h"

#import "YzLookMenmreyVC.h"

#import "YzGoldRechargeViewController.h"

#import "YzCashOutViewController.h"

#import "YzCashOutRMBViewController.h"

@interface YzwalletViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSArray *cellTitleArray;
@property (nonatomic, strong)NSArray *cellImageArray;
@property (nonatomic , strong)UITableView *TableView;
@property (nonatomic ,strong)UIView *headerView;//

@property (nonatomic, strong)UILabel *goldNumLabel;
@property (nonatomic, strong)UILabel *moneyNumLabel;
@property (nonatomic, strong)UILabel *mallGoldNumLabel;
@property (nonatomic, strong)UILabel *silverNumLabel;

@end

@implementation YzwalletViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = @"我的钱包";

    self.cellTitleArray = @[@"账单明细",@"购买金积分"];
    self.cellImageArray = @[@"My_bill",@"pay_gold"];
    
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight +64)];
    self.TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.TableView.backgroundColor = RGBColor(237, 237, 237);
    self.TableView.delegate = self;
    self.TableView.dataSource = self;
    [self.view addSubview:self.TableView];

    //老布局
    //    [self loadUI];
    
    //新布局
    [self loadNewUI];
}

-(void)loadNewUI{

    UIView *HeadView = [[UIView alloc] init];
    HeadView.frame   = CGRectMake(0, 0, YzWidth, YzWidth/2);
    HeadView.backgroundColor = RGBColor(246, 247, 248);
    self.TableView.tableHeaderView = HeadView;
    
    NSArray *titleArray = @[@[@"金积分", @"余额"], @[@"商家金积分", @"银积分"]];
    NSArray *imageArray = @[@[@"pay_gold", @"jinbi222"], @[@"pay_gold", @"pay_silver"]];
    NSArray *pointArray = @[@[@"--", @"--"], @[@"--", @"--"]];
    NSArray *tagArr = @[@[@"0", @"1"], @[@"2", @"3"]];
    
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j ++) {
            UIButton *bgView = [UIButton buttonWithType:UIButtonTypeCustom];
            NSString *tag = tagArr[i][j];
            bgView.tag = [tag integerValue];
            bgView.frame   = CGRectMake(j * YzWidth / 2, i * YzWidth / 4, YzWidth/2, YzWidth/4);
            
            [bgView addTarget:self action:@selector(showCashoutViewController:) forControlEvents:UIControlEventTouchUpInside];
            [HeadView addSubview:bgView];
            
            UIButton *imagebtn = [UIButton buttonWithType:UIButtonTypeCustom];
            imagebtn.frame     = CGRectMake(15, 15, 15, 15);
            [imagebtn setBackgroundImage:[UIImage imageNamed:imageArray[i][j]] forState:UIControlStateNormal];
            [bgView addSubview:imagebtn];
            
            UILabel *titleLabel = [[UILabel alloc] init];
            titleLabel.text     = titleArray[i][j];
            titleLabel.frame    = CGRectMake(35, 10, 100, 20);
            titleLabel.textColor = RGBColor(43, 44, 44);
            titleLabel.textAlignment = NSTextAlignmentLeft;
            titleLabel.font = [UIFont systemFontOfSize:15.0f];
            [bgView addSubview:titleLabel];
            
            
            UILabel *pointLabel = [[UILabel alloc]init];
            pointLabel = [[UILabel alloc]init];
            pointLabel.frame    = CGRectMake(30,40, YzWidth/2, 20);
            pointLabel.text = pointArray[i][j];
            pointLabel.font = [UIFont systemFontOfSize:20.0f];
            pointLabel.textAlignment = NSTextAlignmentLeft;
            [bgView addSubview:pointLabel];
            
            if (i == 0 && j == 0) {
                _goldNumLabel = pointLabel;
            }
            if (i == 0 && j == 1) {
                _moneyNumLabel = pointLabel;
            }
            if (i == 1 && j == 0) {
                _mallGoldNumLabel = pointLabel;
            }
            if (i == 1 && j == 1) {
                _silverNumLabel = pointLabel;
            }
        }
    }
    
    UIView *lineViewH = [[UIView alloc]initWithFrame:CGRectMake(HeadView.frame.size.width / 2, 0, 0.5, HeadView.frame.size.height)];
    lineViewH.backgroundColor = RGBColor(231, 232, 233);
    [HeadView addSubview:lineViewH];
    
    UIView *lineViewV = [[UIView alloc]initWithFrame:CGRectMake(0, HeadView.frame.size.height /2, HeadView.frame.size.width, 0.5)];
    lineViewV.backgroundColor = RGBColor(231, 232, 233);
    [HeadView addSubview:lineViewV];
    
}

- (void)showCashoutViewController:(UIButton *)btn{
    NSLog(@"选择钱包 - %ld", (long)btn.tag);
    if (btn.tag == 0 || btn.tag == 2) {
        YzCashOutViewController *vc = [[YzCashOutViewController alloc]init];
        vc.cashOutType = btn.tag;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (btn.tag == 1) {
        YzCashOutRMBViewController *rmbVc =[[YzCashOutRMBViewController alloc]init];
        [self.navigationController pushViewController:rmbVc animated:YES];
    }
}

#pragma MARK - UITABLEVIEWDATA

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.cellTitleArray.count;
    return  self.cellTitleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.imageView.image = [UIImage imageNamed:self.cellImageArray[indexPath.row]];
//    cell.textLabel.text = self.cellTitleArray[indexPath.row];
    cell.textLabel.text = self.cellTitleArray[indexPath.row];
    cell.textLabel.alpha = 0.7;
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return YzWidth/2;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
//        case 0:
//        {
//           //商家首款码
//            YzSilverViewController *silverVc = [[YzSilverViewController alloc]init];
//            [self.navigationController pushViewController:silverVc animated:YES];
//        }
//            break;
        case 0:
        {
            //账单明细
            NewOrderDetailVC *vc = [[NewOrderDetailVC alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
            
        case 1:
        {
            //购买金积分
            YzGoldRechargeViewController *vc = [[YzGoldRechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
        default:
            break;
    }
}

#pragma mark - SHOW VIEWCONTROLLER

//提现
- (void)cashOutWithFBH{
    
}

#pragma MARK - URL Request
// 获取用户余额信息
- (void)getGoldRechargeDataRequest{
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *data = resDic[@"data"];
            //用户金积分
            _goldNumLabel.text      = [NSString stringWithFormat:@"%@",data[@"score"]];
            _moneyNumLabel.text     = [NSString stringWithFormat:@"%@",data[@"balance"]];
            _mallGoldNumLabel.text  = [NSString stringWithFormat:@"%@",data[@"store_score"]];
            _silverNumLabel.text    = [NSString stringWithFormat:@"%@",data[@"silver"]];
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setTranslucent:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self getGoldRechargeDataRequest];
}

@end
