//
//  LikeLoginCellView.h
//  ASJ
//
//  Created by YYY on 2019/7/6.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockDynamicPasswordButton.h"
#import "WTReTextField.h"
NS_ASSUME_NONNULL_BEGIN

@interface LikeLoginCellView : UIView
- (instancetype)initWithFrame:(CGRect)frame title:(NSString*)localTitle placeholder:(NSString*)localPlaceholder keyBoardType:(NSString *)keyBoardType style:(NSString *)style height:(NSString *)theHeight withDynaBtn:(BOOL)withDynaBtn;
@property(nonatomic,retain)UILabel              *leftLabel;
@property(nonatomic,retain)WTReTextField          *rightTextFiled;
@property(nonatomic,retain)UIView               *line;
@property(nonatomic,assign)BOOL withDynaBtn;
@property(nonatomic,retain)BlockDynamicPasswordButton  *SMSBtn;

@end

NS_ASSUME_NONNULL_END
