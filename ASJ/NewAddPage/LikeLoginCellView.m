//
//  LikeLoginCellView.m
//  ASJ
//
//  Created by YYY on 2019/7/6.
//  Copyright © 2019 TS. All rights reserved.
//

#import "LikeLoginCellView.h"
#import "BlockDynamicPasswordButton.h"

@implementation LikeLoginCellView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString*)localTitle placeholder:(NSString*)localPlaceholder keyBoardType:(NSString *)keyBoardType style:(NSString *)style height:(NSString *)theHeight withDynaBtn:(BOOL)withDynaBtn;{
    
    frame = CGRectMake(0, 0, YzWidth, 60 );
    
    self = [super initWithFrame:frame];
    if (self) {
        double left =  32 ;
        double fontSize = 13 ;
        
        //
        //左标签
        //
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectZero block:^(UILabel *result) {
            result.backgroundColor = [UIColor clearColor];
            result.text = localTitle;
            result.font = [UIFont systemFontOfSize:fontSize];
        }];
        _leftLabel.userInteractionEnabled = NO;
        _leftLabel.textAlignment = NSTextAlignmentCenter;
        [_leftLabel sizeToFit];
        _leftLabel.centerY = frame.size.height / 2.0;
        _leftLabel.left = left;
        [self addSubview:_leftLabel];
        
        //
        //右输入框
        //
        _rightTextFiled = [[WTReTextField alloc]init];
        _rightTextFiled.font = [UIFont systemFontOfSize:fontSize];
        NSLog(@"%@",_rightTextFiled.font);//.SFUIText-Regular
        _rightTextFiled.delegate = self;
        _rightTextFiled.height = _leftLabel.height;
        _rightTextFiled.left = _leftLabel.right + 18;
        _rightTextFiled.width = YzWidth - _leftLabel.left  - _rightTextFiled.left;
        _rightTextFiled.centerY = _leftLabel.centerY;
        [self addSubview:_rightTextFiled];
        _rightTextFiled.placeholder = localPlaceholder;
        
        //
        //下横线
        //
        _line = [[UIView alloc]initWithFrame:CGRectZero];
        _line.backgroundColor = RGBColor_(230);
        _line.left = _leftLabel.left;
        _line.width = YzWidth - _leftLabel.left * 2;
        _line.height = 1;
        _line.bottom = frame.size.height - 3;
        _line.clipsToBounds = NO;
        [self addSubview:_line];
        self.clipsToBounds = NO;

        //
        //获取动态密码
        //
        _withDynaBtn = withDynaBtn;
        if (_withDynaBtn == YES) {

            _SMSBtn = [[BlockDynamicPasswordButton alloc] initWithFrame:CGRectMake(0, 0, 85 , 25 ) andMaxTouchDownNum:10 andTimeInterval:60 andClickedAction:^(int touchUpNum) {
                
            }];
            [_SMSBtn sizeToFit];
            _SMSBtn.right = _line.right;
            _SMSBtn.centerY = _leftLabel.centerY;
            _rightTextFiled.width -= _SMSBtn.width;
            [self addSubview:_SMSBtn];
   
        }
    
    
        LikeLoginCellView *weakObject = self;
        [self setTapActionWithBlock:^{
            if (weakObject.rightTextFiled.userInteractionEnabled == YES  && weakObject.rightTextFiled.enabled == YES && weakObject.rightTextFiled.hidden == NO) {
                [weakObject.rightTextFiled becomeFirstResponder];
            } else {
                
            }
        }];
    }
    return self;
}


@end
