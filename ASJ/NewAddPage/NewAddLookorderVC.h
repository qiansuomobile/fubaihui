//
//  NewAddLookorderVC.h
//  ASJ
//
//  Created by a1 on 2019/6/11.
//  Copyright © 2019年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewAddLookorderVC : UIViewController

//物流id
@property (nonatomic, copy)NSString *wuliuID;

//物流单号
@property (nonatomic, copy)NSString *wuliuNum;

@end

NS_ASSUME_NONNULL_END
