//
//  NewAddLookorderVC.m
//  ASJ
//
//  Created by a1 on 2019/6/11.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "NewAddLookorderVC.h"

@interface NewAddLookorderVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong ) UITableView *mainTableView;
@property (nonatomic , strong ) NSMutableArray *datasouceArray;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *wuliuNumLabel;

@property (nonatomic, strong) UILabel *kefuLabel;
@end

@implementation NewAddLookorderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"查看物流";
    
    self.mainTableView = [[UITableView alloc] init];
    self.mainTableView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 64);
    self.mainTableView.backgroundColor = [UIColor whiteColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    
    _datasouceArray = [NSMutableArray array];
    
    UIView *infoView = [[UIView alloc] init];
    infoView.frame   = CGRectMake(0, 0, YzWidth, YzWidth/4+10 +40);
    infoView.backgroundColor = [UIColor whiteColor];
    self.mainTableView.tableHeaderView = infoView;
    
    _imgView = [[UIImageView alloc] init];
    _imgView.frame = CGRectMake(10, 10, YzWidth/4 - 20, YzWidth/4 - 20);
    [_imgView setImage:[UIImage imageNamed:@"yyyGrayImg"]];
    [infoView addSubview:_imgView];
    
    NSArray *titleArray = @[@"快递单号",@"客服电话：400-000-0387"];
    
    _wuliuNumLabel = [[UILabel alloc]init];
    _kefuLabel = [[UILabel alloc]init];
    NSArray *labelArr = @[_wuliuNumLabel, _kefuLabel];
    
    for (int i = 0 ; i < titleArray.count ; i ++) {
        
        UILabel *infoLabel = labelArr[i];
        infoLabel.text     = titleArray[i];
        infoLabel.frame    = CGRectMake(YzWidth/4, i * 25 + 10, YzWidth/1.3, 25);
        [infoView addSubview:infoLabel];
        infoLabel.textColor = [UIColor grayColor];
        infoLabel.font      = [UIFont systemFontOfSize: 13];
        infoLabel.textAlignment = NSTextAlignmentLeft;
        
        if (i == 0) {
            infoLabel.textColor = [UIColor cyanColor];
            infoLabel.font      = [UIFont systemFontOfSize: 18];
            
        }
        
    }
    
    UILabel *grayColor = [[UILabel alloc] init];
    grayColor.backgroundColor = [UIColor grayColor];
    grayColor.frame = CGRectMake(0, YzWidth/4, YzWidth, 10);
    [infoView addSubview:grayColor];
    
    _wuliuNumLabel.text = [NSString stringWithFormat:@"快递单号:%@",self.wuliuNum];
    [self requestLogisticsData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.datasouceArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LookCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"LookCell"];
    }
    if (indexPath.row == 0) {
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.font      = [UIFont systemFontOfSize:16];
    }else{
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font      = [UIFont systemFontOfSize:12];
    }
    
    NSDictionary *dic = _datasouceArray[indexPath.row];
    cell.detailTextLabel.text = dic[@"time"];
    cell.textLabel.text = dic[@"context"];
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *titleView = [[UIView alloc] init];
    titleView.frame   = CGRectMake(0, 0, YzWidth, 44);
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame    = CGRectMake(15, 0, YzWidth/2, 44);
    titleLabel.text     = @"物流跟踪";
    titleLabel.backgroundColor = [UIColor whiteColor];
    [titleView addSubview:titleLabel];
    return titleView;
}

#pragma mark - data request
- (void)requestLogisticsData{
    NSDictionary *parameter = @{@"id":self.wuliuID,
                                @"nu":self.wuliuNum
                                };
    
    [NetMethod Post:FBHRequestUrl(kUrl_courier_api) parameters:parameter success:^(id responseObject) {
        NSDictionary *dataDic = responseObject;
        if (dataDic[@"success"]) {
            NSArray *dataArr = dataDic[@"data"];
            for (NSDictionary *dic in dataArr) {
                [_datasouceArray addObject:dic];
            }
            [_mainTableView reloadData];
        }else{
            [MBProgressHUD showError:dataDic[@"reason"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}
@end
