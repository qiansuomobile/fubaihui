//
//  NewAddMoneyVC.m
//  ASJ
//
//  Created by a1 on 2019/5/28.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "NewAddMoneyVC.h"

@interface NewAddMoneyVC ()

@end

@implementation NewAddMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"充值";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addNewUI];
}

-(void)addNewUI{
    for (int i = 0; i <4;  i ++) {
        
        UIButton *moneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moneyBtn.frame = CGRectMake(YzWidth/2*(i%2) - 20, 40 +YzWidth/4*(i /2), YzWidth/2, YzWidth/4);
        [self.view addSubview:moneyBtn];
        
        moneyBtn.layer.borderColor = RGBColor(29, 146, 68).CGColor;
        moneyBtn.layer.borderWidth = 0.5;
        moneyBtn.layer.cornerRadius = 4;
    }

    
    
    UIButton *addMoneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addMoneyBtn.frame     = CGRectMake(50, YzHeight - 100, YzWidth - 100, 40);
    [addMoneyBtn setTitle:@"立即充值" forState:UIControlStateNormal];
    addMoneyBtn.backgroundColor = RGBColor(27, 150, 67);
    [self.view addSubview:addMoneyBtn];
    
    [addMoneyBtn addTarget:self action:@selector(addMoneyAction) forControlEvents:UIControlEventTouchUpInside];
}
//立即充值
-(void)addMoneyAction{
    
}
@end
