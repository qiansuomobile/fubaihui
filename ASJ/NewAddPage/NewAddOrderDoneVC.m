//
//  NewAddOrderDoneVC.m
//  ASJ
//
//  Created by a1 on 2019/6/9.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "NewAddOrderDoneVC.h"

@interface NewAddOrderDoneVC ()

@end

@implementation NewAddOrderDoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单确认";
    
    NSArray *titleArray = @[@"姓名:",@"手机号:",@"预约时间: 2019 5.23"];
    for (int i = 0 ; i < 3 ; i ++) {
        
        UILabel *leftLabel = [[UILabel alloc] init];
        leftLabel.frame    = CGRectMake(10, 10+40*i, 100, 30);
        leftLabel.text     = titleArray[i];
        [self.view addSubview:leftLabel];
        if (i == 2) {
            leftLabel.frame = CGRectMake(10, 10 + 40 * i , YzWidth - 20 , 30);
        }
        
        if(i<2){
        UITextField *infoTF  = [[UITextField alloc] init];
        infoTF.frame         = CGRectMake(110, 10 + 40 * i, YzWidth - 120, 30);
        infoTF.textAlignment = NSTextAlignmentRight;
        [self.view addSubview:infoTF];
        }
        
    }
    
    
    UIButton *upLoadBut = [UIButton buttonWithType:UIButtonTypeCustom];
    upLoadBut.frame     = CGRectMake(YzWidth/2-60, YzHeight - 60, 120, 30);
    [upLoadBut setTitle:@"提交预约" forState:UIControlStateNormal];
    [upLoadBut setTintColor:RGBColor(56, 249, 64)];
    upLoadBut.layer.cornerRadius = 15;
    [self.view addSubview:upLoadBut];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
