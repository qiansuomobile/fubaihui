//
//  NewAddYuyueVC.m
//  ASJ
//
//  Created by a1 on 2019/6/7.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "NewAddYuyueVC.h"

@interface NewAddYuyueVC ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic , strong) NSArray *times;
@end

@implementation NewAddYuyueVC

- (void)viewDidLoad {
   
    [super viewDidLoad];
        
    self.view.backgroundColor = [UIColor whiteColor];

    
    
    
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.frame         = CGRectMake(0, YzWidth, YzWidth, YzWidth);
    pickerView.delegate      = self;
    pickerView.dataSource    = self;
    [self.view addSubview:pickerView];
    
    NSArray *times = @[@"7:00",@"8:00",@"9:00",@"10:00",@"11:00",@"12:00",@"13:00",@"14:00",@"15:00",@"16:00",@"17:00",@"18:00"];
    self.times = times;

    
    UIView *busniseInfoView = [[UIView alloc] init];
    busniseInfoView.frame   = CGRectMake(0, 0, YzWidth, YzWidth/3);
    [self.view addSubview:busniseInfoView];
    
    UIImageView *logoImage = [[UIImageView alloc] init];
    logoImage.frame        = CGRectMake(10, 10, YzWidth/3-20, YzWidth/3-20);
    [busniseInfoView addSubview:logoImage];
    
    UILabel *infolabel     = [[UILabel alloc] init];
    infolabel.frame        = CGRectMake(YzWidth/3, 10, YzWidth/2, YzWidth/6-10);
    [busniseInfoView addSubview:infolabel];

    
    UILabel *datelabel     = [[UILabel alloc] init];
    datelabel.frame        = CGRectMake(YzWidth/3, YzWidth/6+10, YzWidth/2, YzWidth/6);
    [busniseInfoView addSubview:datelabel];
    

}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件包含的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1; // 返回1表明该控件只包含1列
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // 由于该控件只包含一列，因此无须理会列序号参数component
    // 该方法返回teams.count，表明teams包含多少个元素，该控件就包含多少行
    return     self.times.count;
}


// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为UIPickerView
// 中指定列和列表项的标题文本
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    // 由于该控件只包含一列，因此无须理会列序号参数component
    // 该方法根据row参数返回teams中的元素，row参数代表列表项的编号，
    // 因此该方法表示第几个列表项，就使用teams中的第几个元素
    
    return [    self.times objectAtIndex:row];
}

// 当用户选中UIPickerViewDataSource中指定列和列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    // 使用一个UIAlertView来显示用户选中的列表项
    UIAlertView* alert = [[UIAlertView alloc]
                          initWithTitle:@"提示"
                          message:[NSString stringWithFormat:@"你选中的球队是：%@"
                                   , [     self.times objectAtIndex:row]]
                          delegate:nil
                          cancelButtonTitle:@"确定"
                          otherButtonTitles:nil];
    [alert show];
}

@end
