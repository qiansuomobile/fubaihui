//
//  NewBaseVC.m
//  ASJ
//
//  Created by a1 on 2019/6/7.
//  Copyright © 2019年 TS. All rights reserved.
//

#import "NewBaseVC.h"

@interface NewBaseVC ()

@end

@implementation NewBaseVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}


@end
