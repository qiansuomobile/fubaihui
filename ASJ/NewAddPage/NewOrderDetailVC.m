//
//  NewOrderDetailVC.m
//  ASJ
//
//  Created by a1 on 2019/6/7.
//  Copyright © 2019年 TS. All rights reserved.
//

@interface YzOrderDetailCell : UITableViewCell

@property (nonatomic , strong) UILabel *titleLabel1;//title
@property (nonatomic , strong) UILabel *godenlLabel;//品类
@property (nonatomic , strong) UILabel *yinLabel;//银积分
@property (nonatomic , strong) UILabel *lastMoneyLabel;//余额
@property (nonatomic, strong) UILabel *store_scoreLabel; //商家金积分

@property (nonatomic, strong) UILabel *timeLabel; //时间
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end

@implementation YzOrderDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        //        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubview];
    }
    return self;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"YzMyTuijianCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell){
        cell = [[self alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}


-(void)setUpSubview{
    
    UILabel *imageV = [[UILabel alloc]init];
    [self addSubview:imageV];
    self.titleLabel1 = imageV;
    
    
    UILabel *one = [[UILabel alloc]init];
    one.font = [UIFont systemFontOfSize:13];
    one.textAlignment = NSTextAlignmentLeft;
    [self addSubview:one];
    _godenlLabel = one;
    
    self.titleLabel1.frame = CGRectMake(35, 10, YzWidth - 60, 30);
    self.titleLabel1.textAlignment = NSTextAlignmentLeft;
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.font = [UIFont systemFontOfSize:13];
    self.timeLabel.frame = CGRectMake(35, 100, YzWidth - 70, 30);
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    
    self.yinLabel = [[UILabel alloc]init];
    self.yinLabel.font = [UIFont systemFontOfSize:13];
    self.yinLabel.textAlignment = NSTextAlignmentLeft;
    
    self.lastMoneyLabel = [[UILabel alloc]init];
    self.lastMoneyLabel.font = [UIFont systemFontOfSize:13];
    self.lastMoneyLabel.textAlignment = NSTextAlignmentLeft;
    
    self.store_scoreLabel = [[UILabel alloc]init];
    self.store_scoreLabel.font = [UIFont systemFontOfSize:13];
    self.store_scoreLabel.textAlignment = NSTextAlignmentLeft;
    
    self.yinLabel.frame         = CGRectMake(35, 40, YzWidth / 2, 30);
    self.lastMoneyLabel.frame   = CGRectMake(YzWidth / 2, 40, YzWidth / 2, 30);
    self.godenlLabel.frame      = CGRectMake(35, 70, YzWidth / 2, 30);
    self.store_scoreLabel.frame = CGRectMake(YzWidth / 2, 70, YzWidth / 2, 30);
    
    [self addSubview:self.timeLabel];
    [self addSubview:self.yinLabel];
    [self addSubview:self.lastMoneyLabel];
    [self addSubview:self.store_scoreLabel];
}

@end

#import "NewOrderDetailVC.h"

@interface NewOrderDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong ) UITableView *mainTableView;
@property (nonatomic , strong ) NSMutableArray *datasouceArray;

@end

@implementation NewOrderDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"账单明细";
    
//    self.title = @"我的推荐";
    self.mainTableView = [[UITableView alloc] init];
    self.mainTableView.frame = CGRectMake(0, 0, YzWidth, YzHeight - 64);
    self.mainTableView.backgroundColor = [UIColor whiteColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    
    [self getMyBillDataRequset];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return _datasouceArray.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"myBillDetailCell";
    YzOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[YzOrderDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSString *status;
    
    
    NSDictionary *dic = _datasouceArray[indexPath.row];
    
    if ([dic[@"status"] isEqualToString:@"1"]) {
        status = @"转入";
    }else{
        status = @"转出";
    }
    
    cell.titleLabel1.text = [NSString stringWithFormat:@"%@:%@",status, dic[@"back1"]] ;
    cell.timeLabel.text   = [self getTimeFromTimestamp:dic[@"ctime"]];
    
    cell.godenlLabel.text = [NSString stringWithFormat:@"金积分：%@",dic[@"score"]];
    cell.yinLabel.text = [NSString stringWithFormat:@"银积分：%@",dic[@"silver"]];
    cell.store_scoreLabel.text = [NSString stringWithFormat:@"商家金积分：%@",dic[@"store_score"]];
    cell.lastMoneyLabel.text = [NSString stringWithFormat:@"余额：%@",dic[@"balance"]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 130;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *dateView = [[UIView alloc] init];
    dateView.frame   = CGRectMake(0, 0, YzWidth, 40);
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.frame    = CGRectMake(30, 0, YzWidth, 40);
    dateLabel.text     = @"账单明细";
    dateLabel.textAlignment = NSTextAlignmentLeft;
    dateLabel.backgroundColor = [UIColor whiteColor];
    [dateView addSubview:dateLabel];
    return dateView;
}

#pragma MARK - URL Request
/**
 获取我的账单明细
 */
- (void)getMyBillDataRequset{
    NSDictionary *parameterDic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_my_bill) parameters:parameterDic success:^(id responseObject) {
        NSDictionary *responsDic = responseObject;
        if ([responsDic[@"code"] integerValue] == 200) {
            NSArray *dataArr = responseObject[@"data"];
            _datasouceArray = [NSMutableArray arrayWithArray:dataArr];
            [_mainTableView reloadData];
        }else{
            [MBProgressHUD showError:responsDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma MARK - CALSS
- (NSString *)getTimeFromTimestamp:(NSString *)time{
    
    //将对象类型的时间转换为NSDate类型
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    
    //设置时间格式
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //将时间转换为字符串
    
    NSString *timeStr=[formatter stringFromDate:myDate];

    return timeStr;
}

@end
