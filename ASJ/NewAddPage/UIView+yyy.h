//
//  UIView+yyy.h
//  ASJ
//
//  Created by YYY on 2019/7/3.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^PublicBlock)(id result) ;

@interface UIView (yyy)
- (instancetype)initWithFrame:(CGRect)frame block:(PublicBlock)block;//yu

@end

NS_ASSUME_NONNULL_END
