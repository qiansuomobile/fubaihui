//
//  UIView+yyy.m
//  ASJ
//
//  Created by YYY on 2019/7/3.
//  Copyright © 2019 TS. All rights reserved.
//

#import "UIView+yyy.h"


@implementation UIView (yyy)
- (instancetype)initWithFrame:(CGRect)frame block:(PublicBlock)block
{
    self = [self initWithFrame:frame];
    if (self) {
        __block UIView * ws = self;
        block(ws);
    }
    return self;
}

//-(void)dealloc{
//    
//}
@end
