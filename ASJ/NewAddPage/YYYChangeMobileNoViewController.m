//
//  YYYChangeMobileNoViewController.m
//  ASJ
//
//  Created by YYY on 2019/7/6.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYChangeMobileNoViewController.h"
#import "LikeLoginCellView.h"
@interface YYYChangeMobileNoViewController ()

@end

@implementation YYYChangeMobileNoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"修改手机号";
    self.view.backgroundColor = RGBColor_(239);
    
    LikeLoginCellView *newNo = [[LikeLoginCellView alloc] initWithFrame:CGRectZero title:@"请输入手机号:" placeholder:@"请输入11位手机号码" keyBoardType:@"" style:@"" height:@"" withDynaBtn:NO];
    [self.view addSubview:newNo];
    newNo.rightTextFiled.pattern = @"^(\\d{3}(?: )){1}(\\d{4}(?: )){1}\\d{4}$";
    newNo.rightTextFiled.keyboardType = UIKeyboardTypeNumberPad;
    newNo.top = SafeAreaHeight;
    
    
    LikeLoginCellView *sms = [[LikeLoginCellView alloc] initWithFrame:CGRectZero title:@"请输入验证码:" placeholder:@"请输入验证码" keyBoardType:@"" style:@"" height:@"" withDynaBtn:YES];
    [self.view addSubview:sms];
    sms.top = newNo.bottom ;
    sms.rightTextFiled.pattern = @"^\\d{6}$";
    sms.rightTextFiled.keyboardType = UIKeyboardTypeNumberPad;

    
    
    //验证码点击事件
    sms.SMSBtn.dynamicBtnBlock = ^(int touchUpNum) {
        
        
        
        NSDictionary *dic = @{@"phone":[newNo.rightTextFiled.text stringByReplacingOccurrencesOfString:@" " withString:@""]};
        
        [NetMethod Post:FBHRequestUrl(kUrl_send_sms) parameters:dic success:^(id responseObject) {
            NSDictionary *dic = responseObject;
            int code = [[dic objectForKey:@"code"] intValue];
            if (code ==200) {
                //发送成功
//                result(YES);
            }else{
//                result(NO);
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            }
        } failure:^(NSError *error) {
//            result(NO);
            [MBProgressHUD showError:error.description toView:self.view];
        }];
    };
    
    
    UIButton *changeBtn = [[UIButton alloc] initWithFrame:CGRectZero block:^(UIButton * result) {
        result.width = YzWidth - 50 * 2;
        result.height = 40;
        result.backgroundColor = RGBColor(76, 146, 66);
        [result setTitle:@"确定修改" forState:UIControlStateNormal];
        [self.view addSubview:result];
        result.top = sms.bottom + 55;
        result.centerX = YzWidth/2.0;
        result.layer.cornerRadius = 20;
    }];
    
    
    [changeBtn setTapActionWithBlock:^{
        [self.view endEditing:YES];
        NSDictionary *dic = @{@"uid":LoveDriverID,
                              @"oldPhone":LoveUserName,
                              @"phone":[newNo.rightTextFiled.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                              @"token":FBH_USER_TOKEN,
                              @"yzm":sms.rightTextFiled.text
                              
                              };
        [SVProgressHUD showWithStatus:@"正在加载..."];

        [NetMethod Post:FBHRequestUrl(@"/APP/User/editphone") parameters:dic success:^(id responseObject) {
            NSDictionary *dic = responseObject;
            int code = [[dic objectForKey:@"code"] intValue];
            if (code ==200) {
                //发送成功
                //                result(YES);
                [MBProgressHUD showMessag:@"手机号码修改成功" toView:self.view];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
                
                [[NSUserDefaults standardUserDefaults] setObject:[newNo.rightTextFiled.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"LoveUserName"];

            }else{
                //                result(NO);
                [MBProgressHUD showError:[responseObject objectForKey:@"returncode"] toView:self.view];
            }
            [SVProgressHUD dismiss];

        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];

            //            result(NO);
            [MBProgressHUD showError:error.description toView:self.view];
        }];
    }];
    
}



@end
