//
//  YYYChooseView.h
//  ASJ
//
//  Created by YYY on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YYYChooseViewDelegate <NSObject>
- (void)chooseStoreClass:(NSDictionary *)dic;
@end

@interface YYYChooseView : UIView
@property(nonatomic,retain)NSArray *listArr;

@property (nonatomic, weak)id<YYYChooseViewDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
