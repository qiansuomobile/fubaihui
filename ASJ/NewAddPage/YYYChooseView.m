//
//  YYYChooseView.m
//  ASJ
//
//  Created by YYY on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYChooseView.h"

@implementation YYYChooseView

- (instancetype)initWithFrame:(CGRect)frame
{
    
    frame.size.width = YzWidth;
    frame.size.height = YzHeight - SafeAreaHeight;
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        //白背景
        UIScrollView *white = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, YzWidth/2.3, frame.size.height)];
        [self addSubview:white];
        white.backgroundColor = [UIColor whiteColor];
        white.right = YzWidth;
        //回北京
        UIScrollView *gray = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,YzWidth - YzWidth/2.3, frame.size.height)];
        [self addSubview:gray];
        gray.backgroundColor = [UIColor blackColor];
        gray.left = 0;
        gray.alpha = 0.4;
        [gray setTapActionWithBlock:^{
            [self removeFromSuperview];
        }];
        //分类
        UIView *line = [[UIView alloc] initWithFrame:CGRectZero];
        line.height = 1;
        line.width = white.width - 10;
        line.backgroundColor = RGBColor(242, 243, 243);
        [white addSubview:line];
        line.centerX = white.width/2.0;
        line.centerY = 20;
        
        UILabel *feiLeiLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,0,20 )];
        feiLeiLabel.text = @" 分类 ";
        feiLeiLabel.font = [UIFont systemFontOfSize:14];
        feiLeiLabel.textAlignment = NSTextAlignmentCenter;
        [white addSubview:feiLeiLabel];
        [feiLeiLabel sizeToFit];
        feiLeiLabel.centerY = line.centerY;
        feiLeiLabel.centerX = line.centerX;
        feiLeiLabel.backgroundColor = [UIColor whiteColor];
        
        
      //下边的分类
        if (_listArr) {
            [self createWithArr:_listArr view:white];

        }else{
            [NetMethod Post:FBHRequestUrl(kUrl_get_business)
                parameters:@{}
                   success:^(id responseObject) {
                       if ([responseObject[@"code"] isEqual:@200]) {
                           
                           _listArr = responseObject[@"data"];
                           if (_listArr) {
                               [self createWithArr:_listArr view:white];
                           }
                       }
                       
                   }failure:^(NSError *error) {
                       [MBProgressHUD showSuccess:@"请检查网络连接" toView:self];
                   }];
        }
        
        

        
        
    }
    return self;
}
- (void)createWithArr:(NSArray*)arr view:(UIView *)white{
    
    float originY = 50;
    
    for (int i = 0; i < arr.count ; i++) {
        
        NSDictionary *dic = arr[i];
        
        UILabel *feiLeiLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,0,20 )];
        feiLeiLabel.text = dic[@"name"];
        feiLeiLabel.font = [UIFont systemFontOfSize:14];
        feiLeiLabel.textAlignment = NSTextAlignmentCenter;
        [white addSubview:feiLeiLabel];
        [feiLeiLabel sizeToFit];
        feiLeiLabel.top = originY;
        feiLeiLabel.centerX = white.width/2.0;
        feiLeiLabel.backgroundColor = [UIColor whiteColor];
        feiLeiLabel.userInteractionEnabled  = YES;
        [feiLeiLabel setTapActionWithBlock:^{
            [self.delegate chooseStoreClass:dic];
            [self removeFromSuperview];
        }];
        originY += 40;
    }
    

}
@end
