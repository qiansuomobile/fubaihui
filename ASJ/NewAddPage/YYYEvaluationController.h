//
//  YYYEvaluationController.h
//  ASJ
//
//  Created by YYY on 2019/7/19.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YzMyoderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYYEvaluationController : UIViewController

@property (nonatomic, strong)YzMyoderModel *model;

@end

NS_ASSUME_NONNULL_END
