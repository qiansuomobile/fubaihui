//
//  YYYEvaluationController.m
//  ASJ
//
//  Created by YYY on 2019/7/19.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYEvaluationController.h"
#import "MessagePhotoView.h"
#import "DWQRatingView.h"

@interface StarViewCell : UIView{
    
}
@property(nonatomic,retain)DWQRatingView *star;
@end
@implementation StarViewCell

- (instancetype)initWithFrame:(CGRect)frame leftLabel:(NSString *)text block:(PublicBlock)block{
    
    frame.size.height = 30;
    frame.size.width = YzWidth;
    self.backgroundColor = [UIColor whiteColor];
    self = [self initWithFrame:frame];
    if (self) {
        __block UIView * ws = self;
        
        
        //左label
        UILabel *laftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, YzWidth /2.0, MAXFLOAT) block:^(UILabel * result) {
            result.text = text;
            result.textColor = RGBColor(0, 0, 0);
            result.font = [UIFont systemFontOfSize:12];
            result.numberOfLines = 5;
            [result sizeToFit];
            result.left = 10;
//            result.top =  5 ;
            [self addSubview:result];
        }];
        _star=[[DWQRatingView alloc]init];
        _star.needIntValue=YES;   //是否整数显示，默认整数显示
        _star.canTouch=YES;//是否可以点击，默认为NO
        _star.scoreNum=@5;//星星显示个数
        _star.normalColorChain([UIColor grayColor]);
        _star.highlightColorChian(RGBColor(254, 230, 0));
        _star.frame = CGRectMake(0, 0, 100, 20);
        _star.right = YzWidth;
        [self addSubview:_star];
        block(ws);
    }
    return self;
}

@end

// self.photoView = [[MessagePhotoView
@interface YYYEvaluationController () <MessagePhotoViewDelegate>
@property(nonatomic,retain)MessagePhotoView *photoView;
@property(nonatomic,retain)UITextView *textView;
@end

@implementation YYYEvaluationController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"评价";
    self.view.backgroundColor = RGBColor(239, 239, 245);
    
    
    //
    //上面的商品信息view
    //
    UIView *topShopView = [[UIView alloc] initWithFrame:CGRectZero block:^(UIView *result) {
        result.backgroundColor = [UIColor whiteColor];
        result.height = 100;
        result.width = YzWidth;
        [self.view addSubview:result];
        
        UIView *topShopView_ = result;
        //
        //上左图
        //
        UIImageView *shopImageView = [[UIImageView alloc] initWithFrame:CGRectZero block:^(UIImageView *result) {
            result.width = 70;
            result.height = 70;
            result.left = 5;
            result.centerY = topShopView_.height/2.0;
            [result sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FBHBaseURL,self.model.goods_img]] placeholderImage:[UIImage imageNamed:@"logo"]];
            [topShopView_ addSubview:result];
        }];
        //
        //上中文字
        //
        UILabel *secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, YzWidth - shopImageView.right * 2 - 20, MAXFLOAT) block:^(UILabel * result) {
            result.text = self.model.goods_name;
            result.textColor = RGBColor(0, 0, 0);
            result.font = [UIFont systemFontOfSize:12];
            result.numberOfLines = 5;
            [result sizeToFit];
            result.left = shopImageView.right + 20;;
            result.top = shopImageView.top + 5 ;
            [topShopView_ addSubview:result];
        }];
        //
        //上右文字
        //
        UILabel *topRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0) block:^(UILabel * result) {
            result.text = [NSString stringWithFormat:@"¥%@", self.model.goods_price];
            result.textColor = RGBColor(0, 0, 0);
            result.font = [UIFont systemFontOfSize:14];
            result.numberOfLines = 1;
            [result sizeToFit];
            result.right = YzWidth - 20;;
            result.top = shopImageView.top ;
            [topShopView_ addSubview:result];
        }];
        //
        //下右文字
        //
        UILabel *bottomRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0) block:^(UILabel * result) {
            result.text = [NSString stringWithFormat:@"X%@", self.model.goods_num];
            result.textColor = RGBColor(190, 190, 190);
            result.font = [UIFont systemFontOfSize:14];
            result.numberOfLines = 1;
            [result sizeToFit];
            result.right = YzWidth - 20;;
            result.top = topRightLabel.bottom + 5 ;
            [topShopView_ addSubview:result];
        }];
        
        
    }];


    //
    //中间的写字选图片view
    //
//    UIView *middleShopView = [[UIView alloc] initWithFrame:CGRectZero block:^(UIView *result) {
//        result.backgroundColor = [UIColor whiteColor];
//        result.height = 200;
//        result.width = YzWidth;
//        result.top = topShopView.bottom + 10;
//        [self.view addSubview:result];
//
//        UIView *middleShopView_ = result;
//        //
//        //写评论的textview
//        //
//        UITextView *textView = [[UITextView alloc]initWithFrame:CGRectZero block:^(UITextView *result) {
////            result.contentInset = UIEdgeInsetsMake(10,10,10,10);
//            result.frame = CGRectMake(10, 10, YzWidth - 10 *2, 100);
////            result.height = 100;
////            result.width = YzWidth;
//            [middleShopView_ addSubview:result];
//
//
//            // _placeholderLabel
//            UILabel *placeHolderLabel = [[UILabel alloc] init];
//            placeHolderLabel.text = @"请输入内容";
//            placeHolderLabel.numberOfLines = 0;
//            placeHolderLabel.textColor = [UIColor lightGrayColor];
//            [placeHolderLabel sizeToFit];
//            [result addSubview:placeHolderLabel];
//            // same font
//            result.font = [UIFont systemFontOfSize:13.f];
//            placeHolderLabel.font = [UIFont systemFontOfSize:13.f];
//            [result setValue:placeHolderLabel forKey:@"_placeholderLabel"];
//
//        }];
//        //
//        //添加图片imageview
//        //
//        UIImageView *addImageView = [[UIImageView alloc] initWithFrame:CGRectZero block:^(UIImageView *result) {
//            result.width = 80;
//            result.height = 80;
//            result.left = 10;
//            result.top = textView.bottom;
//            [result sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"YYYAddImage"]];
//            [middleShopView_ addSubview:result];
//            [result setTapActionWithBlock:^{
//
//
//
//            }];
//        }];
//
//
//
//    }];
    
    
    if (!self.photoView)
    {
        self.photoView = [[MessagePhotoView alloc]initWithFrame:CGRectMake(0, topShopView.bottom + 10,YzWidth, 250)];
        self.photoView.imgCount = 3;
        [self.view addSubview:self.photoView];
        
        self.photoView.delegate = self;
        
        //
        //写评论的textview
        //
        UITextView *textView = [[UITextView alloc]initWithFrame:CGRectZero block:^(UITextView *result) {
            //            result.contentInset = UIEdgeInsetsMake(10,10,10,10);
            result.frame = CGRectMake(10, 10, YzWidth - 10 *2, 70);
            //            result.height = 100;
            //            result.width = YzWidth;
            [self.photoView addSubview:result];
            
            
            // _placeholderLabel
            UILabel *placeHolderLabel = [[UILabel alloc] init];
            placeHolderLabel.text = @"请输入内容";
            placeHolderLabel.numberOfLines = 0;
            placeHolderLabel.textColor = [UIColor lightGrayColor];
            [placeHolderLabel sizeToFit];
            [result addSubview:placeHolderLabel];
            // same font
            result.font = [UIFont systemFontOfSize:13.f];
            placeHolderLabel.font = [UIFont systemFontOfSize:13.f];
            [result setValue:placeHolderLabel forKey:@"_placeholderLabel"];
        }];
        _textView = textView;
    }
    
    //
    //下边的星星
    //
    StarViewCell *zh = [[StarViewCell alloc] initWithFrame:CGRectZero leftLabel:@"综合评价" block:^(StarViewCell* result) {
        result.top = self.photoView.bottom + 10;
        [self.view addSubview:result];
    }];
    StarViewCell *spms = [[StarViewCell alloc] initWithFrame:CGRectZero leftLabel:@"商品描述" block:^(StarViewCell* result) {
        result.top = zh.bottom;
        [self.view addSubview:result];
    }];
    StarViewCell *wlfw = [[StarViewCell alloc] initWithFrame:CGRectZero leftLabel:@"物流服务" block:^(StarViewCell* result) {
        result.top = spms.bottom;
        [self.view addSubview:result];
    }];
    StarViewCell *fwtd = [[StarViewCell alloc] initWithFrame:CGRectZero leftLabel:@"服务态度" block:^(StarViewCell* result) {
        result.top = wlfw.bottom;
        [self.view addSubview:result];
    }];

    UIButton *payBtn = [[UIButton alloc] initWithFrame:CGRectZero block:^(UIButton * result) {
        result.width = YzWidth - 50 * 2;
        result.height = 40;
        result.backgroundColor = RGBColor(76, 146, 66);
        [result setTitle:@"提交" forState:UIControlStateNormal];
        [self.view addSubview:result];
        result.top = fwtd.bottom + 30;
        result.centerX = YzWidth/2.0;
        result.layer.cornerRadius = 20;
    }];
    
    [payBtn setTapActionWithBlock:^{
       //点击提交
//        NSLog(@"点击提交 内容%@",_textView.text);
//        NSLog(@"点击提交 图片%@",self.photoView.ImageDatas);
//        NSLog(@"点击提交 服务态度的分数%@",fwtd.star.scoreNum);
        
        NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                    @"token":FBH_USER_TOKEN,
                                    @"id":self.model.order_id,
                                    @"goods_c":spms.star.scoreNum,
                                    @"wl":wlfw.star.scoreNum,
                                    @"service_c":fwtd.star.scoreNum,
                                    @"content":_textView.text
                                    };
        [self submitOrderAssessWithDic:parameDic];
    }];
}


//实现代理方法
-(void)addPicker:(ZYQAssetPickerController *)picker{
    
    [self presentViewController:picker animated:YES completion:nil];
}

//08.23 相机调用跳转
-(void)addUIImagePicker:(UIImagePickerController *)picker
{
    [self presentViewController:picker animated:YES completion:nil];
}
//if (self.photoView.photoMenuItems.count==0) {

#pragma mark - Url Request

//提交订单
- (void)submitOrderAssessWithDic:(NSDictionary *)dic{
    
    [self showHudInView:self.view hint:@"提交评价中"];
//    [NetMethod Post:FBHRequestUrl(kUrl_do_evaluate) parameters:dic success:^(id responseObject) {
//        [self hideHud];
//        if ([responseObject[@"code"] intValue] == 200) {
//            [MBProgressHUD showSuccess:@"评价成功" toView:self.view];
//            [self.navigationController popViewControllerAnimated:YES];
//        }else{
//            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
//        }
//    } failure:^(NSError *error) {
//        [MBProgressHUD showError:NetProblem toView:self.view];
//    }];
    
    [NetMethod Post:FBHRequestUrl(kUrl_do_evaluate) parameters:dic IMGparameters:@{@"file":self.photoView.ImageDatas} success:^(id responseObject) {
        [self hideHud];
        if ([responseObject[@"code"] intValue] == 200) {
            [MBProgressHUD showSuccess:@"评价成功" toView:self.view];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
         [MBProgressHUD showError:NetProblem toView:self.view];
    }];
    
    
    
    
}

//上传图片
- (void)uploadImgData{
//    [NetMethod Post:FBHRequestUrl(kUrl_img_upload) parameters:nil IMGparameters:nil success:^(id) {
//
//    } failure:^(NSError *) {
//        [MBProgressHUD showError:NetProblem toView:self.view];
//    }];
}

@end
