//
//  YYYJoinShopSecondViewController.m
//  ASJ
//
//  Created by YYY on 2019/7/1.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYJoinShopSecondViewController.h"
#import "DriverShopViewController.h"

#import "JZLocationConverter.h"

@interface YYYJoinShopSecondViewController ()
@property(nonatomic,retain)NSString *clatitude;
@property(nonatomic,retain)NSString *clongtude;

@property (nonatomic, retain)NSString *store_name;
@end

@implementation YYYJoinShopSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD showWithStatus:@"正在加载..."];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [NetMethod Post: FBHRequestUrl(@"/APP/Xtojoin/mer_details")
         parameters:@{@"id":self.sid}
            success:^(id responseObject) {
                
                
                NSDictionary *dic = responseObject;
                int code = [[dic objectForKey:@"code"] intValue];
                if (code == 200)
                {
                    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, 150)];

                    //左图
                    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 60,60)];
                    [img sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(dic[@"data"][@"logo"])] placeholderImage:[UIImage imageNamed:@"logo"]];
                    [topView addSubview:img];
                    
                    //右上文字
                    UILabel *_rightL = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 35, 40)];
                    _rightL.text = dic[@"data"][@"name"];
//                    _leftLabel.textColor = RGBColor(168, 169, 170);
                    _rightL.font = [UIFont systemFontOfSize:13];
                    [_rightL sizeToFit];
                    _rightL.left = img.right + 10;
                    _rightL.top = img.top;
                    [topView addSubview:_rightL];
                    _store_name = dic[@"data"][@"name"];
                    
                    NSDictionary *data = dic[@"data"];
                    UILabel *detailAddress = [[UILabel alloc] initWithFrame:CGRectMake(85, 45, YzWidth - 85 - 50, 40)];
                    detailAddress.text = [NSString stringWithFormat:@"详细地址:%@%@%@%@",data[@"province"], data[@"city"], data[@"district"], data[@"address_detail"]];
                    detailAddress.font = [UIFont systemFontOfSize:12];
                    detailAddress.numberOfLines = 2;
                    [detailAddress sizeToFit];
//                    detailAddress.top = img.top;
                    [topView addSubview:detailAddress];

                    
                    
                    
                    //右图 下拉
                    UIImageView *imgRight = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 15,10)];
                    [imgRight sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"yyyDownImg.png"]];
                    [topView addSubview:imgRight];
                    imgRight.right = YzWidth - 30;
                    imgRight.centerY = img.centerY;
                    [imgRight setTapActionWithBlock:^{
                        
                        //
                        UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
                        detailView.backgroundColor = [UIColor clearColor];
                        [self.view addSubview:detailView];
                        
                        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight)];
                        grayView.alpha = 0.4;
                        grayView.backgroundColor = [UIColor blackColor];
                        [detailView addSubview:grayView];
                        [grayView setTapActionWithBlock:^{
                            [detailView removeFromSuperview];
                        }];
                        
                        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(40, 60, YzWidth - 2*40, YzHeight -SafeAreaHeight - 2 *60)];
                        whiteView.backgroundColor = [UIColor whiteColor];
                        [detailView addSubview:whiteView];
                        
                        // 名字
                        UILabel *_rightL = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 35, 40)];
                        _rightL.text = [NSString stringWithFormat:@"%@\r简介", dic[@"data"][@"name"]];
                        //_leftLabel.textColor = RGBColor(168, 169, 170);
                        _rightL.font = [UIFont systemFontOfSize:15];
                        [_rightL sizeToFit];
                        _rightL.left = 15;
                        _rightL.top = 15;
                        [whiteView addSubview:_rightL];
                        
                        //简介 内容
                        UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, whiteView.width - 15 * 2, MAXFLOAT)];
                        descLabel.text = dic[@"data"][@"content"];
                        //_leftLabel.textColor = RGBColor(168, 169, 170);
                        descLabel.font = [UIFont systemFontOfSize:13];
                        [descLabel sizeToFit];
                        descLabel.left = 15;
                        descLabel.top = _rightL.bottom + 15;
                        [whiteView addSubview:descLabel];
                        
                        
                    }];
                    //横线
                    UIView *firstLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, 1)];
                    firstLine.backgroundColor = RGBColor(239, 239, 239);
                    [topView addSubview:firstLine];
                    firstLine.top = img.bottom + 5;
                    
                    //定位图
                    UIImageView *imgL = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, 15,20)];
                    [imgL sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"yyyLocation"]];
                    [topView addSubview:imgL];
                    imgL.left = 25;
                    imgL.top = firstLine.top + 16;
                    [imgL setTapActionWithBlock:^{
                        [self showMap];
                    }];
                    
                    
                    _clatitude = dic[@"data"][@"latitude"];
                    _clongtude = dic[@"data"][@"longitude"];
                    
                    NSString *distance;
                    if (![_clatitude isKindOfClass:[NSNull class]] && ![_clongtude isKindOfClass:[NSNull class]]) {
//                        NSString *latitude = dic[@"latitude"];
//                        NSString *longitude = dic[@"longitude"];
                        if (_clatitude.length > 0 && _clongtude.length > 0) {
                            distance =  [self locationDistanceWithLatitude:_clatitude longitude:_clongtude];
                        }else{
                            distance = @"未知距离";
                        }
                    }else{
                        distance =  @"未知距离";
                    }
                    
                    
                    //地址
                    UILabel *disLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 35, 40)];
                    disLabel.text = dic[@"data"][@"contacts"];
                    disLabel.font = [UIFont systemFontOfSize:13];
                    [disLabel sizeToFit];
                    disLabel.textColor = RGBColor(93, 93, 93);
                    disLabel.left = imgL.right + 10;
                    disLabel.top = imgL.top - 5;
                    [topView addSubview:disLabel];
                    [disLabel setTapActionWithBlock:^{
                        [self showMap];
                    }];
                    //距离
                    
                    UILabel *juliLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 35, 40)];
                    juliLabel.text = [NSString stringWithFormat:@"(距离%@km)",distance]; ;
                    juliLabel.textColor = RGBColor(168, 169, 170);
                    juliLabel.font = [UIFont systemFontOfSize:10];
                    [juliLabel sizeToFit];
                    juliLabel.left = imgL.right + 10;
                    juliLabel.top = disLabel.bottom + 5;
                    [topView addSubview:juliLabel];
                    [juliLabel setTapActionWithBlock:^{
                        [self showMap];
                    }];
                    //右图
                    
                    UIImageView *imgPhone = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, 148/2.0 , 108/2.0)];
                    [imgPhone sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"yyyPhone"]];
                    [topView addSubview:imgPhone];
                    imgPhone.right = YzWidth;
                    imgPhone.top = firstLine.top + 1;
                    [imgPhone setTapActionWithBlock:^{
                        UIWebView*callWebview =[[UIWebView alloc] init];
                        NSURL *telURL =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"tel:",dic[@"data"][@"phone"]]];// 貌似tel:// 或者 tel: 都行
                        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];//搜索;
                        //记得添加到view上
                        [self.view addSubview:callWebview];
                    }];
                    
                    //横线
                    
                    UIView *sencondLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, YzWidth, 1)];
                    sencondLine.backgroundColor = firstLine.backgroundColor;
                    [topView addSubview:sencondLine];
                    sencondLine.top = imgL.bottom + 16;
                    
                    
//
                    topView.height = sencondLine.bottom;
                    [self.view addSubview:topView];
                    
                    
                    //下面
                    DriverShopViewController *DsVC = [[DriverShopViewController alloc] init];
                    DsVC.ShopType = _ShopType;
                    DsVC.sid = _sid;
                    UIView *descView = DsVC.view;
                    descView.height -= topView.height;
                    descView.top += topView.height;
                    [self.view addSubview:descView];
                    
                }else{
                    NSString *msg = [dic objectForKey:@"msg"];
                    [MBProgressHUD showError:msg toView:self.view];
                }
                
                
                [SVProgressHUD dismiss];
                
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
                NSLog(@"error = %@",error.description);
                
            }];
    
    
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showGoodsDetail:)name:@"kNotification_show_goods_detail" object:nil];
}
- (void)showMap{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"请选择地图" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles: @"用高德地图导航", @"用百度地图导航", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}
- (void)showGoodsDetail:(NSNotification *)notification{
    
    NSDictionary *dic = notification.userInfo;
    GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
    goodDetailVC.goodsID = dic[@"goodsID"];
    goodDetailVC.shopType = self.ShopType;
    goodDetailVC.title = self.title;
    
    [self.navigationController pushViewController:goodDetailVC animated:YES];
}
#pragma mark - UIActionSheetDelegate
//根据被点击的按钮做出反应，0对应destructiveButton，之后的button依次排序
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    NSLog(@"buttonIndex %ld", (long)buttonIndex);
    
//baidumap://map/direction?origin=34.264642646862,108.95108518068&destination=40.007623,116.360582&coord_type=bd09ll&mode=driving&src=ios.baidu.openAPIdemo
    
    if (buttonIndex == 0) {
        if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"iosamap://"]]){
            
            CLLocationCoordinate2D coords = CLLocationCoordinate2DMake([_clatitude doubleValue], [_clongtude doubleValue]);
            CLLocationCoordinate2D be = [JZLocationConverter bd09ToGcj02:coords];
            NSString *urlString = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=%@&sid=BGVIS1&slat=&slon=&sname=&did=BGVIS2&dlat=%f&dlon=%f&dname=%@&dev=0&t=0",@"福百惠",be.latitude, be.longitude, _store_name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:^(BOOL success) {
                if (!success) {
                    [MBProgressHUD showSuccess:@"导航失败！" toView:self.view];
                }
            }];
        }else{
            
            [MBProgressHUD showSuccess:@"没有安装高德地图" toView:self.view];
        }
    }
    
    if (buttonIndex == 1) {
        if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"baidumap://"]]){
            NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin=我的位置&destination=name:%@|latlng:%f,%f&coord_type=bd0911&mode=driving&src=ios.fubaihui.bdmap",_store_name, [_clatitude doubleValue], [_clongtude doubleValue]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:^(BOOL success) {
                if (!success) {
                    [MBProgressHUD showSuccess:@"导航失败！" toView:self.view];
                }
            }];
        }else{
            
            [MBProgressHUD showSuccess:@"没有安装百度地图" toView:self.view];
        }
    }
}


- (NSString *)locationDistanceWithLatitude:(NSString *)latitude longitude:(NSString *)longitude{
    if (!_currentLocation) {
        return @"未知距离";
    }
    if (!latitude || !longitude) {
        return @"未知距离";
    }
    CLLocation *before=[[CLLocation alloc] initWithLatitude:[latitude doubleValue] longitude:[longitude doubleValue]];
    // 计算距离
    CLLocationDistance meters=[_currentLocation distanceFromLocation:before];
    return [NSString stringWithFormat:@"%.2f",meters/1000];
}


@end
