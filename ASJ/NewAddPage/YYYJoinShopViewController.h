//
//  YYYJoinShopViewController.h
//  ASJ
//
//  Created by YYY on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYYChooseView.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYYJoinShopViewController : UIViewController<YYYChooseViewDelegate>
@property(nonatomic,assign)BOOL isFromSearch;
@property(nonatomic,retain)NSString *searchText;

@property (nonatomic, assign)NSInteger shopType;

@end

NS_ASSUME_NONNULL_END
