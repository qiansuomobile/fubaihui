//
//  YYYJoinShopViewController.m
//  ASJ
//  //yu
//  Created by YYY on 2019/6/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYJoinShopViewController.h"
#import "YzNewAddFriendCell.h"
#import "CZHAddressPickerView.h"
#import "DriverShopViewController.h"

#import "YYYJoinShopSecondViewController.h"
#import "JZLocationConverter.h"
#import <CoreLocation/CoreLocation.h>

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

@interface ChooseCityView : UIView

@property(nonatomic , strong) UILabel       *leftLabel;
@property(nonatomic , strong) UITextField   *rightTextFiled;

@end

@implementation ChooseCityView
- (instancetype)initWithFrame:(CGRect)frame
{
    frame = CGRectMake(0, 0, YzWidth/3.0 - 20 , 40);
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        //左边文字
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 30, 40)];
        _leftLabel.text = @"省份";
        _leftLabel.textColor = RGBColor(168, 169, 170);
        _leftLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_leftLabel];
        
        
        //灰色区域  文字+下拉
        _rightTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(_leftLabel.right, 0, self.width - 60 + 15, 20) block:^(UITextField *result) {
            [result setFont:[UIFont systemFontOfSize:13]];
            result.centerY = _leftLabel.centerY;
            result.backgroundColor = RGBColor(231, 232, 233);
            result.rightViewMode =  UITextFieldViewModeAlways;
            result.userInteractionEnabled = NO;
            [self addSubview:result];
            //小箭头
            UIView *imageBackGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            imageBackGView.width = 37/3.0 + 5;
            imageBackGView.height = 21/3.0;
            UIImageView *imageV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"yyyDownLightGrayImg"] ];
            imageV.width = 37/3.0;
            imageV.height = 21/3.0;
            [imageBackGView addSubview:imageV];
            result.rightView = imageBackGView;
        }];

        
    }
    return self;
}
@end


@interface YYYJoinShopViewController ()<UITableViewDataSource,UITableViewDelegate, CLLocationManagerDelegate>
@property(nonatomic , strong) UITableView *shopTableView;
@property(nonatomic , strong) NSMutableArray *shopArray;
@property(nonatomic , assign) int page;

@property (nonatomic, strong)ChooseCityView *shengFen;
@property (nonatomic, strong)ChooseCityView *chengShi;
@property (nonatomic, strong)ChooseCityView *quXian;

@property (nonatomic, copy)NSString *shengFenID;
@property (nonatomic, copy)NSString *chengShiID;
@property (nonatomic, copy)NSString *quXianID;

@property (nonatomic, strong)UIButton *threeDotBtn;

@property(nonatomic , strong) CLLocationManager *locationmanager;//定位服务
@property(nonatomic , copy) NSString *currentCity;//当前城市
@property(nonatomic , copy) NSString *strlatitude;//经度
@property(nonatomic , copy) NSString *strlongitude;//纬度

@property (nonatomic, strong)CLLocation *currentLocation;

//店铺数据
@property (nonatomic, strong) NSMutableArray *goodsArray;

//商家数据
@property (nonatomic, strong) NSMutableArray *storeArray;

//商家分类id
@property (nonatomic, copy) NSString *garage_cid;

@end

@implementation YYYJoinShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _page = 1;
    self.view.backgroundColor = [UIColor whiteColor];
    self.garage_cid = @"";

    //shopTableView
    self.shopTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, YzHeight-SafeAreaHeight) style:UITableViewStyleGrouped];
    self.shopTableView.delegate = self;
    self.shopTableView.dataSource = self;
    self.shopTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.shopTableView.estimatedRowHeight = 0.0;
    self.shopTableView.estimatedSectionHeaderHeight = 0;
    self.shopTableView.estimatedSectionFooterHeight = 0;
    self.shopArray = [NSMutableArray arrayWithArray:@[]];
    [self.view addSubview:self.shopTableView];

    //区县那行
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, YzWidth / 4 * 3, 40)];
//    headerView.backgroundColor = [UIColor redColor];
    _shengFen = [[ChooseCityView alloc]initWithFrame:CGRectZero];
    _chengShi = [[ChooseCityView alloc]initWithFrame:CGRectZero];
    _chengShi.leftLabel.text = @"城市";
    _quXian = [[ChooseCityView alloc]initWithFrame:CGRectZero];
    _quXian.leftLabel.text = @"区县";
    
    [headerView addSubview:_shengFen];
    [headerView addSubview:_chengShi];
    [headerView addSubview:_quXian];
    _chengShi.left = _shengFen.right;
    _quXian.left = _chengShi.right;
    
    _shengFen.rightTextFiled.text = @"请选择";
    _chengShi.rightTextFiled.text = @"请选择";
    _quXian.rightTextFiled.text = @"请选择";
    
    _shengFenID = @"";
    _chengShiID = @"";
    _quXianID = @"";
    
    //三个点
    self.threeDotBtn = [[UIButton alloc] initWithFrame:CGRectZero block:^(UIButton* result) {
        result.height = 20;
        result.width = (YzWidth - _quXian.right)/2 + 30;
        result.centerY = _quXian.centerY;
        result.centerX = _quXian.right + (YzWidth - _quXian.right)/2.0 - 5;
        [result setTitle:@"分类" forState:UIControlStateNormal];
        result.titleLabel.font = [UIFont systemFontOfSize:10];
        result.backgroundColor =  RGBColor(231, 232, 233);;
        [result setTitleColor:RGBColor(147, 147, 147) forState:UIControlStateNormal];
        [headerView addSubview:result];
    }];

    _shopTableView.tableHeaderView = headerView;

    //
//    三个点点击事件
    
    YYYChooseView *chooseView = [[YYYChooseView alloc]initWithFrame:CGRectZero];
    chooseView.delegate = self;
    self.threeDotBtn.userInteractionEnabled = YES;
    
    WS(weakself);
    [_threeDotBtn setTapActionWithBlock:^{

        [weakself.view addSubview:chooseView ];
    }];
    
    //
    //区域点击事件
    //
    headerView.userInteractionEnabled = YES;
    [headerView setTapActionWithBlock:^{
        [CZHAddressPickerView areaPickerViewWithAreaBlock:^(NSDictionary *province, NSDictionary *city, NSDictionary *area) {
            _shengFen.rightTextFiled.text = province[@"region_name"];
            _chengShi.rightTextFiled.text = city[@"region_name"];
            _quXian.rightTextFiled.text = area[@"region_name"];
            
            _shengFenID = province[@"region_id"];
            _chengShiID = city[@"region_id"];
            _quXianID = area[@"region_id"];
            [self screeningMall];
        }];
    }];
  
    [self loadData];
    [self getLocation];
}

- (void)loadData{
    
    NSString *storeType;
    if (self.shopType == 1) {
        //加盟商家
        storeType = @"1";
    }else{
        storeType = @"0";
    }
    
    NSString *requestUrl = @"";
    
    NSDictionary *requsetDic = @{};
    
    
    //假数据
    //    _isFromSearch = NO;
    
    if(_isFromSearch == YES){
        requestUrl = FBHRequestUrl(@"/APP/Xone/tsearch");
        requsetDic = @{@"keyword":_searchText};
        [self.shopArray removeAllObjects];
        [self requestWithUrl:requestUrl dic:requsetDic];
    }else{
        [self.shopArray removeAllObjects];
        [self screeningMall];
    }
}

- (void)screeningMall{
     [SVProgressHUD showWithStatus:@"正在查找..."];
    NSString *storeType;
    if (self.shopType == 1) {
        //加盟商家
        storeType = @"1";
    }else{
        storeType = @"0";
    }
    
    NSDictionary *dic = @{@"type":storeType,
                          @"garage_cid":self.garage_cid,
                          @"sheng":self.shengFenID,
                          @"shi":self.chengShiID,
                          @"xian":self.quXianID
                          };
    _isFromSearch = NO;
    [NetMethod Post:FBHRequestUrl(kUrl_mall_screening) parameters:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        NSDictionary *dic = responseObject;
        int code = [[dic objectForKey:@"code"] intValue];
        if (code == 200){
            [self.shopArray removeAllObjects];
            NSArray *data =  dic[@"data"];
            
            if ([data isKindOfClass:[NSArray class]]) {
                NSString *distance;
                
                for (NSDictionary *dic in data) {
                    if (![dic[@"latitude"] isKindOfClass:[NSNull class]] && ![dic[@"longitude"] isKindOfClass:[NSNull class]]) {
                        NSString *latitude = dic[@"latitude"];
                        NSString *longitude = dic[@"longitude"];
                        if (latitude.length > 0 && longitude.length > 0) {
                            distance =  [self locationDistanceWithLatitude:latitude longitude:longitude];
                        }else{
                            distance = @"未知距离";
                        }
                    }else{
                        distance =  @"未知距离";
                    }
                    NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:dic];
                    [mDic setValue:distance forKey:@"juli"];
                    [self.shopArray addObject:mDic];
                }
            }
            self.shopArray = [NSMutableArray arrayWithArray:[self distanceSortWithStoreData:self.shopArray]];
            [self.shopTableView reloadData];
        }else{
            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"error = %@",error.description);
    }];
}

-(void)requestWithUrl:(NSString *)requestUrl dic:(NSDictionary *)requsetDic{

    [SVProgressHUD showWithStatus:@"正在搜索..."];

    [NetMethod Post:requestUrl parameters:requsetDic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        NSDictionary *dic = responseObject;
        int code = [[dic objectForKey:@"code"] intValue];
        if (code == 200){
            if (_isFromSearch) {
                NSDictionary *dataDic = responseObject[@"data"];
                // 商家
                NSArray *storeArr = dataDic[@"store"];
                if (![storeArr isKindOfClass:[NSNull class]]) {
                    _storeArray = [NSMutableArray array];
                    for (NSDictionary *dic in storeArr) {
                        
                        NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:dic];
                        [mDic setValue:@"store" forKey:@"type"];
                        
                        NSString *distance;
                        if (![dic[@"latitude"] isKindOfClass:[NSNull class]] && ![dic[@"longitude"] isKindOfClass:[NSNull class]]) {
                            NSString *latitude = dic[@"latitude"];
                            NSString *longitude = dic[@"longitude"];
                            if (latitude.length > 0 && longitude.length > 0) {
                                distance =  [self locationDistanceWithLatitude:latitude longitude:longitude];
                            }else{
                                distance = @"未知距离";
                            }
                        }else{
                            distance =  @"未知距离";
                        }
                        
                        [mDic setValue:distance forKey:@"juli"];
                        
                        
                        [_storeArray addObject:mDic];
                    }
                    _storeArray = [NSMutableArray arrayWithArray:[self distanceSortWithStoreData:_storeArray]];
                    [self.shopArray addObject:_storeArray];
                }
                
                //商品
                NSArray *goodsArr = dataDic[@"goods"];
                if (![goodsArr isKindOfClass:[NSNull class]]) {
                    _goodsArray = [NSMutableArray array];
                    for (NSDictionary *dic in goodsArr) {
                        NSMutableDictionary *d = [NSMutableDictionary dictionaryWithDictionary:dic];
                        [d setValue:@"goods" forKey:@"type"];
                        [_goodsArray addObject:d];
                    }
                    [self.shopArray addObject:_goodsArray];
                }
                [self.shopTableView reloadData];
                return;
            }
            NSArray *data =  dic[@"data"];
            
            if ([data isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in data) {
                    [self.shopArray addObject:dic];
                }
            }
            [self.shopTableView reloadData];
            
        }else{
//            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:@"无更多数据" toView:self.view];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"error = %@",error.description);
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark --- tableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_isFromSearch) {
        return self.shopArray.count;
    }
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_isFromSearch) {
        NSArray *arr = self.shopArray[section];
        return arr.count;
    }
    return self.shopArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YzWidth/4+10;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YzNewAddFriendCell *cell1 = [YzNewAddFriendCell cellWithTableView:tableView];
    
    if (!cell1) {
        cell1 =[[NSBundle mainBundle] loadNibNamed:@"YzNewAddFriendCell" owner:self options:nil].lastObject;
        [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    if (_isFromSearch) {
        NSArray *dataArray = self.shopArray[indexPath.section];
        NSDictionary *dic = dataArray[indexPath.row];
        if ([dic[@"type"] isEqualToString:@"goods"]) {
            
            NSString *str = [NSString stringWithFormat:@"%@%@",FBHBaseURL,[dic objectForKey:@"cover"]];
            [cell1.mapImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
            
            //左上标题
            cell1.mapTitleLabel.text =[dic objectForKey:@"title"];
            cell1.mapJlLabel.text = dic[@"descriptions"];
            
        }else{
            //左图
            NSString *str = [NSString stringWithFormat:@"%@%@",FBHBaseURL,[dic objectForKey:@"logo"]];
            [cell1.mapImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
            
            //左上标题
            cell1.mapTitleLabel.text =[dic objectForKey:@"name"];
            
            //右下距离
            NSString *distance = dic[@"juli"];
            if ([distance isEqualToString:@"未知距离"]) {
                cell1.mapJlLabel.text = distance;
            }else{
                cell1.mapJlLabel.text =[NSString stringWithFormat:@"%@%@",distance,@"km"];
            }
        }
    }else{
        
        NSDictionary *dic = self.shopArray[indexPath.row];
        NSString *distance = dic[@"juli"];
        
        //左图
        NSString *str = [NSString stringWithFormat:@"%@%@",FBHBaseURL,[dic objectForKey:@"logo"]];
        [cell1.mapImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        
        //左上标题
        cell1.mapTitleLabel.text =[dic objectForKey:@"name"];
        
        //右下距离
        if ([distance isEqualToString:@"未知距离"]) {
            cell1.mapJlLabel.text = distance;
        }else{
            cell1.mapJlLabel.text =[NSString stringWithFormat:@"%@%@",distance,@"km"];
        }
    }
    
    //左下详情
//    cell1.phoneNumber.text= [NSString stringWithFormat:@"%@",[self.shopArray[indexPath.row]objectForKey:@"id"]];
    
    return cell1;
}

#pragma mark - tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isFromSearch) {
        NSArray *arr = self.shopArray[indexPath.section];
        NSDictionary *dic = arr[indexPath.row];
        if ([dic[@"type"] isEqualToString:@"goods"]) {
            // 跳转商品详情
            GoodsDetailViewController *goodDetailVC = [[GoodsDetailViewController alloc] init];
            goodDetailVC.goodsID = dic[@"id"];
//            goodDetailVC.shopType = self.ShopType;
            [self.navigationController pushViewController:goodDetailVC animated:YES];
        }else{
            // 跳转商家店铺
            YYYJoinShopSecondViewController *secondVc = [[YYYJoinShopSecondViewController alloc]init];
            secondVc.ShopType = self.shopType;
            secondVc.sid = dic[@"id"];
            secondVc.title = dic[@"name"];
            secondVc.currentLocation = _currentLocation;
            [self.navigationController pushViewController:secondVc animated:YES];
        }
        return;
    }
    NSDictionary *dic = self.shopArray[indexPath.row];
    
    YYYJoinShopSecondViewController *secondVc = [[YYYJoinShopSecondViewController alloc]init];
    secondVc.ShopType = self.shopType;
    secondVc.sid = dic[@"id"];
    secondVc.title = dic[@"name"];
    secondVc.currentLocation = _currentLocation;
    [self.navigationController pushViewController:secondVc animated:YES];
    

    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0;

}

#pragma mark -Location

- (NSString *)locationDistanceWithLatitude:(NSString *)latitude longitude:(NSString *)longitude{
    if (!_currentLocation) {
        return @"未知距离";
    }
    if (!latitude || !longitude) {
        return @"未知距离";
    }
    CLLocation *before=[[CLLocation alloc] initWithLatitude:[latitude doubleValue] longitude:[longitude doubleValue]];
    // 计算距离
    CLLocationDistance meters=[_currentLocation distanceFromLocation:before];
    return [NSString stringWithFormat:@"%.2f",meters/1000];
}

-(void)getLocation
{
    //判断定位功能是否打开
    if ([CLLocationManager locationServicesEnabled]) {
        _locationmanager = [[CLLocationManager alloc]init];
        _locationmanager.delegate = self;
        [_locationmanager requestWhenInUseAuthorization];
        
        //设置寻址精度
        _locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationmanager.distanceFilter = 5.0;
        [_locationmanager startUpdatingLocation];
    }
}

#pragma mark CoreLocation delegate (定位失败)
//定位失败后调用此代理方法
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//    //设置提示提醒用户打开定位服务
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"允许定位提示" message:@"请在设置中打开定位" preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"打开定位" style:UIAlertActionStyleDefault handler:nil];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
//    [alert addAction:okAction];
//    [alert addAction:cancelAction];
//    [self presentViewController:alert animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"定位🐯 status %d", status);
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // 获取位置信息
        [_locationmanager startUpdatingLocation];
    }
}


#pragma mark 定位成功后则执行此代理方法
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [manager stopUpdatingHeading];
    //旧址
    CLLocation *currentLocation = [locations lastObject];
    
    //位置纠偏
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    CLLocationCoordinate2D be = [JZLocationConverter wgs84ToBd09:coords];
    CLLocation *current = [[CLLocation alloc] initWithLatitude:be.latitude longitude:be.longitude];
    _currentLocation = current;
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //打印当前的经度与纬度
    NSLog(@"纬度-%f,经度-%f",current.coordinate.latitude,current.coordinate.longitude);
    
    [self.shopTableView reloadData];
    //反地理编码
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (placemarks.count > 0) {
            CLPlacemark *placeMark = placemarks[0];
            _currentCity = placeMark.locality;
            if (!_currentCity) {
                _currentCity = @"无法定位当前城市";
            }
            
            /*看需求定义一个全局变量来接收赋值*/
//            NSLog(@"国家-%@",placeMark.country);//当前国家
//            NSLog(@"省-%@",placeMark.administrativeArea);
//            NSLog(@"城市-%@",placeMark.locality);//当前的城市
//            NSLog(@"位置-%@",placeMark.subLocality);//当前的位置
//            NSLog(@"街道-%@",placeMark.thoroughfare);//当前街道
//            NSLog(@"具体地址-%@",placeMark.name);//具体地址
//            if (placeMark.administrativeArea) {
//                _shengFen.rightTextFiled.text = placeMark.administrativeArea;
//            }else{
//                _shengFen.rightTextFiled.text = placeMark.locality;
//            }
//            _chengShi.rightTextFiled.text = placeMark.locality;
//            _quXian.rightTextFiled.text = placeMark.subLocality;
        }
    }];
    
}

#pragma MARK - YYYChooseViewDelegate
- (void)chooseStoreClass:(NSDictionary *)dic{
    self.garage_cid = dic[@"id"];
    [_threeDotBtn setTitle:dic[@"name"] forState:UIControlStateNormal];
    [self loadData];
    NSLog(@"choose dic %@", dic[@"name"]);
}

#pragma mark - function

- (NSMutableArray *)distanceSortWithStoreData:(NSArray *)array{
    
    if(array.count == 0){
        return [NSMutableArray array];
    }
    
    
    id temp;
    int i, j;
    NSMutableArray *arr = [NSMutableArray arrayWithArray:array];
    
    for (i=0; i < [arr count] - 1; ++i) {
        
        for (j=0; j < [arr count] - i - 1; ++j) {
            
            NSDictionary *dic1 = arr[j];
            NSDictionary *dic2 = arr[j+1];
            if (![dic1[@"juli"] isEqualToString:@"未知距离"] &&
                ![dic2[@"juli"] isEqualToString:@"未知距离"]) {
                if ([dic1[@"juli"] doubleValue] > [dic2[@"juli"] doubleValue]) {
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        
    }
    return arr;
}

@end
