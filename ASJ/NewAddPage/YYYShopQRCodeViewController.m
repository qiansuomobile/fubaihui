//
//  YYYShopQRCodeViewController.m
//  ASJ
//
//  Created by YYY on 2019/7/3.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YYYShopQRCodeViewController.h"
#import "YzGoldRechargeViewController.h"

#import "PayViewController.h"

@interface YYYShopQRCodeViewController ()

@property (nonatomic, strong) UITextField *monTexfiled;

@property (nonatomic, strong)UILabel *descLabel;

@end

@implementation YYYShopQRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商家支付码";
    self.view.backgroundColor = RGBColor(228, 228, 228);
    
    
    _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 10 - 15 * 2, MAXFLOAT) block:^(UILabel * result) {
        result.text = @"会员金积分：--";
        result.textColor = RGBColor(0, 0, 0);
        result.font = [UIFont systemFontOfSize:15];
        [result sizeToFit];
        result.numberOfLines = 1;
        result.left = 30;
        result.top = 50;
        result.width = 200;
        [self.view addSubview:result];
    }];
    
    
    UILabel *secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 10 - 15 * 2, MAXFLOAT) block:^(UILabel * result) {
        result.text = @"输入支付金额：";
        result.textColor = RGBColor(0, 0, 0);
        result.font = [UIFont systemFontOfSize:15];
        [result sizeToFit];
        result.left = _descLabel.left;
        result.top = _descLabel.bottom + 10;
        [self.view addSubview:result];
    }];
    
    _monTexfiled = [[UITextField alloc] initWithFrame:CGRectZero block:^(UITextField *result) {
        result.width = YzWidth/3.0;
        result.height = secondLabel.height + 10;
        result.centerY = secondLabel.centerY;
        [self.view addSubview:result];
        result.left = secondLabel.right + 5 ;
        result.placeholder = @"请输入";
        result.backgroundColor = RGBColor(204, 204, 204);
        result.layer.cornerRadius = 2.5;
        result.keyboardType = UIKeyboardTypeNumberPad;
        result.font = [UIFont systemFontOfSize:15];
    }];
    
    
    UIButton *payBtn = [[UIButton alloc] initWithFrame:CGRectZero block:^(UIButton * result) {
        result.width = YzWidth - 50 * 2;
        result.height = 40;
        result.backgroundColor = RGBColor(76, 146, 66);
        [result setTitle:@"确认支付" forState:UIControlStateNormal];
        [self.view addSubview:result];
        result.top = _monTexfiled.bottom + 30;
        result.centerX = YzWidth/2.0;
        result.layer.cornerRadius = 20;
    }];
    
    [payBtn setTapActionWithBlock:^{
        [self doPayStore];
    }];
    
    UIButton *jinjifenBtn = [[UIButton alloc] initWithFrame:CGRectZero block:^(UIButton * result) {
        result.width = YzWidth - 50 * 2;
        result.height = 40;
        result.backgroundColor = RGBColor(76, 146, 66);
        [result setTitle:@"去购买金积分" forState:UIControlStateNormal];
        [self.view addSubview:result];
        result.top = payBtn.bottom + 25;
        result.centerX = YzWidth/2.0;
        result.layer.cornerRadius = 20;
    }];
    
    [jinjifenBtn setTapActionWithBlock:^{
        NSLog(@"点击了去购买金积分");
        [self payGold];
    }];
    
    [self getGoldRechargeDataRequest];
}

#pragma MARK - URL Request
- (void)doPayStore{
    
    if ([_monTexfiled.text floatValue] <= 0 || !_monTexfiled.text) {
        
        [MBProgressHUD showError:@"支付金额不能为0" toView:self.view];
        return;
    }
    
    //跳转到支付页面
    PayViewController *payViewController = [[PayViewController alloc]init];
    payViewController.storePayDic = @{@"store_id":self.theID, @"price":_monTexfiled.text};
    payViewController.payPrice = [_monTexfiled.text floatValue];
    [self.navigationController pushViewController:payViewController animated:YES];
}

// 获取用户余额信息
- (void)getGoldRechargeDataRequest{
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *dataDic = resDic[@"data"];
            NSString *score = dataDic[@"score"];
            self.descLabel.text = [NSString stringWithFormat:@"会员金积分：%@", score];
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark - action
- (void)payGold{
    YzGoldRechargeViewController *vc = [[YzGoldRechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)dealloc{
    
}
@end
