//
//  AccountViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountViewController : UIViewController
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableArray *buyNum;
@property (nonatomic,strong) NSMutableArray *goodIDArr;
@property (nonatomic,assign) NSInteger caculateNumber;
@property (nonatomic,assign) float price;
@property (nonatomic,assign) BOOL isConvert;
//用来区分商城类型 0.正品  1.银积分
@property (nonatomic, copy) NSString *ShopType;

//优惠券
@property (nonatomic, strong)NSArray *coupons;
@end
