//
//  AccountViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AccountViewController.h"
#import "AddressTableViewCell.h"
#import "DetailTableViewCell.h"
#import "CountTableViewCell.h"
#import "LeaveMessageTableViewCell.h"
#import "DiscountTableViewCell.h"
#import "CaculateBottomView.h"
#import "PayViewController.h"
#import "ShopCarModel.h"
#import "YGAddressM.h"
#import "YzMyLocationVC.h"
#import "YzSetAddressVC.h"
#import "FinishPayViewController.h"

#import "ShoppingCarTableViewCell.h"
#import "ShoppingCartModel.h"
#import "ShoppingCartGoods.h"
#import "ShoppingCarHeaderView.h"
@interface AccountViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,yzLocationDelegate,UITextFieldDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) CaculateBottomView *bottomView;
//最大抵值
@property (nonatomic,assign) float maxChit;
//备注
@property (nonatomic,strong) UITextField *commentTF;
//地址数据
@property (nonatomic,strong) NSMutableArray *addressArr;
@property (nonatomic,strong) YGAddressM *addressModel;
//判断地址是否从下一个页面回传的
@property (nonatomic,strong) NSIndexPath *indexPath;

// 店铺商品汇总
@property (nonatomic,strong) NSMutableArray *malls;
@end

@implementation AccountViewController

-(void)setPrice:(float)price{
    _price = price;
    if ([_ShopType isEqualToString:@"1"]) {
        self.bottomView.priceL.text = [NSString stringWithFormat:@"%.0f银积分",_price];
    }else{
        self.bottomView.priceL.text = [NSString stringWithFormat:@"¥%0.2f",_price];
    }
}

- (void)setShopType:(NSString *)ShopType{
    _ShopType = ShopType;
}

- (void)setCoupons:(NSArray *)coupons{
    _coupons = coupons;
}


-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

-(NSMutableArray *)addressArr{
    if (!_addressArr) {
        _addressArr = [NSMutableArray array];
    }
    return _addressArr;
}

-(void)viewWillAppear:(BOOL)animated{
    if (!_indexPath) {
//        NSLog(@"%@",_ShopType);
        [self requestAddressData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self requestData];
}

//请求地址数据
-(void)requestAddressData{
    NSDictionary *addressDic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_place_list) parameters:addressDic success:^(id responseObject) {
        [self.addressArr removeAllObjects];
        NSLog(@"address:%@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            NSArray *listArr = responseObject[@"list"];
            
            if (listArr.count >= 1) {
                for (NSDictionary *dic in responseObject[@"list"]) {
                    YGAddressM *addressModel = [[YGAddressM alloc] init];
                    [addressModel setValuesForKeysWithDictionary:dic];
                    [self.addressArr addObject:addressModel];
                }
                self.addressModel = self.addressArr[0];
                //一个cell刷新
                NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
                [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
            }else{
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"设置" message:@"您还没有设置收货地址,请点击这里设置" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//                [alertView show];
            }
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


//请求数据
-(void)requestData{
    
    _malls = [NSMutableArray array];
    NSString *pay_type;
    if ([_ShopType isEqualToString:@"1"]) {
        pay_type = @"4";
    }else{
        pay_type = @"0";
    }
    
    NSDictionary *parametDic = @{@"uid":LoveDriverID,
                                 @"pay_type":pay_type,
                                 @"cart_id":_goodIDArr,
                                 @"token":FBH_USER_TOKEN};
    
    [NetMethod Post:FBHRequestUrl(kUrl_show_order) parameters:parametDic success:^(id responseObject) {
        if ([responseObject[@"code"] isEqual:@200]) {
            
            NSArray *mallArry = responseObject[@"data"];
            if (mallArry && mallArry.count > 0) {
                for (NSDictionary *dic in mallArry) {
                    ShoppingCartModel *model = [[ShoppingCartModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_malls addObject:model];
                }
            }
//            [self changeGoodsTotalPrice];
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
    
}

-(void)createUI{
    [self initNavigationBar];
    [self initTableView];
}

-(void) initNavigationBar{
    self.title = @"确定订单";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
}


- (CaculateBottomView *)bottomView{
    
    if (!_bottomView) {
        _bottomView = [[CaculateBottomView alloc] init];
        [_bottomView.submitBtn addTarget:self action:@selector(clickSubmitOrder) forControlEvents:UIControlEventTouchUpInside];
        if (self.isConvert) {
            [_bottomView.submitBtn setTitle:@"确定支付" forState:UIControlStateNormal];
        }
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left).offset(0);
            make.right.equalTo(self.view.mas_right).offset(0);
            make.bottom.equalTo(self.view.mas_bottom).offset(0);
            make.top.equalTo(self.tableView.mas_bottom).offset(0);
        }];
    }
    
    return _bottomView;
}

-(void) initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 60*KHeight, 0));
    }];
}
//点击提交订单
-(void)clickSubmitOrder{
    if (![_ShopType isEqualToString:@"1"]) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"确认支付" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alertview show];
    }else{
        [self requestFirstData];
    }
}
//订单生成接口
-(void)requestFirstData{
    //准备数据
    NSMutableArray *priceArr = [NSMutableArray array];
    NSMutableArray *sortsArr = [NSMutableArray array];
    NSMutableArray *silverArr = [NSMutableArray array];
    NSMutableArray *maxArr = [NSMutableArray array];
    NSMutableArray *nameArr = [NSMutableArray array];
    NSMutableArray *idArr = [NSMutableArray array];
    NSMutableArray *rmbArr = [NSMutableArray array];
   
    for (ShopCarModel *model in self.dataArr) {
        [priceArr addObject:model.price];
        [sortsArr addObject:model.f_sorts];
        [silverArr addObject:model.f_silver];
        [maxArr addObject:model.maxchit];
        [nameArr addObject:model.title];
        [idArr addObject:model.goodID];
        [rmbArr addObject:model.carrmb];
    }
    //post数据
    if (!self.addressModel) {
        [MBProgressHUD showSuccess:@"请填写收货地址" toView:self.view];
    }else{
        
        NSString *pay_type;
        if ([_ShopType isEqualToString:@"1"]) {
            pay_type = @"4";
        }else{
            pay_type = @"0";
        }
        
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *d in _coupons) {
            [couponArr addObject:d[@"id"]];
        }
        NSDictionary *dic = @{@"uid":LoveDriverID,
                              @"pay_type":pay_type,
                              @"plcid":self.addressModel.LocationID,
                              @"token":FBH_USER_TOKEN,
                              @"coupon":couponArr,
                              @"cart_id":_goodIDArr
                              };
        
        [NetMethod Post:FBHRequestUrl(kUrl_add_order) parameters:dic success:^(id responseObject) {
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
            if ([responseObject[@"code"] isEqual:@200]) {
                PayViewController *payVC = [[PayViewController alloc] init];
                payVC.orderID = responseObject[@"id"][@"id"];
                payVC.orderNum = responseObject[@"id"][@"ordernum"];
                NSString *p = responseObject[@"id"][@"total_pay_price"];
                payVC.payPrice = [p floatValue];
                payVC.merge = @"1";
                payVC.payType = [_ShopType integerValue];
                [self.navigationController pushViewController:payVC animated:YES];

            }
        } failure:^(NSError *error) {
            NSLog(@"error:%@",error.description);
            [MBProgressHUD showSuccess:NetProblem toView:self.view];
        }];
    }
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

//点击选取地址
-(void)clickSelectAddress{
    YzMyLocationVC *locationVC = [[YzMyLocationVC alloc] init];
    locationVC.delegate = self;
    locationVC.isAccount = YES;
    [self.navigationController pushViewController:locationVC animated:YES];
}

#pragma mark --- yzLocationDelegate
-(void)GetMylocation:(YGAddressM *)LocationModel index:(NSIndexPath *)index{
    NSLog(@"回传地址");
    self.addressModel= LocationModel;
    self.indexPath = index;
    //一个cell刷新
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];

}

#pragma mark --- UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        if (![alertView.title isEqualToString:@"确认支付"]){
            //点击取消时直接返回购物车
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        if (![alertView.title isEqualToString:@"确认支付"]){
            //点击确定
            YzMyLocationVC *locationVC = [[YzMyLocationVC alloc] init];
            locationVC.delegate = self;
            locationVC.isAccount = YES;
            [self.navigationController pushViewController:locationVC animated:YES];

        }else{
            [self requestFirstData];
        }
        
    }
}


#pragma mark --- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        YzMyLocationVC *locationVC = [[YzMyLocationVC alloc] init];
        locationVC.delegate = self;
        locationVC.isAccount = YES;
        [self.navigationController pushViewController:locationVC animated:YES];
    }
}
#pragma MAKR -- UITABLEVIEW DATASOURCE

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _malls.count + 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return 1;
    }
    ShoppingCartModel *model = _malls[section-1];
    return model.goodsArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        AddressTableViewCell *cell = [[AddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        if (self.addressModel) {
            cell.addressM = self.addressModel;
        }
        [cell.locationBtn addTarget:self action:@selector(clickSelectAddress) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    static NSString *CellIdentifier = @"goodsDetailCell";
    ShoppingCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[ShoppingCarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    ShoppingCartModel *model = _malls[indexPath.section - 1];
    ShoppingCartGoods *goods = model.goodsArr[indexPath.row];
    cell.goodsModel = goods;
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {return nil;}
    
    ShoppingCartModel *model = _malls[section - 1];
    ShoppingCarHeaderView *headerView = [[ShoppingCarHeaderView alloc] init];
    headerView.titleL.text = model.store_name;
    headerView.frame = CGRectMake(0, 0, MainScreen.width, 30);
    headerView.selectBtn.tag = section + 200;
    headerView.couponButton.hidden = YES;
//    [headerView.selectBtn addTarget:self action:@selector(clickHeaderBtn:) forControlEvents:UIControlEventTouchUpInside];
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section != 0) {
        return 115*KHeight;
    }
    return 110*KHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section != 0) {
        return 50*KHeight;
    }
    return 0;
}

#pragma mark --- UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma MARK - 
- (void)changeGoodsTotalPrice{
    int p = 0;
    for (ShoppingCartModel *model in _malls) {
        p = p + [model.store_total_price intValue];
    }
    self.price = p;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
