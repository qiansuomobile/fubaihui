//
//  FinishPayViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinishPayViewController : UIViewController
@property (nonatomic,strong) NSString *orderNum;

@property (nonatomic,assign) BOOL isConvert;

@property (nonatomic,assign) float payPrice;

@property (nonatomic,assign) NSInteger sliverPay;

@property (nonatomic,strong) UILabel *priceL;
//用来区分商城类型 1.正品  2.兑换  3.养护连锁
@property (nonatomic,assign) NSInteger ShopType;

@end
