//
//  FinishPayViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "FinishPayViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface FinishPayViewController ()
@property (nonatomic,strong) UILabel *sellerL;
@property (nonatomic,strong) UILabel *productL;
@property (nonatomic,strong) UIView *finishView;
@end

@implementation FinishPayViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBColor(238, 238, 238);
    
    [self createUI];

}

-(void)createUI{
    
    if (self.isConvert) {
       
        [self requestData];
    
    }
    
    [self initNavigationBar];
    [self initFinishPayView];
    [self initBackBtn];
    
}

-(void)requestData{
  
    NSDictionary *dic = @{@"user_id":LoveDriverID,@"orderid":self.orderNum};
    NSString *url;
    if (self.ShopType == 2) {
        url =@"APP/Shopa/goods_success";
    }else{
        url = @"APP/Shopyhls/goodj_success";
    }
    [NetMethod Post:LoveDriverURL(url) parameters:dic success:^(id responseObject) {
        
        NSLog(@"dic === %@",responseObject);
      
        if (![responseObject[@"list"] isKindOfClass:[NSNull class]]) {
           
            self.sliverPay = [responseObject[@"list"][@"rmb"] integerValue];
            if (self.ShopType == 2) {
                _priceL.text = [NSString stringWithFormat:@"%ld银积分",self.sliverPay];
            }else{
                _priceL.text = [NSString stringWithFormat:@"%ld金积分",self.sliverPay];
            }
            
            return;
        
        }
        
        [MBProgressHUD showSuccess:@"未获取到订单数据" toView:self.view];
        
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
        NSLog(@"error = %@",error.description);

    }];
}


-(void) initBackBtn{
   
    UIButton *backBtn = [[UIButton alloc] init];
    
    [backBtn setTitle:@"返回商城" forState:UIControlStateNormal];
    
    [backBtn setBackgroundColor:RGBColor(240, 67, 29)];
    
    backBtn.layer.cornerRadius = 3.0;
    
    [backBtn addTarget:self action:@selector(backToRoot) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backBtn];
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(self.view.mas_left).offset(15*Kwidth);
        make.right.equalTo(self.view.mas_right).offset(-15*Kwidth);
        make.bottom.equalTo(self.view.mas_bottom).offset(-60*KHeight);
        make.height.mas_equalTo(50*KHeight);
    }];
}

-(void)backToRoot{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)initFinishPayView{
    _finishView = [[UIView alloc] init];
    _finishView.backgroundColor = [UIColor whiteColor];
    _finishView.layer.borderWidth = 0.5;
    _finishView.layer.borderColor = [RGBColor(151, 151, 151) CGColor];
    [self.view addSubview:_finishView];
    [_finishView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(Kwidth*15);
        make.right.equalTo(self.view.mas_right).offset(-Kwidth*15);
        make.top.equalTo(self.view.mas_top).offset(Kwidth*20);
        make.height.mas_equalTo(330*KHeight);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = RGBColor(151, 151, 151);
    [_finishView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_finishView.mas_left).offset(0);
        make.right.equalTo(_finishView.mas_right).offset(0);
        make.top.equalTo(_finishView.mas_top).offset(80*KHeight);
        make.height.mas_equalTo(0.5);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wanchengzhifu"]
                              ];
    [_finishView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_finishView.mas_left).offset(90*Kwidth);
        make.top.equalTo(_finishView.mas_top).offset(20*KHeight);
        make.width.mas_equalTo(40*KHeight);
        make.height.mas_equalTo(40*KHeight);
    }];
    
    UILabel *finishL = [[UILabel alloc] init];
    finishL.text = @"完成支付";
    finishL.textColor = RGBColor(75, 166, 45);
    finishL.textAlignment = NSTextAlignmentLeft;
    finishL.font = [UIFont systemFontOfSize:24*KHeight];
    [_finishView addSubview:finishL];
    [finishL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).offset(12*Kwidth);
        make.top.equalTo(_finishView.mas_top).offset(20*KHeight);
        make.height.mas_equalTo(40*KHeight);
    }];
    
    UILabel *labelOne = [[UILabel alloc] init];
    labelOne.textAlignment = NSTextAlignmentLeft;
    labelOne.textColor = RGBColor(131, 131, 131);
    labelOne.text = @"收款商户";
    labelOne.numberOfLines = 1;
    labelOne.font = [UIFont systemFontOfSize:18*KHeight];
    [_finishView addSubview:labelOne];
    [labelOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_finishView.mas_left).offset(15*Kwidth);
        make.top.equalTo(lineView.mas_bottom).offset(25*KHeight);
        make.width.mas_equalTo(80*Kwidth);
        //make.height.mas_equalTo(60*KHeight);
    }];
    
    _sellerL = [[UILabel alloc] init];
    _sellerL.textAlignment = NSTextAlignmentLeft;
    _sellerL.lineBreakMode = NSLineBreakByWordWrapping;
    _sellerL.text = @"北京福百惠有限公司";
    _sellerL.numberOfLines = 0;
    _sellerL.font = [UIFont systemFontOfSize:20*KHeight];
    [_finishView addSubview:_sellerL];
    [_sellerL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(labelOne.mas_right).offset(15*Kwidth);
        make.top.equalTo(lineView.mas_bottom).offset(25*KHeight);
        make.right.equalTo(_finishView.mas_right).offset(-15*Kwidth);
    }];
    
    
    UILabel *labelTwo = [[UILabel alloc] init];
    labelTwo.textAlignment = NSTextAlignmentLeft;
    labelTwo.textColor = RGBColor(131, 131, 131);
    labelTwo.text = @"订单编号";
    labelTwo.numberOfLines = 1;
    labelTwo.font = [UIFont systemFontOfSize:18*KHeight];
    [_finishView addSubview:labelTwo];
    [labelTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_finishView.mas_left).offset(15*Kwidth);
        make.top.equalTo(labelOne.mas_bottom).offset(15*KHeight);
        make.width.mas_equalTo(80*Kwidth);
        //make.height.mas_equalTo(60*KHeight);
    }];
    
    _productL = [[UILabel alloc] init];
    _productL.textAlignment = NSTextAlignmentLeft;
    _productL.lineBreakMode = NSLineBreakByWordWrapping;
    _productL.text = [NSString stringWithFormat:@"%@",self.orderNum];
    _productL.numberOfLines = 2;
    _productL.font = [UIFont systemFontOfSize:22*KHeight];
    [_finishView addSubview:_productL];
    [_productL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(labelTwo.mas_right).offset(15*Kwidth);
        make.top.equalTo(labelOne.mas_bottom).offset(15*KHeight);
        make.right.equalTo(_finishView.mas_right).offset(-15*Kwidth);
    }];
    
    UILabel *labelThree = [[UILabel alloc] init];
    labelThree.textAlignment = NSTextAlignmentLeft;
    labelThree.textColor = RGBColor(131, 131, 131);
    labelThree.text = @"付款金额";
    labelThree.numberOfLines = 1;
    labelThree.font = [UIFont systemFontOfSize:18*KHeight];
    [_finishView addSubview:labelThree];
    [labelThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_finishView.mas_left).offset(15*Kwidth);
        make.top.equalTo(labelTwo.mas_bottom).offset(15*KHeight);
        make.width.mas_equalTo(80*Kwidth);
    }];
    
    _priceL = [[UILabel alloc] init];
    _priceL.textAlignment = NSTextAlignmentRight;
    _priceL.text = [NSString stringWithFormat:@"%.2f",self.payPrice];
    _priceL.font = [UIFont systemFontOfSize:28*KHeight];
    [_finishView addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelTwo.mas_bottom).offset(15*KHeight);
        make.right.equalTo(_finishView.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(60*KHeight);
    }];
    
    UILabel *rmbL = [[UILabel alloc] init];
    rmbL.text = @"¥";
    rmbL.textAlignment = NSTextAlignmentRight;
    rmbL.font = [UIFont systemFontOfSize:17*KHeight];
    [_finishView addSubview:rmbL];
    [rmbL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelTwo.mas_bottom).offset(18*KHeight);
        make.right.equalTo(_priceL.mas_left).offset(0);
        make.height.mas_equalTo(60*KHeight);
    }];
    if (self.isConvert) {
        rmbL.hidden = YES;
        if (self.ShopType == 2) {
            _priceL.text = [NSString stringWithFormat:@"%ld银积分",self.sliverPay];
        }else{
            _priceL.text = [NSString stringWithFormat:@"%ld金积分",self.sliverPay];
        }
    }
}

-(void)initNavigationBar{
    self.title = @"完成支付";
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftBtn;
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
