//
//  PayViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"
#import "AliPayTool.h"

@interface PayViewController : UIViewController
//判断页面是否从购买金币页面跳转的
@property (nonatomic,assign) BOOL ifOrNoToBuyGold;

//订单id
@property (nonatomic,strong) NSString *orderID;

//订单编号
@property (nonatomic ,copy)NSString *orderNum;
//实际支付价格
@property (nonatomic,assign) float payPrice;

@property (nonatomic,strong) OrderModel *orderModel;

//支付方式 0.现金、金积分 1.银积分

@property (nonatomic,assign) NSInteger payType;

// 订单列表付0 购物车1
@property (nonatomic, copy) NSString *merge;


// 如果是商家扫码支付，传这个参数
@property (nonatomic, strong)NSDictionary *storePayDic;


@end
