//
//  PayViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "PayViewController.h"
#import "PayWayTableViewCell.h"
#import "FinishPayViewController.h"
//#import "DataSigner.h"
#import "GBWXPayManager.h"
#import "YzGoldPayViewController.h"

@interface PayViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *footerView;
@property (nonatomic,strong) UIButton *payBtn;
@property (nonatomic,strong) NSMutableArray *btnArr;

//当前拥有金币数
@property (nonatomic,strong) NSString *jifen;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,strong) NSMutableArray *titleArr;

//当前选择支付方式按钮
@property (nonatomic,strong) UIButton *currentBtn;
//支付方式 0.支付宝  1.微信  2.金积分  3.银积分
@property (nonatomic,assign) NSInteger payWay;

@property (nonatomic, strong)UIImageView *payIconImgView;

@end

@implementation PayViewController

-(NSMutableArray *)titleArr{
    if (!_titleArr) {
        if (self.payType == 1) {
            //银积分
            _titleArr = [NSMutableArray arrayWithObject:@"银积分支付"];
            return _titleArr;
        }
        _titleArr = [NSMutableArray arrayWithObjects:@"微信支付",@"支付宝支付",@"金积分支付", nil];
    }
    return _titleArr;
}

-(NSMutableArray *)imageArr{
    if (!_imageArr) {
        if (self.payType == 1) {
            //银积分
            _imageArr = [NSMutableArray arrayWithObject:@"pay_silver"];
            return _imageArr;
        }
        _imageArr = [NSMutableArray arrayWithObjects:@"pay_wechat",@"pay_alipay",@"pay_gold", nil];
    }
    
    return _imageArr;
}


-(NSMutableArray *)btnArr{
    if (!_btnArr) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}

-(void)setPayPrice:(float)payPrice{
  
    _payPrice = payPrice;
    
    //一个cell刷新
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:2];
    
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    [self createUI];
//    [self requestData];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AliPayResult:) name:@"AliPayResult" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(YZWXpayresult:) name:@"WXpayresult" object:nil];
}

-(void)createUI{
    [self initNavigationBar];
    [self initFooterView];
    [self initTableView];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(void)initNavigationBar{
    self.title = @"支付确认";
    self.payWay = -1;
}

-(void)initFooterView{
    _footerView = [[UIView alloc] init];
  
    UIView *lineView = [[UIView alloc] init];
    
    lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [_footerView addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_footerView.mas_left).offset(0);
        make.right.equalTo(_footerView.mas_right).offset(0);
        make.height.mas_equalTo(10*KHeight);
        make.top.equalTo(_footerView.mas_top).offset(0);
    }];
    
    _payBtn = [[UIButton alloc] init];
    [_payBtn setBackgroundColor:RGBColor(252, 70, 30)];
    _payBtn.layer.cornerRadius = 4.0;
    if (self.payType == 1) {
        [_payBtn setTitle:[NSString stringWithFormat:@"确认支付%0.2f银积分",self.payPrice] forState:UIControlStateNormal];
    }else{
        [_payBtn setTitle:[NSString stringWithFormat:@"确认支付%0.2f元",self.payPrice] forState:UIControlStateNormal];
    }
    
    [_payBtn addTarget:self action:@selector(finishPay) forControlEvents:UIControlEventTouchUpInside];
    [_footerView addSubview:_payBtn];
    [_payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_footerView.mas_left).offset(15*Kwidth);
        make.right.equalTo(_footerView.mas_right).offset(-15*Kwidth);
        make.bottom.equalTo(_footerView.mas_bottom).offset(-70*KHeight);
        make.height.mas_equalTo(KHeight*50);
    }];
}

-(void)finishPay{
    
    if (self.payWay == 0) {
        if (self.payType == 1) {
            // 银积分支付
            [self doGoldPay];
            return;
        }
        //微信支付
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
            [self doWxPay];
        }else{
            [MBProgressHUD showSuccess:@"您的手机没有安装微信" toView:self.view];
        }
        
    }else if (self.payWay == 1){
        //支付宝支付
        [self doAlipayPay];
    }else if(self.payWay  == 2){
        //金积分支付
        [self doGoldPay];
    }else{
        [MBProgressHUD showError:@"请选择支付方式" toView:self.view];
    }
}

-(void)AliPayResult:(NSNotification *)notification{
    
    NSInteger resultStatus = [notification.object[@"resultStatus"] integerValue];
   
    
    switch (resultStatus) {
        case 9000:{
            [SVProgressHUD showSuccessWithStatus:@"支付宝支付成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];

//            [self aliPayNotif];
        }
            break;
        case 6001:{
            [MBProgressHUD showSuccess:@"用户中途取消支付" toView:self.view];
        }
            break;
        case 6000:{
            [MBProgressHUD showSuccess:@"网络连接超时" toView:self.view];
        }
            break;
        case 4000:{
            [MBProgressHUD showSuccess:@"订单处理失败" toView:self.view];
        }
            break;
        default:
            break;
    }
}


-(void)YZWXpayresult:(NSNotification* )notification{
    
    NSString* result = notification.object;
   
    NSLog(@" 支付结果   1 0 ********** %@",result);
    
    if([result isEqualToString:@"1"]){
        
//        [MBProgressHUD showSuccess:@"支付成功" toView:self.view];
        [SVProgressHUD showSuccessWithStatus:@"微信支付成功"];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
//        [self wxPayNotif];
    }else{
        //在这里写支付失败之后的回调操作
        [MBProgressHUD showSuccess:@"支付失败" toView:self.view];

        //                [YGAlertTool showAlert:@"微信支付失败"];
        //[self.view makeToast:@"微信支付失败" duration:1 position:@"center"];
    }
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark --- UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.imageArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PayWayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"payStyleCell"];
    if (!cell) {
        cell = [[PayWayTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.titleStr = self.titleArr[indexPath.row];
    cell.imageStr = self.imageArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedBtn.tag = 100 + indexPath.row;
    [self.btnArr addObject:cell.selectedBtn];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PayWayTableViewCell *cell = (PayWayTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selectedBtn.selected = YES;
    
    self.payWay = indexPath.row;
    if (indexPath.row == 0 || indexPath.row == 1) {
        //现金支付
        if (self.payType == 1) {
            _payIconImgView.image = [UIImage imageNamed:@"pay_silver"];
        }else{
            _payIconImgView.image = [UIImage imageNamed:@"pay_moeny"];
        }
       
    }else{
        //金积分支付
        _payIconImgView.image = [UIImage imageNamed:@"pay_gold"];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    PayWayTableViewCell *cell = (PayWayTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selectedBtn.selected = NO;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     return 60 * KHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return _footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
     return 140 * KHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    
    _payIconImgView = [[UIImageView alloc]init];
    _payIconImgView.image = [UIImage imageNamed:@"pay_moeny"];
    [headerView addSubview:_payIconImgView];
    
    UILabel *priceLabel = [[UILabel alloc]init];
    priceLabel.font = [UIFont systemFontOfSize:35.0f];
    priceLabel.text = [NSString stringWithFormat:@"%.2f",_payPrice];
    [headerView addSubview:priceLabel];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(headerView.mas_bottom).offset(-20.0f);
        make.centerX.equalTo(headerView.mas_centerX).offset(0);
        make.height.mas_equalTo(30);
    }];
    
    [_payIconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(headerView.mas_bottom).offset(-20.0f);
        make.right.equalTo(priceLabel.mas_left).offset(-10.0f);
        make.width.offset(15.0f);
        make.height.offset(15.0f);
    }];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 120.0;
}

#pragma mark --- UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        //点击确定
        if ([self.orderModel.scores integerValue] >= [self.jifen integerValue]) {
            //最大抵现大于用户积分
            self.payPrice = [self.orderModel.rmb floatValue]-[self.jifen floatValue];
        }else{
            //最大抵现小于用户积分
            self.payPrice = [self.orderModel.rmb floatValue]-[self.orderModel.scores floatValue];
        }

        self.payType = 0;
    }else{
        //点击取消
        self.payPrice = [self.orderModel.rmb floatValue];
        self.currentBtn.selected = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma MARK - SHOW VIEW CONTROLLER

//金积分支付
- (void)doGoldPay{
    
    if (self.storePayDic) {
        [self setSignWithType:3];
        return;
    }
    
    YzGoldPayViewController *goldPayViewController = [[YzGoldPayViewController alloc]init];
    goldPayViewController.orderID = _orderID;
    goldPayViewController.price = _payPrice;
    goldPayViewController.shopType = self.payType;
    goldPayViewController.merge = self.merge;
    [self.navigationController pushViewController:goldPayViewController animated:YES];
}

//银积分支付
- (void)doSliverPay{
    
}


#pragma mark - PAY

//微信支付
-(void)doWxPay{
    
    
    if (self.storePayDic) {
        [self setSignWithType:2];
        return;
    }
    
    
    NSDictionary *payDic = @{
                             @"ordernum":_orderNum,
                             @"money":[NSString stringWithFormat:@"%.2f",_payPrice],
                             @"act":@"order"
                             };
    [NetMethod Post:FBHRequestUrl(kUrl_pay_wxpay) parameters:payDic success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        
        if ([dict[@"errorCode"] isEqual:@0]) {
            self.orderNum = dict[@"out_trade_no"];
            NSDictionary *responesData = dict[@"responseData"];
            NSDictionary *appDic = responesData[@"app_response"];
            //调起微信支付
            [AliPayTool showWxPayWithAppDic:appDic];
        }else{
            NSLog(@"请求微信支付签名错误⚠️ %@",dict[@"errorMsg"]);
            [MBProgressHUD showError:dict[@"errorMsg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

// 支付宝支付
- (void)doAlipayPay{
    if (self.storePayDic) {
        [self setSignWithType:1];
        return;
    }

    NSDictionary *dic = @{@"out_trade_no":_orderNum,
                          @"total_amount":[NSString stringWithFormat:@"%.2f",_payPrice],
                          @"subject":@"福百惠商城",
                          @"body":@"",
                          @"act":@"order"
                          };
    [NetMethod Post:FBHRequestUrl(kUrl_pay_alipay) parameters:dic success:^(id responseObject) {
        NSDictionary *jsonDict = responseObject;
        if ([jsonDict[@"code"] integerValue] == 200) {
            NSDictionary *dataDic = jsonDict[@"data"];
            NSString *signedString = dataDic[@"response"];
            self.orderNum = dataDic[@"out_trade_no"];
            if (signedString != nil) {
                //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
                NSString *appScheme = @"FBHALIPAY";
                // NOTE: 调用支付结果开始支付
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:nil];
                });
            }
        }else{
             [MBProgressHUD showError:@"请求支付宝支付错误" toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark - 商家扫码支付
//获取签名 1 支付宝 2 微信 3 金积分
- (void)setSignWithType:(NSInteger)type{
    //获取签名
    NSDictionary *parameters = @{@"uid":LoveDriverID,
                                 @"token":FBH_USER_TOKEN,
                                 @"store_id":self.storePayDic[@"store_id"],
                                 @"price":self.storePayDic[@"price"],
                                 @"pay_type":[NSNumber numberWithInteger:type],
                                 @"act":@"saoma"
                                 };
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetMethod Post:FBHRequestUrl(kUrl_get_sign) parameters:parameters success:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responseObject[@"code"] intValue] == 200) {
            
            NSString *sign = responseObject[@"data"];
            [self payString:sign type:type];
        }else{
            [MBProgressHUD showError:@"获取签名失败" toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}


//支付
- (void)payString:(NSString *)sign type:(NSInteger)type{
    
    NSDictionary *p = @{@"uid":LoveDriverID,
                        @"token":FBH_USER_TOKEN,
                        @"store_id":self.storePayDic[@"store_id"],
                        @"price":self.storePayDic[@"price"],
                        @"sign":sign,
                        @"pay_type":[NSNumber numberWithInteger:type],
                        @"act":@"saoma"
                        };
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetMethod Post:FBHRequestUrl(kUrl_pay_store) parameters:p success:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSDictionary *dic = responseObject;
        if (type == 1) {
            if ([dic[@"code"] integerValue] == 200) {
                NSDictionary *dataDic = dic[@"data"];
                NSString *signedString = dataDic[@"response"];
                self.orderNum = dataDic[@"out_trade_no"];
                if (signedString != nil) {
                    //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
                    NSString *appScheme = @"FBHALIPAY";
                    // NOTE: 调用支付结果开始支付
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:nil];
                    });
                }
            }else{
                [MBProgressHUD showError:dic[@"msg"] toView:self.view];
            }
        }else if (type == 2) {
            //微信支付
            
            if ([dic[@"errorCode"] isEqual:@0]) {
                self.orderNum = dic[@"out_trade_no"];
                NSDictionary *responesData = dic[@"responseData"];
                NSDictionary *appDic = responesData[@"app_response"];
                //调起微信支付
                [AliPayTool showWxPayWithAppDic:appDic];
            }else{
                NSLog(@"请求微信支付签名错误⚠️ %@",dic[@"errorMsg"]);
                [MBProgressHUD showError:dic[@"errorMsg"] toView:self.view];
            }
        }else if (type == 3) {
            if ([dic[@"code"] intValue] == 200) {
                [SVProgressHUD showSuccessWithStatus:
                [NSString stringWithFormat:@"支付%@金积分成功",self.storePayDic[@"price"]]];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showError:dic[@"msg"] toView:self.view];
            }
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

@end
