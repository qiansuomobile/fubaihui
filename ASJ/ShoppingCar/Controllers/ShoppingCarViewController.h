//
//  ShoppingCarViewController.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ShoppingCartType) {
    ShoppingCartTypeFBH,                // 福百惠商城购物车
    ShoppingCartTypeSilver = 4,         // 银积分商城购物车
    ShoppingCartTypeStore = 5           //加盟商家购物车
};

@interface ShoppingCarViewController : UIViewController
@property (nonatomic,assign) BOOL twoPage;
//兑换商城？
@property (nonatomic,assign) BOOL isConvert;

/**
 用来区分商城类型 0.福百惠商城  4.银积分商城 5.加盟商家
 */
@property (nonatomic, assign) ShoppingCartType type;
@end
