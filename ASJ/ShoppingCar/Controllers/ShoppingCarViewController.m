//
//  ShoppingCarViewController.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/22.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ShoppingCarViewController.h"
#import "ShoppingCarTableViewCell.h"
#import "ShoppingCarHeaderView.h"
#import "ShopCarBottomView.h"
#import "AccountViewController.h"
#import "ShopCarModel.h"
#import "LoginViewController.h"

#import "ShoppingCartModel.h"
#import "ShoppingCartGoods.h"

#import "YzMyCouponTableViewController.h"

#import "YzHeaderFooterView.h"

@interface ShoppingCarViewController ()<UITableViewDelegate,UITableViewDataSource,ShoppingCarTableViewCellDelegate, YzMyCouponDelegate>
@property (nonatomic,strong) UITableView *tableView;
//计算当前购物车商品价格
@property (nonatomic,assign) float price;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,strong) UIButton *buyBtn;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) ShopCarBottomView *bottomView;
@property (nonatomic,strong) NSMutableArray *cells;

@property (nonatomic,strong) NSMutableArray *dataArr;
//购买数量数组
@property (nonatomic,strong) NSMutableArray *buyNumArr;
//购买商品总数量
@property (nonatomic,assign) NSInteger caculateNumber;
@property (nonatomic,strong) UISegmentedControl *segmentedControl;

//判断在未登录情况下,是否跳转过登录页面
@property (nonatomic,assign) BOOL isPush;

//商家店铺
@property (nonatomic, strong)NSMutableArray *malls;

//当前去选择的商家店铺
@property (nonatomic, assign)NSInteger selectMallRow;

//已选择优惠券
@property (nonatomic, strong)NSMutableArray *selectCouponArr;
@end

@implementation ShoppingCarViewController

-(void)setCaculateNumber:(NSInteger)caculateNumber{
    _caculateNumber = caculateNumber;
    [_bottomView.buyBtn setTitle:[NSString stringWithFormat:@"结算(%ld)",(long)caculateNumber] forState:UIControlStateNormal];
}

-(void)setPrice:(float)price{
    _price = price;
    if (_type == ShoppingCartTypeSilver) {
         _bottomView.priceL.text = [NSString stringWithFormat:@"%0.0f银积分",price];
    }else{
        _bottomView.priceL.text = [NSString stringWithFormat:@"¥%0.2f",price];
    }
}

-(NSMutableArray *)buyNumArr{
    if (!_buyNumArr) {
        _buyNumArr = [NSMutableArray array];
    }
    return _buyNumArr;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];;
    }
    return _dataArr;
}

-(NSMutableArray *)cells{
    if (!_cells) {
        _cells = [NSMutableArray array];
    }
    return _cells;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    if (LoveDriverID) {
        [self requestData];

    }else{
        if (self.isPush) {
            [MBProgressHUD showSuccess:@"您还没有登录" toView:self.view];
        }else{
            self.isPush = YES;
            LoginViewController *vc = [[LoginViewController alloc]init];

            YzNavigationVC *NaVC = [[YzNavigationVC alloc]initWithRootViewController:vc];

            [self presentViewController:NaVC animated:YES completion:nil];
        }
    }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _selectCouponArr = [NSMutableArray array];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self createUI];
    
}

-(void)createUI{
    [self initNavigationBar];
    [self initBottomView];
    [self initTableView];
}

#pragma mark - Cart Data Request
//加载购物车数据
-(void)requestData{
   
    if (!LoveDriverID) {
        [MBProgressHUD showSuccess:@"您还没有登录" toView:self.view];
        return;
    }
    
    [self loadShoppingDataWithType:self.type];
}

// 福百惠商城购物车
- (void)loadShoppingDataWithType:(ShoppingCartType)type{
    
    _malls = [NSMutableArray array];
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"token":FBH_USER_TOKEN,
                          @"type":[NSNumber numberWithInteger:type]};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetMethod Post:FBHRequestUrl(kUrl_cart_list) parameters:dic success:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responseObject[@"code"] isEqual:@200]) {
            NSArray *mallArry = responseObject[@"data"];
            if (mallArry && mallArry.count > 0) {
                for (NSDictionary *dic in mallArry) {
                    if (dic.allKeys.count > 1) {
                        ShoppingCartModel *model = [[ShoppingCartModel alloc]init];
                        [model setValuesForKeysWithDictionary:dic];
                        [_malls addObject:model];
                    }
                }
            }
            //刷新界面
            [self changeGoodsTotalPrice];
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:NetProblem toView:self.view];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)requestDelegateShoppingDataWithShopID:(NSString *)shopID{
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"token":FBH_USER_TOKEN,
                          @"id":shopID
                          };
    [NetMethod Post:FBHRequestUrl(kUrl_del_goods) parameters:dic success:^(id responseObject) {
        if ([responseObject[@"code"] isEqual:@200]) {
            [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
        }else{
             [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
         [MBProgressHUD showSuccess:NetProblem toView:self.view];
    }];
}


-(void)initNavigationBar{
    //self.title = @"购物车";
    _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"福百惠商城",@"银积分商城",@"加盟商家"]];
//    if (self.isConvert) {
//        if (self.ShopType == 2) {
//            self.shopType1 = @"银积分商城";
//        }else{
//            self.shopType1 = @"金积分商城";
//        }
//    }else{
//        self.shopType1 = @"正品商城";
//    }
    //设置默认选择
    if (_type == 0) {
        //福百惠商城
        _segmentedControl.selectedSegmentIndex = 0;
    }else if(_type == 4){
        //银积分购物车
        _segmentedControl.selectedSegmentIndex = 1;
    }else{
        //加盟商家
        _segmentedControl.selectedSegmentIndex = 2;
    }
    
    self.navigationItem.titleView = _segmentedControl;
    _segmentedControl.tintColor = [UIColor whiteColor];
    [_segmentedControl addTarget:self action:@selector(clickSegment:) forControlEvents:UIControlEventValueChanged];
    
//    UIButton *backBtn = [[UIButton alloc] init];
//    backBtn.frame = CGRectMake(0, 0, 15, 20);
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    backBtn.imageView.contentMode = UIViewContentModeScaleToFill;
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    if (self.twoPage) {
//        self.navigationItem.leftBarButtonItem = leftBtn;
//    }
}

-(void)clickSegment:(UISegmentedControl *)sender{

    if (sender.selectedSegmentIndex == 0) {
        self.type = ShoppingCartTypeFBH;
        self.isConvert = NO;
        [self requestData];
    }else if(sender.selectedSegmentIndex == 1){
        self.type = ShoppingCartTypeSilver;
        self.isConvert = YES;
        [self requestData];
    }else{
        self.type = ShoppingCartTypeStore;
        self.isConvert = NO;
        [self requestData];
    }
}

-(void)clickBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initBottomView{
    _bottomView = [[ShopCarBottomView alloc] init];
    [_bottomView.buyBtn addTarget:self action:@selector(clickBuy) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (!self.twoPage) {
            make.left.equalTo(self.view.mas_left).offset(0);
            make.right.equalTo(self.view.mas_right).offset(0);
            make.height.mas_equalTo(55*KHeight);
            make.bottom.equalTo(self.view.mas_bottom).offset(-SafeAreaBottom -  49);
        }else{
            make.left.equalTo(self.view.mas_left).offset(0);
            make.right.equalTo(self.view.mas_right).offset(0);
            make.bottom.equalTo(self.view.mas_bottom).offset(-SafeAreaBottom);
            make.height.mas_equalTo(55*KHeight);
        }
        
    }];
}

-(void)initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left).offset(0);
            make.right.equalTo(self.view.mas_right).offset(0);
            make.top.equalTo(self.view.mas_top).offset(0);
            make.bottom.equalTo(_bottomView.mas_top).offset(0);
    }];
}

//点击结算
-(void)clickBuy{
    //先判断购物车中是否有商品
    if (self.caculateNumber == 0) {
        [MBProgressHUD showSuccess:@"请先将商品添加到购物车中" toView:self.view];
    }else{
        //准备数据
        NSMutableArray *idArr = [NSMutableArray array];
        for (ShoppingCartModel *model in _malls) {
            for (ShoppingCartGoods *goods in model.goodsArr) {
                 [idArr addObject:goods.sc_id];
            }
        }
        
        //跳转
        AccountViewController *accountVC = [[AccountViewController alloc] init];
//        accountVC.buyNum = self.buyNumArr;
        if (self.type == ShoppingCartTypeSilver) {
            accountVC.ShopType = @"1";
        }
        accountVC.goodIDArr = idArr;
        accountVC.coupons = [NSArray arrayWithArray:_selectCouponArr];
        accountVC.price = _price;
        [self.navigationController pushViewController:accountVC animated:YES];
        
        [_selectCouponArr removeAllObjects];
    }
    
}



//点击cell上的selectedBtn
-(void)clickCellBtn:(UIButton *)sender{
    sender.selected = !sender.selected;
}

//点击headerView上的selectedBtn
-(void)clickHeaderBtn:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
        //选中状态
        for (ShoppingCarTableViewCell *cell in self.cells) {
            if (cell.section == sender.tag - 200) {
                cell.selectBtn.selected = YES;
            }
        }
    }else{
        //未选中状态
        for (ShoppingCarTableViewCell *cell in self.cells) {
            if (cell.section == sender.tag - 200) {
                cell.selectBtn.selected = NO;
            }
        }
    }
    
}

#pragma mark --- UITableViewDelegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return _malls.count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    ShoppingCartModel *model = _malls[section];
    return model.goodsArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 定义cell标识  每个cell对应一个自己的标识
    static NSString *CellIdentifier = @"goodsDetailCell";
    ShoppingCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[ShoppingCarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    cell.idx = indexPath;
    ShoppingCartModel *model = _malls[indexPath.section];
    ShoppingCartGoods *goods = model.goodsArr[indexPath.row];
    cell.goodsModel = goods;
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    YzHeaderFooterView  *tableViewheaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"tableViewHeaderView"];
    ShoppingCartModel *model = _malls[section];
    if (!tableViewheaderView) {
        tableViewheaderView = [[YzHeaderFooterView alloc] initWithReuseIdentifier:@"tableViewHeaderView"];
    }
    
    tableViewheaderView.hView.selectBtn.tag = section + 200;
    tableViewheaderView.hView.couponButton.tag = section + 200;
    [tableViewheaderView.hView.selectBtn addTarget:self action:@selector(clickHeaderBtn:) forControlEvents:UIControlEventTouchUpInside];
    [tableViewheaderView.hView.couponButton addTarget:self action:@selector(showMyMallCoupon:) forControlEvents:UIControlEventTouchUpInside];
    
    tableViewheaderView.hView.titleL.text = model.store_name;
    if ([model.store_id isEqualToString:@"0"]) {
        tableViewheaderView.hView.couponButton.hidden = YES;
    }else{
        tableViewheaderView.hView.couponButton.hidden = NO;
    }
    
    return tableViewheaderView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115*KHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50*KHeight;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
    //删除
    WS(weakSelf);
    UIContextualAction *deleteRowAction = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleDestructive title:@"delete" handler:^(UIContextualAction * _Nonnull action, __kindof UIView * _Nonnull sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
        
        ShoppingCartModel *model = weakSelf.malls[indexPath.section];
        ShoppingCartGoods *goods = model.goodsArr[indexPath.row];
        [weakSelf requestDelegateShoppingDataWithShopID:goods.sc_id];
        NSMutableArray *goosArr = [NSMutableArray arrayWithArray:model.goodsArr];
        [goosArr removeObject:goods];
        model.goodsArr = goosArr;
        
        if(model.goodsArr.count > 0){
            [weakSelf.malls replaceObjectAtIndex:indexPath.section withObject:model];
        }else{
            [weakSelf.malls removeObjectAtIndex:indexPath.section];
        }
        
        
        completionHandler (YES);
        [weakSelf changeGoodsTotalPrice];
        [weakSelf.tableView reloadData];
    }];
    deleteRowAction.title = @"删除商品";
    UISwipeActionsConfiguration *config = [UISwipeActionsConfiguration configurationWithActions:@[deleteRowAction]];
    return config;
}

#pragma mark --- ShoppingCarTableViewCellDelegate

//监听购买数量变化
-(void)getBuyNumber:(NSInteger)buyNum atRow:(NSIndexPath *)idx{
    
    ShoppingCartModel *model = _malls[idx.section];
    ShoppingCartGoods *goods = model.goodsArr[idx.row];
    
    NSDictionary *dic = @{@"uid":LoveDriverID,
                          @"goods_id":goods.g_id,
                          @"number":[NSNumber numberWithInteger:buyNum],
                          @"token":FBH_USER_TOKEN
                          };
    
    [NetMethod Post:FBHRequestUrl(kUrl_setinc_goods) parameters:dic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [self requestData];
        }else{
            [MBProgressHUD showError:responseObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

-(void)dealloc{
    [[[AFHTTPRequestOperationManager manager]operationQueue] cancelAllOperations];
}

#pragma mark -ACTION

/**
 改变购物车商品总价
 */
- (void)changeGoodsTotalPrice{
    float p = 0;
    self.caculateNumber = 0;
    for (ShoppingCartModel *model in _malls) {
        for (ShoppingCartGoods *goods in model.goodsArr) {
            p = p + [goods.subtotal floatValue];
            self.caculateNumber ++;
        }
    }
    
    if (self.type == ShoppingCartTypeFBH) {
        for (NSDictionary *dic in _selectCouponArr) {
            p = p - [dic[@"money"] floatValue];
        }
    }
    self.price = p;
}

- (void)showMyMallCoupon:(UIButton *)btn{
    _selectMallRow = btn.tag - 200;
    
    ShoppingCartModel *model = self.malls[_selectMallRow];
    
    int pr = 0;
    for (ShoppingCartGoods *goods in model.goodsArr) {
        pr = pr + [goods.subtotal intValue];
    }
    
    YzMyCouponTableViewController *couponVC = [[YzMyCouponTableViewController  alloc]init];
    couponVC.sid = model.store_id;
    couponVC.price = pr;
    couponVC.delegate = self;
    [self.navigationController pushViewController:couponVC animated:YES];
}

#pragma mark - YzMallCouponDelegate
- (void)mallCouponInfo:(NSDictionary *)dic{
    
    UITableViewHeaderFooterView  *header = [self.tableView headerViewForSection:_selectMallRow];
    
    ShoppingCarHeaderView *headerView;
    NSArray *subViews = [header subviews];
    for (id v in subViews) {
        if ([v isKindOfClass:[ShoppingCarHeaderView class]]) {
            headerView = (ShoppingCarHeaderView *)v;
        }
    }
    if (headerView) {
        NSString *coupon = [NSString stringWithFormat:@"优惠券满%@减%@",dic[@"money_condition"],dic[@"money"]];
        [headerView.couponButton setTitle:coupon forState:UIControlStateNormal];
        [_selectCouponArr addObject:dic];
        [self changeGoodsTotalPrice];
    }
//    ShoppingCarHeaderView *headerView = (ShoppingCarHeaderView *)[self.tableView headerViewForSection:_selectMallRow];
//    NSString *coupon = [NSString stringWithFormat:@"优惠券满%@减%@",dic[@"gold_condition"],dic[@"gold"]];
//    [headerView.couponButton setTitle:coupon forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
