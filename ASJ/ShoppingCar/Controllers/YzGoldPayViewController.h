//
//  YzGoldPayViewController.h
//  ASJ
//
//  Created by Dororo on 2019/7/7.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YzGoldPayViewController : UIViewController

@property (nonatomic, copy)NSString *orderID;

@property (nonatomic, assign)float price;

@property (nonatomic, assign)NSInteger shopType;

@property (nonatomic, copy) NSString *merge;
@end

NS_ASSUME_NONNULL_END
