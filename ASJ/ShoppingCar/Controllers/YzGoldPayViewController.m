//
//  YzGoldPayViewController.m
//  ASJ
//
//  Created by Dororo on 2019/7/7.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzGoldPayViewController.h"
#import "YzGoldRechargeViewController.h"

@interface YzGoldPayViewController ()
@property (weak, nonatomic) IBOutlet UILabel *payGoldLabel;
//余额
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (weak, nonatomic) IBOutlet UIButton *goldPayButton;
@property (weak, nonatomic) IBOutlet UIButton *goldRechargeButton;

@end

@implementation YzGoldPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getGoldRechargeDataRequest];
    if (self.shopType == 1) {
        self.title = @"银积分支付";
        _goldRechargeButton.hidden = YES;
    }else{
        self.title = @"金积分支付";
        _goldRechargeButton.hidden = NO;
    }
}

#pragma mark - URL Requset

// 获取用户金积分余额
- (void)getGoldRechargeDataRequest{
    
    if (self.shopType == 1) {
        _payGoldLabel.text = [NSString stringWithFormat:@"所需银积分：%.2f",_price];
    }else{
        _payGoldLabel.text = [NSString stringWithFormat:@"所需金积分：%.2f",_price];
    }
    
    NSDictionary *dic = @{@"uid":LoveDriverID};
    [NetMethod Post:FBHRequestUrl(kUrl_get_wallet) parameters:dic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            NSDictionary *data = resDic[@"data"];
            //用户金积分
            if (self.shopType == 1) {
                _balanceLabel.text = [NSString stringWithFormat:@"银积分余额：%@",data[@"silver"]];
            }else{
                _balanceLabel.text = [NSString stringWithFormat:@"金积分余额：%@",data[@"score"]];
            }
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

//进行积分支付
- (void)goodsPayWithGold{
    
    NSString *payType;
    if (self.shopType == 1) {
        payType = @"4";
    }else{
        payType = @"3";
    }
    
    NSDictionary *parameDic = @{@"uid":LoveDriverID,
                                @"id":_orderID,
                                @"pay_type":payType,
                                @"merge":self.merge,
                                @"token":FBH_USER_TOKEN
                                };
    [NetMethod Post:FBHRequestUrl(kUrl_order_pay) parameters:parameDic success:^(id responseObject) {
        NSDictionary *resDic = responseObject;
        if ([resDic[@"code"] intValue] == 200) {
            [SVProgressHUD showSuccessWithStatus:@"支付成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError:resDic[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
}

#pragma mark - Action
- (IBAction)goldPayClick:(UIButton *)sender {
    [self goodsPayWithGold];
}

- (IBAction)goldRechargeClick:(UIButton *)sender {
    YzGoldRechargeViewController *goldRechargeViewController = [[YzGoldRechargeViewController alloc]init];
    [self.navigationController pushViewController:goldRechargeViewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
