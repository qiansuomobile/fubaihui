//
//  YzHeaderFooterView.h
//  ASJ
//
//  Created by Dororo on 2019/7/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCarHeaderView.h"
NS_ASSUME_NONNULL_BEGIN

@interface YzHeaderFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong)ShoppingCarHeaderView *hView;

@end

NS_ASSUME_NONNULL_END
