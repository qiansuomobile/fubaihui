//
//  YzHeaderFooterView.m
//  ASJ
//
//  Created by Dororo on 2019/7/30.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YzHeaderFooterView.h"

@implementation YzHeaderFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _hView = [[ShoppingCarHeaderView alloc] init];
        _hView.frame = CGRectMake(0, 0, MainScreen.width, 50*KHeight);
        [self addSubview:_hView];
    }
    
    return self;
}

@end
