//
//  OrderModel.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/14.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject
@property (nonatomic,strong) NSString *good_id;
@property (nonatomic,strong) NSString *ordernum;
@property (nonatomic,strong) NSString *user_id;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *pay_status;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *place;
@property (nonatomic,strong) NSString *rmb;
@property (nonatomic,strong) NSString *scores;
@property (nonatomic,strong) NSString *alipay;
@property (nonatomic,strong) NSString *alipay_order;
@property (nonatomic,strong) NSString *logistics;
@property (nonatomic,strong) NSString *number;
@property (nonatomic,strong) NSString *beizhu;
@property (nonatomic,strong) NSString *nickname;
@property (nonatomic,strong) NSString *mobile;
@end
