//
//  ShopCarModel.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/8.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ShopCarModel.h"

@implementation ShopCarModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.desc = value;
    }
    if ([key isEqualToString:@"id"]){
        self.goodID = value;
    }
    if ([key isEqualToString:@"template"]) {
        self.temp = value;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
