//
//  ShoppingCartGoods.h
//  ASJ
//
//  Created by Dororo on 2019/7/1.
//  Copyright © 2019 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShoppingCartGoods : NSObject

//购物车id
@property (nonatomic, copy)NSString *sc_id;
//商品id
@property (nonatomic, copy)NSString *g_id;
//商品名称
@property (nonatomic, copy)NSString *title;
//商品数量
@property (nonatomic, copy)NSString *num;
//商品价格
@property (nonatomic, copy)NSString *pay_price;
//商品库存
@property (nonatomic, copy)NSString *number;
//店铺id
@property (nonatomic, copy)NSString *store_id;
//商品图
@property (nonatomic, copy)NSString *img;
//商品小计
@property (nonatomic, copy)NSString *subtotal;

@end

NS_ASSUME_NONNULL_END
