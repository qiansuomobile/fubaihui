//
//  ShoppingCartGoods.m
//  ASJ
//
//  Created by Dororo on 2019/7/1.
//  Copyright © 2019 TS. All rights reserved.
//

#import "ShoppingCartGoods.h"

@implementation ShoppingCartGoods
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self.sc_id = value;
    }
    if ([key isEqualToString:@"cart_id"]) {
        self.sc_id = value;
    }
    
    if ([key isEqualToString:@"cart_num"]) {
        self.num = value;
    }
    if ([key isEqualToString:@"cover"]) {
        self.img = value;
    }
    if ([key isEqualToString:@"price"]) {
        self.pay_price = value;
    }
}
@end
