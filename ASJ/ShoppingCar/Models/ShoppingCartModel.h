//
//  ShoppingCartModel.h
//  ASJ
//
//  Created by Dororo on 2019/7/1.
//  Copyright © 2019 TS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingCartGoods.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShoppingCartModel : NSObject
//店铺名称
@property (nonatomic, copy)NSString *store_name;
//店铺id
@property (nonatomic, copy)NSString *store_id;
//购物车商品数组
@property (nonatomic, strong)NSArray<ShoppingCartGoods *> *goodsArr;
//每家店铺总计
@property (nonatomic, copy)NSString *store_total_price;
//店铺优惠券列表
@property (nonatomic, copy)NSString *coupon_list;
@end

NS_ASSUME_NONNULL_END
