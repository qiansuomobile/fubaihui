//
//  ShoppingCartModel.m
//  ASJ
//
//  Created by Dororo on 2019/7/1.
//  Copyright © 2019 TS. All rights reserved.
//

#import "ShoppingCartModel.h"

@implementation ShoppingCartModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"goods"]) {
        NSArray *goods = (NSArray *)value;
        NSMutableArray *mutableArr = [NSMutableArray array];
        for (NSDictionary *dic in goods) {
            ShoppingCartGoods *goods = [[ShoppingCartGoods alloc]init];
            [goods setValuesForKeysWithDictionary:dic];
            [mutableArr addObject:goods];
        }
        self.goodsArr = mutableArr;
    }
}
@end
