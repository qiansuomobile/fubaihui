//
//  AddressTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YGAddressM.h"
@interface AddressTableViewCell : UITableViewCell
@property (nonatomic,strong) UIButton *locationBtn;
@property (nonatomic,strong) UILabel *customerL;
@property (nonatomic,strong) UILabel *phoneL;
@property (nonatomic,strong) UILabel *addressL;
@property (nonatomic,strong) UILabel *descriptL;
@property (nonatomic,strong) YGAddressM *addressM;
@end
