//
//  AddressTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AddressTableViewCell.h"

@implementation AddressTableViewCell

-(void)setAddressM:(YGAddressM *)addressM{
    _customerL.text = [NSString stringWithFormat:@"收货人:%@",addressM.New_nickname];
    _phoneL.text = addressM.New_mobile;
    _addressL.text = [NSString stringWithFormat:@"收货地址:%@",addressM.New_place];
    
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _locationBtn = [[UIButton alloc] init];
    [_locationBtn setBackgroundImage:[UIImage imageNamed:@"didian"] forState:UIControlStateNormal];
    [self addSubview:_locationBtn];
    [_locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(30*Kwidth);
        make.height.mas_equalTo(30*KHeight);
    }];
    
    _customerL = [[UILabel alloc] init];
    _customerL.text = @"收货人：";
    _customerL.textAlignment = NSTextAlignmentLeft;
    _customerL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_customerL];
    [_customerL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_locationBtn.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.height.mas_equalTo(25*KHeight);
    }];
    
    _phoneL = [[UILabel alloc] init];
    _phoneL.text = @"";
    _phoneL.textAlignment = NSTextAlignmentRight;
    _phoneL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:_phoneL];
    [_phoneL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-35*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.height.mas_equalTo(25*KHeight);
    }];
    
    _addressL = [[UILabel alloc] init];
    _addressL.text = @"收货地址：";
    _addressL.textAlignment = NSTextAlignmentLeft;
    _addressL.font = [UIFont systemFontOfSize:13*KHeight];
    _addressL.lineBreakMode = NSLineBreakByWordWrapping;
    _addressL.numberOfLines = 2;
    [self addSubview:_addressL];
    [_addressL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.locationBtn.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.customerL.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(40*KHeight);
    }];
    
    _descriptL = [[UILabel alloc] init];
    _descriptL.text = @"(收货不方便时，可选择免费收货服务)";
    _descriptL.textAlignment = NSTextAlignmentLeft;
    _descriptL.font = [UIFont systemFontOfSize:13*KHeight];
    _descriptL.textColor = RGBColor(208, 163, 66);
    [self addSubview:_descriptL];
    [_descriptL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.locationBtn.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.addressL.mas_bottom).offset(0);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tiao"]];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.height.mas_equalTo(5*KHeight);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
