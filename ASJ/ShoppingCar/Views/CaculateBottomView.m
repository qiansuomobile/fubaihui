//
//  CaculateBottomView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CaculateBottomView.h"

@implementation CaculateBottomView

-(id)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    _submitBtn = [[UIButton alloc] init];
    [_submitBtn setBackgroundColor:RGBColor(252, 70, 30)];
    [_submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:_submitBtn];
    [_submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(Kwidth*120);
    }];
    
    _priceL = [[UILabel alloc] init];
    _priceL.text = @"¥799.00";
    _priceL.font = [UIFont systemFontOfSize:20*KHeight];
    _priceL.textAlignment = NSTextAlignmentRight;
    _priceL.textColor = RGBColor(252, 70, 30);
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_submitBtn.mas_left).offset(-5*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(25*KHeight);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label = [[UILabel alloc] init];
    label.text = @"合计：";
    label.font = [UIFont systemFontOfSize:20*KHeight];
    label.textAlignment = NSTextAlignmentRight;
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_priceL.mas_left).offset(0);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(25*KHeight);
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
