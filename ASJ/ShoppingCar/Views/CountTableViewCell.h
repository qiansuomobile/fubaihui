//
//  CountTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountTableViewCell : UITableViewCell
@property (nonatomic,strong) PAStepper *stepper;
@end
