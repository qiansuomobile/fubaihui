//
//  CountTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CountTableViewCell.h"

@implementation CountTableViewCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    _stepper = [[PAStepper alloc] initWithFrame:CGRectMake(MainScreen.width-100*Kwidth, 13*KHeight, 85*Kwidth, 25*KHeight)];
    [self addSubview:_stepper];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
