//
//  DetailTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopCarModel.h"

@interface DetailTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *image;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *typeL;
@property (nonatomic,strong) UILabel *priceL;
@property (nonatomic,strong) UIButton *rightBtn;
@property (nonatomic,strong) ShopCarModel *shopCarModel;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,strong) UILabel *rmbL;
@end
