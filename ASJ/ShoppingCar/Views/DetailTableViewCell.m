//
//  DetailTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

-(void)setShopCarModel:(ShopCarModel *)shopCarModel{
    _shopCarModel = shopCarModel;
    
    [_image sd_setImageWithURL:[NSURL URLWithString:shopCarModel.cover] placeholderImage:[UIImage imageNamed:@"logo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        _image.image = image;
    }];
    
    _titleL.text = shopCarModel.title;
    _priceL.text = shopCarModel.price;
    _countL.text = [NSString stringWithFormat:@"x%@",shopCarModel.buyNum];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    self.backgroundColor = RGBColor(245, 245, 245);
    _image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chaipin"]];
    [self addSubview:_image];
    [_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(100*KHeight);
        make.width.mas_equalTo(100*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"Mobil 美孚1号汽车润滑油0W-40 4L+1L API SN级发动机油5L组合卓越的低价垃圾油";
    _titleL.numberOfLines = 2;
    _titleL.font = [UIFont systemFontOfSize:14*KHeight];
    _titleL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_image.mas_right).offset(10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(35*KHeight);
    }];
    _typeL = [[UILabel alloc] init];
    _typeL.text = @"类型：4L";
    _typeL.font = [UIFont systemFontOfSize:14*KHeight];
    _typeL.textAlignment = NSTextAlignmentLeft;
    _typeL.textColor = RGBColor(167, 167, 167);
    [self addSubview:_typeL];
    [_typeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_image.mas_right).offset(10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(40*KHeight);
    }];

    _rmbL = [[UILabel alloc] init];
    _rmbL.text = @"¥";
    _rmbL.font = [UIFont systemFontOfSize:10*KHeight];
    _rmbL.textAlignment = NSTextAlignmentLeft;
    _rmbL.textColor = RGBColor(250, 83, 47);
    [self addSubview:_rmbL];
    [_rmbL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_image.mas_right).offset(10*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.height.mas_equalTo(15*KHeight);
    }];
    
    _priceL = [[UILabel alloc] init];
    _priceL.text = @"799";
    _priceL.font = [UIFont systemFontOfSize:15*KHeight];
    _priceL.textAlignment = NSTextAlignmentLeft;
    _priceL.textColor = RGBColor(250, 83, 47);
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rmbL.mas_right).offset(3*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    _countL = [[UILabel alloc] init];
    _countL.text = @"x12";
    _countL.font = [UIFont systemFontOfSize:15*KHeight];
    _countL.textAlignment = NSTextAlignmentRight;
    [self addSubview:_countL];
    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.bottom.equalTo(self.mas_bottom).offset(-10*KHeight);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    _rightBtn = [[UIButton alloc] init];
    [_rightBtn setBackgroundImage:[UIImage imageNamed:@"rightArror"] forState:UIControlStateNormal];
    [self addSubview:_rightBtn];
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.height.mas_equalTo(15*KHeight);
        make.width.mas_equalTo(15*KHeight);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
