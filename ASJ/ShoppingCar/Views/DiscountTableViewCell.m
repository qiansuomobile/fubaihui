//
//  DiscountTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "DiscountTableViewCell.h"

@implementation DiscountTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"可用882金A积分抵¥80.00";
    _titleL.font = [UIFont systemFontOfSize:17*KHeight];
    _titleL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(17*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(20*KHeight);
    }];
        
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
