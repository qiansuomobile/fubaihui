//
//  LeaveMessageTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "LeaveMessageTableViewCell.h"

@implementation LeaveMessageTableViewCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UILabel *label = [[UILabel alloc] init];
    label.text = @"买家留言:";
    label.font = [UIFont systemFontOfSize:17];
    label.textAlignment = NSTextAlignmentLeft;
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(17*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    _messageTF = [[UITextField alloc] init];
    _messageTF.textAlignment = NSTextAlignmentLeft;
    _messageTF.placeholder = @"选填:对本次交易的说明                  ";

    _messageTF.font = [UIFont systemFontOfSize:17];
    [self addSubview:_messageTF];
    [_messageTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).offset(0);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(20*KHeight);
        make.right.equalTo(self.mas_right).offset(-35*Kwidth);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
