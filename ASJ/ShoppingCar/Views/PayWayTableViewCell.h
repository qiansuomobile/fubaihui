//
//  PayWayTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayWayTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *payImageView;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UIButton *selectedBtn;
@property (nonatomic,strong) NSString *imageStr;
@property (nonatomic,strong) NSString *titleStr;
@end
