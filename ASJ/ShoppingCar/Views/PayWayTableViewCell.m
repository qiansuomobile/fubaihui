//
//  PayWayTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/29.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "PayWayTableViewCell.h"

@implementation PayWayTableViewCell

-(void)setImageStr:(NSString *)imageStr{
    _payImageView.image = [UIImage imageNamed:imageStr];
}

-(void)setTitleStr:(NSString *)titleStr{
    _titleL.text = titleStr;
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _payImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zhifubao-48"]];
    [self addSubview:_payImageView];
    [_payImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(17*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(35*KHeight);
        make.width.mas_equalTo(35*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"支付宝";
    _titleL.font = [UIFont systemFontOfSize:15*KHeight];
    _titleL.textColor = RGBColor(161, 161, 161);
    _titleL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_payImageView.mas_right).offset(10*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(20*KHeight);
    }];
    _selectedBtn = [[UIButton alloc] init];
    [_selectedBtn setBackgroundImage:[UIImage imageNamed:@"zhifuweixuanze-48"] forState:UIControlStateNormal];
    [_selectedBtn setBackgroundImage:[UIImage imageNamed:@"zhifuxuanze-48"] forState:UIControlStateSelected];
    _selectedBtn.userInteractionEnabled = NO;
    [self addSubview:_selectedBtn];
    [_selectedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(25*KHeight);
        make.width.mas_equalTo(25*KHeight);
    }];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
