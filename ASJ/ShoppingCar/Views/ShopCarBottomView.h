//
//  ShopCarBottomView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopCarBottomView : UIView
@property (nonatomic,assign) float price;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,strong) UIButton *buyBtn;
@property (nonatomic,strong) UILabel *priceL;
@end
