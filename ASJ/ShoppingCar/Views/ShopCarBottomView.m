//
//  ShopCarBottomView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ShopCarBottomView.h"

@implementation ShopCarBottomView

-(id)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    _selectBtn = [[UIButton alloc] init];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"weixuanzhe-72"] forState:UIControlStateNormal];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"xuanzhe-72"] forState:UIControlStateSelected];
    _selectBtn.hidden = YES;
    [self addSubview:_selectBtn];
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(Kwidth*25);
        make.width.mas_equalTo(Kwidth*25);
    }];
    
    UILabel *titleL = [[UILabel alloc] init];
    titleL.text = @"全选";
    titleL.font = [UIFont systemFontOfSize:18*KHeight];
    titleL.textAlignment = NSTextAlignmentLeft;
    titleL.hidden = YES;
    [self addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_selectBtn.mas_right).offset(15*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(30*Kwidth);
    }];
    
    _buyBtn = [[UIButton alloc] init];
    [_buyBtn setBackgroundColor:RGBColor(252, 70, 30)];
    [_buyBtn setTitle:@"结算(0)" forState:UIControlStateNormal];
    _buyBtn.titleLabel.font = [UIFont systemFontOfSize:20*KHeight];
    [self addSubview:_buyBtn];
    [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(Kwidth*120);
    }];
    _priceL = [[UILabel alloc] init];
    _priceL.textAlignment = NSTextAlignmentLeft;
    _priceL.text = @"¥0";
    _priceL.textColor = RGBColor(252, 70, 30);
    _priceL.font = [UIFont systemFontOfSize:17*KHeight];
    [self addSubview:_priceL];
    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_buyBtn.mas_left).offset(-8*Kwidth);
        make.top.equalTo(self.mas_top).offset(20*KHeight);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    
    
    UILabel *hejiL = [[UILabel alloc] init];
    hejiL.textAlignment = NSTextAlignmentRight;
    hejiL.text = @"合计:";
    hejiL.font = [UIFont systemFontOfSize:18*KHeight];
    [self addSubview:hejiL];
    [hejiL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_priceL.mas_left).offset(0);
        make.top.equalTo(self.mas_top).offset(20*KHeight);
        make.height.mas_equalTo(20*KHeight);
    }];
    //
//    UILabel *transL = [[UILabel alloc] init];
//    transL.text = @"不含运费";
//    transL.textAlignment = NSTextAlignmentRight;
//    transL.font = [UIFont systemFontOfSize:13*KHeight];
//    transL.textColor = RGBColor(109, 109, 109);
//    [self addSubview:transL];
//    [transL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(_buyBtn.mas_left).offset(-8*Kwidth);
//        make.top.equalTo(hejiL.mas_bottom).offset(2);
//        make.height.mas_equalTo(20*KHeight);
//    }];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
