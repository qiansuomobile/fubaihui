//
//  ShoppingCarHeaderView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCarHeaderView : UIView
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,strong) UIButton *moreBtn;

@property (nonatomic, strong)UIButton *couponButton;
@end
