//
//  ShoppingCarHeaderView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/26.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ShoppingCarHeaderView.h"

@implementation ShoppingCarHeaderView

-(id)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    _selectBtn = [[UIButton alloc] init];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"weixuanzhe-72"] forState:UIControlStateNormal];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"xuanzhe-72"] forState:UIControlStateSelected];
    [self addSubview:_selectBtn];
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(Kwidth*25);
        make.width.mas_equalTo(Kwidth*25);
    }];
    //暂时隐藏
    _selectBtn.hidden = YES;
    
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"福百惠商城";
    _titleL.font = [UIFont systemFontOfSize:17*KHeight];
    _titleL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(30*Kwidth);
    }];
    
    _moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_moreBtn setBackgroundImage:[UIImage imageNamed:@"rightArror"] forState:UIControlStateNormal];
    [self addSubview:_moreBtn];
    [_moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleL.mas_right).offset(20);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(20);
    }];
    
    _couponButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_couponButton setTitle:@"优惠券" forState:UIControlStateNormal];
    [_couponButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    _couponButton.titleLabel.font = [UIFont systemFontOfSize:15.0F];
    [self addSubview:_couponButton];
    [_couponButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.width.mas_equalTo(YzWidth - _titleL.frame.size.width - 90);
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
