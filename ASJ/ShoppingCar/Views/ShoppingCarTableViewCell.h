//
//  ShoppingCarTableViewCell.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/25.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartGoods.h"
#import "PKYStepper.h"

@protocol ShoppingCarTableViewCellDelegate <NSObject>

-(void)getBuyNumber:(NSInteger)buyNum atRow:(NSIndexPath *)idx;

@end

@interface ShoppingCarTableViewCell : UITableViewCell
@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,strong) UILabel *titleL;
@property (nonatomic,strong) UILabel *typeL;
@property (nonatomic,strong) UILabel *discountL;
//@property (nonatomic,strong) UILabel *giveL;
//@property (nonatomic,strong) UILabel *priceL;
//@property (nonatomic,strong) UILabel *countL;
//@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UILabel *rmbOneL;
//删除按键  默认隐藏
//@property (nonatomic,strong) UIButton *deleteBtn;
//@property (nonatomic,strong) PAStepper *stepper;
@property (nonatomic,strong) PKYStepper *stepper;
@property (nonatomic,strong) UIImageView *goodsImage;
//section
@property (nonatomic,assign) NSInteger section;
//row
@property (nonatomic, strong) NSIndexPath *idx;
//模型类
@property (nonatomic,strong) ShoppingCartGoods *goodsModel;
@property (nonatomic,assign) id<ShoppingCarTableViewCellDelegate>delegate;
@end
