//
//  ShoppingCarTableViewCell.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/25.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "ShoppingCarTableViewCell.h"
@interface ShoppingCarTableViewCell()
@property (nonatomic) id<SDWebImageOperation> photoDownloadOperation;
@end
@implementation ShoppingCarTableViewCell

-(void)setGoodsModel:(ShoppingCartGoods *)goodsModel{
    _goodsModel = goodsModel;
    [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:FBHRequestUrl(_goodsModel.img)]
                       placeholderImage:[UIImage imageNamed:@"logo"]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  
    }];
    _titleL.text = _goodsModel.title;
    _discountL.text = _goodsModel.pay_price;
//    _priceL.text = [NSString stringWithFormat:@"¥%@",_goodsModel.pay_rice];
//    if ([shopCarModel.f_silver isEqualToString:@"0"] && [shopCarModel.f_sorts isEqualToString:@"0"]) {
//        _giveL.text = @"无反币";
//    }else if (![shopCarModel.f_silver isEqualToString:@"0"] && [shopCarModel.f_sorts isEqualToString:@"0"]){
//        _giveL.text = [NSString stringWithFormat:@"送银币%@",shopCarModel.f_silver];
//    }else if ([shopCarModel.f_silver isEqualToString:@"0"] && ![shopCarModel.f_sorts isEqualToString:@"0"]){
//        _giveL.text = [NSString stringWithFormat:@"送金币%@",shopCarModel.f_silver];
//    }else{
//        _giveL.text = [NSString stringWithFormat:@"送金币%@ 送银币%@",shopCarModel.f_sorts,shopCarModel.f_silver];
//    }
    //重新设置lineView的约束
//    [_lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(_priceL.mas_centerY);
//        make.left.equalTo(_priceL.mas_left).offset(-2*Kwidth);
//        make.right.equalTo(_priceL.mas_right).offset(2*Kwidth);
//        make.height.mas_equalTo(1*KHeight);
//    }];
//    _countL.text = [NSString stringWithFormat:@"x%@",_goodsModel.num];
    _stepper.value = [_goodsModel.num integerValue];
    _stepper.maximum = [_goodsModel.number integerValue];
    _stepper.countLabel.text = _goodsModel.num;
    _stepper.minimum = 1;
    _typeL.text = [NSString stringWithFormat:@"库存:%@件",_goodsModel.number];
    [_stepper setBorderColor:RGBColor(140, 140, 140)];
    [_stepper setLabelTextColor:[UIColor blackColor]];
    [_stepper setButtonTextColor:[UIColor blackColor] forState:UIControlStateNormal];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI{
    
    _selectBtn = [[UIButton alloc] init];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"weixuanzhe-72"] forState:UIControlStateNormal];
    [_selectBtn setBackgroundImage:[UIImage imageNamed:@"xuanzhe-72"] forState:UIControlStateSelected];
    [self addSubview:_selectBtn];
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(Kwidth*25);
        make.width.mas_equalTo(Kwidth*25);
    }];
    //暂时隐藏
//    _selectBtn.hidden = YES;
    
    self.backgroundColor = RGBColor(248, 248, 248);
   
    
    _goodsImage = [[UIImageView alloc] init];
   
    [self addSubview:_goodsImage];
   
    [_goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15*Kwidth);
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(80*KHeight);
        make.height.mas_equalTo(80*KHeight);
    }];
    
    _titleL = [[UILabel alloc] init];
    _titleL.text = @"--";
    _titleL.textAlignment = NSTextAlignmentLeft;
    _titleL.lineBreakMode = NSLineBreakByCharWrapping;
    _titleL.numberOfLines = 2;
    _titleL.font = [UIFont systemFontOfSize:15*KHeight];
    [self addSubview:_titleL];
    [_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_goodsImage.mas_right).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(self.mas_top).offset(5*KHeight);
        make.height.mas_equalTo(40*KHeight);
    }];
    
    _typeL = [[UILabel alloc] init];
    _typeL.text = @"类型：4L";
    _typeL.textAlignment = NSTextAlignmentLeft;
    _typeL.font = [UIFont systemFontOfSize:13*KHeight];
    _typeL.textColor = RGBColor(136, 136, 136);
    [self addSubview:_typeL];
    [_typeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_goodsImage.mas_right).offset(10*Kwidth);
        make.right.equalTo(self.mas_right).offset(-10*Kwidth);
        make.top.equalTo(_titleL.mas_bottom).offset(25.0);
        make.height.mas_equalTo(20*KHeight);
    }];
    
    _rmbOneL = [[UILabel alloc] init];
    _rmbOneL.text = @"¥";
    _rmbOneL.font = [UIFont systemFontOfSize:11*KHeight];
    _rmbOneL.textAlignment = NSTextAlignmentLeft;
    _rmbOneL.textColor = RGBColor(252, 69, 31);
    [self addSubview:_rmbOneL];
    [_rmbOneL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_goodsImage.mas_right).offset(10*Kwidth);
        make.top.equalTo(_typeL.mas_bottom).offset(6*KHeight);
        make.height.mas_equalTo(10*KHeight);
    }];
    
    _discountL = [[UILabel alloc] init];
    _discountL.text = @"--";
    _discountL.textColor = RGBColor(252, 69, 31);
    _discountL.font = [UIFont systemFontOfSize:20*KHeight];
    _discountL.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_discountL];
    [_discountL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rmbOneL.mas_right).offset(3*Kwidth);
        make.top.equalTo(_typeL.mas_bottom).offset(0);
        make.height.mas_equalTo(20*KHeight);
    }];
    
//    _priceL = [[UILabel alloc] init];
//    _priceL.text = @"¥148";
//    _priceL.textAlignment = NSTextAlignmentLeft;
//    _priceL.font = [UIFont systemFontOfSize:13*KHeight];
//    _priceL.textColor = RGBColor(136, 136, 136);
//    [self addSubview:_priceL];
//    [_priceL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_discountL.mas_right).offset(10*Kwidth);
//        make.top.equalTo(_typeL.mas_bottom).offset(0);
//        make.height.mas_equalTo(20*KHeight);
//    }];
    
//    _lineView = [[UIView alloc] init];
//    _lineView.backgroundColor = RGBColor(136, 136, 136);
//    [self addSubview:_lineView];
//    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_discountL.mas_right).offset(8*Kwidth);
//        make.top.equalTo(_typeL.mas_bottom).offset(10*KHeight);
//        make.height.mas_equalTo(1*KHeight);
//        make.width.mas_equalTo(35*Kwidth);
//    }];
    
//    _countL = [[UILabel alloc] init];
//    _countL.textAlignment =NSTextAlignmentRight;
//    _countL.text = @"X1";
//    _countL.font = [UIFont systemFontOfSize:15*KHeight];
//    [self addSubview:_countL];
//    [_countL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_right).offset(-15*Kwidth);
//        make.bottom.equalTo(self.mas_bottom).offset(-15*KHeight);
//        make.height.mas_equalTo(25*KHeight);
//    }];
    
//    _deleteBtn = [[UIButton alloc] init];
//    [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
//    [_deleteBtn setBackgroundColor:[UIColor redColor]];
////    _deleteBtn.hidden = YES;
//    [self addSubview:_deleteBtn];
//    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top).offset(0);
//        make.right.equalTo(self.mas_right).offset(0);
//        make.bottom.equalTo(self.mas_bottom).offset(0);
//        make.width.mas_equalTo(70*Kwidth);
//    }];
//
    _stepper = [[PKYStepper alloc] initWithFrame:CGRectMake(YzWidth - 120, 115*KHeight - 20*KHeight - 10, 110, 20*KHeight)];
////    _stepper.hidden = YES;
////    [_stepper addTarget:self action:@selector(stpperBtnClick:) forControlEvents:UIControlEventValueChanged];
    _stepper.minimum = 1;
    [_stepper setBorderColor:[UIColor whiteColor]];
    _stepper.backgroundColor = [UIColor groupTableViewBackgroundColor];
    __weak typeof(self) weakSelf = self;
//    _stepper.valueChangedCallback = ^(PKYStepper *stepper, float count) {
////        stepper.countLabel.text = [NSString stringWithFormat:@"%d", (int)count];
////        weakSelf.countL.text = [NSString stringWithFormat:@"x%0.0f",weakSelf.stepper.value];
//        if ([weakSelf.delegate respondsToSelector:@selector(getBuyNumber:atRow:)]) {
//            [weakSelf.delegate getBuyNumber:weakSelf.stepper.value atRow:weakSelf.row];
//        }
//    };
    
    _stepper.incrementCallback = ^(PKYStepper *stepper, float newValue) {
        if ([weakSelf.delegate respondsToSelector:@selector(getBuyNumber:atRow:)]) {
            [weakSelf.delegate getBuyNumber:weakSelf.stepper.value atRow:weakSelf.idx];
        }
    };
    
    _stepper.decrementCallback = ^(PKYStepper *stepper, float newValue) {
        if ([weakSelf.delegate respondsToSelector:@selector(getBuyNumber:atRow:)]) {
            [weakSelf.delegate getBuyNumber:weakSelf.stepper.value atRow:weakSelf.idx];
        }
    };
    [self addSubview:_stepper];

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
