//
//  AudioTalkManager.h
//  ASJ
//
//  Created by Dororo on 2019/8/21.
//  Copyright © 2019 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioTalkManager : NSObject
+ (AudioTalkManager *)sharedClinet;

- (void)playAudioWithData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
