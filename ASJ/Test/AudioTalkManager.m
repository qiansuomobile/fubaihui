//
//  AudioTalkManager.m
//  ASJ
//
//  Created by Dororo on 2019/8/21.
//  Copyright © 2019 TS. All rights reserved.
//

#import "AudioTalkManager.h"
#import <AVFoundation/AVFoundation.h>
@interface AudioTalkManager ()<AVSpeechSynthesizerDelegate>

@property (nonatomic, strong)AVSpeechSynthesizer *av;

@property (nonatomic, strong)MPVolumeView *privateVoulmeView;

@end

@implementation AudioTalkManager

static AudioTalkManager *instance = nil;

+ (AudioTalkManager *)sharedClinet {
    
    if (instance == nil) {
        
        instance = [[super allocWithZone:NULL] init];
    }
    return instance;
}

- (void)playAudioWithData:(NSDictionary *)dic{
    
//    _privateVoulmeView = [[MPVolumeView alloc]init];
////    _privateVoulmeView.hidden = YES;
//    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
//    [window addSubview:_privateVoulmeView];
//    
//    [self setSystemVolume:1.0];
//    
    
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDuckOthers error:&error];
    
    _av= [[AVSpeechSynthesizer alloc]init];
    _av.delegate=self;//挂上代理
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc]initWithString:dic[@"body"]];//需要转换的文字
    utterance.rate = 0.5;// 设置语速，范围0-1，注意0最慢，1最快；AVSpeechUtteranceMinimumSpeechRate最慢，AVSpeechUtteranceMaximumSpeechRate最快
    utterance.pitchMultiplier = 1.0;
    
    AVSpeechSynthesisVoice *voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];//设置发音，这是中文普通话
    utterance.voice= voice;
    [_av speakUtterance:utterance];//开始
}

- (void)speechSynthesizer:(AVSpeechSynthesizer*)synthesizer didStartSpeechUtterance:(AVSpeechUtterance*)utterance{
    
    NSLog(@"---开始播放");
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer*)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance*)utterance{
    
    NSLog(@"---完成播放");
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer*)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance*)utterance{
    
    NSLog(@"---播放中止");
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer*)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance*)utterance{
    
    NSLog(@"---恢复播放");
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer*)synthesizer didCancelSpeechUtterance:(AVSpeechUtterance*)utterance{
    
    NSLog(@"---播放取消");
}

- (void)setSystemVolume:(float)volume {
    UISlider* volumeViewSlider = nil;
    for (UIView *view in [self.privateVoulmeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            volumeViewSlider = (UISlider*)view;
            break;
        }
    }
    if (volumeViewSlider != nil) {
        [volumeViewSlider setValue:volume animated:NO];
        //通过send
        [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

@end
