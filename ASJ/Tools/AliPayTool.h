//
//  AliPayTool.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/25.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AliPayTool : NSObject


/**
 支付宝支付
 1.向服务端发送订单信息
 2.收到服务端返回的签名结果，进行支付

 @param subjects 订单标题
 @param body 商品描述
 @param price 价格
 @param orderId 订单号
 @param callBack 支付结果
 */
+ (void)payByAliWithSubjects:(NSString *)subjects
                        body:(NSString *)body
                       price:(float)price
                     orderId:(NSString *)orderId
                    callBack:(void(^)(NSDictionary *info))callBack;



/**
 微信支付
 1.向服务端发送订单信息
 2.收到服务端的签名结果，进行支付

 @param orderId 订单号
 @param price 价格
 @param callBack 支付结果
 */
+ (void)payByWeChatWithOrderID:(NSString *)orderId
                       price:(float)price;

+ (void)showWxPayWithAppDic:(NSDictionary *)appDic;

+ (void)showAliPayWithSignedString:(NSString *)string;

@end
