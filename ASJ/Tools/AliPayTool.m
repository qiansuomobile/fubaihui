//
//  AliPayTool.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/25.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "AliPayTool.h"
#import "WXApi.h"

@implementation AliPayTool
+(void)payByAliWithSubjects:(NSString *)subjects body:(NSString *)body price:(float)price orderId:(NSString *)orderId callBack:(void (^)(NSDictionary *))callBack{
    
    
    //1.创建URL
    NSURL *url = [NSURL URLWithString:FBHRequestUrl(kUrl_pay_alipay)];
    //2.创建请求
    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *bobyString = [NSString stringWithFormat:@"out_trade_no=%@&total_amount=%.2f&subject=%@&body=%@",orderId, price, subjects, body];
    
    NSData *postData = [bobyString dataUsingEncoding:NSUTF8StringEncoding];
    [mutableRequest setHTTPMethod:@"POST"];
    [mutableRequest setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *task = [session dataTaskWithRequest:mutableRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil) {
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            if ([jsonDict[@"code"] integerValue] == 200) {
                NSDictionary *dataDic = jsonDict[@"data"];
                NSString *signedString = dataDic[@"response"];
                if (signedString != nil) {
                    //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
                    NSString *appScheme = @"FBHALIPAY";
                    // NOTE: 调用支付结果开始支付
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
                            NSLog(@"reslut = %@",resultDic);
                        }];
                    });
                }
            }else{
//                [MBProgressHUD showError:jsonDict[@"msg"] toView:self.view];
                NSLog(@"支付宝返回订单信息 :%@", jsonDict[@"msg"]);
            }
        }
    }];
    //启动任务
    [task resume];
}

+ (void)payByWeChatWithOrderID:(NSString *)orderId price:(float)price{
    
    NSDictionary *payDic = @{
                             @"ordernum":orderId,
                             @"money":[NSString stringWithFormat:@"%.2f",price]
                             };
    [NetMethod Post:FBHRequestUrl(kUrl_pay_wxpay) parameters:payDic success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        
        if ([dict[@"errorCode"] isEqual:@0]) {
            
            NSDictionary *responesData = dict[@"responseData"];
            NSDictionary *appDic = responesData[@"app_response"];
            //调起微信支付
            [AliPayTool showWxPayWithAppDic:appDic];
        }else{
            NSLog(@"请求微信支付签名错误⚠️ %@",dict[@"errorMsg"]);
        }
    } failure:^(NSError *error) {
        NSLog(@"请求微信支付签名服务器错误%@",error.description);
    }];
}

+ (void)showWxPayWithAppDic:(NSDictionary *)appDic{
    
    //调起微信支付
    PayReq* req             = [[PayReq alloc] init] ;
    req.partnerId           = [appDic objectForKey:@"partnerid"];
    req.prepayId            = [appDic objectForKey:@"prepayid"];
    req.nonceStr            = [appDic objectForKey:@"noncestr"];
    req.timeStamp           = [appDic[@"timestamp"] intValue];
    req.package             = [appDic objectForKey:@"package"];
    req.sign                = [appDic objectForKey:@"sign"];
    [WXApi sendReq:req];
}

+ (void)showAliPayWithSignedString:(NSString *)string{
    
    NSString *appScheme = @"FBHALIPAY";
    [[AlipaySDK defaultService] payOrder:string fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
    }];
}
@end
