//
//  CustomTextView.h
//  ASJ
//
//  Created by 叶岳洋 on 16/8/31.
//  Copyright © 2016年 TS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView
@property (nonatomic,strong) NSString *placeholder;
@property (nonatomic,strong) UILabel *placeholderL;
@property (nonatomic,strong) UILabel *countL;
@property (nonatomic,assign) BOOL isLimit;//是否有字数限制

-(id)initWithLimit:(BOOL)isLimit;

@end
