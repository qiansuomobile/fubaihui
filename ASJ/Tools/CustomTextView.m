//
//  CustomTextView.m
//  ASJ
//
//  Created by 叶岳洋 on 16/8/31.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView

-(id)initWithLimit:(BOOL)isLimit{
    self = [super init];
    if (self) {
        self.isLimit = isLimit;
        UILabel *placeholderLabel = [[UILabel alloc]init];//添加一个占位label
        
        placeholderLabel.backgroundColor= [UIColor clearColor];
        
        placeholderLabel.numberOfLines=0; //设置可以输入多行文字时可以自动换行
        placeholderLabel.textColor = [UIColor lightGrayColor];
        [self addSubview:placeholderLabel];
        self.placeholderL= placeholderLabel; //赋值保存
        self.countL = [[UILabel alloc] init];
        self.countL.font = [UIFont systemFontOfSize:18*KHeight];
        self.countL.textAlignment = NSTextAlignmentRight;
        //self.countL.text = @"0/49";
        
        [self addSubview:self.countL];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self]; //通知:监听文字的改变
        
    }
    return self;
}

-(void)textDidChange{
    self.placeholderL.hidden = self.hasText;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    self.placeholderL.frame = CGRectMake(2*Kwidth, 11*KHeight, self.frame.size.width-4*Kwidth, 20*KHeight);
    if (self.isLimit) {
        self.countL.frame = CGRectMake(self.frame.size.width - 70*Kwidth, self.frame.size.height-30*KHeight, 50*Kwidth, 20*KHeight);
    }
    
    //根据文字计算高度
    
   // CGSize maxSize =CGSizeMake(self.placeholderL.frame.size.width,MAXFLOAT);
    
//    self.placeholderL.frame.size.height= [self.placeholder boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.placeholderL.font} context:nil].size.height;
    
}

-(void)setPlaceholder:(NSString *)placeholder{
    _placeholder = [placeholder copy];
    self.placeholderL.text = placeholder;
}

- (void)setFont:(UIFont*)font{
    
    [super setFont:font];
    
    self.placeholderL.font= font;
    
    //重新计算子控件frame
    
    [self setNeedsLayout];
    
}

- (void)setText:(NSString*)text{
    
    [super setText:text];
    
    [self textDidChange]; //这里调用的就是 UITextViewTextDidChangeNotification 通知的回调
    
}

- (void)setAttributedText:(NSAttributedString*)attributedText{
    
    [super setAttributedText:attributedText];
    
    [self textDidChange]; //这里调用的就是UITextViewTextDidChangeNotification 通知的回调
    
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
