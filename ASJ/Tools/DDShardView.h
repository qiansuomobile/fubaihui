//
//  DDShardView.h
//  DDBoatingMachine
//
//  Created by Dororo on 2019/4/5.
//  Copyright © 2019 DeepDive Co.,Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, DDShareType){
    DDShareTypeIMG,
    DDShareTypeURL
};

@protocol DDShareDelegate <NSObject>

@optional
- (void)dismissShareView;
@end

@interface DDShardView : UIView

@property (nonatomic, assign)DDShareType type;
@property (nonatomic, weak)id<DDShareDelegate>delegate;
- (void)shareEventScoreImage:(UIImage *)img;
@end

NS_ASSUME_NONNULL_END
