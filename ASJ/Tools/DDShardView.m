//
//  DDShardView.m
//  DDBoatingMachine
//
//  Created by Dororo on 2019/4/5.
//  Copyright © 2019 DeepDive Co.,Ltd. All rights reserved.
//

#import "DDShardView.h"
#import "YZShare.h"
#import "UIColor+Utilities.h"

@interface DDShardView ()

@property (nonatomic, strong)UIView *bView;
@property (nonatomic, strong)UIView *alertView;
@property (nonatomic, strong)UIButton *closeBtn;

@property (nonatomic, strong)UIButton *wechatBtn;
@property (nonatomic, strong)UIButton *friendBtn;

@property (nonatomic, strong)UILabel *wechatLab;
@property (nonatomic, strong)UILabel *friendLab;

@property (nonatomic, strong)UIImage *shareImage;

@property (nonatomic, strong)UIView *shareBgView;

@property (nonatomic, copy)NSString *objectType;
@property (nonatomic, copy)NSString *objectID;

@end

@implementation DDShardView


- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self layoutSubViewsForShardView];
    }
    return self;
}

- (instancetype)init{
    if (self = [super init]) {
        [self layoutSubViewsForShardView];
    }
    return self;
}

- (void)layoutSubViewsForShardView{
    
    self.frame = CGRectMake(0, 0, YzWidth, YzHeight);
    
    /*创建灰色背景*/
    _bView = [[UIView alloc] initWithFrame:self.frame];
    _bView.alpha = 0;
    _bView.backgroundColor = [UIColor blackColor];
    [self addSubview:_bView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView:)];
    [_bView addGestureRecognizer:tapGesture];
    
    [self addSubview:self.alertView];
    
    [self.alertView addSubview:self.wechatLab];
    [self.alertView addSubview:self.friendLab];
    
    [self.alertView addSubview:self.closeBtn];
    [self.alertView addSubview:self.wechatBtn];
    [self.alertView addSubview:self.friendBtn];
    
    [self.alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(20);
        make.right.offset(-20);
        make.bottom.offset(140);
        make.height.mas_equalTo(140);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.and.right.and.bottom.offset(0);
        make.height.mas_equalTo(40);
    }];
    
    [self.wechatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(40);
        make.top.offset(18);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(44);
    }];
    
    
    [self.friendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.offset(18);
        make.right.offset(-40);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(44);
    }];
    
    [self.wechatLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(40);
        make.top.equalTo(self.wechatBtn.mas_bottom).offset(3);
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(44);
    }];
    
    [self.friendLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.friendBtn.mas_bottom).offset(3);
        make.right.offset(-40);
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(44);
    }];
}



- (void)showViewWithType:(NSString *)type data:(NSString *)objID{
    
    [self showContentView];
}

- (void)shareEventScoreImage:(UIImage *)img{
    
    _shareImage = img;
    [self showContentView];
}


- (void)showContentView{
    UIWindow * window = [UIApplication sharedApplication].windows[0];
    [window addSubview:self];
    
    [self.alertView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);
        make.right.offset(-20);
        make.bottom.offset(-20);
        make.height.mas_equalTo(140);
    }];
    [UIView animateWithDuration:0.5 animations:^{
        self.bView.alpha = 0.5;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissContactView:(UITapGestureRecognizer *)tapGesture{
    [self dismissContactView];
}


-(void)dismissContactView{
    
    if ([_delegate respondsToSelector:@selector(dismissShareView)]) {
        [_delegate dismissShareView];
    }
    
    __weak typeof(self)weakSelf = self;
    
    [self.alertView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);
        make.right.offset(-20);
        make.bottom.offset(140);
        make.height.mas_equalTo(140);
    }];
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.bView.alpha = 0;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}

#pragma mark -分享到各个平台
- (void)shardWithWechat{
    [YZShare shareParamsByImage:_shareImage WXScene:WXSceneSession];
    [self dismissContactView];
}

- (void)shardWithFriend{
    [YZShare shareParamsByImage:_shareImage WXScene:WXSceneTimeline];
    [self dismissContactView];
}

- (void)closeShardView{
    [self dismissContactView];
}

#pragma mark - getter
- (UIView *)alertView {
    if (!_alertView) {
        _alertView = [[UIView alloc]init];
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.masksToBounds = YES;
        _alertView.layer.cornerRadius = 10.0f;
        _alertView.layer.shouldRasterize = YES;
        _alertView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    return _alertView;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setBackgroundColor:[UIColor colorWithHex:0xE6E6E6]];
        [_closeBtn setImage:IMG_NAMED(@"shard_close") forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeShardView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (UIButton *)wechatBtn{
    if (!_wechatBtn) {
        _wechatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_wechatBtn setImage:IMG_NAMED(@"shard_wechat") forState:UIControlStateNormal];
        [_wechatBtn addTarget:self action:@selector(shardWithWechat) forControlEvents:UIControlEventTouchUpInside];
    }
    return _wechatBtn;
}

- (UIButton *)friendBtn{
    if (!_friendBtn) {
        _friendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_friendBtn setImage:IMG_NAMED(@"shard_friend") forState:UIControlStateNormal];
        [_friendBtn addTarget:self action:@selector(shardWithFriend) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendBtn;
}

- (UILabel *)wechatLab{
    if (!_wechatLab) {
        _wechatLab = [[UILabel alloc]init];
        _wechatLab.text = @"微信";
        _wechatLab.textAlignment = NSTextAlignmentCenter;
        _wechatLab.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        _wechatLab.textColor = [UIColor colorWithHex:0x939AA4];
    }
    return _wechatLab;
}

- (UILabel *)friendLab{
    if (!_friendLab) {
        _friendLab = [[UILabel alloc]init];
        _friendLab.text = @"朋友圈";
        _friendLab.textAlignment = NSTextAlignmentCenter;
        _friendLab.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        _friendLab.textColor = [UIColor colorWithHex:0x939AA4];
    }
    return _friendLab;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
