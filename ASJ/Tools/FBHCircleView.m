//
//  FBHCircleView.m
//  ASJ
//
//  Created by Dororo on 2019/6/28.
//  Copyright © 2019 TS. All rights reserved.
//

#import "FBHCircleView.h"

@implementation FBHCircleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5.0;
    }
    return self;
}

@end
