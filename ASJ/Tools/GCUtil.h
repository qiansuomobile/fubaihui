//
//  GCUtil.h
//  iMagazine2
//
//  Created by dreamRen on 12-11-16.
//  Copyright (c) 2012年 iHope. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "FXLabel.h"
//#import "SGCommonMacros.h"
//#import "SGSettingInfo.h"
#import "NSString+SGJSON.h"
#import "NSDictionary+SGJSON.h"
//#import "UMSocial.h"
#import <WebKit/WebKit.h>

@interface GCUtil : NSObject

+(BOOL)connectedToNetwork;

//显示提示框,只有一个确定按钮
+(void)showInfoAlert:(NSString*)aInfo;
//显示请求错误
+(void)showDataErrorAlert;

//添加单击事件
+(UITapGestureRecognizer *)getTapGesture:(id)target action:(SEL)action;
+(UILongPressGestureRecognizer*)getLongPressClick:(id)target action:(SEL)action;

//是否空格
+(BOOL)isEmptyOrWhitespace:(NSString*)aStr;

//按比例更改图片大小
+(UIImage*)scaleAspectFitImage:(UIImage*)aImage ToSize:(CGSize)aSize;

//按比例更改图片大小,短的那个边等于toSize对应的边的长度,长的那个变>=toSize对应的边的长度
+(UIImage*)scaleImage:(UIImage*)aImage ToSize:(CGSize)aSize;
+(UIImage*)scale2Image:(UIImage*)aImage ToSize:(CGSize)aSize;

+(NSString *)getToday;
+(NSString *)formatToDay:(NSDate*)aDate;
//日期+随机数
+(NSString*)getTimeAndRandom;

//格式化日期, 时间
+(NSString*)formatToDayAndTime:(NSDate*)aDate;
+(NSString*)formatToDayTime:(NSString*)aSecond;
+(NSString*)formatDayFrom1970Seconds:(NSString*)aSecond;
+(NSString*)formatDayAndTimeFrom1970Seconds:(NSString*)aSecond;
+(NSString*)pollCommentTimeFrom1970Seconds:(NSString*)aSecond;


+(BOOL)isMobileNumber:(NSString *)mobileNum;
+(BOOL)isValidateEmail:(NSString *)email;
//正则判断中文英文，及数字
+(BOOL)isTextFiledNumber:(NSString*)textField;

+(UIView*)getDarkView:(CGRect)aRect;

+(UIView*)getNoShiPinViewInView:(UIView*)aView;

//渐变label
//+(FXLabel*)getJianBianLabelWithTitle:(NSString*)aTitle;
//+(FXLabel*)getJianBianLabelWithFrame:(CGRect)aRect WithFont:(UIFont*)aFont WithTitle:(NSString*)aTitle;

+ (UIImage *)imageWithImage:(UIImage*)inImage withColorMatrix:(const float*)f;
+(NSString*) digest:(NSString*)input;

//LK
+(NSDate *)timestampToDate:(NSString *)timestamp;
+ (NSDate *)dateOfDateStr:(NSString *)dateStr;
+ (UIImage *)changeImageSizeWithImage:(UIImage *)image withSpecifiedArea:(UIEdgeInsets)insets;
+ (UIColor*)getPixelColorAtLocation:(CGPoint)point withImage:(NSString *)imageName;
+ (id)marrySecurity:(NSString *)password withUserName:(NSString *)userName;
+ (id)marryUserName:(NSString *)name;
+ (id)marryPersonalName:(NSString *)name;
+ (BOOL) marryEmail:(NSString *)email;
+ (BOOL) marryMoney:(NSString *)money;
//yl
//+(void)showSaoYiSaoUMShareSDKSheetView:(UIViewController*)sheetView
//                     shareText:(NSString*)text
//                    shareImage:(UIImage *)shareImage
//                      delegate:(id <UMSocialUIDelegate>)delegate;
//宋迪添加方法
+(UIViewController*)getParentViewController:(UIView*)view;
//+(void)hideLeftMenu;
//+(void)showLeftMenu;
//+(void)showUMShareSDKSheetView:(UIViewController*)sheetView
//                     shareText:(NSString*)text
//                    shareImage:(UIImage *)shareImage
//                      delegate:(id<UMSocialUIDelegate>)delegate;

+(NSArray*)getPlistFile:(NSString*)fileName;
+(void)writePlistFile:(NSArray*)data andFileName:(NSString*)fileName;
+(NSString*)insertString:(NSString*)addStr originalStr:(NSString*)oStr atIndex:(int)index;
+(NSString*)nowDateWithFormater:(NSString*)formater;
+(NSString*)nowWeekDay;
+(NSString*)getChineseCalendarWithDate:(NSDate *)date;
+(NSString*)getFalseDataJson:(NSString*)filePath;

+(void)saveEntitysData:(id)entity fileName:(NSString*)fileName;
+(id)entitysData:(NSString*)fileName;

//获取当前日期时间戳
+(NSString*)timeIntervalSince1970;
//多少分钟前的格式时间戳
+(NSString *)intervalSinceNow:(NSString *)theDate;




/**
 * 计算指定时间与当前的时间差
 * @param compareDate   某一指定时间
 * @return 多少(秒or分or天or月or年)+前 (比如，3天前、10分钟前)
 */
+(NSString *)compareCurrentTime:(NSDate*)compareDate;


+(UIColor *)hexStringToColor:(NSString *)hex;

+(NSString*)encodeUrlString:(NSString*)urlString;

+(NSInteger)getChartLong:(NSString *)strtemp;

+(void)getUserJifen;
//+(void)SaveUserJifen:(NSString *)userJifen;
//时间戳转换
+ (NSString *)timeString:(NSString *)timeStamp;
//判断是否是wifi环境  结果说明：0-无连接   1-wifi    2-3G
//+ (NSInteger)isWifiEnvironment;




+( long long ) fileSizeAtPath:( NSString *) filePath;


+ ( float ) folderSizeAtPath:( NSString *) folderPath;


+ ( NSString * ) YzfolderSizeAtPath:( NSString *) folderPath;


+(void)clearFile;





+(void)writeToCache:(NSString *)RequsetUrl;


+(void)YzSetWebCache:(NSString *)RequsetUrl webView:(WKWebView *)webView;


+(BOOL )GetWebCachaData:(NSString *)RequsetUrl;

@end
