//
//  GCUtil.m
//  iMagazine2
//
//  Created by dreamRen on 12-11-16.
//  Copyright (c) 2012年 iHope. All rights reserved.
//

#import "GCUtil.h"
//#import "Reachability.h"
#include <CommonCrypto/CommonDigest.h>
#import <netdb.h>
#import <QuartzCore/QuartzCore.h>


@implementation GCUtil

//是否连网
+(BOOL)connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return NO;
    }
    
    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
}

//显示提示框,只有一个确定按钮
+(void)showInfoAlert:(NSString*)aInfo{
    UIAlertView *tAlert=[[UIAlertView alloc] initWithTitle:@"提示" message:aInfo delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [tAlert show];
    
}


//显示请求错误
+(void)showDataErrorAlert{
    [GCUtil showInfoAlert:@"获取数据错误"];
}

//添加单击事件
+(UITapGestureRecognizer *)getTapGesture:(id)target action:(SEL)action{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    tapGestureRecognizer.delegate=target;
    tapGestureRecognizer.numberOfTapsRequired = 1;
    return tapGestureRecognizer;
}

+(UILongPressGestureRecognizer*)getLongPressClick:(id)target action:(SEL)action{
    UILongPressGestureRecognizer *longPressRecognizer=[[UILongPressGestureRecognizer alloc] initWithTarget:target action:action];
    longPressRecognizer.delegate=target;
    [longPressRecognizer setMinimumPressDuration:1.0];
    return longPressRecognizer;
}


+(BOOL)isEmptyOrWhitespace:(NSString*)aStr {
    // A nil or NULL string is not the same as an empty string
    return 0 == aStr.length || ![aStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length;
}


//from大小等比例缩小,如果原始大小比目标大小小,则保持原始大小
+(CGSize)reduceSizeFrom:(CGSize)fromSize To:(CGSize)toSize{
    
    float iWidth=toSize.width;
    float iHeight=toSize.height;
    
    float iImageWidth=fromSize.width;
    float iImageHeight=fromSize.height;
    
    if (iImageWidth>0 && iImageHeight>0 && (iImageWidth>iWidth || iImageHeight>iHeight)) {
        if (iImageWidth/iImageHeight >= iWidth/iHeight) {
            iImageHeight=iWidth*iImageHeight/iImageWidth;
            iImageWidth=iWidth;
        }else{
            iImageWidth=iHeight*iImageWidth/iImageHeight;
            iImageHeight=iHeight;
        }
    }
    
    return CGSizeMake(iImageWidth, iImageHeight);
}

//按比例更改图片大小
+(UIImage*)scaleAspectFitImage:(UIImage*)aImage ToSize:(CGSize)aSize
{
    float iImageWidth=aImage.size.width;
    float iImageHeight=aImage.size.height;
    
    if (iImageWidth>0 && iImageHeight>0){
        if(iImageWidth/iImageHeight>=aSize.width/aSize.height) {
            iImageHeight=aSize.width*iImageHeight/iImageWidth;
            iImageWidth=aSize.width;
        }else{
            iImageWidth=aSize.height*iImageWidth/iImageHeight;
            iImageHeight=aSize.height;
        }
    }
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(CGSizeMake(iImageWidth, iImageHeight));
    // 绘制改变大小的图片
    [aImage drawInRect:CGRectMake(0, 0, iImageWidth, iImageHeight)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

//按比例更改图片大小,短的那个边等于toSize对应的边的长度,长的那个变>=toSize对应的边的长度
+(UIImage*)scaleImage:(UIImage*)aImage ToSize:(CGSize)aSize
{
    float iImageWidth=aImage.size.width;
    float iImageHeight=aImage.size.height;
    
    if (iImageWidth>0 && iImageHeight>0){
        if(iImageWidth/iImageHeight>=aSize.width/aSize.height) {
            iImageWidth=iImageWidth*aSize.height/iImageHeight;
            iImageHeight=aSize.height;
        }else{
            iImageHeight=iImageHeight*aSize.width/iImageWidth;
            iImageWidth=aSize.width;
        }
    }
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(CGSizeMake(iImageWidth, iImageHeight));
    // 绘制改变大小的图片
    [aImage drawInRect:CGRectMake(0, 0, iImageWidth, iImageHeight)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}
+(UIImage*)scale2Image:(UIImage*)aImage ToSize:(CGSize)aSize
{
    float iImageWidth=aSize.width;
    float iImageHeight=aSize.height;
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(CGSizeMake(iImageWidth, iImageHeight));
    // 绘制改变大小的图片
    [aImage drawInRect:CGRectMake(0, 0, iImageWidth, iImageHeight)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

+(NSString *)getToday
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSString *)formatToDay:(NSDate*)aDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:aDate];
}

//日期+随机数
+(NSString*)getTimeAndRandom{
    int iRandom=arc4random();
	if (iRandom<0) {
		iRandom=-iRandom;
	}
	
	NSDateFormatter *tFormat=[[NSDateFormatter alloc] init];
	[tFormat setDateFormat:@"yyyyMMddHHmmss"];
	NSString *tResult=[NSString stringWithFormat:@"%@%d",[tFormat stringFromDate:[NSDate date]],iRandom];
    return tResult;
}

//格式化日期, 时间
+(NSString*)formatToDayAndTime:(NSDate*)aDate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:aDate];
}
+(NSString*)formatToDayTime:(NSString*)aSecond{
    NSDate *tDate=[NSDate dateWithTimeIntervalSince1970:[aSecond longLongValue]/1000];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd"];
    return [dateFormatter stringFromDate:tDate];
}
+(NSString*)formatDayFrom1970Seconds:(NSString*)aSecond{
    
    
    NSDate *tDate=[NSDate dateWithTimeIntervalSince1970:[aSecond longLongValue]/1000];
    //    return [GCUtil formatToDayAndTime:tDate];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:tDate];
}
+(NSString*)formatDayAndTimeFrom1970Seconds:(NSString*)aSecond{
//    if ([GCUtil isEmptyOrWhitespace:aSecond]) {
//        return @"";
//    }

    NSDate *tDate=[NSDate dateWithTimeIntervalSince1970:[aSecond longLongValue]/1000];
    //    return [GCUtil formatToDayAndTime:tDate];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormatter stringFromDate:tDate];
}

+(NSString*)pollCommentTimeFrom1970Seconds:(NSString*)aSecond
{
    NSDate *tDate=[NSDate dateWithTimeIntervalSince1970:[aSecond longLongValue]/1000];
    //    return [GCUtil formatToDayAndTime:tDate];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    return [dateFormatter stringFromDate:tDate];
}

+(BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
//正则判断中文英文，及数字
+(BOOL)isTextFiledNumber:(NSString*)textField
{
  NSString * str = @"^\\w{0,6}$";
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    if ([predicate evaluateWithObject:textField]== YES) {
        
        return YES;
    }
    else{
        return NO;
    }
}
// 正则判断手机号码地址格式
+(BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[0235-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(UIView*)getDarkView:(CGRect)aRect{
    UIView *tView=[[UIView alloc] initWithFrame:aRect];
//    tView.backgroundColor=kkDarkColor;
    tView.layer.borderColor=[[UIColor colorWithRed:37/255.0 green:38/255.0 blue:39/255.0 alpha:1.0] CGColor];
    tView.layer.borderWidth=0.6;
    return tView;
}

+(UIView*)getNoShiPinViewInView:(UIView*)aView{
    UIView *tView=[[UIView alloc] initWithFrame:CGRectMake((aView.frame.size.width-67)/2, (aView.frame.size.height-67-33)/2-20, 67, 67+33)];
    tView.backgroundColor=[UIColor clearColor];
    [aView addSubview:tView];
    
    
    UIImageView *tImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 67, 67)];
    tImageView.image=[UIImage imageNamed:@"noshipin.png"];
    [tView addSubview:tImageView];
    
    
    UILabel *tLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 67, 67, 33)];
    tLabel.backgroundColor=[UIColor clearColor];
    tLabel.font=[UIFont boldSystemFontOfSize:16];
//    tLabel.textColor=kkCellGrayLabelColor;
    [tLabel setTextAlignment:NSTextAlignmentCenter];
    tLabel.text=@"暂无视频";
    [tView addSubview:tLabel];
    
    
    return tView;
}

//+(FXLabel*)getJianBianLabelWithTitle:(NSString*)aTitle{
//    FXLabel *topTitleLabel=[[[FXLabel alloc] initWithFrame:CGRectMake(0, 0, 190, 40)] autorelease];
//    topTitleLabel.backgroundColor=[UIColor clearColor];
//    topTitleLabel.font=[UIFont boldSystemFontOfSize:20.0f];
//    topTitleLabel.textAlignment = NSTextAlignmentCenter;
//    topTitleLabel.text=aTitle;
//    topTitleLabel.gradientStartColor = [UIColor whiteColor];
//    topTitleLabel.gradientEndColor = [UIColor whiteColor];
//    return topTitleLabel;
//}

//+(FXLabel*)getJianBianLabelWithFrame:(CGRect)aRect WithFont:(UIFont*)aFont WithTitle:(NSString*)aTitle{
//    FXLabel *topTitleLabel=[[[FXLabel alloc] initWithFrame:aRect] autorelease];
//    topTitleLabel.backgroundColor=[UIColor clearColor];
//    topTitleLabel.font=aFont;
//    topTitleLabel.textAlignment=NSTextAlignmentCenter;
//    topTitleLabel.text=aTitle;
//    topTitleLabel.gradientStartColor = [UIColor whiteColor];
//    topTitleLabel.gradientEndColor = [UIColor whiteColor];
//    return topTitleLabel;
//}

/////任海丽添加方法
+ (UIImage*)imageWithImage:(UIImage*)inImage withColorMatrix:(const float*) f
{
	unsigned char *imgPixel = RequestImagePixelData(inImage);
	CGImageRef inImageRef = [inImage CGImage];
	unsigned long w = CGImageGetWidth(inImageRef);
	unsigned long h = CGImageGetHeight(inImageRef);
	
	int wOff = 0;
	int pixOff = 0;
	
    
	for(GLuint y = 0;y< h;y++)//双层循环按照长宽的像素个数迭代每个像素点
	{
		pixOff = wOff;
		
		for (GLuint x = 0; x<w; x++)
		{
			int red = (unsigned char)imgPixel[pixOff];
			int green = (unsigned char)imgPixel[pixOff+1];
			int blue = (unsigned char)imgPixel[pixOff+2];
            int alpha = (unsigned char)imgPixel[pixOff+3];
            changeRGBA(&red, &green, &blue, &alpha, f);
            
            //回写数据
			imgPixel[pixOff] = red;
			imgPixel[pixOff+1] = green;
			imgPixel[pixOff+2] = blue;
            imgPixel[pixOff+3] = alpha;
            
            
			pixOff += 4; //将数组的索引指向下四个元素
		}
        
		wOff += w * 4;
	}
    
	NSInteger dataLength = w * h * 4;
    
    //下面的代码创建要输出的图像的相关参数
	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, imgPixel, dataLength, NULL);
    
	int bitsPerComponent = 8;
	int bitsPerPixel = 32;
	unsigned long bytesPerRow = 4 * w;
	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
	
	
	CGImageRef imageRef = CGImageCreate(w, h, bitsPerComponent, bitsPerPixel, bytesPerRow,colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);//创建要输出的图像
	
	UIImage *myImage = [UIImage imageWithCGImage:imageRef];
	
	CFRelease(imageRef);
	CGColorSpaceRelease(colorSpaceRef);
	CGDataProviderRelease(provider);
	return myImage;
}

static CGContextRef CreateRGBABitmapContext (CGImageRef inImage)// 返回一个使用RGBA通道的位图上下文
{
	CGContextRef context = NULL;
	CGColorSpaceRef colorSpace;
	void *bitmapData; //内存空间的指针，该内存空间的大小等于图像使用RGB通道所占用的字节数。
	unsigned long bitmapByteCount;
	unsigned long bitmapBytesPerRow;
    
	size_t pixelsWide = CGImageGetWidth(inImage); //获取横向的像素点的个数
	size_t pixelsHigh = CGImageGetHeight(inImage); //纵向
    
	bitmapBytesPerRow	= (pixelsWide * 4); //每一行的像素点占用的字节数，每个像素点的ARGB四个通道各占8个bit(0-255)的空间
	bitmapByteCount	= (bitmapBytesPerRow * pixelsHigh); //计算整张图占用的字节数
    
	colorSpace = CGColorSpaceCreateDeviceRGB();//创建依赖于设备的RGB通道
	
	bitmapData = malloc(bitmapByteCount); //分配足够容纳图片字节数的内存空间
    
	context = CGBitmapContextCreate (bitmapData, pixelsWide, pixelsHigh, 8, bitmapBytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);
    //创建CoreGraphic的图形上下文，该上下文描述了bitmaData指向的内存空间需要绘制的图像的一些绘制参数
    
	CGColorSpaceRelease( colorSpace );
    //Core Foundation中通过含有Create、Alloc的方法名字创建的指针，需要使用CFRelease()函数释放
    
	return context;
}

static unsigned char *RequestImagePixelData(UIImage *inImage)
// 返回一个指针，该指针指向一个数组，数组中的每四个元素都是图像上的一个像素点的RGBA的数值(0-255)，用无符号的char是因为它正好的取值范围就是0-255
{
	CGImageRef img = [inImage CGImage];
	CGSize size = [inImage size];
    
	CGContextRef cgctx = CreateRGBABitmapContext(img); //使用上面的函数创建上下文
	
	CGRect rect = {{0,0},{size.width, size.height}};
    
	CGContextDrawImage(cgctx, rect, img); //将目标图像绘制到指定的上下文，实际为上下文内的bitmapData。
	unsigned char *data = CGBitmapContextGetData (cgctx);
    
	CGContextRelease(cgctx);//释放上面的函数创建的上下文
	return data;
}

static void changeRGBA(int *red,int *green,int *blue,int *alpha, const float* f)//修改RGB的值
{
    int redV = *red;
    int greenV = *green;
    int blueV = *blue;
    int alphaV = *alpha;
    
    *red = f[0] * redV + f[1] * greenV + f[2] * blueV + f[3] * alphaV + f[4];
    *green = f[0+5] * redV + f[1+5] * greenV + f[2+5] * blueV + f[3+5] * alphaV + f[4+5];
    *blue = f[0+5*2] * redV + f[1+5*2] * greenV + f[2+5*2] * blueV + f[3+5*2] * alphaV + f[4+5*2];
    *alpha = f[0+5*3] * redV + f[1+5*3] * greenV + f[2+5*3] * blueV + f[3+5*3] * alphaV + f[4+5*3];
    
    if (*red > 255)
    {
        *red = 255;
    }
    if(*red < 0)
    {
        *red = 0;
    }
    if (*green > 255)
    {
        *green = 255;
    }
    if (*green < 0)
    {
        *green = 0;
    }
    if (*blue > 255)
    {
        *blue = 255;
    }
    if (*blue < 0)
    {
        *blue = 0;
    }
    if (*alpha > 255)
    {
        *alpha = 255;
    }
    if (*alpha < 0)
    {
        *alpha = 0;
    }
}
+(NSString*) digest:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, data.length, digest);
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;
}

//LK
+(NSDate *)timestampToDate:(NSString *)timestamp
{
    return [NSDate dateWithTimeIntervalSince1970:[timestamp intValue]];
}

+ (NSDate *)dateOfDateStr:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    return date;
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //    return [dateFormatter dateFromString:date];
}

+ (UIImage *)changeImageSizeWithImage:(UIImage *)image withSpecifiedArea:(UIEdgeInsets)insets
{
    image = [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    return image;
}

+ (UIColor*)getPixelColorAtLocation:(CGPoint)point withImage:(NSString *)imageName{
    UIColor* color =nil;
    UIImage *image= [UIImage imageNamed:imageName];
    CGImageRef inImage = image.CGImage;
    // Create offscreen bitmap context to draw the image into. Format ARGB is 4bytes for each pixel: Alpa, Red, Green, Blue
    CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
    if (cgctx ==NULL) { return nil;  }
    
    size_t w =CGImageGetWidth(inImage);
    size_t h =CGImageGetHeight(inImage);
    CGRect rect ={{0,0},{w,h}};
    
    // Draw the imageto the bitmap context. Once we draw, the memory
    // allocated forthe context for rendering will then contain the
    // raw image datain the specified color space.
    CGContextDrawImage(cgctx, rect,inImage);
    
    // Now we can geta pointer to the image data associated with the bitmap
    // context.
    unsigned char* data = CGBitmapContextGetData (cgctx);
    if (data !=NULL) {
        //offset locates the pixel in the data from x,y.
        //4for 4 bytes of data per pixel, w is width of one row of data.
        @try {
            int offset = 4*((w*round(point.y))+round(point.x));
            NSLog(@"offset: %d", offset);
            int alpha =  data[offset];
            int red = data[offset+1];
            int green = data[offset+2];
            int blue = data[offset+3];
            NSLog(@"offset: %i colors: RGB A %i %i %i %i",offset,red,green,blue,alpha);
            color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
        }
        @catch (NSException * e) {
            NSLog(@"%@",[e reason]);
        }
        @finally {
        }
        
    }
    // When finished,release the context
    CGContextRelease(cgctx);
    // Free imagedata memory for the context
    if (data) {free(data); }
    
    return color;
}

+ (CGContextRef)createARGBBitmapContextFromImage:(CGImageRef) inImage {
    
    CGContextRef    context =NULL;
    CGColorSpaceRef colorSpace;
    void *         bitmapData;
    unsigned long            bitmapByteCount;
    unsigned long            bitmapBytesPerRow;
    
    // Get imagewidth, height. We'll use the entire image.
    size_t pixelsWide= CGImageGetWidth(inImage);
    size_t pixelsHigh= CGImageGetHeight(inImage);
    
    // Declare thenumber of bytes per row. Each pixel in the bitmap in this
    // example isrepresented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow *pixelsHigh);
    
    // Use thegeneric RGB color space.
    colorSpace =CGColorSpaceCreateDeviceRGB();
    
    if (colorSpace ==NULL)
    {
        fprintf(stderr,"Error allocating color space\n");
        return NULL;
    }
    
    // Allocatememory for image data. This is the destination in memory
    // where anydrawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData ==NULL)
    {
        fprintf (stderr,"Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create thebitmap context. We want pre-multiplied ARGB, 8-bits
    // per component.Regardless of what the source image format is
    // (CMYK,Grayscale, and so on) it will be converted over to the format
    // specified hereby CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,     // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context ==NULL)
    {
        free (bitmapData);
        fprintf (stderr,"Context not created!");
    }
    // Make sure andrelease colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

//0 密码中不能含有特殊字符  1密码不能和用户名相同 2密码通过
+ (id)marrySecurity:(NSString *)password withUserName:(NSString *)userName
{
    if (password.length >= 6 && password.length <= 20) {
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:@"\\W" options:0 error:nil];
        if (regex1 != nil) {
            NSTextCheckingResult *result1 = [regex1 firstMatchInString:password options:0 range:NSMakeRange(0, [password length])];
            if (!result1) {
                NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:@"^[a-z0-9A-Z]{6,20}$" options:0 error:nil];
                if (regex2 != nil) {
                    NSTextCheckingResult *result2 = [regex2 firstMatchInString:password options:0 range:NSMakeRange(0, [password length])];
                    if (result2) {
                        if ([password rangeOfString:@"_"].location == NSNotFound) {
                            if (![password isEqualToString:userName]) {
                                return [NSNumber numberWithInt:2];
                            }else {
                                return [NSNumber numberWithInt:1];
                            }
                        }
                    }
                }
            }
        }
    }
    return [NSNumber numberWithInt:0];
}

//0 用户名格式不匹配    1 不允许出现大写字母    2 不允许出现特殊字符     3 通过
+ (id)marryUserName:(NSString *)name
{
    int userNameCharLength = [self convertToInt:name];
    if (userNameCharLength >= 4 && userNameCharLength <= 20) {
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:@"\\W" options:0 error:nil];
        if (regex1 != nil) {
            NSTextCheckingResult *result1 = [regex1 firstMatchInString:name options:0 range:NSMakeRange(0, [name length])];
            if (!result1) {
                NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]+" options:0 error:nil];
                if (regex2 != nil) {
                    NSTextCheckingResult *result2 = [regex2 firstMatchInString:name options:0 range:NSMakeRange(0, [name length])];
                    if (!result2) {
                        if ([name rangeOfString:@"_"].location == NSNotFound) {
                            return [NSNumber numberWithInt:3];
                        }else {
                            return [NSNumber numberWithInt:2];
                        }
                    }else {
                        return [NSNumber numberWithInt:1];
                    }
                }
            }
        }
    }
    return [NSNumber numberWithInt:0];;
}

//0 姓名格式不正确  1 正确
+ (id)marryPersonalName:(NSString *)name
{
    if (name.length >= 2 && name.length <= 6) {
        if ([name rangeOfString:@" "].location != NSNotFound) {
            return [NSNumber numberWithInt:0];
        }
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:@"\\W" options:0 error:nil];
        if (regex1 != nil) {
            NSTextCheckingResult *result1 = [regex1 firstMatchInString:name options:0 range:NSMakeRange(0, [name length])];
            if (!result1) {
                NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:@"[0-9_a-zA-Z]+" options:0 error:nil];
                if (regex2 != nil) {
                    NSTextCheckingResult *result2 = [regex2 firstMatchInString:name options:0 range:NSMakeRange(0, [name length])];
                    if (!result2) {
                        return [NSNumber numberWithInt:1];
                    }
                }
            }
        }
    }
    return [NSNumber numberWithInt:0];
}

+ (BOOL) marryEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL) marryMoney:(NSString *)money
{
    NSString *moneyRegex = @"[0-9]{1,4}\\.[0-9]{1}|[0-9]{1,4}";
    NSPredicate *moneyTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", moneyRegex];
    if ([moneyTest evaluateWithObject:money]) {
        moneyRegex = @"^[0]{1}\\.[0-9]{1}|^[0]{1}";
        moneyTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", moneyRegex];
        if ([moneyTest evaluateWithObject:money]) {
            return YES;
        }else {
            moneyRegex = @"0[0-9]+\\.[0-9]{1}|0[0-9]+";
            moneyTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", moneyRegex];
            if (![moneyTest evaluateWithObject:money]) {
//                moneyRegex = @"[9]{4}\\.[1-9]{1}";
//                moneyTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", moneyRegex];
//                if (![moneyTest evaluateWithObject:money]) {
//                    return YES;
//                }
                return YES;
            }
        }
    }
    return NO;
}

+ (int)convertToInt:(NSString*)strtemp {
    
    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
    
}


+(UIViewController*)getParentViewController:(UIView*)view{
    id object = [view nextResponder];
    while (![object isKindOfClass:[UIViewController class]] &&object != nil) {
        object = [object nextResponder];
    }
    return (UIViewController*)object;
}

//+(void)hideLeftMenu{
//    appdelegate* app= (SGAppDelegate*)[UIApplication sharedApplication].delegate;
//    [app.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
//}

//+(void)showLeftMenu{
//    SGAppDelegate* app= (SGAppDelegate*)[UIApplication sharedApplication].delegate;
//    [app.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
//}
//+(void)showSaoYiSaoUMShareSDKSheetView:(UIViewController*)sheetView
//                             shareText:(NSString*)text
//                            shareImage:(UIImage *)shareImage
//                              delegate:(id <UMSocialUIDelegate>)delegate{
//    [UMSocialSnsService presentSnsIconSheetView:sheetView
//                                         appKey:ShareSDK_Appkey
//                                      shareText:text
//                                     shareImage:shareImage
//                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone]
//                                       delegate:delegate];
//}
//+(void)showUMShareSDKSheetView:(UIViewController*)sheetView
//                     shareText:(NSString*)text
//                    shareImage:(UIImage *)shareImage
//                      delegate:(id <UMSocialUIDelegate>)delegate{
//    
//    [UMSocialSnsService presentSnsIconSheetView:sheetView
//                                         appKey:ShareSDK_Appkey
//                                      shareText:text
//                                     shareImage:shareImage
//                                shareToSnsNames:@[UMShareToSina,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToTencent,UMShareToQzone,UMShareToQQ]
//                                       delegate:delegate];
//}

+(NSArray*)getPlistFile:(NSString*)fileName{
    NSArray *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [ doc objectAtIndex:0 ];
    
    NSLog(@"%@",docPath);
    NSString* filePath=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    NSArray* arrayData=[NSArray arrayWithContentsOfFile:filePath];
    return arrayData;
}

+(void)writePlistFile:(NSArray*)data andFileName:(NSString*)fileName{
    NSArray *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [ doc objectAtIndex:0 ];
    [data writeToFile:[docPath stringByAppendingPathComponent:fileName] atomically:YES];
}

+(NSString*)insertString:(NSString*)addStr originalStr:(NSString*)oStr atIndex:(int)index{
    NSMutableString* changeString=[NSMutableString stringWithString:oStr];
    [changeString insertString:addStr atIndex:index];
    NSString* str = [NSString stringWithString:changeString];
    return str;
}

+(NSString*)nowDateWithFormater:(NSString*)formater{
    NSDate* nowDate=[NSDate date];
    NSDateFormatter* dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:formater];
    return [dateFormatter stringFromDate:nowDate];
}

+(NSString*)nowWeekDay{
    NSString* weekDay=[self nowDateWithFormater:@"EEEE"];
    if ([[weekDay substringToIndex:2] isEqualToString:@"星期"]) {
        return weekDay;
    }
    if ([weekDay isEqualToString:@"Monday"]) {
        return @"星期一";
    }
    else if ([weekDay isEqualToString:@"Tuesday"]) {
        return @"星期二";
    }
    else if ([weekDay isEqualToString:@"Wednesday"]) {
        return @"星期三";
    }
    else if ([weekDay isEqualToString:@"Thursday"]) {
        return @"星期四";
    }
    else if ([weekDay isEqualToString:@"Friday"]) {
        return @"星期五";
    }
    else if ([weekDay isEqualToString:@"Saturday"]) {
        return @"星期六";
    }
    else if ([weekDay isEqualToString:@"Sunday"]) {
        return @"星期日";
    }
    return @"星期日";
}

+(NSString*)getChineseCalendarWithDate:(NSDate *)date{
    NSArray *chineseYears = [NSArray arrayWithObjects:
                             @"甲子", @"乙丑", @"丙寅",    @"丁卯",    @"戊辰",    @"己巳",    @"庚午",    @"辛未",    @"壬申",    @"癸酉",
                             @"甲戌",    @"乙亥",    @"丙子",    @"丁丑", @"戊寅",    @"己卯",    @"庚辰",    @"辛己",    @"壬午",    @"癸未",
                             @"甲申",    @"乙酉",    @"丙戌",    @"丁亥",    @"戊子",    @"己丑",    @"庚寅",    @"辛卯",    @"壬辰",    @"癸巳",
                             @"甲午",    @"乙未",    @"丙申",    @"丁酉",    @"戊戌",    @"己亥",    @"庚子",    @"辛丑",    @"壬寅",    @"癸丑",
                             @"甲辰",    @"乙巳",    @"丙午",    @"丁未",    @"戊申",    @"己酉",    @"庚戌",    @"辛亥",    @"壬子",    @"癸丑",
                             @"甲寅",    @"乙卯",    @"丙辰",    @"丁巳",    @"戊午",    @"己未",    @"庚申",    @"辛酉",    @"壬戌",    @"癸亥", nil];
    
    NSArray *chineseMonths=[NSArray arrayWithObjects:
                            @"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",
                            @"九月", @"十月", @"冬月", @"腊月", nil];
    
    
    NSArray *chineseDays=[NSArray arrayWithObjects:
                          @"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",
                          @"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十",
                          @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十",  nil];
    
    
    NSCalendar *localeCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    
    NSDateComponents *localeComp = [localeCalendar components:unitFlags fromDate:date];
    
    NSString *y_str = [chineseYears objectAtIndex:localeComp.year-1];
    
    NSString *m_str = [chineseMonths objectAtIndex:localeComp.month-1];
    NSString *d_str = [chineseDays objectAtIndex:localeComp.day-1];
    
    NSString *chineseCal_str =[NSString stringWithFormat: @"农历%@%@%@",y_str,m_str,d_str];
    
    return chineseCal_str;
}

+(NSString*)getFalseDataJson:(NSString*)filePath{
    NSString* content = [NSString stringWithContentsOfFile:[[[NSBundle mainBundle]resourcePath] stringByAppendingPathComponent:filePath] encoding:NSUTF8StringEncoding error:nil];
    return content;
}

+(void)saveEntitysData:(id)entity fileName:(NSString*)fileName{
    NSMutableData *data = [NSMutableData data];
    
    NSKeyedArchiver *arch = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    
    [arch encodeObject:entity forKey:fileName];
    
    [arch finishEncoding];
    
    NSArray *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docPath = [ doc objectAtIndex:0 ];
   
    [data writeToFile:[docPath stringByAppendingPathComponent:fileName] atomically:YES];
}
+(id)entitysData:(NSString*)fileName{
    NSArray *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [ doc objectAtIndex:0];
    NSData *entitysData = [NSData dataWithContentsOfFile:[docPath stringByAppendingPathComponent:fileName]];
    NSKeyedUnarchiver *unArch = [[NSKeyedUnarchiver alloc]initForReadingWithData:entitysData];
    return [unArch decodeObjectForKey:fileName];
}

+(NSString*)timeIntervalSince1970{
    NSDate* nowDate=[[NSDate alloc]init];
    return [NSString stringWithFormat:@"%.0f",[nowDate timeIntervalSince1970]*1000];
}

+(NSString *)intervalSinceNow:(NSString *)theDate
{
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *d=[date dateFromString:[self formatDayAndTimeFrom1970Seconds:theDate]];
    NSTimeInterval late=[d timeIntervalSince1970]*1;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *timeString=@"";
    NSTimeInterval cha=now-late;
    if (cha/3600<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/60];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@分钟前", timeString];
    }
    if (cha/3600>1&&cha/86400<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/3600];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@小时前", timeString];
    }
    if (cha/86400>1)
    {
        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@天前", timeString];
    }
    return timeString;
}


+(NSString *)compareCurrentTime:(NSDate*)compareDate
{
    NSTimeInterval  timeInterval = [compareDate timeIntervalSinceNow];
    timeInterval = -timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%ld分前",temp];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    
    else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%ld天前",temp];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld月前",temp];
    }
    else{
        temp = temp/12;
        //result = [NSString stringWithFormat:@"%ld年前",temp];
        result=@"不久前";
    }
    
    return  result;
}


+(UIColor *)hexStringToColor:(NSString *)hex{
    //删除字符串中的空格
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6)
    {
        return [UIColor clearColor];
    }
    // strip 0X if it appears
    //如果是0x开头的，那么截取字符串，字符串从索引为2的位置开始，一直到末尾
    if ([cString hasPrefix:@"0X"])
    {
        cString = [cString substringFromIndex:2];
    }
    //如果是#开头的，那么截取字符串，字符串从索引为1的位置开始，一直到末尾
    if ([cString hasPrefix:@"#"])
    {
        cString = [cString substringFromIndex:1];
    }
    if ([cString length] != 6)
    {
        return [UIColor clearColor];
    }
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    //r
    NSString *rString = [cString substringWithRange:range];
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:1.0];
    
}
+(NSString*)encodeUrlString:(NSString*)urlString{
    NSString *result = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                            (CFStringRef)urlString, nil,
                                                                                            (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    return result;
}

+(NSInteger)getChartLong:(NSString *)strtemp{
    NSInteger strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
        
    }
    return strlength;
}
//获取本地积分
+(void)getUserJifen{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"...%@",[ud objectForKey:@"currentUserID"]);
    
    if ([ud objectForKey:@"currentUserID"] != nil && [[NSString stringWithFormat:@"%@",[ud objectForKey:@"currentUserID"]] length]!= 0) {
        NSLog(@"currentUserID:%@",[ud objectForKey:@"currentUserID"]);
        NSString *userID = [ud objectForKey:@"currentUserID"];
        
        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *plistPaht=[paths objectAtIndex:0];
        
        NSString *fileName=[plistPaht stringByAppendingPathComponent:@"userInformation.plist"];
        
        NSLog(@"%@",fileName);
        NSDictionary *userDictionary = [[NSDictionary alloc]initWithContentsOfFile:fileName];
        
        NSDictionary *userInfoDic = [[NSDictionary alloc]initWithDictionary:
                                     [userDictionary objectForKey:[NSString stringWithFormat:@"%@",userID]]];
        
        NSLog(@"%@",userInfoDic);
//        TSMDataCenter *dataCenter = [TSMDataCenter defaultDataCenter];
//        [dataCenter setUserJifen:userInfoDic[@"jifen"]];
    }
}
//+(void)SaveUserJifen:(NSString *)userJifen{
//    TSMDataCenter *tsmdata=[TSMDataCenter defaultDataCenter];
//    tsmdata.userJifen=userJifen;
//    NSLog(@"--方法里--%@",tsmdata.userJifen);
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    //积分存到本地
//    if ([ud objectForKey:@"currentUserID"] != nil && [[NSString stringWithFormat:@"%@",[ud objectForKey:@"currentUserID"]] length]!= 0) {
//        NSLog(@"currentUserID:%@",[ud objectForKey:@"currentUserID"]);
//        NSString *userID = [ud objectForKey:@"currentUserID"];
//        
//        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *plistPaht=[paths objectAtIndex:0];
//        
//        NSString *fileName=[plistPaht stringByAppendingPathComponent:@"userInformation.plist"];
//        
//        NSLog(@"%@",fileName);
//        NSDictionary *userDictionary = [[NSDictionary alloc]initWithContentsOfFile:fileName];
//        
//        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc]initWithDictionary:
//                                            [userDictionary objectForKey:[NSString stringWithFormat:@"%@",userID]]];
//        [userInfoDic setObject:tsmdata.userJifen forKey:@"jifen"];
//        
//        
//        
//        
//        if (![TSMFileManager isExistAtFile:fileName]) {
//            NSMutableDictionary *rootDic=[[NSMutableDictionary alloc]init];
//            [rootDic setObject:userInfoDic forKey:[NSString stringWithFormat:@"%@",tsmdata.userID]];
//            [rootDic writeToFile:fileName atomically:YES];
//            
//            [userInfoDic release];
//            [rootDic release];
//        }else{
//            NSMutableDictionary *rootDic = [[NSMutableDictionary alloc]initWithContentsOfFile:fileName];
//            [rootDic setObject:userInfoDic forKey:[NSString stringWithFormat:@"%@",tsmdata.userID]];
//            [rootDic writeToFile:fileName atomically:YES];
//            [userInfoDic release];
//            [rootDic release];
//        }
//    }
//
//}

//时间戳转换
+ (NSString *)timeString:(NSString *)timeStamp{
    NSDate *createDate = [NSDate dateWithTimeIntervalSince1970:[timeStamp doubleValue]/1000];
    
    // 创建一个日期格式器
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    // 为日期格式器设置格式字符串
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *selectedDateYearString = [dateFormatter stringFromDate:createDate];
    return selectedDateYearString;
}

//+ (NSInteger)isWifiEnvironment
//{
////    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
////    //结果说明：0-无连接   1-wifi    2-3G
////    NSInteger stateNet = [reachability currentReachabilityStatus];
////    return stateNet;
//}



+( long long ) fileSizeAtPath:( NSString *) filePath{
    
    NSFileManager * manager = [ NSFileManager defaultManager ];
    
    if ([manager fileExistsAtPath :filePath]){
        
        return [[manager attributesOfItemAtPath :filePath error : nil ] fileSize ];
    }
    
    return 0 ;
    
}


+ ( float ) folderSizeAtPath:( NSString *) folderPath{
    
    NSFileManager * manager = [ NSFileManager defaultManager ];
    
    if (![manager fileExistsAtPath :folderPath]) return 0 ;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath :folderPath] objectEnumerator ];
    
    NSString * fileName;
    
    long long folderSize = 0 ;
    
    while ((fileName = [childFilesEnumerator nextObject ]) != nil ){
        
        NSString * fileAbsolutePath = [folderPath stringByAppendingPathComponent :fileName];
        
        folderSize += [ self fileSizeAtPath :fileAbsolutePath];
        
    }
    
    
    //    NSString* fileData;
    //
    //    if (folderSize > 1024) {
    //
    //        folderSize =  folderSize / 1024.0;
    //
    //        fileData = [NSString stringWithFormat:@"%.1lldM",folderSize];
    //
    //    }else{
    //
    //        fileData =[NSString stringWithFormat:@"%lldKB",folderSize];
    //    }
    //
    
    return folderSize/( 1024.0 * 1024.0 );
    
    
    //    return fileData;
}



//计算缓存

+ ( NSString *) YzfolderSizeAtPath:( NSString *) folderPath{
    
    NSFileManager * manager = [ NSFileManager defaultManager ];
    
    if (![manager fileExistsAtPath :folderPath]) return 0 ;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath :folderPath] objectEnumerator ];
    
    NSString * fileName;
    
    CGFloat folderSize = 0 ;
    
    while ((fileName = [childFilesEnumerator nextObject ]) != nil ){
        
        NSString * fileAbsolutePath = [folderPath stringByAppendingPathComponent :fileName];
        
        folderSize += [ self fileSizeAtPath :fileAbsolutePath];
        
    }
    
    folderSize = folderSize/1024;
    
    NSString* fileData;
    
    if (folderSize > 1024) {
        
        folderSize =  folderSize / 1024.0;
        
        fileData = [NSString stringWithFormat:@"%.1fM",folderSize];
        
    }else{
        
        fileData =[NSString stringWithFormat:@"%.1fKB",folderSize];
    }
    
    return fileData;
}


//清理缓存

+(void)clearFile
{
    NSString * cachPath = [ NSSearchPathForDirectoriesInDomains ( NSCachesDirectory , NSUserDomainMask , YES ) firstObject ];
    
    NSArray * files = [[ NSFileManager defaultManager ] subpathsAtPath :cachPath];
    
    NSLog ( @"cachpath = %@" , cachPath);
    
    for ( NSString * p in files) {
        
        NSError * error = nil ;
        
        NSString * path = [cachPath stringByAppendingPathComponent :p];
        
        if ([[ NSFileManager defaultManager ] fileExistsAtPath :path]) {
            
            [[ NSFileManager defaultManager ] removeItemAtPath :path error :&error];
            
        }
        
    }
    
    //    [ self performSelectorOnMainThread : @selector (clearCachSuccess) withObject : nil waitUntilDone : YES ];
    
}

//是否已缓存网页;

+(BOOL)GetWebCachaData:(NSString *)RequsetUrl{
    
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    //获取document路径
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,      NSUserDomainMask, YES) objectAtIndex:0];
    
    [fileManager createDirectoryAtPath:[cachesPath stringByAppendingString:@"/Caches"]withIntermediateDirectories:YES attributes:nil error:nil];
    
    //写入路径
    NSString * path = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/Caches/%lu.html",(unsigned long)[RequsetUrl hash]]];
    
    BOOL result = [fileManager fileExistsAtPath:path];
    
    return result;
    
}



//网页写入缓存
+(void)writeToCache:(NSString *)RequsetUrl
{
    NSString * htmlResponseStr = [NSString stringWithContentsOfURL:[NSURL URLWithString:RequsetUrl] encoding:NSUTF8StringEncoding error:Nil];
    //创建文件管理器
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    //获取document路径
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,      NSUserDomainMask, YES) objectAtIndex:0];
    
    [fileManager createDirectoryAtPath:[cachesPath stringByAppendingString:@"/Caches"]withIntermediateDirectories:YES attributes:nil error:nil];
    
    //写入路径
    NSString * path = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/Caches/%lu.html",(unsigned long)[RequsetUrl hash]]];
    
    [htmlResponseStr writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

+(void)YzSetWebCache:(NSString *)RequsetUrl webView:(WKWebView *)webView{
    
//    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES) objectAtIndex:0];
//
//    NSString * path = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/Caches/%lu.html",
//                                                           (unsigned long)[RequsetUrl hash]]];
    
//    NSString *htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
//    if (!(htmlString ==nil || [htmlString isEqualToString:@""])) {
//
//
//        //已缓存网页
//        [webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:RequsetUrl]];
//
//
//
//    }else{
//        //未缓存
//
//        NSURL *url = [NSURL URLWithString:RequsetUrl];
//
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//
//        [webView loadRequest:request];
//
//        [GCUtil writeToCache:RequsetUrl];
//    }

    NSURL *url = [NSURL URLWithString:RequsetUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [webView loadRequest:request];
    
    [GCUtil writeToCache:RequsetUrl];
}







@end
