//
//  MJHeaderFooterBuilder.h
//  ASJ
//
//  Created by zhang ming on 2018/3/1.
//  Copyright © 2018年 TS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJHeaderFooterBuilder : NSObject
+ (MJRefreshStateHeader *)mj_headerWithTarget:(id)target selector:(SEL)selctor;
+ (MJRefreshBackNormalFooter *)mj_footerWithTarget:(id)target selector:(SEL)selctor;
@end
