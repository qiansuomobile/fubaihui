//
//  MJHeaderFooterBuilder.m
//  ASJ
//
//  Created by zhang ming on 2018/3/1.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "MJHeaderFooterBuilder.h"

@implementation MJHeaderFooterBuilder

+ (MJRefreshStateHeader *)mj_headerWithTarget:(id)target selector:(SEL)selctor{
    return [MJRefreshStateHeader headerWithRefreshingTarget:target refreshingAction:selctor];
}

+ (MJRefreshBackNormalFooter *)mj_footerWithTarget:(id)target selector:(SEL)selctor{
    return [MJRefreshBackNormalFooter footerWithRefreshingTarget:target refreshingAction:selctor];
}

/*
 self.tableView.mj_header =
 self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
 */
@end
