//
//  NSDictionary+SGJSON.h
//  HLJNewsApp
//
//  Created by SGD on 14-5-9.
//  Copyright (c) 2014年 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SGJSON)

-(NSString*)JSONValue;

@end
