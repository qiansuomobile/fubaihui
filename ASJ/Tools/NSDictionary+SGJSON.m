//
//  NSDictionary+SGJSON.m
//  HLJNewsApp
//
//  Created by SGD on 14-5-9.
//  Copyright (c) 2014年 SG. All rights reserved.
//

#import "NSDictionary+SGJSON.h"

@implementation NSDictionary (SGJSON)

-(NSString *)JSONValue{
    return [[NSString alloc]initWithData:[NSJSONSerialization dataWithJSONObject:self
                                                                  options:kNilOptions
                                                                    error:nil]
                         encoding:NSUTF8StringEncoding];
    
    
}

@end
