//
//  NSString+SGJSON.h
//  HLJNewsApp
//
//  Created by SGD on 14-4-3.
//  Copyright (c) 2014年 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SGJSON)

- (id)JSONValue;


@end
