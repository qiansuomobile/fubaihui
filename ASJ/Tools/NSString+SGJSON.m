//
//  NSString+SGJSON.m
//  HLJNewsApp
//
//  Created by SGD on 14-4-3.
//  Copyright (c) 2014年 SG. All rights reserved.
//

#import "NSString+SGJSON.h"

@implementation NSString (SGJSON)

-(id)JSONValue{
    return [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding]
                                           options:kNilOptions
                                             error:nil];
}

@end
