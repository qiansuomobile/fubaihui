//
//  NetMethod.h
//  Yesyes
//
//  Created by 叶岳洋 on 16/3/31.
//  Copyright © 2016年 尹超. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetMethod : NSObject
/**
 *  发送get请求
 *
 *  @param URLString  请求的基本的url
 *  @param parameters 请求的参数字典
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 */
+ (void)GET:(NSString *)URLString
 parameters:(id)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSError *error))failure;


/**
 *  发送post请求
 *
 *  @param URLString  请求的基本的url
 *  @param parameters 请求的参数字典
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 */
+ (void)Post:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure;

/**
 *  上传请求
 *
 *  @param URLString  请求的基本的url
 *  @param parameters 请求的参数字典
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
// */
//+ (void)Upload:(NSString *)URLString
//    parameters:(id)parameters
//   uploadParam:(NSDictionary *)uploadParam
//       success:(void (^)(id responseObject))success
//       failure:(void (^)(NSError *error))failure;



//上传图片

+(void)Post:(NSString *)URLString
 parameters:(id)parameters
IMGparameters:(NSDictionary *)IMGparameters
    success:(void (^)(id))success
    failure:(void (^)(NSError *))failure;






+(NSString *)setEMOjiStr:(NSString *)EmojiString;



+(NSString *)GetEmojiStr:(NSString *)EmojiString;




+ (NSString *)saveFileToDocuments:(NSString *)url;

+ (BOOL)getUserToken:(void (^) (BOOL result)) result;

+ (void)sendDeviceToken:(NSString *)deviceToken;

@end
