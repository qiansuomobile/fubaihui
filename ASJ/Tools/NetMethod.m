//
//  NetMethod.m
//  Yesyes
//
//  Created by 叶岳洋 on 16/3/31.
//  Copyright © 2016年 尹超. All rights reserved.
//

#import "NetMethod.h"
#import "AFNetworking.h"
#import "SDWebImageManager.h"



@implementation NetMethod

+(void)Post:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(NSError *))failure{
   
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    
    NSLog(@"实际发送url:%@ body:%@ ",URLString,parameters);
    
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //yu
        if ([parameters[@"file"] isKindOfClass:[NSData class]]) {
            [formData appendPartWithFileData:parameters[@"file"] name:@"file" fileName:@"imageFile.jpg" mimeType:@"image/jpg"];
        }

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
//            NSLog(@"实际发送url:%@ body:%@ ",URLString,parameters);
            NSLog(@"返回的数据:%@  ",responseObject);

                        success(responseObject);
                    }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
                        failure(error);
                    }
    }];
    
}

+(void)GET:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/json", nil];
//    [manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
//        
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//       
//        if (success) {
//            success(responseObject);
//        }
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
//    
//    [manager GET:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if (success) {
//            success(responseObject);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
    
    NSLog(@"实际发送url:%@ body:%@ ",URLString,parameters);
    [manager GET:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            
            NSLog(@"返回的数据:%@  ",responseObject);

                        success(responseObject);
                    }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
                        failure(error);
                    }
    }];

}


+(void)Post:(NSString *)URLString parameters:(id)parameters IMGparameters:(NSDictionary *)IMGparameters success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        id obj = IMGparameters[IMGparameters.allKeys[0]];
        if ([obj isKindOfClass:[NSArray class]]) {
            NSArray *arr = obj;
            int i = 0;
            for (NSData *data in arr) {
                
                [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"file%d",i] fileName:[NSString stringWithFormat:@"imageFile%d.jpg",i] mimeType:@"image/jpg"];
                i++;
            }
        }else{
            [formData appendPartWithFileData:IMGparameters[IMGparameters.allKeys[0]] name:IMGparameters.allKeys[0] fileName:@"imageFile.jpg" mimeType:@"image/jpg"];
        }
    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              if (success) {
                  success(responseObject);
                  NSLog(@"返回的数据:%@  ",responseObject);
              }
              
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure) {
            failure(error);
        }

    }];
}



//表情转字符串

+(NSString *)setEMOjiStr:(NSString *)EmojiString{
    
    NSString *uniStr = [NSString stringWithUTF8String:[EmojiString UTF8String]];
    
    NSData *uniData = [uniStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *goodStr = [[NSString alloc] initWithData:uniData encoding:NSUTF8StringEncoding] ;
    
    return goodStr;
    
}


//字符串转表情

+(NSString *)GetEmojiStr:(NSString *)EmojiString{
    
    const char *jsonString = [EmojiString UTF8String];   // goodStr 服务器返回的 json
    
    NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
    
    NSString *goodMsg1 = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];

    return goodMsg1;

}

// 下载文件到Documents中并返回路径
+ (NSString *)saveFileToDocuments:(NSString *)url
{
    NSString *resultFilePath = @"";
    if (url.length > 7) {// 本应用服务器端的域名是7位，所以先判断url是否正确
        
        
        NSString *destFilePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:[url substringFromIndex:7]]; // 去除域名，组合成本地文件PATH
        NSString *destFolderPath = [destFilePath stringByDeletingLastPathComponent];
        
        // 判断路径文件夹是否存在不存在则创建
        if (! [[NSFileManager defaultManager] fileExistsAtPath:destFolderPath]) {
            
            [[NSFileManager defaultManager] createDirectoryAtPath:destFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        // 判断该文件是否已经下载过
        if ([[NSFileManager defaultManager] fileExistsAtPath:destFilePath]) {
            
            resultFilePath = destFilePath;
        } else {
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            if ([imageData writeToFile:destFilePath atomically:YES]) {
                resultFilePath = destFilePath;
            }
        }
    }
    
    
    return resultFilePath;
}


+ (BOOL)getUserToken:(void (^)(BOOL))result{
    
    NSDictionary *parameDic = @{@"uid":LoveDriverID, @"app_key":TOKEN_KEY};
    [NetMethod Post:FBHRequestUrl(kUrl_get_token) parameters:parameDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        int code = [[dic objectForKey:@"code"] intValue];
        if (code == 200){
            NSString *token = dic[@"data"];
            NSLog(@"user token %@", token);
            [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"FBH_USER_TOKEN"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            result(YES);
        }else{
            result(NO);
        }
    } failure:^(NSError *error) {
        result(NO);
    }];
    return YES;
}


+ (void)sendDeviceToken:(NSString *)deviceToken{
    NSDictionary *param = @{@"uid":LoveDriverID,
                            @"token":FBH_USER_TOKEN,
                            @"device_token":deviceToken,
                            @"device_type":@"1"
                            };
    
    [NetMethod Post:FBHRequestUrl(kUrl_reveive_token) parameters:param success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            NSLog(@"上传device token成功");
        }else{
            [SVProgressHUD showErrorWithStatus:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"网络错误上传token失败");
    }];
}


@end
