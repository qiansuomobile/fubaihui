//
//  VerifyPhoneNumber.h
//  BWSXiaoMaChe
//
//  Created by lujialong on 16/1/8.
//  Copyright © 2016年 Journey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VerifyPhoneNumber : NSObject
+ (BOOL)isMobileNumber:(NSString *)mobileNum;



//空字符串  判断
+ (BOOL)isBlankString:(NSString *)string;

+ (BOOL)checkTelNumber:(NSString*) telNumber;
@end
