//
//  VerifyPictureURL.h
//  ASJ
//
//  Created by 叶岳洋 on 16/9/5.
//  Copyright © 2016年 TS. All rights reserved.
//
#import "SortGoods.h"

#import "GoodsDetailViewController.h"

#import <UIKit/UIKit.h>


#import <Foundation/Foundation.h>

@interface VerifyPictureURL : NSObject


+(NSString *)GetPictureURL:(NSString *)contentStr;

// 时间戳转换
+(NSString *)dateStringFromNumberTimer:(NSString *)timerStr;


+(NSString *)dateStringFromTimer:(NSString *)timerStr;

//二、根据NSString类型时间戳转换为日期格式

+ (NSString *)getDateAccordingTime:(NSString *)aTime formatStyle:(NSString *)formate;


//转换成星期
+ (NSString *)getWeekDayFordate:(NSString *)data;

//哪天
+ (NSString *)ctime:(NSString *)Week;


+(void)YZcheckNETwork;



//+(BOOL)requestDetailDataWith:(NSString *)goodID;

//空字符串判断

+(BOOL) isBlankString:(NSString *)string;





+(void)setUpBtn:(UIButton*)btn title:(NSString *)title tag:(NSInteger)tag;


+(void)setUpBtn:(UIButton*)btn font:(UIFont *)font title:(NSString *)title color:(UIColor*)color tag:(NSInteger)tag;

//将手机号转换成131*****540格式
+(NSString *)FormatPhoneNumber:(NSString *)phoneNum;


+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;

@end
