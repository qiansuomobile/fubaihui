//
//  VerifyPictureURL.m
//  ASJ
//
//  Created by 叶岳洋 on 16/9/5.
//  Copyright © 2016年 TS. All rights reserved.
//

#import "VerifyPictureURL.h"


//@interface VerifyPictureURL ()
//
//@end


@implementation VerifyPictureURL






+(NSString *)GetPictureURL:(NSString *)contentStr{
    
    NSRange range = [contentStr rangeOfString:@"Uploads"];
    contentStr = [contentStr substringFromIndex:range.location];
    NSRange range2 = [contentStr rangeOfString:@".jpg"];
    contentStr = [contentStr substringToIndex:range2.location];
    contentStr = [NSString stringWithFormat:@"http://www.woaisiji.com/%@.jpg",contentStr];
    return contentStr;
    
}


+ (NSString *)ctime:(NSString *)Week{
    
    
    // Tue Mar 10 17:32:22 +0800 2015
    // 字符串转换NSDate
    //    _created_at = @"Tue Mar 11 17:48:24 +0800 2015";
    
    // 日期格式字符串
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    //fmt.dateFormat = @"EEE MMM d HH:mm:ss Z yyyy";
    fmt.dateFormat = @"yyyy MM dd";
    // 设置格式本地化,日期格式字符串需要知道是哪个国家的日期，才知道怎么转换
    fmt.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
    long long time=[Week longLongValue];
    NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:time];
    //NSDate *date = [fmt dateFromString:_add_time];
    //        NSLog(@"date%@",date);
    if ([date isThisYear]) { // 今年
        
        if ([date isToday]) { // 今天
            
            // 计算跟当前时间差距
            NSDateComponents *cmp = [date deltaWithNow];
            
            if (cmp.hour >= 1) {
                return [NSString stringWithFormat:@"%ld小时之前",(long)cmp.hour];
            }else if (cmp.minute > 1){
                return [NSString stringWithFormat:@"%ld分钟之前",(long)cmp.minute];
            }else{
                return @"刚刚";
            }
            
        }else if ([date isYesterday]){ // 昨天
            fmt.dateFormat = @"  HH:mm";
            return  [fmt stringFromDate:date];
            
        }else{ // 前天
            
            fmt.dateFormat = @"MM-dd";
            return  [fmt stringFromDate:date];
        }
        
        
        
    }else{ // 不是今年
        
        fmt.dateFormat = @"yyyy-MM-dd HH:mm";
        //            NSLog(@"不是今年");
        return [fmt stringFromDate:date];
        
    }
    
    return Week;
    
    
}



+(NSString *)dateStringFromNumberTimer:(NSString *)timerStr{
    
    
//    +28800
    NSTimeInterval time=[timerStr doubleValue];//因为时差问题要加8小时 == 28800 sec
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
   
    
//    NSLog(@"date:%@",[detaildate description]);
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
     return  [dateFormatter stringFromDate: detaildate];


}


+(NSString *)dateStringFromTimer:(NSString *)timerStr{
    
    //转化为Double
//        double t = [timerStr doubleValue]/1000;
    //计算出距离1970的NSDate
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timerStr doubleValue]];
    //转化为 时间格式化字符串
    NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
    df.dateFormat = @"yyyy-MM-dd HH:MM";
    
    //    HH:mm:ss
    //转化为 时间字符串
    return [df stringFromDate:date];
}


+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
   
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"shanghai"];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"yyyy年MM月dd日 HH:mm"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]/ 1000.0];
    NSString* dateString = [formatter stringFromDate:date];
   
    return dateString;

}





//二、根据NSString类型时间戳转换为日期格式

+ (NSString *)getDateAccordingTime:(NSString *)aTime formatStyle:(NSString *)formate{
    
    NSDate *nowDate = [NSDate dateWithTimeIntervalSince1970:[aTime intValue]];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:formate];
    return[formatter stringFromDate:nowDate];
}





//根据时间戳获取星期几
+ (NSString *)getWeekDayFordate:(NSString *)data
{
    
    
    
    NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];

    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:[data intValue]];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:newDate];

    NSString *weekStr = [weekday objectAtIndex:components.weekday];
    
    return weekStr;
}



- (NSString *)interceptTimeStampFromStr:(NSString *)str{
    if (!str || [str length] == 0 ) {  // 字符串为空判断
        return @"";
    }
    NSMutableString * muStr = [NSMutableString stringWithString:str];
    NSString * timeStampString = [NSString string];
    //  遍历取出括号内的时间戳
    
    
    for (int i = 0; i < str.length; i ++) {
        NSRange startRang = [muStr rangeOfString:@"("];
        NSRange endRang = [muStr rangeOfString:@")"];
        if (startRang.location != NSNotFound) {
            // 左边括号位置
            NSInteger loc = startRang.location;
            // 右边括号距离左边括号的长度
            NSInteger len = endRang.location - startRang.location;
            // 截取括号时间戳内容
            timeStampString = [muStr substringWithRange:NSMakeRange(loc + 1,len - 1)];
        }
    }
    
    // 把时间戳转化成时间
    NSTimeInterval interval=[timeStampString doubleValue] / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *objDateformat = [[NSDateFormatter alloc] init];
    [objDateformat setDateFormat:@"yyyy-MM-dd"];
    //    HH:mm:ss.SSS
    NSString * timeStr = [NSString stringWithFormat:@"%@",[objDateformat stringFromDate: date]];
    
    return timeStr;
}

-(NSString *)timerConversion:(double)timeIntervalInMilliSecond
{
    NSTimeInterval time = (timeIntervalInMilliSecond+28800)/1000;//因为时差问题要加8小时 == 28800 sec
    NSDate * detaildate =[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //设定时间格式,这里可以设置成自己需要的格式
    
    [dateFormatter setDateFormat:@"yyyy.MM.dd HH:mm"];
    
    NSString * currentDateStr = [dateFormatter stringFromDate: detaildate];
    
    return currentDateStr;
    
}




+(void)YZcheckNETwork{
    
    //创建网络监听管理者对象
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    /*
     typedef NS_ENUM(NSInteger, AFNetworkReachabilityStatus) {
     AFNetworkReachabilityStatusUnknown          = -1,//未识别的网络
     AFNetworkReachabilityStatusNotReachable     = 0,//不可达的网络(未连接)
     AFNetworkReachabilityStatusReachableViaWWAN = 1,//2G,3G,4G...
     AFNetworkReachabilityStatusReachableViaWiFi = 2,//wifi网络
     };
     */
    //设置监听
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                //                NSLog(@"未识别的网络");
                
                
//                [MBProgressHUD showSuccess:@"未识别的网络" toView:super.];
            {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"未识别的网络" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                
            }
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
            {
                //                NSLog(@"不可达的网络(未连接)");
                
//                [MBProgressHUD showSuccess:@"网络异常,请检查网络连接"  toView:self.window.view];
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"网络异常,请检查网络连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];

                return ;
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"2G,3G,4G...的网络");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"wifi的网络");
                break;
            default:
                break;
        }
    }];
    
    //开始监听
    [manager startMonitoring];
    
    
    //     [self.view endEditing:YES];
    
    
}








//+(BOOL)requestDetailDataWith:(NSString *)goodID{
//   
//    NSDictionary *dic = @{@"id":goodID};
//   
//    
//    BOOL Jump;
////    MBProgressHUD *hud = [MBProgressHUD showMessag:@"正在加载" toView:self.view];
//    
//    [NetMethod Post:LoveDriverURL(@"APP/Shop/detail") parameters:dic success:^(id responseObject) {
//        //NSLog(@"responsed === %@",responseObject);
//        
//        
//        if ([responseObject[@"code"] isEqual:@200]) {
//            
//            
//            SortGoods *sortGood = [[SortGoods alloc] init];
//            [sortGood setValuesForKeysWithDictionary:responseObject[@"info"]];
//            sortGood.content = [VerifyPictureURL GetPictureURL:sortGood.content];
//            
////            return YES;
//            
////            hud.labelText = @"加载成功";
////            Jump = YES;
//            
////            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////                [hud hide:YES];
//              
////                GoodsDetailViewController *goodDetailVC =
//            [[GoodsDetailViewController alloc] init];
////                goodDetailVC.sortGoods = sortGood;
////
////                [self.navigationController pushViewController:goodDetailVC animated:YES];
//            
////            });
//        }
//    
//    } failure:^(NSError *error) {
//    
//        
//        
////        [MBProgressHUD showSuccess:NetProblem toView:self.view];
//   
//    }];
//
//
//    return NO;
//
//}



+(BOOL)isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



+(void)setUpBtn:(UIButton*)btn title:(NSString *)title tag:(NSInteger)tag{
    
    //    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.titleLabel.font = [UIFont systemFontOfSize:12];

    btn.tag= tag;

    [btn setTitle:title forState:UIControlStateNormal];
    
    btn.backgroundColor =[UIColor orangeColor];
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btn.layer setMasksToBounds:YES];
    
    [btn.layer setCornerRadius:2.0];
    
}




+(void)setUpBtn:(UIButton*)btn font:(UIFont *)font title:(NSString *)title color:(UIColor*)color tag:(NSInteger)tag{
    
    btn.titleLabel.font = font;
    
    btn.tag= tag;
    
    [btn setTitle:title forState:UIControlStateNormal];
    
    btn.backgroundColor = color;
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
//    [btn.layer setMasksToBounds:YES];
    
//    [btn.layer setCornerRadius:2.0];

    
    
}

+(NSString *)FormatPhoneNumber:(NSString *)phoneNum{
    if (phoneNum.length == 11) {
        NSString *str1 = [phoneNum substringWithRange:NSMakeRange(0, 3)];
        NSString *str2 = [phoneNum substringWithRange:NSMakeRange(8, 3)];
        phoneNum = [NSString stringWithFormat:@"%@*****%@",str1,str2];
        return phoneNum;
    }else{
        NSLog(@"手机号格式不对");
        return phoneNum;
    }
}

@end
