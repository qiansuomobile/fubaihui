//
//  YZShare.h
//  ASJ
//
//  Created by Dororo on 2019/7/15.
//  Copyright © 2019 TS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
#import "WXApiObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface YZShare : NSObject
+ (void)shareParamsByImage:(UIImage *)img
                   WXScene:(int)scene;
@end

NS_ASSUME_NONNULL_END
