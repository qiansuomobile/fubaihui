//
//  YZShare.m
//  ASJ
//
//  Created by Dororo on 2019/7/15.
//  Copyright © 2019 TS. All rights reserved.
//

#import "YZShare.h"

@implementation YZShare
+ (void)shareParamsByImage:(UIImage *)img WXScene:(int)scene{
    
    if (![WXApi isWXAppInstalled]) {
        [SVProgressHUD showInfoWithStatus:@"未安装微信"];
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = imageData;
    
    WXMediaMessage *message = [WXMediaMessage message];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"yyyGrayImg"
                                                         ofType:@"png"];
    message.thumbData = [NSData dataWithContentsOfFile:filePath];
    message.mediaObject = imageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}
@end
