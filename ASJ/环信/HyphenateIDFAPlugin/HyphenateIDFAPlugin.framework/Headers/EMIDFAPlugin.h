//
//  EMIDFAPlugin.h
//  HyphenateSDK
//
//  Created by XieYajie on 22/03/2017.
//  Copyright © 2017 easemob.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMIDFAPlugin : NSObject

+ (NSString *)getIDFA;

@end
