/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "ContactListViewController.h"

#import "ChatViewController.h"
#import "RobotListViewController.h"
#import "ChatroomListViewController.h"
#import "AddFriendViewController.h"
#import "ApplyViewController.h"
#import "UserProfileManager.h"
#import "RealtimeSearchUtil.h"
#import "UserProfileManager.h"
#import "DropMenuDelegateReceiver.h"
#import "BaseTableViewCell.h"
#import "UIViewController+SearchController.h"
#import "YzChangePassWord.h"
#import "ConversationListController.h"
#import "YzUserInfoModel.h"

#if DEMO_CALL == 1
#import "DemoConfManager.h"
#endif

@implementation NSString (search)

//根据用户昵称进行搜索
- (NSString*)showName
{
    return [[UserProfileManager sharedInstance] getNickNameWithUsername:self];
}

@end

@interface ContactListViewController ()<UISearchBarDelegate, UIActionSheetDelegate, EaseUserCellDelegate, EMSearchControllerDelegate>
{
    NSIndexPath *_currentLongPressIndex;
}

@property (strong, nonatomic) NSMutableArray *sectionTitles;
@property (strong, nonatomic) NSMutableArray *contactsSource;
@property (nonatomic, strong)UITableView* dropMenu;
@property (nonatomic, strong)DropMenuDelegateReceiver* receiver;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;

@property (nonatomic) NSInteger unapplyCount;

@property (nonatomic) NSIndexPath *indexPath;

@property (nonatomic, strong) NSArray *otherPlatformIds;

@end
//static NSInteger Pages;

@implementation ContactListViewController

-(void)LoadFrindList{
    self.dataSourceArray = [[NSMutableArray alloc]init];
    NSDictionary *userInfo = @{@"uid":LoveDriverID,
                               @"token":FBH_USER_TOKEN,
                               @"page":@"1",
                               @"row_num":@"50"
                               };
    [NetMethod Post:FBHRequestUrl(kUrl_friend_list) parameters:userInfo success:^(id responseObject) {
        NSDictionary *InfoDic = responseObject;
        
        //        NSLog(@"%@",InfoDic);
        
        if ([InfoDic[@"code"] isEqual:@200]){
            
            if ([InfoDic[@"data"] isKindOfClass:[NSNull class]]) {
                
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                
                return ;
            }
            
            [self.dataSourceArray removeAllObjects];
            for (NSDictionary *dic in InfoDic[@"data"]) {
                
//                YzUserInfoModel *model = [YzUserInfoModel mj_objectWithKeyValues:UserInfoDic];
//
//                model.UserID = UserInfoDic[@"id"];
                
                [self.dataSourceArray addObject:dic];
            }
            [self.tableView reloadData];
        }
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的好友";
    self.showRefreshHeader = YES;
    
    _contactsSource = [NSMutableArray array];
    _sectionTitles = [NSMutableArray array];
    [self LoadFrindList];

    // 环信UIdemo中有用到Parse, 加载用户好友个人信息
//    [[UserProfileManager sharedInstance] loadUserProfileInBackgroundWithBuddy:self.contactsSource saveToLoacal:YES completion:NULL];
    
    [self setupSearchController];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"yzzaddd"] style:UIBarButtonItemStylePlain target:self action:@selector(showDropDownMenu)];
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(haoyou:)name:@"haoyouTongZhi" object:nil];
}
//接受好友请求
- (void)haoyou:(NSNotification *)text{
    
    [self addFriend:LoveDriverID WithUserName:text.userInfo[@"str"]];
}
- (void)addFriend:(NSString *)loginName WithUserName:(NSString *)userName{
    //           加好友
    NSDictionary *Info = @{@"uid":LoveDriverID,
                           @"add_who":userName,
                           @"token":FBH_USER_TOKEN};
    
    [NetMethod Post:FBHRequestUrl(kUrl_friend_add) parameters:Info success:^(id responseObject) {
        if ([responseObject[@"code"] isEqual:@200]) {
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }else{
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }
        [self LoadFrindList];
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:error.localizedDescription toView:self.view];
    }];
}
//删除好友
- (void)deleltFriend:(NSString*)pid
{
    
    
    NSDictionary *Info = @{@"uid":LoveDriverID,
                           @"friend_id":pid,
                           @"token":FBH_USER_TOKEN};
    
    [NetMethod Post:FBHRequestUrl(kUrl_friend_del) parameters:Info success:^(id responseObject) {
        
        NSLog(@"-----------%@",responseObject);
        if ([responseObject[@"code"] isEqual:@200]) {
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }else{
            [MBProgressHUD showSuccess:responseObject[@"msg"] toView:self.view];
        }
        [self LoadFrindList];
    } failure:^(NSError *error) {
        [MBProgressHUD showSuccess:error.localizedDescription toView:self.view];
    }];
}

- (void)showDropDownMenu{
    self.dropMenu = [[UITableView alloc]initWithFrame:CGRectMake(YzWidth - 120, 60, 100, 80) style:UITableViewStylePlain];
    self.receiver = [DropMenuDelegateReceiver new];
    __weak typeof(self)weakSelf = self;
    self.receiver.block1 = ^{
        [weakSelf removeTableView];
        ConversationListController*chatListVC = [[ConversationListController alloc] initWithNibName:nil bundle:nil];
        [weakSelf.navigationController pushViewController:chatListVC animated:YES];
    };
    self.receiver.block2 = ^{
        [weakSelf removeTableView];
        [weakSelf addFriendAction];
    };
    self.receiver.block3 = ^{
        [weakSelf feedBack];
    };
    [self.dropMenu registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.dropMenu.delegate = self.receiver;
    self.dropMenu.dataSource = self.receiver;
    self.dropMenu.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.dropMenu.layer.borderWidth = 1;
    self.dropMenu.layer.cornerRadius = 4;
    [self.navigationController.view addSubview:self.dropMenu];
}
//意见反馈
- (void)feedBack{
    [self removeTableView];
    YzChangePassWord *Center = [[YzChangePassWord alloc]init];
    Center.SetTitle = @"意见反馈";
    [self.navigationController pushViewController:Center animated:YES];
}
- (void)removeTableView{
    if (self.dropMenu.superview) {
        [self.dropMenu removeFromSuperview];
        self.dropMenu = nil;
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadApplyView];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NSString *CellIdentifier = @"addFriend";
            EaseUserCell *cell = (EaseUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[EaseUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.avatarView.image = [UIImage imageNamed:@"newFriends"];
            cell.titleLabel.text = NSLocalizedString(@"title.apply", @"Application and notification");
            cell.avatarView.badge = self.unapplyCount;
            return cell;
        }
        return nil;
    }
    else {
        NSString *CellIdentifier = [EaseUserCell cellIdentifierWithModel:nil];
        EaseUserCell *cell = (EaseUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[EaseUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
        if(indexPath.row< conversations.count) {
            EMConversation *conversation =conversations[indexPath.row];
            cell.avatarView.badge = conversation.unreadMessagesCount;
        }
        NSDictionary *dic = [self.dataSourceArray objectAtIndex:indexPath.row];
        NSString *path = [NSString stringWithFormat:@"%@%@",FBHBaseURL,dic[@"headpic"]];
        [cell.avatarView.imageView sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        cell.titleLabel.text = dic[@"nickname"];
        
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger section = indexPath.section;
    if (section == 0) {
        [self.navigationController pushViewController:[ApplyViewController shareController] animated:YES];
    }else{
        NSDictionary *dic = self.dataSourceArray[indexPath.row];
        [self.searchController.searchBar endEditing:YES];
        YzUserInfoModel *model  = [[YzUserInfoModel alloc]init];
        model.uid = dic[@"add_who"];
        model.nickname = dic[@"nickname"];
        model.headpic = dic[@"headpic"];
        
        ChatViewController *chatController = [[ChatViewController alloc] initWithConversationChatter:model.uid conversationType:EMConversationTypeChat withInforModel:model];
        NSString *nickname =  dic[@"nickname"];
        chatController.model = model;
        chatController.title = nickname.length > 0 ? nickname : dic[@"add_who"];
        [self.navigationController pushViewController:chatController animated:YES];
    }
}
                                                       
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //在iOS8.0上，必须加上这个方法才能出发左划操作
    NSDictionary *dic = [self.dataSourceArray objectAtIndex:indexPath.row];
    self.indexPath = nil;
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient].contactManager deleteContact:dic[@"add_who"] isDeleteConversation:YES];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [self deleltFriend:dic[@"add_who"]];
                [self.dataSourceArray removeObjectAtIndex:indexPath.row];
                [tableView  deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                [tableView endUpdates];
            }
            else{
                [weakSelf showHint:[NSString stringWithFormat:NSLocalizedString(@"deleteFailed", @"Delete failed:%@"), error.errorDescription]];
                [weakSelf.tableView reloadData];
            }
        });
    });
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex || _currentLongPressIndex == nil) {
        return;
    }
    
    NSIndexPath *indexPath = _currentLongPressIndex;
    EaseUserModel *model = [[self.dataArray objectAtIndex:(indexPath.section - 2)] objectAtIndex:indexPath.row];
    _currentLongPressIndex = nil;
    
    [self hideHud];
    [self showHudInView:self.view hint:NSLocalizedString(@"wait", @"Pleae wait...")];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient].contactManager addUserToBlackList:model.buddy relationshipBoth:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf hideHud];
            if (!error) {
                //由于加入黑名单成功后会刷新黑名单，所以此处不需要再更改好友列表
                [weakSelf reloadDataSource];
            }
            else {
                [weakSelf showHint:error.errorDescription];
            }
        });
    });
    
}
                                                       
#pragma mark - EaseUserCellDelegate
                                                       
- (void)cellLongPressAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 || indexPath.section == 1) {
        return;
    }
    
    _currentLongPressIndex = indexPath;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") destructiveButtonTitle:NSLocalizedString(@"friend.block", @"join the blacklist") otherButtonTitles:nil, nil];
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
}
                                               
#pragma mark - EMSearchControllerDelegate     
                                                       
- (void)cancelButtonClicked
{
    [[RealtimeSearchUtil currentUtil] realtimeSearchStop];
}
                                               
- (void)searchTextChangeWithString:(NSString *)aString
{
    __weak typeof(self) weakSelf = self;
    [[RealtimeSearchUtil currentUtil] realtimeSearchWithSource:self.dataSourceArray searchText:aString collationStringSelector:@selector(showName) resultBlock:^(NSArray *results) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.resultController.displaySource removeAllObjects];
                [weakSelf.resultController.displaySource addObjectsFromArray:self.dataSourceArray];
                [weakSelf.resultController.tableView reloadData];
            });
        
    }];
}

#pragma mark - action

- (void)deleteCellAction:(NSIndexPath *)aIndexPath
{
    self.indexPath = aIndexPath;
    UIAlertView *alertView = [[ UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"message.deleteConversation", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
    [alertView show];
}

- (id)setupCellEditActions:(NSIndexPath *)aIndexPath
{
    if ([UIDevice currentDevice].systemVersion.floatValue < 11.0) {
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"delete",@"Delete") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            [self deleteCellAction:indexPath];
        }];
        deleteAction.backgroundColor = [UIColor redColor];
        return @[deleteAction];
    } else {
        UIContextualAction *deleteAction = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleDestructive title:NSLocalizedString(@"delete",@"Delete") handler:^(UIContextualAction * _Nonnull action, __kindof UIView * _Nonnull sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
            [self deleteCellAction:aIndexPath];
        }];
        deleteAction.backgroundColor = [UIColor redColor];
        
        UISwipeActionsConfiguration *config = [UISwipeActionsConfiguration configurationWithActions:@[deleteAction]];
        config.performsFirstActionWithFullSwipe = NO;
        return config;
    }
}

- (void)addContactAction
{
    AddFriendViewController *addController = [[AddFriendViewController alloc] init];
    [self.navigationController pushViewController:addController animated:YES];
}

#pragma mark - private
                                                       
- (void)setupSearchController
{
    [self enableSearchController];
    
    __weak ContactListViewController *weakSelf = self;
    [self.resultController setCellForRowAtIndexPathCompletion:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
        static NSString *CellIdentifier = @"BaseTableViewCell";
        BaseTableViewCell *cell = (BaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[BaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        NSDictionary *dic = weakSelf.dataSourceArray[indexPath.row];
        NSString *ImgPath = [NSString stringWithFormat:@"%@%@",FBHBaseURL,dic[@"headpic"]];
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:ImgPath]];
        cell.textLabel.text = dic[@"nickname"];
        cell.username = dic[@"nickname"];
        
        return cell;
    }];
    
    [self.resultController setHeightForRowAtIndexPathCompletion:^CGFloat(UITableView *tableView, NSIndexPath *indexPath) {
        return 50;
    }];
    
    [self.resultController setDidSelectRowAtIndexPathCompletion:^(UITableView *tableView, NSIndexPath *indexPath) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSDictionary *dic = weakSelf.dataSourceArray[indexPath.row];
        NSString *buddy =  dic[@"uid"];
        [weakSelf.searchController.searchBar endEditing:YES];
        YzUserInfoModel *model  = [[YzUserInfoModel alloc]init];
        model.uid = dic[@"add_who"];
        model.nickname = dic[@"nickname"];
        model.headpic = dic[@"headpic"];
        ChatViewController *chatVC = [[ChatViewController alloc] initWithConversationChatter:buddy
                                     conversationType:EMConversationTypeChat withInforModel:model];
        chatVC.title = [[UserProfileManager sharedInstance] getNickNameWithUsername:dic[@"nickname"]];
        [weakSelf.navigationController pushViewController:chatVC animated:YES];
                                               
        [weakSelf cancelSearch];
    }];
        
    UISearchBar *searchBar = self.searchController.searchBar;
    CGFloat height = searchBar.frame.size.height;
    if (height == 0) {
        height = 50;
    }
    searchBar.frame = CGRectMake(0, 0, self.tableView.frame.size.width, height);
    self.tableView.tableHeaderView = searchBar;
    [self.tableView reloadData];
}

- (void)_sortDataArray:(NSArray *)buddyList
{
    [self.dataArray removeAllObjects];
    [self.sectionTitles removeAllObjects];
    NSMutableArray *contactsSource = [NSMutableArray array];
    
    //从获取的数据中剔除黑名单中的好友
    NSArray *blockList = [[EMClient sharedClient].contactManager getBlackList];
    for (NSString *buddy in buddyList) {
        if (![blockList containsObject:buddy]) {
            [contactsSource addObject:buddy];
        }
    }
    
    //建立索引的核心, 返回27，是a－z和＃
    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
    [self.sectionTitles addObjectsFromArray:[indexCollation sectionTitles]];
    
    NSInteger highSection = [self.sectionTitles count];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
    for (int i = 0; i < highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sortedArray addObject:sectionArray];
    }
    
    //按首字母分组
    for (NSString *buddy in contactsSource) {
        EaseUserModel *model = [[EaseUserModel alloc] initWithBuddy:buddy];
        if (model) {
            model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];
            model.nickname = [[UserProfileManager sharedInstance] getNickNameWithUsername:buddy];
            
            NSString *firstLetter = [EaseChineseToPinyin pinyinFromChineseString:[[UserProfileManager sharedInstance] getNickNameWithUsername:buddy]];
            NSInteger section;
            if (firstLetter.length > 0) {
                section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
            } else {
                section = [sortedArray count] - 1;
            }
            
            NSMutableArray *array = [sortedArray objectAtIndex:section];
            [array addObject:model];
        }
    }
    
    //每个section内的数组排序
    for (int i = 0; i < [sortedArray count]; i++) {
        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(EaseUserModel *obj1, EaseUserModel *obj2) {
            NSString *firstLetter1 = [EaseChineseToPinyin pinyinFromChineseString:obj1.buddy];
            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
            
            NSString *firstLetter2 = [EaseChineseToPinyin pinyinFromChineseString:obj2.buddy];
            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
            
            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
        }];
        
        
        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
    }
    
    //去掉空的section
    for (NSInteger i = [sortedArray count] - 1; i >= 0; i--) {
        NSArray *array = [sortedArray objectAtIndex:i];
        if ([array count] == 0) {
            [sortedArray removeObjectAtIndex:i];
            [self.sectionTitles removeObjectAtIndex:i];
        }
    }
    
    [self.dataArray addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
}

#pragma mark - data

- (void)tableViewDidTriggerHeaderRefresh
{
    [self showHudInView:self.view hint:NSLocalizedString(@"loadData", @"Load data...")];
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = nil;
        NSArray *buddyList = [[EMClient sharedClient].contactManager getContactsFromServerWithError:&error];
        
        weakself.otherPlatformIds = [[EMClient sharedClient].contactManager getSelfIdsOnOtherPlatformWithError:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideHud];
        });
        if (!error) {
            [[EMClient sharedClient].contactManager getBlackListFromServerWithError:&error];
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakself.contactsSource removeAllObjects];
                    
                    for (NSInteger i = (buddyList.count - 1); i >= 0; i--) {
                        NSString *username = [buddyList objectAtIndex:i];
                        [weakself.contactsSource addObject:username];
                    }
                    [weakself _sortDataArray:self.contactsSource];
                });
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
//                [weakself showHint:NSLocalizedString(@"loadDataFailed", @"Load data failed.")];
                [weakself reloadDataSource];
            });
        }
        [weakself tableViewDidFinishTriggerHeader:YES reload:NO];
    });
}

#pragma mark - public

- (void)reloadDataSource
{
    [self.dataArray removeAllObjects];
    [self.contactsSource removeAllObjects];
    
    NSArray *buddyList = [[EMClient sharedClient].contactManager getContacts];
    
    for (NSString *buddy in buddyList) {
        [self.contactsSource addObject:buddy];
    }
    [self _sortDataArray:self.contactsSource];
    
    [self.tableView reloadData];
}

- (void)reloadApplyView
{
    NSInteger count = [[[ApplyViewController shareController] dataSource] count];
    self.unapplyCount = count;
    [self.tableView reloadData];
}

- (void)reloadGroupView
{
    [self reloadApplyView];
    
    if (_groupController) {
        [_groupController tableViewDidTriggerHeaderRefresh];
    }
}

- (void)addFriendAction
{
    AddFriendViewController *addController = [[AddFriendViewController alloc] init];
    [self.navigationController pushViewController:addController animated:YES];
}

@end
