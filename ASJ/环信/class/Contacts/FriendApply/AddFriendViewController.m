/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "AddFriendViewController.h"

#import "ApplyViewController.h"
#import "AddFriendCell.h"
#import "InvitationManager.h"
#import "YzLoveVipModel.h"
#import "ChatViewController.h"

@interface AddFriendViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataSource;

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation AddFriendViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    self.title = @"添加好友";
    self.view.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.0];
    
    [self.view addSubview:self.headerView];
    
    self.dataSource = [NSMutableArray array];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.headerView.frame)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.0];
    self.tableView.tableFooterView = footerView;
    
    UIButton *searchButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    searchButton.accessibilityIdentifier = @"search_contact";
    [searchButton setTitle:NSLocalizedString(@"search", @"Search") forState:UIControlStateNormal];
    [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:searchButton]];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    backButton.accessibilityIdentifier = @"back";
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.navigationItem setLeftBarButtonItem:backItem];
    
    [self.view addSubview:self.textField];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getter

- (UIView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
        _headerView.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.0];
        
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 40)];
        _textField.accessibilityIdentifier = @"contact_name";
        _textField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _textField.layer.borderWidth = 0.5;
        _textField.layer.cornerRadius = 3;
        _textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
        _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:15.0];
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.placeholder = NSLocalizedString(@"friend.inputNameToSearch", @"input to find friends");
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.delegate = self;
        [_headerView addSubview:_textField];
    }
    
    return _headerView;
}
#pragma mark - action
- (void)request{
    
    NSLog(@"555555----------");
    
    NSDictionary *param = @{@"uid":LoveDriverID,
                            @"token":FBH_USER_TOKEN,
                            @"page":@"1",
                            @"row_num":@"15",
                            @"keyword":self.textField.text};
    
    [NetMethod Post:FBHRequestUrl(kUrl_friend_search) parameters:param success:^(id responseObject){
        NSDictionary *InfoDic = responseObject;
        if (self.textField.isFirstResponder) {
            [self.textField resignFirstResponder];
        }
        if ([InfoDic[@"code"] integerValue] == 200){
            if ([InfoDic[@"data"] isKindOfClass:[NSNull class]]) {
                [MBProgressHUD showSuccess:@"暂无数据" toView:self.view];
                return ;
            }else{
                NSArray *arr = InfoDic[@"data"];
                if (arr.count == 0) {
                    [MBProgressHUD showSuccess:@"该号未注册会员" toView:self.view];
                    return;
                }
                if (self.dataSource.count) {
                    [self.dataSource removeAllObjects];
                }
                
                [self.dataSource addObjectsFromArray:[YzLoveVipModel mj_objectArrayWithKeyValuesArray:arr]];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError* error){
        [MBProgressHUD showSuccess:[NSString stringWithFormat:@"%@",error.localizedDescription] toView:self.view];
  
    }];



}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AddFriendCell";
    AddFriendCell *cell = (AddFriendCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[AddFriendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    YzLoveVipModel *model = self.dataSource[indexPath.row];
    NSString *ImgPath = [NSString stringWithFormat:@"%@%@",FBHBaseURL, model.headpic];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:ImgPath] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
    cell.textLabel.text = model.nickname;
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedIndexPath = indexPath;
    YzLoveVipModel *buddyName = [self.dataSource objectAtIndex:indexPath.row];
    if ([buddyName.uid isEqualToString:LoveDriverID]) {
        [MBProgressHUD showError:@"不能和自己聊天" toView:self.view];
        return;
    }
    
    YzUserInfoModel *model  = [[YzUserInfoModel alloc]init];
    model.uid = buddyName.uid;
    model.nickname = buddyName.nickname;
    model.headpic = buddyName.headpic;
    
    ChatViewController *chatController = [[ChatViewController alloc] initWithConversationChatter:model.uid conversationType:EMConversationTypeChat withInforModel:model];
    NSString *nickname =  buddyName.nickname;
    chatController.model = model;
    chatController.title = nickname.length > 0 ? nickname : buddyName.uid;
    [self.navigationController pushViewController:chatController animated:YES];
    
    [[EMClient sharedClient].contactManager addContact:buddyName.uid message:@"好友申请" completion:^(NSString *aUsername, EMError *aError) {
        
    }];
    
//    if ([buddyName.is_friend isEqualToString:@"0"]) {
//        NSDictionary *Info = @{@"uid":LoveDriverID,
//                               @"add_who":buddyName.uid,
//                               @"token":FBH_USER_TOKEN};
//
//        [NetMethod Post:FBHRequestUrl(kUrl_friend_add) parameters:Info success:^(id responseObject) {
//
//        } failure:^(NSError *error) {
//            [MBProgressHUD showSuccess:error.localizedDescription toView:self.view];
//        }];
//
//        [[EMClient sharedClient].contactManager addContact:buddyName.uid message:@"好友申请" completion:^(NSString *aUsername, EMError *aError) {
//
//        }];
//    }
    
    
    
//    if ([self didBuddyExist:buddyName.uid]) {
//        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.repeat", @"'%@'has been your friend!"), buddyName];
//
//        [EMAlertView showAlertWithTitle:message
//                                message:nil
//                        completionBlock:nil
//                      cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
//                      otherButtonTitles:nil];
//
//    }
//    else if([self hasSendBuddyRequest:buddyName.uid])
//    {
//        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.repeatApply", @"you have send fridend request to '%@'!"), buddyName];
//        [EMAlertView showAlertWithTitle:message
//                                message:nil
//                        completionBlock:nil
//                      cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
//                      otherButtonTitles:nil];
//
//    }else{
//        [self showMessageAlertView];
//    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - action

- (void)searchAction
{
    
    [_textField resignFirstResponder];
    
    NSString *searchName = _textField.text;
    if ([searchName length] == 0) {
        return;
    }
    
#warning Test code, by default, the user system has a user with the same name.
    searchName = [searchName lowercaseString];
    NSString *loginUsername = [[[EMClient sharedClient] currentUsername] lowercaseString];
    if ([searchName isEqualToString:loginUsername]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notAddSelf", @"can't add yourself as a friend") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        [alertView show];
        
        return;
    }
    
    //判断是否已发来申请
    NSArray *applyArray = [[ApplyViewController shareController] dataSource];
    if (applyArray && [applyArray count] > 0) {
        for (ApplyEntity *entity in applyArray) {
            ApplyStyle style = [entity.style intValue];
            BOOL isGroup = style == ApplyStyleFriend ? NO : YES;
            if (!isGroup && [entity.applicantUsername isEqualToString:searchName]) {
                NSString *str = [NSString stringWithFormat:NSLocalizedString(@"friend.repeatInvite", @"%@ have sent the application to you"), searchName];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:str delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                [alertView show];
                
                return;
            }
        }
    }
    
    [self.dataSource removeAllObjects];
    [self request];
//    [self.dataSource addObject:searchName];
//    [self.tableView reloadData];
}

- (BOOL)hasSendBuddyRequest:(NSString *)buddyName
{
    NSArray *userlist = [[EMClient sharedClient].contactManager getContacts];
    for (NSString *username in userlist) {
        if ([username isEqualToString:buddyName]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)didBuddyExist:(NSString *)buddyName
{
    NSArray *userlist = [[EMClient sharedClient].contactManager getContacts];
    for (NSString *username in userlist) {
        if ([username isEqualToString:buddyName]){
            return YES;
        }
    }
    return NO;
}

- (void)showMessageAlertView
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"saySomething", @"say somthing")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                          otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView cancelButtonIndex] != buttonIndex) {
        UITextField *messageTextField = [alertView textFieldAtIndex:0];
        
        NSString *messageStr = @"";
        NSString *username = [[EMClient sharedClient] currentUsername];
        if (messageTextField.text.length > 0) {
            messageStr = [NSString stringWithFormat:@"%@：%@", username, messageTextField.text];
        }
        else{
            messageStr = [NSString stringWithFormat:NSLocalizedString(@"friend.somebodyInvite", @"%@ invite you as a friend"), username];
        }
        [self sendFriendApplyAtIndexPath:self.selectedIndexPath
                                 message:messageStr];
    }
}

- (void)sendFriendApplyAtIndexPath:(NSIndexPath *)indexPath
                           message:(NSString *)message
{
    YzLoveVipModel *buddyName = [self.dataSource objectAtIndex:indexPath.row];
    if (buddyName.uid && buddyName.uid.length > 0) {
        [self showHudInView:self.view hint:NSLocalizedString(@"friend.sendApply", @"sending application...")];
        EMError *error = [[EMClient sharedClient].contactManager addContact:buddyName.uid message:message];
        [self hideHud];
        if (error) {
            [self showHint:NSLocalizedString(@"friend.sendApplyFail", @"send application fails, please operate again")];
        }
        else{
            [self showHint:NSLocalizedString(@"friend.sendApplySuccess", @"send successfully")];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
