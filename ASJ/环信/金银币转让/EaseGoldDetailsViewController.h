//
//  EaseGoldDetailsViewController.h
//  ASJ
//
//  Created by Ss H on 2018/3/19.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseMessageViewController.h"

@interface EaseGoldDetailsViewController : UIViewController
@property (nonatomic,retain)NSDictionary *ext;
@property (nonatomic,retain)EMMessage *userInfoModel;
@property (nonatomic,retain)UILabel *titleLabel;
@property (nonatomic,retain)UILabel *numLabel;
@property (nonatomic,retain)UILabel *fromTimeLabel;
@property (nonatomic,retain)UILabel *toTimeLabel;
@property (nonatomic,retain)UIImageView *imageView;

@end
