//
//  EaseGoldDetailsViewController.m
//  ASJ
//
//  Created by Ss H on 2018/3/19.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseGoldDetailsViewController.h"

@interface EaseGoldDetailsViewController ()

@end

@implementation EaseGoldDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"转让详情";
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.numLabel];
    [self.view addSubview:self.fromTimeLabel];
    [self.view addSubview:self.imageView];
//    [self.view addSubview:self.toTimeLabel];

}
-(UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(YzWidth/2-20, 30, 40, 40)];
        NSString *ImgPath = [self.userInfoModel.ext objectForKey:@"image"];
        [_imageView sd_setImageWithURL:[NSURL URLWithString:ImgPath] placeholderImage:[UIImage imageNamed:@"logo"]];
    }
    return _imageView;
}
-(UILabel*)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, YzWidth, 30)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = [self.ext objectForKey:@"nameTitle2"];
    }
    return _titleLabel;
}
-(UILabel*)numLabel
{
    if (!_numLabel) {
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 130, YzWidth, 40)];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.text = [self.ext objectForKey:@"goldStr"];
    }
    return _numLabel;
}
-(UILabel*)fromTimeLabel
{
    if (!_fromTimeLabel) {
        _fromTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, YzHeight-130, YzWidth, 30)];
        _fromTimeLabel.textAlignment = NSTextAlignmentCenter;
        NSString *time = [NSString stringWithFormat:@"%lld",self.userInfoModel.timestamp];
        _fromTimeLabel.text =[NSString stringWithFormat:@"%@%@",@"创建时间",[self timeStr:time]];
    }
    return _fromTimeLabel;
}
//-(UILabel*)toTimeLabel
//{
//    if (!_toTimeLabel) {
//        _toTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, YzHeight-100, YzWidth, 30)];
//        _toTimeLabel.textAlignment = NSTextAlignmentCenter;
//        NSString *time = [NSString stringWithFormat:@"%lld",self.userInfoModel.timestamp];
//
//        _toTimeLabel.text =[self timeStr:time];
//
//    }
//    return _toTimeLabel;
//}
-(NSString *)timeStr:(NSString *)time
{
    float num = [time floatValue]/1000;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY/MM/dd"];
    
    NSDate*confromTimesp = [NSDate dateWithTimeIntervalSince1970:num];
    NSString*confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
