//
//  EaseGoldTransferToConfirmViewController.h
//  ASJ
//
//  Created by Ss H on 2018/3/15.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseMessageViewController.h"

@interface EaseGoldTransferToConfirmViewController : UIViewController
@property (nonatomic , retain)YzUserInfoModel *userInfoModel;
@property (nonatomic , retain)NSString *num;
@property (nonatomic,retain)NSString *titleStr;
@property (nonatomic,retain)UILabel *titleLabel;
@property (nonatomic,retain)UILabel *numLabel;
@property (nonatomic,retain)NSString *imageStr;
@property (nonatomic,retain)UIButton *toConfirmButton;


@end
