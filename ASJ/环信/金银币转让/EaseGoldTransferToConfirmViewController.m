//
//  EaseGoldTransferToConfirmViewController.m
//  ASJ
//
//  Created by Ss H on 2018/3/15.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseGoldTransferToConfirmViewController.h"

@interface EaseGoldTransferToConfirmViewController ()

@end

@implementation EaseGoldTransferToConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWithNavigationView];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.numLabel];
    [self.view addSubview:self.toConfirmButton];
    
}
//导航栏ui
-(void)initWithNavigationView
{
    UIButton *leftbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftbtn.frame=CGRectMake(10, 20, 45, 45);
    [leftbtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [leftbtn setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [self.view addSubview:leftbtn];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-80, 60)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"支付";
    [self.view addSubview:titleLabel];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width-80, 0.5)];
    lineView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:lineView];
    
}
-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width-80, 30)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = self.userInfoModel.nickname;
    }
    return _titleLabel;
}
-(UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, self.view.frame.size.width-80, 60)];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.font = [UIFont systemFontOfSize:25];
        if ([self.titleStr isEqualToString:@"金积分转让"]) {
            _numLabel.text =[NSString stringWithFormat:@"%@%@",self.num,@"金积分"] ;
        }else{
            _numLabel.text =[NSString stringWithFormat:@"%@%@",self.num,@"银积分"] ;
        }
    }
    return _numLabel;
}
-(UIButton *)toConfirmButton
{
    if (!_toConfirmButton) {
        _toConfirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _toConfirmButton.frame = CGRectMake(20, YzHeight/2-120, self.view.frame.size.width-120, 40);
        [_toConfirmButton setTitle:@"确认支付" forState:UIControlStateNormal];
        _toConfirmButton.layer.cornerRadius = 3.0;
        _toConfirmButton.layer.borderColor = [UIColor grayColor].CGColor;
        _toConfirmButton.layer.borderWidth = 0.5f;
        _toConfirmButton.titleLabel.font = [UIFont systemFontOfSize:25];
        _toConfirmButton.backgroundColor = [UIColor orangeColor];
        [_toConfirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_toConfirmButton addTarget:self action:@selector(toTransferMessage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _toConfirmButton;
}
//吊起转账接口
-(void)toTransferMessage
{
   
    NSString *type;
    if ([self.titleStr isEqualToString:@"金积分转让"]) {
        type = @"0";
    }else{
        type = @"1";

    }
    
    if (!self.userInfoModel.uid) {
        [MBProgressHUD showError:@"非好友不能转让积分" toView:self.view];
        return;
    }
    
    NSDictionary *signDic = @{@"uid":LoveDriverID,
                                  @"token":FBH_USER_TOKEN,
                                  @"give_uid":self.userInfoModel.uid,
                                  @"price":self.num,
                                  @"type":type
                                  };
    
    
    // 获取服务端签名
    [NetMethod Post:FBHRequestUrl(kUrl_get_sign) parameters:signDic success:^(id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            NSString *sign = responseObject[@"data"];
            [self goldPayTransferWithSign:sign];
        }else{
            [MBProgressHUD showError:@"获取签名失败" toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:NetProblem toView:self.view];
    }];
    
    NSLog(@"转账");
}

- (void)goldPayTransferWithSign:(NSString *)sign{
    
    NSString *type;
    if ([self.titleStr isEqualToString:@"金积分转让"]) {
        type = @"0";
    }else{
        type = @"1";
        
    }
    
    NSDictionary *transferDic = @{@"uid":LoveDriverID,
                                  @"token":FBH_USER_TOKEN,
                                  @"give_uid":self.userInfoModel.uid,
                                  @"price":self.num,
                                  @"type":type,
                                  @"sign":sign
                                  };
    [NetMethod Post:FBHRequestUrl(kUrl_give_integral) parameters:transferDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        int code = [[dic objectForKey:@"code"] intValue];
        
        if (code == 200)
        {
            [MBProgressHUD showSuccess:@"转账成功" toView:self.view];
            [self.view removeFromSuperview];
            
            NSString *str;
            NSString *str1;
            NSString *ifGold = @"YES";
            if ([self.titleStr isEqualToString:@"金积分转让"]) {
                str = @"gold";
                str1 =[NSString stringWithFormat:@"%@%@",self.num,@"金积分"] ;
            }else{
                str = @"silver";
                str1 =[NSString stringWithFormat:@"%@%@",self.num,@"银积分"] ;
            }
            //创建通知
            NSNotification *notification =[NSNotification notificationWithName:@"zifuTz" object:nil userInfo:@{@"num":self.num,@"goldStr":str1,@"nameTitle":self.titleStr,@"nameTitle2":[NSString stringWithFormat:@"%@%@",@"转给",self.userInfoModel.nickname],@"image":self.imageStr,@"type":str,@"em_readFire":ifGold ,@"records":@YES}];
            //通过通知中心发送通知
            [[NSNotificationCenter defaultCenter] postNotification:notification];
            
        }else{
            
            NSString *msg = [dic objectForKey:@"msg"];
            [MBProgressHUD showError:msg toView:self.view];
            
        }
        
    } failure:^(NSError *error) {
        
        NSLog(@"error = %@",error.description);
        
    }];
}
-(void)dismiss{
    
    [self.view removeFromSuperview];
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"addressQuXiao" object:nil userInfo:nil];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
