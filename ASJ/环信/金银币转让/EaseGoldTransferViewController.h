//
//  EaseGoldTransferViewController.h
//  ASJ
//
//  Created by Ss H on 2018/3/14.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseMessageViewController.h"
#import "EaseGoldTransferToConfirmViewController.h"

@interface EaseGoldTransferViewController : UIViewController
@property (nonatomic , retain)YzUserInfoModel *userInfoModel;
@property (nonatomic,retain)NSString *uId;
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *imageUrl;
@property (nonatomic,retain)NSString *titleStr;
@property (nonatomic,retain)UIView *backGroundView;
@property (nonatomic,retain)UIScrollView *backGroundScrollView;
@property (nonatomic,retain)UIImageView *imageView;
@property (nonatomic,retain)UILabel *titleLabel;
@property (nonatomic,retain)UITextField *goldOrTransferTextField;
@property (nonatomic,retain)UIButton *transferButton;
@property (nonatomic,retain)UIView *addWindowView;
@property (strong, nonatomic) id<IMessageModel> model;

@property(nonatomic,retain)EaseGoldTransferToConfirmViewController *ToConfirmVc;



@end
