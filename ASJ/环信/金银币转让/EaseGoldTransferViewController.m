//
//  EaseGoldTransferViewController.m
//  ASJ
//
//  Created by Ss H on 2018/3/14.
//  Copyright © 2018年 TS. All rights reserved.
//

#import "EaseGoldTransferViewController.h"

#define SafeAreaTopHeight ([UIScreen mainScreen].bounds.size.height == 812.0 ? 88 : 64)

@interface EaseGoldTransferViewController ()
@end

@implementation EaseGoldTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self initWithNavigationView];
    [self.view addSubview:self.backGroundScrollView];
    [self.backGroundScrollView addSubview:self.backGroundView];
    [self.backGroundView addSubview:self.imageView];
    [self.backGroundView addSubview:self.titleLabel];
    [self.backGroundView addSubview:self.transferButton];
    [self.backGroundView addSubview:self.goldOrTransferTextField];
    [self.goldOrTransferTextField becomeFirstResponder];
    if (YzWidth == 320) {
        self.backGroundScrollView.contentOffset = CGPointMake(0, -30);
    }
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addressQuXiao:)name:@"addressQuXiao" object:nil];
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zhifuTongzi:)name:@"zifuTz" object:nil];
}
//导航栏ui
-(void)initWithNavigationView
{
    //设置导航栏的leftButton
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, YzWidth, SafeAreaTopHeight)];
    navigationView.backgroundColor = Red;
    [self.view addSubview:navigationView];
    UIButton *leftbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftbtn.frame=CGRectMake(10, 20, 45, 45);
    [leftbtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [leftbtn setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [navigationView addSubview:leftbtn];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, YzWidth-leftbtn.frame.size.width, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = self.titleStr;
    [navigationView addSubview:titleLabel];
}
-(UIView *)backGroundView
{
    if (!_backGroundView) {
        _backGroundView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, YzWidth-20, (YzHeight -SafeAreaTopHeight-10)/2+40 )];
        _backGroundView.backgroundColor = [UIColor whiteColor];
    }
    return _backGroundView;
}
-(UIScrollView *)backGroundScrollView
{
    if (!_backGroundScrollView) {
        _backGroundScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, YzWidth, YzHeight -SafeAreaTopHeight-10 )];
        _backGroundScrollView.backgroundColor =[UIColor colorWithRed:248 / 255.0 green:248 / 255.0 blue:248 / 255.0 alpha:1.0] ;
    }
    return _backGroundScrollView;
}
-(UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(YzWidth/2-25, 20, 30, 40)];
        NSString *ImgPath = [NSString stringWithFormat:@"%@%@",FBHBaseURL,self.userInfoModel.headpic];
        [_imageView sd_setImageWithURL:[NSURL URLWithString:ImgPath] placeholderImage:[UIImage imageNamed:@"yyyGrayImg"]];
        UILabel *moneyLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, _imageView.frame.origin.y+80, _backGroundView.frame.size.width, 30)];
        moneyLabel.text = @"转账";
        moneyLabel.textColor = [UIColor grayColor];
        [_backGroundView addSubview:moneyLabel];
    }
    return _imageView;
}
-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _imageView.frame.size.height+_imageView.frame.origin.y, _backGroundView.frame.size.width, 30)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = self.userInfoModel.nickname;
    }
    return _titleLabel;
}
-(UITextField *)goldOrTransferTextField
{
    if (!_goldOrTransferTextField) {
        _goldOrTransferTextField = [[UITextField alloc]initWithFrame:CGRectMake(0, _imageView.frame.origin.y+110, _backGroundView.frame.size.width, 60)];
        //设置左边视图的宽度
        _goldOrTransferTextField.leftView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 70, 60)];
        _goldOrTransferTextField.leftViewMode = UITextFieldViewModeAlways;
        _goldOrTransferTextField.font = [UIFont systemFontOfSize:30];
        _goldOrTransferTextField.keyboardType = UIKeyboardTypeNumberPad;
        UILabel *leftView = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 60, 60)];
        leftView.text = [self.titleStr substringToIndex:3];
//        leftView.font = [UIFont systemFontOfSize:20];
        leftView.textAlignment = NSTextAlignmentCenter;
        [_goldOrTransferTextField.leftView addSubview:leftView];
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(10, _goldOrTransferTextField.frame.size.height+_goldOrTransferTextField.frame.origin.y+15, _backGroundView.frame.size.width-20, 0.5)];
        lineView.backgroundColor = [UIColor grayColor];
        [self.backGroundView addSubview:lineView];
    }
    return _goldOrTransferTextField;
}
-(UIButton *)transferButton
{
    if (!_transferButton) {
        _transferButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _transferButton.frame = CGRectMake(20, _backGroundView.frame.size.height-80, _backGroundView.frame.size.width-40, 40);
        [_transferButton setTitle:@"转账" forState:UIControlStateNormal];
        _transferButton.layer.cornerRadius = 3.0;
        _transferButton.layer.borderColor = [UIColor grayColor].CGColor;
        _transferButton.layer.borderWidth = 0.5f;
        _transferButton.titleLabel.font = [UIFont systemFontOfSize:30];
        _transferButton.backgroundColor = [UIColor orangeColor];
        [_transferButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_transferButton addTarget:self action:@selector(transferButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _transferButton;
}

//转让点击事件
-(void)transferButtonAction{
    
    [_goldOrTransferTextField resignFirstResponder];

    self.addWindowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.addWindowView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7];
    self.ToConfirmVc = [[EaseGoldTransferToConfirmViewController alloc]init];
    self.ToConfirmVc.userInfoModel = self.userInfoModel;
    self.ToConfirmVc.num = [NSString stringWithFormat:@"%@",_goldOrTransferTextField.text];
    self.ToConfirmVc.imageStr =[NSString stringWithFormat:@"%@%@",FBHBaseURL,self.userInfoModel.headpic];
    self.ToConfirmVc.titleStr = self.titleStr;
    self.ToConfirmVc.view.frame=CGRectMake(40,YzHeight/4, YzWidth-80, YzHeight/2-60);
    [self.addWindowView addSubview:self.ToConfirmVc.view];
    [[UIApplication sharedApplication].keyWindow addSubview:self.addWindowView];
    
}
//点击蒙蔽页面取消按钮
- (void)addressQuXiao:(NSNotification *)text{
    [self.addWindowView removeFromSuperview];
}
- (void)zhifuTongzi:(NSNotification *)text{
    [self.addWindowView removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismiss{
    
    [_goldOrTransferTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
